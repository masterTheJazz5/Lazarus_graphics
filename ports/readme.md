
## Ports

Everything listed herein is FULLY supported. Pascal code only in the master branch, and main folders.
Ports go IN THIS FOLDER.

NO porting to C\CPP\VC (any VC) will be done by me, if you backport(not only will you give credit where its due)- its on you.
If you break things, you keep all of the pieces-

my best advice is to make frequent backups, especially of release-able "releases" that function/work, even if partially implemented(or buggy).
Its better to release something, and have code in flux- than to release nothing because the code is in flux, and broken.


fones:
------

no guarantee. Im a PC guy.

AMIGA:
-----
No guarantee if this is FOSS or not, if its not I will link to github instead. I thought this interesting.

This is an up-and-comer.
m68k needs picasso96+, in general most add-on board should work. This incldes vampyre. Natve video out boards WONT WORK.
edit the modelist, and color palette(!!!)

Im dumping SDL here, if you want to try this. I dont have one, I can emulate- either via Pi, or by UAE.(qemu??)
Note that pre-AmigaOS4 options are limited due to "availablity of OpenGL".

MorphOS runs ok on PowerPC books, its "the new standard". If the Os "looks like ASS", its a m68k.
Uses DOS internally(and 32bit instructions, whether or not the cpu supports them).

You like win31? youll love the older amiga....PowerPC editions mimick win7+

-There is no "x86 amiga", you are either emulating a m68k- or a PowerPC one. The RasPi is often used because "JIT code -adjusts itself" quite faster on the Pi.
ATMEL AVR(arduino) emulation uses a similar concept.

My code can hook this. if SDL can run, Lazarus GFX can run. I dont have amiga specific tweaks added yet. Screen Resoloutions might be "jacked-up".

"pure basic":
-------------

QB64 is available. The source files are using windows linefeeds- fix that -- and theyll build on unices.
(MSFT QB support ended with dos)

M$FT has moved VB onto VB.net-

(to promote .net- lighten app depends requirements).
-They are using it internally, and development is not stalled- just moved(and enhanced).

mono/.NET development still continues, as a matter of fact- Pascal can also use .NET(ABC Pascal,russian site)

"VB"?

FreeBasic:
--------

FreeBasic is yet another option, but theres 3 different IDEs. Only ONE seems to work correctly.


GAMBAS:
-------

MonoDevel is better supported.
Unices only. Non-portable code. CYGWIN and MSYS cant magically fix your problems. Needs UX code(wxwidgets) at minimal.
SDL support this way, WILL BE HAMPERED.

RealBasic:
----------

-This is not available for 68k Mac, however a OSX/OS9 combination of RealBasic(vb equivalent) is still available- and supported.
Be aware theres TWO seperate installers- one for OSX, another for OS9.
Older unsupported installers can be found on "macintosh garden" website.

I stick with version 5.5- This way I get win32(basic win95/98/me), os9, and OSX support. (5.3 is used for windows)

Only modern(post Lion OSX) are supported, otherwise from the same company.

Apple has a habit of(fpc devs are no stranger to this) forcing the latest OS on you, to force the latest coding standard on you-
then trying to "bite you in the ascii" for "coding for a dated platform". OS9 and PowerPC platforms have been long forgotten by Apple,
its basically abandonHardWare to them.

I wil happily supporting porting LazGfx to these environments.

Python:
------

Yes, im aware this uses pygame/pygame-sdl2. Ive not ported that yet. More working examples of SDL/SDLv2.
sdl12-compat is available for other platforms? -EEE- IDK.

## Why not TKinter?

Python TKinter crashes on OSX, limited support is available for OS9. Perhaps this is a TOOL(kit) problem.
OS9 had an issue where things would deadlock, if reentrant code was called, sometimes. TOOLS are crated this way thru MPW.


WebGL(3d):
------

Some code modifications may be necessary, but this works well given two things:

computer support(not my dept on this)
bandwidth(needs lag free high speed communications, to function- for an example see SpaceX/NASA "ISS docking trainer")


JSCanvas(2d):
---------
THis is technically an IE API...but it works well across browsers. Theres also HTML5 Canvas API.


CodeWarrior:
--------

Use for OS9 and below. FPC/TP/BP only has limited support for pre-OS8.
Fetch from mac garden website. Abandonware.
