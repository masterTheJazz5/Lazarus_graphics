#
# Just Triangle Faces (.jtf) exporter for Blender
#

import bpy, bmesh, struct, array

def triangulate(mesh):
    import bmesh
    bm = bmesh.new()
    bm.from_mesh(mesh)
    bmesh.ops.triangulate(bm, faces=bm.faces)
    bm.to_mesh(mesh)
    bm.free()

def faceToLine(face):
    return " ".join([("%.6f" % v) for v in face] + ["\n"])

def write(path):
    scene = bpy.context.scene
    data = []
    faceCount = 0
    for obj in bpy.context.selected_objects:
        bm = None
        mesh = None
        try:
            mesh = obj.to_mesh(scene, True, 'PREVIEW')
            bm = bmesh.new();
            bm.from_mesh(mesh)
            bmesh.ops.triangulate(bm, faces=bm.faces)
            bm.to_mesh(mesh)
            bm.free()
            bm = None
        except Exception as e:
            print('Error:')
            print(e)
            if mesh is not None:
                bpy.data.meshes.remove(mesh)
                mesh = None
            if bm is not None:
                bm.free()
        if mesh is not None:
            mesh.calc_tessface(free_mpoly=True)
            matrix = obj.matrix_world.copy()
            v = mesh.vertices
            for mf in mesh.tessfaces:
                fv = mf.vertices
                uv0 = mesh.tessface_uv_textures[0].data[mf.index].uv1
                uv1 = mesh.tessface_uv_textures[0].data[mf.index].uv2
                uv2 = mesh.tessface_uv_textures[0].data[mf.index].uv3
                data.append(v[fv[0]].co[0])
                data.append(v[fv[0]].co[2])
                data.append(-v[fv[0]].co[1])
                data.append(v[fv[0]].normal[0])
                data.append(v[fv[0]].normal[2])
                data.append(-v[fv[0]].normal[1])
                data.append(uv0[0])
                data.append(1-uv0[1])
                data.append(v[fv[1]].co[0])
                data.append(v[fv[1]].co[2])
                data.append(-v[fv[1]].co[1])
                data.append(v[fv[1]].normal[0])
                data.append(v[fv[1]].normal[2])
                data.append(-v[fv[1]].normal[1])
                data.append(uv1[0])
                data.append(1-uv1[1])
                data.append(v[fv[2]].co[0])
                data.append(v[fv[2]].co[2])
                data.append(-v[fv[2]].co[1])
                data.append(v[fv[2]].normal[0])
                data.append(v[fv[2]].normal[2])
                data.append(-v[fv[2]].normal[1])
                data.append(uv2[0])
                data.append(1-uv2[1])
                faceCount = faceCount + 1
            bpy.data.meshes.remove(mesh)
    file = open(path, "wb")
    head = struct.pack('<BBBBii', 74, 84, 70, 33, 0, faceCount)
    file.write(head)
    floats = array.array('f', data)
    floats.tofile(file)
    file.close()
