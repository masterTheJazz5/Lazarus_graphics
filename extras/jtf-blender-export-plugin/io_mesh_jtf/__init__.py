#
# Just Triangle Faces (.jtf) exporter for Blender
#

bl_info = {
    "name": "JTF format",
    "author": "Kostas Michalopoulos (badsector)",
    "version": (0, 1, 0),
    "blender": (2, 78, 0),
    "location": "File > Import-Export > Just Triangle Faces",
    "description": "Import-Export JTF files",
    "warning": "",
    "wiki_url": "",
    "support": "COMMUNITY",
    "category": "Import-Export",
}

if "bpy" in locals():
    import importlib
    if "export_jtf" in locals():
        importlib.reload(export_jtf)
else:
    import bpy

from bpy.props import StringProperty, BoolProperty
from bpy_extras.io_utils import ExportHelper

class JTFExporter(bpy.types.Operator, ExportHelper):
    """Save triangle mesh data with UV and normals"""
    bl_idname = "export_mesh.jtf"
    bl_label = "Export JTF"
    filename_ext = ".jtf"
    filter_glob = StringProperty(default="*.jtf", options={'HIDDEN'})
    def execute(self, context):
        from . import export_jtf
        export_jtf.write(self.filepath)
        return {'FINISHED'}

def menu_export(self, context):
    self.layout.operator(JTFExporter.bl_idname, text="Just Triangle Faces (.jtf)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_export)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_export)

if __name__ == "__main__":
    register()
