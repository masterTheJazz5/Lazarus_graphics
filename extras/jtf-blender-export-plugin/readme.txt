Blender 2.78 export script for JTF
==================================

  Place the io_mesh_jtf directory to your blender addons folder
(like blender/2.78/scripts/addons).  Then from user preferences
go to Addons, select Import-Export category and then scroll down
to find the JTF export addon.  Click on the checkbox at the top
left corner to enable it.

Kostas "Bad Sector" Michalopoulos
