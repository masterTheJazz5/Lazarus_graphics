unit logger;
{$mode objfpc}

interface


uses
{$IFDEF windows}
	dos,windows,wincrt,
{$ENDIF}
{$IFDEF go32}
	dos,crt,
{$ENDIF}
{$ifdef unix}
    baseunix,ncrt,
{$endif}    
    sysutils;


const
  critical='CRITICAL ERROR: ';
  normal='Hint: ';
  warning='WARNING: ';

var
   outputfile: Textfile; 
   logging,donelogging:boolean;
   MyTime: TDateTime;
   IsConsoleInvoked:boolean; external; //lazgfx unit


Procedure LogLn(s: string);
procedure StopLogging; 
procedure StartLogging;

implementation



//usually you want to write a line.
Procedure LogLn(s: string);
var
    v:string;
Begin

  writeln( DateTimeToStr(MyTime),' : ',s); //to the debugging outputfile console first, then file.
  v:=( (DateTimeToStr(MyTime))+' : '+s);
  Writeln(outputfile,v);
End;


procedure StartLogging; 
var
  outputfile:file; //oops! forgot this.
begin
//open or rewrite the file

  {$I-}
  Assign (outputfile,'lazgfx-debug.log');
  Reset (outputfile);
  {$I+}

  if IoResult=0 then begin
    logging:=true;
    donelogging:=false;
  end else begin
      writeln('Log File Doesnt Exist, Creating it.');
     {$I-}
      assign(outputfile,'lazgfx-debug.log'); //spurious?
      rewrite(outputfile);
     {$I+}  
       
   if IoResult=0 then begin
    logging:=true;
    donelogging:=false;
   end else begin  
    //if we have an issue here, we dont have write permission in the current directory. Kind of a problem.
    writeln('SERIOUS PROBLEM:  cannot write to current directory. Bailing.');
    halt;    
  end;
end;
end;

procedure StopLogging; //finalization

begin
    donelogging:=true;
    logging:=false;
    close(outputfile);
end;

begin 
end.
 

