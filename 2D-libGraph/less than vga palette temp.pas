  valuelist4a: array [0..11] of byte;
  valuelist4b: array [0..11] of byte;
  valuelist4c: array [0..11] of byte;
  valuelist4d: array [0..8] of byte;

//EGA- not CGA16. If this was a fixed and locked pallette- this might be accurate.
//CGA pulls from one of the four above.(DONT CHEAT!!)

  valuelist16: array [0..48] of byte; //actual palette
  valuelist64: array [0..191] of byte; //pull from this list- palette
//I have to work out a way of not cheating but leaving the 16 colors viable- even dragged back from 32bit floats...


//for your convienience- greyscale palettes
  GreyList16:array [0..48] of byte;
  valuelist256: array [0..767] of byte;

//note: use type defines if this doesnt work like it should.
  TPalette4a:TRec4;
  TPalette4b:TRec4;
  TPalette4c:TRec4;
  TPalette4d:TRec2;

  PPalette4a:^TRec4;
  PPalette4b:^TRec4;
  PPalette4c:^TRec4;
  PPalette4d:^TRec2;

//there is no 16color palette- you mean 64- set w 16..
  PPalette64:^TRec64;
  TPalette64:TRec64;

  PPalette256:^TRec256;
  TPalette256:TRec256;

//patchy hack
  PPalette16Grey:^TRec64;
  TPalette64Grey:TRec64;

  PPalette256Grey:^TRec256;
  TPalette256Grey:TRec256;


procedure initPaletteGrey16;
procedure initPaletteGrey256;
procedure initCGAPalette0;
procedure initCGAPalette1;
procedure initCGAPalette2;
procedure initCGAPalette3;
procedure initPalette64;
procedure Save16Palette(filename:string);
procedure Read16Palette(filename:string; ReadColorFile:boolean);
procedure initPalette256;
procedure Save256Palette(filename:string);
procedure Read256Palette(filename:string; ReadColorFile:boolean);

function RGB4FromLongWord(Someword:Longword):SDL_Color;
function RGB8FromLongWord(Someword:Longword):SDL_Color;
function RGB5551FromLongWord(Someword:Longword):SDL_Color;
function RGB565FromLongWord(Someword:Longword):SDL_Color;

function Word15ToSDLColor(someWord:Word):PSDL_Color;
var
    someColor:PSDL_Color;

begin
    red_mask := $F800;
    green_mask := $7E0;
    blue_mask := $1F;

    red_value := (someWord and red_mask) shr 11;
    green_value := (someWord and green_mask) shr 5;
    blue_value := (someWord and blue_mask);

    // Expand to 8-bit values.
    someColor^.r := red_value shl 3;
    someColor^.g := green_value shl 2;
    someColor^.b := blue_value shl 3;

    Word15ToSDLColor:=someColor;
end;

function Word16ToSDLColor(someWord:Word):PSDL_Color;
var
    someColor:PSDL_Color;

begin
    red_mask := $7C00;
    green_mask := $3E0;
    blue_mask := $1F;

    red_value := (someWord and red_mask) shr 10;
    green_value := (someWord and green_mask) shr 5;
    blue_value := (someWord and blue_mask);

    // Expand to 8-bit values.
    someColor^.r := red_value shl 3;
    someColor^.g := green_value shl 3;
    someColor^.b := blue_value shl 3;

    Word16ToSDLColor:=someColor;
end;

//converts SDL RGB record into GLShort/Word
function SDLColorToWord15(someColor:PSDL_Color):Word;
var
    someWord:Word;

begin
   someWord := ((someColor^.r shl 11) or (someColor^.g shl 5) or someColor^.b);
end;

function SDLColorToWord16(someColor:PSDL_Color):Word;
var
    someWord:Word;

begin
   someWord := ((someColor^.r shl 10) or (someColor^.g shl 5) or someColor^.b);
end;


{
Thers a RUB here:
    CGA uses 4 colors from 16 possible but in higher depths-there is no way to tell what the color is set to.
        Ideally you would restrict to "nearest colors" using some algorithm.
        RGBK or CMYK is a sane, reasonable value for a CGA palette. Its just not "historically accurate".
        (I cant stop you- but Id start with THESE defaults)

    Same with EGA. EGA and above were programmable.
    VGA was a standard- but Ive seen some awkward VGA palettes, too.
    Thank God palettes stopped at 256 colors.

    Think in "crayon colors" and you will do fine here.
    (There is a small box, a medium box, and a YUGE box of crayons here.)

CGA Palette:

160x100: full 16 available
320x200: use ONE of these (modes 0,1,2) palettes

Black, as well as all of the other colors- are programmable- 
although most old apps did not change them. 

This applies to anything above 160x100.

    0: black,red, yellow, green
    1: black,cyan, magenta, white
    2: black,cyan,red,white (greyscale)

640x200: "Black" and "white" only

}

procedure initCGAPalette0;

var
   num,i:integer;

begin  

valuelist4a[0]:=$00;
valuelist4a[1]:=$00;
valuelist4a[2]:=$00;

valuelist4a[3]:=$55;
valuelist4a[4]:=$ff;
valuelist4a[5]:=$55;

valuelist4a[6]:=$ff;
valuelist4a[7]:=$55;
valuelist4a[8]:=$55;

valuelist4a[9]:=$ff;
valuelist4a[10]:=$ff;
valuelist4a[11]:=$55;


if HalfShadeCGA then begin
   i:=0;
   num:=0; 
   repeat 
      Tpalette4a.colors[num]^.r:=valuelist4a[i];
      Tpalette4a.colors[num]^.g:=valuelist4a[i+1];
      Tpalette4a.colors[num]^.b:=valuelist4a[i+2];
      Tpalette4a.colors[num]^.a:=$7f;
      inc(i,3);
      inc(num); 
  until num=3;

end else begin
   i:=0;
   num:=0; 
   repeat 
      Tpalette4a.colors[num]^.r:=valuelist4a[i];
      Tpalette4a.colors[num]^.g:=valuelist4a[i+1];
      Tpalette4a.colors[num]^.b:=valuelist4a[i+2];
      Tpalette4a.colors[num]^.a:=$ff;
      inc(i,3);
      inc(num); 
  until num=3;
          CanChangePalette:=true;
  glColorTable(GL_COLOR_TABLE, GL_RGBA8, 4, GL_RGBA, GL_UNSIGNED_BYTE, Ppalette4a);

end;
end;

procedure initCGAPalette1;
var
   num,i:integer;
//remember these are dropped to half-intense if requested, so you need the "full color".
begin  

valuelist4b[00]:=$00;
valuelist4b[01]:=$00;
valuelist4b[02]:=$00;

valuelist4b[03]:=$55;
valuelist4b[04]:=$ff;
valuelist4b[05]:=$ff;

valuelist4b[06]:=$ff;
valuelist4b[07]:=$55;
valuelist4b[08]:=$ff;

valuelist4b[09]:=$ff;
valuelist4b[10]:=$ff;
valuelist4b[11]:=$ff;


if HalfShadeCGA then begin
   i:=0;
   num:=0; 
   repeat 
      Tpalette4b.colors[num]^.r:=valuelist4b[i];
      Tpalette4b.colors[num]^.g:=valuelist4b[i+1];
      Tpalette4b.colors[num]^.b:=valuelist4b[i+2];
      Tpalette4b.colors[num]^.a:=$7f;
      inc(i,3);
      inc(num); 
  until num=3;

end else begin
   i:=0;
   num:=0; 
   repeat 
      Tpalette4b.colors[num]^.r:=valuelist4b[i];
      Tpalette4b.colors[num]^.g:=valuelist4b[i+1];
      Tpalette4b.colors[num]^.b:=valuelist4b[i+2];
      Tpalette4b.colors[num]^.a:=$ff;
      inc(i,3);
      inc(num); 
  until num=3;
          CanChangePalette:=true;
  glColorTable(GL_COLOR_TABLE, GL_RGBA8, 4, GL_RGBA, GL_UNSIGNED_BYTE, Ppalette4b);

end;
end;

//Hackish- reserved for greyscale
procedure initCGAPalette2;
var
   num,i:integer;

begin

valuelist4c[00]:=$00;
valuelist4c[01]:=$00;
valuelist4c[02]:=$00;

valuelist4c[03]:=$3f;
valuelist4c[04]:=$3f;
valuelist4c[05]:=$3f;

valuelist4c[06]:=$7f;
valuelist4c[07]:=$7f;
valuelist4c[08]:=$7f;

valuelist4c[09]:=$ff;
valuelist4c[10]:=$ff;
valuelist4c[11]:=$ff;

if HalfShadeCGA then begin
   i:=0;
   num:=0; 
   repeat 
      Tpalette4c.colors[num]^.r:=valuelist4c[i];
      Tpalette4c.colors[num]^.g:=valuelist4c[i+1];
      Tpalette4c.colors[num]^.b:=valuelist4c[i+2];
      Tpalette4c.colors[num]^.a:=$7f;
      inc(i,3);
      inc(num); 
  until num=3;

end else begin
   i:=0;
   num:=0; 
   repeat 
      Tpalette4c.colors[num]^.r:=valuelist4c[i];
      Tpalette4c.colors[num]^.g:=valuelist4c[i+1];
      Tpalette4c.colors[num]^.b:=valuelist4c[i+2];
      Tpalette4c.colors[num]^.a:=$ff;
      inc(i,3);
      inc(num); 
  until num=3;
          CanChangePalette:=true;
  glColorTable(GL_COLOR_TABLE, GL_RGBA8, 4, GL_RGBA, GL_UNSIGNED_BYTE, Ppalette4c);

end;
end;

//Mode6 of original spec
procedure initCGAPalette3;
var
   num,i:integer;

begin

    valuelist4d[00]:=$00;
    valuelist4d[01]:=$00;
    valuelist4d[02]:=$00;

    valuelist4d[03]:=$ff;
    valuelist4d[04]:=$ff;
    valuelist4d[05]:=$ff;

    Tpalette4d.colors[num]^.r:=valuelist4d[i];
    Tpalette4d.colors[num]^.g:=valuelist4d[i+1];
    Tpalette4d.colors[num]^.b:=valuelist4d[i+2];
    Tpalette4d.colors[num]^.a:=$ff;

    Tpalette4d.colors[num]^.r:=valuelist4d[i];
    Tpalette4d.colors[num]^.g:=valuelist4d[i+1];
    Tpalette4d.colors[num]^.b:=valuelist4d[i+2];
    Tpalette4d.colors[num]^.a:=$ff;

    CanChangePalette:=true;
    glColorTable(GL_COLOR_TABLE, GL_RGBA8, 2, GL_RGBA, GL_UNSIGNED_BYTE, Ppalette4d);

end;


procedure initPalette64;
//there are 64 names- but theres no way of knowing if you changed the defaults-so I wont assume.
//only 16 out of those 64 are available at any time.

// I swear these are PanTone corrected with "Light GRey 55s" -but Ill go with it.
//set to initial 16- programmable- as long as MaxY=200.

{
This is the easiest way to do EGA colors:

for each of 16 colors
 
A=      0, 63, 127 and 255. 
HEXA:   0  3F  7F      FF

}


var
   num,i:integer;

begin  

valuelist64[0]:=$00;
valuelist64[1]:=$00;
valuelist64[2]:=$00;

valuelist64[3]:=$AA;
valuelist64[4]:=$00;
valuelist64[5]:=$00;

valuelist64[6]:=$00;
valuelist64[7]:=$AA;
valuelist64[8]:=$00;

valuelist64[9]:=$00;
valuelist64[10]:=$AA;
valuelist64[11]:=$AA;

valuelist64[12]:=$AA;
valuelist64[13]:=$00;
valuelist64[14]:=$00;

valuelist64[15]:=$AA;
valuelist64[16]:=$00;
valuelist64[17]:=$AA;

//brown- dark yellow
valuelist64[18]:=$AA;
valuelist64[19]:=$55;
valuelist64[20]:=$00;

valuelist64[21]:=$AA;
valuelist64[22]:=$AA;
valuelist64[23]:=$AA;

valuelist64[24]:=$55;
valuelist64[25]:=$55;
valuelist64[26]:=$55;

valuelist64[27]:=$55;
valuelist64[28]:=$55;
valuelist64[29]:=$ff;

valuelist64[30]:=$55;
valuelist64[31]:=$ff;
valuelist64[32]:=$55;

valuelist64[33]:=$55;
valuelist64[34]:=$ff;
valuelist64[35]:=$ff;

valuelist64[36]:=$ff;
valuelist64[37]:=$55;
valuelist64[38]:=$55;

valuelist64[39]:=$ff;
valuelist64[40]:=$55;
valuelist64[41]:=$ff;

//14=Y
valuelist64[42]:=$ff;
valuelist64[43]:=$ff;
valuelist64[44]:=$55;

valuelist64[45]:=$ff;
valuelist64[46]:=$ff;
valuelist64[47]:=$ff;


   i:=0;
   num:=0; 
   repeat 
      Tpalette64.colors[num]^.r:=valuelist64[i];
      Tpalette64.colors[num]^.g:=valuelist64[i+1];
      Tpalette64.colors[num]^.b:=valuelist64[i+2];
      Tpalette64.colors[num]^.a:=$7f;
      inc(i,3);
      inc(num); 
  until num=7;

   i:=25;
   num:=8; 
   repeat 
      Tpalette64.colors[num]^.r:=valuelist64[i];
      Tpalette64.colors[num]^.g:=valuelist64[i+1];
      Tpalette64.colors[num]^.b:=valuelist64[i+2];
      Tpalette64.colors[num]^.a:=$ff;
      inc(i,3);
      inc(num); 
  until num=15;
      if MaxY=200 then //HEY! I didnt write the spec...
          CanChangePalette:=true
      else
          CanChangePalette:=false;
  glColorTable(GL_COLOR_TABLE, GL_RGBA8, 16, GL_RGBA, GL_UNSIGNED_BYTE, Ppalette64);
end;

8bit-(256 color) is paletted RGB mode
	-this is the last paletted mode


---
4bit(16)->8bit(256):

NO CHANGE, just switch palettes.

EGA modes:
	just use the first 16 colors of the palette and change them as you need.
	its easier to not do 4bit conversion, but use 8bit color and convert that.

	- is it possible the original 16 colors can point elsewhere- yes, 
	but that would de-standardize "upscaling the colors" from 16-> 256 modes.

Downsizing bits:

-you cant put whats not there- you can only "dither down" or "fake it" by using "odd patterns".
what this is -is tricking your eyes with "almost similar pixel data".

//reads one pixel- and only one pixel
function GetPixelElse(x,y:word):SDL_Color;
var
    someColor:PSDL_Color;

begin
    case (bpp) of
//need to read 24 or 32 bpp and convert these-much like 15 and 16bpp modes (sorry)

//        2: glReadPixels(x, y, 1,1, GL_COLOR_INDEX, GL_UNSIGNED_BYTE, someColor);
//        4: glReadPixels(x, y, 1,1, GL_COLOR_INDEX, GL_UNSIGNED_BYTE, someColor);
//        8: glReadPixels(x, y, 1,1, GL_COLOR_INDEX, GL_UNSIGNED_BYTE, someColor);

        24: glReadPixels(x, y, 1,1, GL_RGB, GL_UNSIGNED_BYTE, someColor);
        32: glReadPixels(x, y, 1,1, GL_RGBA, GL_UNSIGNED_BYTE, someColor);
    end;
end;


function GetPixel1516(x,y:word):PSDL_Color;
var
    someWord:word;
    someColor:PSDL_Color;
    shit:data;
begin
//gawd- you C and GL guys...are getting on my nerves...

    case (bpp) of

        15: begin
            //we get a word
            glReadPixels(x, y, 1,1, GL_RGB, GL_UNSIGNED_SHORT_5_5_5_1, shit);
            someWord:=shit^;
            //now give me a converted SDL_Color
            someColor:=Word15ToSDLColor(someword);
            someColor^.a:=$ff; //ignored
        end;
        16: begin 
            glReadPixels(x, y, 1,1, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, shit);
            someWord:=shit^;
            someColor:=Word16ToSDLColor(someword);
        end;
    end;
    GetPixel1516:=someColor;
end;


//necessary wrapper- output is "inconsistently fucked"
function GetPixel(x,y:word):PSDL_Color;
var
    someColor:PSDL_Color;
begin
    case (bpp) of
        2,4,8,24,32:GetPixelElse(x,y);
        15,16:GetPixel1516(x,y);
    end;
    GetPixel:=someColor;
end;


//similar to SDL_GetPixels- we read a Rect from a Texture.
function readpixels1516Tex(x,y,width,height:integer; Texture:LongWord):PmyShortPixels;

begin
  case (bpp) of

//these are 16bit values(words, not DWords)
//so unpack, then repack it.

	15: begin
  			glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
            glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_SHORT_5_5_5_1, wordpixels);
  			glPixelStorei(GL_PACK_ALIGNMENT, 4);
    end;
	16: begin
  			glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
            glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, wordpixels);
  			glPixelStorei(GL_PACK_ALIGNMENT, 4);
    end;

    else begin
        //wrong routine called!!
    end;
  end;
    readpixels1516Tex:=wordpixels;
end;

//(24bit RGB->15bits hex):
function RGB24To15bit(incolor:SDL_Color):SDL_Color;
var
    outcolor:SDL_Color;
begin
        outcolor.r := incolor.r * (2 shl 5) mod (2 shl 8);
        outcolor.g := incolor.g * (2 shl 5) mod (2 shl 8);
        outcolor.b := incolor.b * (2 shl 5) mod (2 shl 8);
end;

function RGB24To16bit(incolor:SDL_Color):SDL_Color;
var
    outcolor:SDL_Color;

begin
        outcolor.r := incolor.r * (2 shl 5) mod (2 shl 8);
        outcolor.g := incolor.g * (2 shl 6) mod (2 shl 8);
        outcolor.b := incolor.b * (2 shl 5) mod (2 shl 8);
end;

//this uses pixels, mod me for lines,arcs
//hackish but it works with GL

procedure DrawPoly(GiveMePoints:Points);	
//use predefind objects or tessellate if you have need of more surface "curvature"
var
    num:integer;

begin	
    num:=0;
	if FilledPolys then	

		//fill when done
	else
		//dont fill when done
		
		repeat
		//line (previous(x,y) to next (x,y))
		
//			glVertex2s(GiveMePoints[num].x,GiveMePoints[num].y);	
			inc(num);
		until num=sizeof(GiveMePoints);

	SDL_Flip;
end;


{
if your wondering why pixel ops are slow- this is why. 

Each Pixel (in a Line) in the past were checked to see if we needed to invert them.

This is unwise- the routines work(well)- there is no need to slow them down.
If anything- SPEED THEM UP!

If you need to invert a pixmap(sub bitmap/sub texture) then do that instead. 
Affect only the texture in question, its FASTER.

So the whole arguement as to how things are put on screen(xor,not,and,or) is moot.
-Its an age-old arguement, too.

some of this came to be from outdated linestyle code:
        linestyles wanted instructions on the HOW to draw lines AND the MEANS and the THICKNESS.

The means is a stippled bitmap(blit)- which will remain- and "the thickness" can remain.

But Im not modifying(or checking to modify) -each pixel-in flight(bounds or otherwise).


}

//this is used ONLY for LINES- Ive discovered, in a very hackish way.
procedure SetWriteMode(WriteMode : word);
begin
    if IsConsoleInvoked then
        LogLn('SetWriteMode: So you want to be a sloth? Go to a Zoo. Not changing Pixel data mid-flight.');     
    {$ifdef lcl}
        ShowMessage('NO- you will NOT- modify per-pixel data mid-flight.');
    {$endif}
    exit;
end;

//paletted
procedure setFGColor(color:byte);
var
	colorToSet:PSDL_Color;
    r,g,b:byte;
    

begin
   if MaxColors=2 then begin
        colorToSet:=Tpalette4d.colors[color];
        glColor3b(colorToSet^.r,colorToSet^.g,colorToSet^.b ); 
   end; 
{
//these 3 have to check mode requested
   if ((MaxColors=4) and (Mode=CGA)) then begin
        colorToSet:=Tpalette4a.colors[color];
        glColor3b(colorToSet^.r,colorToSet^.g,colorToSet^.b ); 
   end if ((MaxColors=4) and (Mode=CGA1)) then begin
        colorToSet:=Tpalette4b.colors[color];
        glColor3b(colorToSet^.r,colorToSet^.g,colorToSet^.b ); 
   end if ((MaxColors=4) and (Mode=CGA2)) then begin
        colorToSet:=Tpalette4c.colors[color];
        glColor3b(colorToSet^.r,colorToSet^.g,colorToSet^.b ); 
   end;
}
   if MaxColors=256 then begin
        colorToSet:=Tpalette256.colors[color];
        glColor3b(colorToSet^.r,colorToSet^.g,colorToSet^.b ); 
   end else if MaxColors=64 then begin
		colorToSet:=Tpalette64.colors[color];
        glColor3b(colorToSet^.r,colorToSet^.g,colorToSet^.b ); 
   end;
end;

//clear with color
procedure clearDevice(index:byte); overload;
//we deal with RGB data - borrow some floats along the way
//this specific RGB data is in a LUT(look up table-its not given by the user)
var
	r,g,b:byte;
    somecolor:PSDL_Color;
begin
    if MaxColors>256 then begin
        if IsConsoleInvoked then
           Logln('ERROR: i cant do that. not indexed.');
		 {$ifdef lcl}
			ShowMessage('Attempted to Clearscreen(index) with non-indexed data.');
		 {$endif}

        LogLn('Attempting to clearscreen(index) with non-indexed data.');
        exit;
    end;

    if MaxColors=16 then
        somecolor:=TPalette64.colors[index];

    if MaxColors=64 then
        somecolor:=Tpalette64.colors[index];

    if MaxColors=256 then
        somecolor:=Tpalette256.colors[index];

    glClearColor(ByteToFloat(somecolor^.r), ByteToFloat(somecolor^.g), ByteToFloat(somecolor^.b), 1.0);
    if Render3d then
        	glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    else	 
        glClear(GL_COLOR_BUFFER_BIT);
    //the color changed- so reset _bgcolor internal value
    _bgcolor.r:=r;
    _bgcolor.g:=g;
    _bgcolor.b:=b;
    _bgcolor.a:=$ff;
end;

function Word16_from_RGB(red,green, blue:byte):Word; //Word=GLShort
//565 16bit color
begin
  red:= red shr 3;
  green:= green shr 2;
  blue:=  blue shr 3;
  alpha:=$ff; //ignored
  Word16_from_RGB:= (red shl 11) or (green shl 5) or blue;
end;

function Word15_from_RGB(red,green, blue:byte):Word; //Word=GLShort
//5551 16bit color
begin
  red:= red shr 3;
  green:= green shr 3;
  blue:=  blue shr 3;
  alpha:=$ff; //dropped and ignored
  Word16_from_RGB:= (red shl 10) or (green shl 4) or blue;
end;


function RGB4FromLongWord(Someword:Longword):SDL_Color;

var
    LoColor,color:SDL_Color;
    index:byte;
    x:integer;

begin

    if MaxColors =4 then begin
        x:=0;
        repeat
{
//which?? theres four of them...
            if  ((RGBToHex32(Tpalette4^.Colors[x])=SomeWord) then begin //we found a match
                 RGB4FromLongWord:= palette4^.colors[x];
                 exit;
            end;
} 
           inc(x);
        until x=4;
    end else begin //data invalid
             color.r := $00;
             color.g:= $00;
             color.b:= $00;
             color.a:= $00;
            RGB4FromLongWord:= color;
            exit;
    end;
    
    if MaxColors =16 then begin
        //use the DWord to check the limited pallette for a match. Do not shift splitRGB values.
        x:=0;
        repeat
            if  ( (RGBToHex32(Tpalette64.Colors[x]^)=SomeWord)) then begin //we found a match
                 RGB4FromLongWord:= color;
                 exit;
            end;
            inc(x);
        until x=16;
    end else begin //data invalid
             color.r := $00;
             color.g:= $00;
             color.b:= $00;
             color.a:= $00;
            RGB4FromLongWord:= color;
            exit;
    end;


end;

function RGB8FromLongWord(Someword:Longword):SDL_Color;
var
    color:SDL_Color;
    x:integer;

begin

{$ifndef mswindows}

//get the DWord
    color.r:= (someword shr 24) mod 255;
    color.g:= (((someword shr 16) and $ff) mod 255);
    color.b:= ((someword shr 8 and $ff) mod 255);
    color.a:= $ff;
{$endif}


{$ifdef mswindows}
//windows stores it backwards for the GPU
//get the DWord
    color.b:= (someword shr 16) mod 255;
    color.g:= (((someword shr 8) and $ff) mod 255);
    color.r:= ((someword and $ff) mod 255);
    color.a:=$ff;

{$endif}    

    
//CGA,EGA is 4bit, not 8. That was a SDL hack.

    if MaxColors=256 then begin
    //to save time - and hell on me- let use the RGB pair from the DWord and compare it to whats in the palette, unshifted.
        x:=0;
        repeat
            if  (Tpalette256.colors[x]^.r=color.r) and (Tpalette256.colors[x]^.g=color.g) and (Tpalette256.colors[x]^.b=color.b) then begin
                RGB8FromLongWord:= color;
                exit;
            end;
            inc(x);
        until x=256;
    end else begin //data invalid
             color.r := $00;
             color.g:= $00;
             color.b:= $00;
             color.a:= $00;
            RGB8FromLongWord:= color;
    end;
end;

//if you get hell- you shouldnt- take off the 255 limiter

function RGB5551FromLongWord(Someword:Longword):SDL_Color;
var
    HiWord:Word;
    color:SDL_Color;
begin

{$ifndef mswindows}

//get the DWord
    color.r:= (someword shr 16) mod 255;
    color.g:= (((someword shr 8) and $ff) mod 255);
    color.b:= ((someword and $ff) mod 255);
    color.a:=$ff;
{$endif}


{$ifdef mswindows}
//windows stores it backwards for the GPU
//get the DWord
    color.b:= (someword shr 16) mod 255;
    color.g:= (((someword shr 8) and $ff) mod 255);
    color.r:= ((someword and $ff) mod 255);
    color.a:=$ff;

{$endif}

//333 mod

    color.r:= color.r shr 3;
    color.g:= color.g shr 3;
    color.b:= color.b shr 3;

    RGB5551FromLongWord:=color;
end;

function RGB565FromLongWord(Someword:Longword):SDL_Color;
var
    HiWord:Word;
    color:SDL_Color;
begin
{$ifndef mswindows}

//get the DWord
    color.r:= (someword shr 16) mod 255;
    color.g:= (((someword shr 8) and $ff) mod 255);
    color.b:= ((someword and $ff) mod 255);
    color.a:=$ff;
{$endif}


{$ifdef mswindows}
//windows stores it backwards for the GPU
//get the DWord
    color.b:= (someword shr 16) mod 255;
    color.g:= (((someword shr 8) and $ff) mod 255);
    color.r:= ((someword and $ff) mod 255);
    color.a:=$ff;

{$endif}
//323 mod
    color.r:= color.r shr 3;
    color.g:= color.g shr 2;
    color.b:= color.b shr 3;

    RGB565FromLongWord:=color;
end;


//hex value, not palette number
function EGAtoVGA(color: LongWord): LongWord;
begin
  EGAtoVGA := color shl 2;
end;

//test if these modes are set
function Double320x200: Boolean;
begin
  Double320x200 := ((MaxX=320) and (MaxY=200) and (DoubleSize=true));
end;

function Double320x240: Boolean;
begin
  Double320x240 := ((MaxX=320) and (MaxY=240) and (DoubleSize=true));
end;

//test if these modes are set
function Double320x200: Boolean;
begin
  Double320x200 := ((MaxX=320) and (MaxY=200) and (DoubleSize=true));
end;

function Double320x240: Boolean;
begin
  Double320x240 := ((MaxX=320) and (MaxY=240) and (DoubleSize=true));
end;

//TURTLE GRAPHICS:

//penUp, moveTo,PenDown....

Procedure MoveTo(X,Y: Word);
//precursor to functions that dont explicity use XY. 
//Rather they assume XY is set before an operation.

Begin
  //set pixel location in 1D array (XY)
end;
