### What is this?

This is effectively a Lazarus/FreePascal "Graphics interface"(its not TCanvas, nor BGI). Uses SDL(for lower level access).)

It allows one to "draw" in 2 dimensions,**despite being already in  graphics modes**
Im not replicating Borlands code, but I have a similar API.

-Hence the libGraph C references. (This is FAAR better.)

Ivve removed retro computing code. i8086 can use Borland, not even sure if FPC will fit on a 32mb HDD--assuming its lying around, unused these days.
(You got sum balls writing code for that platform. Its pre-internet.)

I am targeting "consumer gaming devices and home PCs" as well as "internet era (and beyond)" machines.
There are other compilers(Rascal, Turbo, Borland) for those older machines- and the assembler routines do them well, for what they do. The code cant change due to hardware limitations.

I need a 32bit LFB - and addressing - to get anything going correctly- as uch I refuse to solely target 16bit hardware. AMIGA is the exception.
AMIGA used 32bit (software) instructions, knowing full well that the hardware was in fact, 16bit.

Apple went the other way, 128bit fetches on "bubble" g3s (and other machines) instead of 32bit ones (quasi-mmx acceleration if you "flipped the cpu instructions around").


#### How far along are we?

Individual low level interfaces have become unnecessary.

libPTC ,libuos, etc- should provde the necessary support that we need to accomplish the mission of the "equivalent BGI interface".
What Im doing is updating the interface/API and porting it to unices,etc.

(THE BGI does exist on unices, its in X11CorePrim API- libPTC exploits this-but the code is ancient. There isnt an FPC implementation, however. At lest not a "KNOWN one".)

SDLv2 may prove a moot point as compat libs are out now (to update code on the fly) as is with pre and DX9 apps, thru vulkan APIs.

I am still working some routines(missing in SDLv1?) and some C code portage.
I am not happy with the LCL devs- and the shitty writing of T(window)Handle. (OR TCanvas.)

ugh. The JEDI code is a mess...I will need a minute.

### crossTools and compiling

supported targets:

lin64- you should be using this
win64- the "much loved" windows (8+)
lin32- better known as i686 linux .. The standard was RedHat v5..yes, hello 1990s. (This is the oldest Pentium based CPU Im supporting. this was win98 era)
win32- is better known as pre-2010 windows,(likely win7 and below.)
Macintosh/PowerPC -hardware doesnt work like you think it does.
Amiga- the proprietary "constantly hacked and modded" expensive as hell consumer PC--before there was one. "The standard setter."


There are X11 dev files here(libPTC?). SDL doesnt use them. They can be moved.

X11CorePrim devel(by itself, including demos) DOES need these files.
Will this compile w Embercardero Delphi? Possibly, with license restrctions(unportable code, not multiplatform). I dont write solely on win(32).

Ignore my comments about "X11" --if using windows.
**SEE BELOW AS TO WHY** (baseunix, my ass)

XMing connects to XServer on Windows. Does it work with (Win10 and ) WSL? IDK. Should work with MSYS/CYGWIN.
By default you get libCACA(libSHIT, ascii crapart) and no X11 support, even installing SDL doesnt fix this. Im looking into this.
(I aim to use Native Win API calls thru SDL here.)

Porting for Unix- via windows-(with exception of MSYS/CYGWIN?? is OUT.) Porting the other way works, and wine is a testbed.

### DOS and modes

-DOS never had the (video) mode set (we always hopped into it)
Borland code is x86 specific, 16bit DOS int 10 calls, mostly.

Later on, (386) 486+ systems added in 32bit API thuking (DPMI/HXDPMI) with 32bit addressing
(usually inline to the binary file and setting/releasing graphics modes).

ModeX(VESA SVGA++) is a very common(hack) implementation.

Windows/Mac/Amiga and unices(typically) always set graphics modes now.
Gone are the days of an actual Terminal, and a commandline-only interface. This presents a few challenges.

#### DODGY SDL

SDL is incomplete(and dodgy).

We dont "change the mode", we "adjust the output window" to compensate for what we need. FS is a taad different.
USUALLY, TO use lower modes you need to (on HW within the past 12 years or so) launch "ANOTHER X11 instance", or launch one with "special parameters".
I will get into how to do that in a bit, its not necessary.

### no Tcanvas?

I dont want to hear the bickering. <br>
Could it be done, yes- nobodys done it right, yet. SDL2 cheats, and uses a "GL context"(at least on X11)
SDLv2 uses GL. I think the other dev is "hacking GL" to his "Lazarus advantage" here.

        1- This is Lazarus only implementation.
		OSes not supporting Lazarus( or not running X11) wont have the routines. (non-portable)

        2- TCanvas code is a mess. Its full of class instantiation and OOP.

        3- Class(instantiated) structures need to be rewritten per OS (at LCL level at least- an FPC bug)
                This requires knowledge of X11,OSX, and Win32 DirectX (5-7) APIs at a minimum
                -Use of SDL greatly simplifies this.


Apparently the BareGame library(GitHub) started developement about the same time as me.
https://github.com/sysrpl/Bare.Game

Im in no way stealing anything here- I just have another working example. My code doesnt require Lazarus/LCL.
The BareGame code is limited to SDL2, not SDL1- where I started. This limits compile targets/architectures.

I **do** think force limiting to SDL2 is wrong- you have to limit your output accordingly on older systems.
**However, most tasks can still be accomplished.**

Ive yet to thouroughly test these routines- output may yet be limited.
TPalettes are my invention, and fix for several iterations of nonexistent code.

I still believe we shouldnt be using C-SDL for this- that OS APIs are faster(of course less depends).
However, IT WORKS FOR NOW.

-If you can deal with these limitations, try the bare.game library seperately.

The push these days is to override SDLv1 hooks and force SDLv2 via compatibility layer/API. This also improves rendering.

#### SDL Mac Dev Notes

SDL 1.2.13 is the highest supported on OS9.
SDL 1.2.15 is available for OSX 10.4.x capable PowerPC systems(imac bubble,iBooks).
	-I believe I know why this happened.

SDL v 1.3 = 1.2.15+ ..and releases after that took a jump to SDLv2.0.

Im seeng  JEDI code in flux with GL routines...and SDLv2 32bit addressing/ Alpha buts in use. This is forbidden in SDLv1.2. VRAM/VGPU limits, addressing, etc. The blit code is also bugged.

This said:

	I do seriously believe that this platform(PowerPC BE 32bit) is much better served from Debian 7(g3) and Ubuntu 16(g4).
	QEMU can emulate this(ppc mac99 mode) just fine.  Update your sources and rebuild qemu, its worth it. QEMU will bot where G4 will not with "Decrementer exceptions".

	Im not sure(as QEMU uses emulated GFX card, and ATI-ROM mod hangs somewhat) if the real GFX HW is supported. There is a ROM image, I suggest using it.
	Drive hackery with /dev/cdrom and /dev/sr1 may be needed with an external drive to get the system in a "supported state" (on older hardware).

    Do not be blown back by the dated appearance(OS age)- these systems are more than capable.
	The main package tree hasnt changed all that much(mostly SSL and libC system updates, kernel removal of old gfx hw,X11 updates).

	Jekyll will be broken in Debian 8, newer OS Releases fix the issue(libJS improperly included, and vulnerable, unpatchable). This includes ubuntu16.

#### Linux

	Linux is Linux.

    I dont care if its on a Mac or not. (Double check my Endianness code, please? ARGB vs RGBA)
	Debian provides a VERY SANE DEV PLATFORM(PPC included)

**PowerPC systems are NOT vulnerable to Spectre and Meltdown.**

Every Mac computer since late 2006 (G5) )has been 64 bit capable. (Snow Leopard on up-previous were 32bit Powerpc)
This includes x86_64 intel mac.
I dont code for M1- its not supported yet by fpc. (Nor do I have one).


Get used to tangents, cosin, and sin usage(arctan,tan,etc)
(curves,plots, lines, polys, tris- yes, tris..your video card loves TRIs...)

MessageBox:

With Working messageBox(es) we dont need the console.(REPLACES: writeln)
We can address this in one of two ways:

1-

	Request input via graphics mode(after drawing text)
	Display data via graphics mode(with or without additional multimedia graphics)
	caveat-
		you need TVision or GVision APIs for this

2-

	Ping the user with another app Dialog (older systems cant use this)

	SDL throws a dialog in another window, above your application.(needs Desktop)
	Tvision and GVision should do it INSIDE the application. (no desktop assumed)

Theres a huge difference between these two.

We need to hack(case #2) the crt unit to FAIL to use writeln when graphics mode is set.
(The first hack is to cull writeln/readln(or at least redirect IO).
You should be using "input driven API". I should smack such disobedience "with the way of doing things correctly".


Logging:
------

We have logging.

we can fetch the SDL/LCL error(and possibly trace it)...why do we care about
ancient ShortInt C-ish responses? (DELETE THOSE BULLSHIT! F-me, SDL shouldnt even be using them!)

This was a quirk in debugging with older cpus, where all functions returned a status code.
Its also very common in C and Assembler- but not needed. You can pass error codes, trip them, even spew debugging code around(strings)....

SDL  or LCL?? Dialog boxes(ugly) will still work, even with bugged subroutines. (SDL is barebones, It looks like X11CorePrim code. Hello 1990s!)


**LOG EVERYTHING**  <br>
If in doubt, LOG IT.


SDL minor failures are common...they can lead to bigger crashes.
Rule of thumb:

		output it to stdout (console) if we can, else write it out to a file.
		Always log to a file.


Image Support:
-------

FreeImage:

FreeImage needs to replace SDL_image routines. SDL_Image CAN be used, I would prefer FreeImage.<br>
This will require it to be installed prior to compile time, however.
("IMG_xxxx()_ code" is SDL_image, not FreeImage)<br>


Seatbelts are optional:
--------

Range Checks are not quite where they need to be in my code. Im aware of this.<br>

This is a side-effect of implementing new things, you forget sometimes- you are overburndened
with a million other floating concepts and a billion lines of "misshappen code".

**Very limited routines should be called if Graphics modes are not set, or coming down**

I explicitly am targeting D2D(DX), X11 (Prims), and QDraw2D/Quartz2D targets thru SDL.<br>
Cairo Code will have to be rewritten (alongside the Pascal headers made by another dev!)

misc:
-------

DONT PRESUME ANYTHING.EVER.

Apparently I have to reCreate WinDos unit for Windows???<br>
NO, IT DOES NOT EXIST.

Dos:

		A TRUE DOS MODE (Int21 based) FILE IO Routine Library
		This includes ARGV and GetEnv and PutEnv processing.

WinDos:

		A fork of WinCrt(GUI terminal or xterm) that allows for FileIO(int 21 calls).
		Mimcks the DOS UNIT in post WinME era.

		This includes ARGV and GetEnv and PutEnv processing.


FPC BUG:

**DOS UNIT CAN NOT EVER USE BASEUNIX routines(ITS NOT UNIX BASED)!**

WHO GOT THE DUMBASS IDEA TO USE the BASEUNIX unit in the DOS,WINDOS units for FPC RTL?
	dumbass fpc devs, thats who.

50 thousand units, one for each OS--even if some are unnecessary, unused, and NOT NEEDED.
**WRONG WAY TO IMPLEMENT THINGS**


BaseUnix(UNICES ONLY, int 80 kernel calls):

INT80 is a "programmed OS" hook, much like INT10, INT 21 were for DOS.
**THIS SAID** :

Kernel only access thru Ring0, as a user(even root), although you get access to the OS, and Filesystem, and RAW Hardware-
		You still CANT ACCESS the RAW hardware.
		You have to request access, wait, then check replies(similar to dos). Still , ths can only be done AS ROOT. BAD IDEA. You want MultiUser(and X11) access.

So, there is a way to (Xterm, ncurses, console) access these OS APIs. FPC tries to use them.
Once runlevel 5 (X11) is set, things change again. The crt unit and keyboard unit now have to "compete for resources", and you have event-based input(IO) and
have to do some weird hacking just to get a "drawable" in a "window". Let alone Fullscreen.

OH? YOU WANTED TO RULE THE WORLD? TOUGH. THE OS IS IN THE WAY!

**As Ive said, BASEUNIX, UNIX,Linux APIs CANNOT EVER be used on windows.**
(They would wreek havok, if anything ever worked..)

This needs to be reimplemented as ... wait for it...... WinDos and WinKeyb and WinCRT. (pre WinME can use Dos,keyb,crt)
sysutils? NOPE.


uses
	BaseUnix <-- but we are on WINDOWS! There is "no Unix", and it doesnt work that way with WSL.


FPC devs need to FIX -FPC-, BADLY!

xTerms were made to connect and mimck the old "VT100 hardware interfaces" used decades previously.<br>
An xTerm is like a DOS CRT environment with "different OS hooks". <br>

You are working with an actual "shell" -and the kernel itself, usually, underneath an xTerm. <br>
A VTerm is like traveling to/fro DOS in 32bit DPMI mode.Only theres no "thunking", its "always on".<br>


Its like staring at an ancient dinsaur. You have a screen and keyboard (the node is elsewhere)...yes... the node...is .... elsewhere....<br>
(...like in the server room down the hall.....)

VTERM7 is usually running X11. However, theres no standards.<br>
On some Linux-es its on VTERM1.<br>
YOU CAN "BOMB OUT" to another terminal, kill X11, and go on with your day.
(try that with windows- you cant, theres no underlying anything.)


Windows consoles were implemented differently.

		ITS ALL FAKE (after WinME)

Then came PowerShell, a Unixy super version of the "Command Prompt".

		AGAIN, ITS ALL FAKE!!

You have the windows Task Manager when things go south- (if it co-operates).


**This is why consoleIO doesnt work(no UI) in a "UI app" in windows(post Win ME)**
(calling from a command prompt window invokes a 32bit win32 API(shell32?), there **STILL** cannot be writeln() support this way- it has to be compiled as a "console application")

Ever gotten the win32 (DPMI missing) error (on DOS):
	"This application/executable cannot be run in DOS mode"

- This is where that comes from.

#### compiling

-But does it compile, AH!

Ive stopped at WinNT era on purpose.
However, you may wish to build for win9x/ME/FreeDOS environments.

The code should build - for releases.
It may not if its in WIP status.

For older systems like 8088/8086/186/286/386- this will have be gutted and rebuilt.

		The borland units work for these setups- however, other functions are added and units need to be overlayed (vm bank swapped) ,etc ,etc.
		Anything over 16 color EGA and 640x480(maybe smaller) will have to be removed, including "color conversion codes".
		Your palettes are in hardware- you dont need a LUT at all, or any related code.
		Your routines are likely in assembler(for speed).
		You will lose a ton of graphics(JPEG,gif,etc) and image routines/functions due to "hardware limits". Things may look ugly.

For everyone else-

		Use the makefile, or the batchFile.


### ARG! My video output (window) is corrupted!

Cause:

	You probably re-wrote the window event handler. Your window got "overwrote" by another graphical process(it happens)

To fix this:

	Tap the Window_Expose event and "PageFlip on exposure". (Easier than it sounds to fix)

Id advise also pausing your application/game once "Focus is left"- why process anything in the background if its not needed?
(Keep your timers active, just dont draw in the background, perhaps pause audio as well)


#### Rendering

Remember, render one frame- then bail. Come back for more.<br>
(you have become a vicious video hound, like a horny teenager--but you cannot have it all at once)

A video callback is nothing more than a timer(interrupt) launching a procedure every so often.

**DO NOT RENDER IN A LOOP(render thru a videoCallback function instead)-
this is a COMMON MISTAKE--
	it blocks input processing and can lead to CPU lockups**

-Further, SDL2 intentionally blocks ALL rendering threads (while in use) under X11. THIS IS WRONG.

#### JEDI code?

JEDI Headers are severely lacking and are broken in places they should not be,
(I am using modified ALTERNATE Pascal JEDI headers -that Ive personally patched).

-This includes longInt/DWord length patches for 64bit systems.

#### Network

Network API has yet to be tested. Im thinking Synapse here- I need some demos.
SDL_Net needs the header file section copied into the file. I hadnt done that yet.

### OpenGL

OpenGL appears to otherwise not be affected by Lazarus in the same manner as SDL is,
as long as you remember that the Context is inside a window.(you need GLUT or something to switch to Fullscreen)<br>

Therefore- "something must handle the window". The LCL usually does this for us.<br>
(Both components are required.)

#### The Context(GC)

Looking thru hundreds of lines of code on this can be a taaad confusing.

Graphics must be created in this fashion:

<pre>
Window Handler	-Tells OS of intent to create a window
	Window 		-for Window Handler API (above) to control
		Graphic Context(The Canvas) -you cant draw here, intent of "area" to draw with (bpp definitions,etc)
			MainSurface/Textures -you draw here
</pre>

Closing is in the oppsoite direction. You release control in reverse order. <br>
(Its very much like a magick or wiccan ritual.)

### Cant I just USE SDL2_BGI and hook the C?

SDLBGI: SDL2 (in C) can be used-
but "as-is" needs a lot of palette and other hacking.<br>

The code is "very dodgy" in places.<br>
If you need something "in a pinch" that "fully" supports "most" BGI functions, use it.

Otherwise, use my routines. They are more "feature complete".


### Can I use Other SDL code with LazGfx?

YES. Until I done porting- PLEASE DO SO.

In otherwords:

	Do I have to limit myself to the BGI Core?
	NO.

Yes, you can mix the routines-

		as long as a "Graphics Rendering Context" is open(call Initgraph first).
		DO NOT MIX OpenGL and SDL.

There can be a few issues- most SDL functions are not checked for sanity and need to be wrapped, but if you "feel the need"
I will not stop you. (SDL doesnt care, either)

What will happen if you do is a "clustered mess". Text functions(including ttf) will not work...
other things may be broken.


### Can SDL use Lazarus?

Passing the window handler (and mroeso the GC) will fix the Lazarus<-->SDL link bug, allowing Lazarus apps to use SDL.<br>
(I dont know how to do this, its not working)
Keep in mind that you need to hook-override the input API routine as well.
(much like an exit handler, but code may need to be modded on both ends)


### SDL? You can do better.

I am. Shut up.
Look at the X11CorePrimitives code Im writing.
Other devs are witnessing the Pango/cairo/GTK glitches- just as I have already discovered.
Theres only one way out of this mess(even from python)-

	DONT USE THESE LIBRARIES.

For now- SDL "accomplishes the mission".<br>
It is a starting point for me before I start rewriting a dozen other units on five platforms (in 15 languages).

SDL code is not generally of very much use by itself.<br>
People just dont understand how to use it- or its shortcomings(theres a lot).
SDL does in fact try to override/overwrite several functions/proceedures that are already in place -and which work.

This causes confusion between C/Pascal linking/compiling (and in programmers head).

If you think you can do better- stop talking smack, and start writing code. <br>
(CLEAN LOGICAL CODE, not that VC and CPP OOP garbage.)

I am working on ditching SDL. Little by little.


#### The SDL_BGI C Code is bugged

YES, I know. Its SAD. IM NOT FIXING IT! (and its not my code)


### My GL crashes!

**GL will crash if your color data is not aligned properly**.
AKA: 24bpp or higher (my units use a LUT for less than this).


Code is intentionally written to support double buffer (offscreen) rendering methods.<br>
The VSYNC hint (and VSYNC timed drawing) may have to be set on some OSes.

Keep in mind, Batch operations in VideoBuffer(VESA: LFB) "Surfaces" are faster than recursive calls to Put/GetPixel-<br>
(or the Renderer equivalents).

**This is where SDL(and your code) is bugged and slows down**.

There is also a SSE glitch on x86(i386) builds with FPC. It has to do with code alignment.<br>

Either write code for SSE on x64 processors -or use MMX instead.<br>
This bug is hard to work around (FPC/GCC core) but modern processors with SSE2+ are probably x64 based anyways.

- This is why Im defaulting to SDL2 now. Its been 2012+ since the x64 switch??? Youve had time- and likely upgraded.
If you havent...(shame on you),  you are stuck with SDLv1.



## OSes Supported

### Unices

		:-)

This should work on most unices/Linux-es that fpc (or Lazarus) supports.<br>
The only requirement is SDL 1 or SDL 2 packages be installed. Includes depends like SDL_Image...
PLEASE INSTALL BOTH v1 and v2. Thank me later.

If SDL doesnt support your box, I cant either.
-Dont forget to install the dev libraries/units if building source code.

Basically, if its TUX- it should build(assuming I havent broken the code)-and work.<br>
I dont have BSD, I dont USE BSD(open or free)--please dont ask about it. If YOU DO- you are on your own.

Note:

    SDL has issue with libsvga on BSD - for some reason.

Cairo support and/or XRender/XRander support may be coming soon.

Do note that cairo, GL,GLUT, and likely GTK/QT- do not require x11 pre se.
Even SDL assumes that -IT- needs X11.

Most developers assume these libraries do need X11.
This is incorrect(likely why they reinvent the input/window apis).
-Most of these actually can hook a framebuffer/TrueConsole/VTerm.

Cairo goes so far to say that using a 6x6x6(212 color block) palette-
it will probe to see / correct for- deficiencies in the attached terminal.

This is worth more investigating. Its a heap of code, headers (and some painkillers are needed), and testing.
My code uses PURE x11 Primitive hooks- some routines may be easier to use(are they more feture complete??).


#### MacShell/OSX (darwin unix)

NO, I dont give a damn about backwards POSIX, AIUX, SOUIX (OS9 terminal hack) compatibility.<br>
Either it works, or it doesnt. (Its a confusing mess to implement.)


### Windows

For any decent amount of work-
**WINDOWS 2K is the MINIMUM SUPPORTED WINDOWS OS**

Windows 2K can use DirectX9c -(you have to hunt down an installer), XP ships with DX7+<br>

Win2K doesnt suffer from "activation bugs"-that service didnt exist yet.<br>
So- if you are having issues w emulating XP(and keeping it active)--try Win2K (Pro/Workstation) instead.
Be sure and get the "128Bit security patch", not just Service Packs 4 and 5.

NTFS "has file loss", be aware of this in Win2K- Win10. The bug has been patched(again) in win10.<br>

libPTC/PTCPas will support below- thru win95, maybe dos..that extension needs to be sorted, gone thru, properly implemented.
Its still in construction phases, dont expect anything to work. The code is hooked for compatibility reasons.

It might work inside dosbox/freeDos.

#### What about Win 95/98/ME?

95/98/and ME(486-Pentium era) are "glorified user-interfaced DOS-boxes".<br>
Code for DOS (go32v2 or basic Win32 APIs) and youll be fine here.

These OSes do not use the "NT core"(VM86 cpu modes).
DirectX <6 and WinG were used here, as well.

WinAPI is likely hooked for win31-95/98/me functions. Ive not tested this yet.

### Mac OS9

OS9 capable systems usually had a 512MB Dimm slot somewhere.<br>
This would put them on -par with WinME, Win2k -and possibly low end WinXP systems(if added).
If youve not upgraded- UPGRADE.

CodeWarrior Base (OS9) is used as a starting point.<br>
Syntax is similar to fpc's MacPascal modes, and win32 build mode(xp era) is available, this is much like Lazarus.
(CW Pro v4/v7 (patched) is the last to support Pascal.)

DrawSprockets(pre-OSX) are preferred over QuickDraw/QuickDraw GX. <br>
Documentation does exist, but its hard to come by.<br>

OS9 WILL NOT **AND CANNOT** SUPPORT SDLv2.<br>
If you can dual-boot back into OSX- do so. <br>
-Then you can use SDL and OpenGL. I cant promise you SDLv2, but I can try.

### OSX

OSX can use Lazarus(now)- or the older FP UInterfaces+XCode method. Both work.
Be wary of "distribution specific issues" like FAT BINARIES and "forced 64bit compatibility" imposed by "Apple".

fpc should run but its buried inside of Lazarus on the command line.
(/usr/local/lib/something/somewhere....)

SDL has to be build "nibless" and placed by hand for anything to use it.

### Dos

Dos-Based systems generally dont connect to the internet.<br>
(You would be browsing in text mode if you were.)

	The trick is to setup Packet Communications and use (NE2000?) an ISA network card with dos mode drivers(10BaseT).

RETRO-Computing (and curiosity) are really the only reasons for using (or emulating) such old systems.<br>
They are a real PITA to write code for, even with DPMI and LFB/VESA available.


There are 3 (major) DOS compiler modes:

These two are emulated by PC-EM/possibly DosBox/DosEmu (right click the window, if confused)

	PURE 8088/86 (fpc i8086 8-bit port)
	286/386 (used??) (16bit cpu port)

	386-486DX(Qemu can take over here- fpc go32v2 can build Pentium/Pentium II code)

Integer and math addressing still use 16bits, until the 486.
Newer Pentiums and clones add multimedia cpu extensions.
**WATCH THE PENTIUM MATH BUG**

---


When you build using fpc(lazarus cant hook SDL correctly)-

Distribute the DLLs when you distribute your application of LazGfx, PLEASE!<br>
(Dont make people hunt these down.)

The core routines are here-
	I have to work on ellippses,polys, and fills-
		but SDL has the routines(as does GL), Im not starting from scratch here.

The tricky parts are C-syncing and Line drawing.

---

Sound is provided by libuos(Portaudio)


#### COPYLEFT

This code is a "black boxed spinoff" work written primarily for FPC in Pascal.
Nothing was reverse engineered- except published documentation.

Borland, INC. has been bought out and seems to "be no more".
Unlike Microsoft, I respect thier codebase and right to copyright.

Original code (for DOS) (c) Borland, INC.
and ported (from C) via FreePascal (FPK) dev team, myself and a few others.

I have left reference in the code where it is due.
I only accept credit where its due me.

Code is universal language of itself.
If I can understand German or russian programmers, you can understand my "sailored" english.

---

#### The Boat Demo

The boat image is actual TCanvas output:

Heres how you draw it.

In case youre wondering- this demo is from a book.<br>
It was taken from Owen Bishops Java demo.The port was for a Pascal book.<br>
(Its not my work, I dont take credit for it)<br>

Expected output size: (640,480)

---

SetBackgroundColor(SkyBlue);

SetColor(Blue);
SDL_Rect(0,310,640,480); //the ocean


//brush and pen color are a TCanvas hack- bump in one pixel of specified Poly and trace it with a different color
//ignored for now- but the boat will not have a red outline.

SDL_FilledPolyGon((180,60),(180,380),(340,380));
SDL_FilledPolyGon((180,60),(145,120),(120,180),(105,240),(110,300),(120,340),(145,375),(175,380),(160,340),(145,300),(140,240),(150,180),(160,120));

SetColor(Red);
SDL_FilledPolyGon((80,395),(130,420),(360,420),(380,400);
SDL_FilledPolyGon((180,40),(180,60),(220,50));
SDL_Rect(180,40,178,420);

Fontsize(12);
FontColor(Green)
outText(150,50,'Graphics with Lazarus')


### Credit

This is not Borland's code. It is blackboxed by myself, accelerating the previous attempts of others.

If I didnt write the code, Ill let you know. <br>
I wrote 93% of this unit myself.<br>

The "C merge" will change this percentage, the Professor/Doctorate seems to have a better implementation of input APIs.
-However, his code has some major flaws in it.

The Palette and ModeList code is 100% my implementation(currently breaks compiling), based upon previous VESA specifications.<br>
I take no credit for older VESA/SDL/SDL2 nor JEDI teams code(just my mods and hacks).

All code used is LGPL/GPL or equivalent licensed. GPL allows higher level licensing such as BSD/Apache(at ones option).

ObjectGraphics:

ObjGfx is Copyright © 1999
Mark Iuzzolino & Daniel Robbins.

Portions Copyright © 1972
Harold J Iuzzolino

Special thanx to Mark Baldwin and Ted Gruber for their contributions to the data structures and resolution of VESA coding problems;
Henrique de Paula for writing the NewFrontier manual;
Ansgar Scherp, Remco De Korte, Scott Earnest, Jare/Iguana, Denthor of Asphyxia, and others for their graphical demos
Micah LaCombe and Shawn Bailly for showing mouse support was possible.

Shitty software rendering engine(SSRE) and LazHep:
(Doom, Duke Nukem, Wolfenstien - like game engine)

Kostas "Bad Sector" Michalopoulos
badsectoracula@gmail.com

http://runtimelegend.com/blog/author/kostas-bad-sector-michalopoulos.html

