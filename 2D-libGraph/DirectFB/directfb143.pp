{
  
  DirectFB 1.4.3 bindings for FreePascal.
  (c) Copyright 2009-2010  The WiSeSLAp Team
  Written by Roland Schäfer <mail@rolandschaefer.net>

  This file is licensed under the same license as the original header:

  (c) Copyright 2001-2009  The world wide DirectFB Open Source Community (directfb.org)
  (c) Copyright 2000-2004  Convergence (integrated media) GmbH

  All rights reserved.

  Written by Denis Oliver Kropp <dok@directfb.org>,
            Andreas Hundt <andi@fischlustig.de>,
            Sven Neumann <neo@directfb.org>,
            Ville Syrjälä <syrjala@sci.fi> and
            Claudio Ciccani <klan@users.sf.net>.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA 02111-1307, USA.
}


const
  DFB_GRAPHICS_DRIVER_INFO_NAME_LENGTH = 40;
  DFB_GRAPHICS_DRIVER_INFO_VENDOR_LENGTH = 60;

  DFB_DISPLAYLAYER_IDS_MAX = 32;

  DTEID_UTF8  = 0;
  DTEID_OTHER = 1;

  DFB_NUM_PIXELFORMATS = 35;
  DFB_DISPLAY_LAYER_DESC_NAME_LENGTH = 32;
  DFB_DISPLAY_LAYER_SOURCE_DESC_NAME_LENGTH = 24;
  DFB_SCREEN_DESC_NAME_LENGTH = 32;
  DFB_INPUT_DEVICE_DESC_NAME_LENGTH = 32;
  DFB_INPUT_DEVICE_DESC_VENDOR_LENGTH = 40;
  DFB_GRAPHICS_DEVICE_DESC_NAME_LENGTH = 48;
  DFB_GRAPHICS_DEVICE_DESC_VENDOR_LENGTH = 64;

  DLID_PRIMARY = $0000;

  DLSID_SURFACE = $0000;

  DSCID_PRIMARY = $0000;

  DIDID_KEYBOARD  = $0000;
  DIDID_MOUSE = $0001;
  DIDID_JOYSTICK = $0002;
  DIDID_REMOTE = $0003;
  DIDID_ANY = $0010;

  DFB_SCREEN_MIXER_DESC_NAME_LENGTH = 24;
  DFB_SCREEN_OUTPUT_DESC_NAME_LENGTH = 24;
  
  DFB_SCREEN_ENCODER_DESC_NAME_LENGTH = 24;

  DFB_STREAM_DESC_ENCODING_LENGTH = 30;
  DFB_STREAM_DESC_TITLE_LENGTH = 255;
  DFB_STREAM_DESC_AUTHOR_LENGTH = 255;
  DFB_STREAM_DESC_ALBUM_LENGTH = 255;
  DFB_STREAM_DESC_GENRE_LENGTH = 32;
  DFB_STREAM_DESC_COMMENT_LENGTH = 255;


{ Pascal-only type defintions }

type
  PCInt = ^CInt;
  PPCint = ^PCint;
  PPChar = ^PChar;
  PS32 = ^S32;
  PU8 = ^U8;



{ Type aliases}

type
  DFBScreenID = CUInt;
  PDFBScreenID = ^DFBScreenID;

  DFBDisplayLayerID = CUInt;
  PDFBDisplayLayerID = ^DFBDisplayLayerID;

  DFBDisplayLayerSourceID = CUInt;
  PDFBDisplayLayerSourceID = ^DFBDisplayLayerSourceID;

  DFBWindowID = CUInt;
  PDFBWindowID = ^DFBWindowID;

  DFBInputDeviceID = CUInt;
  PDFBInputDeviceID = ^DFBInputDeviceID;

  DFBTextEncodingID = CUInt;
  PDFBTextEncodingID = ^DFBTextEncodingID;

  DFBDisplayLayerIDs = U32;
  PDFBDisplayLayerIDs = ^DFBDisplayLayerIDs;

  DFBColorID = CUInt;
  PDFBColorID = ^DFBColorID;

const
  DCID_PRIMARY : DFBColorID = 0;
  DCID_OUTLINE : DFBColorID = 1;
  DFB_COLOR_IDS_MAX : DFBColorID = 8;


{ Enumerations }


const
  _DFB__RESULT_OFFSET  = ((CUInt('D') and $7f) * $02000000) +
    ((CUInt('F') and $7f) * 00040000) +
    ((CUInt('B') and $7f) * 00000800);
  _DFB_NOVIDEOMEMORY = ((CUInt('D') and $7f) * $02000000) +
    ((CUInt('F') and $7f) * 00040000) +
    ((CUInt('B') and $7f) * 00000800) +
    $00000001;
  _DFB_MISSINGFONT = ((CUInt('D') and $7f) * $02000000) +
    ((CUInt('F') and $7f) * 00040000) +
    ((CUInt('B') and $7f) * 00000800) +
    $00000002;
  _DFB_MISSINGIMAGE = ((CUInt('D') and $7f) * $02000000) +
   ((CUInt('F') and $7f) * 00040000) +
   ((CUInt('B') and $7f) * 00000800) +
   $00000003;

type
  DirectResult = (
    DFB_OK = $00000000,
    DFB_FAILURE = $00000001,
    DFB_INIT = $00000002,
    DFB_BUG = $00000003,
    DFB_DEAD = $00000004,
    DFB_UNSUPPORTED = $00000005,
    DFB_UNIMPLEMENTED = $00000006,
    DFB_ACCESSDENIED = $00000007,
    DFB_INVAREA = $00000008,
    DFB_INVARG = $00000009,
    DFB_NOSYSTEMMEMORY = $00000010,
    DFB_NOSHAREDMEMORY = $00000011,
    DFB_LOCKED = $00000012,
    DFB_BUFFEREMPTY = $00000013,
    DFB_FILENOTFOUND = $00000014,
    DFB_IO = $00000015,
    DFB_BUSY = $00000016,
    DFB_NOIMPL = $00000017,
    DFB_TIMEOUT = $00000018,
    DFB_THIZNULL = $00000019,
    DFB_IDNOTFOUND = $00000020,
    DFB_DESTROYED = $00000021,
    DFB_FUSION = $00000022,
    DFB_BUFFERTOOLARGE = $00000023,
    DFB_INTERRUPTED = $00000024,
    DFB_NOCONTEXT = $00000025,
    DFB_TEMPUNAVAIL = $00000026,
    DFB_LIMITEXCEEDED = $00000027,
    DFB_NOSUCHMETHOD = $00000028,
    DFB_NOSUCHINSTANCE = $00000029,
    DFB_ITEMNOTFOUND = $00000030,
    DFB_VERSIONMISMATCH = $00000031,
    DFB_EOF = $00000032,
    DFB_SUSPENDED = $00000033,
    DFB_INCOMPLETE = $00000034,
    DFB_NOCORE = $00000035,
    DFB__RESULT_OFFSET  = _DFB__RESULT_OFFSET,
    DFB_NOVIDEOMEMORY = _DFB_NOVIDEOMEMORY,
    DFB_MISSINGFONT = _DFB_MISSINGFONT,
    DFB_MISSINGIMAGE = _DFB_MISSINGIMAGE
  );
  PDirectResult = ^DirectResult;
  DFBResult = DirectResult;
  PDFBResult = ^DFBResult;


function DFBErrToStr(Result : DFBResult) : string;
function DFBErrToText(Result : DFBResult) : string;


type
  DFBCooperativeLevel = (
    DFSCL_NORMAL        := $00000000,
    DFSCL_FULLSCREEN,
    DFSCL_EXCLUSIVE
  );
  PDFBCooperativeLevel = ^DFBCooperativeLevel;
  

type
  DFBAccelerationMask = CUInt;
  PDFBAccelerationMask = ^DFBAccelerationMask;
const
  DFXL_NONE           : DFBAccelerationMask = $00000000;
  DFXL_FILLRECTANGLE  : DFBAccelerationMask = $00000001;
  DFXL_DRAWRECTANGLE  : DFBAccelerationMask = $00000002;
  DFXL_DRAWLINE       : DFBAccelerationMask = $00000004;
  DFXL_FILLTRIANGLE   : DFBAccelerationMask = $00000008;
  DFXL_BLIT           : DFBAccelerationMask = $00010000;
  DFXL_STRETCHBLIT    : DFBAccelerationMask = $00020000;
  DFXL_TEXTRIANGLES   : DFBAccelerationMask = $00040000;
  DFXL_BLIT2          : DFBAccelerationMask = $00080000;
  DFXL_DRAWSTRING     : DFBAccelerationMask = $01000000;
  DFXL_ALL            : DFBAccelerationMask = $010F000F;
  DFXL_ALL_DRAW       : DFBAccelerationMask = $0000000F;
  DFXL_ALL_BLIT       : DFBAccelerationMask = $010F0000;


type
  DFBSurfaceBlittingFlags = CUInt;
  PDFBSurfaceBlittingFlags = ^DFBSurfaceBlittingFlags;
const
  DSBLIT_NOFX               : DFBSurfaceBlittingFlags = $00000000;
  DSBLIT_BLEND_ALPHACHANNEL : DFBSurfaceBlittingFlags = $00000001;
  DSBLIT_BLEND_COLORALPHA   : DFBSurfaceBlittingFlags = $00000002;
  DSBLIT_COLORIZE           : DFBSurfaceBlittingFlags = $00000004;
  DSBLIT_SRC_COLORKEY       : DFBSurfaceBlittingFlags = $00000008;
  DSBLIT_DST_COLORKEY       : DFBSurfaceBlittingFlags = $00000010;
  DSBLIT_SRC_PREMULTIPLY    : DFBSurfaceBlittingFlags = $00000020;
  DSBLIT_DST_PREMULTIPLY    : DFBSurfaceBlittingFlags = $00000040;
  DSBLIT_DEMULTIPLY         : DFBSurfaceBlittingFlags = $00000080;
  DSBLIT_DEINTERLACE        : DFBSurfaceBlittingFlags = $00000100;
  DSBLIT_SRC_PREMULTCOLOR   : DFBSurfaceBlittingFlags = $00000200;
  DSBLIT_XOR                : DFBSurfaceBlittingFlags = $00000400;
  DSBLIT_INDEX_TRANSLATION  : DFBSurfaceBlittingFlags = $00000800;
  DSBLIT_ROTATE180          : DFBSurfaceBlittingFlags = $00001000;
  DSBLIT_ROTATE90           : DFBSurfaceBlittingFlags = $00002000;
  DSBLIT_ROTATE270          : DFBSurfaceBlittingFlags = $00004000;
  DSBLIT_COLORKEY_PROTECT   : DFBSurfaceBlittingFlags = $00010000;
  DSBLIT_SRC_MASK_ALPHA     : DFBSurfaceBlittingFlags = $00100000;
  DSBLIT_SRC_MASK_COLOR     : DFBSurfaceBlittingFlags = $00200000;
  DSBLIT_SOURCE2            : DFBSurfaceBlittingFlags = $00400000;
  DSBLIT_FLIP_HORIZONTAL    : DFBSurfaceBlittingFlags = $01000000;
  DSBLIT_FLIP_VERTICAL      : DFBSurfaceBlittingFlags = $02000000;



type
  DFBSurfaceDrawingFlags = CUInt;
  PDFBSurfaceDrawingFlags = ^DFBSurfaceDrawingFlags;
const
  DSDRAW_NOFX               : DFBSurfaceDrawingFlags = $00000000;
  DSDRAW_BLEND              : DFBSurfaceDrawingFlags = $00000001;
  DSDRAW_DST_COLORKEY       : DFBSurfaceDrawingFlags = $00000002;
  DSDRAW_SRC_PREMULTIPLY    : DFBSurfaceDrawingFlags = $00000004;
  DSDRAW_DST_PREMULTIPLY    : DFBSurfaceDrawingFlags = $00000008;
  DSDRAW_DEMULTIPLY         : DFBSurfaceDrawingFlags = $00000010;
  DSDRAW_XOR                : DFBSurfaceDrawingFlags = $00000020;

type
  DFBEnumerationResult = (
    DFENUM_OK           := $00000000,
    DFENUM_CANCEL       := $00000001
  );
  PDFBEnumerationResult = ^DFBEnumerationResult;

  DFBBoolean = (
    DFB_FALSE := 0,
    DFB_TRUE
  );
  PDFBBoolean = ^DFBBoolean;


type
  DFBSurfaceDescriptionFlags = CUInt;
  PDFBSurfaceDescriptionFlags = ^DFBSurfaceDescriptionFlags;
const
  DSDESC_NONE         : DFBSurfaceDescriptionFlags = $00000000;
  DSDESC_CAPS         : DFBSurfaceDescriptionFlags = $00000001;
  DSDESC_WIDTH        : DFBSurfaceDescriptionFlags = $00000002;
  DSDESC_HEIGHT       : DFBSurfaceDescriptionFlags = $00000004;
  DSDESC_PIXELFORMAT  : DFBSurfaceDescriptionFlags = $00000008;
  DSDESC_PREALLOCATED : DFBSurfaceDescriptionFlags = $00000010;
  DSDESC_PALETTE      : DFBSurfaceDescriptionFlags = $00000020;
  DSDESC_RESOURCE_ID  : DFBSurfaceDescriptionFlags = $00000100;
  DSDESC_HINTS        : DFBSurfaceDescriptionFlags = $00000200;
  DSDESC_ALL          : DFBSurfaceDescriptionFlags = $0000033F;


type
  DFBSurfaceCapabilities = CUInt;
  PDFBSurfaceCapabilities = ^DFBSurfaceCapabilities;
const
    DSCAPS_NONE          : DFBSurfaceCapabilities = $00000000;
    DSCAPS_PRIMARY       : DFBSurfaceCapabilities = $00000001;
    DSCAPS_SYSTEMONLY    : DFBSurfaceCapabilities = $00000002;
    DSCAPS_VIDEOONLY     : DFBSurfaceCapabilities = $00000004;
    DSCAPS_DOUBLE        : DFBSurfaceCapabilities = $00000010;
    DSCAPS_SUBSURFACE    : DFBSurfaceCapabilities = $00000020;
    DSCAPS_INTERLACED    : DFBSurfaceCapabilities = $00000040;
    DSCAPS_SEPARATED     : DFBSurfaceCapabilities = $00000080;
    DSCAPS_STATIC_ALLOC  : DFBSurfaceCapabilities = $00000100;
    DSCAPS_TRIPLE        : DFBSurfaceCapabilities = $00000200;
    DSCAPS_FLIPPING      : DFBSurfaceCapabilities = $00000210;
    DSCAPS_PREMULTIPLIED : DFBSurfaceCapabilities = $00001000;
    DSCAPS_DEPTH         : DFBSurfaceCapabilities = $00010000;
    DSCAPS_SHARED        : DFBSurfaceCapabilities = $00100000;
    DSCAPS_ROTATED       : DFBSurfaceCapabilities = $01000000;
    DSCAPS_ALL           : DFBSurfaceCapabilities = $011113F7;

  

type
  DFBSurfacePixelFormat = CUInt;
  PDFBSurfacePixelFormat = ^DFBSurfacePixelFormat;
const
  DSPF_UNKNOWN  : DFBSurfacePixelFormat = $00000000;

  DSPF_ARGB1555 : DFBSurfacePixelFormat =
    (0 and $7F ) or
    ((15 and $1F) shl 7) or
    ((1 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);
  
  DSPF_RGB16 : DFBSurfacePixelFormat = 
    (1 and $7F ) or
    ((16 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGB24 : DFBSurfacePixelFormat = 
    (2 and $7F ) or
    ((24 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((3 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGB32 : DFBSurfacePixelFormat = 
    (3 and $7F ) or
    ((24 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((4 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  
  DSPF_ARGB : DFBSurfacePixelFormat = 
    (4 and $7F ) or
    ((24 and $1F) shl 7) or
    ((8 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((4 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_A8 : DFBSurfacePixelFormat = 
    (5 and $7F ) or
    ((0 and $1F) shl 7) or
    ((8 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_YUY2 : DFBSurfacePixelFormat = 
    (6 and $7F ) or
    ((16 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGB332 : DFBSurfacePixelFormat = 
    (7 and $7F ) or
    ((8 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_UYVY : DFBSurfacePixelFormat = 
    (8 and $7F ) or
    ((16 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_I420 : DFBSurfacePixelFormat = 
    (9 and $7F ) or
    ((12 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((2 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_YV12 : DFBSurfacePixelFormat = 
    (10 and $7F ) or
    ((12 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((2 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_LUT8 : DFBSurfacePixelFormat = 
    (11 and $7F ) or
    ((8 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (1 shl 30) or
    (0 shl 31);

  DSPF_ALUT44 : DFBSurfacePixelFormat = 
    (12 and $7F ) or
    ((4 and $1F) shl 7) or
    ((4 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (1 shl 30) or
    (0 shl 31);

  DSPF_AiRGB : DFBSurfacePixelFormat = 
    (13 and $7F ) or
    ((24 and $1F) shl 7) or
    ((8 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((4 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (1 shl 31);

  DSPF_A1 : DFBSurfacePixelFormat = 
    (14 and $7F ) or
    ((0 and $1F) shl 7) or
    ((1 and $0F) shl 12) or
    (1 shl 16) or
    ((1 and $07) shl 17) or
    ((0 and $07) shl 20) or
    ((7 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_NV12 : DFBSurfacePixelFormat = 
    (15 and $7F ) or
    ((12 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((2 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_NV16 : DFBSurfacePixelFormat = 
    (16 and $7F ) or
    ((24 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((1 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_ARGB2554 : DFBSurfacePixelFormat = 
    (17 and $7F ) or
    ((14 and $1F) shl 7) or
    ((2 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_ARGB4444 : DFBSurfacePixelFormat = 
    (18 and $7F ) or
    ((12 and $1F) shl 7) or
    ((4 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGBA4444 : DFBSurfacePixelFormat = 
    (19 and $7F ) or
    ((12 and $1F) shl 7) or
    ((4 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_NV21 : DFBSurfacePixelFormat = 
    (20 and $7F ) or
    ((12 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((2 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_AYUV : DFBSurfacePixelFormat = 
    (21 and $7F ) or
    ((24 and $1F) shl 7) or
    ((8 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((4 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_A4 : DFBSurfacePixelFormat = 
    (22 and $7F ) or
    ((0 and $1F) shl 7) or
    ((4 and $0F) shl 12) or
    (1 shl 16) or
    ((4 and $07) shl 17) or
    ((0 and $07) shl 20) or
    ((1 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_ARGB1666 : DFBSurfacePixelFormat = 
    (23 and $7F ) or
    ((18 and $1F) shl 7) or
    ((1 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((3 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_ARGB6666 : DFBSurfacePixelFormat = 
    (24 and $7F ) or
    ((18 and $1F) shl 7) or
    ((6 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((3 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGB18 : DFBSurfacePixelFormat = 
    (25 and $7F ) or
    ((18 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((3 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_LUT2 : DFBSurfacePixelFormat = 
    (26 and $7F ) or
    ((2 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (1 shl 16) or
    ((2 and $07) shl 17) or
    ((0 and $07) shl 20) or
    ((3 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (1 shl 30) or
    (0 shl 31);

  DSPF_RGB444 : DFBSurfacePixelFormat = 
    (27 and $7F ) or
    ((12 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGB555 : DFBSurfacePixelFormat = 
    (28 and $7F ) or
    ((15 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_BGR555 : DFBSurfacePixelFormat = 
    (29 and $7F ) or
    ((15 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_RGBA5551 : DFBSurfacePixelFormat = 
    (30 and $7F ) or
    ((15 and $1F) shl 7) or
    ((1 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((2 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_YUV444P : DFBSurfacePixelFormat = 
    (31 and $7F ) or
    ((24 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((1 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((2 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_ARGB8565 : DFBSurfacePixelFormat = 
    (32 and $7F ) or
    ((16 and $1F) shl 7) or
    ((8 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((3 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_AVYU : DFBSurfacePixelFormat = 
    (33 and $7F ) or
    ((24 and $1F) shl 7) or
    ((8 and $0F) shl 12) or
    (1 shl 16) or
    ((0 and $07) shl 17) or
    ((4 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);

  DSPF_VYU : DFBSurfacePixelFormat = 
    (34 and $7F ) or
    ((24 and $1F) shl 7) or
    ((0 and $0F) shl 12) or
    (0 shl 16) or
    ((0 and $07) shl 17) or
    ((3 and $07) shl 20) or
    ((0 and $07) shl 23) or
    ((0 and $03) shl 26) or
    ((0 and $03) shl 28) or
    (0 shl 30) or
    (0 shl 31);


type
  DFBSurfaceHintFlags = CUInt;
  PDFBSurfaceHintFlags = ^DFBSurfaceHintFlags;
const
  DSHF_NONE                : DFBSurfaceHintFlags = $00000000;
  DSHF_LAYER               : DFBSurfaceHintFlags = $00000001;
  DSHF_WINDOW              : DFBSurfaceHintFlags = $00000002;
  DSHF_CURSOR              : DFBSurfaceHintFlags = $00000004;
  DSHF_FONT                : DFBSurfaceHintFlags = $00000008;
  DSHF_ALL                 : DFBSurfaceHintFlags = $0000000F;


type
  DFBPaletteDescriptionFlags = CUInt;
  PDFBPaletteDescriptionFlags = ^DFBPaletteDescriptionFlags;
const
    DPDESC_CAPS         : DFBPaletteDescriptionFlags = $00000001;
    DPDESC_SIZE         : DFBPaletteDescriptionFlags = $00000002;
    DPDESC_ENTRIES      : DFBPaletteDescriptionFlags = $00000004;

type  
  DFBPaletteCapabilities = CUInt;
  PDFBPaletteCapabilities = ^DFBPaletteCapabilities;
const
  DPCAPS_NONE         : DFBPaletteCapabilities = $00000000;


type
  DFBDisplayLayerCapabilities = CUInt;
  PDFBDisplayLayerCapabilities = ^DFBDisplayLayerCapabilities;
const
    DLCAPS_NONE              : DFBDisplayLayerCapabilities = $00000000;
    DLCAPS_SURFACE           : DFBDisplayLayerCapabilities = $00000001;
    DLCAPS_OPACITY           : DFBDisplayLayerCapabilities = $00000002;
    DLCAPS_ALPHACHANNEL      : DFBDisplayLayerCapabilities = $00000004;
    DLCAPS_SCREEN_LOCATION   : DFBDisplayLayerCapabilities = $00000008;
    DLCAPS_FLICKER_FILTERING : DFBDisplayLayerCapabilities = $00000010;
    DLCAPS_DEINTERLACING     : DFBDisplayLayerCapabilities = $00000020;
    DLCAPS_SRC_COLORKEY      : DFBDisplayLayerCapabilities = $00000040;
    DLCAPS_DST_COLORKEY      : DFBDisplayLayerCapabilities = $00000080;
    DLCAPS_BRIGHTNESS        : DFBDisplayLayerCapabilities = $00000100;
    DLCAPS_CONTRAST          : DFBDisplayLayerCapabilities = $00000200;
    DLCAPS_HUE               : DFBDisplayLayerCapabilities = $00000400;
    DLCAPS_SATURATION        : DFBDisplayLayerCapabilities = $00000800;
    DLCAPS_LEVELS            : DFBDisplayLayerCapabilities = $00001000;
    DLCAPS_FIELD_PARITY      : DFBDisplayLayerCapabilities = $00002000;
    DLCAPS_WINDOWS           : DFBDisplayLayerCapabilities = $00004000;
    DLCAPS_SOURCES           : DFBDisplayLayerCapabilities = $00008000;
    DLCAPS_ALPHA_RAMP        : DFBDisplayLayerCapabilities = $00010000;
    DLCAPS_PREMULTIPLIED     : DFBDisplayLayerCapabilities = $00020000;
    DLCAPS_SCREEN_POSITION   : DFBDisplayLayerCapabilities = $00100000;
    DLCAPS_SCREEN_SIZE       : DFBDisplayLayerCapabilities = $00200000;
    DLCAPS_CLIP_REGIONS      : DFBDisplayLayerCapabilities = $00400000;
    DLCAPS_ALL               : DFBDisplayLayerCapabilities = $0073FFFF;


type
  DFBScreenCapabilities = CUInt;
  PDFBScreenCapabilities = ^DFBScreenCapabilities;
const
  DSCCAPS_NONE             : DFBScreenCapabilities = $00000000;
  DSCCAPS_VSYNC            : DFBScreenCapabilities = $00000001;
  DSCCAPS_POWER_MANAGEMENT : DFBScreenCapabilities = $00000002;
  DSCCAPS_MIXERS           : DFBScreenCapabilities = $00000010;
  DSCCAPS_ENCODERS         : DFBScreenCapabilities = $00000020;
  DSCCAPS_OUTPUTS          : DFBScreenCapabilities = $00000040;
  DSCCAPS_ALL              : DFBScreenCapabilities = $00000073;


type
  DFBDisplayLayerOptions = CUInt;
  PDFBDisplayLayerOptions = ^DFBDisplayLayerOptions;
const
  DLOP_NONE                : DFBDisplayLayerOptions = $00000000;
  DLOP_ALPHACHANNEL        : DFBDisplayLayerOptions = $00000001;
  DLOP_FLICKER_FILTERING   : DFBDisplayLayerOptions = $00000002;
  DLOP_DEINTERLACING       : DFBDisplayLayerOptions = $00000004;
  DLOP_SRC_COLORKEY        : DFBDisplayLayerOptions = $00000008;
  DLOP_DST_COLORKEY        : DFBDisplayLayerOptions = $00000010;
  DLOP_OPACITY             : DFBDisplayLayerOptions = $00000020;
  DLOP_FIELD_PARITY        : DFBDisplayLayerOptions = $00000040;
  
    
type
  DFBDisplayLayerBufferMode = CUInt;
  PDFBDisplayLayerBufferMode = ^DFBDisplayLayerBufferMode;
const
  DLBM_UNKNOWN    : DFBDisplayLayerBufferMode = $00000000;
  DLBM_FRONTONLY  : DFBDisplayLayerBufferMode = $00000001;
  DLBM_BACKVIDEO  : DFBDisplayLayerBufferMode = $00000002;
  DLBM_BACKSYSTEM : DFBDisplayLayerBufferMode = $00000004;
  DLBM_TRIPLE     : DFBDisplayLayerBufferMode = $00000008;
  DLBM_WINDOWS    : DFBDisplayLayerBufferMode = $00000010;


type
  DFBSurfaceRenderOptions = CUInt;
  PDFBSurfaceRenderOptions = ^DFBSurfaceRenderOptions;
const
  DSRO_NONE                 : DFBSurfaceRenderOptions = $00000000;
  DSRO_SMOOTH_UPSCALE       : DFBSurfaceRenderOptions = $00000001;
  DSRO_SMOOTH_DOWNSCALE     : DFBSurfaceRenderOptions = $00000002;
  DSRO_MATRIX               : DFBSurfaceRenderOptions = $00000004;
  DSRO_ANTIALIAS            : DFBSurfaceRenderOptions = $00000008;
  DSRO_ALL                  : DFBSurfaceRenderOptions = $0000000F;


type
  DFBDisplayLayerTypeFlags = CUInt;
  PDFBDisplayLayerTypeFlags = ^DFBDisplayLayerTypeFlags;
const
    DLTF_NONE           : DFBDisplayLayerTypeFlags = $00000000;
    DLTF_GRAPHICS       : DFBDisplayLayerTypeFlags = $00000001;
    DLTF_VIDEO          : DFBDisplayLayerTypeFlags = $00000002;
    DLTF_STILL_PICTURE  : DFBDisplayLayerTypeFlags = $00000004;
    DLTF_BACKGROUND     : DFBDisplayLayerTypeFlags = $00000008;
    DLTF_ALL            : DFBDisplayLayerTypeFlags = $0000000F;


type
  DFBInputDeviceTypeFlags = CUInt;
  PDFBInputDeviceTypeFlags = ^DFBInputDeviceTypeFlags;
const
  DIDTF_NONE          : DFBInputDeviceTypeFlags = $00000000;
  DIDTF_KEYBOARD      : DFBInputDeviceTypeFlags = $00000001;
  DIDTF_MOUSE         : DFBInputDeviceTypeFlags = $00000002;
  DIDTF_JOYSTICK      : DFBInputDeviceTypeFlags = $00000004;
  DIDTF_REMOTE        : DFBInputDeviceTypeFlags = $00000008;
  DIDTF_VIRTUAL       : DFBInputDeviceTypeFlags = $00000010;
  DIDTF_ALL           : DFBInputDeviceTypeFlags = $0000001F;


type
  DFBInputDeviceCapabilities = CUInt;
  PDFBInputDeviceCapabilities = ^DFBInputDeviceCapabilities;
const
    DICAPS_KEYS         : DFBInputDeviceCapabilities = $00000001;
    DICAPS_AXES         : DFBInputDeviceCapabilities = $00000002;
    DICAPS_BUTTONS      : DFBInputDeviceCapabilities = $00000004;
    DICAPS_ALL          : DFBInputDeviceCapabilities = $00000007;

 
type
  DFBInputDeviceButtonIdentifier = CUInt;
  PDFBInputDeviceButtonIdentifier = ^DFBInputDeviceButtonIdentifier;
const
  DIBI_LEFT :   DFBInputDeviceButtonIdentifier = $00000000;
  DIBI_RIGHT :  DFBInputDeviceButtonIdentifier= $00000001;
  DIBI_MIDDLE : DFBInputDeviceButtonIdentifier = $00000002;
  DIBI_LAST :   DFBInputDeviceButtonIdentifier = $0000001F;
  DIBI_FIRST :  DFBInputDeviceButtonIdentifier = $00000000;



type
  DFBInputDeviceAxisIdentifier = CUInt;
  PDFBInputDeviceAxisIdentifier = ^DFBInputDeviceAxisIdentifier;
const
  DIAI_X :    DFBInputDeviceAxisIdentifier = $00000000;
  DIAI_Y :      DFBInputDeviceAxisIdentifier = $00000001;
  DIAI_Z :      DFBInputDeviceAxisIdentifier = $00000002;
  DIAI_LAST :   DFBInputDeviceAxisIdentifier = $0000001F;
  DIAI_FIRST : DFBInputDeviceAxisIdentifier = $00000000;

 
type
  DFBWindowDescriptionFlags = CUInt;
  PDFBWindowDescriptionFlags = ^DFBWindowDescriptionFlags;
const
  DWDESC_CAPS         : DFBWindowDescriptionFlags = $00000001;
  DWDESC_WIDTH        : DFBWindowDescriptionFlags = $00000002;
  DWDESC_HEIGHT       : DFBWindowDescriptionFlags = $00000004;
  DWDESC_PIXELFORMAT  : DFBWindowDescriptionFlags = $00000008;
  DWDESC_POSX         : DFBWindowDescriptionFlags = $00000010;
  DWDESC_POSY         : DFBWindowDescriptionFlags = $00000020;
  DWDESC_SURFACE_CAPS : DFBWindowDescriptionFlags = $00000040;
  DWDESC_PARENT       : DFBWindowDescriptionFlags = $00000080;
  DWDESC_OPTIONS      : DFBWindowDescriptionFlags = $00000100;
  DWDESC_STACKING     : DFBWindowDescriptionFlags = $00000200;
  DWDESC_TOPLEVEL_ID  : DFBWindowDescriptionFlags = $00000400;
  DWDESC_RESOURCE_ID  : DFBWindowDescriptionFlags = $00001000;

 
type
  DFBDataBufferDescriptionFlags = CUInt;
  PDFBDataBufferDescriptionFlags = ^DFBDataBufferDescriptionFlags;
const
  DBDESC_FILE         : DFBDataBufferDescriptionFlags = $00000001;
  DBDESC_MEMORY       : DFBDataBufferDescriptionFlags = $00000002;

 
type
  DFBWindowCapabilities = CUInt;
  PDFBWindowCapabilities = ^DFBWindowCapabilities;
const
  DWCAPS_NONE         : DFBWindowCapabilities = $00000000;
  DWCAPS_ALPHACHANNEL : DFBWindowCapabilities = $00000001;
  DWCAPS_DOUBLEBUFFER : DFBWindowCapabilities = $00000002;
  DWCAPS_INPUTONLY    : DFBWindowCapabilities = $00000004;
  DWCAPS_NODECORATION : DFBWindowCapabilities = $00000008;
  DWCAPS_SUBWINDOW    : DFBWindowCapabilities = $00000010;
  DWCAPS_NOFOCUS      : DFBWindowCapabilities = $00000100;
  DWCAPS_COLOR        : DFBWindowCapabilities = $00000020;
  DWCAPS_ALL          : DFBWindowCapabilities = $0000013F;


type 
  DFBWindowOptions = CUInt;
  PDFBWindowOptions = ^DFBWindowOptions;
const
  DWOP_NONE           : DFBWindowOptions = $00000000;
  DWOP_COLORKEYING    : DFBWindowOptions = $00000001;
  DWOP_ALPHACHANNEL   : DFBWindowOptions = $00000002;
  DWOP_OPAQUE_REGION  : DFBWindowOptions = $00000004;
  DWOP_SHAPED         : DFBWindowOptions = $00000008;
  DWOP_KEEP_POSITION  : DFBWindowOptions = $00000010;
  DWOP_KEEP_SIZE      : DFBWindowOptions = $00000020;
  DWOP_KEEP_STACKING  : DFBWindowOptions = $00000040;
  DWOP_GHOST          : DFBWindowOptions = $00001000;
  DWOP_INDESTRUCTIBLE : DFBWindowOptions = $00002000;
  DWOP_SCALE          : DFBWindowOptions = $00010000;
  DWOP_KEEP_ABOVE     : DFBWindowOptions = $00100000;
  DWOP_KEEP_UNDER     : DFBWindowOptions = $00200000;
  DWOP_FOLLOW_BOUNDS  : DFBWindowOptions = $00400000;
  DWOP_ALL            : DFBWindowOptions = $0071307F;


type 
  DFBWindowStackingClass = CUInt;
  PDFBWindowStackingClass = ^DFBWindowStackingClass;
const 
  DWSC_MIDDLE         : DFBWindowStackingClass = $00000000;
  DWSC_UPPER          : DFBWindowStackingClass = $00000001;
  DWSC_LOWER          : DFBWindowStackingClass = $00000002;


type
  DFBFontAttributes = CUInt;
  PDFBFontAttributes = ^DFBFontAttributes;
const
  DFFA_NONE           : DFBFontAttributes = $00000000;
  DFFA_NOKERNING      : DFBFontAttributes = $00000001;
  DFFA_NOHINTING      : DFBFontAttributes = $00000002;
  DFFA_MONOCHROME     : DFBFontAttributes = $00000004;
  DFFA_NOCHARMAP      : DFBFontAttributes = $00000008;
  DFFA_FIXEDCLIP      : DFBFontAttributes = $00000010;
  DFFA_NOBITMAP       : DFBFontAttributes = $00000020;
  DFFA_OUTLINED       : DFBFontAttributes = $00000040;
 
type
  DFBFontDescriptionFlags = CUInt;
  PDFBFontDescriptionFlags = ^DFBFontDescriptionFlags;
const
  DFDESC_ATTRIBUTES      : DFBFontDescriptionFlags = $00000001;
  DFDESC_HEIGHT          : DFBFontDescriptionFlags = $00000002;
  DFDESC_WIDTH           : DFBFontDescriptionFlags = $00000004;
  DFDESC_INDEX           : DFBFontDescriptionFlags = $00000008;
  DFDESC_FIXEDADVANCE    : DFBFontDescriptionFlags = $00000010;
  DFDESC_FRACT_HEIGHT    : DFBFontDescriptionFlags = $00000020;
  DFDESC_FRACT_WIDTH     : DFBFontDescriptionFlags = $00000040;
  DFDESC_OUTLINE_WIDTH   : DFBFontDescriptionFlags = $00000080;
  DFDESC_OUTLINE_OPACITY : DFBFontDescriptionFlags = $00000100;

type
  DFBDisplayLayerSourceCaps = CUInt;
  PDFBDisplayLayerSourceCaps = ^DFBDisplayLayerSourceCaps;
const
  DDLSCAPS_NONE    : DFBDisplayLayerSourceCaps = $00000000;
  DDLSCAPS_SURFACE : DFBDisplayLayerSourceCaps = $00000001;
  DDLSCAPS_ALL     : DFBDisplayLayerSourceCaps = $00000001;


type
  DFBInputDeviceAxisInfoFlags = CUInt;
  PDFBInputDeviceAxisInfoFlags = ^DFBInputDeviceAxisInfoFlags;
const
  DIAIF_NONE                    : DFBInputDeviceAxisInfoFlags = $00000000;
  DIAIF_ABS_MIN                 : DFBInputDeviceAxisInfoFlags = $00000001;
  DIAIF_ABS_MAX                 : DFBInputDeviceAxisInfoFlags = $00000002;
  DIAIF_ALL                     : DFBInputDeviceAxisInfoFlags = $00000003;


type
  DFBVideoProviderCapabilities = CUInt;
  PDFBVideoProviderCapabilities = ^DFBVideoProviderCapabilities;
const
  DVCAPS_BASIC       : DFBVideoProviderCapabilities = $00000000;
  DVCAPS_SEEK        : DFBVideoProviderCapabilities = $00000001;
  DVCAPS_SCALE       : DFBVideoProviderCapabilities = $00000002;
  DVCAPS_INTERLACED  : DFBVideoProviderCapabilities = $00000004;
  DVCAPS_SPEED       : DFBVideoProviderCapabilities = $00000008;
  DVCAPS_BRIGHTNESS  : DFBVideoProviderCapabilities = $00000010;
  DVCAPS_CONTRAST    : DFBVideoProviderCapabilities = $00000020;
  DVCAPS_HUE         : DFBVideoProviderCapabilities = $00000040;
  DVCAPS_SATURATION  : DFBVideoProviderCapabilities = $00000080;
  DVCAPS_INTERACTIVE : DFBVideoProviderCapabilities = $00000100;
  DVCAPS_VOLUME      : DFBVideoProviderCapabilities = $00000200;
  DVCAPS_EVENT       : DFBVideoProviderCapabilities = $00000400;
  DVCAPS_ATTRIBUTES  : DFBVideoProviderCapabilities = $00000800;
  DVCAPS_AUDIO_SEL   : DFBVideoProviderCapabilities = $00001000;


type
  DFBVideoProviderStatus = CUInt;
  PDFBVideoProviderStatus = ^DFBVideoProviderStatus;
const
  DVSTATE_UNKNOWN    : DFBVideoProviderStatus = $00000000;
  DVSTATE_PLAY       : DFBVideoProviderStatus = $00000001;
  DVSTATE_STOP       : DFBVideoProviderStatus = $00000002;
  DVSTATE_FINISHED   : DFBVideoProviderStatus = $00000003;
  DVSTATE_BUFFERING  : DFBVideoProviderStatus = $00000004;


type
  DFBVideoProviderPlaybackFlags = CUInt;
  PDFBVideoProviderPlaybackFlags = ^DFBVideoProviderPlaybackFlags;
const
  DVPLAY_NOFX        : DFBVideoProviderPlaybackFlags = $00000000;
  DVPLAY_REWIND      : DFBVideoProviderPlaybackFlags = $00000001;
  DVPLAY_LOOPING     : DFBVideoProviderPlaybackFlags = $00000002;


type
  DFBVideoProviderAudioUnits = CUInt;
  PDFBVideoProviderAudioUnits = ^DFBVideoProviderAudioUnits;
const
  DVAUDIOUNIT_NONE   : DFBVideoProviderAudioUnits = $00000000;
  DVAUDIOUNIT_ONE    : DFBVideoProviderAudioUnits = $00000001;
  DVAUDIOUNIT_TWO    : DFBVideoProviderAudioUnits = $00000002;
  DVAUDIOUNIT_THREE  : DFBVideoProviderAudioUnits = $00000004;
  DVAUDIOUNIT_FOUR   : DFBVideoProviderAudioUnits = $00000008;
  DVAUDIOUNIT_ALL    : DFBVideoProviderAudioUnits = $0000000F;


type
  DFBColorAdjustmentFlags = CUInt;
  PDFBColorAdjustmentFlags = ^DFBColorAdjustmentFlags;
const
  DCAF_NONE         : DFBColorAdjustmentFlags = $00000000;
  DCAF_BRIGHTNESS   : DFBColorAdjustmentFlags = $00000001;
  DCAF_CONTRAST     : DFBColorAdjustmentFlags = $00000002;
  DCAF_HUE          : DFBColorAdjustmentFlags = $00000004;
  DCAF_SATURATION   : DFBColorAdjustmentFlags = $00000008;
  DCAF_ALL          : DFBColorAdjustmentFlags = $0000000F;


type
  DFBDisplayLayerCooperativeLevel = (
    DLSCL_SHARED             := 0,
    DLSCL_EXCLUSIVE,
    DLSCL_ADMINISTRATIVE
  );
  PDFBDisplayLayerCooperativeLevel = ^DFBDisplayLayerCooperativeLevel;


  DFBDisplayLayerBackgroundMode = (
    DLBM_DONTCARE            := 0,
    DLBM_COLOR,
    DLBM_IMAGE,
    DLBM_TILE
  );
  PDFBDisplayLayerBackgroundMode = ^DFBDisplayLayerBackgroundMode;


type
  DFBDisplayLayerConfigFlags = CUInt;
  PDFBDisplayLayerConfigFlags = ^DFBDisplayLayerConfigFlags;
const
  DLCONF_NONE              : DFBDisplayLayerConfigFlags = $00000000;
  DLCONF_WIDTH             : DFBDisplayLayerConfigFlags = $00000001;
  DLCONF_HEIGHT            : DFBDisplayLayerConfigFlags = $00000002;
  DLCONF_PIXELFORMAT       : DFBDisplayLayerConfigFlags = $00000004;
  DLCONF_BUFFERMODE        : DFBDisplayLayerConfigFlags = $00000008;
  DLCONF_OPTIONS           : DFBDisplayLayerConfigFlags = $00000010;
  DLCONF_SOURCE            : DFBDisplayLayerConfigFlags = $00000020;
  DLCONF_SURFACE_CAPS      : DFBDisplayLayerConfigFlags = $00000040;
  DLCONF_ALL               : DFBDisplayLayerConfigFlags = $0000007F;
 

type
  DFBScreenPowerMode = (
    DSPM_ON        := 0,
    DSPM_STANDBY,
    DSPM_SUSPEND,
    DSPM_OFF
  );
  PDFBScreenPowerMode = ^DFBScreenPowerMode;


type
  DFBScreenMixerCapabilities = CUInt;
  PDFBScreenMixerCapabilities = ^DFBScreenMixerCapabilities;
const
  DSMCAPS_NONE         : DFBScreenMixerCapabilities = $00000000;
  DSMCAPS_FULL         : DFBScreenMixerCapabilities = $00000001;
  DSMCAPS_SUB_LEVEL    : DFBScreenMixerCapabilities = $00000002;
  DSMCAPS_SUB_LAYERS   : DFBScreenMixerCapabilities = $00000004;
  DSMCAPS_BACKGROUND   : DFBScreenMixerCapabilities = $00000008;
  

type
  DFBScreenMixerConfigFlags = CUInt;
  PDFBScreenMixerConfigFlags = ^DFBScreenMixerConfigFlags;
const
  DSMCONF_NONE         : DFBScreenMixerConfigFlags = $00000000;
  DSMCONF_TREE         : DFBScreenMixerConfigFlags = $00000001;
  DSMCONF_LEVEL        : DFBScreenMixerConfigFlags = $00000002;
  DSMCONF_LAYERS       : DFBScreenMixerConfigFlags = $00000004;
  DSMCONF_BACKGROUND   : DFBScreenMixerConfigFlags = $00000010;
  DSMCONF_ALL          : DFBScreenMixerConfigFlags = $00000017;

  

type
  DFBScreenMixerTree = CUInt;
  PDFBScreenMixerTree = ^DFBScreenMixerTree;
const
  DSMT_UNKNOWN         : DFBScreenMixerTree = $00000000;
  DSMT_FULL            : DFBScreenMixerTree = $00000001;
  DSMT_SUB_LEVEL       : DFBScreenMixerTree = $00000002;
  DSMT_SUB_LAYERS      : DFBScreenMixerTree = $00000003;


type
  DFBScreenOutputCapabilities = CUInt;
  PDFBScreenOutputCapabilities = ^DFBScreenOutputCapabilities;
const
  DSOCAPS_NONE          : DFBScreenOutputCapabilities = $00000000;
  DSOCAPS_CONNECTORS    : DFBScreenOutputCapabilities = $00000001;
  DSOCAPS_ENCODER_SEL   : DFBScreenOutputCapabilities = $00000010;
  DSOCAPS_SIGNAL_SEL    : DFBScreenOutputCapabilities = $00000020;
  DSOCAPS_CONNECTOR_SEL : DFBScreenOutputCapabilities = $00000040;
  DSOCAPS_SLOW_BLANKING : DFBScreenOutputCapabilities = $00000080;
  DSOCAPS_RESOLUTION    : DFBScreenOutputCapabilities = $00000100;
  DSOCAPS_ALL           : DFBScreenOutputCapabilities = $000001F1;
  

type
  DFBScreenOutputConnectors = CUInt;
  PDFBScreenOutputConnectors = ^DFBScreenOutputConnectors;
const
  DSOC_UNKNOWN        : DFBScreenOutputConnectors = $00000000;
  DSOC_VGA            : DFBScreenOutputConnectors = $00000001;
  DSOC_SCART          : DFBScreenOutputConnectors = $00000002;
  DSOC_YC             : DFBScreenOutputConnectors = $00000004;
  DSOC_CVBS           : DFBScreenOutputConnectors = $00000008;
  DSOC_SCART2         : DFBScreenOutputConnectors = $00000010;
  DSOC_COMPONENT      : DFBScreenOutputConnectors = $00000020;
  DSOC_HDMI           : DFBScreenOutputConnectors = $00000040;
  

type
  DFBScreenOutputSignals = CUInt;
  PDFBScreenOutputSignals = ^DFBScreenOutputSignals;
const
  DSOS_NONE           : DFBScreenOutputSignals = $00000000;
  DSOS_VGA            : DFBScreenOutputSignals = $00000001;
  DSOS_YC             : DFBScreenOutputSignals = $00000002;
  DSOS_CVBS           : DFBScreenOutputSignals = $00000004;
  DSOS_RGB            : DFBScreenOutputSignals = $00000008;
  DSOS_YCBCR          : DFBScreenOutputSignals = $00000010;
  DSOS_HDMI           : DFBScreenOutputSignals = $00000020;
  DSOS_656            : DFBScreenOutputSignals = $00000040;


type
  DFBScreenOutputSlowBlankingSignals = CUInt;
  PDFBScreenOutputSlowBlankingSignals = ^DFBScreenOutputSlowBlankingSignals;
const
  DSOSB_OFF           : DFBScreenOutputSlowBlankingSignals = $00000000;
  DSOSB_16x9          : DFBScreenOutputSlowBlankingSignals = $00000001;
  DSOSB_4x3           : DFBScreenOutputSlowBlankingSignals = $00000002;
  DSOSB_FOLLOW        : DFBScreenOutputSlowBlankingSignals = $00000004;
  DSOSB_MONITOR       : DFBScreenOutputSlowBlankingSignals = $00000008;
  

type
  DFBScreenOutputResolution = CUInt;
  PDFBScreenOutputResolution = ^DFBScreenOutputResolution;
const
  DSOR_UNKNOWN   : DFBScreenOutputResolution = $00000000;
  DSOR_640_480   : DFBScreenOutputResolution = $00000001;
  DSOR_720_480   : DFBScreenOutputResolution = $00000002;
  DSOR_720_576   : DFBScreenOutputResolution = $00000004;
  DSOR_800_600   : DFBScreenOutputResolution = $00000008;
  DSOR_1024_768  : DFBScreenOutputResolution = $00000010;
  DSOR_1152_864  : DFBScreenOutputResolution = $00000020;
  DSOR_1280_720  : DFBScreenOutputResolution = $00000040;
  DSOR_1280_768  : DFBScreenOutputResolution = $00000080;
  DSOR_1280_960  : DFBScreenOutputResolution = $00000100;
  DSOR_1280_1024 : DFBScreenOutputResolution = $00000200;
  DSOR_1400_1050 : DFBScreenOutputResolution = $00000400;
  DSOR_1600_1200 : DFBScreenOutputResolution = $00000800;
  DSOR_1920_1080 : DFBScreenOutputResolution = $00001000;
  DSOR_ALL       : DFBScreenOutputResolution = $00001FFF;


type
  DFBScreenOutputConfigFlags = CUInt;
  PDFBScreenOutputConfigFlags = ^DFBScreenOutputConfigFlags;
const
  DSOCONF_NONE         : DFBScreenOutputConfigFlags = $00000000;
  DSOCONF_ENCODER      : DFBScreenOutputConfigFlags = $00000001;
  DSOCONF_SIGNALS      : DFBScreenOutputConfigFlags = $00000002;
  DSOCONF_CONNECTORS   : DFBScreenOutputConfigFlags = $00000004;
  DSOCONF_SLOW_BLANKING: DFBScreenOutputConfigFlags = $00000008;
  DSOCONF_RESOLUTION   : DFBScreenOutputConfigFlags = $00000010;
  DSOCONF_ALL          : DFBScreenOutputConfigFlags = $0000001F;


type
  DFBScreenEncoderCapabilities = CUInt;
  PDFBScreenEncoderCapabilities = ^DFBScreenEncoderCapabilities;
const
  DSECAPS_NONE         : DFBScreenEncoderCapabilities = $00000000;
  DSECAPS_TV_STANDARDS : DFBScreenEncoderCapabilities = $00000001;
  DSECAPS_TEST_PICTURE : DFBScreenEncoderCapabilities = $00000002;
  DSECAPS_MIXER_SEL    : DFBScreenEncoderCapabilities = $00000004;
  DSECAPS_OUT_SIGNALS  : DFBScreenEncoderCapabilities = $00000008;
  DSECAPS_SCANMODE     : DFBScreenEncoderCapabilities = $00000010;
  DSECAPS_FREQUENCY    : DFBScreenEncoderCapabilities = $00000020;
  DSECAPS_BRIGHTNESS   : DFBScreenEncoderCapabilities = $00000100;
  DSECAPS_CONTRAST     : DFBScreenEncoderCapabilities = $00000200;
  DSECAPS_HUE          : DFBScreenEncoderCapabilities = $00000400;
  DSECAPS_SATURATION   : DFBScreenEncoderCapabilities = $00000800;
  DSECAPS_CONNECTORS   : DFBScreenEncoderCapabilities = $00001000;
  DSECAPS_SLOW_BLANKING : DFBScreenEncoderCapabilities = $00002000;
  DSECAPS_RESOLUTION   : DFBScreenEncoderCapabilities = $00004000;
  DSECAPS_ALL          : DFBScreenEncoderCapabilities = $00007f3f;

  
type
  DFBScreenEncoderType = CUInt;
  PDFBScreenEncoderType = ^DFBScreenEncoderType;
const
  DSET_UNKNOWN         : DFBScreenEncoderType = $00000000;
  DSET_CRTC            : DFBScreenEncoderType = $00000001;
  DSET_TV              : DFBScreenEncoderType = $00000002;
  DSET_DIGITAL         : DFBScreenEncoderType = $00000004;

  
type
  DFBScreenEncoderTVStandards = CUInt;
  PDFBScreenEncoderTVStandards = ^DFBScreenEncoderTVStandards;
const
  DSETV_UNKNOWN        : DFBScreenEncoderTVStandards = $00000000;
  DSETV_PAL            : DFBScreenEncoderTVStandards = $00000001;
  DSETV_NTSC           : DFBScreenEncoderTVStandards = $00000002;
  DSETV_SECAM          : DFBScreenEncoderTVStandards = $00000004;
  DSETV_PAL_60         : DFBScreenEncoderTVStandards = $00000008;
  DSETV_PAL_BG         : DFBScreenEncoderTVStandards = $00000010;
  DSETV_PAL_I          : DFBScreenEncoderTVStandards = $00000020;
  DSETV_PAL_M          : DFBScreenEncoderTVStandards = $00000040;
  DSETV_PAL_N          : DFBScreenEncoderTVStandards = $00000080;
  DSETV_PAL_NC         : DFBScreenEncoderTVStandards = $00000100;
  DSETV_NTSC_M_JPN     : DFBScreenEncoderTVStandards = $00000200;
  DSETV_NTSC_443       : DFBScreenEncoderTVStandards = $00000800;
  DSETV_DIGITAL        : DFBScreenEncoderTVStandards = $00000400;
  DSETV_ALL            : DFBScreenEncoderTVStandards = $00000FFF;
  

type
  DFBScreenEncoderScanMode = CUInt;
  PDFBScreenEncoderScanMode = ^DFBScreenEncoderScanMode;
const
  DSESM_UNKNOWN        : DFBScreenEncoderScanMode = $00000000;
  DSESM_INTERLACED     : DFBScreenEncoderScanMode = $00000001;
  DSESM_PROGRESSIVE    : DFBScreenEncoderScanMode = $00000002;

  
type
  DFBScreenEncoderFrequency = CUInt;
  PDFBScreenEncoderFrequency = ^DFBScreenEncoderFrequency;
const
  DSEF_UNKNOWN        : DFBScreenEncoderFrequency = $00000000;
  DSEF_25HZ           : DFBScreenEncoderFrequency = $00000001;
  DSEF_29_97HZ        : DFBScreenEncoderFrequency = $00000002;
  DSEF_50HZ           : DFBScreenEncoderFrequency = $00000004;
  DSEF_59_94HZ        : DFBScreenEncoderFrequency = $00000008;
  DSEF_60HZ           : DFBScreenEncoderFrequency = $00000010;
  DSEF_75HZ           : DFBScreenEncoderFrequency = $00000020;
  DSEF_30HZ           : DFBScreenEncoderFrequency = $00000040;
  DSEF_24HZ           : DFBScreenEncoderFrequency = $00000080;
  DSEF_23_976HZ       : DFBScreenEncoderFrequency = $00000100;

type
  DFBScreenEncoderConfigFlags = CUInt;
  PDFBScreenEncoderConfigFlags = ^DFBScreenEncoderConfigFlags;
const
  DSECONF_NONE         : DFBScreenEncoderConfigFlags = $00000000;
  DSECONF_TV_STANDARD  : DFBScreenEncoderConfigFlags = $00000001;
  DSECONF_TEST_PICTURE : DFBScreenEncoderConfigFlags = $00000002;
  DSECONF_MIXER        : DFBScreenEncoderConfigFlags = $00000004;
  DSECONF_OUT_SIGNALS  : DFBScreenEncoderConfigFlags = $00000008;
  DSECONF_SCANMODE     : DFBScreenEncoderConfigFlags = $00000010;
  DSECONF_TEST_COLOR   : DFBScreenEncoderConfigFlags = $00000020;
  DSECONF_ADJUSTMENT   : DFBScreenEncoderConfigFlags = $00000040;
  DSECONF_FREQUENCY    : DFBScreenEncoderConfigFlags = $00000080;
  DSECONF_CONNECTORS   : DFBScreenEncoderConfigFlags = $00000100;
  DSECONF_SLOW_BLANKING : DFBScreenEncoderConfigFlags = $00000200;
  DSECONF_RESOLUTION    : DFBScreenEncoderConfigFlags = $00000400;
  DSECONF_ALL          : DFBScreenEncoderConfigFlags = $000007FF;

  
type
  DFBScreenEncoderTestPicture = CUInt;
  PDFBScreenEncoderTestPicture = ^DFBScreenEncoderTestPicture;
const
  DSETP_OFF      : DFBScreenEncoderTestPicture = $00000000;
  DSETP_MULTI    : DFBScreenEncoderTestPicture = $00000001;
  DSETP_SINGLE   : DFBScreenEncoderTestPicture = $00000002;
  DSETP_WHITE    : DFBScreenEncoderTestPicture = $00000010;
  DSETP_YELLOW   : DFBScreenEncoderTestPicture = $00000020;
  DSETP_CYAN     : DFBScreenEncoderTestPicture = $00000030;
  DSETP_GREEN    : DFBScreenEncoderTestPicture = $00000040;
  DSETP_MAGENTA  : DFBScreenEncoderTestPicture = $00000050;
  DSETP_RED      : DFBScreenEncoderTestPicture = $00000060;
  DSETP_BLUE     : DFBScreenEncoderTestPicture = $00000070;
  DSETP_BLACK    : DFBScreenEncoderTestPicture = $00000080;


type
  DFBSurfaceFlipFlags = CUInt;
  PDFBSurfaceFlipFlags = ^DFBSurfaceFlipFlags;
const
  DSFLIP_NONE         : DFBSurfaceFlipFlags = $00000000;
  DSFLIP_WAIT         : DFBSurfaceFlipFlags = $00000001;
  DSFLIP_BLIT         : DFBSurfaceFlipFlags = $00000002;
  DSFLIP_ONSYNC       : DFBSurfaceFlipFlags = $00000004;
  DSFLIP_WAITFORSYNC  : DFBSurfaceFlipFlags = ($00000001 or $00000004);
  DSFLIP_PIPELINE     : DFBSurfaceFlipFlags = $00000008;
  DSFLIP_ONCE         : DFBSurfaceFlipFlags = $00000010;


type
  DFBSurfaceTextFlags = CUInt;
  PDFBSurfaceTextFlags = ^DFBSurfaceTextFlags;

const
   DSTF_LEFT           : DFBSurfaceTextFlags = $00000000;
   DSTF_CENTER         : DFBSurfaceTextFlags = $00000001;
   DSTF_RIGHT          : DFBSurfaceTextFlags = $00000002;
   DSTF_TOP            : DFBSurfaceTextFlags = $00000004;
   DSTF_BOTTOM         : DFBSurfaceTextFlags = $00000008;
   DSTF_OUTLINE        : DFBSurfaceTextFlags = $00000010;
   DSTF_TOPLEFT        : DFBSurfaceTextFlags = ($00000004 or $00000000);
   DSTF_TOPCENTER      : DFBSurfaceTextFlags = ($00000004 or $00000001);
   DSTF_TOPRIGHT       : DFBSurfaceTextFlags = ($00000004 or $00000002);
   DSTF_BOTTOMLEFT     : DFBSurfaceTextFlags = ($00000008 or $00000000);
   DSTF_BOTTOMCENTER   : DFBSurfaceTextFlags = ($00000008 or $00000001);
   DSTF_BOTTOMRIGHT    : DFBSurfaceTextFlags = ($00000008 or $00000002);


type
  DFBSurfaceLockFlags = (
    DSLF_READ           := $00000001,
    DSLF_WRITE          := $00000002
  );
  PDFBSurfaceLockFlags = ^DFBSurfaceLockFlags;
  

type
  DFBSurfacePorterDuffRule = CUInt;
  PDFBSurfacePorterDuffRule = ^DFBSurfacePorterDuffRule;
const
  DSPD_NONE           : DFBSurfacePorterDuffRule =  0;
  DSPD_CLEAR          : DFBSurfacePorterDuffRule =  1;
  DSPD_SRC            : DFBSurfacePorterDuffRule =  2;
  DSPD_SRC_OVER       : DFBSurfacePorterDuffRule =  3;
  DSPD_DST_OVER       : DFBSurfacePorterDuffRule =  4;
  DSPD_SRC_IN         : DFBSurfacePorterDuffRule =  5;
  DSPD_DST_IN         : DFBSurfacePorterDuffRule =  6;
  DSPD_SRC_OUT        : DFBSurfacePorterDuffRule =  7;
  DSPD_DST_OUT        : DFBSurfacePorterDuffRule =  8;
  DSPD_SRC_ATOP       : DFBSurfacePorterDuffRule =  9;
  DSPD_DST_ATOP       : DFBSurfacePorterDuffRule = 10;
  DSPD_ADD            : DFBSurfacePorterDuffRule = 11;
  DSPD_XOR            : DFBSurfacePorterDuffRule = 12;
  DSPD_DST            : DFBSurfacePorterDuffRule = 13;
  

type
  DFBSurfaceBlendFunction = CUInt;
  PDFBSurfaceBlendFunction = ^DFBSurfaceBlendFunction;
const
  DSBF_UNKNOWN            : DFBSurfaceBlendFunction = 0;
  DSBF_ZERO               : DFBSurfaceBlendFunction = 1; 
  DSBF_ONE                : DFBSurfaceBlendFunction = 2; 
  DSBF_SRCCOLOR           : DFBSurfaceBlendFunction = 3; 
  DSBF_INVSRCCOLOR        : DFBSurfaceBlendFunction = 4; 
  DSBF_SRCALPHA           : DFBSurfaceBlendFunction = 5; 
  DSBF_INVSRCALPHA        : DFBSurfaceBlendFunction = 6; 
  DSBF_DESTALPHA          : DFBSurfaceBlendFunction = 7; 
  DSBF_INVDESTALPHA       : DFBSurfaceBlendFunction = 8; 
  DSBF_DESTCOLOR          : DFBSurfaceBlendFunction = 9; 
  DSBF_INVDESTCOLOR       : DFBSurfaceBlendFunction = 10; 
  DSBF_SRCALPHASAT        : DFBSurfaceBlendFunction = 11 ;


type
  DFBTriangleFormation = (
    DTTF_LIST,
    DTTF_STRIP,
    DTTF_FAN
  );
  PDFBTriangleFormation = ^DFBTriangleFormation;
  

type
  DFBSurfaceMaskFlags = CUInt;
  PDFBSurfaceMaskFlags = ^DFBSurfaceMaskFlags;
const
 DSMF_NONE      : DFBSurfaceMaskFlags = $00000000;
 DSMF_STENCIL   : DFBSurfaceMaskFlags = $00000001;
 DSMF_ALL       : DFBSurfaceMaskFlags = $00000001;


type
  DFBInputDeviceKeyState = CUInt;
  PDFBInputDeviceKeyState = ^DFBInputDeviceKeyState;
const
  DIKS_UP             : DFBInputDeviceKeyState = $00000000;
  DIKS_DOWN           : DFBInputDeviceKeyState = $00000001;


type
  DFBInputDeviceButtonState = CUInt;
  PDFBInputDeviceButtonState = ^DFBInputDeviceButtonState;
const
  DIBS_UP             : DFBInputDeviceButtonState = $00000000;
  DIBS_DOWN           : DFBInputDeviceButtonState = $00000001;
  

type
  DFBInputDeviceButtonMask = CUInt;
  PDFBInputDeviceButtonMask = ^DFBInputDeviceButtonMask;
const
  DIBM_LEFT           : DFBInputDeviceButtonMask = $00000001;
  DIBM_RIGHT          : DFBInputDeviceButtonMask = $00000002;
  DIBM_MIDDLE         : DFBInputDeviceButtonMask = $00000004;
  

type
  DFBInputDeviceModifierMask = CUInt;
  PDFBInputDeviceModifierMask = ^DFBInputDeviceModifierMask;
  
const
   DIMM_SHIFT     : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_SHIFT));
   DIMM_CONTROL   : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_CONTROL));
   DIMM_ALT       : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_ALT));
   DIMM_ALTGR     : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_ALTGR));
   DIMM_META      : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_META));
   DIMM_SUPER     : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_SUPER));
   DIMM_HYPER     : DFBInputDeviceModifierMask = (1 shl LongInt(DIMKI_HYPER));


type
  DFBEventClass = CUInt;
  PDFBEventClass = ^DFBEventClass;
const
  DFEC_NONE           : DFBEventClass = $00;
  DFEC_INPUT          : DFBEventClass = $01;
  DFEC_WINDOW         : DFBEventClass = $02;
  DFEC_USER           : DFBEventClass = $03;
  DFEC_UNIVERSAL      : DFBEventClass = $04;
  DFEC_VIDEOPROVIDER  : DFBEventClass = $05;


type
  DFBInputEventType = (
    DIET_UNKNOWN        = 0,
    DIET_KEYPRESS,
    DIET_KEYRELEASE,
    DIET_BUTTONPRESS,
    DIET_BUTTONRELEASE,
    DIET_AXISMOTION
  );
  PDFBInputEventType = ^DFBInputEventType;


type
  DFBInputEventFlags = CUInt;
  PDFBInputEventFlags = ^DFBInputEventFlags;
const
  DIEF_NONE           : DFBInputEventFlags = $0000;
  DIEF_TIMESTAMP      : DFBInputEventFlags = $0001;
  DIEF_AXISABS        : DFBInputEventFlags = $0002;
  DIEF_AXISREL        : DFBInputEventFlags = $0004;
  DIEF_KEYCODE        : DFBInputEventFlags = $0008;
  DIEF_KEYID          : DFBInputEventFlags = $0010;
  DIEF_KEYSYMBOL      : DFBInputEventFlags = $0020;
  DIEF_MODIFIERS      : DFBInputEventFlags = $0040;
  DIEF_LOCKS          : DFBInputEventFlags = $0080;
  DIEF_BUTTONS        : DFBInputEventFlags = $0100;
  DIEF_GLOBAL         : DFBInputEventFlags = $0200;
  DIEF_REPEAT         : DFBInputEventFlags = $0400;
  DIEF_FOLLOW         : DFBInputEventFlags = $0800;
  DIEF_MIN            : DFBInputEventFlags = $1000;
  DIEF_MAX            : DFBInputEventFlags = $2000;


type
  DFBWindowEventType = CUInt;
  PDFBWindowEventType = ^DFBWindowEventType;
  
const
  DWET_NONE            = $00000000;
  DWET_POSITION        = $00000001;
  DWET_SIZE            = $00000002;
  DWET_CLOSE           = $00000004;
  DWET_DESTROYED       = $00000008;
  DWET_GOTFOCUS        = $00000010;
  DWET_LOSTFOCUS       = $00000020;
  DWET_KEYDOWN         = $00000100;
  DWET_KEYUP           = $00000200;
  DWET_BUTTONDOWN      = $00010000;
  DWET_BUTTONUP        = $00020000;
  DWET_MOTION          = $00040000;
  DWET_ENTER           = $00080000;
  DWET_LEAVE           = $00100000;
  DWET_WHEEL           = $00200000;
  DWET_ALL             = $003F033F;
  DWET_POSITION_SIZE  = CUInt(DWET_POSITION) or CUInt(DWET_SIZE);


type
  DFBWindowEventFlags = CUInt;
  PDFBWindowEventFlags = ^DFBWindowEventFlags;
  
const 
   DWEF_NONE           : DFBWindowEventFlags = $00000000;
   DWEF_RETURNED       : DFBWindowEventFlags = $00000001;
   DWEF_REPEAT         : DFBWindowEventFlags = $00000010;
   DWEF_ALL            : DFBWindowEventFlags = $00000011;


type
  DFBVideoProviderEventType = CUInt;
  PDFBVideoProviderEventType = ^DFBVideoProviderEventType;
const
  DVPET_NONE           : DFBVideoProviderEventType = $00000000;
  DVPET_STARTED        : DFBVideoProviderEventType = $00000001;
  DVPET_STOPPED        : DFBVideoProviderEventType = $00000002;
  DVPET_SPEEDCHANGE    : DFBVideoProviderEventType = $00000004;
  DVPET_STREAMCHANGE   : DFBVideoProviderEventType = $00000008;
  DVPET_FATALERROR     : DFBVideoProviderEventType = $00000010;
  DVPET_FINISHED       : DFBVideoProviderEventType = $00000020;
  DVPET_SURFACECHANGE  : DFBVideoProviderEventType = $00000040;
  DVPET_FRAMEDECODED   : DFBVideoProviderEventType = $00000080;
  DVPET_FRAMEDISPLAYED : DFBVideoProviderEventType = $00000100;
  DVPET_DATAEXHAUSTED  : DFBVideoProviderEventType = $00000200;
  DVPET_VIDEOACTION    : DFBVideoProviderEventType = $00000400;
  DVPET_DATALOW        : DFBVideoProviderEventType = $00000800;
  DVPET_DATAHIGH       : DFBVideoProviderEventType = $00001000;
  DVPET_BUFFERTIMELOW  : DFBVideoProviderEventType = $00002000;
  DVPET_BUFFERTIMEHIGH : DFBVideoProviderEventType = $00004000;
  DVPET_ALL            : DFBVideoProviderEventType = $00007FFF;


type
  DFBVideoProviderEventDataSubType = CUInt;
  PDFBVideoProviderEventDataSubType = ^DFBVideoProviderEventDataSubType;
const
  DVPEDST_UNKNOWN      : DFBVideoProviderEventDataSubType = $00000000;
  DVPEDST_AUDIO        : DFBVideoProviderEventDataSubType = $00000001;
  DVPEDST_VIDEO        : DFBVideoProviderEventDataSubType = $00000002;
  DVPEDST_DATA         : DFBVideoProviderEventDataSubType = $00000004;
  DVPEDST_ALL          : DFBVideoProviderEventDataSubType = $00000007;


type
  DFBWindowKeySelection = CUInt;
  PDFBWindowKeySelection = ^DFBWindowKeySelection;
const
  DWKS_ALL            : DFBWindowKeySelection = $00000000;
  DWKS_NONE           : DFBWindowKeySelection = $00000001;
  DWKS_LIST           : DFBWindowKeySelection = $00000002;

  
type
  DFBWindowGeometryMode = CUInt;
  PDFBWindowGeometryMode = ^DFBWindowGeometryMode;
const
    DWGM_DEFAULT        : DFBWindowGeometryMode = $00000000;
    DWGM_FOLLOW         : DFBWindowGeometryMode = $00000001;
    DWGM_RECTANGLE      : DFBWindowGeometryMode = $00000002;
    DWGM_LOCATION       : DFBWindowGeometryMode = $00000003;


type
  DFBImageCapabilities = CUInt;
  PDFBImageCapabilities = ^DFBImageCapabilities;
const
  DICAPS_NONE            : DFBImageCapabilities = $00000000;
  DICAPS_ALPHACHANNEL    : DFBImageCapabilities = $00000001;
  DICAPS_COLORKEY        : DFBImageCapabilities = $00000002;
  

type
  DIRenderCallbackResult = (
    DIRCR_OK,
    DIRCR_ABORT
  );
  PDIRenderCallbackResult = ^DIRenderCallbackResult;


type
  DFBStreamCapabilities = CUInt;
  PDFBStreamCapabilities = ^DFBStreamCapabilities;
const
  DVSCAPS_NONE         : DFBStreamCapabilities = $00000000;
  DVSCAPS_VIDEO        : DFBStreamCapabilities = $00000001;
  DVSCAPS_AUDIO        : DFBStreamCapabilities = $00000002;


type
  DFBStreamFormat = CUInt;
  PDFBStreamFormat = ^DFBStreamFormat;
const
  DSF_ES         : DFBStreamFormat = $00000000;
  DSF_PES        : DFBStreamFormat = $00000001;



{ Records }


type
  DFBGraphicsDriverInfo = record
    major : CInt;
    minor : CInt;
    name_ : array [0..DFB_GRAPHICS_DRIVER_INFO_NAME_LENGTH-1] of CChar;
    vendor : array [0..DFB_GRAPHICS_DRIVER_INFO_VENDOR_LENGTH-1] of CChar;
  end;
  PDFBGraphicsDriverInfo = ^DFBGraphicsDriverInfo;

  DFBGraphicsDeviceDescription = record
    acceleration_mask : DFBAccelerationMask;
    blitting_flags : DFBSurfaceBlittingFlags;
    drawing_flags : DFBSurfaceDrawingFlags;
    video_memory : CUInt;
    name_ : array [0..DFB_GRAPHICS_DEVICE_DESC_NAME_LENGTH-1] of CChar;
    vendor : array [0..DFB_GRAPHICS_DEVICE_DESC_VENDOR_LENGTH-1] of CChar;
    driver : DFBGraphicsDriverInfo;
  end;
  PDFBGraphicsDeviceDescription = ^DFBGraphicsDeviceDescription;

  DFBPoint = record
    x : CInt;
    y : CInt;
  end;
  PDFBPoint = ^DFBPoint;
  
  DFBSpan = record
    x : CInt;
    w : CInt;
  end;
  PDFBSpan = ^DFBSpan;

  DFBDimension = record
      w : CInt;
      h : CInt;
  end;
  PDFBDimension = ^DFBDimension;

  DFBRectangle = record
    x : CInt;
    y : CInt;
    w : CInt;
    h : CInt;
  end;
  PDFBRectangle = ^DFBRectangle;

  DFBLocation = record
    x : CFloat;
    y : CFloat;
    w : CFloat;
    h : CFloat;
  end;
  PDFBLocation = ^DFBLocation;

  DFBRegion = record
    x1 : CInt;
    y1 : CInt;
    x2 : CInt;
    y2 : CInt;
  end;
  PDFBRegion = ^DFBRegion;

  DFBInsets = record
    l : CInt;
    t : CInt;
    r : CInt;
    b : CInt;
  end;
  PDFBInsets = ^DFBInsets;

  DFBTriangle = record
    x1 : CInt;
    y1 : CInt;
    x2 : CInt;
    y2 : CInt;
    x3 : CInt;
    y3 : CInt;
  end;
  PDFBTriangle = ^DFBTriangle;

  DFBColor = record
    a : U8;
    r : U8;
    g : U8;
    b : U8;
  end;
  PDFBColor = ^DFBColor;

  preallocated_struct = record
    data : Pointer;
    pitch : CInt;
  end;

  palette_struct = record
    entries : PDFBColor;
    size : CUInt;
  end; 

  DFBSurfaceDescription = record
    flags : DFBSurfaceDescriptionFlags;
    caps : DFBSurfaceCapabilities;
    width : CInt;
    height : CInt;
    pixelformat : DFBSurfacePixelFormat;
    preallocated : array [0..1] of preallocated_struct;
    palette : palette_struct;
    resource_id : CULong;
    hintss : DFBSurfaceHintFlags;
  end;
  PDFBSurfaceDescription = ^DFBSurfaceDescription;

  DFBPaletteDescription = record
    flags : DFBPaletteDescriptionFlags;
    caps : DFBPaletteCapabilities;
    size : CUInt;
    entries : PDFBColor;
  end;
  PDFBPaletteDescription = ^DFBPaletteDescription;

  DFBColorKey = record
    index : U8;
    r : U8;
    g : U8;
    b : U8;
  end;
  PDFBColorKey = ^DFBColorKey;

  DFBColorYUV = record
    a : U8;
    y : U8;
    u : U8;
    v : U8;
  end;
  PDFBColorYUV = ^DFBColorYUV;


  DFBFontDescription = record
    flags : DFBFontDescriptionFlags;
    attributes : DFBFontAttributes;
    height : CInt;
    width : CInt;
    index_ : CUInt;
    fixed_advance : CInt;
    fract_height : CInt;
    fract_width : CInt;
    outline_width : CInt;
    outline_opacity : CInt;
  end;
  PDFBFontDescription = ^DFBFontDescription;


  DFBDisplayLayerDescription = record 
    type_ : DFBDisplayLayerTypeFlags;
    caps : DFBDisplayLayerCapabilities;
    name_ : array[0..(DFB_DISPLAY_LAYER_DESC_NAME_LENGTH)-1] of CChar;
    level : CInt;
    regions : CInt;
    sources : CInt;
    clip_regions : CInt;
  end;
  PDFBDisplayLayerDescription = ^DFBDisplayLayerDescription;


  DFBDisplayLayerSourceDescription = record
    source_id : DFBDisplayLayerSourceID;
    name_ : array[0..(DFB_DISPLAY_LAYER_SOURCE_DESC_NAME_LENGTH)-1] of CChar;
    caps : DFBDisplayLayerSourceCaps;
  end;
  PDFBDisplayLayerSourceDescription = ^DFBDisplayLayerSourceDescription;


  DFBScreenDescription = record
    caps : DFBScreenCapabilities;
    name_ : array[0..DFB_SCREEN_DESC_NAME_LENGTH-1] of CChar;
    mixers :CInt;
    encoders :CInt;
    outputs : CInt;
  end;
  PDFBScreenDescription = ^DFBScreenDescription;


  DFBInputDeviceDescription = record
    type_: DFBInputDeviceTypeFlags;
    caps : DFBInputDeviceCapabilities;
    min_keycode : CInt;
    max_keycode :CInt;
    max_axis : DFBInputDeviceAxisIdentifier;
    max_button : DFBInputDeviceButtonIdentifier;
    name_ : array [0..DFB_INPUT_DEVICE_DESC_NAME_LENGTH-1] of CChar;
    vendor : array [0..DFB_INPUT_DEVICE_DESC_VENDOR_LENGTH-1] of CChar;
  end;
  PDFBInputDeviceDescription = ^DFBInputDeviceDescription;


  DFBInputDeviceAxisInfo = record
    flags : DFBInputDeviceAxisInfoFlags;
    abs_min : CInt;
    abs_max : CInt;
  end;
  PDFBInputDeviceAxisInfo = ^DFBInputDeviceAxisInfo;


  DFBWindowDescription = record
    flags : DFBWindowDescriptionFlags;
    caps : DFBWindowCapabilities;
    width: CInt;
    height : CInt;
    pixelformat : DFBSurfacePixelFormat;
    posx : CInt;
    posy : CInt;
    surface_caps : DFBSurfaceCapabilities;
    parent_id : DFBWindowID;
    options : DFBWindowOptions;
    stacking : DFBWindowStackingClass;
    resource_id : CULong;
    toplevel_id : DFBWindowID;
  end;
  PDFBWindowDescription = ^DFBWindowDescription;

  memory_struct = record
    data : Pointer;
    length : CUint;
  end;

  DFBDataBufferDescription = record
    flags : DFBDataBufferDescriptionFlags;
    file_ : PChar;
    memory : memory_struct;
  end;
  PDFBDataBufferDescription = ^DFBDataBufferDescription;


  DFBColorAdjustment = record 
    flags : DFBColorAdjustmentFlags;
    brightness : U16;
    contrast : U16;
    hue : U16;
    saturation : U16;
  end;
  PDFBColorAdjustment = ^DFBColorAdjustment;


  DFBDisplayLayerConfig = record
    flags : DFBDisplayLayerConfigFlags;
    width :CInt;
    height : CInt;
    pixelformat : DFBSurfacePixelFormat;
    buffermode : DFBDisplayLayerBufferMode;
    options : DFBDisplayLayerOptions;
    source : DFBDisplayLayerSourceID;
    surface_caps : DFBSurfaceCapabilities;
  end;
  PDFBDisplayLayerConfig = ^DFBDisplayLayerConfig;


  DFBScreenMixerDescription = record
    caps : DFBScreenMixerCapabilities;
    layers : DFBDisplayLayerIDs;
    sub_num : CInt;
    sub_layers : DFBDisplayLayerIDs;
    name_ : array [0..DFB_SCREEN_MIXER_DESC_NAME_LENGTH-1] of CChar;
  end;
  PDFBScreenMixerDescription = ^DFBScreenMixerDescription;



  DFBScreenMixerConfig = record
    flags : DFBScreenMixerConfigFlags;
    tree : DFBScreenMixerTree;
    level : CInt;
    layers : DFBDisplayLayerIDs;
    background : DFBColor;
  end;
  PDFBScreenMixerConfig = ^DFBScreenMixerConfig;


  DFBScreenOutputDescription = record
    caps : DFBScreenOutputCapabilities;
    all_connectors : DFBScreenOutputConnectors;
    all_signals : DFBScreenOutputSignals;
    all_resolutions : DFBScreenOutputResolution;
    name_ : array [0..DFB_SCREEN_OUTPUT_DESC_NAME_LENGTH-1] of CChar;
  end;
  PDFBScreenOutputDescription = ^DFBScreenOutputDescription;


  DFBScreenOutputConfig = record
    flags : DFBScreenOutputConfigFlags;
    encoder : CInt;
    out_signals : DFBScreenOutputSignals;
    out_connectors : DFBScreenOutputConnectors;
    slow_blanking : DFBScreenOutputSlowBlankingSignals;
    resolution : DFBScreenOutputResolution;
  end;
  PDFBScreenOutputConfig = ^DFBScreenOutputConfig;


  DFBScreenEncoderDescription = record
    caps : DFBScreenEncoderCapabilities;
    type_ : DFBScreenEncoderType;
    tv_standards :  DFBScreenEncoderTVStandards;
    out_signals : DFBScreenOutputSignals;
    all_connectors : DFBScreenOutputConnectors;
    all_resolutions : DFBScreenOutputResolution;
    name_ : array [0..DFB_SCREEN_ENCODER_DESC_NAME_LENGTH-1] of CChar;
  end;
  PDFBScreenEncoderDescription = ^DFBScreenEncoderDescription;


  DFBScreenEncoderConfig = record
    flags : DFBScreenEncoderConfigFlags;
    tv_standard : DFBScreenEncoderTVStandards;
    test_picture : DFBScreenEncoderTestPicture;
    mixer : CInt;
    out_signals : DFBScreenOutputSignals;
    out_connectors : DFBScreenOutputConnectors;
    slow_blanking : DFBScreenOutputSlowBlankingSignals;
    scanmode : DFBScreenEncoderScanMode;
    test_color : DFBColor;
    adjustment : DFBColorAdjustment;
    frequency : DFBScreenEncoderFrequency;
    resolution : DFBScreenOutputResolution;
  end;
  PDFBScreenEncoderConfig = ^DFBScreenEncoderConfig;


   DFBVertex = record
    x : CFloat;
    y : CFloat;
    z : CFloat;
    w : CFloat;
    s : CFloat;
    t : CFloat;
  end;
  PDFBVertex = ^DFBVertex;


  DFBInputEvent = record
    clazz : DFBEventClass;
    type_ : DFBInputEventType;
    device_id : DFBInputDeviceID;
    flags : DFBInputEventFlags;
    timestamp : UnixType.timeval;

    key_code : CInt;
    key_id : DFBInputDeviceKeyIdentifier;
    key_symbol : DFBInputDeviceKeySymbol;
    modifiers : DFBInputDeviceModifierMask;
    locks : DFBInputDeviceLockState;

    button : DFBInputDeviceButtonIdentifier;
    buttons : DFBInputDeviceButtonMask;
    axis : DFBInputDeviceAxisIdentifier;
    axisabs :CInt;
    axisrel : CInt;
    min : CInt;
    max : CInt;
  end;
  PDFBInputEvent = ^DFBInputEvent;


  DFBWindowEvent = record
    clazz: DFBEventClass;
    type_ : DFBWindowEventType;
    flags : DFBWindowEventFlags;
    window_id : DFBWindowID;
    x : CInt;
    y : CInt;
    cx : CInt;
    cy : CInt;
    step : CInt;
    w : CInt;
    h : CInt;
    key_code : CInt;
    key_id : DFBInputDeviceKeyIdentifier;
    key_symbol : DFBInputDeviceKeySymbol;
    modifiers : DFBInputDeviceModifierMask;
    locks : DFBInputDeviceLockState;

    button : DFBInputDeviceButtonIdentifier;
    buttons : DFBInputDeviceButtonMask;
    timestamp : timeval;
  end;
  PDFBWindowEvent = ^DFBWindowEvent;


  DFBVideoProviderEvent = record
    clazz : DFBEventClass;
    type_ : DFBVideoProviderEventType;
    data_type : DFBVideoProviderEventDataSubType;
    data : array [0..3] of CInt;
  end;
  PDFBVideoProviderEvent = ^DFBVideoProviderEvent;

 
  DFBUserEvent = record
    clazz : DFBEventClass;
    type_ :CUInt;
    data : Pointer;
  end;
  PDFBUserEvent = ^DFBUserEvent;

 
  DFBUniversalEvent = record
    clazz : DFBEventClass;
    size : CUInt;
  end;
  PDFBUniversalEvent = ^DFBUniversalEvent;


  DFBEvent = record
    case Longint of
    0 : ( clazz : DFBEventClass );
    1 : ( input : DFBInputEvent );
    2 : ( window : DFBWindowEvent );
    3 : ( user : DFBUserEvent );
    4 : ( universal : DFBUniversalEvent );
    5 : ( videoprovider : DFBVideoProviderEvent );
  end;
  PDFBEvent = ^DFBEvent;


  DFBEventBufferStats = record
    num_events : CUInt;
    DFEC_INPUT : CUInt;
    DFEC_WINDOW : CUInt;
    DFEC_USER : CUInt;
    DFEC_UNIVERSAL : CUInt;
    DFEC_VIDEOPROVIDER : CUInt;
    DIET_KEYPRESS : CUInt;
    DIET_KEYRELEASE : CUInt;
    DIET_BUTTONPRESS : CUInt;
    DIET_BUTTONRELEASE : CUInt;
    DIET_AXISMOTION : CUInt;
    DWET_POSITION : CUInt;
    DWET_SIZE : CUInt;
    DWET_CLOSE : CUInt;
    DWET_DESTROYED : CUInt;
    DWET_GOTFOCUS : CUInt;
    DWET_LOSTFOCUS : CUInt;
    DWET_KEYDOWN : CUInt;
    DWET_KEYUP : CUInt;
    DWET_BUTTONDOWN : CUInt;
    DWET_BUTTONUP : CUInt;
    DWET_MOTION : CUInt;
    DWET_ENTER : CUInt;
    DWET_LEAVE : CUInt;
    DWET_WHEEL : CUInt;
    DWET_POSITION_SIZE : CUInt;
    DVPET_STARTED : CUInt;
    DVPET_STOPPED : CUInt;
    DVPET_SPEEDCHANGE : CUInt;
    DVPET_STREAMCHANGE : CUInt;
    DVPET_FATALERROR : CUInt;
    DVPET_FINISHED : CUInt;
    DVPET_SURFACECHANGE : CUInt;
    DVPET_FRAMEDECODED : CUInt;
    DVPET_FRAMEDISPLAYED : CUInt;
    DVPET_DATAEXHAUSTED : CUInt;
    DVPET_DATALOW : CUInt;
    DVPET_VIDEOACTION : CUInt;
    DVPET_DATAHIGH : CUInt;
    DVPET_BUFFERTIMELOW : CUInt;
    DVPET_BUFFERTIMEHIGH : CUInt;
  end;
  PDFBEventBufferStats = ^DFBEventBufferStats;


  DFBWindowGeometry = record
    mode : DFBWindowGeometryMode;
    rectangle : DFBRectangle;
    location : DFBLocation;
  end;
  PDFBWindowGeometry = ^DFBWindowGeometry;


  DFBImageDescription = record
    caps : DFBImageCapabilities;
    colorkey_r : U8;
    colorkey_g : U8;
    colorkey_b : U8;
  end;
  PDFBImageDescription = ^DFBImageDescription;


  
  video_struct_sd = record
    encoding : array [0..DFB_STREAM_DESC_ENCODING_LENGTH-1] of CChar;
    framerate : CDouble;
    aspect : CDouble;
    bitrate : CInt;
    afd : CInt;
    width : CInt;
    height : CInt;
  end;
  
  
  audio_struct_sd = record
    encoding : array [0..DFB_STREAM_DESC_ENCODING_LENGTH-1] of CChar;
    samplerate : CInt;
    channels : CInt;
    bitrate : CInt;
  end;
  
  
  DFBStreamDescription = record
    caps : DFBStreamCapabilities;
    video : video_struct_sd;
    audio : audio_struct_sd;
    title : array [0..DFB_STREAM_DESC_TITLE_LENGTH-1] of CChar;
    author : array [0..DFB_STREAM_DESC_AUTHOR_LENGTH-1] of CChar;
    album : array [0..DFB_STREAM_DESC_ALBUM_LENGTH-1] of CChar;
    year : CShort;
    genre : array [0..DFB_STREAM_DESC_GENRE_LENGTH-1] of CChar;
    comment : array [0..DFB_STREAM_DESC_COMMENT_LENGTH-1] of CChar;
  end;
  PDFBStreamDescription = ^DFBStreamDescription;
  

  video_struct_sa = record
    encoding : array [0..DFB_STREAM_DESC_ENCODING_LENGTH-1] of CChar;
    format : DFBStreamFormat;
  end;

  audio_struct_sa = record
    encoding : array [0..DFB_STREAM_DESC_ENCODING_LENGTH-1] of CChar;
    format : DFBStreamFormat;
  end;

  DFBStreamAttributes = record
    video : video_struct_sa;
    audio : audio_struct_sa;
  end;
  PDFBStreamAttributes = ^DFBStreamAttributes;
  

  video_struct_bo = record
    buffer_size : CUInt;
    minimum_level : CUInt;
    maximum_level : CUInt;
    current_level : CUInt;
  end;


  audio_struct_bo = record
    buffer_size : CUInt;
    minimum_level : CUInt;
    maximum_level : CUInt;
    current_level : CUInt;
  end;


  DFBBufferOccupancy = record
    valid : DFBStreamCapabilities;
    video : video_struct_bo;
    audio : audio_struct_bo;
  end;
  PDFBBufferOccupancy = ^DFBBufferOccupancy;


  video_struct_bt = record
    minimum_level : CUInt;
    maximum_level : CUInt;
    minimum_time : CUInt;
    maximum_time : CUInt;
  end;

  audio_struct_bt = record
    minimum_level : CUInt;
    maximum_level : CUInt;
    minimum_time : CUInt;
    maximum_time : CUInt;
  end;

  DFBBufferThresholds = record
    selection : DFBStreamCapabilities;
    video : video_struct_bt;
    audio : audio_struct_bt;
  end;
  PDFBBufferThresholds = ^DFBBufferThresholds;



{ Procedural types }

type
  DFBVideoModeCallback = function (width, height, bpp : CInt;
    callbackdata : Pointer) : PDFBEnumerationResult;


  DFBScreenCallback = function (
    screen_id : DFBScreenID;
    desc : DFBScreenDescription;
    callbackdata : Pointer
  ) :  PDFBEnumerationResult;


  DFBDisplayLayerCallback = function (
    layer_id : DFBDisplayLayerID;
    desc : DFBDisplayLayerDescription;
    callbackdata : Pointer
  ) : PDFBEnumerationResult;


  DFBInputDeviceCallback = function (
    device_id : DFBInputDeviceID;
    desc : DFBInputDeviceDescription;
    callbackdata : Pointer
  ) : PDFBEnumerationResult;


  DFBGetDataCallback = function (
    buffer : Pointer;
    length : CUint;
    callbackdata : Pointer
  ) : PDFBEnumerationResult;


  DFBTextEncodingCallback = function (
    encoding_id : DFBTextEncodingID;
    name_ : PChar;
    context : Pointer
  ) : PDFBEnumerationResult;


  DIRenderCallback = function(rect : PDFBRectangle; ctx : Pointer
    ) : PDIRenderCallbackResult;


  DVFrameCallback = function(ctx : Pointer) : Pointer;



{ Variables }

var
  directfb_major_version : DWord; CVar; external;
  directfb_minor_version : DWord; CVar; external;
  directfb_micro_version : DWord; CVar; external;
  directfb_binary_age : DWord; CVar; external;
  directfb_interface_age : DWord; CVar; external;


{ "Interfaces" }


type

  { DONT BREAK THIS LONG TYPE BLOCK, OR TYPE FORWARDING WONT WORK! }

  PIDirectFBPalette = ^IDirectFBPalette;
  PPIDirectFBPalette = ^PIDirectFBPalette;

  PIDirectFBSurface = ^IDirectFBSurface;
  PPIDirectFBSurface = ^PIDirectFBSurface;

  PIDirectFBInputDevice = ^IDirectFBInputDevice;
  PPIDirectFBInputDevice = ^PIDirectFBInputDevice;
  
  PIDirectFBDisplayLayer = ^IDirectFBDisplayLayer;
  PPIDirectFBDisplayLayer = ^PIDirectFBDisplayLayer;

  PIDirectFBWindow = ^IDirectFBWindow;
  PPIDirectFBWindow = ^PIDirectFBWindow;

  PIDirectFBEventBuffer = ^IDirectFBEventBuffer;
  PPIDirectFBEventBuffer = ^PIDirectFBEventBuffer;
  
  PIDirectFBFont = ^IDirectFBFont;
  PPIDirectFBFont = ^PIDirectFBFont;

  PIDirectFBImageProvider = ^IDirectFBImageProvider;
  PPIDirectFBImageProvider= ^PIDirectFBImageProvider;

  PIDirectFBVideoProvider = ^IDirectFBVideoProvider;
  PPIDirectFBVideoProvider = ^PIDirectFBVideoProvider;

  PIDirectFBDataBuffer = ^IDirectFBDataBuffer;
  PPIDirectFBDataBuffer = ^PIDirectFBDataBuffer;

  PIDirectFBScreen = ^IDirectFBScreen;
  PPIDirectFBScreen = ^PIDirectFBScreen;

  PIDirectFBGL = ^IDirectFBGL;
  PPIDirectFBGL = ^PIDirectFBGL;

  PIDirectFB = ^IDirectFB;
  PPIDirectFB = ^PIDirectFB;


  { Interface for read/write access to the colors of a palette object and for cloning it. }

  IDirectFBPalette = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBPalette ) : DFBResult;
    Release : function( thiz : PIDirectFBPalette ) : DFBResult;


    GetCapabilities : function(
      thiz : PIDirectFBPalette;
      ret_caps : PDFBPaletteCapabilities
    ) : DFBResult;

    GetSize : function(
      thiz : PIDirectFBPalette;
      ret_size : PCUInt
    ) : DFBResult;



    SetEntries : function(
      thiz : PIDirectFBPalette;
      entries : PDFBColor;
      num_entries : CUInt;
      offset : CUInt
    ) : DFBResult;

    GetEntries : function(
      thiz : PIDirectFBPalette;
      ret_entries : PDFBColor;
      num_entries : CUInt;
      offset : CUInt
    ) : DFBResult;

    FindBestMatch : function(
      thiz : PIDirectFBPalette;
      r : U8;
      g : U8;
      b : U8;
      a : U8;
      ret_index : PCUInt
    ) : DFBResult;



    CreateCopy : function(
      thiz : PIDirectFBPalette;
      ret_interface : PPIDirectFBPalette
    ) : DFBResult;



    SetEntriesYUV : function(
      thiz : PIDirectFBPalette;
      entries : PDFBColorYUV;
      num_entries : CUInt;
      offset : CUInt
    ) : DFBResult;

    GetEntriesYUV : function(
      thiz : PIDirectFBPalette;
      ret_entries : PDFBColorYUV;
      num_entries: CUInt;
      offset : CUInt
    ) : DFBResult;

    FindBestMatchYUV : function(
      thiz : PIDirectFBPalette;
      y : U8;
      u : U8;
      v : U8;
      a : U8;
      ret_index : PCUInt
    ) : DFBResult;

  end;


  { Interface to a surface object, being a graphics context for rendering and state control,
    buffer operations, palette access and sub area translate'n'clip logic. }

  IDirectFBSurface = record
  

    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBSurface ) : DFBResult;
    Release : function( thiz : PIDirectFBSurface ) : DFBResult;
  

    GetCapabilities : function(
      thiz : PIDirectFBSurface;
      ret_caps : PDFBSurfaceCapabilities
    ) : DFBResult;

    GetPosition : function(
      thiz : PIDirectFBSurface;
      ret_x : PCInt;
      ret_y : PCInt
    ) : DFBResult;

    GetSize : function(
      thiz : PIDirectFBSurface;
      ret_width : PCInt;
      ret_height : PCInt
    ) : DFBResult;


    GetVisibleRectangle : function(
      thiz : PIDirectFBSurface;
      ret_rect : PDFBRectangle
    ) : DFBResult;

    GetPixelFormat : function(
      thiz : PIDirectFBSurface;
      ret_format : PDFBSurfacePixelFormat
    ) : DFBResult;


    GetAccelerationMask : function(
      thiz : PIDirectFBSurface;
      source : PIDirectFBSurface;
      ret_mask : PDFBAccelerationMask
    ) : DFBResult;




    GetPalette : function(
      thiz : PIDirectFBSurface;
      ret_interface : PPIDirectFBPalette
    ) : DFBResult;

    SetPalette : function(
      thiz : PIDirectFBSurface;
      palette : PIDirectFBPalette
    ) : DFBResult;


    SetAlphaRamp : function(
      thiz : PIDirectFBSurface;
      a0 : U8;
      a1 : U8;
      a2 : U8;
      a3 : U8
    ) : DFBResult;




    Lock : function(
      thiz : PIDirectFBSurface;
      flags : DFBSurfaceLockFlags;
      ret_ptr : PPointer;
      ret_pitch : PCInt
    ) : DFBResult;


    GetFramebufferOffset : function(
      thiz : PIDirectFBSurface;
      offset : PCInt
    ) : DFBResult;

    Unlock : function(
      thiz : PIDirectFBSurface
    ) : DFBResult;


    Flip : function(
      thiz : PIDirectFBSurface;
      region : PDFBRegion;
      flags : DFBSurfaceFlipFlags
    ) : DFBResult;


    SetField : function(
      thiz : PIDirectFBSurface;
      field : CInt
    ) : DFBResult;



    Clear : function(
      thiz : PIDirectFBSurface;
      r : U8;
      g : U8;
      b : U8;
      a : U8
    ) : DFBResult;



    SetClip : function(
      thiz : PIDirectFBSurface;
      clip : PDFBRegion
    ) : DFBResult;

    GetClip : function(
      thiz : PIDirectFBSurface;
      ret_clip : PDFBRegion
    ) : DFBResult;


    SetColor : function(
      thiz : PIDirectFBSurface;
      r : U8;
      g : U8;
      b : U8;
      a : U8
    ) : DFBResult;


    SetColorIndex : function(
      thiz : PIDirectFBSurface;
      index_ : CUInt
    ) : DFBResult;

    SetSrcBlendFunction : function(
      thiz : PIDirectFBSurface;
      function_ : DFBSurfaceBlendFunction
    ) : DFBResult;

    SetDstBlendFunction : function(
      thiz : PIDirectFBSurface;
      function_ : DFBSurfaceBlendFunction
    ) : DFBResult;

    SetPorterDuff : function(
      thiz : PIDirectFBSurface;
      rule : DFBSurfacePorterDuffRule
    ) : DFBResult;

    SetSrcColorKey : function(
      thiz : PIDirectFBSurface;
      r : U8;
      g : U8;
      b : U8
    ) : DFBResult;


    SetSrcColorKeyIndex : function(
      thiz : PIDirectFBSurface;
      index_ : CUInt
    ) : DFBResult;

    SetDstColorKey : function(
      thiz : PIDirectFBSurface;
      r : U8;
      g : U8;
      b : U8
    ) : DFBResult;


    SetDstColorKeyIndex : function(
      thiz : PIDirectFBSurface;
      index_ : CUInt
    ) : DFBResult;


    SetBlittingFlags : function(
      thiz : PIDirectFBSurface;
      flags : DFBSurfaceBlittingFlags
    ) : DFBResult;


    Blit : function(
      thiz : PIDirectFBSurface;
      source : PIDirectFBSurface;
      source_rect : PDFBRectangle;
      x : CInt;
      y : CInt
    ) : DFBResult;


    TileBlit : function(
      thiz : PIDirectFBSurface;
      source : PIDirectFBSurface;
      source_rect : PDFBRectangle;
      x : CInt;
      y : CInt
    ) : DFBResult;


    BatchBlit : function(
      thiz : PIDirectFBSurface;
      source : PIDirectFBSurface;
      source_rects : PDFBRectangle;
      dest_points : PDFBPoint;
      num : CInt
    ) : DFBResult;


    StretchBlit : function(
      thiz : PIDirectFBSurface;
      source : PIDirectFBSurface;
      source_rect : PDFBRectangle;
      destination_rect : PDFBRectangle
    ) : DFBResult;


    TextureTriangles : function(
      thiz : PIDirectFBSurface;
      texture : PIDirectFBSurface;
      vertices : PDFBVertex;
      indices : PCInt;
      num : CInt;
      formation : DFBTriangleFormation
    ) : DFBResult;


    SetDrawingFlags : function(
      thiz : PIDirectFBSurface;
      flags : DFBSurfaceDrawingFlags
    ) : DFBResult;

    FillRectangle : function(
      thiz : PIDirectFBSurface;
      x : CInt;
      y : CInt;
      w : CInt;
      h : CInt
    ) : DFBResult;

    DrawRectangle : function(
      thiz : PIDirectFBSurface;
      x : CInt;
      y : CInt;
      w : CInt;
      h : CInt
    ) : DFBResult;

    DrawLine : function(
      thiz : PIDirectFBSurface;
      x1 : CInt;
      y1 : CInt;
      x2 : CInt;
      y2 : CInt
    ) : DFBResult;

    DrawLines : function(
      thiz : PIDirectFBSurface;
      lines : PDFBRegion;
      num_lines : CUInt
    ) : DFBResult;

    FillTriangle : function(
      thiz : PIDirectFBSurface;
      x1 : CInt;
      y1 : CInt;
      x2 : CInt;
      y2 : CInt;
      x3 : CInt;
      y3 : CInt
    ) : DFBResult;


    FillRectangles : function(
      thiz : PIDirectFBSurface;
      rects : PDFBRectangle;
      num : CUInt
    ) : DFBResult;


    FillSpans : function(
      thiz : PIDirectFBSurface;
      y : CInt;
      spans : PDFBSpan;
      num : CUInt
    ) : DFBResult;
     

    FillTriangles : function(
      thiz : PIDirectFBSurface;
      tris : PDFBTriangle;
      num : CUInt
    ) : DFBResult;



    SetFont : function(
      thiz : PIDirectFBSurface;
      font : PIDirectFBFont
    ) : DFBResult;


    GetFont : function(
      thiz : PIDirectFBSurface;
      ret_font : PPIDirectFBFont
    ) : DFBResult;


    DrawString : function(
      thiz : PIDirectFBSurface;
      text : PChar;
      bytes : CInt;
      x : CInt;
      y : CInt;
      flags : DFBSurfaceTextFlags
    ) : DFBResult;



    DrawGlyph : function(
      thiz : PIDirectFBSurface;
      character : CUInt;
      x : CInt;
      y : CInt;
      flags : DFBSurfaceTextFlags
    ) : DFBResult;

    SetEncoding : function(
      thiz : PIDirectFBSurface;
      encoding : DFBTextEncodingID
    ) : DFBResult;



    GetSubSurface : function(
      thiz : PIDirectFBSurface;
      rect : PDFBRectangle;
      ret_interface : PPIDirectFBSurface
    ) : DFBResult;


    GetGL : function(
      thiz : PIDirectFBSurface;
      ret_interface : PPIDirectFBGL
    ) : DFBResult;



    Dump : function(
      thiz : PIDirectFBSurface;
      directory : PChar;
      prefix : PChar
    ) : DFBResult;


    DisableAcceleration : function(
      thiz : PIDirectFBSurface;
      mask : DFBAccelerationMask
    ) : DFBResult;


    ReleaseSource : function(
      thiz : PIDirectFBSurface
    ) : DFBResult;



    SetIndexTranslation : function(
      thiz : PIDirectFBSurface;
      indices : PCInt;
      num_indices : CInt
    ) : DFBResult;


    SetRenderOptions : function(
      thiz : PIDirectFBSurface;
      options : DFBSurfaceRenderOptions
    ) : DFBResult;


    SetMatrix : function(
      thiz : PIDirectFBSurface;
      matrix : PS32
    ) : DFBResult;


    SetSourceMask : function(
      thiz : PIDirectFBSurface;
      mask : PIDirectFBSurface;
      x : CInt;
      y : CInt;
      flags : DFBSurfaceMaskFlags
    ) : DFBResult;


    MakeSubSurface : function(
      thiz : PIDirectFBSurface;
      from : PIDirectFBSurface;
      rect : PDFBRectangle
    ) : DFBResult;


    Write_ : function(
      thiz : PIDirectFBSurface;
      rect : PDFBRectangle;
      ptr : Pointer;
      pitch : CInt
     ) : DFBResult;


    Read_ : function(
      thiz : PIDirectFBSurface;
      rect : PDFBRectangle;
      ptr : Pointer;
      pitch : CInt
    ) : DFBResult;

    SetColors : function(
      thiz : PIDirectFBSurface;
      ids : PDFBColorID;
      colors : PDFBColor;
      num : CUInt
    ) : DFBResult;

    BatchBlit2 : function(
      thiz : PIDirectFBSurface;
      source : PIDirectFBSurface;
      source2 : PIDirectFBSurface;
      source_rects : PDFBRectangle;
      dest_points : PDFBPoint;
      source2_points : PDFBPoint;
      num : CInt
    ) : DFBResult;

  end;


  { Input device interface for keymap access, event buffers and state queries. }
  
  IDirectFBInputDevice = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBInputDevice ) : DFBResult;
    Release : function( thiz : PIDirectFBInputDevice ) : DFBResult;


    GetID : function(
      thiz : PIDirectFBInputDevice;
      ret_device_id : PDFBInputDeviceID
    ) : DFBResult;

    GetDescription : function(
      thiz : PIDirectFBInputDevice;
      ret_desc : PDFBInputDeviceDescription
    ) : DFBResult;



    GetKeymapEntry : function(
      thiz : PIDirectFBInputDevice;
      keycode : CInt;
      ret_entry : PDFBInputDeviceKeymapEntry
    ) : DFBResult;



    CreateEventBuffer : function(
      thiz : PIDirectFBInputDevice;
      ret_buffer : PPIDirectFBEventBuffer
    ) : DFBResult;

    IDAttachEventBuffer : function(
      thiz : PIDirectFBInputDevice;
      buffer : PIDirectFBEventBuffer
    ) : DFBResult;
     
    DetachEventBuffer : function(
      thiz : PIDirectFBInputDevice;
      buffer : PIDirectFBEventBuffer
    ) : DFBResult;



    GetKeyState : function(
      thiz : PIDirectFBInputDevice;
      key_id : DFBInputDeviceKeyIdentifier;
      ret_state : PDFBInputDeviceKeyState
    ) : DFBResult;

    GetModifiers : function(
      thiz : PIDirectFBInputDevice;
      ret_modifiers : PDFBInputDeviceModifierMask
    ) : DFBResult;

    GetLockState : function(
      thiz : PIDirectFBInputDevice;
      ret_locks : PDFBInputDeviceLockState
    ) : DFBResult;

    GetButtons : function(
      thiz : PIDirectFBInputDevice;
      ret_buttons : PDFBInputDeviceButtonMask
    ) : DFBResult;

    GetButtonState : function(
      thiz : PIDirectFBInputDevice;
      button : DFBInputDeviceButtonIdentifier;
      ret_state : PDFBInputDeviceButtonState
    ) : DFBResult;

    GetAxis : function(
      thiz : PIDirectFBInputDevice;
      axis : DFBInputDeviceAxisIdentifier;
      ret_pos : PCInt
    ) : DFBResult;



    GetXY : function(
      thiz : PIDirectFBInputDevice;
      ret_x : PCInt;
      ret_y : PCInt
    ) : DFBResult;

  end;
  
  
  { Layer interface for configuration, window stack usage or direct surface access, with shared/exclusive context. }
  
  IDirectFBDisplayLayer = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBDisplayLayer ) : DFBResult;
    Release : function( thiz : PIDirectFBDisplayLayer ) : DFBResult;


    GetID : function(
      thiz : PIDirectFBDisplayLayer;
      ret_layer_id : PDFBDisplayLayerID
    ) : DFBResult;

    GetDescription : function(
      thiz : PIDirectFBDisplayLayer;
      ret_desc : PDFBDisplayLayerDescription
    ) : DFBResult;

    GetSourceDescriptions : function(
      thiz : PIDirectFBDisplayLayer;
      ret_descriptions : PDFBDisplayLayerSourceDescription
    ) : DFBResult;

    GetCurrentOutputField : function(
      thiz : PIDirectFBDisplayLayer;
      ret_field : PCInt
    ) : DFBResult;



    GetSurface : function(
      thiz : PIDirectFBDisplayLayer;
      ret_interface : PPIDirectFBSurface
    ) : DFBResult;

    GetScreen : function(
      thiz : PIDirectFBDisplayLayer;
      ret_interface : PPIDirectFBScreen
    ) : DFBResult;



    SetCooperativeLevel : function(
      thiz : PIDirectFBDisplayLayer;
      level : DFBDisplayLayerCooperativeLevel
    ) : DFBResult;

    GetConfiguration : function(
      thiz : PIDirectFBDisplayLayer;
      ret_config : PDFBDisplayLayerConfig
    ) : DFBResult;

    TestConfiguration : function(
      thiz : PIDirectFBDisplayLayer;
      config : PDFBDisplayLayerConfig;
      ret_failed : PDFBDisplayLayerConfigFlags
    ) : DFBResult;

    SetConfiguration : function(
      thiz : PIDirectFBDisplayLayer;
      config : PDFBDisplayLayerConfig
    ) : DFBResult;



    SetScreenLocation : function(
      thiz : PIDirectFBDisplayLayer;
      x : CFloat;
      y : CFloat;
      width : CFloat;
      height : CFloat
    ) : DFBResult;

    SetScreenPosition : function(
      thiz : PIDirectFBDisplayLayer;
      x : CInt;
      y : CInt
    ) : DFBResult;

    SetScreenRectangle : function(
      thiz : PIDirectFBDisplayLayer;
      x : CInt;
      y : CInt;
      width : CInt;
      height : CInt
    ) : DFBResult;



    SetOpacity : function(
      thiz : PIDirectFBDisplayLayer;
      opacity : U8
    ) : DFBResult;

    SetSourceRectangle : function(
      thiz : PIDirectFBDisplayLayer;
      x : CInt;
      y : CInt;
      width : CInt;
      height : CInt
    ) : DFBResult;

    SetFieldParity : function(
      thiz : PIDirectFBDisplayLayer;
      field : CInt
    ) : DFBResult;

    SetClipRegions : function(
      thiz : PIDirectFBDisplayLayer;
      regions : PDFBRegion;
      num_regions : CInt;
      positive : DFBBoolean
    ) : DFBResult;



    SetSrcColorKey : function(
      thiz : PIDirectFBDisplayLayer;
      r : U8;
      g : U8;
      b : U8
    ) : DFBResult;

    SetDstColorKey : function(
      thiz : PIDirectFBDisplayLayer;
      r : U8;
      g : U8;
      b : U8
    ) : DFBResult;



    GetLevel : function(
      thiz : PIDirectFBDisplayLayer;
      ret_level : PCInt
    ) : DFBResult;

    SetLevel : function(
      thiz : PIDirectFBDisplayLayer;
      level : CInt
    ) : DFBResult;



    SetBackgroundMode : function(
      thiz : PIDirectFBDisplayLayer;
      mode : DFBDisplayLayerBackgroundMode
    ) : DFBResult;

    SetBackgroundImage : function(
      thiz : PIDirectFBDisplayLayer;
      surface : PIDirectFBSurface
    ) : DFBResult;

    SetBackgroundColor : function(
      thiz : PIDirectFBDisplayLayer;
      r : U8;
      g : U8;
      b : U8;
      a : U8
    ) : DFBResult;


    GetColorAdjustment : function(
      thiz : PIDirectFBDisplayLayer;
      ret_adj : PDFBColorAdjustment
    ) : DFBResult;

    SetColorAdjustment : function(
      thiz : PIDirectFBDisplayLayer;
      adj : PDFBColorAdjustment
    ) : DFBResult;



    CreateWindow : function(
      thiz : PIDirectFBDisplayLayer;
      desc : PDFBWindowDescription;
      ret_interface : PPIDirectFBWindow
    ) : DFBResult;

    GetWindow : function(
      thiz : PIDirectFBDisplayLayer;
      window_id : DFBWindowID;
      ret_interface : PPIDirectFBWindow
    ) : DFBResult;



    EnableCursor : function(
      thiz : PIDirectFBDisplayLayer;
      enable : CInt
    ) : DFBResult;

    GetCursorPosition : function(
      thiz : PIDirectFBDisplayLayer;
      ret_x : PCInt;
      ret_y : PCInt
    ) : DFBResult;

    WarpCursor : function(
      thiz : PIDirectFBDisplayLayer;
      x : CInt;
      y : CInt
    ) : DFBResult;

    SetCursorAcceleration : function(
      thiz : PIDirectFBDisplayLayer;
      numerator : CInt;
      denominator : CInt;
      threshold : CInt
    ) : DFBResult;

    SetCursorShape : function(
      thiz : PIDirectFBDisplayLayer;
      shape : PIDirectFBSurface;
      hot_x : CInt;
      hot_y : CInt
    ) : DFBResult;

    SetCursorOpacity : function(
      thiz : PIDirectFBDisplayLayer;
      opacity : U8
    ) : DFBResult;



    WaitForSync : function(
      thiz : PIDirectFBDisplayLayer
    ) : DFBResult;



    SwitchContext : function(
      thiz : PIDirectFBDisplayLayer;
      exclusive : DFBBoolean
    ) : DFBResult;



    SetRotation : function(
      thiz : PIDirectFBDisplayLayer;
      rotation : CInt
    ) : DFBResult;

    GetRotation : function(
      thiz : PIDirectFBDisplayLayer;
      ret_rotation : PCInt
    ) : DFBResult;

  end;
  
  
  { Interface to a window object, controlling appearance and focus, positioning and stacking,
    event buffers and surface access. }
  
  IDirectFBWindow = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBWindow ) : DFBResult;
    Release : function( thiz : PIDirectFBWindow ) : DFBResult;


    GetID : function(
      thiz : PIDirectFBWindow;
      ret_window_id : PDFBWindowID
    ) : DFBResult;

    GetPosition : function(
      thiz : PIDirectFBWindow;
      ret_x : PCInt;
      ret_y : PCInt
    ) : DFBResult;

    GetSize : function(
      thiz : PIDirectFBWindow;
      ret_width : PCInt;
      ret_height : PCInt
    ) : DFBResult;



    Close : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    Destroy : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;



    GetSurface : function(
      thiz : PIDirectFBWindow;
      ret_surface : PPIDirectFBSurface
    ) : DFBResult;

    ResizeSurface : function(
      thiz : PIDirectFBWindow;
      width : CInt;
      height : CInt
    ) : DFBResult;



    CreateEventBuffer : function(
      thiz : PIDirectFBWindow;
      ret_buffer : PPIDirectFBEventBuffer
    ) : DFBResult;

    AttachEventBuffer : function(
      thiz : PIDirectFBWindow;
      buffer : PIDirectFBEventBuffer
    ) : DFBResult;
     
    DetachEventBuffer : function(
      thiz : PIDirectFBWindow;
      buffer : PIDirectFBEventBuffer
    ) : DFBResult;

    EnableEvents : function(
      thiz : PIDirectFBWindow;
      mask : DFBWindowEventType
    ) : DFBResult;

    DisableEvents : function(
      thiz : PIDirectFBWindow;
      mask : DFBWindowEventType
    ) : DFBResult;



    SetOptions : function(
      thiz : PIDirectFBWindow;
      options : DFBWindowOptions
    ) : DFBResult;

    GetOptions : function(
      thiz : PIDirectFBWindow;
      ret_options : PDFBWindowOptions
    ) : DFBResult;

    SetColor : function(
      thiz : PIDirectFBWindow;
      r : U8;
      g : U8;
      b : U8;
      a : U8
    ) : DFBResult;

    SetColorKey : function(
      thiz : PIDirectFBWindow;
      r : U8;
      g : U8;
      b : U8
    ) : DFBResult;

    SetColorKeyIndex : function(
      thiz : PIDirectFBWindow;
      index_ : CUInt
    ) : DFBResult;

    SetOpacity : function(
      thiz : PIDirectFBWindow;
      opacity : U8
    ) : DFBResult;

    SetOpaqueRegion : function(
      thiz : PIDirectFBWindow;
      x1 : CInt;
      y1 : CInt;
      x2 : CInt;
      y2 : CInt
    ) : DFBResult;

    GetOpacity : function(
      thiz : PIDirectFBWindow;
      ret_opacity : PU8
    ) : DFBResult;

    SetCursorShape : function(
      thiz : PIDirectFBWindow;
      shape : PIDirectFBSurface;
      hot_x : CInt;
      hot_y : CInt
    ) : DFBResult;

    Move : function(
      thiz : PIDirectFBWindow;
      dx : CInt;
      dy : CInt
    ) : DFBResult;

    MoveTo : function(
      thiz : PIDirectFBWindow;
      x : CInt;
      y : CInt
    ) : DFBResult;

    Resize : function(
      thiz : PIDirectFBWindow;
      width : CInt;
      height : CInt
    ) : DFBResult;

    SetBounds : function(
      thiz : PIDirectFBWindow;
      x : CInt;
      y : CInt;
      width : CInt;
      height : CInt
    ) : DFBResult;

    SetStackingClass : function(
      thiz : PIDirectFBWindow;
      stacking_class : DFBWindowStackingClass
    ) : DFBResult;

    Raise_ : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    Lower : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    RaiseToTop : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    LowerToBottom : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    PutAtop : function(
      thiz : PIDirectFBWindow;
      lower : PIDirectFBWindow
    ) : DFBResult;

    PutBelow : function(
      thiz : PIDirectFBWindow;
      upper : PIDirectFBWindow
    ) : DFBResult;

    Bind : function(
      thiz : PIDirectFBWindow;
      window : PIDirectFBWindow;
      x : CInt;
      y : CInt
    ) : DFBResult;

    Unbind : function(
      thiz : PIDirectFBWindow;
      window : PIDirectFBWindow
    ) : DFBResult;

    RequestFocus : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    GrabKeyboard : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    UngrabKeyboard : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    GrabPointer : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    UngrabPointer : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    GrabKey : function(
      thiz : PIDirectFBWindow;
      symbol : DFBInputDeviceKeySymbol;
      modifiers : DFBInputDeviceModifierMask
    ) : DFBResult;

    UngrabKey : function(
      thiz : PIDirectFBWindow;
      symbol : DFBInputDeviceKeySymbol;
      modifiers : DFBInputDeviceModifierMask
    ) : DFBResult;

    SetKeySelection : function(
      thiz : PIDirectFBWindow;
      selection : DFBWindowKeySelection;
      keys : PDFBInputDeviceKeySymbol;
      num_keys : CUInt
    ) : DFBResult;

    GrabUnselectedKeys : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    UngrabUnselectedKeys : function(
      thiz : PIDirectFBWindow
    ) : DFBResult;

    SetSrcGeometry : function(
      thiz : PIDirectFBWindow;
      geometry : PDFBWindowGeometry
    ) : DFBResult;

    SetDstGeometry : function(
      thiz : PIDirectFBWindow;
      geometry : PDFBWindowGeometry
    ) : DFBResult;

    SetProperty : function(
      thiz : PIDirectFBWindow;
      key : PChar;
      value : Pointer;
      ret_old_value : PPointer
    ) : DFBResult;

    GetProperty : function(
      thiz : PIDirectFBWindow;
      key : PChar;
      ret_value : PPointer
    ) : DFBResult;

    RemoveProperty : function(
      thiz : PIDirectFBWindow;
      key : PChar;
      ret_value : PPointer
    ) : DFBResult;

    SetRotation : function(
      thiz : PIDirectFBWindow;
      rotation : CInt
    ) : DFBResult;

    SetAssociation : function(
      thiz : PIDirectFBWindow;
      window_id : DFBWindowID
    ) : DFBResult;

    SetApplicationID : function(
      thiz : PIDirectFBWindow;
      application_id : CULong
    ) : DFBResult;

    GetApplicationID : function(
      thiz : PIDirectFBWindow;
      ret_application_id : PCULong
    ) : DFBResult;

    BeginUpdates : function(
      thiz : PIDirectFBWindow;
      update : PDFBRegion
    ) : DFBResult;

  end;


  { Interface to a local event buffer to send/receive events, wait for events, abort waiting or reset buffer. }
  
  IDirectFBEventBuffer = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBEventBuffer ) : DFBResult;
    Release : function( thiz : PIDirectFBEventBuffer ) : DFBResult;


    Reset : function(
      thiz : PIDirectFBEventBuffer
    ) : DFBResult;



    WaitForEvent : function(
      thiz : PIDirectFBEventBuffer
    ) : DFBResult;

    WaitForEventWithTimeout : function(
      thiz : PIDirectFBEventBuffer;
      seconds : CUInt;
      milli_seconds : CUInt
    ) : DFBResult;



    GetEvent : function(
      thiz : PIDirectFBEventBuffer;
      ret_event : PDFBEvent
    ) : DFBResult;

    PeekEvent : function(
      thiz : PIDirectFBEventBuffer;
      ret_event : PDFBEvent
    ) : DFBResult;

    HasEvent : function(
      thiz : PIDirectFBEventBuffer
    ) : DFBResult;



    PostEvent : function(
      thiz : PIDirectFBEventBuffer;
      event : PDFBEvent
    ) : DFBResult;

    WakeUp : function(
      thiz : PIDirectFBEventBuffer
    ) : DFBResult;



    CreateFileDescriptor : function(
      thiz : PIDirectFBEventBuffer;
      ret_fd : PCInt
    ) : DFBResult;



    EnableStatistics : function(
      thiz : PIDirectFBEventBuffer;
      enable : DFBBoolean
    ) : DFBResult;

    GetStatistics : function(
      thiz : PIDirectFBEventBuffer;
      ret_stats : PDFBEventBufferStats
    ) : DFBResult;

  end;
  

  { Font interface for getting metrics, measuring strings or single characters, query/choose encodings. }
  
  IDirectFBFont = record  


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBFont ) : DFBResult;
    Release : function( thiz : PIDirectFBFont ) : DFBResult;


    GetAscender : function(
      thiz : PIDirectFBFont;
      ret_ascender : PCInt
    ) : DFBResult;

    GetDescender : function(
      thiz : PIDirectFBFont;
      ret_descender : PCInt
    ) : DFBResult;

    GetHeight : function(
      thiz : PIDirectFBFont;
      ret_height : PCInt
    ) : DFBResult;

    GetMaxAdvance : function(
      thiz : PIDirectFBFont;
      ret_maxadvance : PCInt
    ) : DFBResult;

    GetKerning : function(
      thiz : PIDirectFBFont;
      prev : CUInt;
      current : CUInt;
      ret_kern_x : PCInt;
      ret_kern_y : PCInt
    ) : DFBResult;


    GetStringWidth : function(
      thiz : PIDirectFBFont;
      text : PChar;
      bytes : CInt;
      ret_width : PCInt
    ) : DFBResult;

    GetStringExtents : function(
      thiz : PIDirectFBFont;
      text : PChar;
      bytes : CInt;
      ret_logical_rect : PDFBRectangle;
      ret_ink_rect : PDFBRectangle
    ) : DFBResult;

    GetGlyphExtents : function(
      thiz : PIDirectFBFont;
      character : CUInt;
      ret_rect : PDFBRectangle;
      ret_advance : PCInt
    ) : DFBResult;

    GetStringBreak : function(
      thiz : PIDirectFBFont;
      text : PChar;
      bytes : CInt;
      max_width : CInt;
      ret_width : PCInt;
      ret_str_length : PCInt;
      ret_next_line : PPChar
    ) : DFBResult;


    SetEncoding : function(
      thiz : PIDirectFBFont;
      encoding : DFBTextEncodingID
    ) : DFBResult;

    EnumEncodings : function(
      thiz : PIDirectFBFont;
      callback : DFBTextEncodingCallback;
      context : Pointer
    ) : DFBResult;

    FindEncoding : function(
      thiz : PIDirectFBFont;
      name : PChar;
      ret_encoding : PDFBTextEncodingID
    ) : DFBResult;

  end;


  { Interface to an image provider, retrieving information about the image and rendering it to a surface. }
  
  IDirectFBImageProvider = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBImageProvider ) : DFBResult;
    Release : function( thiz : PIDirectFBImageProvider ) : DFBResult;


    GetSurfaceDescription : function(
      thiz : PIDirectFBImageProvider;
      ret_dsc : PDFBSurfaceDescription
    ) : DFBResult;

    GetImageDescription : function(
      thiz : PIDirectFBImageProvider;
      ret_dsc : PDFBImageDescription
    ) : DFBResult;



    RenderTo : function(
      thiz : PIDirectFBImageProvider;
      destination : PIDirectFBSurface;
      destination_rect : PDFBRectangle
    ) : DFBResult;

    SetRenderCallback : function(
      thiz : PIDirectFBImageProvider;
      callback : DIRenderCallback;
      callback_data : Pointer
    ) : DFBResult;



    WriteBack : function(
      thiz : PIDirectFBImageProvider;
      surface : PIDirectFBSurface;
      src_rect : PDFBRectangle;
      filename : PChar
    ) : DFBResult;

  end;


  { Interface to a video provider for playback with advanced control and basic stream information. }

  IDirectFBVideoProvider = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBVideoProvider ) : DFBResult;
    Release : function( thiz : PIDirectFBVideoProvider ) : DFBResult;


    GetCapabilities : function(
      thiz : PIDirectFBVideoProvider;
      ret_caps : PDFBVideoProviderCapabilities
    ) : DFBResult;

    GetSurfaceDescription : function(
      thiz : PIDirectFBVideoProvider;
      ret_dsc : PDFBSurfaceDescription
    ) : DFBResult;

    GetStreamDescription : function(
      thiz : PIDirectFBVideoProvider;
      ret_dsc : PDFBStreamDescription
    ) : DFBResult;



    PlayTo : function(
      thiz : PIDirectFBVideoProvider;
      destination : PIDirectFBSurface;
      destination_rect : PDFBRectangle;
      callback : DVFrameCallback;
      ctx : Pointer
    ) : DFBResult;

    Stop : function(
      thiz : PIDirectFBVideoProvider
    ) : DFBResult;

    GetStatus : function(
      thiz : PIDirectFBVideoProvider;
      ret_status : PDFBVideoProviderStatus
    ) : DFBResult;



    SeekTo : function(
      thiz : PIDirectFBVideoProvider;
      seconds : CDouble
    ) : DFBResult;

    GetPos : function(
      thiz : PIDirectFBVideoProvider;
      ret_seconds : PCDouble
    ) : DFBResult;

    GetLength : function(
      thiz : PIDirectFBVideoProvider;
      ret_seconds : PCDouble
    ) : DFBResult;


    GetColorAdjustment : function(
      thiz : PIDirectFBVideoProvider;
      ret_adj : PDFBColorAdjustment
    ) : DFBResult;

    SetColorAdjustment : function(
      thiz : PIDirectFBVideoProvider;
      adj : PDFBColorAdjustment
    ) : DFBResult;


    SendEvent : function(
      thiz : PIDirectFBVideoProvider;
      event : PDFBEvent
    ) : DFBResult;

   
    SetPlaybackFlags : function(
      thiz : PIDirectFBVideoProvider;
      flags : DFBVideoProviderPlaybackFlags
    ) : DFBResult;
     
    SetSpeed : function(
      thiz : PIDirectFBVideoProvider;
      multiplier : CDouble
    ) : DFBResult;
     
    GetSpeed : function(
      thiz : PIDirectFBVideoProvider;
      ret_multiplier : PCDouble
    ) : DFBResult;
     
    SetVolume : function(
      thiz : PIDirectFBVideoProvider;
      level : CFloat
    ) : DFBResult;

    GetVolume : function(
      thiz : PIDirectFBVideoProvider;
      ret_level : PCFloat
    ) : DFBResult;

    SetStreamAttributes : function(
      thiz : PIDirectFBVideoProvider;
      attr : DFBStreamAttributes
    ) : DFBResult;

    SetAudioOutputs : function(
      thiz : PIDirectFBVideoProvider;
      audioUnits : PDFBVideoProviderAudioUnits
    ) : DFBResult;

    GetAudioOutputs : function(
      thiz : PIDirectFBVideoProvider;
      audioUnits : PDFBVideoProviderAudioUnits
    ) : DFBResult;

     SetAudioDelay : function(
      thiz : PIDirectFBVideoProvider;
      delay : CLong
     ) : DFBResult;



    CreateEventBuffer : function(
      thiz : PIDirectFBVideoProvider;
      ret_buffer : PPIDirectFBEventBuffer
    ) : DFBResult;

    AttachEventBuffer : function(
      thiz : PIDirectFBVideoProvider;
      buffer : PIDirectFBEventBuffer
    ) : DFBResult;

    EnableEvents : function(
      thiz : PIDirectFBVideoProvider;
      mask : DFBVideoProviderEventType
    ) : DFBResult;

    DisableEvents : function(
      thiz : PIDirectFBVideoProvider;
      mask : DFBVideoProviderEventType
    ) : DFBResult;

    DetachEventBuffer : function(
      thiz : PIDirectFBVideoProvider;
      buffer : PIDirectFBEventBuffer
    ) : DFBResult;



    GetBufferOccupancy : function(
      thiz : PIDirectFBVideoProvider;
      ret_occ : PDFBBufferOccupancy
    ) : DFBResult;

    SetBufferThresholds : function(
      thiz : PIDirectFBVideoProvider;
      thresh : DFBBufferThresholds
    ) : DFBResult;

    GetBufferThresholds : function(
      thiz : PIDirectFBVideoProvider;
      ret_thresh : PDFBBufferThresholds
    ) : DFBResult;

  end;


  { Data buffer interface, providing unified access to different kinds of data storage and live feed. }

  IDirectFBDataBuffer = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBDataBuffer ) : DFBResult;
    Release : function( thiz : PIDirectFBDataBuffer ) : DFBResult;
 

    Flush : function(
      thiz : PIDirectFBDataBuffer
    ) : DFBResult;

    Finish : function(
      thiz : PIDirectFBDataBuffer
    ) : DFBResult;

    SeekTo : function(
      thiz : PIDirectFBDataBuffer;
      offset : CUInt
    ) : DFBResult;

    GetPosition : function(
      thiz : PIDirectFBDataBuffer;
      ret_offset : PCUInt
    ) : DFBResult;

    GetLength : function(
      thiz : PIDirectFBDataBuffer;
      ret_length : PCUInt
    ) : DFBResult;


    WaitForData : function(
      thiz : PIDirectFBDataBuffer;
      length : CUInt
    ) : DFBResult;

    WaitForDataWithTimeout : function(
      thiz : PIDirectFBDataBuffer;
      length : CUInt;
      seconds : CUInt;
      milli_seconds : CUInt
    ) : DFBResult;



    GetData : function(
      thiz : PIDirectFBDataBuffer;
      length : CUInt;
      ret_data : Pointer;
      ret_read : PCUInt
    ) : DFBResult;

    PeekData : function(
      thiz : PIDirectFBDataBuffer;
      length : CUInt;
      offset : CInt;
      ret_data : Pointer;
      ret_read : PCUInt
    ) : DFBResult;

    HasData : function(
      thiz : PIDirectFBDataBuffer
    ) : DFBResult;



    PutData : function(
      thiz : PIDirectFBDataBuffer;
      data : Pointer;
      length : CUInt
    ) : DFBResult;



    CreateImageProvider : function(
      thiz : PIDirectFBDataBuffer;
      interface_ : PPIDirectFBImageProvider
    ) : DFBResult;

    CreateVideoProvider : function(
      thiz : PIDirectFBDataBuffer;
      interface_ : PPIDirectFBVideoProvider
    ) : DFBResult;
 
  end;


  { Interface to different display outputs, encoders, connector settings, power management and synchronization. }

  IDirectFBScreen = record
 

    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBScreen ) : DFBResult;
    Release : function( thiz : PIDirectFBScreen ) : DFBResult;
   

    GetID : function(
      thiz : PIDirectFBScreen;
      ret_screen_id : PDFBScreenID
    ) : DFBResult;


    GetDescription : function(
      thiz : PIDirectFBScreen;
      ret_desc : PDFBScreenDescription
    ) : DFBResult;
     

    GetSize : function(
      thiz : PIDirectFBScreen;
      ret_width : PCInt;
      ret_height : PCInt
    ) : DFBResult;




    EnumDisplayLayers : function(
      thiz : PIDirectFBScreen;
      callback : DFBDisplayLayerCallback;
      callbackdata : Pointer
    ) : DFBResult;




    SetPowerMode : function(
      thiz : PIDirectFBScreen;
      mode : DFBScreenPowerMode
    ) : DFBResult;




    WaitForSync : function(
      thiz : PIDirectFBScreen
    ) : DFBResult;


    GetMixerDescriptions : function(
      thiz : PIDirectFBScreen;
      ret_descriptions : PDFBScreenMixerDescription
    ) : DFBResult;


    GetMixerConfiguration : function(
      thiz : PIDirectFBScreen;
      mixer : CInt;
      ret_config : PDFBScreenMixerConfig
    ) : DFBResult;


    TestMixerConfiguration : function(
      thiz : PIDirectFBScreen;
      mixer : CInt;
      config : PDFBScreenMixerConfig;
      ret_failed : PDFBScreenMixerConfigFlags
    ) : DFBResult;


    SetMixerConfiguration : function(
      thiz : PIDirectFBScreen;
      mixer : CInt;
      config : PDFBScreenMixerConfig
    ) : DFBResult;


    GetEncoderDescriptions : function(
      thiz : PIDirectFBScreen;
      ret_descriptions : PDFBScreenEncoderDescription
    ) : DFBResult;


    GetEncoderConfiguration : function(
      thiz : PIDirectFBScreen;
      encoder : CInt;
      ret_config : PDFBScreenEncoderConfig
    ) : DFBResult;


    TestEncoderConfiguration : function(
      thiz : PIDirectFBScreen;
      encoder : CInt;
      config : PDFBScreenEncoderConfig;
      ret_failed : PDFBScreenEncoderConfigFlags
    ) : DFBResult;


    SetEncoderConfiguration : function(
      thiz : PIDirectFBScreen;
      encoder : CInt;
      config : PDFBScreenEncoderConfig
    ) : DFBResult;




    GetOutputDescriptions : function(
      thiz : PIDirectFBScreen;
      ret_descriptions : PDFBScreenOutputDescription
    ) : DFBResult;


    GetOutputConfiguration : function(
      thiz : PIDirectFBScreen;
      output : CInt;
      ret_config : PDFBScreenOutputConfig
    ) : DFBResult;


    TestOutputConfiguration : function(
      thiz : PIDirectFBScreen;
      output : CInt;
      config : PDFBScreenOutputConfig;
      ret_failed : PDFBScreenOutputConfigFlags
    ) : DFBResult;


    SetOutputConfiguration : function(
      thiz : PIDirectFBScreen;
      output : CInt;
      config : PDFBScreenOutputConfig
    ) : DFBResult;
       
  end;


  { OpenGL context of a surface. }

  IDirectFBGL = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFBGL ) : DFBResult;
    Release : function( thiz : PIDirectFBGL ) : DFBResult;

  end;


  { Main interface of DirectFB, created by DirectFBCreate(). }

  IDirectFB = record


    priv : Pointer;
    magic : CInt;
    AddRef : function( thiz : PIDirectFB ) : DFBResult;
    Release : function( thiz : PIDirectFB ) : DFBResult;


    SetCooperativeLevel : function(
      thiz : PIDirectFB;
      level : DFBCooperativeLevel
    ) : DFBResult;
    
    SetVideoMode : function(
        thiz : PIDirectFB;
        width,
        height,
        bpp : CInt
    ) : DFBResult;



    GetDeviceDescription : function(
        thiz : PIDirectFB;
        ret_desc : PDFBGraphicsDeviceDescription
    ) : DFBResult;

    EnumVideoModes : function(
        thiz : PIDirectFB;
        callback : DFBVideoModeCallback;
        callbackdata : Pointer
    ) : DFBResult;



     CreateSurface : function(
        thiz : PIDirectFB;
        desc : PDFBSurfaceDescription;
        ret_interface : PPIDirectFBSurface
    ) : DFBResult;

    CreatePalette : function(
        thiz : PIDirectFB;
        des : PDFBPaletteDescription;
        ret_interface : PPIDirectFBPalette
    ) : DFBResult;

    

    EnumScreens : function(
      thiz : PIDirectFB;
      callback : DFBScreenCallback;
      callbackdata : Pointer
    ) : DFBResult;

    GetScreen : function(
      thiz : PIDirectFB;
      screen_id : DFBScreenID;
      ret_interface : PPIDirectFBScreen
    ) : DFBResult;



    EnumDisplayLayers : function(
      thiz : PIDirectFB;
      callback :DFBDisplayLayerCallback;
      callbackdata : Pointer
    ) : DFBResult;

    GetDisplayLayer : function(
      thiz : PIDirectFB;
      layer_id : DFBDisplayLayerID;
      ret_interface : PPIDirectFBDisplayLayer
    ) : DFBResult;



    EnumInputDevices : function(
      thiz : PIDirectFB;
      callback : DFBInputDeviceCallback;
      callbackdata : Pointer
    ) : DFBResult;

    GetInputDevice : function(
      thiz : PIDirectFB;
      device_id : DFBInputDeviceID;
      ret_interface : PPIDirectFBInputDevice
    ) : DFBResult;

    CreateEventBuffer : function(
      thiz : PIDirectFB;
      ret_buffer : PPIDirectFBEventBuffer
    ) : DFBResult;

    CreateInputEventBuffer : function(
      thiz : PIDirectFB;
      caps : DFBInputDeviceCapabilities;
      global : DFBBoolean;
      ret_buffer : PPIDirectFBEventBuffer
     ) : DFBResult;



    CreateImageProvider : function(
      thiz : PIDirectFB;
      filename : PChar;
      ret_interface : PPIDirectFBImageProvider
    ) : DFBResult;

    CreateVideoProvider : function(
      thiz : PIDirectFB;
      filename : PChar;
      ret_interface : PPIDirectFBVideoProvider
    ) : DFBResult;

    CreateFont : function(
      thiz : PIDirectFB;
      filename : PChar;
      desc : PDFBFontDescription;
      ret_interface : PPIDirectFBFont
    ) : DFBResult;

    CreateDataBuffer : function(
      thiz : PIDirectFB;
      desc : PDFBDataBufferDescription;
      ret_interface : PPIDirectFBDataBuffer
    ) : DFBResult;



    SetClipboardData : function(
      thiz : PIDirectFB;
      mime_type : PChar;
      data : Pointer;
      size : CUInt;
      ret_timestamp : Ptimeval
    ) : DFBResult;

    GetClipboardData : function(
      thiz : PIDirectFB;
      ret_mimetype : PPChar;
      ret_data : PPointer;
      ret_size : PCUInt
    ) : DFBResult;

    GetClipboardTimeStamp : function(
      thiz : PIDirectFB;
      ret_timestamp : Ptimeval
    ) : DFBResult;



    Suspend : function(
      thiz : PIDirectFB
    ) : DFBResult;

    Resume : function(
      thiz : PIDirectFB
    ) : DFBResult;

    WaitIdle : function(
      thiz : PIDirectFB
    ) : DFBResult;

    WaitForSync : function(
      thiz : PIDirectFB
    ) : DFBResult;



    GetInterface : function(
      thiz : PIDirectFB;
      type_ : PChar;
      implementation_ : PChar;
      arg : Pointer;
      ret_interface : PPointer
    ) : DFBResult;

  end;



{ Function declarations }

function DirectFBCheckVersion(required_major: Dword; required_minor: Dword;
  required_micro: Dword): PChar; external DFBLibName
  name 'DirectFBCheckVersion';


function DirectFBError(msg : PChar;
  result : DirectResult
  ) : DirectResult; external DFBLibName name 'DirectFBError';


function DirectFBErrorFatal(msg : PChar;
  result : DirectResult
  ) : DirectResult; external DFBLibName name 'DirectFBErrorFatal';


function DirectFBErrorString(result : DirectResult) : PChar;
  external DFBLibName name 'DirectFBErrorString';


function DirectFBUsageString : PChar; external DFBLibName
  name 'DirectFBUsageString';


function DirectFBInit(argc : PCInt;
  argv : PPChar
  ) : DirectResult; external DFBLibName name 'DirectFBInit';


function DirectFBSetOption(name_ : PChar;
  value : PChar) : DirectResult; external DFBLibName
  name 'DirectFBSetOption';


function DirectFBCreate(interface_ : PPIDirectFB
  ) : DirectResult; external DFBLibName name 'DirectFBCreate';



function DFB_POINT_EQUAL(a, b : DFBPoint) : Boolean;


function DFB_RECTANGLE_EQUAL(a, b : DFBRectangle) : Boolean;


function DFB_LOCATION_EQUAL(a, b : DFBLocation) : Boolean;


function DFB_REGION_EQUAL(a, b : DFBRegion) : Boolean;


function DFB_COLOR_EQUAL(x, y : DFBColor) : Boolean;


function DFB_COLORKEY_EQUAL(x, y : DFBColorKey) : Boolean;


function DFB_DISPLAYLAYER_IDS_ADD(ids, id : CUInt) : CUInt;


function DFB_DISPLAYLAYER_IDS_REMOVE(ids, id : CUInt) : CUInt;


function DFB_DISPLAYLAYER_IDS_HAVE(ids, id : CUInt) : CUInt;


function DFB_DISPLAYLAYER_IDS_EMPTY(ids : CUInt) : CUInt;


function DFB_DRAWING_FUNCTION(a : CUInt) : CUInt;


function DFB_BLITTING_FUNCTION(a : CUInt) : CUInt;


function DFB_SURFACE_PIXELFORMAT( index_, color_bits, alpha_bits,
  has_alpha, row_bits, row_bytes, align, mul_f, mul_d, has_lut,
  inv_alpha: CUInt ) : CUInt;


function DFB_PIXELFORMAT_INDEX(fmt : CUInt) : CUInt;
function DFB_COLOR_BITS_PER_PIXEL(fmt : CUInt) : CUInt;
function DFB_ALPHA_BITS_PER_PIXEL(fmt : CUInt) : CUInt;
function DFB_PIXELFORMAT_HAS_ALPHA(fmt : CUInt) : Boolean;
function DFB_BITS_PER_PIXEL(fmt : CUInt) : CUInt;
function DFB_BYTES_PER_PIXEL(fmt : CUInt) : CUInt;
function DFB_BYTES_PER_LINE(fmt,width : CUInt) : CUInt;
function DFB_PIXELFORMAT_ALIGNMENT(fmt : CUInt) : CUInt;
function DFB_PLANE_MULTIPLY(fmt,height : CUInt) : CUInt;
function DFB_PIXELFORMAT_IS_INDEXED(fmt : CUInt) : Boolean;
function DFB_PLANAR_PIXELFORMAT(fmt : CUInt) : Boolean;
function DFB_PIXELFORMAT_INV_ALPHA(fmt : CUInt) : Boolean;


function DFB_EVENT(e : DFBEvent) : PDFBEvent;



implementation



function DFBErrToStr(Result : DFBResult) : string;

begin

  case Result of
  
    DFB_OK : DFBErrToStr := 'DFB_OK';
    DFB_FAILURE : DFBErrToStr := 'DFB_FAILURE';
    DFB_INIT : DFBErrToStr := 'DFB_INIT';
    DFB_BUG : DFBErrToStr := 'DFB_BUG';
    DFB_DEAD : DFBErrToStr := 'DFB_DEAD';
    DFB_UNSUPPORTED : DFBErrToStr := 'DFB_UNSUPPORTED';
    DFB_UNIMPLEMENTED : DFBErrToStr := 'DFB_UNIMPLEMENTED';
    DFB_ACCESSDENIED : DFBErrToStr := 'DFB_ACCESSDENIED';
    DFB_INVAREA : DFBErrToStr := 'DFB_INVAREA';
    DFB_INVARG : DFBErrToStr := 'DFB_INVARG';
    DFB_NOSYSTEMMEMORY : DFBErrToStr := 'DFB_NOSYSTEMMEMORY';
    DFB_NOSHAREDMEMORY : DFBErrToStr := 'DFB_NOSHAREDMEMORY';
    DFB_LOCKED : DFBErrToStr := 'DFB_LOCKED';
    DFB_BUFFEREMPTY : DFBErrToStr := 'DFB_BUFFEREMPTY';
    DFB_FILENOTFOUND : DFBErrToStr := 'DFB_FILENOTFOUND';
    DFB_IO : DFBErrToStr := 'DFB_IO';
    DFB_BUSY : DFBErrToStr := 'DFB_BUSY';
    DFB_NOIMPL : DFBErrToStr := 'DFB_NOIMPL';
    DFB_TIMEOUT : DFBErrToStr := 'DFB_TIMEOUT';
    DFB_THIZNULL : DFBErrToStr := 'DFB_THIZNULL';
    DFB_IDNOTFOUND : DFBErrToStr := 'DFB_IDNOTFOUND';
    DFB_DESTROYED : DFBErrToStr := 'DFB_DESTROYED';
    DFB_FUSION : DFBErrToStr := 'DFB_FUSION';
    DFB_BUFFERTOOLARGE : DFBErrToStr := 'DFB_BUFFERTOOLARGE';
    DFB_INTERRUPTED : DFBErrToStr := 'DFB_INTERRUPTED';
    DFB_NOCONTEXT : DFBErrToStr := 'DFB_NOCONTEXT';
    DFB_TEMPUNAVAIL : DFBErrToStr := 'DFB_TEMPUNAVAIL';
    DFB_LIMITEXCEEDED : DFBErrToStr := 'DFB_LIMITEXCEEDED';
    DFB_NOSUCHMETHOD : DFBErrToStr := 'DFB_NOSUCHMETHOD';
    DFB_NOSUCHINSTANCE : DFBErrToStr := 'DFB_NOSUCHINSTANCE';
    DFB_ITEMNOTFOUND : DFBErrToStr := 'DFB_ITEMNOTFOUND';
    DFB_VERSIONMISMATCH : DFBErrToStr := 'DFB_VERSIONMISMATCH';
    DFB_EOF : DFBErrToStr := 'DFB_EOF';
    DFB_SUSPENDED : DFBErrToStr := 'DFB_SUSPENDED';
    DFB_INCOMPLETE : DFBErrToStr := 'DFB_INCOMPLETE';
    DFB_NOCORE : DFBErrToStr := 'DFB_NOCORE';
    DFB__RESULT_OFFSET : DFBErrToStr := 'DFB__RESULT_OFFSET';
    DFB_NOVIDEOMEMORY : DFBErrToStr := 'DFB_NOVIDEOMEMORY';
    DFB_MISSINGFONT : DFBErrToStr := 'DFB_MISSINGFONT';
    DFB_MISSINGIMAGE : DFBErrToStr := 'DFB_MISSINGIMAGE';
  
  else
  
    DFBErrToStr := 'DFB_UNKNOWN_ERROR_CODE';
  
  end;

end;


function DFBErrToText(Result : DFBResult) : string;

begin

  case Result of
  
    DFB_OK : DFBErrToText := 'No error occured.';

    DFB_FAILURE : DFBErrToText :=
      'A general or unknown error occured.';
    DFB_INIT : DFBErrToText :=
      'A general initialization error occured.';
    DFB_BUG : DFBErrToText :=
      'Internal bug or inconsistency has been detected.';
    DFB_DEAD : DFBErrToText :=
      'Interface has a zero reference counter (available in debug mode).';
    DFB_UNSUPPORTED : DFBErrToText :=
      'The requested operation or an argument is (currently) not supported.';
    DFB_UNIMPLEMENTED : DFBErrToText :=
      'The requested operation is not implemented; yet.';
    DFB_ACCESSDENIED : DFBErrToText :=
      'Access to the resource is denied.';
    DFB_INVAREA : DFBErrToText :=
      'An invalid area has been specified or detected.';
    DFB_INVARG : DFBErrToText :=
      'An invalid argument has been specified.';
    DFB_NOSYSTEMMEMORY : DFBErrToText :=
      'There''s not enough system memory.';
    DFB_NOSHAREDMEMORY : DFBErrToText :=
      'There''s not enough shared memory.';
    DFB_LOCKED : DFBErrToText :=
      'The resource is (already) locked.';
    DFB_BUFFEREMPTY : DFBErrToText :=
      'The buffer is empty.';
    DFB_FILENOTFOUND : DFBErrToText :=
      'The specified file has not been found.';
    DFB_IO : DFBErrToText :=
      'A general I/O error occured.';
    DFB_BUSY : DFBErrToText :=
      'The resource or device is busy.';
    DFB_NOIMPL : DFBErrToText :=
      'No implementation for this interface or content type has been found.';
    DFB_TIMEOUT : DFBErrToText :=
      'The operation timed out.';
    DFB_THIZNULL : DFBErrToText :=
      '''thiz'' pointer is NULL.';
    DFB_IDNOTFOUND : DFBErrToText :=
      'No resource has been found by the specified id.';
    DFB_DESTROYED : DFBErrToText :=
      'The underlying object (e.g. a window or surface) has been destroyed.';
    DFB_FUSION : DFBErrToText :=
      'Internal fusion error detected; most likely related to IPC resources.';
    DFB_BUFFERTOOLARGE : DFBErrToText :=
      'Buffer is too large.';
    DFB_INTERRUPTED : DFBErrToText :=
      'The operation has been interrupted.';
    DFB_NOCONTEXT : DFBErrToText :=
      'No context available.';
    DFB_TEMPUNAVAIL : DFBErrToText :=
      'Temporarily unavailable.';
    DFB_LIMITEXCEEDED : DFBErrToText :=
      'Attempted to exceed limit; i.e. any kind of maximum size; count etc.';
    DFB_NOSUCHMETHOD : DFBErrToText :=
      'Requested method is not known; e.g. to remote site.';
    DFB_NOSUCHINSTANCE : DFBErrToText :=
      'Requested instance is not known; e.g. to remote site.';
    DFB_ITEMNOTFOUND : DFBErrToText :=
      'No such item found.';
    DFB_VERSIONMISMATCH : DFBErrToText :=
      'Some versions didn''t match.';
    DFB_EOF : DFBErrToText :=
      'Reached end of file.';
    DFB_SUSPENDED : DFBErrToText :=
      'The requested object is suspended.';
    DFB_INCOMPLETE : DFBErrToText :=
      'The operation has been executed; but not completely.';
    DFB_NOCORE : DFBErrToText :=
      'Core part not available.';
    DFB__RESULT_OFFSET : DFBErrToText :=
      'This error code is meaningless.';
    DFB_NOVIDEOMEMORY : DFBErrToText :=
      'There''s not enough video memory.';
    DFB_MISSINGFONT : DFBErrToText :=
      'No font has been set.';
    DFB_MISSINGIMAGE : DFBErrToText :=
      'No image has been set.';

  else
  
    DFBErrToText := 'An unknown error code was returned.';
  
  end;

end;



function DFB_POINT_EQUAL(a, b : DFBPoint) : Boolean;

begin

  DFB_POINT_EQUAL :=
    (a.x = b.x) and
    (a.y = b.y);

end;


function DFB_RECTANGLE_EQUAL(a, b : DFBRectangle) : Boolean;

begin

  DFB_RECTANGLE_EQUAL :=
    (a.x = b.x) and
    (a.y = b.y) and
    (a.w = b.w) and
    (a.h = b.h);

end;


function DFB_LOCATION_EQUAL(a, b : DFBLocation) : Boolean;

begin

  DFB_LOCATION_EQUAL :=
    (a.x = b.x) and
    (a.y = b.y) and
    (a.w = b.w) and
    (a.h = b.h);

end;


function DFB_REGION_EQUAL(a, b : DFBRegion) : Boolean;

begin

  DFB_REGION_EQUAL :=
    (a.x1 = b.x1) and
    (a.y1 = b.y1) and
    (a.x2 = b.x2) and
    (a.y2 = b.y2);

end;


function DFB_COLOR_EQUAL(x, y : DFBColor) : Boolean;

begin

  DFB_COLOR_EQUAL := (x.a = y.a) and
    (x.r = y.r) and
    (x.g = y.g) and
    (x.b = y.b);

end;


function DFB_COLORKEY_EQUAL(x, y : DFBColorKey) : Boolean;

begin

  DFB_COLORKEY_EQUAL := (x.index = y.index) and
    (x.r = y.r) and
    (x.g = y.g) and
    (x.b = y.b);

end;



function DFB_DISPLAYLAYER_IDS_ADD(ids, id : CUInt) : CUInt;

begin

  DFB_DISPLAYLAYER_IDS_ADD := ids or (1 shl id);

end;


function DFB_DISPLAYLAYER_IDS_REMOVE(ids, id : CUInt) : CUInt;

begin

  DFB_DISPLAYLAYER_IDS_REMOVE := ids and (not (1 shl id));

end;


function DFB_DISPLAYLAYER_IDS_HAVE(ids, id : CUInt) : CUInt;

begin

  DFB_DISPLAYLAYER_IDS_HAVE := ids and (1 shl id);

end;



function DFB_DISPLAYLAYER_IDS_EMPTY(ids : CUInt) : CUInt;

begin

  DFB_DISPLAYLAYER_IDS_EMPTY := 0;

end;


function DFB_DRAWING_FUNCTION(a : CUInt) : CUInt;

begin

  DFB_DRAWING_FUNCTION := a and $0000FFFF;

end;


function DFB_BLITTING_FUNCTION(a : CUInt) : CUInt;

begin

  DFB_BLITTING_FUNCTION := a and $FFFF0000;

end;


function DFB_SURFACE_PIXELFORMAT( index_, color_bits, alpha_bits,
  has_alpha, row_bits, row_bytes, align, mul_f, mul_d, has_lut,
  inv_alpha: CUInt ) : CUInt;

var

  HasAlpha, HasLut, InvAlpha : CUInt;

begin

  if has_alpha = 1 then HasAlpha := 1 else HasAlpha := 0;
  if has_lut = 1 then HasLut := 1 else HasLut := 0;
  if inv_alpha = 1 then InvAlpha := 1 else InvAlpha := 0;
  
  DFB_SURFACE_PIXELFORMAT :=
    (index_ and $7F ) or
    ((color_bits and $1F) shl 7) or
    ((alpha_bits and $0F) shl 12) or
    (HasAlpha shl 16) or
    ((row_bits and $07) shl 17) or
    ((row_bytes and $07) shl 20) or
    ((align and $07) shl 23) or
    ((mul_f and $03) shl 26) or
    ((mul_d and $03) shl 28) or
    (HasLut shl 30) or
    (InvAlpha shl 31);

end;



function DFB_PIXELFORMAT_INDEX(fmt : CUInt) : CUInt;

begin

      DFB_PIXELFORMAT_INDEX := fmt and $0000007F;

end;


function DFB_COLOR_BITS_PER_PIXEL(fmt : CUInt) : CUInt;

begin

   DFB_COLOR_BITS_PER_PIXEL := (fmt and $00000F80) shr 7;

end;


function DFB_ALPHA_BITS_PER_PIXEL(fmt : CUInt) : CUInt;

begin

   DFB_ALPHA_BITS_PER_PIXEL := (fmt and $0000F000) shr 12;

end;


function DFB_PIXELFORMAT_HAS_ALPHA(fmt : CUInt) : Boolean;

begin

  DFB_PIXELFORMAT_HAS_ALPHA := (fmt and $00010000) <> 0

end;


function DFB_BITS_PER_PIXEL(fmt : CUInt) : CUInt;

begin

  DFB_BITS_PER_PIXEL := (fmt and $007E0000) shr 17;

end;


function DFB_BYTES_PER_PIXEL(fmt : CUInt) : CUInt;

begin

  DFB_BYTES_PER_PIXEL := (fmt and $00700000) shr 20;

end;


function DFB_BYTES_PER_LINE(fmt,width : CUInt) : CUInt;

begin

  DFB_BYTES_PER_LINE :=
    ((((fmt and $007E0000) shr 17) * width + 7) shr 3);

end;


function DFB_PIXELFORMAT_ALIGNMENT(fmt : CUInt) : CUInt;

begin

  DFB_PIXELFORMAT_ALIGNMENT := ((fmt and $03800000) shr 23);

end;


function DFB_PLANE_MULTIPLY(fmt,height : CUInt) : CUInt;

begin

  DFB_PLANE_MULTIPLY :=
    (((((fmt and $3C000000) shr 26) + 4) * height) shr 2);

end;


function DFB_PIXELFORMAT_IS_INDEXED(fmt : CUInt) : Boolean;

begin

  DFB_PIXELFORMAT_IS_INDEXED := (fmt and $40000000) <> 0;

end;


function DFB_PLANAR_PIXELFORMAT(fmt : CUInt) : Boolean;

begin

     DFB_PLANAR_PIXELFORMAT := (fmt and $3C000000) <> 0;

end;


function DFB_PIXELFORMAT_INV_ALPHA(fmt : CUInt) : Boolean;

begin

  DFB_PIXELFORMAT_INV_ALPHA := (fmt and $80000000) <> 0;

end;


function DFB_EVENT(e : DFBEvent) : PDFBEvent;

begin

  DFB_EVENT := @e;

end;
