{
  
  DirectFB 1.2.7 - 1.4.3 bindings for FreePascal.
  (c) Copyright 2009-2010  The WiSeSLAp Team
  Written by Roland Schäfer <mail@rolandschaefer.net>

  This file is licensed under the same license as the original header:

  (c) Copyright 2001-2009  The world wide DirectFB Open Source Community (directfb.org)
  (c) Copyright 2000-2004  Convergence (integrated media) GmbH

  All rights reserved.

  Written by Denis Oliver Kropp <dok@directfb.org>,
            Andreas Hundt <andi@fischlustig.de>,
            Sven Neumann <neo@directfb.org>,
            Ville Syrjälä <syrjala@sci.fi> and
            Claudio Ciccani <klan@users.sf.net>.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA 02111-1307, USA.
}


unit DirectFB;


{$MODE FPC}
{$CALLING CDECL}
{$PACKRECORDS C}


interface


uses
  CTypes,
  UnixType,
  DFBTypes,
  DirectFBKeyboard;

const
  DFBLibName = 'libdirectfb.so';

{$IFDEF FPC_DFB_127}
  {$INFO Compiling with support for DirectFB 1.2.7.}
  {$I directfb127.pp}
{$ELSE}
  {$IFDEF FPC_DFB_128}
    {$I directfb128.pp}
    {$INFO Compiling with support for DirectFB 1.2.8.}
  {$ELSE}
    {$IFDEF FPC_DFB_129}
      {$I directfb129.pp}
      {$INFO Compiling with support for DirectFB 1.2.9.}
    {$ELSE}
      {$IFDEF FPC_DFB_141}
        {$INFO Compiling with support for DirectFB 1.4.1.}
        {$I directfb141.pp}
      {$ELSE}
        {$IFDEF FPC_DFB_142}
          {$INFO Compiling with support for DirectFB 1.4.2.}
          {$I directfb142.pp}
        {$ELSE}
          {$INFO Compiling with support for DirectFB 1.4.3 (default).}
          {$I directfb143.pp}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

  
end.
