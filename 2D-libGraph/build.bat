@ECHO OFF

rem Note: this works on win10 and needs testing on win7 and below.
rem I opted to be safe w regards to Win2k- I know from experience that PII can run it. 
  
reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT fpc -O3 -Sv -FEunits\win32 -FUunits\win32 -Ci -Co -Cr   lazgfx.pas
if %OS%==64BIT fpc -Px86_64 -O3 -FEunits\win64 -FUunits\win64 -Ci -Co -Cr -WR  lazgfx.pas


IF NOT EXIST "c:\windows" (
  REM Likely we only have Dos. Assume prior to 386. Earliy editions of Windows (prior to 3.0) were unlikely to be owned by consumers.
  
  fpc -O3 -Sv -FEunits\i8086 -FUunits\i8086 -Ci -Co -Cr  lazgfx.pas

)

