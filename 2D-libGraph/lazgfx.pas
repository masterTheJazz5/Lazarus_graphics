Unit LazGFX;

{
A "fully creative rewrite" of the "Borland /Generic Graphic Interface" (for FreePascal) implemented "however possible".
(c) 2017-2021 (and beyond) Richard Jasmin

** PENTIUM MINIMUM (or equivalent powerPC) REQUIRED TO BUILD (reasons are mostly internal to SDL)**
Now, this said , *most* amigas are m68k, Im targeting the A500 and A1k here- If you have better, use it.

This code is intentionally NOT BORLAND COMPATIBLE.
It may "NOT even build" on Borland compilers.

-The code is "borland similar".
(I dont care if you dont like it. Some changes were necessary.)


LAZ_BGI sources are being integrated here- bear with me, I hae asolution for the "issues brought up". This includes for GLCanvases(reccommended vs X11 Canvases).
GL requires 24/32bpp, the rest must be emulated via LUT, etc. Nobody else seems to want to do the necessary work. (The MD is close, not correct: Form.OnKey, as noted)

** DONT OVERTHINK THIS UNIT**

There are easy solutions to these problems imposed upon us-

SDL unifies and simplifies programming for DirectX(D2D), Quartz2D, and X11)- but it doesnt go far enough.
Yes, I am hinting SDL into those libraries, implicitly on GraphInit(d2d,quartz,x11). How effective this will be - is yet to be determined.

Unfortunately, its either 2D(with possible acceleration somehow) or GL 3D and GL3D doesnt do 2D base functions very well.
That doesnt mean that we should use old (1990s era) code for video hardware access, modeswitching,etc- nor should we mandate that all routines be drawn by OpenGL - "just beacuse".

Theres more power under your hood than you realize. Most younger machines use MMX,SSE, AVX- and the older use assembler- for speed. X11 is accelerated, but without "window decorations" on purpose.
(and this is why SDL base dialogs usually look like ass)

However- the SDL syntax is unfamiliar to most -to be of use. Hence this unit.

X11CorePrimitives(even cairo) are not as basic as many would think.
2.8Trillion collors support, thru 1080p? Thats nothing to poke fun of.

SDL cant even keep up.

OpenGL hooking X11 window context-- is a cinch. IDK why SDL makes this so difficult.
I have hooked GL, and X11 CorePrims, no issue.

Both are rather powerful, X11 predefines some palettes for you(unless you dont want that- above 15bpp?). The manual is a little DAFT as to that information, but I am reading it correctly.

The LCL?

This makes me think that the LCL has gotten too fat for its own good. Indeed the FPC dev team, moreso in that regards, too dumb - and arrogant.

Seems the entire sources for the FPC Graph library need an overhaul.
libPTc only hooks the old DOS routines. This is bad, very bad. A very sad state indeed, and the SDL Graph unit that exists? Atrocious garbage!


Prior units are half-assed, incomplete...and simply dont feed the need of modern day programmers. (They only worked in the 90s.)

Legal: Power /Patents go to the modifier(and the one with the functional complete routines).

**SDLv2 leans on GL (2D) too much, and GL was never intended for solely 2D operations. This is where a lot of people screw up.**


Byte/SmallInt are 8bit references
Word/Integer are 16bit references
Longword are 32bit references

QuadWords are 64bit references (IFF: CPU64)


**Code Intentionally uses forced 32bit references(unless required).**

Too many shortcuts (assumptions) are taken with most graphics(libGraph) units.


This is the 'most complete' version that I have found.
Code compatibility is *mostly* here.
A lot was added. SOME WAS CHANGED. DEAL WITH IT.

----
I am "excersing my option" on all associated code linked herein- and changeing the license (for all code).
GPL2 allows "any newer version", such as v3- and accepts that Apache and BSD are more open- "open source licenses".
It also acknowledges the fact that 3rd parties can "Switch" licenses.
(GPLv2 required pingbacks and updates to authors, v3+ do not.)

* THIS IS NOT SHAREWARE. *
It is not College level homework(although some write simmilar).
This code is here to fix issues, promote programming- and integrate solutions into "the Pulic codeSphere".

AS SUCH IT IS THIS CODE IS Apache/Mozilla licensed(FREED).



I dont want to hear bitching about the changes in license(from GPLv2,3/LGPL). 90% of GPL/LGPL licenses uses(moreso FPC) quote (at your option),
therefore, I have exised the option.

Wherefore, the next option is a relicense under GPLv3 standards, which includes alt licenses such as apache.
Wherefore, I have also exised that option as well.


Sections from the "FPC Graphics unit" are from FPC/FPK devs where noted.

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
*including commercial applications and resale*, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software *must not be misrepresented*;
   **you must not claim that you wrote the original software**.
   You must cite the original authors in some way.

2. Altered source versions must be plainly marked as such, and **must not be
   misrepresented as being the original software**.

3. This notice may not be removed or altered from any source distribution.

4. (imposed by codeforge): "NON-FOSS/OSI compliant code" must never be introduced.
   IOW- CODE MUST ALWAYS BE OSI/FOSS "license compliant/compatible".

(NASA interns have already violated clauses 2 and 3- which is the reson why IM REPEATING THIS.)

--

**This unit should OVERRIDE FPCs Graphics unit. **
This unit was never intended to become a GDI/LCL barebones replacement unit. Nor was GVision.

SDL?

Although SDL is designed "for games programming"..the integration involved by the USE, ABUSE, and REUSE-
of SDL and Core Prims highlights so many OS internals its not even funny.

Anyone serious enough to work with SDL on an ongoing basis- is doing some SERIOUS development,
moreso if they are "expanding the reach" of SDL using a language "without much love" such as FreePascal.


Even better if they are GUTTING SDL for a better(and more efficient) lower-level API routine. (not the wisest move, but possible).



event driven applications and games REQUIRE OS (win/mac and linux internals) programming skills.

**STOP THE READKEY NONSENSE!!! PLEASE!!**
Just dont think you can "fake your way thru college"- you code will speak to your shortcuts. As will your logic.


The SDL_Draw unit adds in the gfx  equivalent I was looking for- (it still needs to be ported from C). Sin and Cos are used for Arcs, slices, and circles, ellipses(and whynot?)


libHermes:
    iff this code is portable, fpc doesnt install the full source code. IT MIGHT NOT BE.
    
    INVESTIGATING.(missing altivec?)

   SDL leans heavily on libHermes, even for ops which its not even necessary to do so.


SDL1/2 recombination adds another level of code complexity due to the need for surfaces.
SDL2 does allow for DirectRendering(texture RW streaming ops). Negates need for surfaces.


Notes on the ALMIGHTY OpenGL:

No matter how hard you try, you CANNOT mix SDL2 Renderer calls and OpenGL. (really one and the same ayways)
If youre calling GL , from within SDL2- STOP! Better to call it natively.

GL 2D shapes will have to be coded manually, not all routines work like you think they do. Better to use SDL v2 if unsure.
GL is more powerful, once you know how to use it, IFFF you are OK with writing C-like syntax GLSL vertex/fragment shader files..


If we ever "get rid of SDL" it will likely be due to Cairo support, or use of libPTC.


 -There are toolkits like: Castle.....


---


- If we have a window(and event queue) we should be able to hook it, why is Lazarus BROKEN?

You can do 3d with libPTC.
PTC is in competition with SDLv2, yet internally functions as SDL v1 in places.
I think the project just bitrotted out of existance, I see why with the broken libGraph API.

"Vectors/Vector Math" is in fact 3D operations, usually accompanied by -- you guessed it, Matrix 3D math. (intermediate college algebra). OGL programming at its finest.

(Pull it, bop it, tweak it, push it back...and twist..)

libPTC greatly simplyfies GL (immead) programming.
both SDL and libPTc use libHermes. was libPlacebo ever considerd?

}

{$ifNDEF fpc}

//we may be using MetroWerks on OS8/9 instead of FPC
{$ifdef MWERKS}     //setup MWerks for SDL programming. (MacOS core, not OSX) congrats on doing something right!
        {$define SDL}
{$endif}

//neither MetroWrks, nor FPC was deinfed.

       {$Error  You must use FPC or MetroWerks to compile this.}
{$ENDIF}



{$ifdef i8086}
       {$Error  This code is neither supported, nor will run on this system. Try Borland/Turbo Pascal.}
{$endif}

{$ifdef i686} //DOS based OS until P3. (486->P3)
       {$Warning  Things may be slow, Use of DPMI is enforced.}
       {$define UseDPMI}
{$endif}


// FPC "normal mode", turn on ANSIStrings(null terminated).

{$mode objfpc}
{$H+}


//Console apps on Windows/Mac might not output anything(theres no console)- so we need to make sure we were invoked  correctly.

{$ifdef HASAMIGA}
    {$packrecords 4} //use 32bit data storage. The data has been aligned for some time( even with SDLv1).
{$endif}

//MacShell is the "Mac (SIOUX) console". There is no DOS for a Macintosh. (Do we care?)
//MacShell is a seperate application. Go "fetch" it.


//All MacOSClassic (v9) apps made via fpc are SOIUX/SIOW apps. They can use a console, but no envvars or exit codes can be used. (no sdl tweaks)
//The app can produce output and request input- just not "before it starts", or "after it exits".

//however, console apps made this way CANNOT be threaded, as the TOOL(app) is a thread itself. This poses a problem.
// OS9 development needs to be done under CodeWarrior, using GameSprockets(GX). (Im flying blind here)

//dont tell me that DOS isnt thread capable. I know better. Its an "undocumented CPU feature"


//Im a PASCAL programmer...I stay within the 'bounds'. I dont knowingly produce apps 'that have exploits'.

//Range and overflow checks =ON   (FORCED)
{$Q+}
{$R+}

//TypeInfo(required)
{$M+}

{$IFDEF debug}
//memory tracing, along with heap. And STACK, too.
	{$S+}
{$ENDIF}
//heap: I need memory for a pointer(points anywhere- swap, RAM,zRAM...)
//stack: Function calls(proceedures) to be executed.

//When requesting a pointer- you get an address, dont Nil this. If you do, you need to realloc the pointer- or there will be problems.
//use "Nil" to "sanitize thigs on MemFree/Dispose. Just dont "free" twice.


{$OPTIMIZATION LEVEL3}

interface
{

-DONT RUN A LIBRARY..link to it.
-Anonymous C kernel dev (that runs a library)

This is not -and never will be- your program. Sorry, Jim.
RELEASES are stable builds and nothing else is guaranteed. See the DEMOs for details.

The quote that never was-

Linus: "Dont tell me what to do.
I write CODE for a living.
Everything is mutable."


---
Some notes before we begin:

In Lazarus:

Project-> Console App or  (APPTYPE: console)
Project-> Program    (APPTYPE: GUI)

Set Run Parameters:
"Use Launcher" -to enable runwait.sh and console output (otherwise you might not see it)

Outside of Lazarus:

USE the build scripts and makefile.
SERIOUSLY.
USE THEM.

---

We will have some limits:

        Namely VRAM- if its not onscreen, its in RAM or funneled thru the CPU somewhere...
        You might have 12Gb fre of RAM, but not 256mb of VRAM. libPTC thinks it can (RAMBUFFER??) get around this.

        Older systems could have as less as 16MB VRAM, most notebooks within 5-10 years should have 256.
        The only way to work with these systems is to clear EVERYTHING REPEATEDLY or use CPU and RAM to your advantage.


Many dont understand SDLv1 use- then get thrown and force fed SDL2.x and GPU_ functions.
Please understand- there is TWO methods of doing things.


They are NOT the same.

I have tried to combine functionality rather than duplicate 14k lines of code.

If you are trying to understand the code:

Learn this (in order):

SDL1.2 (learn to page flip)
SDL2.0 (renderer and "render to textures as surfaces"- this is the GL pipeline)
OGL (straight up, then hard hitting shader ops, 2D and 3D "flying blind while drunk"...)


aux libs used:

	uOS (sound)
	Synapse (net, pascal compatible)
    SDL dependencies(mikmod,freetype,etc)
    Cairo "system" dependencies(and Pascal headers)

**X11 DOES NOT REQUIRE the mess that FPC devs have made freetype out to be.**
* WHY IS FREETYPE SUCH A PITA? *


You need to know:

        event-driven input and rendering loops (FORGET the console and ReadKEY)

I dont know how many times that Ive seen readkey() used....IN "EVENT DRIVEN" APPLICATIONS and LIBRARIES.
-or people being dumbasses, and expecting to connect the two worlds of UI and console apps!!
These two worlds DO NOT MIX!

DO NOT IGNORE A VERY IMPORTANT      INTERRUPT-BASED       PART OF YOUR OPERATING SYSTEM!!
THERES A REASON THEY TEACH THIS IN COLLEGE! (at a 4year university)


---

There is a lot of plugins for SDL/SDLv2 and that initially confused the heck out of me.

The JEDI code had to be cleaned up. (Apparently I missed the SDL v12 code somewhere??)
JEDI is more like "JOKER PASCAL DEV-TEAM". A bunch of nobodys, no contact info, no nothing...and sloppy work at best.
The code is now vaporware- as is thier website and linked data.


SDL_Image() routines are JPEG, TIF, PNG support (BMP is in SDL(core))
Other libs for multimedia will be used. Get over it.


Circle/ellipse:
The difference between an ellipse and a circle is that "an ellipse is corrected for the aspect ratio". ..ROLLEYES..
(circles are ellipses which are flattened on two sides)

FONTS:

I have some base fonts- most likely your OS has some - but not standardized ones.
Many coreLibs, such as this one(SDL included) have a basic BMP font setup.
	This is not the same as the old CHR font format.

I can provide a few for SDL v1 if needed.

HELP??
Yes, please. I need some help.


SDL and JEDI have been poorly documented.

1- FPC code seems doggishly slow to update when bugs are discovered. Meanwhile SDL chugs along in C.
(Then we have to BACKPORT the changes again.)

2- The "fpc /lazarus"  units changes as per the HOST OS.
Usually not a problem unless doing kernel development -but its an objective/core Pascal nightmare.


libUOS support adds "wave, mp3, etc" sounds instead of PC speaker beeping
(see the 'beep' command and enable the module -its blacklisted-on Linux)
-sox /libsox provides the rest.

If fixed the linkage (I hope) on these- and put the units where the belong.
- uses clause should build this unit in now. UNTESTED.

* the libraries do build(and work), however.

SDL_Audio "licensing issues" (Fraunhoffer/MP3 is a cash cow) -as with qb64, which also uses SDL- wreak havoc on us.
(Yes- I Have a merge tree of qb64 that builds, just ask...)

-The workaround is to use VLC/FFMpeg or other libraries. libLAME (et al) did parallel development on certain codecs.


LFB:

Linear Framebuffer is "virtually required" and bank switching has had some isues in DOS.

A lot of code back in the day(20 years ago) was written to use "banks"(MMU) due to CHIP limitations.
The dos mouse was broken due to bank switching (at some point).
The way around it was to not need bank switching-by using a LFB. This required 486+ processors and MMUs - as well as 32bit addressing.
The Amiga does some "weird quackery internally", but makes 32bit addressing available.

HUGE difference between a "business machine" trying to run games, and a "end user gaming machine" doing it.

I will assume that you know what this is.
I will not be reprogramming "bankswitching routines".


CGA and EGA have been properly "emulated" by palettes. (This is not planar, its faked planar)
Your standard "Text mode hack" (16 colors) is "EGA palette emulation".

Why the funky graphics. You had 3 bits to determine color(IBM). Thats why.

Those pesky ASCII char graphics?
Anoying programmers tend to abuse the fact that the latter half of cp850(DOS) was "pre-graphics" graphics characters.
Theyre broken on unices because of this fact. (we can put them back ...)


**THIS CODE(and JEDI SDL source headers) have been written (tweaked) for 32 AND 64 bit systems.**
64-bit patches have been implemented.

Code assumes so far- we are PentiumII(1996 onwards).

USER FIXME: Error 216 usually indicates a "memory leak"- or flawed pointer address(i8086 to go32v2 code may exibit this behaviour).

SDL and OpenGL can fire errors with no debugging info- and leave you clueless as to where to start debugging.
(included SDL c units have not been compiled w debug support- this may change)
(yes, Im that confident in my SDL programming)

If you get Err 216, triple check your code, your pointers, etc- and that your translating the pointer definitions(data) correctly. (Likely you are not.)
(P may need to "point" to a "pointer" of an array of X, not "just point there".)
----

Colors are bit banged to hell and back. This is how its alwasy been.
24 bits seems to be the SDL1 limit here, according to what Im reading.
* NOT SO WITH x11!!!! *

Below 15bpp is an LUT emulated 24 bit array
15 and 16bit color uses word storage(16bit aligned for speed on older systems)- padded(shitfted?) to 32bits.
24 bit is an RGB tuple -padded to RGBA. A-bit is ignored, as with 15bit color(555).

The 32bit alignment will play a role, especially later on using OpenGL.
*DO NOT IGNORE THIS FACT. I AM ALIGNING THE CODE FOR ThiS REASON*

Color data is as accurate as possible using data from the net. Presumed shifting (between modes) might not yield "perfect" color shifts.

Color shifting is really HERMES library(internal to SDL), theres a few others. Im only providing "basic" functionality.
* libPTC, included, also uses libHERMES.

Downsizing color bits:

-you cant put whats not there- you can only "dither down" or "fake it" by using "odd patterns".
by tricking your eyes with "almost similar pixel data".

most people use down conversion routines to guess at lower depth approx colors.

What this means is that for non-equal modes that are not "byte aligned" or "full bytes" (above 256 colors)
you have to do color conversion or the colors will be "off".

Futher- forcing the video driver to accept non-aligned data slows things down-
        in case of opengl- it WILL crash things.

15/15bpp shifts aim to be accurate, can be approximated. Most of the rest I have taken care of.

---

Something to be aware of:

Most Games use transparency(PNG) for "invisible background" effects.
-While this is true, however, BMPs can also be used w "colorkey values".
- Often theres a min 4 layers to what youre looking at.

-for example:

SDL_BlitSurface (srcSurface,srcrect,dstSurface,dstrect);
SDL_SetColorKey(Mainsurface,SDL_TRUE,DWord);


PNG can be quantized-
Quantization is not compression- its downgrading the color sheme used in storing the color
and then compensating for it by dithering.(Some math algorithms are used.)
(Beyond the scope of this code)

PNG/PNM itself may or may not work as intended with a black color as a "hole color".This is a known SDL issue.
PNG-Lite may come to the rescue- but I need the Pascal headers, not the C ones.


Not all games use a full 256-color (64x64) palette-
	This is even noted in SkyLander games lately with Activision.
	"Bright happy cheery" Games use colors that are often "limited palettes" in use.

Fading is easy- taking away and adding color---not so much.
However- its not impossible.

TO FADE:

    you need two palettes for each color  :-)  and you need to step the colors used between the two values
    switching all colors (or each individual color) straight to grey wont work- the entire image would go grey.
    you want gracefully delayed steps(and block user input while doing it) over the course of nano-seconds.


I havent added in Android screen modes (depths) yet. These need to be device restricted. Same with Amiga.

---

**CORE EFFICIENCY FAILURE**:

Pixel rendering (the old school methods)
Every Pixel used to be checked for XOR,NOR,NOT,OR...etc. That code has been removed. Dont expect it to work. SERIOUS SLOWDOWNS.

I expect YOU to precompute color shifts (I can help)- especially for larger areas on screen.
ops are restricted, even yet to "pixels" and not groups of them.  WRONG. Speed things up.

-UNLESS THIS IS YOUR INTENT-


Texture/Lighting is a "pixel-based operation". If you use heavy TnL, expect CPU slowdowns.
This is often the case for high CPU requirements in games these days. (offscreen math involved before VRAM loading)

MessageBox:

InGame_MsgBox() routines (OVERLAYS like what Skyrim uses) still needs to be implemented.
FurtherMore, they need to match your given palette scheme. (I can only do so much, I can match the default bpp...)

GVision, routines can now be used where they could not before.
GVision requires Graphics modes(provided here) and TVision routines be present(or similar ones written).
GVision "hook overrides" TVision, much like an exit handler.


FPC team is refusing to fix TVision bugs.

        This is wrong.

That is like saying:
	FPC DEVS dont support Win16 apps and programming(they do now).


Logging will be forced in the folowing conditions:

    under windows with LCL(no terminal)
    under linux with LCL compiled in
	Terminal(tty) ox XTerm calls us

Code has been upgraded from the following:

	original *VERY FLAWED* port (in C) coutesy: Faraz Shahbazker <faraz_ms@rediffmail.com>

	unfinished port (from go32v2 in FPC) courtesy: Evgeniy Ivanov <lolkaantimat@gmail.com>

	some early and or unfinished FPK (FPC) and LCL graphics unit sources
	SDLUtils(Get and PutPixel) SDL v1.2+ -in Pascal (and C) --where is this?

    JEDI SDL headers(unfinished)

    libSVGA Lazarus wiki : http://wiki.lazarus.freepascal.org/svgalib
    libSVGA in C: http://www.svgalib.org/jay/beginners_guide/beginners_guide.html

    StackExchange in C/CPP (source locations documented)
    my own "down the rabbit hole" cobbled code for X11 (based on online GPL demos)

    MonsterSoft staff- "Object Graphics": http://www.monstersoft.com/objgfx/ (albeit old, the ideas presented apply equally well today)

	Guido Gonzato (PhD) "fucked up" implementation of SDL2BGI - flaws are in the Input APIs (eventHandler).
                Key and mouse inputs are supposed to be "pass thru" the routine(in a "cpu register" like fashion).
                		(you dont process input in the event handler, you set flags- and you certainly do not CALL the eventHandler--"it calls you".)
                The code is otherwise, mostly sane.
				(Mind you, this is SDLv2 only)

   GraphicsVision API(will try to tweak for our use)

manuals:
    SDL1.2 pdf
    Turbo Pascal Quick Reference by QUE Publishing ISBN 0880224290
    TCanvas LCL Documentation (different implementation of a 'SDL_screen')
    Lazarus Programming by Blaise Pascal Magazine ISBN 9789490968021
    Getting started w Lazarus and FreePascal ISBN 9781507632529
    JEDI chm file
	TurboVision(TVision) references (where I can find them and understand them.)
    VESA Specification manual(s) v1-3
    "X11Core" online documentation(X Consortium)

Animations:
to animate- screen savers, etc...you need to (page)flip/rendercopy and use DELAYED rendering timers.


bounds:
   cap pixels at viewport
   off screen..gets ignored.
   zooms need to handle the extended size to accomadate "for the zoom"


on 2d games...you are effectively preloading the fore and aft areas on the 'level' like mario.
there are ways to reduce flickering -control renderClear/Flip.

the damn thing should be loaded in memory anyhoo..
what.. 64K for a level? cmon....ITS ZOOMED IN....

  Move (texture,memory,sizeof(texture));

or Better yet:

  Move (texture,memory,sizeof(GraphicsBuffer));

You could write Wolf3D, DukeNukem- or similar games in a few weeks by porting the C(Vectors/GL) in one of the demos. Note that there isnt any collision code in there.
It takes much longer to learn how to write for SDL/X11 than this.

Your hardest time will be creating game sprites and levelmaps.
(Theres a few tricks for embedded systems like the megadrive, etc--very old loading and obj file tricks-embedded zlib compression...etc)

NOTE:

Some SDL BUGS exist - because of stupidity and BS programmers not understanding the material.
"EVERYTHING IS MUTABLE"

SDL TEAM:
    "You must render in the same (mainloop) that handles input." (misunderstood)

10: Process Input
20: Render frame
30: goto 10

I have EXPORTED THE GRAPHICS CONTEXT (its just a pointer).
SEE THE DEMOS.

SDL trick:

The input handler routines(CPP) are written in such a manner that the end programmer has ultimate control over the users input.

This can only happen in one way- you pass it thru to be processed(store it somewhere), then wait for the next event to fire.
In the meanwhile, you should be tripping flags all over the place.
Routines need to be called based on those flags, at the end of the eventHandler "procedure". (if input)

SDL routines require more than just dropping a routine in here-
    you have to know how the original routine wants the data-
        get it in the right format
        call the routine
        and if needed-catch errors, otherwise drop the routine's output.(not everything is a C function, nor requires checks)

- it almost reeks of assembler, however assembler would be faster. (15x according to some)
SDL is not(despite lousy C), written IN ASSEMBLER.

Since we ARE compiling to assembler- Ive taken the liberty of a few "compiler tweaks based upon archetecture".

X11 pops HARD, NASTY errors at the oddest times,( as does GL) so dont ignore the "safety checks".


The biggest problem with SDL is "the bloody syntax".

SDL is not SIMPLE.
The BGI was SIMPLE.
GL and X11- believe it or not, for all that power down there, ARE SIMPLE.


The LCL makes its own (where the hell is it?) window handle, input event handler, and processing (groups of) functions.
-look for OnKeyDown()

If you dont know where your routines HOOK an application, THEN WHY ARE YOU WRITING/USING THEM?
You should also be able to trace this. I can trace mine. WHY CANT YOU TRACE YOURS?

youre reinventing the wheel w GTK,KDE,etc...
the time it takes to trace a program thru its steps(just to init) takes ages now.

ARG!

--Jazz (comments -and code- by me unless otherwise noted)

}

//OSX:  this requires some help compiling "nib-less  SDL routines" and the "SDL framework".
//OSX developing NEEDS MORE INVESTIGATING.
//anyone with an older HackinTosh(dont ask how to do it- you have one or you dont), an old MacMini, or an old iBook- volunteer here.

{$IFDEF UNIX}
    //this is a UNIT...
    {$ifdef cpu64}
      {$PIC ON}  //position independent code, allows relocation. Security feature on unices.
      //however, Ive noticed that on my iBook(G4) that "Tux" doesnt like PIC code, nor is relocation- even for the kernel- available.
      //this may be because Im not using MintPPC(buster+)? Ill need to do some "drive tweaking with QEMU-ppc" and "switch physical disks" to confirm.
      //MintPPC works(use the disk image DL) under QEMU. Radeon HW is a PITA with above Jessie- so maybe itll work? QEMU doesnt emulate this.
    {$endif}

    {$IF defined(DARWIN)} //OSX
//Hackish...Determine whether we are on a PPC by the CPU endian-ness since we know
//that support stopped at 10.5 for PPC arch- and switched to Intel.
    {$linklib gcc} // The whole point is to use a Pascal library-- or hook the C headers.

    {$IFDEF ENDIAN_BIG} //BIG BOYS are PowerPC g3/g4/G5s. IFFFF MAYBE ... POWER9 TALOS in emu modes.
        {$DEFINE CARBON} //some confusion here CoCoa is a native app; Carbon is 8/9 - to 10 api.
        {$linklib SDLmain.o}
        {$linklib SDLimg}
        {$linklib SDLttf}
        {$linklib SDLnet}
        {$linkframework Carbon}
        {$modeswitch objectivec1}

    {$ELSE} //little endian=> OSX 10.5(Leopard-- thru MOJAVE). This excludes PPC arch.
       {$ifdef i686} //I dont support M1. Intel APPLE.
        {$DEFINE COCOA}
        {$linklib SDL2main.o}
        {$linklib SDL2img}
        {$linklib SDL2ttf}
        {$linklib SDL2net}
        {$linkframework Cocoa }

        //dont define X11 here- X11 by itself is ugly due to no WindowManager(kde,gtk,etc)

        //This is usually a hack- to use Linux/X11 apps natively without much code rewrites, in OSX.
        //Id prefer to use native Carbon/Cocoa APIs.
        //May require manual start of the X11/XOrg app by the user.

        //X11 on OSX is just bad code these days, Quartz and Carbon/CoCoa APIs are much better.

        {$modeswitch objectivec2}
       {$endif}
    {$ENDIF}

{$ENDIF} //darwin

{$ENDIF UNIX}

uses

//Threads requires baseunix unit. Baseunix is UNIX. Not windows. Not applicable outside of UNIX.
//(A lot of people dont understand this)
//THERE SHOULD NEVER BE A baseunix unit for windows. NEVER!


//cthreads has to be the first unit in this case-fpc so happens to support the C version.
//we are linking with C units, not writing an independent unit.

//This may be where function calls are getting muffed up.

//Theres some chatter about avoiding OpenGL/DirectX/DirectDraw(mac)- and to use GDI/GDI+/X11Core instead.
//The issue is that those routines are not designed for multimedia or heavy game graphics- there will be massive slowdowns in places if you do that.

//libPTC should be an absolute fallback, however devs are aware at least of the hermes libs. This may be how we "get out of SDL".
//cairo is presenting as-if its the "bees knees", but asks you to use SDL in some circumstances??

{$IFDEF UNIX} //if coming from "dos era" programming experience - use sysutils and baseunix instead of the dos unit.
      cthreads,cmem,
      //pthreads ?? sdl headers seem to think this is ok....

      unix,sysUtils,


{
    I dont know if internally(yet) if SDL uses X11CorePrimitives. It appears SDLv1 did, then they saw the same thing I just saw....
    -where X11 can hook OpenGL with minimal code changes...(and ran to Grandmas house for good).

    That doesnt make OGL BETTER than X11Core. The code is more portable, however, not necessarily BETTER.
    SDLv2 IS NOT- and will never be-- OPENGL. Theres plenty of reasons to keep these two kiddies seperate.

    You have some options on UNICES:
		X11
		Qt/Cairo/GTK(some say no X11 needed...)
		OpenGL

    X11CorePrimitives are "effectively the BGI under Unices". It would not be wise to forget.
    Its just that nobody in FPC Dev team has (EVER??) hooked the routines.
    (Theyve always been there- and apparently a LOT more powerful than most have realized.)

    X11 specific methods can be used to remove SDL overlayer code.
    I dont know enough of the lower-level programming to yank SDL completely yet. But this looks like the direction were going.

    NO, the LCL does NOT use CorePrimX11, it uses GTK/QT interfaces(a reimplementation of same- but at window manager level, not OS "graphical" level)
    It may be possible to work around LCL issues due to this fact. If so, Im all ears(YOU HAVE MY ATTENTION).

   The beauty of X11CorePrim is that I dont have to isolate BSDs...most unix flavors utilize X11 in some manner. (More modern OSes utilize the XOrg accellerated variant.)
   I dont care about wayland, yet, but it greatly simplifies things from what Im reading.
   YES- your RasPi WILL run X11. Quite well, actually. Mailbox acceleration on the console(sans X11) as well. (an AV thing, apparently?)

}


  {$IFDEF COCOA}
     CocoaAll,  //found in: cocoaint "fpc src package". Likely installed, but buried.
  {$ENDIF COCOA}
  
  {$IFDEF DARWIN}
    CarbonPrivate,
   {$ENDIF}
{$ENDIF} //unices

//ctypes: cint,uint,PTRUint,PTR-USINT,sint...etc.
//classes: needs mode fpcobj love -ensure the object-mode define attaches error handlers(and so we can also use them)

    ctypes,strings,strutils,

{$ifdef lcl}
    classes, objects,  //lclintfepiktimer ??  with gameDev- objects(instantiated)+Timers
{$endif}


//THIS WILL BE SLOW! DX and GL APIs are implemented in software. (blame the HW devs)

{$ifdef go32v2} //486+ 32bit DOS ENVIRONMENT!!  (THIS IS NOT "TRUE DOS"!)
    //VESA LFB needs to be accessible. VESA BIOS v2 min. Win32 support enabled ONLY with HXDPMI(DPMI v1 supported).
    //build alongside the binary files, do not try to rewrite the sources.

    {$define win32} //yes, win32 core api functions are allowed here.
    Dos,crt,go32,  //hxdpmi : substite for go32?
    {$define ForceSWRender} //I can check this flag later on.  Windows/Unix uses "proprietary drivers", not VESA. Still decent for most tasks.
{$endif}

// we could use libGraph here, except we are reimplementing it. Most routines would conflict.

  // AMIGA:

  // AMIGA uses DOS, but is in GFX mode by default. There is no "DOS MODE" with an amiga. Its like Mac OS9.
  //remember: ALL Amiga(s) uses DOS at its core(FS). Hardware could be one of the following: x86(AROS), PPC G4(morph), m68k(Actual older amiga- A500,A1k, usually)
  //while version 3 can target A500 and 1k systems(m68k based), youll have a "helluva time" getting them to properly emulate. furthermore, you are 256 color limited
  //AMiGAXL and AMITHILON suffer from a "this is simulated m68k hardware" issue, where drivers are non-existant, or very hard to figure out. Either run AMIGA OS on m68k (or morph on PPC) , or use AROS(via qemu/AMD FX core booted in bios compatibility mode).

  //Unless you have an actual AMIGA- dont write for less than version 4.

//core: but we may not need all of this for SDL. (As with X11)

//cybergfx+ is the intended target. See the SDL demos inside AROS.
{$ifdef hasamiga}
        agraphics,amigados,console,cybergraphics,diskfont,input,inputevent,timer,crtstuff,
{$endif}

{$IFDEF UNIX}
    {$ifndef lcl} //if AppType= GUI then ignore the "output window"..Lazarus is supposed to take care of that- "but youll never see it".
    //never presume.

     //this provides "console experience" in a window.

        ncrt,crtstuff, //ncrt uses the ncurses library(simplified). Thread safe and GL compatible.
        //actual pc speaker module on unices is there, for HW boards(towers) that support it. This is BIOS POST diagnosis port, not soundcard.
        // older (32bit bios) boards still use this. (RoG 64 UEFI?)

        //libPasnCurses?
    {$endif}
{$ENDIF}

{$IFDEF windows} //as if theres a non-MS WINDOWS?
        Windows, // MMsystem,   <-- DirectX preferred. MMSystem is basic (MS?)wav file support. Not sure if this is what we want. EVER.
        //(FLAC is the new WAV...)

    {$ifndef lcl} //readkey/readln/writeln doesnt work in a LCL app(get used to it).

		wincrt, // dont tap DOSs crt routines, use "fakeDOS" terminal or "Powershell" ones, for "WinDOS" (7+).
		//note: sound/nosound arent correct. Its supposed to function like sox's "TONE method"(midi synth).

		//on that note:

        //midisynth is available(even in win10), its a "windows media player extension", overridden by some (free) midi apps
        //libvlc can hook the free soundfonts, but the config option is buried. (can we hack this support in?)

        //libuos may be able to "fill in the gap" here.

		//also: readkey is like "pulling xEvent from x11", although the behaviour is the same.
		//LCL uses "instantiated routines" instead. You have to tap OnEvent() and similar handlers.
		crtstuff,
    {$endif}
{$ENDIF}


//FIXME(planned): missing sprites(blits), tiles,tilemaps, parallax scrolling--
// and BaseGame object code (you need to write most of this)

//yes, I am pulling (generic abstraction) from  the 3D demo.

  typinfo,logger,math,{$if defined (linux) or (windows) or (go32v2)}printer{$endif},uos, //appears to be LPR support. OSX uses Postscript ONLY. FPC BUG: multiple OS versions in the RTL.
  base64, //tilemaps and tiles

  //note: libuos uses portAudio, not PulseAudio. HUGE difference.
  //should work w antiX, and noSystemD debian clones ok.

//Sox can give us a beep(via commandline intervention).
//Its "good enough" for our needs.


//hardware specs limit use of SDL2, and thereby SDL.   Optimize code as much as possible.
//the compiler sets this flag but can be wrong(32 on 64 bit OS)--use the build scripts!

//currently tweakng the MakeFile. Need to account for FPC default locations(UGLY!) and OS Targets. Building by hand, for now.

//im assuming the compiler knows if these are pulled in, that we want optimizations.
//There are other tweaks that I cannot specify here.

  {$IFDEF CPU32}
          {$ifdef i686}
                  //PentiumII  presumed. Let the compiler setup the FPU(or emulation) in earlier CPUs.
                  mmx,
          {$ENDIF}
            
          //No Altivec tweak...
           SDL  //SDLv1 had this weird thing-- all of the headers are in one unit.

  {$Endif}

         //min requirement should be sse, unless you can determine better than I- that you CAN use sse2. (Least common denominator)

  {$IFDEF CPU64} //g5, TALOS Power9 TALOS, modern X86_64

      {$ifdef x86_64} //intel64, not powerpc64
                      sse,  //intel64 should have avx support, however ue to weird quirks "optimal code" may be faster
      {$ENDIF}

  //ppc: altivec??


         SDL2,
         SDL2_ttf, //freeType, TrueType
		// sdl2_net,  --jedi headers not merged yet

		 SDL2_Image

        //sdlMM
        //sdlKitchenSink (vlc?)

         //sdl_gfx is being reimplemented.
         //sdl_gpu needs to be looked at.

  {$ENDIF}


//lets be fair to the compiler..
{$IFDEF debug} ,heaptrc {$ENDIF}


//leave this dangling
;

{
NOTES on units used:

ptc DOES NOT support non-X11 environments.
It does support dos, which is a good starting point.


DOS:

Dos actually had(unimplemented TSR hook) threads. Multitasking was very primordial soup(int33?),
but as with OS9- it DID exist.

Games have managed to implement pc speaker sounds, (or background music) and draw...so its possible.
Probably implemented this way, as well.

Need an example? DUCKTALES(IBM XT 8088/86). LOUD AS HELL. Very very well programmed.

PC speaker Hardware hack(on hardware with a Pc speaker pin):
	take two female arduino "developer" wire and tap female headphone jack wires.splice together.
	(headphone red+white splice together to signal wire)
	* enable the beep unit and "fire it off".
	* also works for "POST debugging" on boot.


FPC bug:
crt is a failsafe "ncurses-ish"....output... use ncurses with graphics modes..its better supported.
DO WE NEED TO REWRITE crt calls to use ncurses unit? LIKELY!!! YIKES.

Note:

ptc.console is NOT the CRT unit. DO NOT TREAT IT THE SAME.

crtstuff is MY enhanced dialog unit
    I will be porting this and maybe more to the graphics routines.


FPC Devs: "It might be wise to include cmem for speedups"
"The uses clause definition of cthreads, enables theaded applications"


mutex(es):

The idea is to talk when you have the rubber chicken or to request it or wait until others are done.
Ideally you would mutex events and process them like cpu interrupts- one at a time.

You will see the use in a moment.
This is like  "critical code sections".

sephamores:
these are like using a loo in a castle-
only so many can do it at once, first come- first served
- but theres more than one toilet.

---

Modes and "the list":

These are "signed ints" because we cant have "the negativity"..
could be 5000 modes...we dont care...
the number is tricky..since we cant setup a variable here...its a "sequential byte".

yes we could do it another way...but then we have to pre-call the setup routine and do some other whacky crap.

"ins and outs" (and hacks) are replaced by a better equivalent.

Y not 4K modes?
1080p is reasonable stopping point until consumers buy better hardware...which takes years...
most computers support up to 1080p output..it will take some more lotta years for that to change.

primary reason: SDL1 stops at 32bit longwords. It was never designed for 64bits.
Reason being: systems didnt exist yet.

SDL1 was further limited(by hardware) to less than 1600x1200 resolution.

scale output(via SDL) if resolution is lower then the window canvas size-or fullscreen.
typically, interlacing is used - "to cheat"

if you dont want to upscale(fuzzy)- set "fat pixel mode" at intended resolution (and draw with neighbors).

 GL doesnt support less than 24bpp.

below 320 will probably be stretched anyways, the canvas is too small on modern screens.(Let me get to this)

MAC: 640x480,800x600,1024x768 up to 24bpp is supported
droids (and pis) will have to have weird half-resolutions and bpps added in later on.

The Pi does support native TV resolution output.
TrueColors modes (>24bpp) produce one problem: We can no longer track the maximum colors , unless using ASN number system(hi-precision calculations)


BitDepth	Colors

1bit B/W = 2
2bit CGA = 4
4bit EGA = 16
8bit VGA = 256
15bit = 32k
16bit = 64k
24bit = 16M

SDL2/X11/GL only:
- x11 uses a 256 multiplier(per byte),some form of "stepping"

30bit (half assed implemantation by industry)
32bit (multiple implementations by industry) = 4.2B -or (16M+blends)

}

// {$fontdata.inc} pull in font data(from the old fpc graphics unit), I plan on adding in Bitmapped fonts for SDLv1.
//SDLv2 can use FreeTYpe/TrueType. As can X11.
//90s style games often used bitmapped fonts(PSO I/II). Jagged edges are the dead givewaway.

type
//this is as compatible as I can make it, we dont care "which standard" has the mode, just that it exists and works.
	graphics_modes=(

m160x100x16,
m320x200x4,m320x200x16,m320x200x256,
m640x200x2,m640x200x16,
m640x480x16, m640x480x256, m640x480x32k, m640x480x64k,m640x480xMil,
m800x600x16,m800x600x256,m800x600x32k,m800x800x64k,m800x600xMil,
m1024x768x16,m1024x768x256,m1024x768x32k,m1024x768x64k,m1024x768xMil,
m1280x720x256,m1280x720x32k,m1280x720x64k,m1280x720xMil,
m1280x1024x16,m1280x1024x256,m1280x1024x32k,m1280x1024x64k,m1280x1024xMil,
m1366x768x256,m1366x768x32k,m1366x768x64k,m1366x768xMil,
m1920x1080x256,m1920x1080x32k,m1920x1080x64k,m1920x1080xMil);

//This record is set  in the InitGraph routine.
//-therefore these need to pop out.
var
    bpp:byte;
    PalettedMode:boolean;
  	MaxX:Word;
    MaxY:Word;
 	MaxColors:Word;
{$IFDEF UNIX}
  {$IFDEF DARWIN} 	
  MacWin: WindowRef;
  {$ENDIF}
{$ENDIF}
type
    
Tmode=record
	ModeNumber:byte;
  	ModeName:string;
	XAspect:byte;
	YAspect:byte;
	AspectRatio:real; //things are computed from this somehow...dos, libptc calculations are waaay off.
end; //record


//centeredHLine: (((x mod 2),(y mod 2)) - ((x1-x2) mod 2))
//direct center of screen modded by half length of the line in each direction.
//For verticle line: use y, not x


//PIXELS!!!!
//how BIG is a pixel? its smaller than you think.


//You are probly used to "MotionJPEG Quantization error BLOCKS" on your TV- those are not pixels.
//Those are compression artifacts after loss of signal (or in weak signal areas).
//That started after the DVD MPEG2 standard and digital TV signals came to be.


//when drawing a line- this is supposed to dictate if the line is dashed or not
//AND how thick it is. Centered is not what you think it means.

  LineStyle=(solid,dotted,center,dashed);  //alternated background drawing.
  Thickness=(normalwidth=1,thickwidth=3,superthickwidth=5,ultimateThickwidth=7);

//LineStyle and ThickNess are usually used together along with PutPixelHow (andPut, NotPut,etc) methods.

//It is extremely inefficient(unwise) to manipulate pixels in such a way otherwise.
//This is a very confusing chunk of borland API that was never intended to be "a public method".

//stipple is what "dashed lines" are called. Like the "stipple on your face".

//most (GL) implementations of this are piss poor.

{
SURFACE:

This is a "buffer copy" of all written string to the screen.
As long as we have this- we can print, and we can copy screen contents.

There is a difference betweek MainSurface(screen) and a back-buffer. MainSurface(SDLv1) is always on screen, as-is "the Renderer".
The renderer needs(gl pipeline) textures(from surface data) pushed to it.

Clearing the backbuffer(double buffering) does NOT clear the screen, it prepares the window for the next surface update.
(I got confused by this at first) . Flipping, updating rects, and renderPresent- takes care of that.

You are used to single buffered (or directRendering function calls) with DOS-ish programming methods.
Double buffering is the "way to go". (mesa: offscreen rendering)


where did writeln go?
- use OutText / OutTextLn , or better yet, the log unit.

Think of Text like a "Transparent Label" or "blitted clear sticker". It sits on top of everything, like a texture.

BitBlits is a special method of FLIPPING.
}

//We arent using bytes for screen resolutions anymore. If you see byte values, its old code(< 90s).


  //var   polypoints: array [0..65535] of points;

  TArcCoordsType = record
      x,y : word;
      xstart,ystart : word;
      xend,yend : word;
  end;

//graphdriver is not really used half the time anyways..most people probe.
//these are range checked numbers internally.

	graphics_driver=(DETECT, CGA, EGA, VGA,VESA); //cga,vga,vesa,hdmi,hdmi1.2 ..... technically. ignored. VESA got abandoned for "proprietary drivers"

//(Cross)HATCHes:

//This is a 8x8 (or 8x12) Font pattern (in HEX) according to the BGI sources(A BLITTER BITMAP in SDL)
//X11- at least, has this already implemented. Partial GL support(2D hud)

//This was only meant for CGA modes. (on green/orange monitors)
//**It was not meant for color modes. Many people erroneously still use them in "color modes"**

//I threw in some custom blits here
   FillSettingsType = (clear,lines,slashes,BackSlashes,rhombus,plus,divide,widePTs,DensePTS);

//For color modes use Blends, or blits - with a "hole pattern in them"

//for hashes, we might be able to get away with a "font hack". They are generally of fg/bg color combos. For BLITS, maybe not so.

{$ifdef cpu64}
SDL_Color=record
	r,g,b,a:byte;
end;
{$endif}

{$ifdef cpu32}
//SDL always defines a 32bit(packed) record. Truth is: v1.2 uses 24bpp
SDL_Color=record
	r,g,b,bs:byte; //sdl v1 ignores the "alpha bit". BLITs and YUV OVerlay(motion video?) will have to be removed/reimplemented.
end;
{$endif}

var

  ArcCoordsType:TArcCoordsType;
  
//Pages:
// A video page can be considered a backbuffer. This is an SDL_Surface, which is not visible.

// Only four are ever allowed, only THREE are ever implemented, using TWO is most common these days.

//these arent 3D coordinaates, but think of "multiple 2D planes". You flip betwee them (with SDL).
//(up to 4mb VRAM each)

    VideoPage: array of SDL_Color; //A surface: [0..MaxX,0..MaxY]

//DummyPalette: SDL_Colors; //multiple, by array without using: array of SDL_Color
   
//ViewPorts:
//A Ton of code enforces a viewport mandate-  even sans viewports- the screen is one. [0,0 to MaxX, MaY]
//This is either the full screen, or an "overlay"(a section thereof), like a minimap, or "fixed in game menu"- like in the game Starflight.

//coordinates are used here,  buffers to remove them.

  TermProc: TTerminateProc; //atexit
  thick:thickness;
  
//  mode:PSDL_DisplayMode;

//I doubt we need much more than 4 viewports. Dialogs are handled seperately(and then removed). Dialogs for DOS did exist.
//AMIGA: A popup like with win31/os2 warp? (yeah, its that oldschool)

  vpBounds: PSDL_Rect; //limit viewports to 0,0-MaxX, MaxY
  windownumber:byte; //>4 is oob
  somelineType:thickness;

//you only scroll,etc within a viewport- you cant escape from it without help.
//you can flip between them, however.

//think minimaps in games like Warcraft and Skyrim


{

INPUT:

This is like "interrupt based kernel programming"- DO NOT code as if "waiting on input"-
    pray it happens, continue on- if it doesnt.

These are the types of events.

	KeyDownEventDefault:KeyDownEvent; //dont process here
	PauseRoutineDefault:PauseRoutine;
	ResumeRoutineDefault:ResumeRoutine;
	escapeRoutineDefault:escapeRoutine;  // a 'pause' button??

	KeyUpEventDefault:KeyUpEvent; //should be processing here

	MouseMovedEventDefault:MouseMovedEvent;
	MouseDownEventDefault:MouseDownEvent;
	MouseUpEventDefault:MouseUpEvent;
	MouseWheelEventDefault:MouseWheelEvent;

    JoyStick something something something
    JoySTickHat something something something
    SendJoyStyickHapticFeedBack something somethine something

	MinimizedDefault:Minimized; //pause
	MaximizedDefault:Maximized; //resume
	FocusedDefault:Focused; //resume
	Lost_FocusDefault:Lost_Focus; //pause

The newer SDL2 BGI rewrite (from the professor) is shit. THats not how you "call the event handler".
** MOF: dont call it, it calls YOU **

}


const
   //Analog joystick dead zone -joysticks seem to be slow to respond in some games....
   //like the Descent I/II rewrites. Freespsace also?

   JOYSTICK_DEAD_ZONE = 5000;

type
//new: Tris.
  Points=record
     x,y:Word;
  end;
  TriPoints= array[0..2] of Points;

var

    Xaspect,YAspect:byte; //4:3 16:9 but the ratio is a REAL number.

//are these two used?
    eventLock: PSDL_Mutex;
    eventWait: PSDL_Cond;

//    palette:PSDL_Palette;
    quit,minimized,paused,wantsFullIMGSupport,nojoy,exitloop:boolean;
    nogoautorefresh:boolean;
    x,y:word;

    gGameController:PSDL_Joystick;
    {$ifdef cpu32}
        MainSurface: PSDL_Surface; export; 
    {$endif}
    FontSurface:PSDL_Surface; //Fonts and TnL mostly at this point

    srcR,destR,TextRect:PSDL_Rect;
    //rmask,gmask,bmask,amask:longword;

    TextFore,TextBack : PSDL_Color; //font fg and bg...

    filename:String;
    fontpath,iconpath:PChar; // look in: "C:\windows\fonts\" or "/usr/share/fonts/"

{

Fonts:
Most OSes have a default of:

    Serif
    Sans(Serif)
    Gothic
    Terminal(Code)
    Tri-Plex

and as a result, some basic (TTF, mostly) fonts are also included. (Royalty FREE-in case youre wondering)

These were originally compiled in fonts, like object code.

}

    font_size:integer;
    style:byte; //BOLD,ITALIC,etc.
    outline:longint;

{
older modes are not used, so y keep them in the list??
 CGA because well..I think you KNOW WHY Im being called here....

 mode13h(320x200x16 or x256) : EXTREMELY COMMON GAME PROGRAMMING
 (we use the more square pixel mode)

Atari modes, etc. were removed. (double the res and pixelSize and we will talk)
* AMIGA support is planned. I dont have an amiga, its emulated. I DO HAVE a PPC ibook(MorphOS), and AROS -under qemu-i386.

}

  ClipPixels: Boolean=true; //always clip, never an option "not to".

  WantsJoyPad:boolean;
  screenshots:longint;

//CDROM access is very limited. LibUos tries to fix this.

//This is Used mostly for Audio CDs and RedBook Audio Games.
//such games have "CDROM Modeswitch delays" in accessing data while playing audio tracks(game skippage).

//example: Descent II(PC) and SonicCD(via PC and SEGA CD Emu)
//you would want ogg or mp3 or wav files these days- on some sort of storage medium.
//the Dreamcast titles (by Sonic Team(TM) and SEGA(TM)) try to address this.

  NonPalette, TrueColor,WantsAudioToo,WantsCDROM:boolean;
  Blink:boolean;
  CenterText:boolean=false; //see crtstuff unit for the trick

  _fgcolor, _bgcolor:DWord;	//this is modified due to hi and true color support.
  //do not use old school index numbers. FETCH the "index based DWord" instead.
  IndexedColor:Byte;

  flip_timer_ms:Longint; //Time in MS between Render calls. (longint orlongword) -in C. (~16ms)

  //ideally ignore this and use GetTicks estimates with "Deltas"
  //this is bare minimum support.
  //both timers and callbacks can be implemented better.

  //Threads...lets make a sweater. DOS CANT USE THREADS. It CAN use "task switching". (early windows INTEL CPU hack?)
{$ifndef go32v2}
  EventThread:Longint; //fpc uses Longint
  EventThreadReturnValue:LongInt; //Threads are supposed to return a error code(nasty C)
{$endif}

  Rect : PSDL_Rect;

  LIBGRAPHICS_ACTIVE:boolean;
  LIBGRAPHICS_INIT:boolean;

  ForceSWSurface:boolean;
  CantDoAudio:boolean; //will audio init? and the other is tripped NOT if in X11.
  //can we modeset in a framebuffer graphics mode? YES. on your hardware? unlikely.
  // ppc ibook running linux???

  Event:PSDL_Event; //^SDL_Event(x11 event)

  himode,lomode:integer;

  //Texture(surface)Format
  format:LongInt;


    {$ifdef cpu64}
        Renderer:PSDL_Renderer; export;
    {$endif}
    window:PSDL_Window; //beavis and butthead: A window... heh..."windows" he he...

    AspectRatio:real; //computed : (AspectX mod AspectY)


{

256 and below "color" paletted modes- this is the lengthiest piece of "rarely used" code , here.
You need (full) DWords or RGB data for TRUE color modes.

'A' bit affects transparency and completes the 'DWord'.
The default setting is to ignore it(FF). This is what is set.

You only change this for 32bit blends.

Most bitmap or blitter or renderer or opengl based code uses some sort of shader(or composite image).
This isnt covered here.


each 256 SDL_color= r,g,b,a whereas in range of 0-255(FF FF) per color.
for 16 color modes we use 0-16(FF) normally.

I use a LUT instead.

16 color mode(4 bit) is technically a wonky spec:
	officially this is composed of:  RGB plus I(light/dark) modes.
	in relaity this is: RGB+CMY colors whereas W+K colors are seperate- its not quite CMYK nor RGB with two intensities(7 or F)

64: 3f , 7f, ff, and 00 intensity -> this is shifted to a 32bit DWord with FF as A bit.

CMYK isnt really a video color standard normally because pixels are RGB.
CMYK is for printing.

The reason has to do with color gamut and other huffy-puff.
(Learn photography if you want the color headache)


CGA modes threw us this curveball:
	4 color , 4 palette hi bit resolution modes that are half ass documented.
	(think t shirt half-tones for screen printing)


VGA/SVGA  and VESA (video electronic standards association) modes are available now.
-of course SDL just simulates all of this (inside a window)


this is only for the default palette of course- if you muck with it.....
 and only up to 256 colors.


blink is a "text attribute" ..a feature...
(write..wait.erase..wait..rewrite..)just like the blinking cursor..


colors:

	MUST be hard defined to set the pallete prior to drawing.


with consts, we can specify an index color as a name and we will get the index, as it were.
(its also easier to copy palette colors with)
}

//auto detect is wonky. Use highest avail.
const
   maxMode=Ord(High(Graphics_Modes));

	BLACK=0;
	RED=1;
	BLUE=2;
	GREEN=3;
	CYAN=4;
	MAGENTA=5;
	BROWN=6;
	LTGRAY=7;
	GRAY=8;
	LTRED=9;
	LTBLUE=10;
	LTGREEN=11;
	LTCYAN=12;
	LTMAGENTA=13;
	YELLOW=14;
	WHITE=15;

//Greyscale colors dont have names.
//original xterm code must have these stored somewhere as string data because parts of "unused holes" and "duplicate data" exist

// I can guarantee you a shade of slateBlue etc.. but not the exact shade.
//(thank you very much whichever programmer fucked this up for us)

Grey0=16;
NavyBlue=17;
DarkBlue=18;
Blue3=19;
Blue4=20;
Blue1=21;
DarkGreen=22;
DeepSkyBlue4=23;
DeepSkyBlue6=24;
DeepSkyBlue7=25;
DeepSkyBlue3=26;
DodgerBlue3=27;
DodgerBlue2=28;
Green4=29;
SpringGreen4=30;
Turquoise4=31;
DeepSkyBlue5=32;
DeepSkyBlue2=33;
DodgerBlue1=34;
Green3=35;
SpringGreen3=36;
DarkCyan=37;
LightSeaGreen=38;
DeepSkyBlue1=39;
DeepSkyBlue8=40;
Green5=41;
SpringGreen5=42;
SpringGreen1=43;
Cyan3=44;
DarkTurquoise=45;
Turquoise2=46;
Green1=47;
SpringGreen2=48;
SpringGreen=49;
MediumSpringGreen=50;
Cyan2=51;
Cyan1=52;
DarkRed=53;
DeepPink4=54;
Purple4=55;
Purple5=56;
Purple3=57;
BlueViolet=58;
Orange4=59;
Grey37=60;
MediumPurple4=61;
SlateBlue3=62;
SlateBlue2=63;
RoyalBlue1=64;
UnUsedHole5=65;
DarkSeaGreen5=66;
PaleTurquoise4=67;
SteelBlue=68;
SteelBlue3=69;
CornflowerBlue=70;
UnUsedHole3=71;
DarkSeaGreen4=72;
CadetBlue=73;
CadetBlue1=74;
SkyBlue2=75;
SteelBlue1=76;
UnUsedHole4=77;
PaleGreen3=78;
SeaGreen3=79;
Aquamarine3=80;
MediumTurquoise=81;
SteelBlue2=82;
UnUsedHole1=83;
SeaGreen2=84;
SeaGreen=85;
SeaGreen1=86;
Aquamarine1=87;
DarkSlateGray2=88;
DarkRed2=89;
DeepPink5=90;
DarkMagenta=91;
DarkMagenta1=92;
DarkViolet=93;
Purple1=94;
Orange5=95;
LightPink4=96;
Plum4=97;
MediumPurple3=98;
MediumPurple5=99;
SlateBlue1=100;
Yellow4=101;
Wheat4=102;
Grey53=103;
LightSlateGrey=104;
MediumPurple=105;
LightSlateBlue=106;
Yellow5=107;
DarkOliveGreen3=108;
DarkSeaGreen=109;
LightSkyBlue1=110;
LightSkyBlue2=111;
SkyBlue3=112;
UnUsedHole2=113;
DarkOliveGreen4=114;
PaleGreen4=115;
DarkSeaGreen3=116;
DarkSlateGray3=117;
SkyBlue1=118;
UnUsedHole=119;
LightGreen=120;
LightGreen1=121;
PaleGreen1=122;
Aquamarine2=123;
DarkSlateGray1=124;
Red3=125;
DeepPink6=126;
MediumVioletRed=127;
Magenta3=128;
DarkViolet2=129;
Purple2=130;
DarkOrange1=131;
IndianRed=132;
HotPink3=133;
MediumOrchid3=134;
MediumOrchid=135;
MediumPurple2=136;
DarkGoldenrod=137;
LightSalmon3=138;
RosyBrown=139;
Grey63=140;
MediumPurple6=141;
MediumPurple1=142;
Gold3=143;
DarkKhaki=144;
NavajoWhite3=145;
Grey69=146;
LightSteelBlue3=147;
LightSteelBlue=148;
Yellow3=149;
DarkOliveGreen5=150;
DarkSeaGreen6=151;
DarkSeaGreen2=152;
LightCyan3=153;
LightSkyBlue3=154;
GreenYellow=155;
DarkOliveGreen2=156;
PaleGreen2=157;
DarkSeaGreen7=158;
DarkSeaGreen1=159;
PaleTurquoise1=160;
Red4=161;
DeepPink3=162;
DeepPink7=163;
Magenta5=164;
Magenta6=165;
Magenta2=166;
DarkOrange2=167;
IndianRed1=168;
HotPink4=169;
HotPink2=170;
Orchid=171;
MediumOrchid1=172;
Orange1=173;
LightSalmon2=174;
LightPink1=175;
Pink1=176;
Plum2=177;
Violet=178;
Gold2=179;
LightGoldenrod4=180;
Tan=181;
MistyRose3=182;
Thistle3=183;
Plum3=184;
Yellow7=185;
Khaki3=186;
LightGoldenrod2=187;
LightYellow3=188;
Grey84=189;
LightSteelBlue1=190;
Yellow2=191;
DarkOliveGreen=192;
DarkOliveGreen1=193;
DarkSeaGreen8=194;
Honeydew2=195;
LightCyan1=196;
Red1=197;
DeepPink2=198;
DeepPink=199;
DeepPink1=200;
Magenta4=201;
Magenta1=202;
OrangeRed=203;
IndianRed2=204;
IndianRed3=205;
HotPink=206;
HotPink1=207;
MediumOrchid2=208;
DarkOrange=209;
Salmon1=210;
LightCoral=211;
PaleVioletRed=212;
Orchid2=213;
Orchid1=214;
Orange=215;
SandyBrown=216;
LightSalmon=217;
LightPink=218;
Pink=219;
Plum=220;
Gold=221;
LightGoldenrod5=222;
LightGoldenrod3=223;
NavajoWhite1=224;
MistyRose1=225;
Thistle1=226;
Yellow1=227;
LightGoldenrod1=228;
Khaki1=229;
Wheat1=230;
Cornsilk=231;
Grey100=232;
Grey3=233;
Grey7=234;
Grey11=235;
Grey15=236;
Grey19=237;
Grey23=238;
Grey27=239;
Grey30=240;
Grey35=241;
Grey39=242;
Grey42=243;
Grey46=244;
Grey50=245;
Grey54=246;
Grey58=247;
Grey62=248;
Grey66=249;
Grey70=250;
Grey74=251;
Grey78=252;
Grey82=253;
Grey85=254;
Grey93=255;

//keep in mind that most of this palette data CAN be altered, and we need to "abstract for -all of -that" as well.
type




{
X11 actually uses similar implementation.

To get the red value of color 5:

red:=Palette16[5].colors^.r;

To get its DWord:

color:= Palette16[5].DWords;

}


//size determined by "Pallette variable"(array x..y of TRec)
TRec=record
   	colors:PSDL_COLOR; //RGB Data
	DWords:DWord; //DWord
end;


//CGA: 4bit (faked) planar, not EGA modes.

//this is the RGBData, not the DWord "colors"

//you will notice that often it seems Im repeating bytes here, Im not.

// you need to load the bytes(read from a file) INTO a R,G,B pattern, as well as in DWord format.
// yes, you CAN convert betwwen the two.

var

InvertColors:boolean;

//this is to assign color values in RGB format. BitFlip ARGB/RGBA  later on.

//4 bytes at a time...
  valuelist2: array [0..7] of byte;
  valuelist4: array [0..15] of byte;
  valuelist16: array [0..63] of byte;
  valuelist64: array [0..255] of byte;
  valuelist256: array [0..767] of byte;


 //weird T typing of everything gets confusing here.
 //This is the one place WE DONT WANT POINTERS

  TPalette4: array [0..3] of TRec;
  //4 CGA color modes, 3 in color,one hi-res B/W
  Tpalette16: array [0..15] of TRec;
  Tpalette16Grey: array [0..15] of TRec;

  //X11 doesnt need this code, it already has a default B/W mode.
  //CGA HI-RES is two color(1bit)
  Tpalette1:array [0..1] of TRec;


  //EGA
  TPalette64: array [0..63] of TRec;
  TPalette64Grey: array [0..63] of TRec;
  

  //VGA
  TPalette256:array [0..255] of TRec;
  TPalette256Grey:array [0..255] of TRec;


//just make sure the palette data(Extensive) matches this setup!!!
// hair raising EDITS otherwise!!!



{$ifdef lcl}
//I have to nosedive into CPP here.....

    //Game=object
    //badguys=object
    //GoodGuys=object
    //weapons=object
    //boardItems=object
    
{$endif}

//headers

function RGBToDWord(somecolor:PSDL_Color):DWord;
function RGBAToDWord(somecolor:PSDL_Color):DWord;
function DWordToRGB(someword:DWord):PSDL_Color;
function DWordToRGBA(someword:DWord):PSDL_Color;
function Byte2Char(input:byte):char;
function Char2Byte(input:char):byte;
function GetFGColorIndex:byte;
procedure setFGColorIndexed(color:byte);
procedure setFGColor(someDword:dword); overload;
procedure setFGColor(r,g,b:byte); overload;
procedure setFGColor(r,g,b,a:word); overload;
procedure setBGColor(index:byte);
procedure setBGColor(someDword:DWord); overload;
procedure setBGColor(r,g,b:byte); overload;
procedure setBGColor(r,g,b,a:byte); overload;
function GetFgDWordRGBA:DWord;
procedure invertPalettedColors;
//wip: function FetchModeList:Tmodelist; 
procedure Texlock(Tex:PSDL_Texture);
procedure TexlockwRect(Tex:PSDL_Texture; Rect:PSDL_Rect);
function lockNewTexture:PSDL_Texture;
procedure TexUnlock(Tex:PSDL_Texture);
procedure clearDevice;
procedure clearscreen;
procedure clearscreen(index:byte); overload;
procedure clearscreen(color:Dword); overload;
procedure clearscreen(r,g,b:byte); overload;
procedure clearscreen(r,g,b,a:byte); overload;
procedure clearviewport;
procedure videoCallback;
Procedure Line(x,y,x1,y1:Word);
procedure Rectangle(Rect:PSDL_Rect); 
procedure Triangle(Tri:TriPoints); 
procedure renderTexture( tex:PSDL_Texture;  ren:PSDL_Renderer;  x,y:word;  clip:PSDL_Rect);
procedure setgraphmode(graphmode:graphics_modes; wantfullscreen:boolean);
function GetPixel(x,y:word):DWord;
Procedure PutPixel;
Procedure PutPixelXY(Renderer:PSDL_Renderer; x,y:Word);
Procedure PutPixelXY(someSurface:PSDL_Surface; x,y:Word);

procedure getmoderange(graphdriver:integer);
function cloneSurface(surface1:PSDL_Surface):PSDL_Surface;
procedure SetViewPort;
procedure SDLSetViewPort(Rect:PSDL_Rect);
procedure circleBres(xc, yc, r:integer);
procedure LoadImage(filename:PChar; Rect:PSDL_Rect);
procedure LoadImageStretched(filename:PChar);
function GetOS: string;
function Word16_from_RGB(red,green, blue:byte):Word;
function Word15_from_RGB(red,green, blue:byte):Word; 
function SDL_GetPixel( SrcSurface : PSDL_Surface; x : integer; y : integer ) : PtrUInt; export;
procedure SDL_PutPixel( DstSurface : PSDL_Surface; x : integer; y : integer); export; 
procedure SDL_PutPixel( Renderer : PSDL_Renderer; x : integer; y : integer); export; 
procedure Blit(NewPixels:PSDL_Surface);
procedure BlitSurfaceToSurface(SrcSurfaceData,DstSurface:PSDL_Surface);
procedure BlitSurfaceWRects(SrcSurfaceData,DstSurface:PSDL_Surface; SrcRect,DstRect:PSDL_Rect);
procedure initPaletteGrey16;
procedure initPaletteGrey256;
procedure initpalette64;
procedure Save64Palette(filename:string);
procedure Read64Palette(filename:string; ReadColor:boolean);
procedure initPaletteCGAa;
procedure initPaletteCGAb;
procedure initPaletteCGAc;
procedure initPaletteCGAd;
procedure Save4PaletteA(filename:string);
procedure Save4PaletteB(filename:string);
procedure Save4PaletteC(filename:string);
procedure Save4Paletted(filename:string);
procedure Save16Palette(filename:string);
procedure Read16Palette(filename:string; ReadColor:boolean);
procedure Read4PaletteA(filename:string);
procedure Read4PaletteB(filename:string);
procedure Read4PaletteC(filename:string; ReadColor:boolean);
procedure Read4PaletteD(filename:string);
procedure initPalette256;
procedure Save256Palette(filename:string);
procedure Read256Palette(filename:string; ReadColor:boolean);
function GetRGBfromIndex(index:byte):PSDL_Color;
function GetDWordfromIndex(index:byte):DWord;
function GetRGBFromHexPalletted(input:DWord):PSDL_Color;
function GetRGBfromHexTC(somedata:DWord):PSDL_Color;
function GetRGBAFromHexTC(input:DWord):PSDL_Color;

function GetFgRGB:PSDL_Color;
function GetFgRGBA:PSDL_Color;
function GetFGName:string;
function GetBGName:string;


procedure setFGColorRGB(someDword:dword);

procedure setFGColorRGB(r,g,b:word); 
procedure setFGColorRGBA(r,g,b,a:word);

procedure setBGColorRGB(r,g,b:byte); 
procedure setBGColorRGBA(r,g,b,a:byte); overload;
procedure FlushBGColor;
function GetBgDWordRGB:DWord;
function GetBgDWordRGBA:DWord;
function GetFgDWordRGB:DWord;

procedure invertColorsBW;
procedure invertColors4;
procedure invertColors16;
procedure invertColors64;
procedure invertColors256;
procedure IntHandler;
procedure RoughSteinbergDither(filename,filename2:string);
procedure lock;
procedure unlock;

//other than MainSurface
procedure lock(someSurface:PSDL_Surface); overload;
procedure unlock(someSurface:PSDL_Surface); overload;

//scratch your head on this one...
{$ifdef cpu32}
    function initgraph(graphdriver:graphics_driver; graphmode:graphics_modes; pathToDriver:string; wantFullScreen:boolean):PSDL_Surface;
{$endif}

{$ifdef cpu64}
    procedure initgraph(graphdriver:graphics_driver; graphmode:graphics_modes; pathToDriver:string; wantFullScreen:boolean);
{$endif}

procedure DrawDoubleLinedWindowDialog(Rect:PSDL_Rect);
procedure DrawSingleLinedWindowDialog(Rect:PSDL_Rect);
function strf(l: longint): string;
Procedure PutPixelClip(x,y: smallint);
procedure LineTo(x,y : smallint);
//wip:procedure GetLineSettings(var ActiveLineInfo : LineSettingsType);
procedure SetLineStyle(LineStyle: word; Pattern: word; Thickness: word);
//wip: procedure GetViewSettings(var viewport : ViewPortType);
Procedure GetArcCoords(var ArcCoords: ArcCoordsType);
Procedure Arc(X,Y : smallint; StAngle,EndAngle,Radius: word);
procedure FillEllipse(X, Y: smallint; XRadius, YRadius: Word);
procedure Sector(x, y: smallint; StAngle,EndAngle, XRadius, YRadius: Word);
procedure SetFillStyle(Pattern : smallint; Color: SDL_Color);
procedure Bar(x1,y1,x2,y2:smallint);
procedure bar3D(x1, y1, x2, y2 : smallint;depth : word;top : boolean);
procedure SetColor(Color: DWord);
function GetColor: DWord;
function GetMaxColor: DWord; 
Procedure MoveRel(Dx, Dy: smallint);
Procedure MoveTo(X,Y: word);
Function GetMaxX: word;
Function GetMaxY: word;

procedure graphdefaults;
procedure GetAspectRatio(var Xasp,Yasp : word);
procedure SetAspectRatio(Xasp, Yasp : word);
procedure SetWriteMode(WriteMode : smallint);
procedure SetWriteMode32(WriteMode : smallint; blendDegree:byte);
procedure GetFillSettings(var Fillinfo:Fillsettingstype);
procedure DrawPoly(numpoints : word;var polypoints);
procedure PieSlice(X,Y,stangle,endAngle:smallint;Radius: Word);
procedure UDPixelTriangle(x,y:word; PointsUp:boolean);
procedure LRPixelTriangle(x,y:word; PointsLeft:boolean);
Procedure closegraph;
function GetX:word;
function GetY:word;
function GetXY:longint;
function getgraphmode:string;
procedure restorecrtmode; 
function getdrivername:string;
Procedure DetectGraph(WantsFullScreen:boolean);
procedure RemoveViewPort(windownumber:byte);
procedure InstallUserDriver(Name: string; AutoDetectPtr: Pointer);
procedure RegisterBGIDriver(driver: pointer);
procedure Diamonds(Thick:thickness; x,y:word);
procedure PlotPixelWithNeighbors(Thick:thickness; x,y:word);
procedure SaveBMPImage(filename:string);
function GetPixels:PSDL_Surface;
        

implementation
//keep in mind that this code is in flux and needs to included in MY routines as: {if lcl} ..{endif}
//I lean heavyily on SDL/libHermes. If you have a better, more optimal way, especially with OGL - better than what Ive hooked inti--USE IT. I havent found better.

//This code is NOT MINE, I intend to give credit to the authore. Found on GitHub.
// LAZ_BGI code. suboptimal, as written.
//  The only thing Sdl needs is the LAZ Tcanvas/TPaintbox (Pointer). Presumes 16 color(8bit), which is incorrect. Should use accelerated rendering(exen X11 prims) , If possible.

// use native LCL functions, unless they become unaccelerated, iffy, then lean on SDL/GL otherwise.

(*

Introduction:

- Add a descendant of TGraphicControl (TPaintbox) or TCustomControl (TPanel) to
  the form. A paintbox is recommended, but it is also possible to pain
  directly on a form.
  The BGI painting then is executed on the rectangle defined by this control.
  The top/left corner of this control has the BGI coordinates (0, 0), and the
  bottom/right corner has (GetMaxX, GetMaxY).

- BGI commands must begin with "InitGraph". In contrast to BGI, the routine does
  not accept the graphics driver, but the canvas onto which the output is
  painted, e.g. "Paintbox1" or "Form1". The other two parameters define the
  width and height of the drawing area:

 so by definition PaintBox1.Canvas = (equivalent to) SDL(1)^.MainSurface.
 Is it accelerated, becons the question, SOME DONT THINK SO. If this is the case, a GL Canvas- see the GL code--can be implemented.
 **EXPERIMENTAL CODE ** LUT for paletted features, is not enabled yet(LAZ and OBJECT CODE must be present first).
 
    InitGraph(Paintbox1.Canvas, Paintbox1.Width, Paintbox1.Height);

- Then the rest of the BGI commands follow like in an original ancient DOS
  program, such as "Line", "OutText", "Circle" etc.

- "CloseGraph" at the end of the graphic output is no longer absolutely required
  any more since the system cannot be switched back to text mode.
  CloseGraph, however, cleans up memory usage - it will be called automatically
  at the end of the program anyway.

- The graphics commands must be called from a routine of the painting cycle of
  the container control. In case of TPaintBox, TPanel or TForm, this is the
  OnPaint event.

- Note that the OnPaint handler must be able to paint the entire control.

- In DOS, often the same function was painted over the same graphic with
  different parameters - this was possible because of the persistent screen
  memory. This is not possible any more because the OS can reqest a complete
  repaint at any time and thus erase the previous drawing. An exception is
  painting into a temporary buffer bitmap - see below.

- Also be prepared of surprises when random numbers are used for some drawing
  parameters, such as in Borland's BGIDEMO.

- The BGI painting routine, by no means, must be allowed to wait for user input
  like in the original BGIDEMO. User input must be handled by the usual
  LCL OnKey* and OnMouse* events outside the painting routine.

- If nevertheless several curves are to be painted above each other, or if
  a flicker-free animation is supposed to be shown then the BGI graphic can
  be buffered:

  - When the graph is supposed to be drawn upon a button click the drawing
    commands must be put into the OnClick event handler of the button. The
    canvas for painting must be the canvas of a temporary bitmap, and the
    drawing routine must trigger a repaint of the control on which the BGI
    graphic is supposed to appear (Paintbox1.Invalidate). In the OnPaint
    handler of the control (Paintbox, Panel, Form, ...) the BGI graphic must
    be copied from the bitmap buffer to the control canvas:

      var
        Buffer: TBitmap;


      procedure TForm1.Button1Click(Sender: TObject);
      begin
        Buffer:=FillDWord(Buffer,sizeof(Buffer),0); //doubleFree or Ptr error as written- dont free a pointer, then try to access it. Zero for security reasons.
        Buffer := TBitmap.Create;
        InitGraph(Buffer.Canvas, Paintbox1.Width, Paintbox1.Height); //createTexture/surface

        {... BGI graphics commands ... }

        TForm1.Paintbox1Paint;
        CloseGraph; //freeTexture/surface
        Paintbox1.Invalidate;
      end;

//pageFlip/RenderPresent

      procedure TForm1.Paintbox1Paint(Sender: TObject);
      begin
        Paintbox1.Canvas.Draw(0, 0, Buffer);
      end;

Differences to BGI:

Theres a linestyle feature w the LCL:

Brushes(think pens)
Pens (like an "erasable" pen IRL)

nobody has taken into account that you can, are allowed to use both in the LCL/SDL. (Use of BGI presumes PENS ONLY.)

setFormSize must be just above (+5px?) the Canvas size, unless FS. Force resize (to size specified in initgraph call- or bail) unless drawing to a ViewPort.
InitGraph setsup the painting context(pulled from form, on app init), as specified otherwise.
drqwing directly on the form is not double buffered, and thereby not recommended. Does not appear to be accelerated, either.
  
BAILED FUNCTIONS are handled. Drop a clue, log, ignore the error. Dont bail. Only bail on actual errors, pointer issues, sdl failures, OOM contions etc.  

- More than that, colors can also
  be defined by "SetRGBColor" and "SetRGBBkColor" (for line/text and background
  fill, respectively). Unlike in the OS, texts are not painted in their own
  color, but by the line color.

- Page switching by SetActivePage/SetVisualPage is not supported at the moment.

- Line types are displayed correctly only with line thickness 1 (NormWidth),
  otherwise only solid lines are shown. Unlike in the BGI, line thicknesses
  other than 1 (NormWidth) or 3 (ThickWidth) can be selected.
  New line type DashDotDotLn.
  User-defined line patterns are not supported at the moment.

- The LCL provides less fill patterns as the BGI.(incorrect, presumed as bitmaps. startx,y and endx,y must be within range, or clipped)
 
 Therefore, all non-empty and
  non-solid fill patterns are emulated by means of bitmaps(true), in the same way
  as user-defined fill patterns. 
  
  Moreover, a rectangle can also be filled by
  a linear color gradient ("GradientRect").--> i didnt take this into consideration.

- Text output uses the same fonts as the LCL, Borlands CHR vector fonts are not
  supported. (They dont scale, use TTF)
  *  Using SetDefaultFont, SetTriplexFont, etc. you can define which
  fonts of the OS are supposed to be used instead of the BGI fonts (DefaultFont,
  Triplex, Small, SansSerif, Gothic), and which font sizes (in points) will be
  selected for the 10 BGI font sizes. Be prepared for differences to the
  original BGI fonts. "RegisterUserFont" is not supported. "InstallUserFont" is
  supported, however, but with different calling parameters (now allowing
  OS font names).
  Similarly every font available in the system can be used for text output by
  using the function SetTextFont.

- DrawPoly and FillPoly draw also the connection between outer and inner polygons
*******************************************************************************)

unit lazBGI;

{ Missing:
- Palettes
- SetActivePage

Problems:
- PutImage using different WriteModes
}

interface

uses
  LCLIntf,
  Classes, SysUtils, Graphics, Controls;


{ BGI declarations }

const
  // Bar3D constants
  TopOn = true;
  TopOff = false;

  // BitBlt constants
  NormalPut = 0;
  CopyPut = 0;
  XORPut = 1;
  OrPut = 2;
  AndPut = 3;
  NotPut = 4;

  // Clipping constants
  ClipOff = false;
  ClipOn = true;

  // color constants
  MaxColors = 15;

  Black = 0;
  Blue = 1;
  Green = 2;
  cyan = 3;
  Red = 4;
  Magenta = 5;
  Brown = 6;
  LightGray = 7;
  DarkGray = 8;
  LightBlue = 9;
  LightGreen = 10;
  LightCyan = 11;
  LightRed = 12;
  LightMagenta = 13;
  Yellow = 14;
  White = 15;

  /// error constants, for reasons of compatiblity only
  grOK = 0;
  grNoInitGraph = -1;
  grNotDetected = -2;
  grFileNotFound = -3;
  grInvalidDriver = -4;
  grNoLoadMem = -5;
  grNoScanMem = -6;
  grNoFloodMem = -7;
  grFontNotFound = -8;
  grNoFontMem = -9;
  grInvalidMode = -10;
  grError = -11;
  grIOError = -12;
  grInvalidFont = -13;
  grInvalidFontNum = -14;


  // Fill pattern constants
  EmptyFill = 0;
  SolidFill = 1;
  LineFill = 2;       // ---
  LtSlashFill = 3;    // ///
  SlashFill = 4;      // /// with thick lines
  BkSlashFill = 5;    // \\\ with thick lines
  LtBkSlashFill = 6;  // \\\
  HatchFill = 7;      // ++++
  XHatchFill = 8;     // xxxx
  InterleaveFill = 9; // dashed lines
  WideDotFill = 10;   // dots with large spacing
  CloseDotFill = 11;  // dots with small spacing
  UserFill = 12;      // user-defined pattern

  // Text alignment in SetTextJustify
  LeftText = 0;
  CenterText = 1;
  RightText = 2;
  BottomText = 0;
  TopText = 2;

  // Line style types
  SolidLn = 0;
  DottedLn = 1;
  CenterLn = 2;
  DashedLn = 3;
  UserBitLn = 4;
  DashDotLn = CenterLn;           // new
  DashDotDotLn = 5;               // new

  // Line widths
  NormWidth = 1;
  ThickWidth = 3;

  // Font types, text display
  DefaultFont = 0;
  TriplexFont = 1;
  SmallFont = 2;
  SansserifFont = 3;
  GothicFont = 4;

  HorizDir = 0;
  VertDir = 1;
  UserCharSize = 0;

type
  ArcCoordsType = record
    X, Y,
    XStart, YStart,
    XEnd, YEnd : integer;
  end;

  FillPatternType = array[1..8] of byte;

  FillSettingsType = record
    Pattern   : word;
    Color     : word;
  end;

  LineSettingsType = record
    LineStyle : word;
    Pattern   : word;
    Thickness : word;
  end;

  PaletteType = record
    Size: byte;
    Colors: Array[0..maxColors] of ShortInt;
  end;

  PointType = record
    X, Y: integer;
  end;

  TextSettingsType = record
    Font: word;
    Direction: word;
    CharSize: word;
    Horiz: word;
    Vert: word;
  end;

  ViewportType = record
    x1, y1, x2, y2: integer;
    Clip: boolean;
  end;


{ BGI routines }

procedure Arc(x, y: integer; StAngle, EndAngle, Radius: word);
procedure Bar(x1, y1, x2, y2: integer);
procedure Bar3D(x1, y1, x2, y2: integer; ADepth: word; ATop: boolean);
procedure Circle(x, y, r: integer);
procedure ClearDevice;
procedure CloseGraph;
procedure DrawPoly(NumPoints: integer; var PolyPoints);
procedure Ellipse(x, y: integer; StAngle, EndAngle, xRadius, yRadius: word);
procedure FillEllipse(x, y: integer; xRadius, yRadius: word);
procedure FillPoly(NumPoints: integer; var PolyPoints);
procedure FloodFill(x, y: integer; ABorder: word);
procedure GetArcCoords(out ArcCoords: ArcCoordsType);
procedure GetAspectRatio(out xasp, yasp: word);
function GetBkColor: word;
function GetColor: word;
function GetDriverName: string;
function GetFgColor: word;
procedure GetFillSettings(out FillInfo: FillSettingsType);
function GetGraphMode: integer;
procedure GetImage(x1, y1, x2, y2: integer; ABitmap: TBitmap);
procedure GetLineSettings(out LineInfo: LineSettingsType);
function GetMaxColor: word;
function GetMaxX: integer;
function GetMaxY: integer;
function GetModeName({%H-}AMode: integer): string;
function GetPixel(x, y: integer): word;
procedure GetTextSettings(out TextInfo: TextSettingsType);
function GetX: integer;
function GetY: integer;
procedure GraphDefaults;
function GraphErrorMsg({%H-}AErrorCode: integer): string;
function GraphResult: integer;
procedure GetViewSettings(out ViewPort: ViewportType);
function ImageSize({%H-}x1, {%H-}y1, {%H-}x2, {%H-}y2: integer): word;
procedure InitGraph(ACanvas: TCanvas; AWidth, AHeight: Integer);  // NOTE: Parameters changed!
function InstallUserDriver({%H-}AName: string; {%H-}AutoDetectPtr: Pointer): integer;
function InstallUserFont(AFontName: string; ASizes: array of integer; AStyle: TFontStyles): integer;  // NOTE: Parameters changed!
procedure Line(x1, y1, x2, y2: Integer);
procedure LineRel(dx, dy: integer);
procedure LineTo(x, y: integer);
procedure MoveRel(dx, dy: integer);
procedure MoveTo(x, y: integer);
procedure OutText(s: string);
procedure OutTextXY(x, y: integer; s: string);
procedure PieSlice(x, y: integer; StAngle, EndAngle, Radius: word);
procedure PutImage(x, y: integer; ABitmap: TBitmap; AMode: word);
procedure PutPixel(x, y: integer; AColor:word);
procedure Rectangle(x1, y1, x2, y2: integer);
function RegisterBGIdriver({%H-}ADriver: Pointer): integer;
function RegisterBGIFont({%H-}AFont: Pointer): integer;
procedure Sector(x, y: integer; StAngle, EndAngle, xRadius, yRadius: word);
procedure SetBkColor(AColorNum: word);
procedure SetFgColor(AColor: word);
procedure SetColor(AColor: word);  // DANGER to call inherited TControl.Color!
procedure SetFillPattern(APattern: FillPatternType; AColor: word);
procedure SetFillStyle(APattern, AColor: word);
procedure SetLineStyle(ALinestyle, APattern, AThickness: word);
procedure SetTextJustify(Horiz, Vert: word);
procedure SetTextStyle(AFont, ADirection, ACharsize: word);
procedure SetUserCharSize({%H-}MultX, {%H-}DivX, {%H-}MultY, {%H-}DivY: word);
procedure SetViewPort(x1, y1, x2, y2: integer; AClip: boolean);
procedure SetWriteMode(AMode: integer);
function  TextHeight(s: string): integer;
function  TextWidth(s: string): integer;

{ Additional declarations and routines }

type
  EBGIGraphError = Exception;

// Colors
function GetBkColorRGB : TColor;
function GetColorRGB : TColor;
procedure SetBkColorRGB(AColor: TColor);
procedure SetColorRGB(AColor: TColor);

function GetDefaultBkColorRGB: TColor;
procedure SetDefaultBkColorRGB(AColor: TColor);

// Fonts
procedure GetTextFont(out AFontname: TFontName; out ASize: integer; out AStyle: TFontStyles);
procedure SetSansSerifFont(AFontname: TFontName; ASizes: array of integer; AStyle: TFontStyles);
procedure SetDefaultFont(AFontname: TFontName; ASizes: array of integer; AStyle: TFontStyles);
procedure SetGothicFont(AFontname: TFontName; ASizes: array of integer; Astyle: TFontStyles);
procedure SetSmallFont(AFontname: TFontName; ASizes: array of integer; AStyle: TFontStyles);
procedure SetTriplexFont(AFontname: TFontName; ASizes: array of integer; AStyle: TFontStyles);
procedure SetTextFont(AFontname: TFontName; ASize: integer; AStyle: TFontStyles);

// Fill
procedure GradientRect(x1, y1, x2, y2: Integer; StartColor, EndColor:TColor; ADirection: integer);
procedure SetFillBitmap(ABitmap: TCustomBitmap);


implementation

uses
  Math,
  Forms;
  
var
  FCanvas: TCanvas;           // Canvas as BGI screen replacement
  FCanvasWidth: Integer;      // Width of the area reserved on the canvas for BGI painting
  FCanvasHeight: Integer;     // Height of the area reserved on the canvas for BGI painting
  FActiveCanvas: TCanvas;     // Canvas used for painting

  FViewPort: ViewportType;    // Current viewport in BGI units
  FArcCoords: ArcCoordsType;  // Coordinates needed by "Arc()"

{ Font management }

type
  TFontSizeArray = array[1..10] of integer;
  TFontRec = record
    Name : TFontName;
    Sizes : TFontSizeArray;
    Style : TFontStyles;
  end;
  TFontArray = array[DefaultFont..10] of TFontRec;

var
  FFonts : TFontArray;         // Array with max 10(+1) BGI fonts
  FNumFonts : integer;         // Count of available fonts in array _Fonts
  FFontName : string;          // Currently used font
  FFontSize : integer;         // Point size of the currently used font
  FFontStyle : TFontStyles;    // Style attributes of the currently used font
  FBgiFont : integer;          // Current font as BGI number
  FBgiSize : integer;          // BGI size of the current font
  FTextAlignHor : integer;     // Current alignment: LeftText...
  FTextAlignVert : integer;    // ... TopText...
  FTextDir : integer;          // Current writing direction (HorizDir...)

{ Area fills
  There are no LCL brushes which exactly correspond to the BGI fills.
  Therefore, the fill patterns are created manually. }
const
  Empty_Fill     : FillPatternType = ($00, $00, $00, $00, $00, $00, $00, $00);
  Solid_Fill     : FillPatternType = ($FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF);
  Line_Fill      : FillPatternType = ($FF, $FF, $00, $00, $FF, $FF, $00, $00);
  Lt_Slash_Fill  : FillPatternType = ($01, $02, $04, $08, $10, $20, $40, $80);
  Slash_Fill     : FillPatternType = ($E0, $C1, $83, $07, $0E, $1C, $38, $70);
  Backslash_Fill : FillPatternType = ($F0, $78, $3C, $1E, $0F, $87, $C3, $E1);
  Lt_Bkslash_Fill: FillPatternType = ($80, $40, $20, $10, $08, $04, $02, $01);
  Hatch_Fill     : FillPatternType = ($FF, $88, $88, $88, $FF, $88, $88, $88);
  XHatch_Fill    : FillPatternType = ($81, $42, $24, $18, $18, $24, $42, $81);
  Interleave_Fill: FillPatternType = ($F0, $00, $00, $00, $0F, $00, $00, $00);
  Wide_Dot_Fill  : FillPatternType = ($80, $00, $08, $00, $80, $00, $08, $00);
  Close_Dot_Fill : FillPatternType = ($88, $00, $22, $00, $88, $00, $22, $00);

var
  User_Fill      : FillPatternType = ($00, $00, $00, $00, $00, $00, $00, $00);

const
  FILL_PATTERNS: Array[EmptyFill..UserFill] of ^FillPatternType =
    (@Empty_Fill, @Solid_Fill, @Line_Fill, @Lt_Slash_Fill, @Slash_Fill,
     @BackSlash_Fill, @Lt_BkSlash_Fill, @Hatch_Fill, @XHatch_Fill,
     @InterLeave_Fill, @Wide_Dot_Fill, @Close_Dot_Fill, @User_Fill);

var
  FFillSettings: FillSettingsType;  // Current fill settings
  FFillBitmap: TBitmap;             // Bitmap for area fills

{ Color management
  The VGA color of the BGI do not correspond exactly to LCL TColors. The
  following colors are extracted from a BGI program and extended from range
  0..63 to 0..255. }
const
  _Black = clBlack;             // R=0 G=0 B=0
  _Blue = $00800000;            // R=0 G=0 B=170
  _Green = $00008000;           // R=0 G=170 B=0
  _Cyan = $00808000;            // R=0 G=170 B=170
  _Red = $00000080;             // R=170 G=0 B=0
  _Magenta = $00800080;         // R=170 G=0 B=170
  _Brown = $00004080;           // R=170 G=85 B=0
  _LightGray = $00808080;       // R=170 G=170 B=170
  _DarkGray = $00404040;        // R=85 G=85 B=85
  _LightBlue = $00FF4040;       // R=85 G=85 B=255
  _LightGreen = $0040FF40;      // R=85 G=255 B=85
  _LightCyan = $00FFFF40;       // R=85 G=255 B=255
  _LightRed = $004040FF;        // R=255 G=85 B=85
  _LightMagenta = $00FF40FF;    // R=255 G=85 B=255
  _Yellow = clYellow;           // R=255 G=255 B=85
  _White = clWhite;             // R=255 G=255 B=255

  MaxVGAColor = 15;

  // These are the 16 VGA colors
  VGAColors : array[0..maxVGAColor] of TColor = (_Black, _Blue, _Green, _Cyan,
    _Red, _Magenta, _Brown, _LightGray, _DarkGray, _LightBlue, _LightGreen,
    _LightCyan, _LightRed, _LightMagenta, _Yellow, _White);

var
  FBkColorRGB: TColor;        // Background color as RGB
  FFgColorRGB: TColor;        // Text and line color as TGB
  FDefaultBkColorRGB: TColor; // Default background color, used by GraphDefaults
  FDefaultColorRGB: TColor;   // Default text/line color, used by GraphDefaults

{ Strings for error messages }

resourcestring
  SNoCanvas = 'Canvas was not assigned.';
  SNoBitmap = 'No bitmap created.';
  SNotImplemented = 'Function not implemented.';
  SInvalidFont = 'Invalid font.';
  SInvalidPattern = 'Invalid fill pattern.';
  SInvalidAncestor = 'Invalid component (has no Canvas).';

{===============================================================================
                              Local routines
===============================================================================}

{ Checks whether a canvas has been assigned for painting and
  raises an exception otherwise. }
procedure CheckCanvas;
begin
  if not Assigned(FCanvas) then
    raise EBGIGraphError.Create(SNoCanvas);

  if not Assigned(FActiveCanvas) then
    raise EBGIGraphError.Create(SNoCanvas);
end;

{ Checks whether the provided bitmap has been created and raises an
  exception otherwise. }
procedure CheckBitmap(ABitmap: TCustomBitmap);
begin
  if not Assigned(ABitmap) then
    raise EBGIGraphError.Create(SNoBitmap);
end;

{ Raises an exception for important BGI functions which are not implemented. }
procedure NotImplementedError;
begin
  raise EBGIGraphError.Create(SNotImplemented);
end;

{ Raises an exception when the font to be used is not valid. }
procedure InvalidFontError;
begin
  raise EBGIGraphError.Create(SInvalidFont);
end;

{ Raises an exception when then fill pattern to be used is not valid }
procedure InvalidPatternError;
begin
  raise EBGIGraphError.Create(SInvalidPattern);
end;

{ Return the canvas used for painting }
function GetActiveCanvas: TCanvas;
begin
  Result := FCanvas;
end;

{ Transforms the global point (x,y) (global with respect to GetMaxX and GetMaxY)
  to the local coordinate system defined by the currenly set viewport. }
procedure LocalPoint(var x,y: Integer);
begin
  x := x - FViewPort.x1;
  y := y - FViewPort.y1;
end;

{ Transforms the point (x, y) from the local viewport coordinate system to
  "global" coordinates of the painting area (GetMaxX, GetMaxY). }
procedure GlobalPoint(var x, y: Integer);
begin
  x := FViewPort.x1 + x;
  y := FViewPort.y1 + y;
end;

{ Exchanges the integers x and y }
procedure Swap(var x, y: Integer);
var
  tmp : integer;
begin
  tmp := x;
  x := y;
  y := tmp;
end;

{ Makes sure that x1 < x2 and y1 < y2 }
procedure CheckCorners(var x1, y1, x2, y2: integer);
begin
  if x2 < x1 then Swap(x1, x2);
  if y2 < y1 then Swap(y1, y2);
end;

{ Converts an LCL RGB-Color to a BGI color (0..15) }
function ColorToPalette(AColor: TColor): word;
var
  i: integer;
begin
  for i := 0 to MaxVGAColor do
    if AColor = VGAColors[i] then
    begin
      Result := i;
      exit;
    end;
  Result := 0;
end;

procedure UseAsBkColor(AColor: TColor);
begin
  FBkColorRGB := AColor;
  if Assigned(FActiveCanvas) then
    FActiveCanvas.Brush.Color := AColor;
end;

procedure UseAsFgColor(AColor: TColor);
begin
  FFgColorRGB := AColor;
  if Assigned(FActiveCanvas) then
  begin
    FActiveCanvas.Pen.Color := AColor;
    FActiveCanvas.Font.Color := AColor;
  end;
end;

{ Converts a BGI color (0...15) to an LCL RGB TColor }
function PaletteToColor(APalette: word): TColor;
begin
  if APalette <= GetMaxColor then
    Result := VGAColors[APalette]
  else
    Result := 0;
end;

{ Converts a BGI pen style to an LCL TPenStyle. }
function PenStyle(AStyle: integer): TPenStyle;
begin
  case AStyle of
    SolidLn      : Result := psSolid;
    DottedLn     : Result := psDot;
    DashedLn     : Result := psDash;
    CenterLn     : Result := psDashDot;
    DashDotDotLn : Result := psDashDotDot;
    else           Result := psSolid;
  end;
end;

{ Converts an LCL TPenStyle to a BGI pen style }
function BgiLinestyle(AStyle: TPenStyle): word;
begin
  case AStyle of
    psSolid      : Result := SolidLn;
    psDot        : Result := DottedLn;
    psDash       : Result := DashedLn;
    psDashDot    : Result := CenterLn;
    psDashDotDot : Result := DashDotDotLn;
    else           Result := SolidLn;
  end;
end;

{ Stores the given parameters in the _Fonts array at index id. }
procedure SetFontParams(id: integer; AName: string; ASizes: TFontSizeArray;
  AStyle: TFontStyles);
begin
  FFonts[id].Name := AName;
  Move(ASizes, FFonts[id].Sizes, SizeOf(ASizes));
  FFonts[id].Style := AStyle;
end;

{ Initializes the _Fonts array }
procedure InitFonts;
var
  i: integer;
begin
  SetDefaultFont('Courier New', [10, 12, 14, 16, 20, 24, 28, 32, 38, 48, 72], []);
  SetGothicFont('Allegro BT', [10, 12, 14, 16, 20, 24, 28, 32, 38, 48, 72], []);
  SetSansserifFont('Arial', [10, 12, 14, 16, 20, 24, 28, 32, 38, 48, 72], []);
  SetSmallFont('Arial Narrow', [6, 7, 8, 10, 12, 14, 16, 20, 24, 32], []);
  SetTriplexFont('Times New Roman', [10, 12, 14, 16, 20, 24, 28, 32, 38, 48, 72], []);
  FNumFonts := 4;
  for i := FNumFonts+1 to 10 do begin
    with FFonts[i] do begin
      Name := '';
      Style := [];
    end;
  end;
end;


{===============================================================================
                               BGI routines
===============================================================================}

// like BGI.
procedure Arc(x,y:integer; StAngle,EndAngle,Radius:word);
var
  x1,y1, x2,y2, x3,y3, x4,y4 : integer;
  sinPhi, cosPhi: Double;
begin
  CheckCanvas;

  FArcCoords.x := x;
  FArcCoords.y := y;

  x1 := x - Radius;
  y1 := y - Radius;
  GlobalPoint(x1,y1);

  x2 := x + Radius;
  y2 := y + Radius;
  GlobalPoint(x2,y2);

  SinCos(DegToRad(StAngle), sinPhi, cosPhi);
  x3 := x + Round(Radius * cosphi);
  y3 := y - round(Radius * sinphi);
  FArcCoords.XStart := x3;
  FArcCoords.YStart := y3;
  GlobalPoint(x3,y3);

  SinCos(DegToRad(EndAngle), sinPhi, cosPhi);
  x4 := x + round(Radius * cosphi);
  y4 := y - round(Radius * sinphi);
  FArcCoords.XEnd := x4;
  FArcCoords.YEnd := y4;
  GlobalPoint(x4, y4);

  FActiveCanvas.Arc(x1,y1, x2,y2, x3,y3, x4,y4);
end;

procedure Bar(x1,y1,x2,y2:integer);
var
  savedPenStyle : TPenStyle;
begin
  CheckCanvas;
  inc(x2);  // LCL ignores the right/bottom corner
  inc(y2);
  GlobalPoint(x1,y1);
  GlobalPoint(x2,y2);
  CheckCorners(x1,y1, x2,y2);
  with FActiveCanvas do begin
    savedPenStyle := Pen.Style;
    try
      Pen.Style := psClear;
      Rectangle(x1, y1, x2, y2);
    finally
      Pen.Style := savedPenStyle;
    end;
  end;
end;

{ Paints a 3D bar like the BGI. The front face extends between the corner points
  (x1, y1) and (x2, y2) and is filled with the current fill color and the current
  fill pattern (SetFillBitmap is ignored!).
  Depth is the depth of the bar in pixels. The side face is drawn to the right
  of the front face and filled with the background color.
  When Top=true a top face of the bar is painted, again filled with the
  background color.
  The border outlines are painted in the current forground pen color (SetColor). }
procedure Bar3D(x1, y1, x2, y2:integer; ADepth: word; ATop: boolean);
var
  savedFillSettings: FillSettingsType;
begin
  CheckCanvas;
  inc(x2);  // LCL ignores the right/bottom edge
  inc(y2);
  GlobalPoint(x1,y1);
  GlobalPoint(x2,y2);
  CheckCorners(x1,y1,x2,y2);

  GetFillSettings(savedFillSettings);
  try
    with FFillSettings do
      SetFillStyle(Pattern, Color);
    with FActiveCanvas do
    begin
      Rectangle(x1,y1,x2,y2);
      Brush.Style := bsSolid;
      Brush.Color := FBkColorRGB;
      Polygon([
        Point(x2, y1),
        Point(x2 + ADepth, y1 - ADepth),
        Point(x2 + ADepth, y2 - ADepth),
        Point(x2, y2),
        Point(x2, y1)
        ]);
      if ATop then Polygon([
        Point(x1, y1),
        Point(x1 + ADepth, y1 - ADepth),
        Point(x2 + ADepth, y1 - ADepth),
        Point(x2, y1),
        Point(x1, y1)
        ]);
    end;
  finally
    SetFillStyle(savedFillSettings.Pattern, savedFillSettings.Color);
  end;
end;

procedure Circle(x, y, r: integer);
var
  savedBrushStyle: TBrushStyle;
begin
  CheckCanvas;
  with FActiveCanvas do begin
    savedBrushStyle := Brush.Style;
    try
      GlobalPoint(x,y);
      Brush.Style := bsClear;
      Ellipse(x-r, y-r, x+r, y+r);
    finally
      Brush.Style := savedBrushStyle;
    end;
  end;
end;

procedure ClearDevice;
var
  savedBrushStyle: TBrushStyle;
begin
  CheckCanvas;
  with FActiveCanvas do begin
    savedBrushStyle := Brush.Style;
    try
      Brush.Style := bsSolid;
      Brush.Color := FBkColorRGB;
      FillRect(0, 0, FCanvasWidth, FCanvasHeight);
    finally
      Brush.Style := savedBrushStyle;
    end;
  end;
end;

{ Cleans up memory }
procedure CloseGraph;
begin
  FreeAndNil(FFillBitmap);
end;

{ Warning in case of self-enclosing polygons: In contrast to the BGI, the LCL
  paints also the connecting line from outer to inner polygon (see BgiDemo -
  LineRelPlay. }
procedure DrawPoly(NumPoints: integer; var PolyPoints);
type
  PPoint = ^PointType;
var
  i: integer;
  P: PPoint;
begin
  CheckCanvas;
  P := PPoint(@PolyPoints);
  i := NumPoints;
  while i > 0 do
  begin
    GlobalPoint(P^.x, P^.y);
    inc(P);
    dec(i);
  end;
  FActiveCanvas.Polyline(@PolyPoints, NumPoints);
end;

procedure Ellipse(x, y: integer; StAngle, EndAngle, xRadius, yRadius:word);
var
  x1,y1, x2,y2, x3,y3, x4,y4: integer;
  cosphi, sinphi: double;
  savedBrushStyle: TBrushStyle;
begin
  CheckCanvas;
  if (StAngle = 0) and (EndAngle = 360) then
  begin
    savedBrushStyle := FActiveCanvas.Brush.Style;
    try
      FActiveCanvas.Brush.Style := bsClear;
      GlobalPoint(x, y);
      FActiveCanvas.Ellipse(x-xRadius, y-yRadius, x+xRadius, y+yRadius);
    finally
      FActiveCanvas.Brush.Style := savedBrushStyle;
    end;
  end else
  begin
    x1 := x - xRadius;
    y1 := y - yRadius;
    GlobalPoint(x1, y1);

    x2 := x + xRadius;
    y2 := y + yRadius;
    GlobalPoint(x2, y2);

    SinCos(DegToRad(StAngle), sinPhi, cosPhi);
    x3 := x + Round(xRadius * cosPhi);
    y3 := y - round(yRadius * sinPhi);
    GlobalPoint(x3, y3);

    SinCos(DegToRad(EndAngle), sinPhi, cosPhi);
    x4 := x + round(xRadius * cosPhi);
    y4 := y - round(yRadius * sinPhi);
    GlobalPoint(x4, y4);

    FActiveCanvas.Arc(x1,y1, x2,y2, x3,y3, x4, y4);
  end;
end;

procedure FillEllipse(x, y: integer; xRadius, yRadius: word);
begin
  CheckCanvas;
  GlobalPoint(x, y);
  FActiveCanvas.Ellipse(x - xRadius, y - yRadius, x + xRadius, y + yRadius);
end;

{ Warning in case of self-enclosing polygons: In contrast to the BGI, the LCL
  paints also the connecting line from outer to inner polygon (see BgiDemo -
  LineRelPlay. }
procedure FillPoly(NumPoints: integer; var PolyPoints);
type
  PPoint = ^PointType;
var
  i: integer;
  P: PPoint;
begin
  CheckCanvas;
  P := PPoint(@PolyPoints);
  i := NumPoints;
  while i > 0 do begin
    GlobalPoint(P^.x, P^.y);
    inc(P);
    dec(i);
  end;
  FActiveCanvas.Polygon(@PolyPoints, NumPoints);
end;

{ Like BGI: Starting at (x, y), fills all pixels until color "ABorder" is found. }
procedure FloodFill(x, y: integer; ABorder: word);
begin
  CheckCanvas;
  GlobalPoint(x, y);
  FActiveCanvas.FloodFill(x, y, PaletteToColor(ABorder), fsBorder);
  // Remark:
  // Using "fsSurface" it would be possible to fill all pixels of the specific color.
end;

procedure GetArcCoords(out ArcCoords: ArcCoordsType);
begin
  ArcCoords := FArcCoords;
end;

{ Usually the pixels should be square. }
procedure GetAspectRatio(out xasp, yasp: word);
begin
  xasp := 1;
  yasp := 1;
end;

function GetBkColor: word;
begin
  CheckCanvas;
  Result := ColorToPalette(FActiveCanvas.Brush.Color);
end;

function GetColor: word;
begin
  CheckCanvas;
  Result := ColorToPalette(FActiveCanvas.Pen.Color);
end;

function GetFgColor: word;
begin
  CheckCanvas;
  Result := ColorToPalette(FActiveCanvas.Pen.Color);
end;

{ Just for compatibility. There is no BGI driver any more... }
function GetDriverName: string;
begin
  result := '';
end;

procedure GetFillSettings(out FillInfo: FillSettingsType);
begin
  FillInfo := FFillSettings;
end;

{ Just for compatibility. There is not graphics mode any more... }
function GetGraphMode: integer;
begin
  Result := 0;
end;

{ Ignores user pattern }
procedure GetLineSettings(out LineInfo: LineSettingsType);
begin
  CheckCanvas;
  LineInfo.LineStyle := BgiLinestyle(FActiveCanvas.Pen.Style);
  LineInfo.Pattern := 0;
  LineInfo.Thickness := FActiveCanvas.Pen.Width;
end;

{ Only 16 VGA colors! More colors can be created by SetRGBColor }
function GetMaxColor: word;
begin
  result := MaxVGAColor;
end;

{ Similar to BGI, but now the size of the drawing area is not defined by the
  monitor but by the size of the rectangle specified in the InitGraph call. }
function GetMaxX: integer;
begin
  Result := FCanvasWidth;
end;

{ See also GetMaxX }
function GetMaxY: integer;
begin
  Result := FCanvasHeight;
end;

{ Just for compatibility - there is no graph mode any more... }
function GetModeName(AMode: integer) : string;
begin
  Result := '';
end;

function GetPixel(x,y: integer): word;
begin
  CheckCanvas;
  GlobalPoint(x, y);
  result := ColorToPalette(FActiveCanvas.Pixels[x,y]);
end;

procedure GetTextSettings(out TextInfo: TextSettingsType);
begin
  CheckCanvas;
  TextInfo.Font := FBgiFont;
  TextInfo.Direction := FTextDir;
  TextInfo.CharSize := FBgiSize;
  TextInfo.Horiz := FTextAlignHor;
  TextInfo.Vert := FTextAlignVert;
end;

procedure GraphDefaults;
begin
  CheckCanvas;
  SetViewPort(0, 0, GetMaxX, GetMaxY, ClipOn);
  UseAsBkColor(FDefaultBkColorRGB);
  UseAsFgColor(FDefaultColorRGB);
  SetTextStyle(DefaultFont, HorizDir, 1);
  SetTextJustify(LeftText, TopText);
  SetWriteMode(CopyPut);
  SetFillStyle(EmptyFill, White);
  SetLineStyle(SolidLn, 0, NormWidth);
  ClearDevice;
end;

{ Is ignored. }
function GraphErrorMsg(AErrorCode: integer): string;
begin
  Result := '';
end;

{ Unlike BGI, the destination of GetImage is not a pointer any more but a
  TBitmap instance which must have been created before calling GetImage
    ABitmap := TBitmap.Create;
  and which must be destroyed when no longer needed.
    ABitmap.Free; }
procedure GetImage(x1, y1, x2, y2: integer; ABitmap: TBitmap);
var
  Rs, Rd : TRect;
begin
  CheckCanvas;
  CheckBitmap(ABitmap);
  CheckCorners(x1, y1, x2, y2);
  GlobalPoint(x1, y1);
  GlobalPoint(x2, y2);
  Rs := Rect(x1, y1, x2, y2);
  Rd := Rect(0, 0, x2-x1, y2-y1);
  with ABitmap do begin
    Width := x2-x1;
    Height := y2-y1;
    Canvas.CopyRect(Rd, FActiveCanvas, Rs);
  end;
end;

{ Is ignored. The typical BGI errors cannot happen here or raise exceptions. }
function GraphResult: integer;
begin
  Result := grOK;
end;

procedure GetViewSettings(out ViewPort: ViewportType);
begin
  ViewPort := FViewPort;
end;

function GetX: integer;
begin
  CheckCanvas;
  Result := FActiveCanvas.PenPos.X - FViewPort.x1;
end;

function GetY: integer;
begin
  CheckCanvas;
  Result := FActiveCanvas.PenPos.Y - FViewPort.y1;
end;

{ Because GetImage/PutImage use TBitmap objects here rather than pointer
  strutures, this function has no effect. Because of this fundamental difference
  relative to the BGI, an exception is raised. }
function {%H-}ImageSize(x1,y1,x2,y2:integer) : word;
begin
  NotImplementedError;
end;

{ Changed calling parameters!
  Defines the LCL control which will paint the BGI output on its canvas.
  The following components are good examples:
  - TPaintbox
  - TPanel
  - TForm.
  Does all initializations by calling GraphDefaults.
  InitGraph MUST BE CALLED BEFORE ANY OTHER CALLS of lazGraph. }
procedure InitGraph(ACanvas: TCanvas; AWidth, AHeight: Integer);
begin
  FCanvas := ACanvas;
  FCanvasWidth := AWidth;
  FCanvasHeight := AHeight;
  if FCanvas = nil then
    raise EBGIGraphError.Create('[InitGraph] No canvas specified.');
  if FCanvasWidth <= 1 then
    raise EBGIGraphError.Create('[InitGraph] Invalid canvas width.');
  if FCanvasHeight <= 1 then
    raise EBGIGraphError.Create('[InitGraph] Invalid canvas height.');
  FActiveCanvas := ACanvas;
  GraphDefaults;
end;

{ Not implemented. Calling this raises an exception because of the fundamental
  difference to the BGI. }
function {%H-}InstallUserDriver(AName:string; AutoDetectPtr:Pointer) : integer;
begin
  NotImplementedError;
end;

{ AFontName must be the name of a font of the system, e.g. 'Times New Roman'
  When this font is not found it is silently replace by a similar one. }
function InstallUserFont(AFontName: string; ASizes: array of integer;
  AStyle: TFontStyles): integer;
begin
  inc(FNumFonts);
  FFonts[FNumFonts].Name := AFontName;
  Move(FFonts[FNumFonts-1].Sizes, FFonts[FNumFonts].Sizes, SizeOf(TFontSizeArray));
  FFonts[FNumFonts].Style := AStyle;
  FFonts[FNumFonts].Sizes := ASizes;
  Result := FNumFonts;
end;

procedure Line(x1, y1, x2, y2: integer);
begin
  CheckCanvas;
  GlobalPoint(x1, y1);
  FActiveCanvas.MoveTo(x1, y1);
  GlobalPoint(x2, y2);
  FActiveCanvas.LineTo(x2, y2);
end;

procedure LineRel(dx, dy: integer);
begin
  CheckCanvas;
  with FActiveCanvas do
    LineTo(PenPos.X + dx, PenPos.Y + dy);
end;

procedure LineTo(x, y: integer);
begin
  CheckCanvas;
  FActiveCanvas.LineTo(x + FViewport.x1, y + FViewport.y1);
end;

procedure MoveTo(x, y: integer);
begin
  CheckCanvas;
  FActiveCanvas.MoveTo(x + FViewport.x1, y + FViewport.y1);
end;

procedure MoveRel(dx, dy: integer);
begin
  CheckCanvas;
  with FActiveCanvas do MoveTo(PenPos.X + dx, PenPos.Y + dy);
end;

procedure OutText(s: string);
var
  w : integer;
begin
  CheckCanvas;
  OutTextXY(FActiveCanvas.PenPos.X, FActiveCanvas.PenPos.Y, s);
  w := TextWidth(s);
  case FTextDir of
    HorizDir :
      with FActiveCanvas do MoveTo(PenPos.X + w, PenPos.Y);
    VertDir :
      with FActiveCanvas do MoveTo(PenPos.X, PenPos.Y - w);
  end;
end;

procedure OutTextXY(x, y: integer; s: string);
var
  R: TRect;
  savedBrushStyle: TBrushStyle;
  w, h: integer;
begin
  CheckCanvas;
  savedBrushStyle := FActiveCanvas.Brush.Style;
  try
    FActiveCanvas.Brush.Style := bsClear;
    w := TextWidth(s);
    h := TextHeight(s);
    case FTextDir of
      HorizDir :
        begin
          case FTextAlignHor of
            LeftText   : ;
            CenterText : x := x - w div 2;
            RightText  : x := x - w;
          end;
          case FTextAlignVert of
            TopText    : ;
            CenterText : y := y - h div 2;
            BottomText : y := y - h;
          end;
          with FViewport do
          begin
            if Clip=ClipOn then
            begin
              R := Rect(x1, y1, x2, y2);
              FActiveCanvas.TextRect(R, x+x1, y+y1, s);
            end else
              FActiveCanvas.TextOut(x+x1, y+y1, s);
          end;
        end;  // HorizDir
      VertDir :
        begin
          case FTextAlignHor of
            LeftText   : ;
            CenterText : x := x - h div 2;
            RightText  : x := x - h;
          end;
          case FTextAlignVert of
            TopText    : y := y + w;
            CenterText : y := y + w div 2;
            BottomText : ;
          end;
          FActiveCanvas.Font.Orientation := 900;
          try
            with FViewPort do
            begin
              if Clip = ClipOn then
              begin
                R := Rect(x1, y1, x2, y2);
                FActiveCanvas.TextRect(R, x+x1, y+y1, s);
              end else
                FActiveCanvas.TextOut(x+x1, y+y1, s);
            end;
          finally
            FActiveCanvas.Font.Orientation := 0;
          end;
        end;  // VertDir
    end;
  finally
    FActiveCanvas.Brush.Style := savedBrushStyle;
  end;
end;

procedure PieSlice(x, y: integer; StAngle, EndAngle, Radius: word);
begin
  Sector(x, y, StAngle, EndAngle, Radius, Radius);
end;

{ Difference to BGI: The data of PutImage originate from a TBitmap instance
  which must have been created before:
    ABitmap := TBitmap.Create
  and destroyed afterwards
    ABitmap.Free; }
procedure PutImage(x, y: integer; ABitmap: TBitmap; AMode: word);
var
  Rsrc, Rdest: TRect;
begin
  CheckCanvas;
  CheckBitmap(ABitmap);
  GlobalPoint(x, y);
  Rsrc := Rect(0, 0, ABitmap.Width, ABitmap.Height);
  Rdest := Rect(x, y, x + ABitmap.Width, y + ABitmap.Height);
  case AMode of
    NormalPut : FActiveCanvas.CopyMode := cmSrcCopy;
    XorPut    : FActiveCanvas.CopyMode := cmPatInvert;
    OrPut     : FActiveCanvas.CopyMode := cmSrcPaint;
    AndPut    : FActiveCanvas.CopyMode := cmSrcAnd;
    NotPut    : FActiveCanvas.CopyMode := cmMergeCopy;
  end;
  { The LCL canvas has many options for CopyMode (TCanvas.CopyMode).
    Possibly this is not 100% correct here... }
  FActiveCanvas.CopyRect(Rdest, ABitmap.Canvas, Rsrc);
end;

procedure PutPixel(x, y: integer; AColor: word);
begin
  CheckCanvas;
  GlobalPoint(x, y);
  FActiveCanvas.Pixels[x,y] := VGAColors[AColor];
end;

procedure Rectangle(x1, y1, x2, y2: integer);
var
  savedBrushStyle: TBrushStyle;
begin
  CheckCanvas;
  inc(x2);    // LCL ignores the lower right edge.
  inc(y2);
  GlobalPoint(x1, y1);
  GlobalPoint(x2, y2);
  CheckCorners(x1,y1, x2,y2);
  with FActiveCanvas do begin
    savedBrushStyle := Brush.Style;
    try
      Brush.Style := bsClear;
      Rectangle(x1, y1, x2, y2);
    finally
      Brush.Style := savedBrushStyle;
    end;
  end;
end;

{ Not implemented. Raises an exception because of this fundamental difference
  to BGI }
function {%H-}RegisterBGIdriver(ADriver: Pointer): integer;
begin
  NotImplementedError;
end;

{ Not implemented. Raises an exception because of this fundamental difference
  to BGI }
function {%H-}RegisterBGIFont(AFont: Pointer): integer;
begin
  NotImplementedError;
end;

procedure Sector(x, y: integer; StAngle, EndAngle, xRadius, yRadius: word);
var
  x1,y1, x2,y2, x3,y3, x4,y4 : integer;
  sinPhi, cosPhi: Double;
begin
  CheckCanvas;

  x1 := x - xRadius;
  y1 := y - yRadius;
  GlobalPoint(x1,y1);

  x2 := x + xRadius;
  y2 := y + yRadius;
  GlobalPoint(x2,y2);

  SinCos(DegToRad(StAngle), sinPhi, cosPhi);
  x3 := x + Round(xRadius * cosPhi);
  y3 := y - round(yRadius * sinPhi);
  GlobalPoint(x3,y3);

  SinCos(DegToRad(EndAngle), sinPhi, cosPhi);
  x4 := x + round(xRadius * cosPhi);
  y4 := y - round(yRadius * sinPhi);
  GlobalPoint(x4, y4);

  FActiveCanvas.Pie(x1,y1, x2,y2, x3,y3, x4,y4);
end;

procedure SetBkColor(AColorNum: word);
begin
  UseAsBkColor(PaletteToColor(AColorNum));
end;

{ Sets the line and text color.
  BUT: When calling SetColor from a TControl event handler does not call THIS
  routine but the inherited SetColor of the control. It is recommended to
  use SetFgColor as a replacement. }
procedure SetColor(AColor: word);
begin
  UseAsFgColor(PaletteToColor(AColor));
end;

{ Replacement for SetColor which cannot be called in the context of a TControl }
procedure SetFgColor(AColor: word);
begin
  UseAsFgColor(PaletteToColor(AColor));
end;

procedure SetFillPattern(APattern: FillPatternType; AColor: word);
begin
  CheckCanvas;
  User_Fill := APattern;
  SetFillStyle(UserFill, AColor);
end;

procedure SetFillStyle(APattern, AColor: word);
const
  Bits : array[0..7] of byte = ($80,$40,$20,$10,$08,$04,$02,$01);
var
  patt: FillPatternType;
  col: TColor;
  i, j: integer;
  x, y: integer;
begin
  CheckCanvas;
  if (APattern > UserFill) then
    InvalidPatternError;

  FreeAndNil(FFillBitmap);

  case APattern of
    EmptyFill :
      with FActiveCanvas do begin
        Brush.Bitmap := nil;
        Brush.Style := bsClear;
      end;
    SolidFill :
      with FActiveCanvas do begin
        Brush.BitMap := nil;
        Brush.Style := bsSolid;
        Brush.Color := PaletteToColor(AColor);
      end;
    else begin
      patt := FILL_PATTERNS[APattern]^;
      FFillBitmap := TBitmap.Create;
      col := PaletteToColor(AColor); //DWord from Paletted number
      FFillBitmap.Width := 8;
      FFillBitmap.Height := 8;
      //write bit pattern(BMP texture)
      for i := 1 to 8 do begin
        y := i-1;
        for j := 0 to 7 do begin
          x := j;
          FFillBitmap.Canvas.Pixels[x,y] := col;
        end;
      end;
      
    end;
    FFillSettings.Pattern := APattern;
    FFillSettings.Color := AColor;
    FActiveCanvas.Brush.Bitmap := FFillBitmap;
  end;
end;

{ Pattern has not effect.
  Line widths different from 1 or 3 are working, too, but line types are working
  only for line width 1. }
procedure SetLineStyle(ALineStyle, APattern, AThickness: word);
begin
  CheckCanvas;
  with FActiveCanvas.Pen do begin
    Style := PenStyle(ALineStyle);
    Width := AThickness;
  end;
end;

procedure SetTextJustify(Horiz, Vert: word);
var
  ts: TTextStyle;
begin
  CheckCanvas;
  FTextAlignHor := Horiz;
  FTextAlignVert := Vert;

  ts := FActiveCanvas.TextStyle;
  ts.Alignment := taLeftJustify;
  ts.Layout := tlTop;
  FActiveCanvas.TextStyle := ts;
end;

{ There are more possibilities with using "SetTextFont" }
procedure SetTextStyle(AFont, ADirection, ACharSize: word);
var
  nam: TFontName;
  siz: integer;
  sty: TFontStyles;
begin
  CheckCanvas;

  if ACharsize > 10 then
    ACharsize := 10;

  if (AFont > FNumFonts) then
    InvalidFontError;

  FBgiFont := AFont;
  FBgiSize := ACharSize;
  FTextDir := ADirection;
  nam := FFonts[AFont].Name;
  siz := FFonts[AFont].Sizes[ACharsize];
  sty := FFonts[AFont].Style;
  SetTextFont(nam, siz, sty);
end;

procedure SetUserCharSize(MultX, DivX, MultY, DivY: word);
begin
  NotImplementedError;
end;

procedure SetViewPort(x1, y1, x2, y2: integer; AClip: boolean);
begin
  CheckCanvas;
  CheckCorners(x1,y1, x2,y2);
  FViewport.x1 := x1;
  FViewport.y1 := y1;
  FViewport.x2 := x2;
  FViewport.y2 := y2;
  FViewport.Clip := AClip;
  FActiveCanvas.Clipping := AClip;
  if AClip then
    FActiveCanvas.ClipRect := Rect(x1, y1, x2, y2);
end;

procedure SetWriteMode(AMode: integer);
begin
  CheckCanvas;
  case AMode of
    CopyPut : FActiveCanvas.Pen.Mode := pmCopy;     // also: NormalPut
    XorPut  : FActiveCanvas.Pen.Mode := pmXor;
  end;
end;

function TextHeight(s: string): integer;
begin
  CheckCanvas;
  Result := FActiveCanvas.TextHeight(s);
end;

function TextWidth(s: string): integer;
begin
  CheckCanvas;
  Result := FActiveCanvas.TextWidth(s);
end;


//==============================================================================
//                        Additional routines
//==============================================================================

{ Returns the current background color as RGB value.
  See also GetBkColor. }
function GetBkColorRGB: TColor;
begin
  CheckCanvas;
  Result := FActiveCanvas.Brush.Color;
end;

{ Returns the current foreground painting color as RGB value.
  See also GetColor and GetFgColor. }
function GetColorRGB: TColor;
begin
  CheckCanvas;
  Result := FActiveCanvas.Pen.Color;
end;

function GetDefaultBkColorRGB: TColor;
begin
  Result := FDefaultColorRGB;
end;

{ See SetTextFont. }
procedure GetTextFont(out AFontName: TFontName; out ASize: integer;
  out AStyle: TFontStyles);
begin
  AFontName := FFontName;
  ASize := FFontSize;
  AStyle := FFontStyle;
end;

procedure GradientRect(x1, y1, x2, y2:integer; StartColor, EndColor:TColor;
  ADirection: integer);
var
  dir: TGradientDirection;
begin
  case ADirection of
    HorizDir: dir := gdHorizontal;
    VertDir: dir := gdVertical;
  end;
  FActiveCanvas.GradientFill(Rect(x1, y1, x2, y2), StartColor, EndColor, dir);
end;

{ Set background color as RGB value.
  See also: SetBkColor
  Can be called BEFORE InitGraph. }
procedure SetBkColorRGB(AColor: TColor);
begin
  UseAsBkColor(AColor);
  FBkColorRGB := AColor;
end;

{ Set line and text color as RGB value.
  See also: SetColor
  Can be called BEFORE InitGraph. }
procedure SetColorRGB(AColor:TColor);
begin
  UseAsFgColor(AColor);
end;

{ Defines the background rgb color used by GraphDefaults }
procedure SetDefaultBkColorRGB(AColor: TColor);
begin
  FDefaultBkColorRGB := AColor;
end;

{ Defines the default font for text output:
  - AFontName: name of the font. e.g. 'MS Sans Serif'
  - ASizes: the 10 character sizes in points (e.g. 8) for the 10 CharSizes
  - AStyle: font attributes as set: fsBold, fsItalic, fsUnderLine, fsStrikeOut
    e.g. [fsBold, fsItalic] of bold and italic text }
procedure SetDefaultFont(AFontName: TFontName; ASizes: array of integer;
  AStyle: TFontStyles);
begin
  SetFontParams(DefaultFont, AFontName, ASizes, AStyle);
end;

{ Defines the GothicFont for text output }
procedure SetGothicFont(AFontName: TFontName; ASizes: array of integer;
  AStyle: TFontStyles);
begin
  SetFontParams(GothicFont, AFontName, ASizes, AStyle);
end;

{ The provided bitmap instance is used in the Bar(), FillPoly() etc calls for
  filling the area.
  In contrast to BGI, any bitmap can be used here. But it must have the size of
  8x8 pixels (or be clipped to this size).
  The bitmap must have been created by ABitmap := TBitmap.Create, and can have
  been loaded from a bmp file by ABitmap.LoadFromFile(<filename>).
  After usage the bitmap must be destroyed by ABitmap.Free. }
procedure SetFillBitmap(ABitmap: TCustomBitmap);
begin
  CheckCanvas;
  FActiveCanvas.Brush.Bitmap := ABitmap;
end;

{ Defines the SansSerifFont for text output }
procedure SetSansSerifFont(AFontName: TFontName; ASizes: array of integer;
  AStyle: TFontStyles);
begin
  SetFontParams(SansSerifFont, AFontName, ASizes, AStyle);
end;

{ Defines the SmallFont for text output }
procedure SetSmallFont(AFontName: TFontName; ASizes: array of integer;
  AStyle: TFontStyles);
begin
  SetFontParams(SmallFont, AFontName, ASizes, AStyle);
end;

{ Defines the font for text output. Overwrites the settings made by "SetTextStyle".
  - AFontName: name of the font, e.g. 'Times New Roman'
  - ASize: font size in points
  - AStyle: Set of style attributes, fsBold, fsItalic, fsUnderLine, fsStrikeOut,
    normally empty []
  When the font does not exist it is replaced by a similar one. No error message. }
procedure SetTextFont(AFontName: TFontName; ASize: integer; AStyle: TFontStyles);
begin
  FFontName := AFontName;
  FFontSize := ASize;
  FFontStyle := AStyle;
  if Assigned(FActiveCanvas) then
    with FActiveCanvas.Font do
    begin
      Name := FFontName;
      Size := FFontSize;
      Style := FFontStyle;
    end;
end;

{ Defines the TriplexFont for text output }
procedure SetTriplexFont(AFontName: TFontName; ASizes: array of integer;
  AStyle: TFontStyles);
begin
  SetFontParams(TriplexFont, AFontName, ASizes, AStyle);
end;


initialization
  FDefaultBkColorRGB := clBlack;
  FDefaultColorRGB := clWhite;
  FBkColorRGB := clBlack;
  FFgColorRGB := clWhite;
  FFillBitmap := nil;
  InitFonts;
  //InitGraph??

finalization
  CloseGraph;

end.



//END LAZ_BGI code

{
All Pascal based libGraph "drawing code" code is copywright(and coded by)

   Gernot Tenchio      - original version
   Florian Klaempfl    - major updates
   Pierre Mueller      - major bugfixes
   Carl Eric Codere    - complete rewrite
   Thomas Schatzl      - optimizations,routines and  suggestions.
   Jonas Maebe         - bugfixes and optimizations

   Richard Jasmin(me)      -  bugfixes, tweaks, rewrites(2018+)

}

//following libPTC tradition ("dont include a whole unit for crap", method):

function Long2String(l: longint): string;
var
  strf:string;
begin
  str(l, strf);
  Long2String:=strf;
end;

procedure ObjectFreeAndNil(var q);
var
  tmp: TObject;
begin
  tmp := TObject(q);
  Pointer(q) := nil;
  tmp.Free;
end;

procedure FreeAndNil(var q);
var
  tmp: Pointer;
begin
  tmp := Pointer(q);
  Pointer(q) := nil;
  if tmp <> nil then
    FreeMem(tmp);
end;

//itos in C:
function IntToStr(Value: Integer): string;
begin
  System.Str(Value, Result);
end;

function IntToStr(Value: Int64): string;
begin
  System.Str(Value, Result);
end;

function IntToStr(Value: QWord): string;
begin
  System.Str(Value, Result);
end;

//theres a few more routines we need(logging)..looking into this.


//end "FPC code" - the rest has been modified "for an EXPLICIT PURPOSE", or created by me.


//basic Dword <-> SDL_Color conversion
//to get the longword

//youll notice a TON of this(including palette code) is BIT fiddling, swapping, manipulation.
//WELCOME TO GRAPHICS MODE PROGRAMMING(and data manipulation).

function RGBToDWord(somecolor:PSDL_Color):DWord;
var
    someDword:DWord;

begin
    someDWord := (somecolor^.R or somecolor^.G or somecolor^.B or $ff);
    RGBToDWord:=someDWord;
end;

{$ifdef cpu64}
function RGBAToDWord(somecolor:PSDL_Color):DWord;
var
    someDword:DWord;

begin
    someDWord := (somecolor^.R or somecolor^.G or somecolor^.B or somecolor^.A);
    RGBAToDWord:=someDWord;
end;
{$endif}

//to break down into components using RGB mask(BGR is backwards mask)
//LUT are faster than CPU "swap" routines.

function DWordToRGB(someword:DWord):PSDL_Color;

var
    somecolor: PSDL_Color;
    
begin 
   if SDL_BYTEORDER = SDL_LIL_ENDIAN then begin
        somecolor^.R := (someword and $FF000000);
        somecolor^.G := ((someword and $00FF0000) shr 8);
        somecolor^.B := ((someword  and $0000FF00) shr 16);
        somecolor^.A := $ff;
    end else begin
        somecolor^.R := $ff;
        somecolor^.G := ((someword  and $0000FF00) shr 16);
        somecolor^.B := ((someword and $00FF0000) shr 8);
        somecolor^.A := (someword and $FF000000);    
    end;    
    DWordToRGB:=somecolor;
end;

function DWordToRGBA(someword:DWord):PSDL_Color;

var
    somecolor: PSDL_Color;
begin
   if SDL_BYTEORDER = SDL_LIL_ENDIAN then begin
        somecolor^.R := (someword and $FF000000);
        somecolor^.G := ((someword and $00FF0000) shr 8);
        somecolor^.B := ((someword  and $0000FF00) shr 16);
        somecolor^.A := ((someword and $000000FF) shr 24);
    end else begin
        somecolor^.R := ((someword and $000000FF) shr 24);
        somecolor^.G := ((someword  and $0000FF00) shr 16);
        somecolor^.B := ((someword and $00FF0000) shr 8);
        somecolor^.A := (someword and $FF000000);
        
    end;
    DWordToRGBA:=somecolor;

end;



{

Its almost time we dive into Lazarus (object code) here- also a lot is done at CPP/VC/VB level almost requires objects to be used.
Im not there yet, input processing API is priority, as is cleaning up(or referencing only valid) SDL drawing prim routines.

This will add support for Lazarus, not just FPC+SDL(1+2). Its a little tricky, but I think I have at least a window handle now.

A bug in SDL crept in(sloppy code) where 1.13+ added in GL code and AA routines, but they were not implemented until v2.
So its wise to remove them, add add back MacOS (Classic) compatibility. Thats the difference.

Opengl is forced w v2, and render targets(GPU)+pipelining is used. GL PIPELINING ignored with never OSes due to use of CL+GL combo rendering.
(APPLEs METAL code, etc)

Shows you how "Stable" standards are....mhm

}


//note: this is for ASCII (DOS) , not unicode.

//byte=0..255, $00-$ff, convert this into its (ASCII) char. Will likely append this to a string at some point.
function Byte2Char(input:byte):char;
var
  output:char;
begin
   output:=chr(input);
end;

//if we want to go the other way
function Char2Byte(input:char):byte;
var
  output:byte;
begin
   output:=ord(input);
end;

var
     pixels:PSDL_Surface; //we dont know how many...could be derived from x*y*(sizeof(DWord).. an array of SDL_Color

//get the last color set(no need to fetch it from the screen, should be tracked).
//like i said: dont nuke this unit.

function GetFgRGB:PSDL_Color;
var
  color:PSDL_Color;

begin
    color:=DWordToRGB(_fgcolor);
    GetFgRGB:=color;
end;

//RGBA requires SDL v2 support
{$ifdef cpu64}
function GetFgRGBA:PSDL_Color;

var
  color:PSDL_Color;

begin
    color:=DWordToRGBA(_fgcolor);
    GetFgRGBA:=color;
end;
{$endif}

{

Palette code:
    This is a BITCH to write, come to find out we are quasi-implementing in SW. (!!)
    On top of this, it only applies to Surfaces. Inefficient, to say the least.
    
    The warning about "surface/texture format not being what you think" is due to: GL/modern HW no longer supporting palettes.
    GPU wants 24/32bpp, you are supplying 8(maybe less). While LUT can be used, output color may not be "as expected", due to "color conversion".
    Note that SDLv2 takes heav handed approach to OpenGL use.

    -Theres some basic math here(and up/downscaling involved). Basically we are being told to request certain colors, and be force fed what is "compatible", or
    our apps, libraries break.

var
    target:PSDL_PixelFormat;
    tmp,surface:PSDL_Surface;

tmp is a surface worked on somewhere else...(mindful of libHermes supported pixelformats here...)

 target := SDL_AllocFormat(SDL_PIXELFORMAT_RGBA32);
 surface := SDL_ConvertSurface(tmp, target, 0);
 SDL_FreeSurface(tmp);
 SDL_FreeFormat(target);


you take the converted surface(24/32 bpp) and "render to texture", then present(or use GL directly with it).

the ops are slow, theres no actual implementation of this anymore.

I can set this up, but its up to YOU to properly implement,use.
}

function GetFGColorIndex:byte;

var
   i:integer;
   someColor:PSDL_Color;
   r,g,b,a:PUInt8;

begin

    someColor:=DWordToRGB(_fgcolor);
    somecolor^.r:=byte(somecolor^.r);
    somecolor^.g:=byte(somecolor^.g);
    somecolor^.b:=byte(somecolor^.b);
    somecolor^.a:= $ff;


     if MaxColors=16 then begin
       i:=0;
       repeat
	        if ((Tpalette64[i].colors^.r=somecolor^.r) and (Tpalette64[i].colors^.g=somecolor^.g) and (Tpalette64[i].colors^.b=somecolor^.b))  then begin
		        GetFGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=15;
     end;

     if MaxColors=256 then begin
       i:=0;
       repeat
	        if ((TPalette256[i].colors^.r=somecolor^.r) and (TPalette256[i].colors^.g=somecolor^.g) and (TPalette256[i].colors^.b=somecolor^.b))  then begin
		        GetFGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=255;

	 end else begin
			logln('GetFGCOlorIndex: Cant Get index from an RGB mode colors.');
		exit;
	 end;
end;



//sets pen color given an indexed input
procedure setFGColorIndexed(color:byte);
var
	colorToSet:PSDL_Color;
    r,g,b:PUInt8; //^Byte, C ref
begin
   //VGA
   if MaxColors=256 then begin
        _fgcolor:=Tpalette256[color].Dwords;
        {$ifdef cpu64}
            //this depends on PutPixel use. Ideally, any furtive call to PutPixel, Render_Rect, etc.. should update this.
            //again: never assume.
            colorToSet:=Tpalette256[color].colors;        
            SDL_SetRenderDrawColor( Renderer, ord(colorToSet^.r), ord(colorToSet^.g), ord(colorToSet^.b), 255 );
        {$endif}
   //ega
   end else if MaxColors=16 then begin
		_fgcolor:=Tpalette64[color].DWords;
		{$ifdef cpu64}
		colorToSet:=TPalette64[color].colors;
		SDL_SetRenderDrawColor( Renderer, ord(colorToSet^.r), ord(colorToSet^.g), ord(colorToSet^.b), 255 );
        {$endif}
   end 
   //CGA
   else if MaxColors=4 then begin
		_fgcolor:=Tpalette16[color].DWords;
		{$ifdef cpu64}
		colorToSet:=TPalette16[color].colors;
		SDL_SetRenderDrawColor( Renderer, ord(colorToSet^.r), ord(colorToSet^.g), ord(colorToSet^.b), 255 );
        {$endif}
   end 
   //BW
   else if MaxColors=2 then begin
        if (not InvertColors) then
    		_fgcolor:=$ffffffff
	    else
	        _fgcolor:=$000000ff;
		{$ifdef cpu64}
		if (not InvertColors) then
    		SDL_SetRenderDrawColor( Renderer, ord($ff), ord($ff), ord($ff), 255 )
        else
            SDL_SetRenderDrawColor( Renderer, ord($00), ord($00), ord($00), 255 );
        {$endif}
   end 
   
   else begin
		logln('setFGColorIndexed: not in paletted(index) mode, refusing.');
		exit;
   end;
end;


//sets pen color to given dword.
procedure setFGColor(someDword:dword); overload;
var
    r,g,b:byte;
    somecolor:PSDL_Color;
    format:longWord;

begin

    _fgcolor:=someDword;
   {$IFDEF cpu64}
    
    //break apart the DWord..we dont need SDL for this.
    somecolor^.r:=byte(r);
    somecolor^.g:=byte(g);
    somecolor^.b:=byte(b);

   SDL_SetRenderDrawColor( Renderer, ord(somecolor^.r), ord(somecolor^.g), ord(somecolor^.b), 255 );
   {$endif}
end;

//bug: r,g,b, is always a byte, word for x*y location.
procedure setFGColor(r,g,b:byte); overload;

begin
   
   {$IFDEF cpu64}
   SDL_SetRenderDrawColor( Renderer, r, g, b, 255 );
   {$endif}

end;

procedure setFGColor(r,g,b,a:word); overload;

begin
   {$ifdef cpu64}
   SDL_SetRenderDrawColor( Renderer, ord(r), ord(g), ord(b), ord(a));
   {$endif}
end;



//sets background color based on index
procedure setBGColor(index:byte);
var
	colorToSet:PSDL_Color;
//if we dont store the value- we cant fetch it later on when we need it.
begin

    if MaxColors=256 then begin
        colorToSet:=Tpalette256[index].colors;
        _bgcolor:=Tpalette256[index].dwords; //set here- fetch later
   {$ifdef cpu64}

	    SDL_SetRenderDrawColor( Renderer, ord(colorToSet^.r), ord(colorToSet^.g), ord(colorToSet^.b), 255 );
	    SDL_RenderClear(Renderer);
   {$endif}

   end else if MaxColors=16 then begin
		colorToSet:=TPalette64[index].colors;
        _bgcolor:=Tpalette256[index].dwords; //set here- fetch later
   {$ifdef cpu64}

   	    SDL_SetRenderDrawColor( Renderer, ord(colorToSet^.r), ord(colorToSet^.g), ord(colorToSet^.b), 255 );
   	    SDL_RenderClear(Renderer);
   {$endif}

   end;
end;

procedure setBGColor(someDword:DWord); overload;
var
    r,g,b:PUInt8;
	somecolor:PSDL_Color;

begin

//bust out the RGB values from the DWord given, dont need sdl for this.

   {$ifdef cpu64}
   SDL_SetRenderDrawColor( Renderer, somecolor^.r, somecolor^.g, somecolor^.b, 255 );
   SDL_RenderClear(renderer);
   {$endif}
end;

procedure setBGColor(r,g,b:byte); overload;

begin

   {$ifdef cpu64}
   SDL_SetRenderDrawColor( Renderer, r, g, b, 255 );
   SDL_RenderClear(renderer);
   {$endif}

end;

//RGBA data requires use of sdl v2
{$ifdef cpu64}

procedure setBGColor(r,g,b,a:byte); overload;

begin
   SDL_SetRenderDrawColor( Renderer, r, g, b, a);
   SDL_RenderClear(renderer);
end;
{$endif}


//remember: _fgcolor and _bgcolor are DWord(s).
{$ifdef cpu64}
function GetFgDWordRGBA:DWord;

var
  somecolor:PSDL_Color;
  r,g,b,a:PUint8;

begin
    if (bpp < 32) then begin //rgba not supported
        logln('GetFgDWordRGBA: trying to fetch fgcolor from wrong input data/mode. Bailing');
        exit;
    end;

	SDL_GetRenderDrawColor(renderer,r,g,b,a);
    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=byte(^a);

    GetFgDWordRGBA:=RGBAToDWord(somecolor);
end;
{$endif}

procedure invertPalettedColors;

//so you see how to do manual shifting etc..
var
   r, g, b, a:PUInt8;
   somecolor:PSDL_Color;
begin

    if (MaxColors>256) or (bpp >8) then begin
        logln('invertpalettedColors called improperly. Set a PALETTED color mode first.');
		exit;
    end;

{$ifdef cpu64}
	SDL_GetRenderDrawColor(renderer, r, g, b, a);
{$endif}

    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=byte(^a);
//for CGA, flip the intensity around. (flip palette around)

    if (MaxColors=16) or (MaxColors =64) then
{$ifdef cpu64}
		SDL_SetRenderDrawColor(renderer, (16 - somecolor^.r), (16 - somecolor^.g), (16 - somecolor^.b), somecolor^.a);
{$endif}

    if MaxColors=256 then
{$ifdef cpu64}
		SDL_SetRenderDrawColor(renderer, (255 - somecolor^.r), (255 - somecolor^.g), (255 - somecolor^.b), somecolor^.a);
{$endif}

end;



var
	mode:PSDL_DisplayMode;

type
    TmodeList= array of TSDL_DisplayMode;


{$ifdef cpu32}
function FetchModeList:Tmodelist;
var
   VideoInfo:PSDL_VideoInfo;
begin
   VideoInfo:=SDL_GetVideoInfo;

   {

   fetches the following:
       booleans:

        hw_available	Is it possible to create hardware surfaces?
        wm_available	Is there a window manager available
        blit_hw	Are hardware to hardware blits accelerated?
        blit_hw_CC	Are hardware to hardware colorkey blits accelerated?
        blit_hw_A	Are hardware to hardware alpha blits accelerated?
        blit_sw	Are software to hardware blits accelerated?
        blit_sw_CC	Are software to hardware colorkey blits accelerated?
        blit_sw_A	Are software to hardware alpha blits accelerated?
        blit_fill	Are color fills accelerated?

        video_mem	Total amount of video memory in Kilobytes
        vfmt	Pixel format of the video device - for highest mode found.

   }
   //returns a list, if applicable to the higest bpp supported.

  if wantsFS then
    modes:=SDL_ListModes(NiL, SDL_FULLSCREEN or SDL_HWSURFACE);
  else
    modes:=SDL_ListModes(NiL, SDL_HWSURFACE);

// Check is there are any modes available */
  if(modes = 0) then begin
     logln('FetchModeList: no hardware surfaces available.');
     if wantsFS then
        modes:=SDL_ListModes(NiL, SDL_FULLSCREEN or SDL_SWSURFACE);
     else
       modes:=SDL_ListModes(NiL, SDL_SWSURFACE);
     if (modes=0) then begin
          logln('FetchModeList: no hardware surfaces available.');
          exit;
    end;

    if(modes =-1) then begin //with modeList do...

    //were going to have to do some weird format -> bpp conversion here...


        i=0;
        while i< hi(modes) do begin //we dont know how many modes there are..so "1 less than X".
            logln(modes[i]^.w,' x ' modes[i]^.h);
            //add this MaxX x MaxY list to "our Modelist".
            inc(i);
        end;
    end;

end;

{$endif}

{$ifdef cpu64}


function FetchModeList:Tmodelist;
var
    mode_index,modes_count ,display_index,display_count:integer;
    i:integer;
    modeList:TmodeList;


begin
   display_count := SDL_GetNumVideoDisplays; //1->display0
   writeln('Number of displays: '+ intTOstr(display_count));
   display_index := 0;

//for each monitor do..
while  (display_index <= display_count) do begin
    writeln('Display: '+intTOstr(display_index));

    modes_count := SDL_GetNumDisplayModes(display_index); //max number of supported modes
    mode_index:= 0;
    i:=0;

    //do for each mode in the number of possible modes
    while ( mode_index <= modes_count ) do begin

        mode^.format:= SDL_PIXELFORMAT_UNKNOWN;
        mode^.w:= 0;
        mode^.h:= 0;
        mode^.refresh_rate:= 0;
        mode^.driverdata:= Nil;


        if (SDL_GetDisplayMode(display_index, mode_index, mode) = 0) then begin //mode supported

            //Log: pixelFormat(bpp)MaxX,MaXy,refrsh_rate
            Logln(IntToStr(SDL_BITSPERPIXEL(mode^.format))+' bpp'+ IntToStr(mode^.w)+ ' x '+IntToStr(mode^.h)+ '@ '+IntToStr(mode^.refresh_rate)+' Hz ');

            //store data in a modeList array

                modeList[i].format:=SDL_BITSPERPIXEL(mode^.format);
                modeList[i].w:=mode^.w;
                modeList[i].h:=mode^.h;
                modeList[i].refresh_rate:=mode^.refresh_rate;
                modeList[i].driverdata:=mode^.driverdata;

        end;
        inc(i);
        inc(mode_index);
    end;
    inc(display_index);

end; //while

end;

//Textures are SDL v2(gl pipeline)

procedure Texlock(Tex:PSDL_Texture);

var
  pitch:PInt;
  pixels:pointer;

begin
  pixels:=Nil;
  pitch^:=0;
  if (LIBGRAPHICS_ACTIVE=false) then begin
    logln('I cant lock a Texture if we are not active: Call initgraph first');
    exit;
  end;
  if (Tex = Nil) then begin
		logln('Cannot Lock unassigned Texture.');
        exit;
  end;
//  SDL_QueryTexture(tex, format, Nil, w, h);
  SDL_SetRenderTarget(renderer, tex);

{$IFDEF mswindows}
  SDL_LockTexture(tex,Nil,pixels,pitch);
{$ENDIF}

end;

procedure TexlockwRect(Tex:PSDL_Texture; Rect:PSDL_Rect);

var

  pitch:PInt;
  pixels:pointer;

begin
  if (LIBGRAPHICS_ACTIVE=false) then begin
    logln('I cant lock a Texture if we are not active: Call initgraph first');
    exit;
  end;
  if (Tex = Nil) then begin
        logln('Cant Lock unassigned Texture');
     exit;
  end;
//  SDL_QueryTexture(tex, format, rect, w, h);
  SDL_SetRenderTarget(renderer, tex);
{$IFDEF mswindows}
  SDL_LockTexture(tex,Nil,pixels,pitch);
{$ENDIF}
end;

function lockNewTexture:PSDL_Texture;

var
  tex:PSDL_Texture;
  pitch:integer;
  Pixels:pointer;

begin
  if (LIBGRAPHICS_ACTIVE=false) then begin
    logln('I cant lock a Texture if we are not active: Call initgraph first');
    exit;
  end;
//case bpp of... sets PixelFormat(forcibly)

  tex:= SDL_CreateTexture(renderer, format, SDL_TEXTUREACCESS_STREAMING, MaxX, MaxY);
  if (tex = Nil) then begin
        logln('Cant Alloc Texture');
     exit;
  end;

  FIllDWord (tex,sizeof(tex),$00000000);

//  SDL_QueryTexture(tex, format, Nil, w, h);
  SDL_SetRenderTarget(renderer, tex);

{$IFDEF mswindows}
  SDL_LockTexture(tex,Nil,pixels,pitch);
{$ENDIF}

  lockNewTexture:=tex; //kick it back
end;

//call when done drawing pixels
procedure TexUnlock(Tex:PSDL_Texture);

begin

  if (LIBGRAPHICS_ACTIVE=false) then begin
    logln('I cant unlock a Texture if we are not active: Call initgraph first');
    exit;
  end;
{$IFDEF mswindows}
    //we are done playing with pixels so....
    SDL_UnLockTexture(tex);
{$ENDIF}

//get stuff ready for the renderer and render.
  SDL_SetRenderTarget(renderer, NiL);
  SDL_RenderCopy(renderer, tex, NiL, NiL);
  SDL_DestroyTexture(tex);

end;
{$endif}


//BGI spec
procedure clearDevice;
var
   somecolor:PSDL_Color;
begin
{$ifdef cpu32}
    FillDWord(MainSurface,sizeof(MainSurface),__bgcolor);
    SDL_PageFlip;
{$endif}

{$ifdef cpu64}
    somecolor:=DWordToRGB(_bgcolor);
    SDL_SetRenderDrawColor(Renderer,ord(somecolor^.r),ord(somecolor^.g),ord(somecolor^.b),255);
    SDL_RenderClear(renderer);
{$endif}

end;

procedure clearscreen;
//this is an alias routine

begin
    if LIBGRAPHICS_ACTIVE=true then
        clearDevice
    else begin //use crt unit
        clrscr;
    end;
end;



//clear w specified color, not current bgcolor
procedure clearscreen(index:byte); overload;

var
	r,g,b:PUInt8;
    somecolor:PSDL_Color;
begin
    if (MaxColors>256) or (PalettedMode) then begin
        logln('Attempting to clearscreen(indexed) with non-indexed data.');
        exit;
    end;
    //it just has to be within the "larger palette"
    if MaxColors=4 then
       somecolor:=TPalette4[index].colors;

    if MaxColors=16 then
       somecolor:=TPalette64[index].colors;

    if MaxColors=256 then
       somecolor:=Tpalette256[index].colors;
{$ifdef cpu32}
    FillDWord(MainSurface,sizeof(MainSurface),somecolor);
    SDL_PageFlip;
{$endif}
{$ifdef cpu64}
    SDL_SetRenderDrawColor(Renderer,ord(somecolor^.r),ord(somecolor^.g),ord(somecolor^.b),255);
	SDL_RenderClear(renderer);
{$endif}
end;


procedure clearscreen(color:Dword); overload;

var
    somecolor:PSDL_Color;
    r,g,b:PUInt8; //byte(ptr)
    i:integer;

begin
    if (bpp =2) then begin //cga
       	i:=0;
		repeat
				if color=TPalette16[i].DWords then begin; //if the DWord matches
					{$ifdef cpu32}
                         FillDWord(MainSurface,sizeof(MainSurface),color);
                         SDL_FLip;
                    {$endif}

                    {$ifdef cpu64}
                        somecolor:=DWordToRGB(color); //assign a RGB vaule from a DWord(break it apart)
                        SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,255);
	                    SDL_RenderClear(renderer);  //this isnt "zap texture" or "textureClear"..its usually a "fullscreen clear".
	                {$endif}
				end;

        until i=15;
        if not (color=TPalette16[i].DWords) then begin;
	        logln('clearscreen: color specified is out of range for given bpp.');
		    exit;
		end;

    end;


	if (bpp =4) then begin //ega
	    //get index from DWord- if it matches
        i:=0;
        repeat
				if color=TPalette64[i].DWords then begin; //if the DWord matches
					{$ifdef cpu32}
                         FillDWord(MainSurface,sizeof(MainSurface),color);
                         SDL_FLip;
                    {$endif}

                    {$ifdef cpu64}
                        somecolor:=DWordToRGB(color); //assign a RGB vaule from a DWord(break it apart)
                        SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,255);
	                    SDL_RenderClear(renderer);  //this isnt "zap texture" or "textureClear"..its usually a "fullscreen clear".
	                {$endif}
				end;
		inc(i);
        until i=63; //yes, actually test the whole palette, not just the assigned colors.
	    if not (color=TPalette64[i].DWords) then begin;
	        logln('clearscreen: color specified is out of range for given bpp.');
		    exit;
		end;

    end;

	if bpp =8 then begin
	    //get index from DWord- if it matches
        i:=0;
        repeat
            if color=Tpalette256[i].DWords then begin;
               {$ifdef cpu32}
                 FillDWord(MainSurface,sizeof(MainSurface),color);
                 SDL_FLip;
              {$endif}

              {$ifdef cpu64}
                 somecolor:=DWordToRGB(color); //assign a RGB vaule from a DWord(break it apart)
                 SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,255);
	             SDL_RenderClear(renderer);  //this isnt "zap texture" or "textureClear"..its usually a "fullscreen clear".
	          {$endif}
            end;
            inc(i);
        until i=255;
	    if color=TPalette256[i].DWords then begin
	        logln('clearscreen: color specified is out of range for given bpp.');
	        exit;
	    end;
	end;

	case bpp of
        15,16,24: begin

            {$ifdef cpu32}
              FillDWord(MainSurface,sizeof(MainSurface),color);
              SDL_FLip;
            {$endif}

            {$ifdef cpu64}
              somecolor:=DWordToRGB(color); //assign a RGB vaule from a DWord(break it apart)
              SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,255);
	          SDL_RenderClear(renderer);  //this isnt "zap texture" or "textureClear"..its usually a "fullscreen clear".
	        {$endif}
	    end;
	    {$ifdef cpu64}
	    32:begin
              somecolor:=DWordToRGB(color); //assign a RGB vaule from a DWord(break it apart)
              SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,somecolor^.a);
	          SDL_RenderClear(renderer);  //this isnt "zap texture" or "textureClear"..its usually a "fullscreen clear".
	    end;
	    {$endif}
    end;
end;

//these two dont need conversion of the data
procedure clearscreen(r,g,b:byte); overload;

var
    color:PSDL_Color;
    somecolor:DWord;
begin
    color^.r:=r;
    color^.g:=g;
    color^.b:=b;
//  color^.a:=255;

    {$ifdef cpu32}
       somecolor:=RGBToDWord(color);
       FillDWord(MainSurface,sizeof(MainSurface),somecolor);
       SDL_FLip;
    {$endif}
    {$ifdef cpu64}
	   SDL_SetRenderDrawColor(Renderer,ord(r),ord(g),ord(b),255);
	   SDL_RenderClear(renderer);
	{$endif}
end;

{$ifdef cpu64} //rgba = sdlv2

procedure clearscreen(r,g,b,a:byte); overload;
begin
	   SDL_SetRenderDrawColor(Renderer,ord(r),ord(g),ord(b),ord(a));
	   SDL_RenderClear(renderer);
end;

{$endif}

var
    texBounds:array[0..8] of PSDL_Rect;
    
//this is for dividing up the screen or dialogs (in game or in app)

//spec says clear the last one assigned
procedure clearviewport;

//clears it, doesnt remove or add a "layered window".
//usually the last viewport set..not necessary the whole "screen"
var
  viewport:PSDL_Rect;

begin
   viewport^.X:= texBounds[windownumber]^.x;
   viewport^.Y:= texBounds[windownumber]^.y;
   viewport^.W:= texBounds[windownumber]^.w;
   viewport^.H:= texBounds[windownumber]^.h;

{$ifdef cpu32}  //viewport could be anywhere, dont assume its FS window.
   if ActiveSurface=nil then
		FillDWord (MainSurface^,(texBounds[windownumber]^.x*texBounds[windowsnumber]^.y*4), _bgColor);
   else
		FillDWord (ActiveSurface^,(texBounds[windownumber]^.x*texBounds[windowsnumber]^.y*4), _bgColor);

{$endif}

{$ifdef cpu64}
//fill with current color
   SDL_RenderFillRect(Renderer, viewport);
{$endif}
end;

//EpicTimer:
// at every X(or close to X in multitasking)- call procedure Y.

// AddTimer(15,videoCallBack);  --15ms ~60fps

//Do not process anything here.

procedure videoCallback;

begin
   if (not Paused) then begin //if paused then ignore screen updates
{$ifdef cpu32}
           SDL_Flip;
{$endif}

{$ifdef cpu64}
           SDL_RenderPresent(Renderer);
{$endif}
   end;
end;


//The point Ive been making here is that nowhere to be found(utils??) is the Line() routine..
//If youre tapping it, its likely thru SDL v2, not SDL v1.2.
type
    Directions=(Left,Right,Up,Down,DiagRight,DiagLeft);

Procedure Line(x,y,x1,y1:Word);
//ignore linestyles and dotted lines for a minute..
//drawing routines assume: 1 Visual "page", direct Texture/surface ops, and that they are the only routine used.

//no point in drawing over Surfaces and Textures(used in most games: this is "presentation/drawing API")..only an assumed "blank" canvas.
// you might be stacking "pen actions"...but generally...this is true.

var
    negX,negY:Word;
    Direction:Directions;

begin

    if (x1>x) then Direction:=Right;
    if (x>x1) then Direction:=Left;
    if (x1>x) then Direction:=Down;
    if (x>x1) then Direction:=Up;

    //its either Slash..or BackSlash...

    if (x1>x) and (y1>y) then Direction:=DiagRight;
    if (x>x1) and (y>y1) then Direction:=DiagLeft;

    case (Direction)of //inverse of what you think.. we are putting pixels on screen in this direction.

    Right: begin // dont touch Y axis
        if (x1>MaxX) then exit;
        repeat
           if (x>MaxX) then exit; //clip
           {$ifdef cpu32}

               //Surface:=SDL_GetVideoSurface;
               //putpixel(surface,x,y); --check putpixel
               PutPixel(x,y);
           {$endif}
           {$ifdef cpu64}
               SDL_RenderDrawPoint(renderer,x,y);
           {$endif}
           inc(x);
        until (x=x1);
    end;

    Left: begin
        negX:=(x-x1); //go backwards by "x1" amount of pixels.
        if (negX <=0) or (x1>=x) then exit; //impossible situation, just dont draw.(clipped)
        repeat
           if x<0 then exit;
            {$ifdef cpu32}
           //to which surface?? agh!!
               PutPixel(x,y);
           {$endif}
           {$ifdef cpu64}
                SDL_RenderDrawPoint(renderer,x,y);
           {$endif}
           dec(x);
        until (x=negX);
    end;

    Up: begin //dont touch X axis
    NegY:=(y-y1);
    if (NegY<=0) or (y1>=y) then exit;
        repeat
           if y<0 then exit;
           {$ifdef cpu32}
           //to which surface?? agh!!
               PutPixel(x,y);
           {$endif}
           {$ifdef cpu64}
                SDL_RenderDrawPoint(renderer,x,y);
           {$endif}
           dec(y);
        until (y=negY);
    end;

    Down: begin
        if (y > MaxY) then exit; //impossible situation, just dont draw.(clipped)
        repeat
            if (y > MaxY) then exit;
            {$ifdef cpu32}
           //to which surface?? agh!!
               PutPixel(x,y);
           {$endif}
           {$ifdef cpu64}
                SDL_RenderDrawPoint(renderer,x,y);
           {$endif}
           inc(y);
        until (y=y1);
    end;
    //why not AND ..the other way around? youll see.. there's only two diagnals.

    DiagLeft: begin //down and to the left

        negX:=(x-x1); //go backwards by "x1" amount of pixels.
        if (y > MaxY) then exit; //impossible situation, just dont draw.(clipped)
        if (negX <=0) or (x1>=x) then exit; //impossible situation, just dont draw.(clipped)
        repeat
           if (x<0) or (y > MaxY) then exit;
            {$ifdef cpu32}
           //to which surface?? agh!!
               PutPixel(x,y);
           {$endif}
           {$ifdef cpu64}
                SDL_RenderDrawPoint(renderer,x,y);
           {$endif}
           dec(x);
           inc(y);
        until (x=negX) or (y=y1);
    end;

    DiagRight: begin //down and to the right
    //fixme: temp unimplimented
    
   
        if (x1>MaxX) then exit;
        repeat
           if (x>MaxX) then exit; //clip
           {$ifdef cpu32}
           //to which surface?? agh!!
               PutPixel(x,y);
           {$endif}
           {$ifdef cpu64}
                SDL_RenderDrawPoint(renderer,x,y);
           {$endif}
           inc(x);
        until (x=x1);
    end;

  end; {case}

end;


var
    FontPointer:PTTF_Font;

procedure Rectangle(Rect:PSDL_Rect); //if you want one, give me four points

begin
    {$ifdef cpu32}
		Line(x,y,x1,y1);
		Line(x1,y1,y2,x2);
		Line(x2,y2,x3,y3);
        Line(x3,y3,x,y)
    {$endif}

    {$ifdef cpu64}
		SDL_RenderDrawRect(renderer,Rect); //draw the box- inside the new "window" shrunk by 2 pixels (4-6 may be better)
	{$endif}
end;


procedure Triangle(Tri:TriPoints); //if you want one, give me three points
var
	TempTex:PSDL_Texture;
	NewSurface,TempSurface:PSDL_Surface;


begin

{$ifdef cpu32}
		NewSurface:=SDL_CreateRGBSurfaceWithFormat(0, MaxX, MaxY, bpp, MainSurface^.format);
{$endif}
{$ifdef cpu64}
//theres no "Main"Surface...
		case (bpp) of

		1,4,8: format:=SDL_PIXELFORMAT_INDEX8;
		15: format:=SDL_PIXELFORMAT_RGB555;

        //we assume on 16bit that we are in 565 not 5551, we should not assume
		16:	format:=SDL_PIXELFORMAT_RGB565;
		24: format:=SDL_PIXELFORMAT_RGB888;

		{$ifdef cpu64}
		32: format:=SDL_PIXELFORMAT_RGBA8888;
		{$endif}
		end;
		NewSurface:=SDL_CreateRGBSurfaceWithFormat(0, MaxX, MaxY, bpp, format);
{$endif}

    if (Newsurface = NiL) then begin
        Logln('SDL_CreateRGBSurface() failed.');

    end;

		Line(Tri[0].x,Tri[0].y,Tri[1].x,Tri[1].y);
		Line(Tri[1].x,Tri[1].y,Tri[2].y,Tri[2].x);
		Line(Tri[2].x,Tri[2].y,Tri[0].x,Tri[0].y);

{$ifdef cpu64}
		TempTex := SDL_CreateTextureFromSurface(Renderer, NewSurface);

		SDL_RenderCopy(renderer, TempTex, Nil, Nil);
        SDL_FreeSurface(NewSurface);
{$endif}

end;

//we need parallax scrolling, oft used with tiles, game backgrounds, etc.

//This is used with terminals, including OS development.
//Take a scanline(or a bunch of them(y=MaxX*SDL_Color)

//Left

//Right

//UP

//Down

//Diagnial(update two of the above at one time..)


{$ifdef cpu64}
{
 Draw an Texture to an Renderer at x, y.
 preserve the texture's width and height- also taking a clip of the texture if desired

- we can use just RenderCopy, but it doesnt take stretching or clipping into account.(its sloppy)

I didnt write this (from stackExchange in C) but it makes sense and SDL is at least "GPL", so we are "safe in licensing".

}


procedure renderTexture( tex:PSDL_Texture;  ren:PSDL_Renderer;  x,y:word;  clip:PSDL_Rect);

var
  dst:PSDL_Rect;

begin

	dst^.x := x;
	dst^.y := y;
	if (clip <> Nil)then begin
		dst^.w := clip^.w;
		dst^.h := clip^.h;
	end
	else begin
		SDL_QueryTexture(tex, NiL, NiL, @dst^.w, @dst^.h);
	end;
	SDL_RenderCopy(ren, tex, clip, dst); //clip is either Nil(whole surface) or assigned

end;

{$endif}

procedure setgraphmode(graphmode:graphics_modes; wantfullscreen:boolean); //with (one of) modes and "fs flag" do...
//initgraph should call us to prevent code duplication
//exporting a record is safer..but may change "a chunk of code" in places.
var
	flags:longint;
    success,IsThere,RendedWindow:integer;
    surface1:PSDL_Surface;
    FSNotPossible:boolean;
    thismode:string;
    wantsFS:string;
    surface:PSDL_Surface;

begin
//we can do fullscreen, but dont force it...
//"upscale the small resolutions" is done with fullscreen.


//if not active:
//are we coming up?
//no? EXIT

if (LIBGRAPHICS_INIT=false) then

begin
		logln('Call initgraph before calling setGraphMode.');
		exit;
end
else if (LIBGRAPHICS_INIT=true) and (LIBGRAPHICS_ACTIVE=false) then begin //initgraph called us


    case bpp of
		1,4,8: format:=SDL_PIXELFORMAT_INDEX8; //yes, 8bpp unless otherwise specified. As long as Palettes are specified, we are ok.
		15: format:=SDL_PIXELFORMAT_RGB555;

        //we assume on 16bit that we are in 565 not 5551, we should not assume
		16:	format:=SDL_PIXELFORMAT_RGB565;
		24: format:=SDL_PIXELFORMAT_RGB888;
		{$ifdef cpu64}
		32: format:=SDL_PIXELFORMAT_RGBA8888;
		{$endif}

    end;

    logln('MaxX: '+Inttostr(MaxX));
    logln('MaxY: '+Inttostr(MaxY));
    logln('bpp: '+Inttostr(bpp));

    if wantfullscreen = true then
        wantsFS:='True'
    else
		wantsFS:='False';
    logln('Full screen requested: '+wantsFS);


         {$IFNDEF LCL} //Not using Lazarus  --note this is "the default assumption". LCL/lazarus hooks are untested, experimental, and buggy right now.
         //according to the lcl docs, however, the code below **should work**.

         {$ifdef cpu32}

            //for amiga: use intiution(unit) to handle, fetch window handles from.

           //call for a surface, which pulls open a window from the OS. SDLv2 does this backwards- sets up a window, then a renderer, then a surface/texture.
           if WantFullScreen then
    		   MainSurface:=SDL_SetVideoMode(MaxX, MaxY, bpp, SDL_HWSURFACE or SDL_DOUBLEBUF or SDL_FULLSCREEN or SDL_HWPALETTE);
    	   else //windowed mode
               MainSurface:=SDL_SetVideoMode(MaxX, MaxY, bpp, SDL_HWSURFACE or SDL_DOUBLEBUF or SDL_HWPALETTE);
    	   if MainSurface=Nil then begin	//no hw acceleration available
                if WantFullScreen then
    		       MainSurface:=SDL_SetVideoMode(MaxX, MaxY, bpp, SDL_SWSURFACE or SDL_DOUBLEBUF or SDL_FULLSCREEN);
    	        else //windowed mode
                   MainSurface:=SDL_SetVideoMode(MaxX, MaxY, bpp, SDL_SWSURFACE or SDL_DOUBLEBUF);
    	       if MainSurface=Nil then begin
	        	   	logln('Something Fishy. Cant setup a window, and no HW acceleration available.');
	        	   	logln('SDL reports: '+' '+ SDL_GetError);
    	        	closegraph;
    	       end;
    	   end;
           exit; //we are done

         {$endif}

         {$ifdef cpu64} //bpp is ignored, sw lut and etc are used. assumes use of 24 or 32bpp native hardware support.(and most likely case)
            //posX,PosY,sizeX,sizeY,flags
         {$IFNDEF LCL}  
            window:= SDL_CreateWindow(PChar('Lazarus Graphics Application'), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, MaxX, MaxY, 0);
         {$ENDIF}
         
         //dont crete a window, use the existing. 
         {$IFDEF LCL}
     {              under GTK a handle is often a PGtkWidget(see below)
                    under Windows a handle is often a HWnd.  (window:= Windows.CreateWindow(PChar('LAZGfX Application'), '', WS_OVERLAPPED, 0, 0, 0, 0, 0, 0, MainInstance, nil);)  
                    under OSX/Carbon a handle is often a object descendant of TCarbonWidget. (TCarbonWidget.LCLObject /TCarbonWindow )
                    under Qt, a handle is often a pointer to a object descendent of TQtWidget. (TQtWidget.LCLObject /TQtMainWindow(Handle).Widget)
    }
			         //WindowHandle -> Application.MainForm.Handle as per the forums,Form1.Handle should work.
		
            {$IFDEF UNIX}           
                //MacOSX
                {$IFDEF Darwin}
                   // window:=(Pointer(TCarbonWidget(PtrUInt(Application.MainForm.Handle))^.widget));
                   
                   //MacWin := TCarbonWidget(Form1.Handle).Widget;
                    MacWin := TCarbonWindow(Form1.Handle).Window;
                {$ENDIF}

                {$IFNDEF DARWIN} //most unices
			
                     window:=GDK_WINDOW_XWINDOW(pointer(PGtkWidget(Form1.Handle)^.Window));
                  //   window:=GDK_WINDOW_XWINDOW(Pointer(PGTKWidget(PtrUInt(Application.MainForm.Handle))^.window));
                {$ENDIF}
                
            {$ENDIF}
			 {$IFDEF mswindows}
			   //cheat: we should be in the foreground
                     Window := GetForegroundWindow; 
            {$ENDIF}

         {$ENDIF}   //lcl

		 if (window = Nil) then begin

	    	   	logln('Something Fishy. I cant grab a window handle from Lazarus API.');
	    	   	logln('SDL reports: '+' '+ SDL_GetError);

	    	closegraph;
		end;
        logln('initgraph: Window is online.');


   if wantFullscreen then begin //init first, then set window->FS

       flags:=(SDL_GetWindowFlags(window));
       flags:=(flags or SDL_WINDOW_FULLSCREEN_DESKTOP); //fake it till u make it..
       //we dont want to change HW videomodes.

       //autoscale us to the monitors actual resolution
       SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 'linear');
       //phones: SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");

       SDL_RenderSetLogicalSize(renderer, MaxX, MaxY);
	   IsThere:=SDL_SetWindowFullscreen(window, flags);

       //thisMode:=getgraphmode;
  	   if ( IsThere < 0 ) then begin
    	 logln('Unable to set FS: ');
    	 logln('SDL reports: '+' '+ SDL_GetError);
         FSNotPossible:=true;

         //if we failed then just gimmie a yuge window..
         SDL_SetWindowSize(window, MaxX, MaxY);

    end;

    	renderer := SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED or SDL_RENDERER_PRESENTVSYNC);
		if (renderer = Nil ) then begin //no renderer w a window
             surface := SDL_GetWindowSurface(window);
			 renderer := SDL_CreateSoftwareRenderer(surface); //create sw if accel hw fails.
        end;
		if (renderer = Nil ) then begin //no renderer and no sw support
	    	   	logln('Something Fishy. No hardware render support and cant setup a software one.');
	    	   	logln('SDL reports: '+' '+ SDL_GetError);
				closegraph;
		end;
    logln('initgraph: rendering online');

{$ENDIF}


//    if bpp=2 then
		//which palette - out of 4(CGA)? - mode has determine this, even if reprogrammed later on.

    if bpp=4 then //EGA
      IniTPalette64;
    if bpp=8 then //VGA
      InitPalette256;

     LIBGRAPHICS_ACTIVE:=true;
     exit; //back to initgraph we go.

  end;
  
  end else if (LIBGRAPHICS_ACTIVE=true) then begin //good to go


    case bpp of
		8: if maxColors<256 then format:=SDL_PIXELFORMAT_INDEX8;
		15: format:=SDL_PIXELFORMAT_RGB555;

        //we assume on 16bit that we are in 565 not 5551, we should not assume. mode 5551 A bit is (00,7f,ff) - black doesnt count(double avail colors).
		//how to determine which to use?
		16: format:=SDL_PIXELFORMAT_RGB565;
		24: format:=SDL_PIXELFORMAT_RGB888;
		{$ifdef cpu64}
		32: format:=SDL_PIXELFORMAT_RGBA8888;
		{$endif}
    end;

        mode^.w:=MaxX;
		mode^.h:=MaxY;
		mode^.refresh_rate:=60; //assumed
		mode^.driverdata:=Nil;

//sdlv1 or sdlv2?
		success:=SDL_SetWindowDisplayMode(window,mode);
        if (success <> 0) then begin
                logln('ERROR: SDL cannot set DisplayMode.');
                logln(SDL_GetError);
			exit;
        end;
		logln('Resolution Re-Size requested.');
        logln('New MaxX: '+InttoStr(MaxX));
		logln('New MaxY: '+Inttostr(MaxY));
		logln('New bpp: '+Inttostr(bpp));
		if wantfullscreen = true then
			wantsFS:='True'
		else
			wantsFS:='False';
		logln('Full screen requested: '+wantsFS);

   if wantFullscreen then begin
       //I dont know about double buffers yet but this looks yuumy..
       //SDL_SetVideoMode(MaxX, MaxY, bpp, SDL_DOUBLEBUF|SDL_FULLSCREEN);

       flags:=(SDL_GetWindowFlags(window));
       flags:=flags or SDL_WINDOW_FULLSCREEN_DESKTOP; //fake it till u make it..

       if ((flags and SDL_WINDOW_FULLSCREEN_DESKTOP) <> 0) then begin
            SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 'linear');
            SDL_RenderSetLogicalSize(renderer, MaxX, MaxY);
            //phones: SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
       end;

	   IsThere:=SDL_SetWindowFullscreen(window, flags);

  	   if ( IsThere < 0 ) then begin
     	         logln('Unable to set FS: ');
    	         logln('SDL reports: '+' '+ SDL_GetError);

              FSNotPossible:=true;

          //if we failed then just gimmie a yuge window..
          SDL_SetWindowSize(window, MaxX, MaxY);
       end;

    end; //wantsFS

   // if bpp=2 then
		//which palette - out of 4(CGA)? - mode has determine this, even if reprogrammed later on.

    if bpp=4 then //EGA
      IniTPalette64;
    if bpp=8 then //VGA
      InitPalette256;

  end; //libGFX active

end; //setGraphMode


//need to export , like with SDLv1. Time for some "headers hackery," again.

{$ifdef cpu64}
function GetPixel(x,y:word):DWord;

// this function is "inconsistently fucked" from C...so lets standardize it...
// this works, its hackish -but it works..SDL_GetPixel is for Surfaces/screens, not renderers.
var
  format:longword;
  TempSurface:PSDL_Surface;
  bpp:byte;
  p:pointer; //the pixel data we want
  GotColor:PSDL_Color;
  pewpew:longword; //DWord....
  pointpew:PtrUInt;
  r,g,b,a:PUInt8;
  index:byte;
  someDWord:DWord;


begin
	rect^.x:=x;
    rect^.y:=y;
	rect^.h:=1;
	rect^.w:=1;
    case bpp of
		8: format:=SDL_PIXELFORMAT_INDEX8;
		15: format:=SDL_PIXELFORMAT_RGB555;

//we assume on 16bit that we are in 565 not 5551, we should not assume
		16: format:=SDL_PIXELFORMAT_RGB565;

		24: format:=SDL_PIXELFORMAT_RGB888;
		32: format:=SDL_PIXELFORMAT_RGBA8888;

    end;


    //(we do this backwards....rendered to surface of the size of 1 pixel)
    if ((MaxColors=256) or (MaxColors=16)) then TempSurface := SDL_CreateRGBSurfaceWithFormat(0, 1, 1, 8, format)
    //its weird...get 4bit indexed colors list in longword format but force me into 256 color mode???

    else if (MaxColors>256) then begin
        case (bpp) of
		    15: TempSurface := SDL_CreateRGBSurfaceWithFormat(0, 1, 1, 15, format);
			16: TempSurface := SDL_CreateRGBSurfaceWithFormat(0, 1, 1, 16, format);
			24: TempSurface := SDL_CreateRGBSurfaceWithFormat(0, 1, 1, 24, format);
			32: TempSurface := SDL_CreateRGBSurfaceWithFormat(0, 1, 1, 32, format);
			else
                logln('Wrong bpp given to get a pixel. I dont how you did this.');
		end;
     end;

    //read a 1x1 rectangle....
    SDL_RenderReadPixels(renderer, rect, format, TempSurface^.pixels, TempSurface^.pitch);


    p:=TempSurface^.pixels;
    bpp := TempSurface^.format^.BytesPerPixel;
    //The C is flawed! 15bpp not taken into account.
    //ignorance is stupidity and laziness!

    // Here TempSurface^.pixels is the address to the pixel data (1px) we want to retrieve


{

"nuking the problem" (unnessessary overcomplication)
 the problem - not well documented for surfaces in C-

is thusly:

  P can be of any size up to 32bit values (because sdl doesnt support 64bit modes)

  is P a byte? (16/256 colors)
  is P a Word? NO. NEVER.Not enough BPP data returned.
  is P a partial DWord? (15/16bit/24bit colors) - spew SDL_Color data (for each byte inside P),
      then fetch an appropriate DWord from that
      (this is a bitwise operation, so we need the DWord inside the pointer to work with the data)

  is P a DWord? (32bit colors)

no type conversion is needed as pointers are flexible
however endianness, although it should be checked for (ALWALYS) plays no role in how P is interpreted.

-and people wonder why thier data coming back from P is flawed?? mhm...

-you nuked it!

}
    case (bpp) of
       8:begin //256 colors
        //now take the longword output

          //check for the hidden 4bpp modes we made available
          if MaxColors=16 then begin
             index:=ord(Byte(^p));
             GetPixel:=TPalette64[index].Dwords;
          end;

          //ok safely in 256 colors mode
          if MaxColors=256 then begin
             index:=ord(Byte(^p));
             GetPixel:=Tpalette256[index].Dwords;
          end;
          exit;
      end;

//these three need some work
//not its not a trick, we need the alpha-bit set to FF on non 32bit colors.
//its ignored but its also "padded data"

      15: begin //15bit
            //to do this one- we split out the RGB pairs, shift them,then recombine them.
            SDL_GetRGB(Longword(^p),TempSurface^.format,r,g,b);

			//play fetch

             gotcolor^.r:=(byte(^r) );
		     gotcolor^.g:=(byte(^g) );
	         gotcolor^.b:=(byte(^b) );

            //shift adjust me
			gotcolor^.r:= (gotcolor^.r shr 3);
			gotcolor^.g:= (gotcolor^.g shr 3);
			gotcolor^.b:= (gotcolor^.b shr 3);

            //repack
            someDWord:= SDL_MapRGB(TempSurface^.format,gotcolor^.r,gotcolor^.g,gotcolor^.b);
            GetPixel:=someDWord;
            exit;
      end;
      16: begin //16bit-565
        //    if (TempSurface^.format=SDL_PIXELFORMAT_RGB565) then begin

            SDL_GetRGB(Longword(^p),TempSurface^.format,r,g,b);

			//play fetch

             gotcolor^.r:=(byte(^r) );
		     gotcolor^.g:=(byte(^g) );
	         gotcolor^.b:=(byte(^b) );

            //shift adjust me
			gotcolor^.r:= (gotcolor^.r shr 3);
			gotcolor^.g:= (gotcolor^.g shr 2);
			gotcolor^.b:= (gotcolor^.b shr 3) ;

           //repack
            someDWord:= SDL_MapRGB(TempSurface^.format,gotcolor^.r,gotcolor^.g,gotcolor^.b);
            GetPixel:=someDWord;
      		exit;
      end;


      24: begin //24bit
            SDL_GetRGB(Longword(^p),TempSurface^.format,r,g,b);

			//play fetch

             gotcolor^.r:=(byte(^r) );
		     gotcolor^.g:=(byte(^g) );
	         gotcolor^.b:=(byte(^b) );
           //repack
            someDWord:= SDL_MapRGB(TempSurface^.format,gotcolor^.r,gotcolor^.g,gotcolor^.b);
            GetPixel:=someDWord;
			exit;
      end;
      32: //32bit

	        GetPixel:=DWord(p);

      else
                logln('Cant Put Pixel values...');
    end; //case

    SDL_FreeSurface(Tempsurface);

end;

Procedure PutPixel; //assumes fgcolor
var
    Surface:PSDL_Surface;

begin
{$ifdef cpu32}
    Surface:=SDL_GetVideoSurface;
    PutPixelXY(Surface,Currx,Curry:Word);
{$endif}

{$ifdef cpu64}
    //either set "TextureStreaming" and create one, or use RenderDrawPoint
    SDL_RenderDrawPoint(Renderer, Currx,Curry);
{$endif}
end;

{$ifdef cpu64}
Procedure PutPixelXY(Renderer:PSDL_Renderer; x,y:Word);

begin
//SDL_renderDrawPoint uses _fgcolor set with SDL_SetRenderDrawColor
  SDL_RenderDrawPoint( Renderer, X, Y );
end;

{$endif}

{$ifdef cpu32}
Procedure PutPixelXY(someSurface:PSDL_Surface; x,y:Word);

begin
    SDL_PutPixel(MainSurface,x,y);
end;

{$endif}


{ifdef cpu64
procedure Line(x,y,a,b:Word);
//alias- for compatibility
begin
	SDL_RenderDrawLine(Renderer,x,y,a,b);
end;
endif}

function getmaxmode:string;
var
   maxmodetest:byte;
   
begin
  if LIBGRAPHICS_ACTIVE then begin
//      maxmodetest:=detectgraph(fullscreenSetting);
//      getmaxmode:=GetEnumName(TypeInfo(graphics_modes), Ord(maxmodetest)); //use the number and get the name of it.
  end;
end;


//the only real use for the "driver" setting...
procedure getmoderange(graphdriver:integer);

begin
{
I have to figure out what mode is what again...
- no im not lazy, just busy.

	if not graphdriver = ord(detect) then begin
  		if (graphdriver = ord(VGA)) then begin
    		  lomode := ord(VGAMed);
    		  himode := ord(m1024x768x24);
  		end else

  		if (graphdriver= ord(VESA)) then begin
    		 lomode := ord(mCGA);
    		 himode := ord(m1920x1080x24);
  		end else

		if (graphdriver = ord(mCGA)) then begin
    	  lomode := ord(mCGA);
    	  himode := ord(mCGA);
  		end;

	end else begin
        if (graphdriver=ord(DETECT)) then begin
            himode:=detectgraph;	 //unfortunate probe here...
// FIXME: return value: (byte to enum value conversion?? - or use a bunch of consts)
// default enum type reads: longword

	    	lomode:= ord(mCGA); //no less than this.
		end else begin
			if IsVTerm then
				logln('Unknown graphdriver setting.');
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,'Unknown graphdriver setting.','ok',NIL);
		end;
	end;
}
end; //getModeRange



//(Its an odd rare case,such as TnL --not discussed here)

function cloneSurface(surface1:PSDL_Surface):PSDL_Surface;
//Lets take the surface to the copy machine...

begin
    cloneSurface := SDL_ConvertSurface(Surface1, Surface1^.format, Surface1^.flags);
end;

{viewports-
    the trick is that we are technically always in one(FPC).
    the joke is:
         what are the co ords?

this might not be BGI spec- but it makes sense.


dont use these two while rendering, pause everything.
}

//for compatibility
procedure SetViewPort(x,y,w,h:Word);
var
   Rect:PSDL_Rect;
begin
    Rect^.X:=x;
    Rect^.Y:=y;
    Rect^.w:=w;
    Rect^.h:=h;

    SDLSetViewPort(Rect);
end;

var
    Textures:array [0..7] of PSDL_Texture;

procedure SDLSetViewPort(Rect:PSDL_Rect);

var
    LastRect:PSDL_Rect;
    ScreenData:Pointer;
    infosurface,savesurface:PSDL_Surface;
    StartXViewPort,StartYViewPort,ViewWidth,ViewHeight:Word;

begin
//freeze movement
   if windownumber=4 then begin
      logln('Attempt to create too many viewports.');
      exit;
   end;
//the beauty of this code, is that I set it up so it doesnt matter if SDLv1 supports it or not.

  if (rect^.X > MaxX) or (rect^.w > MaxX) or (rect^.X > rect^.w) or (rect^.w < 0) then Begin
    logln('invalid setviewport parameters: ('+strf(rect^.x)+','+strf(rect^.y)+'), ('+strf(rect^.w)+','+strf(rect^.h)+')');
    logln('maxx = '+strf(getmaxx)+', maxy = '+strf(getmaxy));
    exit;
  end;

  if (rect^.Y > MaxY) or (rect^.h > MaxY) or (rect^.y > rect^.h) or (rect^.h < 0) then  Begin
    logln('invalid setviewport parameters: ('+strf(rect^.x)+','+strf(rect^.y)+'), ('+strf(rect^.w)+','+strf(rect^.h)+')');
    logln('maxx = '+strf(getmaxx)+', maxy = '+strf(getmaxy));
    exit;
  end;

//lock x,y to viewport, not abs screen coords.
  CurrX := Rect^.x;
  CurrY := Rect^.y;
  StartXViewPort := Rect^.X;
  StartYViewPort := Rect^.Y;
  ViewWidth :=  Rect^.w-Rect^.X;
  ViewHeight:=  Rect^.h-Rect^.Y;


{$ifdef cpu64}
   //get viewport size and put into LastRect
   SDL_RenderGetViewport(renderer,Lastrect);
 {$endif}
   //current co ords = last, then add one.
   inc(windownumber);

  //pixels and info abt the pixels. from the renderer-to a surface, then throw it back into a texture
 //fixme: needs a new routine: ScreenData:=GetPixels(LastRect);
  infoSurface := SDL_GetWindowSurface(Window);
  saveSurface := SDL_CreateRGBSurfaceWithFormatFrom(ScreenData, infoSurface^.w, infoSurface^.h, infoSurface^.format^.BitsPerPixel, (infoSurface^.w * infoSurface^.format^.BytesPerPixel),longword( infoSurface^.format));

  if (saveSurface = NiL) then begin
      logln('Couldnt create SDL_Surface from renderer pixel data. ');
      logln(SDL_GetError);
      exit;
  end;

   Textures[windownumber]:=SDL_CreateTextureFromSurface(renderer,saveSurface);

   //free data
   SDL_FreeSurface(saveSurface);

   saveSurface := NiL;
   infosurface:=Nil;
   ScreenData:=Nil;

{$ifdef cpu64}
   SDL_RenderSetViewport( Renderer, Rect );
{$endif}

   //clearviewport(_fgcoor);

   //"Clipping" is a joke- we always clip.
   //The trick is: do we zoom out? In Most cases- NO. We zoom IN.

end;

//I removed FPC circle/ellipse code because it was too confusing. Furthermore, bresnans algo seems to work better.
//code uses bresnans algoritm (often in BGI) as seen here: https://www.geeksforgeeks.org/bresenhams-circle-drawing-algorithm/

// circle-generation  using Bresenham's algorithm
procedure circleBres(xc, yc, r:word);

var
  x,y,d:integer;

begin
    x := 0;
    y := r;
    d := 3 - (2 * r);
//again: new routine needed: putWNewColor (or set, then put)
//putPixel presumes fg color

    	{$ifdef cpu32}
    		
	        putpixelXY(MainSurface,(xc+x), (yc+y));
            putpixelXY(MainSurface,(xc-x), (yc+y));
            putpixelXY(MainSurface,(xc+x), (yc-y));
            putpixelXY(MainSurface,(xc-x), (yc-y));
            putpixelXY(MainSurface,(xc+y), (yc+x));
            putpixelXY(MainSurface,(xc-y), (yc+x));
            putpixelXY(MainSurface,(xc+y), (yc-x));
            putpixelXY(MainSurface,(xc-y), (yc-x));
    
		{$endif}
		{$ifdef cpu64}
    				
    	    putpixelXY(Renderer,(xc+x), (yc+y));
            putpixelXY(Renderer,(xc-x), (yc+y));
            putpixelXY(Renderer,(xc+x), (yc-y));
            putpixelXY(Renderer,(xc-x), (yc-y));
            putpixelXY(Renderer,(xc+y), (yc+x));
            putpixelXY(Renderer,(xc-y), (yc+x));
            putpixelXY(Renderer,(xc+y), (yc-x));
            putpixelXY(Renderer,(xc-y), (yc-x));
    
		{$endif}

    
    while (y >= x) do begin
        // for each pixel we will
        // draw all eight pixels

           inc(x);

        // check for decision parameter
        // and correspondingly
        // update d, x, y
        if (d > 0)
        then begin
            dec(y);
            d := d + 4 * (x - y) + 10;
        end  else begin
            d := d + 4 * x + 6;
            	{$ifdef cpu32}
    		
	        putpixelXY(MainSurface,(xc+x), (yc+y));
            putpixelXY(MainSurface,(xc-x), (yc+y));
            putpixelXY(MainSurface,(xc+x), (yc-y));
            putpixelXY(MainSurface,(xc-x), (yc-y));
            putpixelXY(MainSurface,(xc+y), (yc+x));
            putpixelXY(MainSurface,(xc-y), (yc+x));
            putpixelXY(MainSurface,(xc+y), (yc-x));
            putpixelXY(MainSurface,(xc-y), (yc-x));
    
		{$endif}
		{$ifdef cpu64}
    				
    	    putpixelXY(Renderer,(xc+x), (yc+y));
            putpixelXY(Renderer,(xc-x), (yc+y));
            putpixelXY(Renderer,(xc+x), (yc-y));
            putpixelXY(Renderer,(xc-x), (yc-y));
            putpixelXY(Renderer,(xc+y), (yc+x));
            putpixelXY(Renderer,(xc-y), (yc+x));
            putpixelXY(Renderer,(xc+y), (yc-x));
            putpixelXY(Renderer,(xc-y), (yc-x));
    
		{$endif}

        {$ifdef cpu32}
            PageFlip;
        {$endif}
        {$ifdef cpu64}
            SDL_RenderPresent(Renderer);
         {$endif}
    end;
 end;

end;

//SDL v2 only..
procedure LoadImage(filename:PChar; Rect:PSDL_Rect);
//Rect: I need to know what size you want the imported image, and where you want it.

var
  tex:PSDL_Texture;

begin
    Tex:=IMG_LoadTexture(renderer,filename); //old code
    SDL_RenderCopy(renderer, tex, Nil, rect); //PutImage at x,y but only for size of WxH
end;

procedure LoadImageStretched(filename:PChar);

var
  tex:PSDL_Texture;

begin
    Tex:=IMG_LoadTexture(renderer,filename); //old code
    SDL_RenderCopy(renderer, tex, Nil, Nil); //scales image to output window size(size of undefined Rect)
end;



{

There is one hardspec'd hack going on:

        Get/PutPixel.
       (SDL_GetPixel/SDL_PutPixel - theres a unit conflict somewhere)


SDL routines require SDL_Get/PutPixel, however- as written- it is co-dependendent on a lot of code here.
This is unavaoidable.

There is one way-and only one way to fix this. Make the units co-dependant at build time.
The linker will merge the code for us- just make sure that use of SDL/SDL2 has this unit present--or using it will fail.

The alternative is to remm out the routines in the SDL units- and require the code here. That would break SDL.
Many internal routines- such as Line, Circle, Rect, etc. require Get- or-Put Pixel ops.

I would rather keep SDL sane. Its half-assed enough- as it is.




greyscale palettes are required for either loading or displaying in BW, or some subset thereof.
this isnt "exactly" duplicated code

hardspec it first, then allow user defined- or "pre-saved" files
that not to say you cant init, save, then load thru the main code instead of useing the hard spec.

the hardspec is here as a default setting, much like most 8x8 font sometimes are included
by default in graphics modes source code.

these are zero based colors. for 16 color modes- 88 or even 80 is not correct, 70 or 77 is.
the 256 hex color data was pulled from xterm (and CGA) specs.

the only guaranteed perfect palette code is (m)CGA -tweaked by me, and Greyscale 256.

(greyscale mCGA is a hackish guess based on rough math, given 14 colors, and also black and white)
256 should mimic xterm colors IF the endianess is correct and I dont need DWord hex math ops

the question becomes- do we really need to set the DWords by hand.. (such a PITA)
while I did this thinking YES, possibly no.



(SDL_Map and SDL_Get RGB/RGBA are here for this purpose but our version is faster-
and doesnt need to use bitdepth checks)


}

function GetOS: string;
//debugging purposes, we test in most cases.

begin

{$IFDEF WIN32}
 if WindowsVersion = wv95 then GetOS:='Windows 95'
 else if WindowsVersion = wvNT4 then GetOS:='Windows NT v.4'
 else if WindowsVersion = wv98 then GetOS:='Windows 98'
 else if WindowsVersion = wvMe then GetOS:='Windows ME'
 else if WindowsVersion = wv2000 then GetOS:='Windows 2000'
 else if WindowsVersion = wvXP then GetOS:='Windows XP'
 else if WindowsVersion = wvServer2003 then GetOS:='Windows Server 2003'
 else if WindowsVersion = wvVista then GetOS:='Windows Vista'
 else if WindowsVersion = wv7 then GetOS:='Windows 7'
//8,8.1, 10???
 else GetOS:='Unknown';
{$ENDIF}

{$IFDEF UNIX}
 {$IFDEF LINUX           }
    {$IFDEF ANDROID       }
             GetOS:='Android';   // ANDROID
    {$ELSE}
            GetOS:='Linux/Unix';
    {$ENDIF}
 {$ENDIF}
{$ENDIF}

{$IFDEF BSD}
    {$IFDEF DARWIN   }    // Macintosh see also MAC below
            GetOS:='Mac OSX';
    {$ELSE}
			GetOS:='BSD';
    {$ENDIF}
{$ENDIF}

{$IFDEF MAC }            // Classic Macintosh
        GetOS:='Early Mac OS';
{$ENDIF}

{$IFDEF GO32V2    }            // GO32V2
        GetOS:='Dos';
{$ENDIF}

end;

//standalone routines. get/putPixel takes care of the conversion for you.
//I like my implementation better.

//16bit from 24bit
function Word16_from_RGB(red,green, blue:byte):Word; //Word=GLShort
//565 16bit color

begin
  red:= red shr 3;
  green:= green shr 2;
  blue:=  blue shr 3;
  Word16_from_RGB:= (red shl 11) or (green shl 5) or blue;
end;

//15bit from 24bit
function Word15_from_RGB(red,green, blue:byte):Word; //Word=GLShort
//555[1] 16bit color

begin
  red:= red shr 3;
  green:= green shr 3;
  blue:=  blue shr 3;
  Word15_from_RGB:= (red shl 10) or (green shl 4) or blue;
end;



//Normally you lock and Unlock Surfaces for batch ops.
//RLE Surfaces (accelerated) MUST be locked prior to pixel ops- and are the only ones-where locking is required.
//REPEATED calls to lock/unlock slow things down

//Why does SDL even bother, if this locks threaded rendering -at all- w X11?

//MAKE SURE that SDL/SDL2 do not by themselves export Get/PutPixel.


//PtrUInt is a C hack- Typically used to output "any length" data- in my case we explicitly want DWords(32 bit byte alignment)
//I suppose we could...put this in the SDL v12 unit...the dev team does not by default include Get/Put.
//keep in mind the variables defined if you do that. FOR NOW, leave it alone. Its all going to make a pot of soup , together.

{$ifdef cpu32}
function SDL_GetPixel( SrcSurface : PSDL_Surface; x : integer; y : integer ) : PtrUInt; export;

var

i:integer;
index:LongWord;

    Location:^Longword; //location in 1D array
    temp15,tempPixel:Longword; //byte[0,1,2,3]
    pixel:longword;
    fetched:^longword;

begin
    if (SDL_MUSTLOCK(SrcSurface)) then
        SDL_LockSurface(SrcSurface);

    bpp:=SrcSurface^.format^.BytesPerPixel;

//location is a confusing misnomer- it should contain a value(DWord), based on screen co-ordinates in 1d
//-we interpret said value.
    Location:=(SrcSurface^.pixels+(y*SrcSurface^.pitch+x) * bpp );


//CGA: To fetch the data- we need to know - from which (mode and palette).
// This is More complex than it initially presents(like college algebra).

//if using BP/TP7- this is less of a concern.

    case bpp of
//<8bpp arent supported, emulate

        //Paletted. Determine Mode and use Mode to leverage which Palette to use.
        //cannot return a longword(yet)- as we dont know from which palette.

       8:begin //max assigned 256 color palette
        //now take the longword output

          //4bpp
          if MaxColors=4 then begin //CGA

            //check mode to find the correct palette

      	     i:=0;
             while (i<3) do begin

		        if (Tpalette4a[i].dwords = location^) then begin //did we find a match?

                    Result:=longword(Tpalette4a[i].Dwords);
                    exit;
                end;
				inc(i);  //no
             end;

      	     i:=0;
             while (i<3) do begin
		        if (Tpalette4b[i].dwords = location^) then begin //did we find a match?
 			        Result:=longword(Tpalette4b[i].Dwords);
                    exit;
               end;
				inc(i);  //no
             end;

      	     i:=0;
	         while (i<3) do begin
		        if (Tpalette4c[i].dwords = location^) then begin //did we find a match?
 			        Result:=longword(Tpalette4c[i].Dwords);
                    exit;
                end;
				inc(i);  //no
             end;

     i:=0;
	         while (i<1 do begin
		        if (Tpalette4d[i].dwords = location^) then begin //did we find a match?
 			        Result:=longword(Tpalette4d[i].Dwords);
                    exit;
                end;
				inc(i);  //no
             end;


             if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);
          end;

          if MaxColors=16 then begin  //EGA
             //compare byte index array (modded, not fixed) to pixel data- bail if doesnt match.

      	     i:=0;
             while (i<15) do begin
		        if (Tpalette64[i].dwords = location^) then begin //did we find a match?
 			       Result:=longword(Tpalette64[i].Dwords);
					exit;
                end;
				inc(i);  //no
             end;

             if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);
          end;

          //ok safely in 256 colors mode
          if MaxColors=255 then begin

      	     i:=0;
             while (i<255) do begin
		        if (Tpalette256[i].dwords = location^) then begin //did we find a match?
 			       Result:=longword(Tpalette256[i].Dwords);
					exit;
				end;
				inc(i);  //no
             end;
             if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);

          end;
       end;

        //special case-- shift the results on 15/16bpp to a longword.

        15: begin
            //extremely undocumented info
            //either we bump up to 32 from 16, then back down to 15..or we hack our way thru things.

            //red and green need to shift.

            temp15:=LongWord(location^); //pull 32bit data (hackish, should pull 16)

                //courtesy microsoft
                //mask it off

            //(x86, talos is little endian, powerpc is big endian)

            //FUCKING HELL! SDL was supposed to take care of this but the headers are missing in SDLv2, only present in SDLv1.


             {$IFDEF ENDIAN_LITTLE}
                Fetched[0]:=(temp15 and $7c00) shr 10;
                Fetched[1]:=(temp15 and $3E0) shr 5;
                Fetched[2]:=(temp15 and $1f);

                if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);

                //repack it (m$ft!! touche! a bug...10 and 4..)
                Result:=(Word(Fetched[0] shl 10 or Fetched[1] shl 4 or Fetched[2]));

			 {$ENDIF}
			 {$IFDEF ENDIAN_BIG}

                //mask it off
                Fetched[0]:=(temp15 and $1f);
                Fetched[1]:=(temp15 and $3E0) shr 5;
                Fetched[2]:=(temp15 and $7c00) shr 10;

                if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);

                Result:=(Word(Fetched[2] shl 10 or Fetched[1] shl 4 or Fetched[0]));
		    {$ENDIF}


        end;

        16:begin //if we hacked 15bpp, why not hack 16?
            temp15:=LongWord(Location); //hackish

             {$IFDEF ENDIAN_LITTLE}

                Fetched[0]:=(temp15 and $f800) shr 10;
                Fetched[1]:=(temp15 and $7E0) shr 5;
                Fetched[2]:=(temp15 and $1f);
				//AA bit?

                if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);

                Result:=Word((Fetched[0] shl 11) or (Fetched[1] shl 5) or Fetched[2]);

            {$ENDIF}
			 {$IFDEF ENDIAN_BIG}

                //mask it off
                Fetched[0]:=(temp15 and $1f);
                Fetched[1]:=(temp15 and $3E0) shr 5;
                Fetched[2]:=(temp15 and $7c00) shr 10;

                if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);

                Result:=Word( (Fetched[2] shl 11) or (Fetched[1] shl 5) or Fetched[0]);

             {$ENDIF}

        end;
        24: begin //tuple to longword- leave some bits as FF

            {$IFDEF ENDIAN_BIG}
                Result:= ((Longword (location[2] shl 24 or location[1] shl 16 or location[0] shl 8) shr 8))
            {$ENDIF}
            {$IFDEF ENDIAN_LITTLE}
                Result:=(Longword((location[0] shl 24 or location[1] shl 16 or location[2] shl 8) shr 8));
            {$ENDIF}
        end;
        32: begin
            if (SDL_MUSTLOCK(SrcSurface)) then
                    SDL_UnLockSurface(SrcSurface);

            Result:= Longword(Location);
        end else
            LogLn('Invalid bit Depth to get Pixel from.');
    end; //case
end;
{$endif}


//export SDL internal routine(missing!!) depending on the version used(expected parameters)
{$ifdef cpu32}
procedure SDL_PutPixel( DstSurface : PSDL_Surface; x : integer; y : integer); export;

   var
    Location1D:DWord;

begin
//dont worry about returning anything, go set the surface pixel at location to value provided
    pixels:=DstSurface^.pixels;
    Location1d:=DstSurface^.pixels+(y*DstSurface^.pitch+x) * bpp;
    pixels[Location1D ]:=_fgcolor;
    //wait for flip, only call when doing circles, rects, fills, etc.
end;

{$endif}

{$ifdef cpu64}
procedure SDL_PutPixel( Renderer : PSDL_Renderer; x : integer; y : integer); export;

begin
   SDL_RenderDrawPoint(renderer,x,y);
    //wait for renderPresent call-- only call whe doing circles, rects, fills, etc.

end;

{$endif}


{
BLITS:
covering 3 scenarios:
          to(MainSurface), to another surface, put a rect (from a surface) onto a rect (of another surface).

}

var
   BlitTransParencyColor:PSDL_Color;
   

{
//Im intentionally skipping over SDL(v2) implementation, to implement it in SDLv1.
procedure Blit(NewPixels:PSDL_Surface);
var
     i:integer;
     oldPixels: array of PSDL_Color;

ifdef cpu32
    oldPixels:MainSurface^.pixels;
endif

ifdef cpu64
    NewTexture:SDL_Texture;
    NewSurface:PSDL_Surface; // from the renderer
endif

begin
ifdef cpu64

   oldPixels:=NewSurface^.pixels; //pixels thereof
endif

         //if  "data to put" is not "size of the Surface/Renderer(screen)" then....
//missing a rct/rectSize struct here...
        if  (NewPixels^.x <> 0) or (NewPixels^.y <>0) or (NewPixels^.w <> MaxX) or (NewPixels^.h <> MaxY) then  begin
            LogLn('Blit: Trying to blit data that doesnt match screen dimensions. Use a different routine.');
            exit;
        end;

        i:=0;
        repeat

// if sdl v2 -then ...

//CreateSurfaceFromRenderer() -is missing.



        //we know the BlitTransparencyColor is Magenta in SDLv12.  Not knowning the color in SDL2 is not a problem. SET IT ELSEWHERE.

            if (NewPixels.pixels^+i = SDL_MapRGB(NewPixels^.format, BlitTransParencyColor^.r, BlitTransParencyColor^.g, BlitTransParencyColor^.b))   then begin     // Search for X color...
                tempColor:=oldPixels[i]; //get the "old" surface color (in the 1D array of them)
                pixeldata[i]:=tempColor; //THAT COLOR goes into the "new data to write", at THIS location.
           inc(i);
        until(w * Pixels^.h);

ifdef cpu32
    Move( NewPixels, MainSurface, sizeof(NewPixels));  //flap the altered data into "target surface".
    SDL_Flip( MainSurface);
endif

//a no brainer here,

// you can use SDLv2 internal routine...some idiot put it in SDL v12 JEDI headers. This explains "why the RGBA useage".
ifdef cpu64
    NewTexture:=SDL_CreateTextureFromSurface(renderer, NewPixels);
    SDL_FreeSurface(NewSurface);
    SDL_RenderCopy(renderer,NewTexture,Nil,Nil);
    SDL_FreeTexture(NewTexture);
    SDL_RenderPresent(renderer);
endif
end;
}

{$ifdef cpu32}
//these two are Surface->Surface only
procedure BlitSurfaceToSurface(SrcSurfaceData,DstSurface:SDL_Surface);
var
     i:integer;
    pixels:SrcSurfaceData^.pixels;
    oldPixels:DstSurface^.pixels;

begin
        i:=0;
        repeat

        //we know the BlitTransparencyColor is Magenta in SDLv12.  Not knowning the color in SDL2 is not a problem. SET IT ELSEWHERE.

            if (pixels[i] = SDL_MapRGB(NewPixels^.format, BlitTransParencyColor^.r, BlitTransParencyColor^.g, BlitTransParencyColor^.b))  then begin      // Search for X color...
                tempColor:=oldPixels[i]; //get the "old" surface color (in the 1D array of them)
                pixels[i]:=tempColor; //THAT COLOR goes into the "new data to write", at THIS location.
           inc(i);
        until(w * Pixels^.h);
    SDL_BlitSurface( SrcSurfaceData, NIL, DstSurface, NIL );
end;

procedure BlitSurfaceWRects(SrcSurfaceData,DstSurface:SDL_Surface; SrcRect,DstRect:SDL_Rect);
var
     i:integer;
    pixels:SrcSurfaceData^.pixels;
    oldPixels:DstSurface^.pixels;

begin
        i:=0;
        repeat

        //we know the BlitTransparencyColor is Magenta in SDLv12.  Not knowning the color in SDL2 is not a problem. SET IT ELSEWHERE.
//forget map/unmap RGB functions.

            if (pixels[i] = SDL_MapRGB(NewPixels^.format, BlitTransParencyColor^.r, BlitTransParencyColor^.g, BlitTransParencyColor^.b))   then begin     // Search for X color...
                tempColor:=oldPixels[i]; //get the "old" surface color (in the 1D array of them)
                pixels[i]:=tempColor; //THAT COLOR goes into the "new data to write", at THIS location.
           inc(i);
        until(w * Pixels^.h);

    SDL_BlitSurface( SrcSurfaceData, SrcRect, DstSurface, DstRect );
end;
{$endif}

//this is 64, not 16, first 16 are typically used. Incomplete procedure.
procedure initPaletteGrey16;

var
   i,num:integer;


begin

valuelist64[0]:=$00;
valuelist64[1]:=$00;
valuelist64[2]:=$00;
valuelist64[3]:=$11;
valuelist64[4]:=$11;
valuelist64[5]:=$11;
valuelist64[6]:=$22;
valuelist64[7]:=$22;
valuelist64[8]:=$22;
valuelist64[9]:=$33;
valuelist64[10]:=$33;
valuelist64[11]:=$33;
valuelist64[12]:=$44;
valuelist64[13]:=$44;
valuelist64[14]:=$44;
valuelist64[15]:=$55;
valuelist64[16]:=$55;
valuelist64[17]:=$55;
valuelist64[18]:=$66;
valuelist64[19]:=$66;
valuelist64[20]:=$66;
valuelist64[21]:=$77;
valuelist64[22]:=$77;
valuelist64[23]:=$77;
valuelist64[24]:=$88;
valuelist64[25]:=$88;
valuelist64[26]:=$88;
valuelist64[27]:=$99;
valuelist64[28]:=$99;
valuelist64[29]:=$99;
valuelist64[30]:=$aa;
valuelist64[31]:=$aa;
valuelist64[32]:=$aa;
valuelist64[33]:=$bb;
valuelist64[34]:=$bb;
valuelist64[35]:=$bb;
valuelist64[36]:=$cc;
valuelist64[37]:=$cc;
valuelist64[38]:=$cc;
valuelist64[39]:=$dd;
valuelist64[40]:=$dd;
valuelist64[41]:=$dd;
valuelist64[42]:=$ee;
valuelist64[43]:=$ee;
valuelist64[44]:=$ee;
valuelist64[45]:=$ff;
valuelist64[46]:=$ff;
valuelist64[47]:=$ff;

   i:=0;
   num:=0;
   repeat
      Tpalette64[num].colors^.r:=valuelist64[i];
      Tpalette64[num].colors^.g:=valuelist64[i+1];
      Tpalette64[num].colors^.b:=valuelist64[i+2];
      Tpalette64[num].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i,3);
      inc(num);
  until num=47;


Tpalette64GRey[0].DWords:=$000000ff;
Tpalette64GRey[1].DWords:=$111111ff;
Tpalette64GRey[2].DWords:=$222222ff;
Tpalette64GRey[3].DWords:=$333333ff;
Tpalette64GRey[4].DWords:=$444444ff;
Tpalette64GRey[5].DWords:=$555555ff;
Tpalette64GRey[6].DWords:=$666666ff;
Tpalette64GRey[7].DWords:=$777777ff;
Tpalette64GRey[8].DWords:=$888888ff;
Tpalette64GRey[9].DWords:=$999999ff;
Tpalette64GRey[10].DWords:=$aaaaaaff;
Tpalette64GRey[11].DWords:=$bbbbbbff;
Tpalette64GRey[12].DWords:=$ccccccff;
Tpalette64GRey[13].DWords:=$ddddddff;
Tpalette64GRey[14].DWords:=$eeeeeeff;
Tpalette64GRey[15].DWords:=$ffffffff;


end;


procedure initPaletteGrey256;

//easy peasy to setup.

var
    i,num:integer;


begin

//(we dont setup valuelist by hand this time)

   i:=0;

  repeat
      Tpalette256Grey[i].colors^.r:=ord(i);
      Tpalette256Grey[i].colors^.g:=ord(i);
      Tpalette256Grey[i].colors^.b:=ord(i);
      Tpalette256Grey[i].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i); //notice the difference <-HERE ..where RGB are the same values
  until i=255;

Tpalette256Grey[0].DWords:=$000000ff;
//the only gauranteed correct values are 0 and 255 right now.
//this will have to be manually set according to the above math.

Tpalette256Grey[1].DWords:=$010101ff;
Tpalette256Grey[2].DWords:=$020202ff;
Tpalette256Grey[3].DWords:=$030303ff;
Tpalette256Grey[4].DWords:=$040404ff;
Tpalette256Grey[5].DWords:=$050505ff;
Tpalette256Grey[6].DWords:=$060606ff;
Tpalette256Grey[7].DWords:=$070707ff;
Tpalette256Grey[8].DWords:=$080808ff;
Tpalette256Grey[9].DWords:=$090909ff;
Tpalette256Grey[10].DWords:=$0a0a0aff;
Tpalette256Grey[11].DWords:=$0b0b0bff;
Tpalette256Grey[12].DWords:=$0c0c0cff;
Tpalette256Grey[13].DWords:=$0d0d0dff;
Tpalette256Grey[14].DWords:=$0e0e0eff;
Tpalette256Grey[15].DWords:=$0f0f0fff;

Tpalette256Grey[16].DWords:=$101010ff;
Tpalette256Grey[17].DWords:=$111111ff;
Tpalette256Grey[18].DWords:=$121212ff;
Tpalette256Grey[19].DWords:=$131313ff;
Tpalette256Grey[20].DWords:=$141414ff;
Tpalette256Grey[21].DWords:=$151515ff;
Tpalette256Grey[22].DWords:=$161616ff;
Tpalette256Grey[23].DWords:=$171717ff;
Tpalette256Grey[24].DWords:=$181818ff;
Tpalette256Grey[25].DWords:=$191919ff;
Tpalette256Grey[26].DWords:=$1a1a1aff;
Tpalette256Grey[27].DWords:=$1b1b1bff;
Tpalette256Grey[28].DWords:=$1c1c1cff;
Tpalette256Grey[29].DWords:=$1d1d1dff;
Tpalette256Grey[30].DWords:=$1e1e1eff;
Tpalette256Grey[31].DWords:=$1f1f1fff;

Tpalette256Grey[32].DWords:=$202020ff;
Tpalette256Grey[33].DWords:=$212121ff;
Tpalette256Grey[34].DWords:=$222222ff;
Tpalette256Grey[35].DWords:=$232323ff;
Tpalette256Grey[36].DWords:=$242424ff;
Tpalette256Grey[37].DWords:=$252525ff;
Tpalette256Grey[38].DWords:=$262626ff;
Tpalette256Grey[39].DWords:=$272727ff;
Tpalette256Grey[40].DWords:=$282828ff;
Tpalette256Grey[41].DWords:=$292929ff;
Tpalette256Grey[42].DWords:=$2a2a2aff;
Tpalette256Grey[43].DWords:=$2b2b2bff;
Tpalette256Grey[44].DWords:=$2c2c2cff;
Tpalette256Grey[45].DWords:=$2d2d2dff;
Tpalette256Grey[46].DWords:=$2e2e2eff;
Tpalette256Grey[47].DWords:=$2f2f2fff;

Tpalette256Grey[48].DWords:=$303030ff;
Tpalette256Grey[49].DWords:=$313131ff;
Tpalette256Grey[50].DWords:=$323232ff;
Tpalette256Grey[51].DWords:=$333333ff;
Tpalette256Grey[52].DWords:=$343434ff;
Tpalette256Grey[53].DWords:=$353535ff;
Tpalette256Grey[54].DWords:=$363636ff;
Tpalette256Grey[55].DWords:=$373737ff;
Tpalette256Grey[56].DWords:=$383838ff;
Tpalette256Grey[57].DWords:=$393939ff;
Tpalette256Grey[58].DWords:=$3a3a3aff;
Tpalette256Grey[59].DWords:=$3b3b3bff;
Tpalette256Grey[60].DWords:=$3c3c3cff;
Tpalette256Grey[61].DWords:=$3d3d3dff;
Tpalette256Grey[62].DWords:=$3e3e3eff;
Tpalette256Grey[63].DWords:=$3f3f3fff;

Tpalette256Grey[64].DWords:=$404040ff;
Tpalette256Grey[65].DWords:=$414141ff;
Tpalette256Grey[66].DWords:=$424242ff;
Tpalette256Grey[67].DWords:=$434343ff;
Tpalette256Grey[68].DWords:=$444444ff;
Tpalette256Grey[69].DWords:=$454545ff;
Tpalette256Grey[70].DWords:=$464646ff;
Tpalette256Grey[71].DWords:=$474747ff;
Tpalette256Grey[72].DWords:=$484848ff;
Tpalette256Grey[73].DWords:=$494949ff;
Tpalette256Grey[74].DWords:=$4a4a4aff;
Tpalette256Grey[75].DWords:=$4b4b4bff;
Tpalette256Grey[76].DWords:=$4c4c4cff;
Tpalette256Grey[77].DWords:=$4d4d4dff;
Tpalette256Grey[78].DWords:=$4e4e4eff;
Tpalette256Grey[79].DWords:=$4f4f4fff;

Tpalette256Grey[80].DWords:=$505050ff;
Tpalette256Grey[81].DWords:=$515151ff;
Tpalette256Grey[82].DWords:=$525252ff;
Tpalette256Grey[83].DWords:=$535353ff;
Tpalette256Grey[84].DWords:=$545454ff;
Tpalette256Grey[85].DWords:=$555555ff;
Tpalette256Grey[86].DWords:=$565656ff;
Tpalette256Grey[87].DWords:=$575757ff;
Tpalette256Grey[88].DWords:=$585858ff;
Tpalette256Grey[89].DWords:=$595959ff;
Tpalette256Grey[90].DWords:=$5a5a5aff;
Tpalette256Grey[91].DWords:=$5b5b5bff;
Tpalette256Grey[92].DWords:=$5c5c5cff;
Tpalette256Grey[93].DWords:=$5d5d5dff;
Tpalette256Grey[94].DWords:=$5e5e5eff;
Tpalette256Grey[95].DWords:=$5f5f5fff;

Tpalette256Grey[96].DWords:=$606060ff;
Tpalette256Grey[97].DWords:=$616161ff;
Tpalette256Grey[98].DWords:=$626262ff;
Tpalette256Grey[99].DWords:=$636363ff;
Tpalette256Grey[100].DWords:=$646464ff;
Tpalette256Grey[101].DWords:=$656565ff;
Tpalette256Grey[102].DWords:=$666666ff;
Tpalette256Grey[103].DWords:=$676767ff;
Tpalette256Grey[104].DWords:=$686868ff;
Tpalette256Grey[105].DWords:=$696969ff;
Tpalette256Grey[106].DWords:=$6a6a6aff;
Tpalette256Grey[107].DWords:=$6b6b6bff;
Tpalette256Grey[108].DWords:=$6c6c6cff;
Tpalette256Grey[109].DWords:=$6d6d6dff;
Tpalette256Grey[110].DWords:=$6e6e6eff;
Tpalette256Grey[111].DWords:=$6f6f6fff;

Tpalette256Grey[112].DWords:=$707070ff;
Tpalette256Grey[113].DWords:=$717171ff;
Tpalette256Grey[114].DWords:=$727272ff;
Tpalette256Grey[115].DWords:=$737373ff;
Tpalette256Grey[116].DWords:=$747474ff;
Tpalette256Grey[117].DWords:=$757575ff;
Tpalette256Grey[118].DWords:=$767676ff;
Tpalette256Grey[119].DWords:=$777777ff;
Tpalette256Grey[120].DWords:=$787878ff;
Tpalette256Grey[121].DWords:=$797979ff;
Tpalette256Grey[122].DWords:=$7a7a7aff;
Tpalette256Grey[123].DWords:=$7b7b7bff;
Tpalette256Grey[124].DWords:=$7c7c7cff;
Tpalette256Grey[125].DWords:=$7d7d7dff;
Tpalette256Grey[126].DWords:=$7e7e7eff;
Tpalette256Grey[127].DWords:=$7f7f7fff;

Tpalette256Grey[128].DWords:=$808080ff;
Tpalette256Grey[129].DWords:=$818181ff;
Tpalette256Grey[130].DWords:=$828282ff;
Tpalette256Grey[131].DWords:=$838383ff;
Tpalette256Grey[132].DWords:=$848484ff;
Tpalette256Grey[133].DWords:=$858585ff;
Tpalette256Grey[134].DWords:=$868686ff;
Tpalette256Grey[135].DWords:=$878787ff;
Tpalette256Grey[136].DWords:=$888888ff;
Tpalette256Grey[137].DWords:=$898989ff;
Tpalette256Grey[138].DWords:=$8a8a8aff;
Tpalette256Grey[139].DWords:=$8b8b8bff;
Tpalette256Grey[140].DWords:=$8c8c8cff;
Tpalette256Grey[141].DWords:=$8d8d8dff;
Tpalette256Grey[142].DWords:=$8e8e8eff;
Tpalette256Grey[143].DWords:=$8f8f8fff;

Tpalette256Grey[144].DWords:=$909090ff;
Tpalette256Grey[145].DWords:=$919191ff;
Tpalette256Grey[146].DWords:=$929292ff;
Tpalette256Grey[147].DWords:=$939393ff;
Tpalette256Grey[148].DWords:=$949494ff;
Tpalette256Grey[149].DWords:=$959595ff;
Tpalette256Grey[150].DWords:=$969696ff;
Tpalette256Grey[151].DWords:=$979797ff;
Tpalette256Grey[152].DWords:=$989898ff;
Tpalette256Grey[153].DWords:=$999999ff;
Tpalette256Grey[154].DWords:=$9a9a9aff;
Tpalette256Grey[155].DWords:=$9b9b9bff;
Tpalette256Grey[156].DWords:=$9c9c9cff;
Tpalette256Grey[157].DWords:=$9d9d9dff;
Tpalette256Grey[158].DWords:=$9e9e9eff;
TPalette256GRey[159].DWords:=$9f9f9fff;

Tpalette256Grey[160].DWords:=$a0a0a0ff;
Tpalette256Grey[161].DWords:=$a1a1a1ff;
Tpalette256Grey[162].DWords:=$a2a2a2ff;
Tpalette256Grey[163].DWords:=$a3a3a3ff;
Tpalette256Grey[164].DWords:=$a4a4a4ff;
Tpalette256Grey[165].DWords:=$a5a5a5ff;
Tpalette256Grey[166].DWords:=$a6a6a6ff;
Tpalette256Grey[167].DWords:=$a7a7a7ff;
Tpalette256Grey[168].DWords:=$a8a8a8ff;
Tpalette256Grey[169].DWords:=$a9a9a9ff;
Tpalette256Grey[170].DWords:=$aaaaaaff;
Tpalette256Grey[171].DWords:=$abababff;
Tpalette256Grey[172].DWords:=$acacacff;
Tpalette256Grey[173].DWords:=$adadadff;
Tpalette256Grey[174].DWords:=$aeaeaeff;
Tpalette256Grey[175].DWords:=$afafafff;

Tpalette256Grey[176].DWords:=$b0b0b0ff;
Tpalette256Grey[177].DWords:=$b1b1b1ff;
Tpalette256Grey[178].DWords:=$b2b2b2ff;
Tpalette256Grey[179].DWords:=$b3b3b3ff;
Tpalette256Grey[180].DWords:=$b4b4b4ff;
Tpalette256Grey[181].DWords:=$b5b5b5ff;
Tpalette256Grey[182].DWords:=$b6b6b6ff;
Tpalette256Grey[183].DWords:=$b7b7b7ff;
Tpalette256Grey[184].DWords:=$b8b8b8ff;
Tpalette256Grey[185].DWords:=$b9b9b9ff;
Tpalette256Grey[186].DWords:=$bababaff;
Tpalette256Grey[187].DWords:=$bbbbbbff;
Tpalette256Grey[188].DWords:=$bcbcbcff;
Tpalette256Grey[189].DWords:=$bdbdbdff;
Tpalette256Grey[190].DWords:=$bebebeff;
Tpalette256Grey[191].DWords:=$bfbfbfff;

Tpalette256Grey[192].DWords:=$c0c0c0ff;
Tpalette256Grey[193].DWords:=$c1c1c1ff;
Tpalette256Grey[194].DWords:=$c2c2c2ff;
Tpalette256Grey[195].DWords:=$c3c3c3ff;
Tpalette256Grey[196].DWords:=$c4c4c4ff;
Tpalette256Grey[197].DWords:=$c5c5c5ff;
Tpalette256Grey[198].DWords:=$c6c6c6ff;
Tpalette256Grey[199].DWords:=$c7c7c7ff;
Tpalette256Grey[200].DWords:=$c8c8c8ff;
Tpalette256Grey[201].DWords:=$c9c9c9ff;
Tpalette256Grey[202].DWords:=$cacacaff;
Tpalette256Grey[203].DWords:=$cbcbcbff;
Tpalette256Grey[204].DWords:=$ccccccff;
Tpalette256Grey[205].DWords:=$cdcdcdff;
Tpalette256Grey[206].DWords:=$cececeff;
Tpalette256Grey[207].DWords:=$cfcfcfff;

Tpalette256Grey[208].DWords:=$d0d0d0ff;
Tpalette256Grey[209].DWords:=$d1d1d1ff;
Tpalette256Grey[210].DWords:=$d2d2d2ff;
Tpalette256Grey[211].DWords:=$d3d3d3ff;
Tpalette256Grey[212].DWords:=$d4d4d4ff;
Tpalette256Grey[213].DWords:=$d5d5d5ff;
Tpalette256Grey[214].DWords:=$d6d6d6ff;
Tpalette256Grey[215].DWords:=$d7d7d7ff;
Tpalette256Grey[216].DWords:=$d8d8d8ff;
Tpalette256Grey[217].DWords:=$d9d9d9ff;
Tpalette256Grey[218].DWords:=$dadadaff;
Tpalette256Grey[219].DWords:=$dbdbdbff;
Tpalette256Grey[220].DWords:=$dcdcdcff;
Tpalette256Grey[221].DWords:=$ddddddff;
Tpalette256Grey[222].DWords:=$dededeff;
Tpalette256Grey[223].DWords:=$dfdfdfff;

Tpalette256Grey[224].DWords:=$e0e0e0ff;
Tpalette256Grey[225].DWords:=$e1e1e1ff;
Tpalette256Grey[226].DWords:=$e2e2e2ff;
Tpalette256Grey[227].DWords:=$e3e3e3ff;
Tpalette256Grey[228].DWords:=$e4e4e4ff;
Tpalette256Grey[229].DWords:=$e5e5e5ff;
Tpalette256Grey[230].DWords:=$e6e6e6ff;
Tpalette256Grey[231].DWords:=$e7e7e7ff;
Tpalette256Grey[232].DWords:=$e8e8e8ff;
Tpalette256Grey[233].DWords:=$e9e9e9ff;
Tpalette256Grey[234].DWords:=$eaeaeaff;
Tpalette256Grey[235].DWords:=$ebebebff;
Tpalette256Grey[236].DWords:=$ecececff;
Tpalette256Grey[237].DWords:=$edededff;
Tpalette256Grey[238].DWords:=$eeeeeeff;
Tpalette256Grey[239].DWords:=$efefefff;

Tpalette256Grey[240].DWords:=$f0f0f0ff;
Tpalette256Grey[241].DWords:=$f1f1f1ff;
Tpalette256Grey[242].DWords:=$f2f2f2ff;
Tpalette256Grey[243].DWords:=$f3f3f3ff;
Tpalette256Grey[244].DWords:=$f4f4f4ff;
Tpalette256Grey[245].DWords:=$f5f5f5ff;
Tpalette256Grey[246].DWords:=$f6f6f6ff;
Tpalette256Grey[247].DWords:=$f7f7f7ff;
Tpalette256Grey[248].DWords:=$f8f8f8ff;
Tpalette256Grey[249].DWords:=$f9f9f9ff;
Tpalette256Grey[250].DWords:=$fafafaff;
Tpalette256Grey[251].DWords:=$fbfbfbff;
Tpalette256Grey[252].DWords:=$fcfcfcff;
Tpalette256Grey[253].DWords:=$fdfdfdff;
Tpalette256Grey[254].DWords:=$fefefeff;
Tpalette256Grey[255].DWords:=$ffffffff;

end;

//fixme: still need to to BW for EGA/CGA/CGAHi

//EGA
procedure initpalette64;
//yes: this is correct. 64 "availabe colors".
//Palette only accesses 16 of those at any time.
//default setting is to original (text) 16 colors
var
   num,i:integer;


begin
//Color Sequence:
//K HiR HiG HiB HiC HiM HiY HiGR LoGr LoR LoG LoB LoC LoM LoY W


valuelist64[0]:=$00;
valuelist64[1]:=$00;
valuelist64[2]:=$00;

valuelist64[3]:=$ff;
valuelist64[4]:=$00;
valuelist64[5]:=$00;

valuelist64[6]:=$00;
valuelist64[7]:=$ff;
valuelist64[8]:=$00;

valuelist64[9]:=$ff;
valuelist64[10]:=$ff;
valuelist64[11]:=$00;

valuelist64[12]:=$00;
valuelist64[13]:=$00;
valuelist64[14]:=$ff;

valuelist64[15]:=$ff;
valuelist64[16]:=$00;
valuelist64[17]:=$ff;

valuelist64[18]:=$ff;
valuelist64[19]:=$ff;
valuelist64[20]:=$00;


valuelist64[21]:=$c0;
valuelist64[22]:=$c0;
valuelist64[23]:=$c0;

valuelist64[24]:=$7f;
valuelist64[25]:=$7f;
valuelist64[26]:=$7f;


valuelist64[27]:=$7f;
valuelist64[28]:=$00;
valuelist64[29]:=$00;

valuelist64[30]:=$00;
valuelist64[31]:=$7f;
valuelist64[32]:=$00;

valuelist64[33]:=$7f;
valuelist64[34]:=$7f;
valuelist64[35]:=$00;

valuelist64[36]:=$00;
valuelist64[37]:=$00;
valuelist64[38]:=$7f;

valuelist64[39]:=$7f;
valuelist64[40]:=$00;
valuelist64[41]:=$7f;

valuelist64[42]:=$00;
valuelist64[43]:=$7f;
valuelist64[44]:=$7f;

valuelist64[45]:=$ff;
valuelist64[46]:=$ff;
valuelist64[47]:=$ff;


   i:=0;
   num:=0;
   repeat
      Tpalette64[num].colors^.r:=valuelist64[i];
      Tpalette64[num].colors^.g:=valuelist64[i+1];
      Tpalette64[num].colors^.b:=valuelist64[i+2];
      Tpalette64[num].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i,3);
      inc(num);
  until num=63;



Tpalette64[0].DWords:=$000000ff;
Tpalette64[1].DWords:=$0000AAff;
Tpalette64[2].DWords:=$00aa00ff;
Tpalette64[3].DWords:=$00aaaaff;
Tpalette64[4].DWords:=$AA0000ff;
Tpalette64[5].DWords:=$AA00AAff;
Tpalette64[6].DWords:=$aaaa00ff;
Tpalette64[7].DWords:=$aaaaaaff;
Tpalette64[8].DWords:=$000055ff;
Tpalette64[9].DWords:=$0000ffff;
Tpalette64[10].DWords:=$00aa55ff;
Tpalette64[11].DWords:=$00aaffff;
Tpalette64[12].DWords:=$aa0055ff;
Tpalette64[13].DWords:=$aa00ffff;
Tpalette64[14].DWords:=$aaaa55ff;
Tpalette64[15].DWords:=$aaaaffff;

Tpalette64[16].DWords:=$005500ff;
Tpalette64[17].DWords:=$0055aaff;
Tpalette64[18].DWords:=$00ff00ff;
Tpalette64[19].DWords:=$00ffaaff;
Tpalette64[20].DWords:=$aa5500ff;
Tpalette64[21].DWords:=$aa55aaff;
Tpalette64[22].DWords:=$aaff00ff;
Tpalette64[23].DWords:=$aaffaaff;
Tpalette64[24].DWords:=$005555ff;
Tpalette64[25].DWords:=$0055ffff;
Tpalette64[26].DWords:=$00ff55ff;
Tpalette64[27].DWords:=$00ffffff;
Tpalette64[28].DWords:=$aa5555ff;
Tpalette64[29].DWords:=$aa55ffff;
Tpalette64[30].DWords:=$aaff55ff;
Tpalette64[31].DWords:=$aaffffff;
Tpalette64[32].DWords:=$550000ff;
Tpalette64[33].DWords:=$5500aaff;
Tpalette64[34].DWords:=$55aa00ff;
Tpalette64[35].DWords:=$55aaaaff;
Tpalette64[36].DWords:=$ff0000ff;
Tpalette64[37].DWords:=$ff00aaff;
Tpalette64[38].DWords:=$ffaa00ff;
Tpalette64[39].DWords:=$ffaaaaff;
Tpalette64[40].DWords:=$550055ff;
Tpalette64[41].DWords:=$5500ffff;
Tpalette64[42].DWords:=$55aa55ff;
Tpalette64[43].DWords:=$55aaffff;
Tpalette64[44].DWords:=$ff0055ff;
Tpalette64[45].DWords:=$ff00ffff;
Tpalette64[46].DWords:=$ffaa55ff;
Tpalette64[47].DWords:=$ffaaffff;
Tpalette64[48].DWords:=$555500ff;
Tpalette64[49].DWords:=$5555aaff;
Tpalette64[50].DWords:=$55ff00ff;
Tpalette64[51].DWords:=$55ffaaff;
Tpalette64[52].DWords:=$ff5500ff;
Tpalette64[53].DWords:=$ff55aaff;
Tpalette64[54].DWords:=$ffff00ff;
Tpalette64[55].DWords:=$ffffaaff;
Tpalette64[56].DWords:=$555555ff;
Tpalette64[57].DWords:=$5555ffff;
Tpalette64[58].DWords:=$55ff55ff;
Tpalette64[59].DWords:=$55ffffff;
Tpalette64[60].DWords:=$ff5555ff;
Tpalette64[61].DWords:=$ff55ffff;
Tpalette64[62].DWords:=$ffff55ff;
Tpalette64[63].DWords:=$ffffffff;


end;


procedure Save64Palette(filename:string);
//this only sets the DWord data, for the RGB seperate values we will have to look at this again..and try to load them automagically.
var
	palette64File : File of DWord; //typed record file
	i,num            : integer;

Begin
	iniTpalette64;
	Assign(palette64File, filename);
	ReWrite(palette64File); //ignore anything existing, also resets bad palette code
    i:=0;
    repeat
		Write(palette64File, Tpalette64[i].DWords); //dump everything out
        inc(i);
	until i=63;
	Close(palette64File);

End;

procedure Read64Palette(filename:string; ReadColor:boolean);
//BW not implemented yet
Var
	palette64File  : File of DWord;
	j,i,num            : integer;

    ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;
Begin
    //yes EGA is WEIRD. The palette data is here, but by default jack-fuck wired.
    //no, we dont care. (As long as colors match the contents of something in the "master palette".)
	Assign(palette64File, filename);
	ReSet(palette64File);
    Seek(palette64File, 0); //find first record

	i:=0;
    if ReadColor=true then begin
        repeat
			Read(palette64File, Tpalette64[i].DWords); //read the first 16 in, but read in the whole 64 in another palette.
			inc(i);
		until i=63;
		
    end;
	Close(palette64File);

    //set the "CGA16 Default sane colors" from the 64.
    //assume its the first 16 in the palette.
    
 
    {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 1);
        inc(i);
    until i=63;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(63);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
      until i=63;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,63);
        
   {$endif}
end;

//4 color CGA, not the 16 color EGA(text) you are used to.
// UGLY!!!
var
	TPalette4a,TPalette4b,TPalette4c: array[0..3] of TRec;
    TPalette4d: array [0..1] of TRec;

//here in case you change something, as with EGA. VGa can be reset- but isnt assigned funny. CGA just maps to the same colors.
//certain numbers can be restricted this way, mimicking the older hardware. "EGA minimum" is basically used anymore.

//This was a challenge- I wanted to see if I could pull it off. I succeeded.
//CGA is weird.Needs to implemented with 4x the necessary code.

procedure initPaletteCGAa;

var
	num,i:integer;

begin

	valuelist4[0]:=$00;
	valuelist4[1]:=$00;
	valuelist4[2]:=$00;
	
	valuelist4[3]:=$aa;
	valuelist4[4]:=$00;
	valuelist4[5]:=$aa;
	
	valuelist4[6]:=$55;
	valuelist4[7]:=$ff;
	valuelist4[8]:=$ff;
	
    valuelist4[9]:=$ff;
	valuelist4[10]:=$ff;
	valuelist4[11]:=$ff;
	
	  i:=0;
   num:=0;
   repeat
      Tpalette4a[num].colors^.r:=valuelist4[i];
      Tpalette4a[num].colors^.g:=valuelist4[i+1];
      Tpalette4a[num].colors^.b:=valuelist4[i+2];
      Tpalette4a[num].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i,4);
      inc(num);
  until num=11;


    TPalette4a[0].DWords:=$000000ff;
    TPalette4a[1].DWords:=$AA00AAff;
    TPalette4a[2].DWords:=$55ffffff;
    TPalette4a[3].DWords:=$ffffffff;
end;


procedure initPaletteCGAb;

var
	num,i:integer;

begin

	valuelist4[0]:=$00;
	valuelist4[1]:=$00;
	valuelist4[2]:=$00;

	valuelist4[3]:=$ff;
	valuelist4[4]:=$55;
	valuelist4[5]:=$55;

	valuelist4[6]:=$55;
	valuelist4[7]:=$ff;
	valuelist4[8]:=$55;

	valuelist4[9]:=$aa;
	valuelist4[10]:=$55;
    valuelist4[11]:=$00;

	  i:=0;
   num:=0;
   repeat
      Tpalette4b[num].colors^.r:=valuelist4[i];
      Tpalette4b[num].colors^.g:=valuelist4[i+1];
      Tpalette4b[num].colors^.b:=valuelist4[i+2];
      Tpalette4b[num].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i,4);
      inc(num);
  until num=11;

//nightmareish hell

TPalette4b[0].DWords:=$000000ff;
TPalette4b[1].DWords:=$ff5555ff;
TPalette4b[2].DWords:=$55ff55ff;
TPalette4b[3].DWords:=$AA5500ff;
end;

//these were made for CONTRAST on Orange/Green "WYSE?? Terminals" used with IBM XT 8088/8086s
//they were not intended to "look pretty".. oddly the palettes CAN be modified, most didnt do that.


procedure initPaletteCGAc;

var
	num,i:integer;

begin
	valuelist4[0]:=$00;
	valuelist4[1]:=$00;
	valuelist4[2]:=$00;
	
	valuelist4[3]:=$55;
	valuelist4[4]:=$ff;
	valuelist4[5]:=$ff;
	
	valuelist4[6]:=$ff;
	valuelist4[7]:=$55;
	valuelist4[8]:=$55;
	
	valuelist4[9]:=$ff;
	valuelist4[10]:=$ff;
	valuelist4[11]:=$ff;
	
	  i:=0;
   num:=0;
   repeat
      Tpalette4c[num].colors^.r:=valuelist4[i];
      Tpalette4c[num].colors^.g:=valuelist4[i+1];
      Tpalette4c[num].colors^.b:=valuelist4[i+2];
      Tpalette4c[num].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i,3);
      inc(num);
  until num=11;

//suposed to be greyscale..has been noted in color

TPalette4c[0].DWords:=$000000ff;
TPalette4c[1].DWords:=$55ffffff;
TPalette4c[2].DWords:=$ff5555ff;
TPalette4c[3].DWords:=$ffffffff;
end;


procedure initPaletteCGAd;

var
	num,i:integer;

begin

	valuelist4[0]:=$ff;
	valuelist4[1]:=$ff;
	valuelist4[2]:=$ff;

	valuelist4[3]:=$00;
	valuelist4[4]:=$00;
    valuelist4[5]:=$00;

      Tpalette4d[0].colors^.r:=valuelist4[0];
      Tpalette4d[0].colors^.g:=valuelist4[1];
      Tpalette4d[0].colors^.b:=valuelist4[2];
      Tpalette4d[0].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
 
      Tpalette4d[1].colors^.r:=valuelist4[3];
      Tpalette4d[1].colors^.g:=valuelist4[4];
      Tpalette4d[1].colors^.b:=valuelist4[5];
      Tpalette4d[1].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
 
//high res, Black ...and ....something
//in CGAHi, only fg can be changed.

    TPalette4d[0].DWords:=$000000ff;
    TPalette4d[1].DWords:=$ffffffff; 

end;

//CGA palettes need 4 routines(sorry)
procedure Save4PaletteA(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;

Begin

    //save us a lot of code and work
    initPaletteCGAa;
	Assign(palette4File, filename);
	ReWrite(palette4File);

  i:=0;
    repeat
 		Write(palette4File, TPalette4a[i].DWords);
        inc(i);
	until i=3;
	Close(palette4File);

End;
//CGA palettes need 4 routines(sorry)
procedure Save4PaletteB(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;

Begin

    //save us a lot of code and work
    initPaletteCGAb;
	Assign(palette4File, filename);
	ReWrite(palette4File);

  i:=0;
    repeat
 		Write(palette4File, TPalette4b[i].DWords);
        inc(i);
	until i=3;
	Close(palette4File);

End;
//CGA palettes need 4 routines(sorry)
procedure Save4PaletteC(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;

Begin

    //save us a lot of code and work
    initPaletteCGAc;
	Assign(palette4File, filename);
	ReWrite(palette4File);

  i:=0;
    repeat
 		Write(palette4File, TPalette4c[i].DWords);
        inc(i);
	until i=3;
	Close(palette4File);

End;

procedure Save4Paletted(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;

Begin

    //save us a lot of code and work
    initPaletteCGAd;
	Assign(palette4File, filename);
	ReWrite(palette4File);

  i:=0;
    repeat
 		Write(palette4File, TPalette4d[i].DWords);
        inc(i);
	until i=1;
	Close(palette4File);

End;

procedure Read16Palette(filename:string; ReadColor:boolean);
//yes, possibly 16BW, not implemented yet.

//Both CGA4a-d valid colors pull from here.
//EGA valid colors pull from Palettte64, with alloc pallette size of 16.

//so some color checking needs to be done accordingly(use dwords, its faster).

Var
	palette16File  : File of DWord;
	i,num            : integer;
   ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;

Begin
	Assign(palette16File, filename);
	ReSet(palette16File);
    Seek(palette16File, 0); //find first record
    i:=0;
        repeat
			Read(palette16File, TPalette16[i].DWords); //read everything in
			inc(i);
		until i=15;

    Close(palette16File);
        
    {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 1);
        inc(i);
    until i=15;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(16);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
        inc(i);
      until i=15;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,16);
        
   {$endif}
        
end;

procedure Read4PaletteA(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;
   ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;

Begin
	Assign(palette4File, filename);
	ReSet(palette4File);
    Seek(palette4File, 0); //find first record
    i:=0;
        repeat
			Read(palette4File, TPalette4a[i].DWords); //read everything in
			inc(i);
		until i=3;
    Close(palette4File);

    {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 1);
        inc(i);
    until i=3;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(3);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
        inc(i);
      until i=3;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,3);
        
   {$endif}
end;

procedure Read4PaletteB(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;
   ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;
    
Begin
	Assign(palette4File, filename);
	ReSet(palette4File);
    Seek(palette4File, 0); //find first record
    i:=0;
        repeat
        //CGA "B" is the most sensible palette
			Read(palette4File, TPalette4b[i].DWords); //read everything in
			inc(i);
		until i=3;
    Close(palette4File);

    {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 1);
        inc(i);
    until i=3;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(3);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
        inc(i);
      until i=3;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,3);
        
   {$endif}

end;

procedure Read4PaletteC(filename:string; ReadColor:boolean);
//possible this one is BW....
//(not accounted for BW yet.)
Var
	palette4File  : File of DWord;
	i,num            : integer;
   ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;

Begin
	Assign(palette4File, filename);
	ReSet(palette4File);
    Seek(palette4File, 0); //find first record
    i:=0;
        repeat
			Read(palette4File, TPalette4c[i].DWords); //read everything in
			inc(i);
		until i=3;
    Close(palette4File);
    {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 1);
        inc(i);
    until i=3;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(3);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
        inc(i);
      until i=3;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,3);
        
   {$endif}

end;

//mostly useless. SEtup as BW, let user change FG color.
procedure Read4PaletteD(filename:string);

Var
	palette4File  : File of DWord;
	i,num            : integer;
   ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;

Begin
	Assign(palette4File, filename);
	ReSet(palette4File);
    Seek(palette4File, 0); //find first record
    i:=0;
        repeat
        //CGA "B" is the most sensible palette
			Read(palette4File, TPalette4d[i].DWords); //read everything in
			inc(i);
		until i=1;
    Close(palette4File);

    {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 2);
        inc(i);
    until i=1;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(2);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
        inc(i);
      until i=1;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,2);
        
   {$endif}
    
end;

//we should ask if we want to read a color or BW file, else we duplicate code for one line of changes.
//there is a way to check- but we would have to peek inside the file or just display it and assume things.

//this could shove a BW file in the color section or a color file in the BW section...
//anyway, theres two sets of arrays and you can reset to defaults if you need to.

procedure initPalette256;

//256 color VGA palette based on XTerm colors(Unix)
// however the first 16 are off according to mCGA specs and have been corrected.
//xterms dont use "bold text by deault" an maybe this is where the confusion is.

//furthermore there are other 256rgb palettes.


var
    num,i:integer;


begin

valuelist256[0]:=$00;
valuelist256[1]:=$00;
valuelist256[2]:=$00;
valuelist256[3]:=$ff;
valuelist256[4]:=$00;
valuelist256[5]:=$00;
valuelist256[6]:=$00;
valuelist256[7]:=$ff;
valuelist256[8]:=$00;
valuelist256[9]:=$ff;
valuelist256[10]:=$ff;
valuelist256[11]:=$00;
valuelist256[12]:=$00;
valuelist256[13]:=$00;
valuelist256[14]:=$ff;
valuelist256[15]:=$ff;
valuelist256[16]:=$00;
valuelist256[17]:=$ff;
valuelist256[18]:=$ff;
valuelist256[19]:=$ff;
valuelist256[20]:=$00;
valuelist256[21]:=$c0;
valuelist256[22]:=$c0;
valuelist256[23]:=$c0;
valuelist256[24]:=$7f;
valuelist256[25]:=$7f;
valuelist256[26]:=$7f;
valuelist256[27]:=$7f;
valuelist256[28]:=$00;
valuelist256[29]:=$00;
valuelist256[30]:=$00;
valuelist256[31]:=$7f;
valuelist256[32]:=$00;
valuelist256[33]:=$7f;
valuelist256[34]:=$7f;
valuelist256[35]:=$00;
valuelist256[36]:=$00;
valuelist256[37]:=$00;
valuelist256[38]:=$7f;
valuelist256[39]:=$7f;
valuelist256[40]:=$00;
valuelist256[41]:=$7f;
valuelist256[42]:=$00;
valuelist256[43]:=$7f;
valuelist256[44]:=$7f;
valuelist256[45]:=$ff;
valuelist256[46]:=$ff;
valuelist256[47]:=$ff;
valuelist256[48]:=$00;
valuelist256[49]:=$00;
valuelist256[50]:=$00;

valuelist256[51]:=$00;
valuelist256[52]:=$00;
valuelist256[53]:=$5f;
valuelist256[54]:=$00;
valuelist256[55]:=$00;
valuelist256[56]:=$87;
valuelist256[57]:=$00;
valuelist256[58]:=$00;
valuelist256[59]:=$af;
valuelist256[60]:=$00;
valuelist256[61]:=$00;
valuelist256[62]:=$d7;
valuelist256[63]:=$00;
valuelist256[64]:=$00;
valuelist256[65]:=$ff;
valuelist256[66]:=$00;
valuelist256[67]:=$5f;
valuelist256[68]:=$00;
valuelist256[69]:=$00;
valuelist256[70]:=$5f;
valuelist256[71]:=$5f;
valuelist256[72]:=$00;
valuelist256[73]:=$5f;
valuelist256[74]:=$87;
valuelist256[75]:=$00;
valuelist256[76]:=$5f;
valuelist256[77]:=$af;
valuelist256[78]:=$00;
valuelist256[79]:=$5f;
valuelist256[80]:=$d7;
valuelist256[81]:=$00;
valuelist256[82]:=$5f;
valuelist256[83]:=$ff;
valuelist256[84]:=$00;
valuelist256[85]:=$87;
valuelist256[86]:=$00;
valuelist256[87]:=$00;
valuelist256[88]:=$87;
valuelist256[89]:=$5f;
valuelist256[90]:=$00;
valuelist256[91]:=$87;
valuelist256[92]:=$87;
valuelist256[93]:=$00;
valuelist256[94]:=$87;
valuelist256[95]:=$af;
valuelist256[96]:=$00;
valuelist256[97]:=$87;
valuelist256[98]:=$d7;
valuelist256[99]:=$00;
valuelist256[100]:=$87;

valuelist256[101]:=$ff;
valuelist256[102]:=$00;
valuelist256[103]:=$af;
valuelist256[104]:=$00;
valuelist256[105]:=$00;
valuelist256[106]:=$af;
valuelist256[107]:=$5f;
valuelist256[108]:=$00;
valuelist256[109]:=$af;
valuelist256[110]:=$87;
valuelist256[111]:=$00;
valuelist256[112]:=$af;
valuelist256[113]:=$af;
valuelist256[114]:=$00;
valuelist256[115]:=$af;
valuelist256[116]:=$d7;
valuelist256[117]:=$00;
valuelist256[118]:=$af;
valuelist256[119]:=$ff;
valuelist256[120]:=$00;
valuelist256[121]:=$d7;
valuelist256[122]:=$00;
valuelist256[123]:=$00;
valuelist256[124]:=$d7;
valuelist256[125]:=$5f;
valuelist256[126]:=$00;
valuelist256[127]:=$d7;
valuelist256[128]:=$87;
valuelist256[129]:=$00;
valuelist256[130]:=$d7;
valuelist256[131]:=$af;
valuelist256[132]:=$00;
valuelist256[133]:=$d7;
valuelist256[134]:=$d7;
valuelist256[135]:=$00;
valuelist256[136]:=$d7;
valuelist256[137]:=$ff;
valuelist256[138]:=$00;
valuelist256[139]:=$ff;
valuelist256[140]:=$00;
valuelist256[141]:=$00;
valuelist256[142]:=$ff;
valuelist256[143]:=$5f;
valuelist256[144]:=$00;
valuelist256[145]:=$ff;
valuelist256[146]:=$87;
valuelist256[147]:=$00;
valuelist256[148]:=$ff;
valuelist256[149]:=$af;
valuelist256[150]:=$00;

valuelist256[151]:=$ff;
valuelist256[152]:=$d7;
valuelist256[153]:=$00;
valuelist256[154]:=$ff;
valuelist256[155]:=$ff;
valuelist256[156]:=$5f;
valuelist256[157]:=$00;
valuelist256[158]:=$00;
valuelist256[159]:=$5f;
valuelist256[160]:=$00;
valuelist256[161]:=$5f;
valuelist256[162]:=$5f;
valuelist256[163]:=$00;
valuelist256[164]:=$87;
valuelist256[165]:=$5f;
valuelist256[166]:=$00;
valuelist256[167]:=$af;
valuelist256[168]:=$5f;
valuelist256[169]:=$00;
valuelist256[170]:=$d7;
valuelist256[171]:=$5f;
valuelist256[172]:=$00;
valuelist256[173]:=$ff;
valuelist256[174]:=$5f;
valuelist256[175]:=$5f;
valuelist256[176]:=$00;
valuelist256[177]:=$5f;
valuelist256[178]:=$5f;
valuelist256[179]:=$5f;
valuelist256[180]:=$5f;
valuelist256[181]:=$5f;
valuelist256[182]:=$87;
valuelist256[183]:=$5f;
valuelist256[184]:=$5f;
valuelist256[185]:=$af;
valuelist256[186]:=$5f;
valuelist256[187]:=$5f;
valuelist256[188]:=$d7;
valuelist256[189]:=$5f;
valuelist256[190]:=$5f;
valuelist256[191]:=$ff;
valuelist256[192]:=$5f;
valuelist256[193]:=$87;
valuelist256[194]:=$00;
valuelist256[195]:=$5f;
valuelist256[196]:=$87;
valuelist256[197]:=$5f;
valuelist256[198]:=$5f;
valuelist256[199]:=$87;
valuelist256[200]:=$87;

valuelist256[201]:=$5f;
valuelist256[202]:=$87;
valuelist256[203]:=$af;
valuelist256[204]:=$5f;
valuelist256[205]:=$87;
valuelist256[206]:=$d7;
valuelist256[207]:=$5f;
valuelist256[208]:=$87;
valuelist256[209]:=$ff;
valuelist256[210]:=$5f;
valuelist256[211]:=$af;
valuelist256[212]:=$00;
valuelist256[213]:=$5f;
valuelist256[214]:=$af;
valuelist256[215]:=$5f;
valuelist256[216]:=$5f;
valuelist256[217]:=$af;
valuelist256[218]:=$87;
valuelist256[219]:=$5f;
valuelist256[220]:=$af;
valuelist256[221]:=$af;
valuelist256[222]:=$5f;
valuelist256[223]:=$af;
valuelist256[224]:=$d7;
valuelist256[225]:=$5f;
valuelist256[226]:=$af;
valuelist256[227]:=$ff;
valuelist256[228]:=$5f;
valuelist256[229]:=$d7;
valuelist256[230]:=$00;
valuelist256[231]:=$5f;
valuelist256[232]:=$d7;
valuelist256[233]:=$5f;
valuelist256[234]:=$5f;
valuelist256[235]:=$d7;
valuelist256[236]:=$87;
valuelist256[237]:=$5f;
valuelist256[238]:=$d7;
valuelist256[239]:=$af;
valuelist256[240]:=$5f;
valuelist256[241]:=$d7;
valuelist256[242]:=$d7;
valuelist256[243]:=$5f;
valuelist256[244]:=$d7;
valuelist256[245]:=$ff;
valuelist256[246]:=$5f;
valuelist256[247]:=$ff;
valuelist256[248]:=$00;
valuelist256[249]:=$5f;
valuelist256[250]:=$ff;
valuelist256[251]:=$5f;
valuelist256[252]:=$5f;
valuelist256[253]:=$ff;
valuelist256[254]:=$87;
valuelist256[255]:=$5f;
valuelist256[256]:=$ff;
valuelist256[257]:=$af;
valuelist256[258]:=$5f;
valuelist256[259]:=$ff;
valuelist256[260]:=$d7;

valuelist256[261]:=$5f;
valuelist256[262]:=$ff;
valuelist256[263]:=$ff;
valuelist256[264]:=$87;
valuelist256[265]:=$00;
valuelist256[266]:=$00;
valuelist256[267]:=$87;
valuelist256[268]:=$00;
valuelist256[269]:=$5f;
valuelist256[270]:=$87;
valuelist256[271]:=$00;
valuelist256[272]:=$87;
valuelist256[273]:=$87;
valuelist256[274]:=$00;
valuelist256[275]:=$af;
valuelist256[276]:=$87;
valuelist256[277]:=$00;
valuelist256[278]:=$d7;
valuelist256[279]:=$87;
valuelist256[280]:=$00;
valuelist256[281]:=$ff;
valuelist256[282]:=$87;
valuelist256[283]:=$5f;
valuelist256[284]:=$00;
valuelist256[285]:=$87;
valuelist256[286]:=$5f;
valuelist256[287]:=$5f;
valuelist256[288]:=$87;
valuelist256[289]:=$5f;
valuelist256[290]:=$87;
valuelist256[291]:=$87;
valuelist256[292]:=$5f;
valuelist256[293]:=$af;
valuelist256[294]:=$87;
valuelist256[295]:=$5f;
valuelist256[296]:=$d7;
valuelist256[297]:=$87;
valuelist256[298]:=$5f;
valuelist256[299]:=$ff;
valuelist256[300]:=$87;

valuelist256[301]:=$87;
valuelist256[302]:=$00;
valuelist256[303]:=$87;
valuelist256[304]:=$87;
valuelist256[305]:=$5f;
valuelist256[306]:=$87;
valuelist256[307]:=$87;
valuelist256[308]:=$87;
valuelist256[309]:=$87;
valuelist256[310]:=$87;
valuelist256[311]:=$af;
valuelist256[312]:=$87;
valuelist256[313]:=$87;
valuelist256[314]:=$d7;
valuelist256[315]:=$87;
valuelist256[316]:=$87;
valuelist256[317]:=$ff;
valuelist256[318]:=$87;
valuelist256[319]:=$af;
valuelist256[320]:=$00;
valuelist256[321]:=$87;
valuelist256[322]:=$af;
valuelist256[323]:=$5f;
valuelist256[324]:=$87;
valuelist256[325]:=$af;
valuelist256[326]:=$87;
valuelist256[327]:=$87;
valuelist256[328]:=$af;
valuelist256[329]:=$af;
valuelist256[330]:=$87;
valuelist256[331]:=$af;
valuelist256[332]:=$d7;
valuelist256[333]:=$87;
valuelist256[334]:=$af;
valuelist256[335]:=$ff;
valuelist256[336]:=$87;
valuelist256[337]:=$d7;
valuelist256[338]:=$00;
valuelist256[339]:=$87;
valuelist256[340]:=$d7;
valuelist256[341]:=$5f;
valuelist256[342]:=$87;
valuelist256[343]:=$d7;
valuelist256[344]:=$87;
valuelist256[345]:=$87;
valuelist256[346]:=$d7;
valuelist256[347]:=$af;
valuelist256[348]:=$87;
valuelist256[349]:=$d7;
valuelist256[350]:=$d7;
valuelist256[351]:=$87;
valuelist256[352]:=$d7;
valuelist256[353]:=$ff;
valuelist256[354]:=$87;
valuelist256[355]:=$ff;
valuelist256[356]:=$00;
valuelist256[357]:=$87;
valuelist256[358]:=$ff;
valuelist256[359]:=$5f;
valuelist256[360]:=$87;
valuelist256[361]:=$ff;
valuelist256[362]:=$87;
valuelist256[363]:=$87;
valuelist256[364]:=$ff;
valuelist256[365]:=$af;
valuelist256[366]:=$87;
valuelist256[367]:=$ff;
valuelist256[368]:=$d7;
valuelist256[369]:=$87;
valuelist256[370]:=$ff;
valuelist256[371]:=$ff;
valuelist256[372]:=$af;
valuelist256[373]:=$00;
valuelist256[374]:=$00;
valuelist256[375]:=$af;
valuelist256[376]:=$00;
valuelist256[377]:=$5f;
valuelist256[378]:=$af;
valuelist256[379]:=$00;
valuelist256[380]:=$87;
valuelist256[381]:=$af;
valuelist256[382]:=$00;
valuelist256[383]:=$af;
valuelist256[384]:=$af;
valuelist256[385]:=$00;
valuelist256[386]:=$d7;
valuelist256[387]:=$af;
valuelist256[388]:=$00;
valuelist256[389]:=$af;
valuelist256[390]:=$af;
valuelist256[391]:=$5f;
valuelist256[392]:=$00;
valuelist256[393]:=$af;
valuelist256[394]:=$5f;
valuelist256[395]:=$5f;
valuelist256[396]:=$af;
valuelist256[397]:=$5f;
valuelist256[398]:=$87;
valuelist256[399]:=$af;
valuelist256[400]:=$5f;

valuelist256[401]:=$af;
valuelist256[402]:=$af;
valuelist256[403]:=$5f;
valuelist256[404]:=$d7;
valuelist256[405]:=$af;
valuelist256[406]:=$5f;
valuelist256[407]:=$ff;
valuelist256[408]:=$af;
valuelist256[409]:=$87;
valuelist256[410]:=$00;
valuelist256[411]:=$af;
valuelist256[412]:=$87;
valuelist256[413]:=$5f;
valuelist256[414]:=$af;
valuelist256[415]:=$87;
valuelist256[416]:=$87;
valuelist256[417]:=$af;
valuelist256[418]:=$87;
valuelist256[419]:=$af;
valuelist256[420]:=$af;
valuelist256[421]:=$87;
valuelist256[422]:=$d7;
valuelist256[423]:=$af;
valuelist256[424]:=$87;
valuelist256[425]:=$ff;
valuelist256[426]:=$af;
valuelist256[427]:=$af;
valuelist256[428]:=$00;
valuelist256[429]:=$af;
valuelist256[430]:=$af;
valuelist256[431]:=$5f;
valuelist256[432]:=$af;
valuelist256[433]:=$af;
valuelist256[434]:=$87;
valuelist256[435]:=$af;
valuelist256[436]:=$af;
valuelist256[437]:=$af;
valuelist256[438]:=$af;
valuelist256[439]:=$af;
valuelist256[440]:=$d7;


valuelist256[441]:=$af;
valuelist256[442]:=$af;
valuelist256[443]:=$ff;
valuelist256[444]:=$af;
valuelist256[445]:=$d7;
valuelist256[446]:=$00;
valuelist256[447]:=$af;
valuelist256[448]:=$d7;
valuelist256[449]:=$5f;

valuelist256[450]:=$af;
valuelist256[451]:=$d7;
valuelist256[452]:=$87;
valuelist256[453]:=$af;
valuelist256[454]:=$d7;
valuelist256[455]:=$af;
valuelist256[456]:=$af;
valuelist256[457]:=$d7;
valuelist256[458]:=$d7;
valuelist256[459]:=$af;

valuelist256[460]:=$d7;
valuelist256[461]:=$ff;
valuelist256[462]:=$af;
valuelist256[463]:=$ff;
valuelist256[464]:=$00;
valuelist256[465]:=$af;
valuelist256[466]:=$ff;
valuelist256[467]:=$5f;
valuelist256[468]:=$af;
valuelist256[469]:=$ff;

valuelist256[470]:=$87;
valuelist256[471]:=$af;
valuelist256[472]:=$ff;
valuelist256[473]:=$af;
valuelist256[474]:=$af;
valuelist256[475]:=$ff;
valuelist256[476]:=$d7;
valuelist256[477]:=$af;
valuelist256[478]:=$ff;
valuelist256[479]:=$ff;

valuelist256[480]:=$d7;
valuelist256[481]:=$00;
valuelist256[482]:=$00;
valuelist256[483]:=$d7;
valuelist256[484]:=$00;
valuelist256[485]:=$5f;
valuelist256[486]:=$d7;
valuelist256[487]:=$00;
valuelist256[488]:=$87;
valuelist256[489]:=$d7;
valuelist256[490]:=$00;

valuelist256[491]:=$af;
valuelist256[492]:=$d7;
valuelist256[493]:=$00;
valuelist256[494]:=$d7;
valuelist256[495]:=$d7;
valuelist256[496]:=$00;
valuelist256[497]:=$ff;
valuelist256[498]:=$d7;
valuelist256[499]:=$5f;

valuelist256[500]:=$00;
valuelist256[501]:=$d7;
valuelist256[502]:=$5f;
valuelist256[503]:=$5f;
valuelist256[504]:=$d7;
valuelist256[505]:=$5f;
valuelist256[506]:=$87;
valuelist256[507]:=$d7;
valuelist256[508]:=$5f;
valuelist256[509]:=$af;
valuelist256[510]:=$d7;
valuelist256[511]:=$5f;
valuelist256[512]:=$d7;
valuelist256[513]:=$d7;
valuelist256[514]:=$5f;
valuelist256[515]:=$ff;
valuelist256[516]:=$d7;
valuelist256[517]:=$87;
valuelist256[518]:=$00;
valuelist256[519]:=$d7;
valuelist256[520]:=$87;
valuelist256[521]:=$5f;
valuelist256[522]:=$d7;
valuelist256[523]:=$87;
valuelist256[524]:=$87;
valuelist256[525]:=$d7;
valuelist256[526]:=$87;
valuelist256[527]:=$af;
valuelist256[528]:=$d7;
valuelist256[529]:=$87;

valuelist256[530]:=$d7;
valuelist256[531]:=$d7;
valuelist256[532]:=$87;
valuelist256[533]:=$ff;
valuelist256[534]:=$d7;
valuelist256[535]:=$af;
valuelist256[536]:=$00;
valuelist256[537]:=$d7;
valuelist256[538]:=$af;
valuelist256[539]:=$5f;

valuelist256[540]:=$d7;
valuelist256[541]:=$af;
valuelist256[542]:=$87;
valuelist256[543]:=$d7;
valuelist256[544]:=$af;
valuelist256[545]:=$af;
valuelist256[546]:=$d7;
valuelist256[547]:=$af;
valuelist256[548]:=$d7;
valuelist256[549]:=$d7;
valuelist256[550]:=$af;
valuelist256[551]:=$ff;
valuelist256[552]:=$d7;
valuelist256[553]:=$d7;
valuelist256[554]:=$00;
valuelist256[555]:=$d7;
valuelist256[556]:=$d7;
valuelist256[557]:=$5f;
valuelist256[558]:=$d7;
valuelist256[559]:=$d7;
valuelist256[560]:=$87;

valuelist256[561]:=$d7;
valuelist256[562]:=$d7;
valuelist256[563]:=$af;
valuelist256[564]:=$d7;
valuelist256[565]:=$d7;
valuelist256[566]:=$d7;
valuelist256[567]:=$d7;
valuelist256[568]:=$d7;
valuelist256[569]:=$ff;

valuelist256[570]:=$d7;
valuelist256[571]:=$ff;
valuelist256[572]:=$00;
valuelist256[573]:=$d7;
valuelist256[574]:=$ff;
valuelist256[575]:=$5f;
valuelist256[576]:=$d7;
valuelist256[577]:=$ff;
valuelist256[578]:=$87;
valuelist256[579]:=$d7;

valuelist256[580]:=$ff;
valuelist256[581]:=$af;
valuelist256[582]:=$d7;
valuelist256[583]:=$ff;
valuelist256[584]:=$d7;
valuelist256[585]:=$d7;
valuelist256[586]:=$ff;
valuelist256[587]:=$ff;
valuelist256[588]:=$ff;
valuelist256[589]:=$00;

valuelist256[590]:=$00;
valuelist256[591]:=$ff;
valuelist256[592]:=$00;
valuelist256[593]:=$5f;
valuelist256[594]:=$ff;
valuelist256[595]:=$00;
valuelist256[596]:=$87;
valuelist256[597]:=$ff;
valuelist256[598]:=$00;
valuelist256[599]:=$af;

valuelist256[600]:=$ff;
valuelist256[601]:=$00;
valuelist256[602]:=$d7;
valuelist256[603]:=$ff;
valuelist256[604]:=$00;
valuelist256[605]:=$ff;
valuelist256[606]:=$ff;
valuelist256[607]:=$5f;
valuelist256[608]:=$00;
valuelist256[609]:=$ff;

valuelist256[610]:=$5f;
valuelist256[611]:=$5f;
valuelist256[612]:=$ff;
valuelist256[613]:=$5f;
valuelist256[614]:=$87;
valuelist256[615]:=$ff;
valuelist256[616]:=$5f;
valuelist256[617]:=$af;
valuelist256[618]:=$ff;
valuelist256[619]:=$5f;

valuelist256[620]:=$d7;
valuelist256[621]:=$ff;
valuelist256[622]:=$5f;
valuelist256[623]:=$ff;
valuelist256[624]:=$ff;
valuelist256[625]:=$87;
valuelist256[626]:=$00;
valuelist256[627]:=$ff;
valuelist256[628]:=$87;
valuelist256[629]:=$5f;

valuelist256[630]:=$ff;
valuelist256[631]:=$87;
valuelist256[632]:=$87;
valuelist256[633]:=$ff;
valuelist256[634]:=$87;
valuelist256[635]:=$af;
valuelist256[636]:=$ff;
valuelist256[637]:=$87;
valuelist256[638]:=$d7;
valuelist256[639]:=$ff;

valuelist256[640]:=$87;
valuelist256[641]:=$ff;
valuelist256[642]:=$ff;
valuelist256[643]:=$af;
valuelist256[644]:=$00;
valuelist256[645]:=$ff;
valuelist256[646]:=$af;
valuelist256[647]:=$5f;
valuelist256[648]:=$ff;
valuelist256[649]:=$af;

valuelist256[650]:=$87;
valuelist256[651]:=$ff;
valuelist256[652]:=$af;
valuelist256[653]:=$af;
valuelist256[654]:=$ff;
valuelist256[655]:=$af;
valuelist256[656]:=$d7;
valuelist256[657]:=$ff;
valuelist256[658]:=$af;
valuelist256[659]:=$ff;

valuelist256[660]:=$ff;
valuelist256[661]:=$d7;
valuelist256[662]:=$00;
valuelist256[663]:=$ff;
valuelist256[664]:=$d7;
valuelist256[665]:=$5f;
valuelist256[666]:=$ff;
valuelist256[667]:=$d7;
valuelist256[668]:=$87;
valuelist256[669]:=$ff;

valuelist256[670]:=$d7;
valuelist256[671]:=$af;
valuelist256[672]:=$ff;
valuelist256[673]:=$d7;
valuelist256[674]:=$d7;
valuelist256[675]:=$ff;
valuelist256[676]:=$d7;
valuelist256[677]:=$ff;
valuelist256[678]:=$ff;
valuelist256[679]:=$ff;

valuelist256[680]:=$00;
valuelist256[681]:=$ff;
valuelist256[682]:=$ff;
valuelist256[683]:=$5f;
valuelist256[684]:=$ff;
valuelist256[685]:=$ff;
valuelist256[686]:=$87;
valuelist256[687]:=$ff;
valuelist256[688]:=$ff;
valuelist256[689]:=$af;

valuelist256[690]:=$ff;
valuelist256[691]:=$ff;
valuelist256[692]:=$d7;
valuelist256[693]:=$ff;
valuelist256[694]:=$ff;
valuelist256[695]:=$ff;
valuelist256[696]:=$08;
valuelist256[697]:=$08;
valuelist256[698]:=$08;
valuelist256[699]:=$12;

valuelist256[700]:=$12;
valuelist256[701]:=$12;
valuelist256[702]:=$1c;
valuelist256[703]:=$1c;
valuelist256[704]:=$1c;
valuelist256[705]:=$26;
valuelist256[706]:=$26;
valuelist256[707]:=$26;
valuelist256[708]:=$30;
valuelist256[709]:=$30;

valuelist256[710]:=$30;
valuelist256[711]:=$3a;
valuelist256[712]:=$3a;
valuelist256[713]:=$3a;
valuelist256[714]:=$44;
valuelist256[715]:=$44;
valuelist256[716]:=$44;
valuelist256[717]:=$4e;
valuelist256[718]:=$4e;
valuelist256[719]:=$4e;

valuelist256[720]:=$58;
valuelist256[721]:=$58;
valuelist256[722]:=$58;
valuelist256[723]:=$62;
valuelist256[724]:=$62;
valuelist256[725]:=$62;
valuelist256[726]:=$6c;
valuelist256[727]:=$6c;
valuelist256[728]:=$6c;
valuelist256[729]:=$76;

valuelist256[730]:=$76;
valuelist256[731]:=$76;
valuelist256[732]:=$80;
valuelist256[733]:=$80;
valuelist256[734]:=$80;
valuelist256[735]:=$8a;
valuelist256[736]:=$8a;
valuelist256[737]:=$8a;
valuelist256[738]:=$94;
valuelist256[739]:=$94;

valuelist256[740]:=$94;
valuelist256[741]:=$9e;
valuelist256[742]:=$9e;
valuelist256[743]:=$9e;
valuelist256[744]:=$a8;
valuelist256[745]:=$a8;
valuelist256[746]:=$a8;
valuelist256[747]:=$b2;
valuelist256[748]:=$b2;
valuelist256[749]:=$b2;

valuelist256[750]:=$bc;
valuelist256[751]:=$bc;
valuelist256[752]:=$bc;
valuelist256[753]:=$c6;
valuelist256[754]:=$c6;
valuelist256[755]:=$c6;
valuelist256[756]:=$d0;
valuelist256[757]:=$d0;
valuelist256[758]:=$d0;
valuelist256[759]:=$da;

valuelist256[760]:=$da;
valuelist256[761]:=$da;
valuelist256[762]:=$e4;
valuelist256[763]:=$e4;
valuelist256[764]:=$e4;
valuelist256[765]:=$ee;
valuelist256[766]:=$ee;
valuelist256[767]:=$ee;
  i:=0;
   num:=0;
   repeat
      Tpalette256[num].colors^.r:=valuelist256[i];
      Tpalette256[num].colors^.g:=valuelist256[i+1];
      Tpalette256[num].colors^.b:=valuelist256[i+2];
      Tpalette256[num].colors^.a:=$ff; //rbgi technically but this is for SDL, not CGA VGA VESA ....
      inc(i,3);
      inc(num);
  until num=767;


//256 color VGA palette based on XTerm colors(Unix)
//first 16 colors now should be more accurate!

TPalette256[0].DWords:=$000000ff;
TPalette256[1].DWords:=$ff0000ff;
TPalette256[2].DWords:=$00ff00ff;
TPalette256[3].DWords:=$ffff00ff;
TPalette256[4].DWords:=$0000ffff;
TPalette256[5].DWords:=$ff00ffff;
TPalette256[6].DWords:=$ffff55ff;
TPalette256[7].DWords:=$c0c0c0ff;
TPalette256[8].DWords:=$7f7f7fff;
TPalette256[9].DWords:=$7f0000ff;
TPalette256[10].DWords:=$007f00ff;
TPalette256[11].DWords:=$7f7f00ff;
TPalette256[12].DWords:=$00007fff;
TPalette256[13].DWords:=$7f007fff;
TPalette256[14].DWords:=$007f7fff;
TPalette256[15].DWords:=$ffffffff;

TPalette256[16].DWords:=$000000ff;
TPalette256[17].DWords:=$00005fff;
TPalette256[18].DWords:=$000087ff;
TPalette256[19].DWords:=$0000afff;
TPalette256[20].DWords:=$0000d7ff;
TPalette256[21].DWords:=$0000ffff;
TPalette256[22].DWords:=$005f00ff;
TPalette256[23].DWords:=$005f5fff;
TPalette256[24].DWords:=$005f87ff;
TPalette256[25].DWords:=$005fafff;
TPalette256[26].DWords:=$005fd7ff;
TPalette256[27].DWords:=$005fffff;
TPalette256[28].DWords:=$008700ff;
TPalette256[29].DWords:=$00875fff;
TPalette256[30].DWords:=$008787ff;

TPalette256[31].DWords:=$0087afff;
TPalette256[32].DWords:=$0087d7ff;
TPalette256[33].DWords:=$0087ffff;
TPalette256[34].DWords:=$00af00ff;
TPalette256[35].DWords:=$00af5fff;
TPalette256[36].DWords:=$00af87ff;
TPalette256[37].DWords:=$00afafff;
TPalette256[38].DWords:=$00afd7ff;
TPalette256[39].DWords:=$00afffff;
TPalette256[40].DWords:=$00d700ff;
TPalette256[41].DWords:=$00d75fff;
TPalette256[42].DWords:=$00d787ff;
TPalette256[43].DWords:=$00d7afff;
TPalette256[44].DWords:=$00d7d7ff;
TPalette256[45].DWords:=$00d7ffff;

TPalette256[46].DWords:=$00ff00ff;
TPalette256[47].DWords:=$00ff5fff;
TPalette256[48].DWords:=$00ff87ff;
TPalette256[49].DWords:=$00ffafff;
TPalette256[50].DWords:=$00ffd7ff;
TPalette256[51].DWords:=$00ffffff;
TPalette256[52].DWords:=$5f0000ff;
TPalette256[53].DWords:=$5f005fff;
TPalette256[54].DWords:=$5f0087ff;
TPalette256[55].DWords:=$5f00afff;
TPalette256[56].DWords:=$5f00d7ff;
TPalette256[57].DWords:=$5f00ffff;
TPalette256[58].DWords:=$5f5f00ff;
TPalette256[59].DWords:=$5f5f5fff;
TPalette256[60].DWords:=$5f5f87ff;

TPalette256[61].DWords:=$5f5fafff;
TPalette256[62].DWords:=$5f5fd7ff;
TPalette256[63].DWords:=$5f5fffff;
TPalette256[64].DWords:=$5f8700ff;
TPalette256[65].DWords:=$5f875fff;
TPalette256[66].DWords:=$5f8787ff;
TPalette256[67].DWords:=$5f87afff;
TPalette256[68].DWords:=$5f87d7ff;
TPalette256[69].DWords:=$5f87ffff;
TPalette256[70].DWords:=$5faf00ff;
TPalette256[71].DWords:=$5faf5fff;
TPalette256[72].DWords:=$5faf87ff;
TPalette256[73].DWords:=$5fafafff;
TPalette256[74].DWords:=$5fafd7ff;
TPalette256[75].DWords:=$5fafffff;

TPalette256[76].DWords:=$5fd700ff;
TPalette256[77].DWords:=$5fd75fff;
TPalette256[78].DWords:=$5fd787ff;
TPalette256[79].DWords:=$5fd7afff;
TPalette256[80].DWords:=$5fd7d7ff;
TPalette256[81].DWords:=$5fd7ffff;
TPalette256[82].DWords:=$5fff00ff;
TPalette256[83].DWords:=$5fff5fff;
TPalette256[84].DWords:=$5fff87ff;
TPalette256[85].DWords:=$5fffafff;
TPalette256[86].DWords:=$5fffd7ff;
TPalette256[87].DWords:=$5fffffff;
TPalette256[88].DWords:=$870000ff;
TPalette256[89].DWords:=$87005fff;
TPalette256[90].DWords:=$870087ff;

TPalette256[91].DWords:=$8700afff;
TPalette256[92].DWords:=$8700d7ff;
TPalette256[93].DWords:=$8700ffff;
TPalette256[94].DWords:=$875f00ff;
TPalette256[95].DWords:=$875f5fff;
TPalette256[96].DWords:=$875f87ff;
TPalette256[97].DWords:=$875fafff;
TPalette256[98].DWords:=$875fd7ff;
TPalette256[99].DWords:=$875fffff;
TPalette256[100].DWords:=$878700ff;
TPalette256[101].DWords:=$87875fff;
TPalette256[102].DWords:=$878787ff;
TPalette256[103].DWords:=$8787afff;
TPalette256[104].DWords:=$8787d7ff;
TPalette256[105].DWords:=$8787ffff;

TPalette256[106].DWords:=$87af00ff;
TPalette256[107].DWords:=$87af5fff;
TPalette256[108].DWords:=$87af87ff;
TPalette256[109].DWords:=$87afafff;


TPalette256[110].DWords:=$87afd7ff;
TPalette256[111].DWords:=$87afffff;
TPalette256[112].DWords:=$87d700ff;
TPalette256[113].DWords:=$87d75fff;
TPalette256[114].DWords:=$87d787ff;
TPalette256[115].DWords:=$87d7afff;
TPalette256[116].DWords:=$87d7d7ff;
TPalette256[117].DWords:=$87d7ffff;
TPalette256[118].DWords:=$87ff00ff;
TPalette256[119].DWords:=$87ff5fff;
TPalette256[120].DWords:=$87ff87ff;

TPalette256[121].DWords:=$87ffafff;
TPalette256[122].DWords:=$87ffd7ff;
TPalette256[123].DWords:=$87ffffff;
TPalette256[124].DWords:=$af0000ff;
TPalette256[125].DWords:=$af005fff;
TPalette256[126].DWords:=$af0087ff;
TPalette256[127].DWords:=$af00afff;
TPalette256[128].DWords:=$af00d7ff;
TPalette256[129].DWords:=$af00ffff;
TPalette256[130].DWords:=$af5f00ff;
TPalette256[131].DWords:=$af5f5fff;
TPalette256[132].DWords:=$af5f87ff;
TPalette256[133].DWords:=$af5fafff;
TPalette256[134].DWords:=$af5fd7ff;
TPalette256[135].DWords:=$af5fffff;

TPalette256[136].DWords:=$af8700ff;
TPalette256[137].DWords:=$af875fff;
TPalette256[138].DWords:=$af8787ff;
TPalette256[139].DWords:=$af87afff;
TPalette256[140].DWords:=$af87d7ff;
TPalette256[141].DWords:=$af87ffff;
TPalette256[142].DWords:=$afaf00ff;
TPalette256[143].DWords:=$afaf5fff;
TPalette256[144].DWords:=$afaf87ff;
TPalette256[145].DWords:=$afafafff;
TPalette256[146].DWords:=$afafd7ff;
TPalette256[147].DWords:=$afafffff;
TPalette256[148].DWords:=$afd700ff;
TPalette256[149].DWords:=$afd75fff;
TPalette256[150].DWords:=$afd787ff;

TPalette256[151].DWords:=$afd7afff;
TPalette256[152].DWords:=$afd7d7ff;
TPalette256[153].DWords:=$afd7ffff;
TPalette256[154].DWords:=$afff00ff;
TPalette256[155].DWords:=$afff5fff;
TPalette256[156].DWords:=$afff87ff;
TPalette256[157].DWords:=$afffafff;
TPalette256[158].DWords:=$afffd7ff;
TPalette256[159].DWords:=$afffffff;
TPalette256[160].DWords:=$d70000ff;
TPalette256[161].DWords:=$d7005fff;
TPalette256[162].DWords:=$d70087ff;
TPalette256[163].DWords:=$d700afff;
TPalette256[164].DWords:=$d700d7ff;
TPalette256[165].DWords:=$d700ffff;

TPalette256[166].DWords:=$d75f00ff;
TPalette256[167].DWords:=$d75f5fff;
TPalette256[168].DWords:=$d75f87ff;
TPalette256[169].DWords:=$d75fafff;
TPalette256[170].DWords:=$d75fd7ff;
TPalette256[171].DWords:=$d75fffff;
TPalette256[172].DWords:=$d78700ff;
TPalette256[173].DWords:=$d7875fff;
TPalette256[174].DWords:=$d78787ff;
TPalette256[175].DWords:=$d787afff;
TPalette256[176].DWords:=$d787d7ff;
TPalette256[177].DWords:=$d787ffff;
TPalette256[178].DWords:=$d7af00ff;
TPalette256[179].DWords:=$d7af5fff;
TPalette256[180].DWords:=$d7af87ff;

TPalette256[181].DWords:=$d7afafff;
TPalette256[182].DWords:=$d7afd7ff;
TPalette256[183].DWords:=$d7afffff;
TPalette256[184].DWords:=$d7d700ff;
TPalette256[185].DWords:=$d7d75fff;
TPalette256[186].DWords:=$d7d787ff;
TPalette256[187].DWords:=$d7d7afff;
TPalette256[188].DWords:=$d7d7d7ff;
TPalette256[189].DWords:=$d7d7ffff;
TPalette256[190].DWords:=$d7ff00ff;
TPalette256[191].DWords:=$d7ff5fff;
TPalette256[192].DWords:=$d7ff87ff;
TPalette256[193].DWords:=$d7ffafff;
TPalette256[194].DWords:=$d7ffd7ff;
TPalette256[195].DWords:=$d7ffffff;

TPalette256[196].DWords:=$ff0000ff;
TPalette256[197].DWords:=$ff005fff;
TPalette256[198].DWords:=$ff0087ff;
TPalette256[199].DWords:=$ff00afff;
TPalette256[200].DWords:=$ff00d7ff;
TPalette256[201].DWords:=$ff00ffff;
TPalette256[202].DWords:=$ff5f00ff;
TPalette256[203].DWords:=$ff5f5fff;
TPalette256[204].DWords:=$ff5f87ff;
TPalette256[205].DWords:=$ff5fafff;
TPalette256[206].DWords:=$ff5fd7ff;
TPalette256[207].DWords:=$ff5fffff;
TPalette256[208].DWords:=$ff8700ff;
TPalette256[209].DWords:=$ff875fff;
TPalette256[210].DWords:=$ff8787ff;

TPalette256[211].DWords:=$ff87afff;
TPalette256[212].DWords:=$ff87d7ff;
TPalette256[213].DWords:=$ff87ffff;
TPalette256[214].DWords:=$ffaf00ff;
TPalette256[215].DWords:=$ffaf5fff;
TPalette256[216].DWords:=$ffaf87ff;
TPalette256[217].DWords:=$ffafafff;
TPalette256[218].DWords:=$ffafd7ff;
TPalette256[219].DWords:=$ffafffff;
TPalette256[220].DWords:=$ffd700ff;

TPalette256[221].DWords:=$ffd75fff;
TPalette256[222].DWords:=$ffd787ff;
TPalette256[223].DWords:=$ffd7afff;
TPalette256[224].DWords:=$ffd7d7ff;

TPalette256[225].DWords:=$ffd7ffff;
TPalette256[226].DWords:=$ffff00ff;
TPalette256[227].DWords:=$ffff5fff;
TPalette256[228].DWords:=$ffff87ff;
TPalette256[229].DWords:=$ffffafff;
TPalette256[230].DWords:=$ffffd7ff;


TPalette256[231].DWords:=$ffffffff;
TPalette256[232].DWords:=$080808ff;
TPalette256[233].DWords:=$121212ff;
TPalette256[234].DWords:=$1c1c1cff;
TPalette256[235].DWords:=$262626ff;
TPalette256[236].DWords:=$303030ff;
TPalette256[237].DWords:=$3a3a3aff;
TPalette256[238].DWords:=$444444ff;
TPalette256[239].DWords:=$4e4e4eff;
TPalette256[240].DWords:=$585858ff;
TPalette256[241].DWords:=$626262ff;
TPalette256[242].DWords:=$6c6c6cff;
TPalette256[243].DWords:=$767676ff;
TPalette256[244].DWords:=$808080ff;
TPalette256[245].DWords:=$8a8a8aff;
TPalette256[246].DWords:=$949494ff;
TPalette256[247].DWords:=$9e9e9eff;
TPalette256[248].DWords:=$a8a8a8ff;
TPalette256[249].DWords:=$b2b2b2ff;
TPalette256[250].DWords:=$bcbcbcff;
TPalette256[251].DWords:=$c6c6c6ff;
TPalette256[252].DWords:=$d0d0d0ff;
TPalette256[253].DWords:=$dadadaff;
TPalette256[254].DWords:=$e4e4e4ff;
TPalette256[255].DWords:=$eeeeeeff;

end;


//supply a filename like:
// yes, you should be using long file names
// PAL256C.dat or PAL256BW.dat

//(dont make me duplicate code to change two lines)

procedure Save256Palette(filename:string; writecolor:boolean);

Var
	palette256File  : File of DWord;
	i,num            : integer;

Begin

    //save us a lot of code and work
    initPalette256;
	Assign(palette256File, filename);
	ReWrite(palette256File);

  i:=0;

    if WriteColor=true then begin

     repeat
 	    	Write(palette256File, TPalette256[i].DWords);
         inc(i);
	    until i=255;
    end else begin
        repeat
 	    	Write(palette256File,Tpalette256Grey[i].DWords);
            inc(i);
	    until i=255;
   
    end;

	Close(palette256File);

End;


procedure Read256Palette(filename:string; ReadColor:boolean);

Var
	palette256File  : File of DWord;
	i,num            : integer;
 ThisPallette:PSDL_Palette;
   smackaroon: TSDL_Colors;
Begin
	Assign(palette256File, filename);
	ReSet(palette256File);
    Seek(palette256File, 0); //find first record


   if ReadColor=true then begin
        repeat
			Read(palette256File, TPalette256[i].DWords); //read everything in
			inc(i);
		until i=255;
    end	else begin
        repeat
			Read(palette256File, TPalette256GRey[i].DWords);
			inc(i);
		until i=255;
	end;
	Close(palette256File);
	
	  {$ifdef cpu32}
    i:=0;
    repeat
        SDL_SetPalette(Mainsurface, SDL_LOGPAL, TPalette64[i].colors, i, 256);
        inc(i);
    until i=1;    
   {$endif}
    {$ifdef cpu64} 
      ThisPallette:=SDL_AllocPalette(256);
      i:=0;
      repeat
        smackaroon[i]^:=TPalette64[i].colors^;
        inc(i);
      until i=255;
        SDL_SetPaletteColors(ThisPallette,smackaroon,0,256);
        
   {$endif}
end;

//semi-generic color functions


function GetRGBfromIndex(index:byte):PSDL_Color;
//if its indexed- we have the rgb definition already!!

var
   somecolor:PSDL_Color;

begin
  if bpp=8 then begin

    if MaxColors =16 then
	      somecolor:=Tpalette64[index].colors //literally get the SDL_color from the index
    else if MaxColors=255 then
	      somecolor:=Tpalette256[index].colors;
    GetRGBFromIndex:=somecolor;
  end else if MaxColors=4 then begin
    case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                         somecolor:=Tpalette4a[index].Colors;
                    2:
                         somecolor:=Tpalette4b[index].Colors;
                    3:
                         somecolor:=Tpalette4c[index].Colors;
                    4: 
                         somecolor:=Tpalette4d[index].Colors;

                   end;     
              end;     
     end;
  end
  
  else begin
		LogLn('Attempt to fetch RGB from non-Indexed color.Wrong routine called.');
        exit;
  end;
end;

function GetDWordfromIndex(index:byte):DWord;
//if its indexed- we have the rgb definition already!!

var
   somecolor:DWord;

begin
  if bpp=8 then begin
    if MaxColors =16 then
	      somecolor:=Tpalette64[index].DWords //literally get the DWord from the index
    else if MaxColors=255 then
	      somecolor:=Tpalette256[index].DWords;
    else if MaxColors=4 then begin
    
    case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                         somecolor:=Tpalette4a[index].DWords;
                    2:
                         somecolor:=Tpalette4b[index].DWords;
                    3:
                         somecolor:=Tpalette4c[index].DWords;
                    4: 
                         somecolor:=Tpalette4d[index].DWords;

                   end;     
            end;    
   end;
   GetDWordFromIndex:=somecolor;
  end else begin
	    	LogLn('Attempt to fetch indexed DWord from non-indexed color');
      exit;

  end;
end;


function GetRGBFromHexPalletted(input:DWord):PSDL_Color;
var
	i:integer;

begin

  if bpp=8 then begin
   if (MaxColors=255) then begin
	   i:=0;
	   while (i<255) do begin
		    if (Tpalette256[i].dwords = input) then begin //did we find a match?
 			   GetRGBFromHexPalletted:=Tpalette256[i].colors;
               exit;

           end else
				inc(i);  //no
       end;
	  //error:no match found
	  LogLn('No match found');
      exit;

   end else if (MaxColors=16) then begin
	    i:=0;
	    while (i<15) do begin

		    if (Tpalette64[i].dwords = input) then begin//did we find a match?
               GetRGBFromHexPalletted:=Tpalette64[i].colors;
               exit;
            end else
				inc(i);  //no
       end;
	  //error:no match found
      exit;

   end;
  end else if (bpp >8) then begin
//use DWordToRGB
       if IsConsoleInvoked then
             LogLn('GetRGBfromHexPalletted: trying to fetch non-existent RGB palletted data in TrueColor mode');

 end;
end;


//its either we hack this to hell- or reimplement MainSurface- the easier option.
//MainSurface^.PixelFormat is unknown-but assignable.

{$ifdef cpu32}
function GetRGBfromHexTC(somedata:DWord):PSDL_Color;

var
  someColor:PSDL_Color;
  r,g,b,a:PUInt8;
  
begin
 if (bpp <15) then begin

			LogLn('Cant Get RGBA data from this bpp.');

      exit;
   end;

       somecolor:=DWordToRGB(somedata:DWord);
       somecolor^.a:=$ff;
   	   GetRGBfromHexTC:=somecolor;
end;
{$endif}

{$ifdef cpu64}
function GetRGBAFromHexTC(input:DWord):PSDL_Color;
var
    somecolor:PSDL_Color;
    r,g,b,a:PUInt8;

begin
   if (bpp <15) then begin

			LogLn('Cant Get RGBA data from this bpp.');

      exit;
   end;

       somecolor:=DWordToRGB(input:DWord);
       GetRGBAfromHexTC:=somecolor;
end;

{$endif}

//get the last color set:
//_bgcolor and _fgcolor hold this information. No need to go anywhere.

function GetFgRGB:PSDL_Color;

begin
    GetFgRGB:=_Fgcolor;
end;

{$ifdef cpu64}
function GetFgRGBA:PSDL_Color;

begin
    GetFgRGBA:=_Fgcolor;
end;
{$endif}

//give me the name(string) of the current fg or bg color (paletted modes only) from the screen


function GetFGName:string;

var
   i:byte;
   somecolor:PSDL_Color;
   someDWord:DWord;

begin
   if (MaxColors> 256) then begin
			LogLn('Cant Get color name from an RGB mode colors.');
	  exit;
   end;
   i:=0;
//check for validity of BG /FG DWord data
   i:=GetFGColorIndex;
   if MaxColors=4 then begin
   
   case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                       GetFGName:=GEtEnumName(typeinfo(Tpalette4aNames),ord(i));
                    2:
                       GetFGName:=GEtEnumName(typeinfo(Tpalette4bNames),ord(i));

                    3:         
                       GetFGName:=GEtEnumName(typeinfo(Tpalette4cNames),ord(i));

                    4:
                      GetFGName:=GEtEnumName(typeinfo(Tpalette4dNames),ord(i));

                   end;     
            end;    
            
   end;         
   if MaxColors=255 then begin
	      GetFGName:=GEtEnumName(typeinfo(TPalette256Names),ord(i));
		  exit;
		  
   end else if MaxColors=16 then begin
	      GetFGName:=GEtEnumName(typeinfo(Tpalette64Names),ord(i));
		  exit;
   end;

end;


function GetBGName:string;

var
   i:byte;
   somecolor:PSDL_Color;
   someDWord:DWord;

begin
   if (MaxColors> 256) then begin
			LogLn('Cant Get color name from an RGB mode colors.');
	  exit;
   end;
   i:=0;
   i:=GetBGColorIndex;
   if MaxColors=4 then begin
   
   case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                       GetBGName:=GEtEnumName(typeinfo(Tpalette4aNames),ord(i));
                    2:
                       GetBGName:=GEtEnumName(typeinfo(Tpalette4bNames),ord(i));

                    3:         
                       GetBGName:=GEtEnumName(typeinfo(Tpalette4cNames),ord(i));

                    4:
                      GetBGName:=GEtEnumName(typeinfo(Tpalette4dNames),ord(i));

                   end;     
            end;    
            
   end;         
   if MaxColors=255 then begin
	      GetBGName:=GEtEnumName(typeinfo(TPalette256Names),ord(i));
		  exit;
		  
   end else if MaxColors=16 then begin
	      GetBGName:=GEtEnumName(typeinfo(Tpalette64Names),ord(i));
		  exit;
   end;

end;


function GetFGColorIndex:byte;

var
   i:integer;
   someColor:PSDL_Color;
   r,g,b,a:PUInt8;

begin
//prevent a SDLv2 fetch from renderer(slow), if possible. fg and bg colors should already be set.


     if MaxColors=4 then begin
       i:=0;
       repeat
       
       //which 4 color CGA palette?
           case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                        _fgcolor:=Tpalette4a[i].DWords;
                    2:
                        _fgcolor:=Tpalette4b[i].DWords;
                    3:
                        _fgcolor:=Tpalette4c[i].DWords;
                    4:
                        _fgcolor:=Tpalette4d[i].DWords;
                   end;     
            end;    
        end;
            //faster to do DWord compare            
             if _fgcolor = TPalette4[i].dwords then begin
		        GetFGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=15;
     end;


     if MaxColors=16 then begin
       i:=0;
       repeat
            //faster to do DWord compare
             if _fgcolor = TPalette64[i].dwords then begin
		        GetFGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=15;
     end;

     if MaxColors=255 then begin
       i:=0;
       repeat
            if _fgcolor = TPalette255[i].dwords then begin
		        GetFGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=255;

	 end else begin
			LogLn('Cant Get index from an RGB mode colors.');
		exit;
	 end;
end;

function GetBGColorIndex:byte;

var
   i:integer;
   someColor:PSDL_Color;
  

begin
//prevent a SDLv2 fetch from renderer(slow), if possible. fg and bg colors should already be set.


     if MaxColors=4 then begin
       i:=0;
       repeat
       
       //which 4 color CGA palette?
           case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                        _bgcolor:=Tpalette4a[i].DWords;
                    2:
                        _bgcolor:=Tpalette4b[i].DWords;
                    3:
                        _bgcolor:=Tpalette4c[i].DWords;
                    4:
                        _bgcolor:=Tpalette4d[i].DWords;
                   end;     
            end;    
        end;
            //faster to do DWord compare            
             if _bgcolor = TPalette4[i].dwords then begin
		        GetFGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=15;
     end;


     if MaxColors=16 then begin
       i:=0;
       repeat
            //faster to do DWord compare
             if _fgcolor = TPalette64[i].dwords then begin
		        GetBGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=15;
     end;

     if MaxColors=255 then begin
       i:=0;
       repeat
            if _fgcolor = TPalette255[i].dwords then begin
		        GetBGColorIndex:=i;
			    exit;
            end;
            inc(i);
       until i=255;

	 end else begin
			LogLn('Cant Get index from an RGB mode colors.');
		exit;
	 end;
end;

{
How to implement "turtle graphics":

on start( the end of initgraph):

show a turtle in center of the screen as a "mouse cursor"
and give user option of "mouse"(pen) or keyboard for input(or assume it)


PenUp -(GotoXY and wait for PenDown to be called)
PenDown -(Line x,y from relx,rely and wait for penUp to be called)


are the only routines we need.

to exit:

InTurtleGraphics:=false;
SDL_RenderClear;

closeGraph when done.

However- this was before the era of the mouse and we can do better in SDL.

Create a "clickable canvas".

PenDown is DownMouseButton[x]
PenUp is UpMouseButton[x]

- (you are drawing with the mouse, not the keyboard.)
- this demo has been done.

(easily catchable in the event handler)

}


//sets "pen color" given an indexed input
procedure setFGColorIndexed(color:byte);

var
    colorToSet:Dword;
    
begin

   if MaxColors=255 then begin
        colorToSet:=Tpalette256[color].DWords;
        _fgcolor:= colorToSet;
   end else if MaxColors=16 then begin //EGA, not CGA
		colorToSet:=Tpalette64[color].DWords;
        _fgcolor:= colorToSet;
   end else if MaxColors=4 then begin //CGA, 
        //which CGA mode? theres 4 different palletes.
        case(mode) of
               CGAMed,CGALo: begin
                   case (CGAPalette) of
                    1:
                        colorToSet:=Tpalette4a[color].DWords;
                    2:
                        colorToSet:=Tpalette4b[color].DWords;
                    3:
                        colorToSet:=Tpalette4c[color].DWords;
                    4:
                        colorToSet:=Tpalette4d[color].DWords;
                   end;     
            end;    
        end;
		_fgcolor:= colorToSet;
        
   end else if MaxColors=2 then begin //CGAHi: can only set FG, not BG color
		colorToSet:=Tpalette16[color].DWords; //the default EGA palette setting, of 16colors, not default 4 cga colors
        _fgcolor:= colorToSet;
   end;

end;


//sets pen color to given dword.
procedure setFGColorRGB(someDword:dword); 
var
    	someColor:PSDL_Color;

begin
        _fgcolor:= someDword;

      {$ifdef cpu64}
        someColor:=DWordToRGB(_fgcolor);
        someColor^.a:=$ff;

        SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,somecolor^.a);      
      {$endif}
        
end;

{$ifdef cpu64}
procedure setFGColorRGBA(someDword:dword); 
var
    	someColor:PSDL_Color;

begin
        _fgcolor:= someDword;

        someColor:=DWordToRGBA(_fgcolor);
        SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,somecolor^.a);      
        
end;
{$endif}


procedure setFGColorRGB(r,g,b:word);  
var
	someColor:PSDL_Color;
	someDWord:DWord;

begin
      somecolor^.r:=byte(^r);
      somecolor^.g:=byte(^g);
      somecolor^.b:=byte(^b);
      someColor^.a:=$ff;

      someDWord:=RGBToDWord(somecolor);
      _fgColor:=someDWord; //this isnt PutPixel...
      {$ifdef cpu64}
        SDL_SetRenderDrawColor(Renderer,somecolor^.r,somecolor^.g,somecolor^.b,somecolor^.a);      
      {$endif}
end;

{$ifdef cpu64}
procedure setFGColorRGBA(r,g,b,a:word); 
var
	someColor:PSDL_Color;
	someDWord:DWord;
    r, g, b, a:PUInt8;

begin

    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=byte(^a);
    
    someDWord:=RGBAToDWord(somecolor);
    _fgcolor:=someDWord;
end;
{$endif}


//sets background color based on index
//which surface?? The main one, or a sub-one?
procedure setBGColorIndexed(index:byte);

begin

    if MaxColors=255 then begin
        _bgcolor:=Tpalette256[index].dwords; //set here- fetch later

   if MaxColors=16 then begin
        _bgcolor:=Tpalette64[index].dwords; //set here- fetch later

   if MaxColors=8 then begin
        _bgcolor:=Tpalette4[index].dwords; //set here- fetch later

   if MaxColors=2 then exit;
        //CGAHi.. not possible, FG only.
    
end;

procedure setBGColor(someDword:DWord); 
begin
       _bgcolor:=SomeDword; //set here- fetch later
end;

procedure setBGColorRGB(r,g,b:byte); 

//bgcolor here and rgb *MAY* not match our palette..be advised.
var
     r, g, b, a:PUInt8;

begin
        somecolor^.r:=byte(^r);
        somecolor^.g:=byte(^g);
        somecolor^.b:=byte(^b);
        somecolor^.a:=byte(^a);
       _bgcolor:=RGBToDWord(somecolor);

end;

{$ifdef cpu64}

procedure setBGColorRGBA(r,g,b,a:byte); overload;

//bgcolor here and rgba *MAY* not match our palette..be advised.
var
    somecolor:PSDL_Color;
     r, g, b, a:PUInt8;
begin
        somecolor^.r:=byte(^r);
        somecolor^.g:=byte(^g);
        somecolor^.b:=byte(^b);
        somecolor^.a:=byte(^a);
        _bgcolor:=RGBAToDWord(somecolor);
end;
{$endif}

procedure FlushBGColor;
//repaint the background, not always wanted. By itself, setting a BGColor in GL/SDL "does nothing" visually.
var
    Rect:PSDL_Rect;

begin
{$ifdef cpu32}
        Rect^.X:=0;
        Rect^.Y:=0;
        Rect^.W:=MaxX;
        Rect^.H:=MaxY;
        SDLFillRect(MainSurface,Rect,_bgcolor);
{$endif}

{$ifdef cpu64}
    	SDL_SetRenderDrawColor(renderer, somecolor^.r, somecolor^.g, somecolor^.b, somecolor^.a);
    	SDL_RenderClear(renderer);
{$endif}
        //reset coords as well
        X:=0;
        y:=0;
        _currY:=0;
        _currX:=0;

end;

//if we set this correctly, we should be able to fetch same with ease, not requiring GetPixel/Renderer routines,
//bonus: its faster.

function GetBgDWordRGB:DWord;
var
	someColor:PSDL_Color;
    someDWord:Dword;
begin

    if (MaxColors<=255) then exit;
          LogLn('Trying to fetch background color -when we have it- in Paletted mode.');
          exit;
       end;

    GetBgDWordRGB:=_bgcolor;
end;

{$ifdef cpu64}
function GetBgDWordRGBA:DWord;
begin
    if (MaxColors<=255) then exit;
          LogLn('Trying to fetch background color -when we have it- in Paletted mode.');
          exit;
       end;

    GetBgDWordRGBA:=_bgcolor;
end;
{$endif}

function GetFgDWordRGB:DWord;
var
	someColor:PSDL_Color;
    someDWord:Dword;
begin

    if (MaxColors<=255) then exit;
          LogLn('Trying to fetch background color -when we have it- in Paletted mode.');
          exit;
       end;

    GetFgDWordRGB:=_fgcolor;
end;

{$ifdef cpu64}
function GetFgDWordRGBA:DWord;
begin
    if (MaxColors<=255) then exit;
          LogLn('Trying to fetch background color -when we have it- in Paletted mode.');
          exit;
       end;

    GetFgDWordRGBA:=_fgcolor;
end;
{$endif}

procedure invertColorsBW;

var
     fgcolorsave,bgcolorsave:DWord;
begin
   if MaxColors=1 then exit; //force compliance w CGAHi mode definition.

   if not Inverted then begin
    fgcolorSave:=_fgcolor;
    bgcolorsave:=_bgcolor;
//flip
    _fgcolor:=_bgcolorSave;
    _bgcolor:=_fgcolorSave;
    
end else begin
   _fgcolor:=fgcolorsave;
   _bgcolor:=bgcolorsave;
end;

end;

//there is no A bit....
{$ifdef cpu64}

procedure invertColors4;

//so you see how to do manual shifting etc..
var
   r, g, b, a:PUInt8;
   somecolor:PSDL_Color;
begin
	somecolor:= DWordToRGB(_fgcolor);
    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=$255;

	SDL_SetRenderDrawColor(renderer, 3 - somecolor^.r, 3 - somecolor^.g, 3 - somecolor^.b, somecolor^.a);

end;

procedure invertColors16;

//so you see how to do manual shifting etc..
var
   r, g, b, a:PUInt8;
   somecolor:PSDL_Color;
begin
		somecolor:= DWordToRGB(_fgcolor);

    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=$255;

	SDL_SetRenderDrawColor(renderer, 15 - somecolor^.r, 15 - somecolor^.g, 15 - somecolor^.b, somecolor^.a);

end;

procedure invertColors64;

//so you see how to do manual shifting etc..
var
   r, g, b, a:PUInt8;
   somecolor:PSDL_Color;
begin

	somecolor:= DWordToRGB(_fgcolor);

    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=$255;

	SDL_SetRenderDrawColor(renderer, 63 - somecolor^.r, 63 - somecolor^.g, 63 - somecolor^.b, somecolor^.a);
end;

procedure invertColors256;

//so you see how to do manual shifting etc..
var
   r, g, b, a:PUInt8;
   somecolor:PSDL_Color;
begin

	somecolor:= DWordToRGB(_fgcolor);

    somecolor^.r:=byte(^r);
    somecolor^.g:=byte(^g);
    somecolor^.b:=byte(^b);
    somecolor^.a:=$255;

	SDL_SetRenderDrawColor(renderer, 255 - somecolor^.r, 255 - somecolor^.g, 255 - somecolor^.b, somecolor^.a);
end;
{$endif}


{

DO NOT BLINDLY ALLOW any function to be called arbitrarily.(this was done in C- I removed the checks for a short while)

two exceptions:

making a 16 or 256 palette and/or modelist file

NET:

	init and teardown need to be added here- I havent gotten to that.

}



//this was ported from (SDL) C-
//https://stackoverflow.com/questions/37978149/sdl1-sdl2-resolution-list-building-with-a-custom-screen-mode-class

//(its a VESA/VGA equivalent for SDL)
//checking supported is in another routine(DetectGraph)

{function FetchModeList:Tmodelist;

//use XRandr...or...SetVideoMode


var
    mode_index,modes_count ,display_index,display_count:integer;
    i:integer;


begin
   display_count := SDL_GetNumVideoDisplays; //1->display0
   LogLn('Number of displays: '+ intTOstr(display_count));
   display_index := 0;

//for each monitor do..
while  (display_index <= display_count) do begin
    LogLn('Display: '+intTOstr(display_index));

    modes_count := SDL_GetNumDisplayModes(display_index); //max number of supported modes
    mode_index:= 0;
    i:=0;

    //do for each mode in the number of possible modes
    while ( mode_index <= modes_count ) do begin

        SDLmodePointer^.format:= SDL_PIXELFORMAT_UNKNOWN;
        SDLmodePointer^.w:= 0;
        SDLmodePointer^.h:= 0;
        SDLmodePointer^.refresh_rate:= 0;
        SDLmodePointer^.driverdata:= Nil;


        if (SDL_GetDisplayMode(display_index, mode_index, SDLmodePointer) = 0) then begin //mode supported

            //Log: pixelFormat(bpp)MaxX,MaXy,refrsh_rate
            LogLn(IntToStr(SDL_BITSPERPIXEL(SDLmodePointer^.format))+' bpp'+ IntToStr(SDLmodePointer^.w)+ ' x '+IntToStr(SDLmodePointer^.h)+ '@ '+IntToStr(SDLmodePointer^.refresh_rate)+' Hz ');

            //store data in a modeList array

                SDLmodeArray[i].format:=SDL_BITSPERPIXEL(SDLmodePointer^.format);
                SDLmodeArray[i].w:=SDLmodePointer^.w;
                SDLmodeArray[i].h:=SDLmodePointer^.h;
                SDLmodeArray[i].refresh_rate:=SDLmodePointer^.refresh_rate;
                SDLmodeArray[i].driverdata:=SDLmodePointer^.driverdata;

        end;
        inc(i);
        inc(mode_index);
    end;
    inc(display_index);

end;
}


{

Graphics detection is a wonky process.
1- fetch (or find) whats supported
2- find highest mode in that list
3- use it, or try to.

repeat num 2 and 3 if you fail- until no modes work

BGI:
I was THIS mode, and none other- it had better be supported (but is it?) 
- I follow the BGI implementation concept.

Although generally in SDL(and on hardware) smaller windows and color depths are supported(emulation),
SDL -by itself- might not support that mode. (This is because your GFX chip- and openGL- does not.)


Do we care- as mostly we are setting windows sizes? Not usually.
WE could care less if the data is scaled on fullsceen-since we arent responsible for the scaling
(whether things look good- is another matter)

A TON of old code was written when pixels were actually visible. NOT THE CASE, anymore. (Its like a pinprick on the screen."LightBrite(TM) dots are actually "bullets".)

DetectGraph is here more for compatibility than anything else.
It seems to be used as often as DEFINED modes.

}

procedure IntHandler;
// this is a semi-dummy routine.
// its up to you what to do with the data.
//due to SDL bugs in WINDOW HANDLING, DONT OVERRIDE THIS.
//instead instal a UserHandler for the "input processing".
//remember, we are recursing, process and bail. Log if necessary.

//do we know when to stop?
var
    quit,minimized:boolean;
    
begin
  quit:=false;
  while quit=false do begin //mainloop -should never exit(should be is own thread)

{ threads: (must use 486+ cpu/PowerPC or equiv)
Pascalmain/GameLoop(calling us)
    Renderer/Processing
    Input

I believe the break routines in the C is incorrect. 
Break =ignore input, not exit routine.

}

  if (SDL_PollEvent(event) = 1) then begin //if events waiting
  // if no events, keep checking for them. only stop on program exit.
  
      case event^.type_ of
        
        //keyboard events
        SDL_KEYDOWN: begin //multiplatform fix for SDL "Close event"(alt+F4,cmd+q) bug
            keyPressed:=true;
			case (event^.key.keysym.sym) of
			    			    	
			        //the debate here is:
                    //1: use a library, like this so you dont have to rewrite this shit.
                    //2: add a reaalyQuit dialog? -or just bail? (flag check!)
                
                //OSX= darwin+unix. We dont care what CPU type
                {$IFDEF UNIX}       // mac & linux 
                    {$IFDEF DARWIN} // mac - powerpc & intel based    
                        SDLK_Q: begin //OSX "killWindow" combo
                            if(ev^.key.keysym.mod and KMOD_META) then begin
                       //ignore the keypress combo if programmer decided on a dialog(or answer is no)
                                if ReallyQuitDialogExists then begin
                                     if not ReallyQuit:=true then break else begin
                                         paused:=true;
                                         quit:=true;         
                                end;
                            end;
                            paused:=true;
                            quit:=true;    
                        end;
                	{$ENDIF}
                {$ENDIF}	
			    SDLK_F4: begin //X11 and windows "kill window" combo
                    //either ALT key			     
                    if(ev^.key.keysym.sym = SDLK_ALT) then begin
                       //ignore the keypress combo if programmer decided on a dialog(or answer is no)
                       if ReallyQuitDialogExists then begin
                            if not ReallyQuit:=true then break else begin
                                paused:=true;
                                quit:=true;
                                
                            end;
                       end;
                       paused:=true;
                       quit:=true;
                                
                     end;
				end;
				//else: allow user processing
				else begin
                    //code written, not imported yet, some is on my phone. Go figure.
                    //passThru input to YOUR CODE.				
				end;
			end;
        end;
        
        
        //as with keys, most just use the DOWN event. NEVER ASSUME.
       SDL_MOUSEBUTTONUP: begin
		   MousebuttonUp:=true;
		   MouseButtonDown:=false;
				// which button
				case ( event^.button.button ) of 
					SDL_BUTTON_LEFT: LeftMouseReleased:=true;
					SDL_BUTTON_RIGHT: RightMouseReleased:=true;
				end;		
		end;				
	    SDL_MOUSEBUTTONDOWN: begin
	      MousebuttonDown:=true;
	      MouseButtonUP:=false;
				// which button
				case ( event^.button.button ) of 
					SDL_BUTTON_LEFT: LeftMousePressed:=true;
					SDL_BUTTON_RIGHT: RightMousePressed:=true;
				end;		
		
	    end;
	    SDL_MOUSEMOved: begin
	        MouseMoved:=true;
	     //export data
	    end;
{

        joyButton:begin
           //getButton
           //export data
        end;
        
	    joyInput,joyHAT: begin
	     //get axis - deadzone
	     //note: doesnt account for drift in sticks, thats a hw bug
	     //export data
	    end;
}
	    //I GOT THIS. YOU handle the input.

        SDL_WINDOWEVENT: begin

                           case event^.window.event of
                             SDL_WINDOWEVENT_MINIMIZED: begin
                                minimized:=true; 
                                paused:=true;
                             end;
                             SDL_WINDOWEVENT_MAXIMIZED: begin
                                minimized:=false; 
                                paused:=false;
                                //redraw window
								{$ifdef cpu64}
									SDL_RenderPresent(renderer);
								{$endif}
								{$ifdef cpu32}
									SDL_Flip;
								{$endif}
                             end;
                             SDL_WINDOWEVENT_ENTER: begin
                             //same routines from 1.2 -> 2.0
                                   SDL_ShowCursor(SDL_DISABLE);
                             end;
                             SDL_WINDOWEVENT_LEAVE: begin
                                   SDL_ShowCursor(SDL_ENABLE);
                             end;
                            //AMIGA has this issue as well. Both with init, and rePaint(we got buried under other windows)
                             SDL_WINDOWEVENT_SHOWN: begin //fix a nasty bug on init
								paused:=false;
								//redraw window
								{$ifdef cpu64}
									SDL_RenderPresent(renderer);
								{$endif}
								{$ifdef cpu32}
									SDL_Flip;
								{$endif}
                             end;

							//we got overlapped by something, redraw or we will look like shit.
                             SDL_WINDOWEVENT_EXPOSED: begin
								paused:=false;
								//redraw window
								{$ifdef cpu64}
									SDL_RenderPresent(renderer);
								{$endif}
								{$ifdef cpu32}
									SDL_Flip;
								{$endif}

                             end;
                             //routine,program init, or exposed while running
                             SDL_WINDOWEVENT_HIDDEN: begin
								paused:=true;
								//redraw window
								{$ifdef cpu64}
									SDL_RenderPresent(renderer);
								{$endif}
								{$ifdef cpu32}
									SDL_Flip;
								{$endif}

                             end;


                             SDL_WINDOWEVENT_CLOSE: begin
                                quit:=true;
                                break; //leave the loop
                             end;

                           end; //case WindowEvent
                           
        end; //SDL_windowEvent
        //can be triggered internally, or via "window killer keys"
        SDL_QUIT: begin
            paused:=true;
            quit:=true;
            break;
            //closegraph(): is below.
        end;
        
      end;  //case
   
   end; //input loop
   
  end; //mainloop
  //if we ever exit...
  closegraph;
end;

// from wikipedia(untested)
procedure RoughSteinbergDither(filename,filename2:string);

var
	pixel:array [0..MaxX,0..MaxY] of DWord;  //The Surface data- this coul dbe used onscreen, just dont modify the pixels "in flight". Try not to assume resolution, either.
	oldpixel,newpixel,quant_error:DWord;
	file1,file2:file;
    Buf : Array[1..4096] of byte;


begin
    assign(file1,filename);
    assign(file2,filename2);

    blockread(file1,buf,sizeof(file));

  while Y<MaxY do begin
	 while x<MaxX do begin

      oldpixel  := pixel[x,y];
      newpixel  := round(oldpixel mod 256);
      pixel[x,y]  := newpixel;
      quant_error  := oldpixel - newpixel;

      pixel[x + 1,y]:= longword(pixel[x + 1,y] + quant_error * 7 mod 16);
      pixel[x - 1,y + 1] := longword(pixel[x - 1,y + 1] + quant_error * 3 mod 16);
      pixel[x,y + 1] := longword(pixel[x,y + 1] + quant_error * 5 mod 16);
      pixel[x + 1,y + 1] := longword(pixel[x + 1,y + 1] + quant_error * 1 mod 16);

    end;
   end;
     blockwrite(file2,buf,sizeof(file));

     close(file1);
	 close(file2);
end;


{

Got weird fucked up c boolean evals? (JEEZ theyre a BITCH to understand....)
  wonky "what ? (eh vs uh) : something" ===>if (evaluation) then (= to this) else (equal to that)
(usually a boolean eval with a byte input- an overflow disaster waiting to happen)

such oddly is the case with xlib for X11. The boolean gives a negative false, not zero and one.
(Thats a disaster waiting to happen)
}


{

PageFlip is being called automatically at minimum (screen_refresh) as an interval.

HOWEVER--
        DO NOT OVER RENDER. It will crash the best of systems.
        AND THE FLICKER RATE WILL MAKE YOU DIZZY. Use callback (timers) instead.
}


procedure lock;
begin
    if SDL_MustLock(MainSurface)=true then
        SDL_LockSurface(Mainsurface);
    //else exit: no locking needed
end;


procedure unlock;
begin
    if SDL_MustLock(MainSurface)=true then
           SDL_UnlockSurface(Mainsurface);
    //else exit: no UNlocking needed
end;

//depending on how you call or if "MainSurface" isnt what you want.
procedure lock(someSurface:PSDL_Surface); overload;
begin
    if SDL_MustLock(someSurface)=true then
        SDL_LockSurface(someSurface);
    //else exit: no locking needed
end;


procedure unlock(someSurface:PSDL_Surface); overload;
begin
    if SDL_MustLock(someSurface)=true then
           SDL_UnlockSurface(someSurface);
    //else exit: no UNlocking needed
end;

//BGI spec
procedure clearDevice;

begin
//dont re-create the wheel, here
  {$ifdef cpu32}
    SetBGColor(MainSurface,_fgcolor,true);
    //since we didnt specify which color, use the current one.
    //assume we want to obliterate the entire screen.

    SDL_FillRect(0,0,MaxX,MaxY);
    {$endif}
    SDL_RenderClear(renderer,_fgcolor);
end;

{
BUG: SDL2 returns Nothing. clean procedural execution(open the GC)

    reason why is that you are drawing to textures, there is no MAIN surface.

    -while I can provide one, it is up to YOU to copy it to a Texture, then RenderPresent it.
    You create Textures(or copy from surfaces), load them directly, or use directRender Calls(SDL_RenderClear/SDL_FillRect,etc)
    since its impossible to know, dont return anything.
    
    DO set the GC pointer(for export)- or youll have nothing to render to.(cpu doesnt presume to know anything)
}

//NEW: do you want fullscreen or not?

//returns MainSurface
{$ifdef cpu32}
    function initgraph(graphdriver:graphics_driver; graphmode:graphics_modes; pathToDriver:string; wantFullScreen:boolean):PSDL_Surface;
{$endif}

//returns nil
{$ifdef cpu64}
    procedure initgraph(graphdriver:graphics_driver; graphmode:graphics_modes; pathToDriver:string; wantFullScreen:boolean);
{$endif}

//This code only works a certain way- thats why languages "lacking globals" cannot compile this code.

//only return a renderer or mainSurface, the GC.

// **SHOULD** BE EXPORTING THE GC pointer by default.
//This eliminates problems later on when using units instead of "raw code" in applications.

//If surface is Nil, we FAILED. ABORT!

var
	i:integer;
	_initflag,_imgflags:longword; //PSDL_Flags?? no such beast
    iconpath:string;
    imagesON,FetchGraphMode:integer;
	mode:PSDL_DisplayMode;
  //  AudioSystemCheck:integer;

begin

  if LIBGRAPHICS_ACTIVE then begin
        LogLn('Initgraph: Graphics already active. Exiting.');
    end;
    exit;
  end;
  pathToDriver:='';  //unused- code compatibility

  case(graphmode) of

{
         we have full frame LFB now- planar storage doesnt make sense, is very bit-wise...
         -and very hard to do animations with.

         bitshifting is significantly faster than math operations, however. Not everyone took this into account.

         SDL becomes slow on put/getpixel ops due to repeated "one shot" operations instead of batches(buffered ops/surfaces)
         each get/putPixel operation *may* have to shift colors- AND was slowed down further by a (now removed) Pixel Mask(BGI).
		 (this was supposed to be only for lines and pen strokes)

         Hardware surfaces add wait times during copy operations between graphics hardware and CPU memory.
         ** SDL1 MAY NOT SUPPORT HW Surfaces on newer hardware**

         Despite the GPU being several orders (10x?) of magnitude faster than the CPU,GPU ops must also be "byte aligned".
         -In this case, 32 bits.

         How 24bit is natively supported, nobody will ever know. Probably like I do 15bit modes.

         Despite rumors-
                CGA is 4COLOR, 4Mode Graphics. 16 color mode is "extremely low resolution" on CGA systems.
                EGA and VGA use 16 color modes.

        You probably remember some text mode hack- or use of text mode symbols, like MegaZuex (openmzx) uses.

		bpp is always set to 8 on lower bitdepths, MaxColors variable checks the actual depth(SDL bug workaround).
		SDL supports these "LSB modes" but wont allow get/putpixel ops in these modes(why?).
MaxColors needs to be an ASN number(big headache)

}

		//standardize these as much as possible....

		//lo-res modes-try using SDL_SOFTSTRETCH()

		//MaxColors needs to be removed or altered for ASN number system.


		//CGA is WEIRD!

		//CGALo
		m160x100x16:begin //truely 80x25 w ext ascii(cp850) graphics chars used instead.
            MaxX:=160;
			MaxY:=100;
			bpp:=4; //no SDL doesnt like 4LSB modes..used internally. In THAT case, use an 8 bit palette, only use/setup the first 16.
			MaxColors:=16;
            NonPalette:=false;
		    TrueColor:=false;
		    //some of these its weird, 4x3 unless otherwise specified
            XAspect:=4;
            YAspect:=3;
		end;

		//CGAMed - there are 4 viable palettes in this mode. assign accordingly
		m320x200x4a:begin
		    MaxX:=320;
			MaxY:=200;
			bpp:=4;
			MaxColors:=4;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
            PaletteNum:=1;
		end;

		//CGAMed2 - there are 4 viable palettes in this mode
		m320x200x4b:begin
		    MaxX:=320;
			MaxY:=200;
			bpp:=4;
			MaxColors:=4;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
            PaletteNum:=2;
		end;

		//CGAMed3 - there are 4 viable palettes in this mode
		m320x200x4c:begin
		    MaxX:=320;
			MaxY:=200;
			bpp:=4;
			MaxColors:=4;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
            PaletteNum:=3;
		end;

		//CGAHi -not B/W perse, just one crayon in use with a dark background(fixed felt)
		m640x200x2:begin
		    MaxX:=640;
			MaxY:=200;
			bpp:=1;
			MaxColors:=1; //cant change the background. :-P
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

         //I added this in, its not spec.

		//CGAHi -B/W
		m640x200x2BW:begin
		    MaxX:=640;
			MaxY:=200;
			bpp:=1;
			MaxColors:=2; //can change the background.
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		//EGA
		m320x200x16:begin
		    MaxX:=320;
			MaxY:=200;
			bpp:=4;
			MaxColors:=16;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		//VGALo
		m320x200x256:begin
			MaxX:=320;
			MaxY:=200;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		//EGAHi (or VGAMed)
		m640x200x16:begin
			MaxX:=640;
			MaxY:=200;
			bpp:=4;
			MaxColors:=16;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		//VGAHi
		m640x480x16:begin
			MaxX:=640;
			MaxY:=480;
			bpp:=4;
			MaxColors:=16;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		m640x480x256:begin
		    MaxX:=800;
			MaxY:=600;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		m640x480x32k:begin
		    MaxX:=640;
			MaxY:=480;
			bpp:=15;
			MaxColors:=32768;
            NonPalette:=True;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		m640x480x64k:begin
		    MaxX:=640;
			MaxY:=480;
			bpp:=16;
			MaxColors:=65535;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		m640x480xMil:begin
		    MaxX:=640;
			MaxY:=480;
			bpp:=24;
            NonPalette:=True;
		    TrueColor:=True;
            XAspect:=4;
            YAspect:=3;
		end;

		//SVGA (and VESA modes)
		m800x600x16:begin
            MaxX:=800;
			MaxY:=600;
			bpp:=4;
			MaxColors:=16;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

        //typically why you cant set below this on recent hardware?? modes below this are not supported. more particularly bpp 24/32 only is used(GL is why).
		m800x600x256:begin
            MaxX:=800;
			MaxY:=600;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;


		m800x600x32k:begin
           MaxX:=800;
		   MaxY:=600;
		   bpp:=15;
		   MaxColors:=32768;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=4;
           YAspect:=3;
		end;
		m800x600xMil:begin
           MaxX:=800;
		   MaxY:=600;
		   bpp:=24;
           NonPalette:=true;
		   TrueColor:=true;
           XAspect:=4;
           YAspect:=3;
		end;

		m1024x768x16:begin
           MaxX:=1024;
		   MaxY:=768;
		   bpp:=8;
		   MaxColors:=16;
           NonPalette:=false;
		   TrueColor:=false;
           XAspect:=4;
           YAspect:=3;
		end;

		m1024x768x256:begin
            MaxX:=1024;
			MaxY:=768;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;

		end;

		m1024x768x32k:begin
           MaxX:=1024;
		   MaxY:=768;
		   bpp:=15;
		   MaxColors:=32768;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=4;
           YAspect:=3;

		end;

		m1024x768x64k:begin
           MaxX:=1024;
		   MaxY:=768;
		   bpp:=16;
		   MaxColors:=65535;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=4;
           YAspect:=3;
		end;

		m1024x768xMil:begin
           MaxX:=1024;
		   MaxY:=768;
		   bpp:=24;
           NonPalette:=true;
		   TrueColor:=true;
           XAspect:=4;
           YAspect:=3;
		end;


		m1280x720x256:begin
            MaxX:=1280;
			MaxY:=720;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=16;
            YAspect:=9;

		end;

		m1280x720x32k:begin
          MaxX:=1280;
		   MaxY:=720;
		   bpp:=15;
		   MaxColors:=32768;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=16;
           YAspect:=9;

		end;

		m1280x720x64k:begin
           MaxX:=1280;
		   MaxY:=720;
		   bpp:=16;
		   MaxColors:=65535;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=16;
           YAspect:=9;

		end;

		m1280x720xMil:begin
		   MaxX:=1280;
		   MaxY:=720;
		   bpp:=24;
           NonPalette:=true;
		   TrueColor:=true;
           XAspect:=16;
           YAspect:=9;

		end;

		m1280x1024x16:begin
			MaxX:=1280;
			MaxY:=1024;
			bpp:=8;
			MaxColors:=16;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;
		end;

		m1280x1024x256:begin
            MaxX:=1280;
			MaxY:=1024;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=4;
            YAspect:=3;

		end;

		m1280x1024x32k:begin
           MaxX:=1280;
		   MaxY:=1024;
		   bpp:=15;
		   MaxColors:=32768;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=4;
           YAspect:=3;

		end;

		m1280x1024x64k:begin
           MaxX:=1280;
		   MaxY:=1024;
		   bpp:=16;
		   MaxColors:=65535;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=4;
           YAspect:=3;

		end;

		m1280x1024xMil:begin
           MaxX:=1280;
		   MaxY:=1024;
		   bpp:=24;
           NonPalette:=true;
		   TrueColor:=true;
           XAspect:=4;
           YAspect:=3;

		end;

		m1366x768x256:begin
            MaxX:=1366;
			MaxY:=768;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=16;
            YAspect:=9;

		end;

		m1366x768x32k:begin
           MaxX:=1366;
		   MaxY:=768;
		   bpp:=15;
		   MaxColors:=32768;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=16;
           YAspect:=9;

		end;

		m1366x768x64k:begin
           MaxX:=1366;
		   MaxY:=768;
		   bpp:=16;
		   MaxColors:=65535;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=16;
           YAspect:=9;

		end;

		m1366x768xMil:begin
           MaxX:=1366;
		   MaxY:=768;
		   bpp:=24;
           NonPalette:=true;
		   TrueColor:=true;
           XAspect:=16;
           YAspect:=9;

		end;

		m1920x1080x256:begin
            MaxX:=1920;
			MaxY:=1080;
			bpp:=8;
			MaxColors:=256;
            NonPalette:=false;
		    TrueColor:=false;
            XAspect:=16;
            YAspect:=9;

		end;

		m1920x1080x32k:begin
		   MaxX:=1920;
		   MaxY:=1080;
		   bpp:=15;
		   MaxColors:=32768;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=16;
           YAspect:=9;

		end;

		m1920x1080x64k:begin
		   MaxX:=1920;
		   MaxY:=1080;
		   bpp:=16;
		   MaxColors:=65535;
           NonPalette:=true;
		   TrueColor:=false;
           XAspect:=16;
           YAspect:=9;

		end;

		m1920x1080xMil:begin
 	       MaxX:=1920;
		   MaxY:=1080;
		   bpp:=24;
           NonPalette:=true;
		   TrueColor:=true;
           XAspect:=16;
           YAspect:=9;

		end;

  end;{case}

//once is enough with this list...its more of a nightmare than you know.


//this part is like a "LD_PRELOAD" hack....it literally adds sdl "loading variables" (to the binary), on the fly.

{$IFDEF mswindows}
	SDL_putenv('SDL_VIDEODRIVER=directx'); //Use DirectX, dont use GDI
	SDL_putenv('SDL_AUDIODRIVER=dsound'); //Fuck it, use direct sound, too.
    ForceSWSurface:=False;

    //else  ForceSWSurface:=true;

{$ENDIF}

{$IFDEF hasamiga}
	SDL_putenv('SDL_VIDEODRIVER=cgx'); //cyberGFX
	SDL_putenv('SDL_AUDIODRIVER=ahi'); //AHI soundSystem - ac97 or ab128
    ForceSWSurface:=False;

    //else  ForceSWSurface:=true;

{$ENDIF}

{
SDL will compile but you need CW to build the app. Unlikely that youll be using "macshell" to invoke a UI app.

IFDEF mac
	SDL_putenv('SDL_VIDEODRIVER=DSp'); //DrawSprockets- I chose this on purpose. Dig for the OS9 Extensions.
	SDL_putenv('SDL_AUDIODRIVER=sndmgr'); //The OLD Mac Audio SubSystem
    //force software surfaces on older hardware
    ForceSWSurface:=True;
ENDIF

}

{$IFDEF darwin}
	SDL_putenv('SDL_VIDEODRIVER=quartz'); //Quartz2D
	SDL_putenv('SDL_AUDIODRIVER=coreaudio'); //The new OSX Audio Subsystem
    ForceSWSurface:=False;
{$ENDIF}

{$IFDEF unix}
   {$IFNDEF DARWIN} //and NOT OSX
       //its x11 ... or wayland.
	   //X11 might have HW acceleration, except with older hardware
	   SDL_putenv('SDL_VIDEODRIVER=x11'); //GLX(GL) contexts are requested THRU x11..
	   SDL_putenv('SDL_AUDIODRIVER=pulse');
       ForceSWSurface:=False;
    {$ENDIF}
{$endif}

{ not available except on very old hardware(some units dont even build):

	   SDL_putenv('SDL_VIDEODRIVER=svgalib'); //svgalib or Directfb??
	   SDL_putenv('SDL_AUDIODRIVER=alsa'); //Guessing here- should be available(has been for awhile).
       ForceSWSurface:=True;
}

   //"usermode" must match available resolutions etc etc etc
   //this is why I removed all of that code..."define what exactly"??

  //attempt to trigger SDL...on most sytems this takes a split second- and succeeds.
  //at minimum setup a timer (TTimer?) and Video callback
  _initflag:= SDL_INIT_VIDEO or SDL_INIT_TIMER;

  //keyb and mouse are handled by default, joystick interface changes in version 2-
  // as does physical connection(joyport or parrallel -to USB).
  if WantsJoyPad then _initflag:= SDL_INIT_VIDEO or SDL_INIT_TIMER or SDL_INIT_JOYSTICK;

  //audio on SDL1 isnt too bad, but use libuos instead


//untested(audio/net):
//  if WantsAudioToo then _initflag:= SDL_INIT_VIDEO or SDL_INIT_AUDIO or SDL_INIT_TIMER;
//  if WantsJoyPad and WantsAudioToo then _initflag:= SDL_INIT_VIDEO or SDL_INIT_AUDIO or SDL_INIT_TIMER or SDL_INIT_JOYSTICK;
//  if WantInet then _initflag:=SDL_Init_Net;


//turn off the parachute!!(SDL2-like)
{$ifdef cpu32}
    _initflag:= _initflag or SDL_INIT_NOPARACHUTE;
{$edif}

  //uses either version:
  if ( SDL_Init(_initflag) < 0 ) then begin
     //we cant speak- write something down.

        LogLn('InitGraph Failed to Launch SDL. **GENERAL ERROR**');
        LogLn(SDL_GetError);
     //if we cant init- dont bother with dialogs.
     LIBGRAPHICS_ACTIVE:=false;
     //reset exitproc?? good idea.

     halt(1); //the other routines are now useless- since we didnt init- dont just exit the routine, DIE instead. closegraph is useless.
  end;


{
 im going to skip the RAM requirements code and instead haarp on proper rendering requirements.
 note that a 12 yr old notebook-try as it may- might not have enough vRAM to pull things off.
 can you squeeze the code to fit into a 486??---you are pushing it.

}

  NoGoAutoRefresh:=false;
  LIBGRAPHICS_INIT:=true;
  LIBGRAPHICS_ACTIVE:=false;

//we do it this way because we might already be active- why duplicate code?

//sets up mode- in standard "BGI" fashion.
  SetGraphMode(Graphmode,wantFullScreen);


//lets get the current refresh rate and set a screen timer to it.
// we cant fetch this from X11? sure we can.
{

  mode^.format := SDL_PIXELFORMAT_UNKNOWN;
  mode^.w:=0;
  mode^.h:=0;
  mode^.refresh_rate:=0;
  mode^.driverdata:=Nil;
  //for physical screen 0 do..

  The SDl equivalent error of:
   		attempting to probe VideoModeInfo block (VESA)


  if(SDL_GetDisplayMode(0,0, mode) <> 0) then begin
			LogLn('Cant get current video mode info. Non-critical error.');
  end;

  //dont refresh faster than the screen.
  if (mode^.refresh_rate > 0)  then
     flip_timer_ms := longint(mode^.refresh_rate)
  else
     flip_timer_ms := 17; //60fps = ~17ms

--from epiktimer:

  AddTimer(flip_timer_ms, @videoCallback);

}

  //CantDoAudio:=false;
    //prepare mixer
{
  if WantsAudioToo then begin

  end;

}

  //initialization of TrueType font engine (SDLv2 only?)
  {$ifdef cpu64}
  if TTF_Init = -1 then begin

    if IsConsoleInvoked then LogLn('I cant engage the font engine, sirs. Likely out of RAM/VRAM.');
	closegraph;
  end;

  {$endif}

//set some sane default variables
  _fgcolor := $FFFFFFFF;	//Default drawing color = white (15)
  _bgcolor := $000000FF;	//default background = black(0)
  someLineType:= NormalWidth;


  paused:=false; //enable the input handler- and start rendering.

  where.X:=0;
  where.Y:=0;
  //dont just set variables, "clear the output window".

  {$ifdef cpu32}
    Rect^.x:=0;
    Rect^.y:=0;
    Rect^.w:=MaxX;
    Rect^.h:=MaxY;
    SDL_FillRect(MainSUrface,Rect,$000000ff);
    SDL_Flip;
  {$endif}

  {$ifdef cpu64}
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
  {$endif}

//set some sensible input specs

  //SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
  //SDL-ShowMouse(False);

  //Check for joysticks
  if WantsJoyPad then begin

    if( SDL_NumJoysticks < 1 ) then begin
        if IsConsoleInvoked then
			LogLn( 'Warning: No joysticks connected!' ); //non-critical, just no "joy input" to handle.

    end else begin //Load joystick
	    gGameController := SDL_JoystickOpen( 0 );

        if( gGameController = NiL ) then begin
		    	LogLn( 'Warning: Unable to open game controller! SDL Error: '+ SDL_GetError);
                LogLn(SDL_GetError);
            noJoy:=true;
        end;
        noJoy:=false;
    end;
  end; //Joypad

 //USE ONLY FOR DEBUG The GC - this is what is exported to your "app" or "game"- from this library.
 //(SDLv1):
 //   LogLn(longword(@MainSurface));

 //SDLv2:
 // LogLn(longword(@Renderer));

 //if at any time either is "Nil"-> exit. YOu need a "watchdog",however, to check.


end; //initgraph


procedure DrawDoubleLinedWindowDialog(Rect:PSDL_Rect);

var
    UL,UR,LL,LR:Points; //PolyPts
    ShrunkenRect,ShurunkenRect2,NewRect:PSDL_Rect;

begin
 //   Tex:=NewTexture;
{$ifdef cpu64}
    SDL_SetViewPort(Rect);
{$endif}
{$ifdef cpu32}
    SetViewport(Rect); //my routine, abstracted BGI
{$endif}

    //come in by 2px, drawRect, come in by 2px, drawrect, come in by 2px- setViewport (I trapped you twice in a box..)
    UL.x:=x+2;
    UL.y:=y+2;
    UR.x:=w-2;
    LR.y:=h-2;
    NewRect:=(UL.x,UL.y,UR.x,LR.y); //same in rect format

	{$ifdef cpu64}
    SDL_RenderDrawRect(NewRect); //draw the box- inside the new "window" shrunk by 2 pixels (4-6 may be better)
	{$endif}
	{$ifdef cpu32}
	Rectangle(NewRect);
    {$endif}

    //shink available space and...
    //do this again- further in
    UL.x:=x+4;
    UL.y:=y+4;
    UR.x:=w-4;
    LR.y:=h-4;

    ShrunkenRect:=(UL.x,UL.y,UR.x,LR.y); //same in rect format


	{$ifdef cpu64}
    SDL_RenderDrawRect(ShrukenRect); //draw the box- inside the new "window" shrunk by 2 pixels (4-6 may be better)
	{$endif}
	{$ifdef cpu32}
	Rectangle(ShrukenRect);
    {$endif}

    UL.x:=x+6;
    UL.y:=y+6;
    UR.x:=w-6;
    LR.y:=h-6;
    ShrunkenRect2:=(UL.x,UL.y,UR.x,LR.y); //same in rect format
 {$ifdef cpu64}
    SDL_SetViewPort(ShrunkenRect);
{$endif}
{$ifdef cpu32}
    SetViewport(ShrunkenRect); //my routine, abstracted BGI
{$endif}
end;

procedure DrawSingleLinedWindowDialog(Rect:PSDL_Rect);

var
    UL,UR,LL,LR:Points; //see header file(polypts is ok here)
    ShrunkenRect,NewRect:PSDL_Rect;

begin
 //   Tex:=NewTexture;
{$ifdef cpu64}
    SDL_SetViewPort(Rect);
{$endif}
{$ifdef cpu32}
    SetViewport(Rect); //my routine, abstracted BGI
{$endif}

    //corect me if Im off- this is guesstimate math here, not actual.
    //the corner co ords
    UL.x:=x+2;
    UL.y:=y+2;
    UR.x:=w-2;
    LR.y:=h-2;
    NewRect:=(UL.x,UL.y,UR.x,LR.y); //same in rect format

	{$ifdef cpu64}
    SDL_RenderDrawRect(NewRect); //draw the box- inside the new "window" shrunk by 2 pixels (4-6 may be better)
	{$endif}
	{$ifdef cpu32}
	Rectangle(NewRect);
    {$endif}
    //do this again- further in (so we dont override the "window" decoration.
    //Theres a TVision/bgi demo of this, but not one for SDL.
    UL.x:=x+4;
    UL.y:=y+4;
    UR.x:=w-4;
    LR.y:=h-4;
    ShrunkenRect:=(UL.x,UL.y,UR.x,LR.y); //same in rect format

{$ifdef cpu64}
    SDL_SetViewPort(ShrunkenRect);
{$endif}
{$ifdef cpu32}
    SetViewport(ShrunkenRect); //my routine, abstracted BGI
{$endif}

end;
{

    See the file COPYING.FPC, included in the fpc distribution,
    for details about the copyrights herein of this block of code.

    "Licensed GPL v2- or AT MY OPTION, greater."

    Copyright (c) 1999-2000 by the Free Pascal development team
    --and ignored for the past 20 years. This is AGES AGO.

    I think someone lost thier "VesaModeInfo block" somewhere down Route 66....
    I found it.

const
       maxsmallint = high(smallint); //Lets measure the size of an ant, here....for no reason... (range=256)

       // Bar3D constants
       TopOn = true;
       TopOff = false;

       // fill pattern for Get/SetFillStyle:
       EmptyFill      = 0;
       SolidFill      = 1;

       LineFill       = 2;
       LtSlashFill    = 3;
       BkSlashFill    = 5;

       XHatchFill     = 8;
       WideDotFill    = 10;
       CloseDotFill   = 11;

       // SetTextJustify constants
       LeftText   = 0;
       CenterText = 1;
       RightText  = 2;

       m512x384   = detectMode +  4;  //mac resolution
       m832x624   = detectMode + 11;  //mac resolution

       FillSettingsType = record
             pattern : FilPatternType;
             color : ColorType;
       end;

       ArcCoordsType = record
             x,y : smallint;
             xstart,ystart : smallint;
             xend,yend : smallint;
       end;

  const

//predefined blits --need some tools for this.

       fillpatternType : array[0..12] of BlitPatternType = (
           transparent background color
           opaque(solidFill) foreground color
            horizontal lines
            slashes
            thick slashes
            thick backslashes
            backslashes
            small boxes
            rhombus
            wide points
            dense points
          );


TYPE
       YRadius:word; stAngle,EndAngle: word; fp: PatternLineProc);

       // this routine is used for FloodFill - it returns an entire
      // screen scan line with a word for each pixel in the scanline.

       getscanlineproc = procedure (X1, X2, Y : smallint; var data);


 - DrawPoly XORPut mode is not exactly the same as in
   the TP graph unit.

 AUTHORS:

   Gernot Tenchio      - original version
   Florian Klaempfl    - major updates
   Pierre Mueller      - major bugfixes
   Carl Eric Codere    - complete rewrite
   Thomas Schatzl      - optimizations,routines and
                           suggestions.
   Jonas Maebe         - bugfixes and optimizations

 Credits (external):

    Original FloodFill code by
        Menno Victor van der star
     (the code has been heavily modified)

Patched up for SDL  by Richard Jasmin

GVision adds:
    Bar,3DBar,statusBar

//These functions/proceedures WORK, however- may need modification for SDL.
}

function strf(l: longint): string;
begin
  str(l, strf)
end;

const
   StdBufferSize = 4096;   { Buffer size for FloodFill  32768?? }

var

//  LineInfo : LineSettingsType;
 // FillSettings: FillSettingsType;

  StartXViewPort: Word;
  StartYViewPort: word;
  ViewWidth : word;
  ViewHeight: word;

  ArcCall: ArcCoordsType;   { Information on the last call to Arc or Ellipse }

//done differently, extracted from SDL itself.

type
  PNewModelist = ^TNewModeList;
  TNewModeList = record
    Mode: PModeInfo;
    next: PNewModeList;
    internModeNumber: smallint;
  end;
  TNewModeInfo = record
    modeInfo: array[lowNewDriver..highNewDriver] of PNewModeList;
    loHiModeNr: array[lowNewDriver..highNewDriver] of record
      lo,hi: smallint;
    end;
  end;

var

  ModeList : PModeInfo;
  newModeList: TNewModeInfo;

  Procedure PutPixelClip(x,y: smallint);
  { for thickwidth lines, because they may call DirectPutPixel for coords }
  { outside the current viewport (bug found by CEC)                       }
  Begin
    If
       ((X >= StartXViewPort) And (X <= (StartXViewPort + ViewWidth)) And
        (Y >= StartYViewPort) And (Y <= (StartYViewPort + ViewHeight))) then
      Begin
        {$ifdef cpu32}
        SDL_PutPixel(MainSurface,x,y);
        {$endif}
        {$ifdef cpu64}
        SDL_PutPixel(renderer,x,y);
        {$endif}
      End
  End;

//put a line from current x,y to targetx,y
  procedure LineTo(x,y : smallint);

   Begin
     Line(CurrX, CurrY, X, Y);

//update X,Y
     CurrX := X;
     CurrY := Y;
   end;


  procedure GetLineSettings(var ActiveLineInfo : LineSettingsType);

   begin
    Activelineinfo:=Lineinfo;
   end;


  procedure SetLineStyle(LineStyle: word; Pattern: word; Thickness: word);

   var
    i: byte;
    j: byte;

   Begin
       LineInfo.Thickness := Thickness;
       LineInfo.LineStyle := LineStyle;

   end;

procedure GetViewSettings(var viewport : ViewPortType);
begin
  ViewPort.X1 := StartXViewPort;
  ViewPort.Y1 := StartYViewPort;
  ViewPort.X2 := ViewWidth + StartXViewPort;
  ViewPort.Y2 := ViewHeight + StartYViewPort;
  ViewPort.Clip := true;
end;

// Not sure if we need this anymore. FILLS.
function GetScanline (X1, X2, Y : word):PPixelData; //cant export an array. export  pointer to it.
  //adaptedfor SDL. used "for fills." see: "racing the beam" programming book.

  Var
    LongWordArray:array [0..MaxX] of SDL_Color;  //array of pixel data, based upon screen size
    PPixelData:^LongWordArray;
Begin

    begin
      For x:=X1 to X2 Do begin
        LongWordArray[x-x1]:=GetPixel(x, y);
        if x>MaxX or x>MaxViewPortX then exit;
       inc(x);
    end
    GetScanLine:=LongWordArray;
  End;


  Procedure GetArcCoords(var ArcCoords: ArcCoordsType);
   Begin
     ArcCoords.X := ArcCall.X;
     ArcCoords.Y := ArcCall.Y;
     ArcCoords.XStart := ArcCall.XStart;
     ArcCoords.YStart := ArcCall.YStart;
     ArcCoords.XEnd := ArcCall.XEnd;
     ArcCoords.YEnd := ArcCall.YEnd;
   end;

//start end and angles are in degrees of a circle (Trig)
  Procedure Arc(X,Y : smallint; StAngle,EndAngle,Radius: word);

   Begin
     Ellipse(X,Y,Radius,(longint(Radius)*XAspect) div YAspect),StAngle,Endangle,LineStyle);
   end;

 procedure FillEllipse(X, Y: smallint; XRadius, YRadius: Word);
  begin
    Ellipse(X,Y,XRadius,YRadius,0,360,LineStyle);
  end;

 procedure Sector(x, y: smallint; StAngle,EndAngle, XRadius, YRadius: Word);
  var
    TmpAngle : Word;
  begin
     { sector does not draw counter-clock wise }
     StAngle := StAngle mod 361;
     EndAngle := EndAngle mod 361;
     if Endangle < StAngle then
       Begin
         TmpAngle:=EndAngle;
         EndAngle:=StAngle;
         StAngle:=TmpAngle;
       end;

     ellipse(x,y,XRadius, YRadius, StAngle, EndAngle,Linestyle); //sector with patternLine by JM. The arc in a pie-pece.
     //but you still need two lines to make it a sector.

     Line(ArcCall.XStart, ArcCall.YStart, x,y);
     Line(x,y,ArcCall.Xend,ArcCall.YEnd);
  end;

  var
     FillSettingsPattern: array [0..7] of string;

//pattern: points to FIllPattern blit number

   procedure SetFillStyle(Pattern : smallint; Color: SDL_Color);

   begin

     if (PallettedMode=true) and  (Color > MaxColors) then
      begin
{$ifdef logging}
           logln('invalid fillstyle parameters');
{$endif logging}
           exit;
      end;

     FillSettingsPattern[0]:='';
     FillSettingsPattern[1]:='';
     FillSettingsPattern[2]:='';
     FillSettingsPattern[3]:='';
     FillSettingsPattern[4]:='';
     FillSettingsPattern[5]:='';
     FillSettingsPattern[6]:='';
     FillSettingsPattern[7]:='';

     FillSettings.Color := Color;

     currentBlit:=FillSettingsPattern[Pattern];
     //newSurface:=loadBitMap(CurrentBlit);

     //if sdl2: RenderCopy(Renderer,newSurface); -- rectSize=8x8, direction?
   end;


  procedure Bar(x1,y1,x2,y2:smallint);
  { Important notes for compatibility with BP:                           }
  {     - No contour is drawn for the lines(3d or rounded)  }

var
      y               : smallint;
      origcolor       : ColorType;
      origlinesettings: Linesettingstype;
      origwritemode   : smallint;

begin
     if y1>y2 then
       begin
          y:=y1;
          y1:=y2;
          y2:=y;
       end;
     if x1>x2 then
       begin
          y:=x1;
          x1:=x2;
          x2:=y;
       end;

     { All lines used are of this style }
     Lineinfo.linestyle:=solidln;
     Lineinfo.thickness:=normwidth;

     case Fillsettings.pattern of
//erase
     EmptyFill :
       begin
         Currentcolor:=_bgcolor;
           line(x1,x2,y,y2);
       end;

//override screen w fgcolor

     SolidFill :
       begin
         CurrentColor:=FillSettings.color;
              line(x1,x2,y,y2);
       end;
     else

//draw with a pattern

      Begin
        CurrentColor:=FillSettings.color;
//basically a fatPixel (Long) rect
          patternline(x1,x2,y,y2);

      end;
    end;
end;


procedure bar3D(x1, y1, x2, y2 : smallint;depth : word;top : boolean);
var

 origwritemode : smallint;
 OldX, OldY : smallint;
begin
  if x1 > x2 then
  begin
    OldX := x1;
    x1 := x2;
    x2 := OldX;
  end;
  if y1 > y2 then
  begin
    OldY := y1;
    y1 := y2;
    y2 := OldY;
  end;

  Bar(x1,y1,x2,y2);
  Rectangle(x1,y1,x2,y2);


  OldX := CurrentX;
  OldY := CurrentY;

//draw the top??

  if top then begin
    Moveto(x1,y1);
    Lineto(x1+depth,y1-depth);
    Lineto(x2+depth,y1-depth);
    Lineto(x2,y1);
  end;

// it has to be a 3d bar...

  if Depth <> 0 then
    Begin
      Moveto(x2+depth,y1-depth);
      Lineto(x2+depth,y2-depth);
      Lineto(x2,y2);
    end;

  CurrentX := OldX;
  CurrentY := OldY;

end;


//make this easy as Pi.

  procedure SetColor(Color: DWord);

   Begin
     _fgColor := Color;
   end;


  function GetColor: DWord;

   Begin
     GetColor := _fgColor;
   end;


  function GetMaxColor: DWord; 
  //were above 256 now...and waay off past jupiter , big!

   begin
      if not PalettedMode then exit; //only works with pallettes
      GetMaxColor:=MaxColor+1; { based on an index of zero so add one }
   end;

//track X,y- dont draw.

   Procedure MoveRel(Dx, Dy: smallint);
    Begin
     CurrentX := CurrentX + Dx;
     CurrentY := CurrentY + Dy;
   end;

   Procedure MoveTo(X,Y: word);
    Begin
     CurrentX := X;
     CurrentY := Y;
    end;


  Function GetMaxX: word;
   Begin
     GetMaxX := MaxX;
   end;

  Function GetMaxY: word;
   Begin
    GetMaxY := MaxY;
   end;

   Function GetDriverName: string;
    begin
      GetDriverName:=DriverName;
    end;


   procedure graphdefaults;

    var
     i: smallint;
   begin
     lineinfo.linestyle:=solidln;
     lineinfo.thickness:=normwidth;

     { By default, according to the TP prog's reference }
     { the default pattern is solid, and the default    }
     { color is the maximum color in the palette.       }

     fillsettings.color:=GetMaxColor;
     fillsettings.pattern:=solidfill;

     CurrentColor:=white;

     { Reset the viewport }
     StartXViewPort := 0;
     StartYViewPort := 0;
     ViewWidth := MaxX;
     ViewHeight := MaxY;

     { Reset CP }
     CurrentX := 0;
     CurrentY := 0;

     SetBGColor(Black);

     { normal write mode }
     CurrentWriteMode := CopyPut;

     { set font style }
     CurrentTextInfo.font := DefaultFont;
     CurrentTextInfo.direction:=HorizDir;
//zero the arcCall x,y structure. --why not?

   end;


  procedure GetAspectRatio(var Xasp,Yasp : word);
  begin
    XAsp:=XAspect;
    YAsp:=YAspect;
  end;

  procedure SetAspectRatio(Xasp, Yasp : word);
  begin
    Xaspect:= XAsp;
    YAspect:= YAsp;
  end;

  //requires 32bpp, for blends.

  //NO- do not have PutPixel call ME.

//Call me for batch ops instead. THIS IS WHERE BORLAND Fd up.
//Borland called ME everytime PutPIexel wanted to operate.

//Lines, polys, fills are ok- if buffered.

  //set the color, then draw.
//Or...
//"Blit" the already drawn image file instead. Its easier to load an inverted image(even by texture storing it), than to pixel-by-pixel flip them.
// What you can also do is "flip every pixel in a surface/texture", then blit/flip said texture. Be mindful of your timing if on the default VideoTimer interval.

  //Blitting is basically the same as PageFlipping. You are supposed to punch 'holes' in the image(transuceny/transparency with a BLIT. SDLv1 says use the magenta color, yet refernces unused AlphaBIts...very bad and confusing code.

  // JUST DONT SetWriteMode o a PERPixel basis with the renderer....or directly to the MainSurface(in batch mode/ fillDWord manner, anyway)
  //doing the op in memory(CPU)- or on any other surface,  is fine.

  procedure SetWriteMode(WriteMode : smallint);
  begin
     Case writemode of
       xorput: _fgcolor:=((_fgcolor mod _bgcolor) and (_fgcolor and _bgcolor)); //negativePut (fore=back)
       copyput: _fgcolor:= _fgcolor; //default
       orput: _fgcolor:= _fgcolor or _bgcolor;//multiply?
       andPut:_fgcolor:=_fgcolor and  _bgcolor; //add
       notPut:_fgcolor:=_bgcolor; //just dont put
     End;
  end;

  procedure SetWriteMode32(WriteMode : smallint; blendDegree:byte);
//change the alpha transparency A-bit. to something "not solid" after adjusting the writemode.

var
  somecolor:PSDL_Color;

   begin
     Case writemode of
       xorput: _fgcolor:=((_fgcolor mod _bgcolor) and (_fgcolor and _bgcolor));  //negativePut
       copyput: _fgcolor:= _fgcolor; //default
       orput: _fgcolor:= _fgcolor or _bgcolor;//multiply?
       andPut:_fgcolor:=_fgcolor and  _bgcolor; //add
       notPut:_fgcolor:=_bgcolor; //just dont put
     End;
     with BlendDegree do begin
            someColor:=DWordToRGBA(_fgcolor);
            somecolor^.A:=BlendDegree;
     end;
   end;


  procedure GetFillSettings(var Fillinfo:Fillsettingstype);
   begin
     Fillinfo:=Fillsettings;
   end;

  procedure DrawPoly(numpoints : word;var polypoints);

    type

      pt = array[0..8190] of PolyPoints;

    var
      i, j, LastPolygonStart: longint;
      Closing: boolean;
    begin
      if numpoints < 2 then
        begin
          exit;
        end;
      Closing := false;
      LastPolygonStart := 0;
      for i:=0 to numpoints-2 do begin
        { skip an edge after each 'closing' edge }
        if not Closing then
          line(pt(polypoints)[i].x,
               pt(polypoints)[i].y,
               pt(polypoints)[i+1].x,
               pt(polypoints)[i+1].y);

        { check if the current edge is 'closing'. This means that it 'closes'
          the polygon by going back to the first point of the polygon.
          Also, 0-length edges are never considered 'closing'. }
        if ((pt(polypoints)[i+1].x <> pt(polypoints)[i].x) or
            (pt(polypoints)[i+1].y <> pt(polypoints)[i].y)) and
            (LastPolygonStart < i) and
           ((pt(polypoints)[i+1].x = pt(polypoints)[LastPolygonStart].x) and
            (pt(polypoints)[i+1].y = pt(polypoints)[LastPolygonStart].y)) then
        begin
          Closing := true;
          LastPolygonStart := i + 2;
        end
        else
          Closing := false;
      end;
    end;


  procedure PieSlice(X,Y,stangle,endAngle:smallint;Radius: Word);
  begin
    Sector(x,y,stangle,endangle,radius,longint(((Radius)*XAspect) div YAspect));
  end;




// end fpc graphics drawing routines.

{

PixelTris are a smidge different.
PixelTris are caddy-corner Pixels(up or down diagonal depending on the direction of the tri.)

Instead of Line, use PutPixel here.
PixelTris are for "more precise rendering".

Plop-drop em randomly for a neat effect...
}

//if you want larger- supply 3 (x,y) points to the input and "draw lines" instead.
procedure UDPixelTriangle(x,y:word; PointsUp:boolean);
//starting at x,y give me a (pixel sized) tri pointing up/down

begin
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x,y);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x,y);		
		{$endif}
		if PointsUp then begin
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x-1,y+1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x-1,y+1);		
		{$endif}
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x+1,y+1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x+1,y+1);		
		{$endif}

		end else begin
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x-1,y-1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x-1,y-1);		
		{$endif}
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x+1,y-1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x+1,y-1);		
		{$endif}

		end;

end;

procedure LRPixelTriangle(x,y:word; PointsLeft:boolean);
//starting at x,y give me a (pixel sized) tri pointing left/right
begin
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x,y);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x,y);		
		{$endif}
		if PointsLeft then begin
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x+1,y+1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x+1,y+1);		
		{$endif}
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x+1,y-1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x+1,y-1);		
		{$endif}

		end else begin
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x-1,y+1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x-1,y+1);		
		{$endif}
		{$ifdef cpu32}
    		SDL_PutPixel(MainSurface,x-1,y-1);
		{$endif}
		{$ifdef cpu64}
    		SDL_PutPixel(Renderer,x-1,y-1);		
		{$endif}

end;


var
    FontPointer:PTTF_Font;


Proceedure closegraph;
var
	Killstatus,Die:cint;
    waittimer:integer;

//free only what is allocated, nothing more- then make sure pointers are empty.
begin

  LIBGRAPHICS_ACTIVE:=false;  //Unset the variable (and disable all of our other functions in the process)


  //if wantsInet then
//  SDLNet_Quit;

  if wantsJoyPad then begin
    SDL_JoystickClose( gGameController );
    FreeAndNil(gGameController);
  end;


  if WantsAudioToo then begin
    Mix_CloseAudio; //close- even if playing

    if chunk<>Nil then
        Mix_FreeChunk(chunk);
    if music<> Nil then
        Mix_FreeMusic(music);

    Mix_Quit;
  end;

   SDL_RemoveTimer(video_timer_id);
   video_timer_id := 0;

   SDL_DestroyMutex(eventLock);
    FreeAndNil(eventLock);

   SDL_DestroyCond(eventWait);
    FreeAndNil(eventWait);

  if (TextFore <> Nil) then begin
	 FreeAndNil(TextFore);
  end;
  if (TextBack <> Nil) then begin
	 FreeAndNil(TextBack);
  end;
{$ifdef cpu64}
  TTF_CloseFont(FontPointer);
  TTF_Quit;

  //its possible that extended images are used also for font datas...
  if wantsFullIMGSupport then
     IMG_Quit;
{$endif}

  die:=9; //signal number 9=kill
  //Kill child if it is alive. we know the pid since we assigned it(the OS really knows it better than us)

  //we are stuck in a loop
  //we can however, trip the loop to exit...

  exitloop:=true;
  Killstatus:=FpKill(EventThread,Die); //send signal (DIE) to thread
  FreeAndNil(Event);

  //free viewports

  x:=8;
  repeat
  //FreeAndNil(x);
	if (Textures[x]<>Nil) then
		SDL_DestroyTexture(Textures[x]);
	Textures[x]:=Nil;
    dec(x);
  until x=0;


  if (MainSurface<> Nil) then begin
	SDL_FreeSurface( MainSurface );
	MainSurface:= Nil;
  end;

{$ifdef cpu64}
  if (Renderer<> Nil) then begin
    Renderer:= Nil;
	SDL_DestroyRenderer( Renderer );
  end;
{$endif}

  if (Window<> Nil) then begin
	Window:= Nil;
  	SDL_DestroyWindow ( Window );
  end;

  //test first if fin doesnt work: exitproc:=oldexitproc;
  SDL_Quit;  //unless you want to mimic the last doom screen here...usually were done....

  if (IsConsoleInvoked) or (IsVTerm) then begin //if "one" or "the other", dont OR the result.
         textcolor(7); //..reset standards...
         clrscr; //text clearscreen
         writeln;
  end;
  LogLn('lazarus graphics unit exited successfully.');
  StopLogging;  //save the file.
  halt(0); //nothing special, just bail gracefully.

end;


// is it CurrX,CurrY or where.X and where.y?

function GetX:word;
begin
  x:=where.X;
end;

function GetY:word;
begin
  y:=where.Y;
end;

function GetXY:longint;
//This is the 1D location in the 2D graphics area(yes, its weird)

begin
  x:=where.X;
  y:=where.Y;
  GetXY := (y * (MainSurface^.pitch mod (sizeof(byte)) ) + x); //word address, the 1D location in RAM/VRAM of the pixel in question. Not in X,Y (2D) format.
 //(Dont ask...)
end;


function getgraphmode:string;
//determine what mode we are in based on bpp, MaxX and MaxY.
var
    bppstring:string;
    format:PSDL_PixelFormat;
    MaxMode:integer;
    tempMaxX,tempMaxY:word;
    findbpp:byte;
    thismode:PSDL_DisplayMode;

begin


   if LIBGRAPHICS_ACTIVE then begin
		x:=0;

        SDL_GetCurrentDisplayMode(0, thismode); //go get w,h,format and refresh rate..
        case (bpp) of
			1,4,8: if maxColors<=256 then format:=SDL_PIXELFORMAT_INDEX8;
			15: format:=SDL_PIXELFORMAT_RGB555;
			//we assume on 16bit that we are in 565 not 5551, we should not assume
			16: format:=SDL_PIXELFORMAT_RGB565;
			24: format:=SDL_PIXELFORMAT_RGB888;
			{$ifdef cpu64}
				32: format:=SDL_PIXELFORMAT_RGB8888;
			{$endif}
        end;
        tempMaxX:=MainSurface^.w;
		tempMaxY:=MainSurface^.h;

        //now we should have the bpp, MAxX and MaxY values

            //peruse th emodeList for the mode we are looking for- either you found a match or you didnt.
			while (ModeList[x]< (Ord(High(Graphics_Modes))-1)) do begin

				if (tempMaxX=MaxX) and (tempMaxY=MaxY)  then begin
                	    if (MainSurface^.format^.BitsPerPixel<>bpp) then break;
                	    //typinfo shit:
			    	    getgraphmode:=GetEnumName(TypeInfo(Graphics_modes), Ord(x));
						exit;
				end;
				inc(x);

        end;

            LogLn('Cant find current mode in modelist.');
        end;
   end;

end;

procedure restorecrtmode; //wrapped closegraph function
begin
  if (not LIBGRAPHICS_ACTIVE) then LogLn('you didnt call initGraph yet...try again?') ;
  closegraph;
end;


{

Blitter, Blittering, Blits, BitBlit (or RenderCopy- with a maybe on freeSurface..)

This code is REQUIRED for proper GAMES Programming, Im not there yet.
A lot of variables have to be end user supplied -and checked.

(Yes, There are some parts of SDL hat you need to know, but the BGI Interface API greatly shortens the learning curve,
especially if you can see what Im doing here...)


Wikipedia-

A blitter is a circuit, sometimes as a coprocessor or a logic block on a microprocessor,
dedicated to the rapid movement and modification of data within a computer's memory.

A blitter can copy large quantities of data from one memory area to another relatively quickly,
and in parallel with the CPU, while freeing up the CPU's more complex capabilities for other operations.

A typical use for a blitter is the movement of a bitmap, such as windows and fonts in a graphical user interface
or images and backgrounds in a 2D computer game.

The name comes from the bit blit operation of the 1973 Xerox Alto, which stands for bit-block transfer.

A "blit operation" is more than a memory copy, because it can involve data that's not byte aligned
(hence the bit in bit blit), handling transparent pixels (pixels which should not overwrite the destination data),
and various ways of combining the source and destination data.

//NewLayer is a loaded (BMP) pointer (filled rect)

// SDL_BlitSurface(NewLayer, nil,MainSurface,nil);


procedure HowBlit(currentwriteMode);
    //perpixel anything is horrendously slow. use the "blitting cpu functions" to our advantage instead.

--shift the palette,stupid
    --apply the shifted palette to the blit
        BLIT
        RenderCopy
        reset the palette and Flip

        //xorBlit
        //andBlit
        //orBlit
        //notBlit(inverted colors)

SDL_ScrollX(Surface,DifX);
SDL_ScrollY(Surface,DifY);


procedure BatchRenderPixels;
//this is an example
var
  PolyArray=array of SDL_Point;

begin
  polyarray[1].x:=5
  polyarray[1].y:=3

  polyarray[2].x:=7
  polyarray[2].y:=7

  SDL_RenderDrawPoints( renderer,polyarray,2);
end;


}


function getdrivername:string;
begin
//not really used anymore-this isnt dos

   getdrivername:='Internal.SDL'; //be a smartASS
end;


Procedure DetectGraph(WantsFullScreen:boolean);
//likely you want FS...
begin

    if ForceSWSurface=false then begin
		if wantsFullsCreen=true then
		    sdlflags:= (SDL_SWSURFACE or SDLHWPALETTE or SDL_DOUBLEBUF or SDL_FULLSCREEN);
		else
			sdlflasgs:= (SDL_SWSURFACE or SDLHWPALETTE or SDL_DOUBLEBUF);
    end;
    if ForceSWSurface=true begin
		if wantsFullsCreen=true then
		    sdlflags:= (SDL_HWSURFACE or SDLHWPALETTE or SDL_DOUBLEBUF or SDL_FULLSCREEN);
		else
			sdlflasgs:= (SDL_HWSURFACE or SDLHWPALETTE or SDL_DOUBLEBUF);
    end;

{$ifdef cpu32}
    //sdl1 hack: use 0,0 to use the current mode size. Try to use bpp appropriate for user request. fake it otherwise.
		test:=SDL_SetVideoMode(0, 0, bpp, sdlflags);
        if test=Nil then begin //we tried an appropriate combo, it didnt work.
			//error out
	             LogLn('Error: ', SDL_GetError);
			closegraph;
	   end;
{$endif}

end; //detectGraph

procedure RemoveViewPort(windownumber:byte);
//the opposite of above...
//set the last window coords..(we might be trying to write to them)
//and redraw the prior window as if the new one was not there(not an easy task).
var
  ThisRect,LastRect:PSDL_Rect;

begin
 paused:=true;
   if windownumber=0 then begin
        LogLn('Attempt to remove non-existant viewport.');
    exit;
   end;
   if windownumber > 1 then begin
  		ThisRect^.X:=TexBounds[windownumber]^.X;
		ThisRect^.Y:=TexBounds[windownumber]^.Y;
	    ThisRect^.W:=TexBounds[windownumber]^.W;
	    ThisRect^.H:=TexBounds[windownumber]^.H;

        //get coords of the previous

        LastRect^.X:=TexBounds[windownumber-1]^.X;
 	    LastRect^.Y:=TexBounds[windownumber-1]^.Y;
 	    LastRect^.W:=TexBounds[windownumber-1]^.W;
	    LastRect^.H:=TexBounds[windownumber-1]^.H;

       //remove the viewport by removing the texture and redrawing the screen.
       //the problem with textures is that they are part of a one-way road. ARG!

        Surfaces[windownumber]:=nil; //Destroy the current Texture
{$ifdef cpu32}
        //blit the old data back
        SDL_Blit(MainSurface,Surfaces[windownumber-1],Nil,LastRect);
        SDL_Flip;
{$endif}

{$ifdef cpu64}

           Surfaces[windownumber-1]:= SDL_CreateRGBSurfaceWithFormatFrom(ScreenData, infoSurface^.w, infoSurface^.h, infoSurface^.format^.BitsPerPixel, (infoSurface^.w * infoSurface^.format^.BytesPerPixel),longword( infoSurface^.format));

  if (Surfaces[windownumber-1]= NiL) then begin
      LogLn('Couldnt create SDL_Surface from renderer pixel data. ');
      LogLn(SDL_GetError);
      exit;

  end;

        temptex:=SDL_CreateTextureFromSurface(Surfaces[windowsnumber-1]);
        RenderCopy(Renderer,tempTex);
        free(tempTex);)
        RenderPresent;
{$endif}

		ThisRect:=LastRect;
        dec(windownumber);

        Paused:=false;
        exit;
   end;
   //else: last window remaining
   Surfaces[1]:=nil;
   ThisRect:=LastRect;

   SDL_Blit(MainSurface,Surfaces[0],nil,LastRect);
   SDL_Flip; //and update back to the old screen before the viewports came here.

   LastRect^.X:=0;
   LastRect^.Y:=0;
   LastRect^.W:=MaxX;
   LastRect^.H:=MaxY;

   dec(windownumber);

   Paused:=false;
end;


//compatibility
//these were hooks to load or unload "driver support code modules" (mostly for DOS)

procedure InstallUserDriver(Name: string; AutoDetectPtr: Pointer);
begin
   LogLn('Function No longer supported: InstallUserDriver');
end;

procedure RegisterBGIDriver(driver: pointer);

begin
   LogLn('Function No longer supported: RegisterBGIDriver');
end;


//linestyle is: (patten,thickness) "passed together" (QUE)

//some personal tweaks:

//procedure SpinningDiamonds(Thick:thickness; x,y:word; speed:integer);
//procedure SpinningPixelTris(Thick:thickness; x,y:word; speed:integer);
//procedure SpinningTris(Thick:thickness; x,y:word; speed:integer);
//procedure SpinningRects(Thick:thickness; x,y:word; speed:integer);


procedure Diamonds(Thick:thickness; x,y:word);
//everyone loves diamonds.... :-P
// Trapeziods, in other words.

begin

   New(Rect);
   Rect^.x:=x;
   Rect^.y:=y;
   case (Thick) of
       NormalWidth: begin
             Rect^.w:=2;
			 Rect^.h:=2;
       end;
       ThickWidth: begin
             Rect^.w:=4;
			 Rect^.h:=4;
       end;
       SuperThickWidth: begin
             Rect^.w:=6;
			 Rect^.h:=6;
       end;
       UltimateThickWidth: begin
             Rect^.w:=8;
			 Rect^.h:=8;
       end;
   end;
   SDL_FillRect(MainSurface, rect,__fgcolor);

   Dispose(Rect);
   //now restore x and y
   case Thick of
		NormalWidth:begin
			x:=x+2;
			y:=y+2;
		end;
		ThickWidth:begin
			x:=x+4;
			y:=y+4;
		end;
		SuperThickWidth: begin
            x:=x+6;
			y:=y+6;
       end;
       UltimateThickWidth: begin
            x:=x+8;
			y:=y+8;
       end;
   end;
end;


procedure PlotPixelWithNeighbors(Thick:thickness; x,y:word);
//this makes the bigger Pixels

// (in other words "blocky bullet holes"...)
// EXPERT topic: smoothing reduces jagged edges

begin
   //more efficient to render a Rect- orrr a filled ellipse?

   New(Rect);
   Rect^.x:=x;
   Rect^.y:=y;
   case (Thick) of
       NormalWidth: begin
             Rect^.w:=2;
			 Rect^.h:=2;
       end;
       ThickWidth: begin
             Rect^.w:=4;
			 Rect^.h:=4;
       end;
       SuperThickWidth: begin
             Rect^.w:=6;
			 Rect^.h:=6;
       end;
       UltimateThickWidth: begin
             Rect^.w:=8;
			 Rect^.h:=8;
       end;
   end;
   SDL_FillRect(MainSurface, rect,__fgcolor); //put a FAT rect on the screen.

   Dispose(Rect);
   //now restore x and y
   case Thick of
		NormalWidth:begin
			x:=x+2;
			y:=y+2;
		end;
		ThickWidth:begin
			x:=x+4;
			y:=y+4;
		end;
		SuperThickWidth: begin
            x:=x+6;
			y:=y+6;
       end;
       UltimateThickWidth: begin
            x:=x+8;
			y:=y+8;
       end;
   end;
end;



procedure SaveBMPImage(filename:string);
var
   saveSurface:PSDL_Surface;
   pitch:integer;

begin
//the downside is that upon ea sdl init for our app/game..this resets.

  filename:='screenshot'+intTostr(screenshots)+'.bmp';
  //screenshotXXXXX.BMP

 //get screen size from current viewport- has to be setup at somepoint prior to being called....
{$ifdef cpu32}
  saveSurface:=GetPixels;
  SDL_SaveBMP(saveSurface, filename);
{$endif}
{$ifdef cpu64}
    case bpp of
		1,4,8: format:=SDL_PIXELFORMAT_RGB888; //yes, 24bpp unless otherwise specified
		15: format:=SDL_PIXELFORMAT_RGB555;

        //we assume on 16bit that we are in 565 not 5551, we should not assume
		16:	format:=SDL_PIXELFORMAT_RGB565;
		24: format:=SDL_PIXELFORMAT_RGB888;
		{$ifdef cpu64}
		32: format:=SDL_PIXELFORMAT_RGBA8888;
		{$endif}

    end;

  //SDL note: between rendering
   paused:=true;
   saveSurface := SDL_CreateRGBSurfaceWithFormat(0, MaxX, MaxY, bpp, format);
  SDL_RenderReadPixels(renderer, Nil, 0,saveSurface^.pixels, saveSurface^.pitch);
  SDL_SaveBMP(saveSurface, filename);

  paused:=false;
{$endif}

  inc(Screenshots);

end;


{$ifdef cpu32}
function GetPixels:PSDL_Surface;
//PLURAL: GET MORE THAN ONE
//this does NOT return a single pixel by default and is written intentionally that way.

//note this function is "slow ASS". It uses recursive looped reads.

var
  vbyte;
  pixelData:array of PSDL_Color;


begin

   if ((Rect^.w=1) or (Rect^.h=1)) then
        LogLn('USE GetPixel. This routine FETCHES MORE THAN ONE');
     exit;
   end;

    // The GetPixel loop- take into account viewport coords, do not assume fullscreen Mainsurface ops.
    v:=0;

    x:=0;
    y:=0;
	repeat
	    repeat
            PixelData[v]:=GetPixel(Viewport[currentviewport]^.x,Viewport[currentviewport]^.y);
            inc(v);
            inc(x);
	    until x= Viewport[currentviewport]^.w;
	 	x:=0;
	    inc(y);
	until y=Viewport[currentviewport]^.h;
	GetPixels:=pixelData;
    exit;
end;
{$endif}

//PascalMain() and/or initialization
begin


{
//NeedFrameBuffer:=false; //trip libPTC-- or something low level. Dont check yet.


FrameBuffer:

   -libPTC provides a "DOS hook" enabling FreeDOS support for "SDL" Graphics modes.
   There is some support with HXDPMI and its associate Win32 loaders....
   (I have personally not had a practical use for it yet.)

  in all practicallity- libSVGA,DirectFB libs may set the screen up--
  but everyone depends on libX11, libCocoa(OS9),libCarbon(OSX),or WinAPI these days... that code doesnt see much
  practical use, is often abandoned because we dont use "those methods" anymore.

  There's no reason we shouldnt utilize libSVGA if its our only option, however.
  -DirectFb code needs a rewrite. Its a PITA in C.
  -libSVGA just refuses to compile on a "modern machine"(kernel 4.13+ doesnt have ums drivers, X11 version 11.3+ demands modesetting)

  (RasPi even uses X11....)
  There is a "micro fpc" version out(Turbo Rascal) for other uses...
}


StartLogging;

//On unices we dont care if LCL is used or not- were still a "console app", even if not in a VTerm.
//UI apps just dont have a "console output window". LOG ANYWAY.

{$IFDEF unix}
//also "apptype" is ignored on unices.

	{$IFNDEF darwin} //Darwin(OSX) doesnt use X11.
	//It may have it, but Quartz is WAAAAY more efficient (and better), if available.

	if (GetEnvironmentVariable('DISPLAY') = '') then begin //X11 is not active
		IsConsole:=true;
		IsVTerm:=true; //This is the proper way to check, dont assume based on "flawed Unix logic".
		{$error: libDirectFB / lib(S)vga support is not built in, nor X11 active. Failing to compile.}
	end;
	{$ENDIF}

  //critically important:

  //did user press CTRL-C?
  //did we "get killed" (by OS, or user doing something to "kill process") ?
  if FpSignal(SigInt, @HandleSigInt) = signalhandler(SIG_ERR) then begin
    Writeln('Failed to install signal error: ', fpGetErrno);
    Halt(1);
  end;

{$ENDIF}

  {$IFDEF mswindows}
		{$IFDEF lcl}
			IsVTerm:=false; //ui app- NO CONSOLE AVAILABLE - not unix, no VTerm
			IsConsole:=false;
		{$endif} //defines a "console app"
		IsVTerm:=false;
		IsConsole:=True;
  {$ENDIF}

   screenshots:=00000000;
   windownumber:=0;
    //something has to be set, unless YOU set it...
    BlitTransParencyColor^.r:=$AA; 
    BlitTransParencyColor^.g:=$AA;
    BlitTransParencyColor^.b:=$00;


finalization //if this library ever exits, call closegraph. You dont add a handler, or exit routine. Should handle crashes, as well(OS/FPC garbage collection)
  CloseGraph;
//in case the above doesnt work...
{

 oldexitproc:=exitproc;
 exitproc:=@CloseGraph;

}

end.
