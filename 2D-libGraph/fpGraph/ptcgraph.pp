{
    This file is part of the Free Pascal run time library.
    Copyright (c) 2010, 2011 by Nikolay Nikolov (nickysn@users.sourceforge.net)
    Copyright (c) 2007 by Daniel Mantione
      member of the Free Pascal development team


Function ClipCoords (Var X,Y : smallint) : Boolean;
{ Adapt to viewport, return TRUE if still in viewport,
  false if outside viewport}

begin
  X:= X + StartXViewPort;
  Y:= Y + StartYViewPort;
  ClipCoords:=Not ClipPixels;
  if ClipPixels then
    Begin
    ClipCoords:=(X < StartXViewPort) or (X > (StartXViewPort + ViewWidth));
    ClipCoords:=ClipCoords or
               ((Y < StartYViewPort) or (Y > (StartYViewPort + ViewHeight)));
    ClipCoords:=Not ClipCoords;
    end;
end;

//from libPtc:
//32 bit color = 24 bit color + "blend mode" enabled.
procedure DirectPixelProc_32bpp(X,Y: smallint);
var
  pixels:Plongword;
begin
//  Writeln('ptc_DirectPixelProc_32bpp(', X, ', ', Y, ')');

  pixels := ptc_surface_lock;
  case CurrentWriteMode of
    XORPut:
      begin
        pixels[x+y*PTCWidth] := pixels[x+y*PTCWidth] xor CurrentColor;
      end;
    OrPut:
      begin
        pixels[x+y*PTCWidth] := pixels[x+y*PTCWidth] or CurrentColor;
      end;
    AndPut:
      begin
        pixels[x+y*PTCWidth] := pixels[x+y*PTCWidth] and CurrentColor;
      end;
    NotPut:
      begin
        pixels[x+y*PTCWidth] := CurrentColor xor $FFFFFF;
      end
  else
    pixels[x+y*PTCWidth] := CurrentColor;
  end;

  ptc_surface_unlock;
  ptc_update;
end;


procedure ptc_HLineProc_32bpp(x, x2,y : smallint);

var pixels:Plongword;
    i:word;
    xtmp: smallint;

begin
//  Writeln('ptc_HLineProc_32bpp(', x, ', ', x2, ', ', y, ')');
  { must we swap the values? }
  if x >= x2 then
  begin
    xtmp := x2;
    x2 := x;
    x:= xtmp;
  end;

  inc(x,StartXViewPort);
  inc(x2,StartXViewPort);
  inc(y,StartYViewPort);
  if ClipPixels then
  begin
    if LineClipped(x,y,x2,y,StartXViewPort,StartYViewPort,
               StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;
  end;

  pixels := ptc_surface_lock;

  case CurrentWriteMode of
    XORPut:
      begin
        for i:=x to x2 do
          pixels[i+y*PTCWidth] := pixels[i+y*PTCWidth] xor CurrentColor;
      end;
    OrPut:
      begin
        for i:=x to x2 do
          pixels[i+y*PTCWidth] := pixels[i+y*PTCWidth] or CurrentColor;
      end;
    AndPut:
      begin
        for i:=x to x2 do
          pixels[i+y*PTCWidth] := pixels[i+y*PTCWidth] and CurrentColor;
      end;
    NotPut:
      begin
        for i:=x to x2 do
          pixels[i+y*PTCWidth] := CurrentColor xor $FFFFFF;
      end
  else
    for i:=x to x2 do
      pixels[i+y*PTCWidth] := CurrentColor;
  end;

  ptc_surface_unlock;
  ptc_update;
end;

procedure ptc_VLineProc_32bpp(x,y,y2 : smallint);
var pixels:PLongWord;
    i:word;
    ytmp: smallint;
begin
  if y >= y2 then
   begin
     ytmp := y2;
     y2 := y;
     y:= ytmp;
   end;

  inc(x,StartXViewPort);
  inc(y,StartYViewPort);
  inc(y2,StartYViewPort);
  if ClipPixels then
  begin
    if LineClipped(x,y,x,y2,StartXViewPort,StartYViewPort,
          StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;
  end;

  pixels := ptc_surface_lock;

  case CurrentWriteMode of
    XORPut:
      begin
        for i:=y to y2 do
          pixels[x+i*PTCWidth] := pixels[x+i*PTCWidth] xor CurrentColor;
      end;
    OrPut:
      begin
        for i:=y to y2 do
          pixels[x+i*PTCWidth] := pixels[x+i*PTCWidth] or CurrentColor;
      end;
    AndPut:
      begin
        for i:=y to y2 do
          pixels[x+i*PTCWidth] := pixels[x+i*PTCWidth] and CurrentColor;
      end;
    NotPut:
      begin
        for i:=y to y2 do
          pixels[x+i*PTCWidth] := CurrentColor xor $FFFFFF;
      end
  else
    for i:=y to y2 do
      pixels[x+i*PTCWidth] := CurrentColor;
  end;

  ptc_surface_unlock;
  ptc_update;
end;

procedure ptc_HLineProc_16bpp(x, x2,y : smallint);

var pixels:Pword;
    i:word;
    xtmp: smallint;

begin
//  Writeln('ptc_HLineProc_16bpp(', x, ', ', x2, ', ', y, ')');
  { must we swap the values? }
  if x >= x2 then
  begin
    xtmp := x2;
    x2 := x;
    x:= xtmp;
  end;

  inc(x,StartXViewPort);
  inc(x2,StartXViewPort);
  inc(y,StartYViewPort);
  if ClipPixels then
  begin
    if LineClipped(x,y,x2,y,StartXViewPort,StartYViewPort,
               StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;
  end;

  pixels := ptc_surface_lock;
    for i:=x to x2 do
      pixels[i+y*PTCWidth] := CurrentColor;
  end;

  ptc_surface_unlock;
  ptc_update;
end;

procedure ptc_VLineProc_16bpp(x,y,y2 : smallint);
var pixels:PWord;
    i:word;
    ytmp: smallint;
begin
  if y >= y2 then
   begin
     ytmp := y2;
     y2 := y;
     y:= ytmp;
   end;

  inc(x,StartXViewPort);
  inc(y,StartYViewPort);
  inc(y2,StartYViewPort);
  if ClipPixels then
  begin
    if LineClipped(x,y,x,y2,StartXViewPort,StartYViewPort,
          StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;
  end;

  pixels := ptc_surface_lock;

    for i:=y to y2 do
      pixels[x+i*PTCWidth] := CurrentColor;
  end;

  ptc_surface_unlock;
  ptc_update;
end;

procedure ptc_PatternLineProc_8bpp(x1,x2,y: smallint);
{********************************************************}
{ Draws a horizontal patterned line according to the     }
{ current Fill Settings.                                 }
{********************************************************}
{ Important notes:                                       }
{  - CurrentColor must be set correctly before entering  }
{    this routine.                                       }
{********************************************************}
var
  pixels: PByte;
  NrIterations: smallint;
  i           : smallint;
  j           : smallint;
  TmpFillPattern : byte;
begin
  { convert to global coordinates ... }
  x1 := x1 + StartXViewPort;
  x2 := x2 + StartXViewPort;
  y  := y + StartYViewPort;
  { if line was fully clipped then exit...}
  if LineClipped(x1,y,x2,y,StartXViewPort,StartYViewPort,
     StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;

  { Get the current pattern }
  TmpFillPattern := FillPatternTable
    [FillSettings.Pattern][(y and $7)+1];

  pixels := ptc_surface_lock;

  { number of times to go throuh the 8x8 pattern }
  NrIterations := abs(x2 - x1+8) div 8;
  for i := 0 to NrIterations do
    for j := 0 to 7 do
    begin
      { x1 mod 8 }
      if RevBitArray[x1 and 7] and TmpFillPattern <> 0 then
        pixels[x1+y*PTCWidth] := FillSettings.Color and ColorMask
      else
        pixels[x1+y*PTCWidth] := CurrentBkColor and ColorMask;
      Inc(x1);
      if x1 > x2 then
      begin
        ptc_surface_unlock;
        ptc_update;
        exit;
      end;
    end;
  ptc_surface_unlock;
  ptc_update;
end;

procedure ptc_PatternLineProc_16bpp(x1,x2,y: smallint);
{********************************************************}
{ Draws a horizontal patterned line according to the     }
{ current Fill Settings.                                 }
{********************************************************}
{ Important notes:                                       }
{  - CurrentColor must be set correctly before entering  }
{    this routine.                                       }
{********************************************************}
var
  pixels: PWord;
  NrIterations: smallint;
  i           : smallint;
  j           : smallint;
  TmpFillPattern : byte;
begin
  { convert to global coordinates ... }
  x1 := x1 + StartXViewPort;
  x2 := x2 + StartXViewPort;
  y  := y + StartYViewPort;
  { if line was fully clipped then exit...}
  if LineClipped(x1,y,x2,y,StartXViewPort,StartYViewPort,
     StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;

  { Get the current pattern }
  TmpFillPattern := FillPatternTable
    [FillSettings.Pattern][(y and $7)+1];

  pixels := ptc_surface_lock;

  { number of times to go throuh the 8x8 pattern }
  NrIterations := abs(x2 - x1+8) div 8;
  for i := 0 to NrIterations do
    for j := 0 to 7 do
    begin
      { x1 mod 8 }
      if RevBitArray[x1 and 7] and TmpFillPattern <> 0 then
        pixels[x1+y*PTCWidth] := FillSettings.Color
      else
        pixels[x1+y*PTCWidth] := CurrentBkColor;
      Inc(x1);
      if x1 > x2 then
      begin
        ptc_surface_unlock;
        ptc_update;
        exit;
      end;
    end;
  ptc_surface_unlock;
  ptc_update;
end;

procedure ptc_PatternLineProc_32bpp(x1,x2,y: smallint);
{********************************************************}
{ Draws a horizontal patterned line according to the     }
{ current Fill Settings.                                 }
{********************************************************}
{ Important notes:                                       }
{  - CurrentColor must be set correctly before entering  }
{    this routine.                                       }
{********************************************************}
var
  pixels: PLongWord;
  NrIterations: smallint;
  i           : smallint;
  j           : smallint;
  TmpFillPattern : byte;
begin
  { convert to global coordinates ... }
  x1 := x1 + StartXViewPort;
  x2 := x2 + StartXViewPort;
  y  := y + StartYViewPort;
  { if line was fully clipped then exit...}
  if LineClipped(x1,y,x2,y,StartXViewPort,StartYViewPort,
     StartXViewPort+ViewWidth, StartYViewPort+ViewHeight) then
      exit;

  { Get the current pattern }
  TmpFillPattern := FillPatternTable
    [FillSettings.Pattern][(y and $7)+1];

  pixels := ptc_surface_lock;

  { number of times to go throuh the 8x8 pattern }
  NrIterations := abs(x2 - x1+8) div 8;
  for i := 0 to NrIterations do
    for j := 0 to 7 do
    begin
      { x1 mod 8 }
      if RevBitArray[x1 and 7] and TmpFillPattern <> 0 then
        pixels[x1+y*PTCWidth] := FillSettings.Color
      else
        pixels[x1+y*PTCWidth] := CurrentBkColor;
      Inc(x1);
      if x1 > x2 then
      begin
        ptc_surface_unlock;
        ptc_update;
        exit;
      end;
    end;
  ptc_surface_unlock;
  ptc_update;
end;


procedure ptc_getrgbpaletteproc (ColorNum: smallint;
                                    var RedValue, GreenValue, BlueValue: smallint);
begin
  { NOTE: this makes the function compatible to the go32v2 graph implementation, but
    *not* with TP7 }
    
  if EGAPaletteEnabled then
    ColorNum := DefaultEGAPalette[ColorNum];

//correct, but not for lazgfx implementation
  RedValue := VGAPalette[ColorNum, 0] shl 2;
  GreenValue := VGAPalette[ColorNum, 1] shl 2;
  BlueValue := VGAPalette[ColorNum, 2] shl 2;
end;

{**********************************************************}
{ Procedure PutImage()                                     }
{----------------------------------------------------------}
{ Displays the image contained in a bitmap starting at X,Y }
{ the first 2 bytes of the bitmap structure define the     }
{ width and height of the bitmap                           }
{ note: This optomized version does not use PutPixel       }
{   Which would be checking the viewport for every pixel   }
{   Instead it just does it's own viewport check once then }
{   puts all the pixels within the veiwport without further}
{   checking.  Also instead of checking BitBlt every pixel }
{   it is only checked once before all the pixels are      }
{   displayed at once   (JMR)                              }
{**********************************************************}
//be advised of blits w sdl, and bmp texture usage.

Procedure ptc_PutImageproc_8bpp(X,Y: smallint; var Bitmap; BitBlt: Word);
type
  pt = array[0..{$ifdef cpu16}16382{$else}$fffffff{$endif}] of word;
  ptw = array[0..2] of longint;
var
  pixels:Pbyte;
  k: longint;
  i, j, y1, x1, deltaX, deltaX1, deltaY: smallint;
  JxW, I_JxW: Longword;
Begin
  inc(x,startXViewPort);
  inc(y,startYViewPort);
  { width/height are 1-based, coordinates are zero based }
  x1 := ptw(Bitmap)[0]+x-1; { get width and adjust end coordinate accordingly }
  y1 := ptw(Bitmap)[1]+y-1; { get height and adjust end coordinate accordingly }
  deltaY := 0;
  deltaX := 0;
  deltaX1 := 0;

  k := 3 * sizeOf(Longint) div sizeOf(Word); { Three reserved longs at start of bitmap }
 { check which part of the image is in the viewport }

  if clipPixels then
    begin
      if y < startYViewPort then
        begin
          deltaY := startYViewPort - y;
          inc(k,(x1-x+1)*deltaY);
          y := startYViewPort;
         end;
      if y1 > startYViewPort+viewHeight then
        y1 := startYViewPort+viewHeight;
      if x < startXViewPort then
        begin
          deltaX := startXViewPort-x;
          x := startXViewPort;
        end;
      if x1 > startXViewPort + viewWidth then
        begin
          deltaX1 := x1 - (startXViewPort + viewWidth);
          x1 := startXViewPort + viewWidth;
        end;
    end;
  pixels := ptc_surface_lock;
 
      Begin
        for j:=Y to Y1 do
          Begin
            JxW:=j*PTCWidth;
            inc(k,deltaX);
            for i:=X to X1 do
              begin
                pixels[i+JxW] := pt(bitmap)[k] and ColorMask;
                inc(k);
              end;
            inc(k,deltaX1);
          End;
      End;
  End; 
  ptc_surface_unlock;
  ptc_update;
end;


{**********************************************************}
{ Procedure GetScanLine()                                  }
{----------------------------------------------------------}
{ Returns the full scanline of the video line of the Y     }
{ coordinate. The values are returned in a WORD array      }
{ each WORD representing a pixel of the specified scanline }
{ note: we only need the pixels inside the ViewPort! (JM)  }

{ note2: extended so you can specify start and end X coord }
{   so it is usable for GetImage too (JM)                  }

{ note3: This optomized version does not use GetPixel,     }
{   Whcih would be checking the viewport for every pixel.  }
{   Instead it just does it's own viewport check once then }
{   gets all the pixels on the scan line without further   }
{   checking  (JMR)                                        }
{**********************************************************}

Procedure PTC_GetScanlineProc_8bpp (X1, X2, Y : smallint; Var Data);
Var
  pixels        : Pbyte;
  x,vpx1,vpx2   : smallint;
Begin
   vpx1:=X1+StartXViewPort;
   vpx2:=X2+StartXViewPort;
   Y:=Y+StartYViewPort;
    { constrain to the part of the scanline that is in the viewport }
    if clipPixels then
       begin
          if vpx1 <  startXViewPort then
             vpx1 := startXViewPort;
          if vpx2 >  startXViewPort + viewWidth then
             vpx2 := startXViewPort + viewWidth;
       end;
    { constrain to the part of the scanline that is on the screen }
    if vpx1 <  0 then
       vpx1 := 0;
    if vpx2 >= PTCwidth then
       vpx2 := PTCwidth-1;
    If (ClipPixels AND (y <= startYViewPort+viewHeight) and (y >= startYViewPort) and (y>=0) and (y<PTCheight)) or Not(ClipPixels) then
       Begin
          pixels := ptc_surface_lock;
          For x:=vpx1 to vpx2 Do
             WordArray(Data)[x-StartXViewPort-x1]:=pixels[x+y*PTCWidth] and ColorMask;
          ptc_surface_unlock;
       End;
End;

Procedure PTC_GetScanlineProc_32bpp (X1, X2, Y : smallint; Var Data);
Var
  pixels        : Plongword;
  x,vpx1,vpx2   : smallint;
Begin
   vpx1:=X1+StartXViewPort;
   vpx2:=X2+StartXViewPort;
   Y:=Y+StartYViewPort;
    { constrain to the part of the scanline that is in the viewport }
    if clipPixels then
       begin
          if vpx1 <  startXViewPort then
             vpx1 := startXViewPort;
          if vpx2 >  startXViewPort + viewWidth then
             vpx2 := startXViewPort + viewWidth;
       end;
    { constrain to the part of the scanline that is on the screen }
    if vpx1 <  0 then
       vpx1 := 0;
    if vpx2 >= PTCwidth then
       vpx2 := PTCwidth-1;
    If (ClipPixels AND (y <= startYViewPort+viewHeight) and (y >= startYViewPort) and (y>=0) and (y<PTCheight)) or Not(ClipPixels) then
       Begin
          pixels := ptc_surface_lock;
          For x:=vpx1 to vpx2 Do
             LongWordArray(Data)[x-StartXViewPort-x1]:=pixels[x+y*PTCWidth];
          ptc_surface_unlock;
       End;
End;

//nope. High definition...amiga..phones....
    function IsNonStandardResolution(AWidth, AHeight: Integer): Boolean;
    begin
      IsNonStandardResolution :=
            not ((AWidth =  320) and (AHeight =  200))
        and not ((AWidth =  640) and (AHeight =  200))
        and not ((AWidth =  640) and (AHeight =  350))
        and not ((AWidth =  640) and (AHeight =  400))
        and not ((AWidth =  640) and (AHeight =  480))
        and not ((AWidth =  720) and (AHeight =  348))
        and not ((AWidth =  800) and (AHeight =  600))
        and not ((AWidth = 1024) and (AHeight =  768))
        and not ((AWidth = 1280) and (AHeight = 1024));
    end;



