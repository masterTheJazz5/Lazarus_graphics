{**************************************************************************
    Copyright            : (c) 2007 Evgeniy Ivanov
   

unit sdlgraph;

interface

{$i graphh.inc}

implementation

{$i graph.inc}

var
    screen: PSDL_Surface; //Global becouse its value is needed by some functions


procedure Slock;
begin
  if SDL_MUSTLOCK(screen) then begin
    if SDL_LockSurface(screen) < 0 then begin
          Log.LogError( Format( 'Cant lock screen: : %s', [SDL_GetError]), 'Slock' );
          CloseGraph;
          end;
    end;      
end;


procedure sdlgraph_PutPixel(X,Y:smallint; color: Word);
begin
 X:= X + StartXViewPort;
 Y:= Y + StartYViewPort;

{ convert to absolute coordinates and then verify clipping...}
 if ClipPixels then
   begin
   if (X < StartXViewPort) or (X > (StartXViewPort + ViewWidth)) then
          exit;
   if (Y < StartYViewPort) or (Y > (StartYViewPort + ViewHeight)) then
          exit;
   end;
 SDL_PutPixel(screen,x,y,255);
 exit;

end;


function sdlgraph_GetPixel(X,Y:smallint):Word;
var
temp:word;
begin
 X:= X + StartXViewPort;
 Y:= Y + StartYViewPort;
 temp:=word(SDL_GetPixel(screen,x,y));
 sdlgraph_GetPixel:=temp;
 exit;
end;

procedure sdlgraph_HLine(x,x2,y: smallint);
var
temp:DefPixelProc;
begin
 HLineDefault(x,x2,y);
 //SDL_DrawLine(screen,X,y,x2,y,255);
end;

procedure sdlgraph_VLine(x,y,y2:smallint);
var
temp:DefPixelProc;
begin
VLineDefault(x,y,y2);
end;


procedure sdlgraph_line(X1, Y1, X2, Y2: smallint);
var
temp:DefPixelProc;
begin
 LineDefault(X1, Y1, X2, Y2);
 //SDL_DrawLine(screen,X1,y1,x2,y2,255);
end;
}

{
procedure sdlgraph_InternalEllipse(X,Y: smallint;XRadius: word;
    YRadius:word; stAngle,EndAngle: word; pl: PatternLineProc);
var
temp:PutPixelProc;
begin
 InternalEllipseDefault(X,Y,XRadius,YRadius,stAngle,EndAngle,pl);
end;
}


procedure InitSDLgraph(Width,Height,BPP:Integer);
var
 videoflags : Uint32;
 videoInfo : PSDL_VideoInfo;
 
 flip_callback_param:Pointer;
 flip_timer_id:PSDL_TimerID;
begin
  if ( SDL_Init( SDL_INIT_TIMER or SDL_INIT_VIDEO ) < 0 ) then
  begin
    Log.LogError( Format( 'Could not initialize SDL : %s', [SDL_GetError] ), 'InitSDLgraph' );
    exit;
  end;


  // Fetch the video info 
  videoInfo := SDL_GetVideoInfo;

  if ( videoInfo = nil ) then
  begin
    Log.LogError( Format( 'Video query failed : %s', [SDL_GetError] ), 'InitSDLgraph' );
    CloseGraph;
    exit;
  end;


  // This checks if hardware blits can be done * /
  if videoInfo^.blit_hw <> 0 then
    videoFlags := videoFlags or SDL_HWACCEL;

  if (SDL_VideoModeOK(Width,Height,BPP,videoFlags) = 0) then
     begin
         //Log.LogError('InitSDLgraph: ',Width,'x',Height,'x',BPP,' - no such mode (also you may check videoflags in the sdlgraph unit (procedure InitSDLgraph)');
         exit;
     end;

  screen := SDL_SetVideoMode(Width, Height, BPP, SDL_SWSURFACE );   // TODO: use videoflags but not SDL_SWSURFACE!
    
if ( screen = nil ) then
  begin
    Log.LogError( Format( 'Unable to SetVideMode : %s', [SDL_GetError]), 'InitSDLgraph' );
    InitSDLgraph:=false;
    exit;
    CloseGraph;
  end;

flip_timer_id := SDL_AddTimer(60,TSDL_NewTimerCallback( @timer_flip ), nil ); //TODO: time interval must be the same as monitor vertical refresh

end;

