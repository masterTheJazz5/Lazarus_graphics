﻿//borrowed from wikipedia. -Jazz

#include <stdlib.h>
#include <unistd.h>
#include <vga.h>

int main(void)
{
   int color = 4;
   int x = 10;
   int y = 10;
   unsigned int seconds = 5;

   /* detect the chipset and give up supervisor rights */
   if (vga_init() < 0)
        return EXIT_FAILURE;

   vga_setmode(G320x200x256);
   vga_setcolor(color);
   vga_drawpixel(x, y);

   sleep(seconds);

   /* restore textmode and fall back to ordinary text console handling */
   vga_setmode(TEXT);

   return EXIT_SUCCESS;
}
