unit LazHelpViewerMainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, LazHelp, FGL;

type

  TImageCacheEntry = class
    Name: string;
    Image: TImage;
  end;

  TImageCache = specialize TFPGObjectList<TImageCacheEntry>;

  { TMain }

  TMain = class(TForm)
    btBrowse: TButton;
    btPreview: TButton;
    edFileName: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    LazHelp1: TLazHelp;
    LazHelpWindowedViewer1: TLazHelpWindowedViewer;
    OpenDialog1: TOpenDialog;
    StartupTimer: TTimer;
    procedure btBrowseClick(Sender: TObject);
    procedure btPreviewClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LazHelp1GetGraphic(Sender: TObject; AName: string;
      var AGraphic: TGraphic);
    procedure StartupTimerTimer(Sender: TObject);
  private
    ImageCache: TImageCache;
    function GetImage(AName: string): TGraphic;
  public
    { public declarations }
  end; 

var
  Main: TMain;

implementation

{$R *.lfm}

{ TMain }

procedure TMain.btBrowseClick(Sender: TObject);
begin
  if not OpenDialog1.Execute then Exit;
  edFileName.Text:=OpenDialog1.FileName;
end;

procedure TMain.btPreviewClick(Sender: TObject);
begin
  LazHelp1.Code:='';
  LazHelp1.FileName:=edFileName.Text;
  LazHelpWindowedViewer1.ShowHelp;
  LazHelpWindowedViewer1.Refresh;
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(ImageCache);
end;

procedure TMain.LazHelp1GetGraphic(Sender: TObject; AName: string;
  var AGraphic: TGraphic);
begin
  AGraphic:=GetImage(AName);
end;

procedure TMain.StartupTimerTimer(Sender: TObject);
var
  FileName: string;
  i: Integer;
begin
  StartupTimer.Enabled:=False;
  FileName:='';
  for i:=1 to Application.ParamCount do FileName:=FileName + ' ' + Application.Params[i];
  FileName:=Trim(FileName);
  edFileName.Text:=FileName;
  if (FileName <> '') and FileExists(FileName) then btPreviewClick(btPreview);
end;

function TMain.GetImage(AName: string): TGraphic;
var
  i: Integer;
  Image: TImage;
  Entry: TImageCacheEntry;
  FileName: string;
begin
  if not Assigned(ImageCache) then ImageCache:=TImageCache.Create(True);
  for i:=0 to ImageCache.Count - 1 do
    if ImageCache[i].Name=AName then
      Exit(ImageCache[i].Image.Picture.Graphic);

  FileName:=ExtractFileDir(edFileName.Text) + DirectorySeparator + AName + '.png';
  if not FileExists(FileName) then begin
    FileName:=ExtractFileDir(edFileName.Text) + DirectorySeparator + AName + '.jpg';
    if not FileExists(FileName) then begin
      FileName:=ExtractFileDir(edFileName.Text) + DirectorySeparator + AName + '.gif';
      if not FileExists(FileName) then begin
        FileName:=ExtractFileDir(edFileName.Text) + DirectorySeparator + AName + '.bmp';
        if not FileExists(FileName) then Exit(nil);
      end;
    end;
  end;

  Result:=nil;
  try
    Image:=TImage.Create(Self);
    Image.Picture.LoadFromFile(FileName);
  except
    Exit;
  end;

  Entry:=TImageCacheEntry.Create;
  Entry.Image:=Image;
  Entry.Name:=AName;

  ImageCache.Add(Entry);

  Result:=Image.Picture.Graphic;
end;

end.

