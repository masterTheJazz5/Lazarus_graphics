LazHelp - A help system for Lazarus written in LCL
==================================================


  This is LazHelp, a help system for Lazarus written in LCL for (theoretical)
maximum portability between the target widgetsets that Lazarus supports.  In
practice the only widgetset i've tested so far is Win32 (which works fine) and
Carbon (which doesn't work).


  Read the included apidocs.txt for information and build the lazhelpeditor
to which has a help site explaining LazHelp in detail.  Before building
lazhelpeditor or lazhelptest, you must install the lazhelppackage.lpk
package first.  This will install five new components in the LazHelp tab.

Kostas "Bad Sector" Michalopoulos
badsectoracula@gmail.com
