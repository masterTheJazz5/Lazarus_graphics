{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit lazhelppackage; 

interface

uses
  LazHelp, LazarusPackageIntf;

implementation

procedure Register; 
begin
  RegisterUnit('LazHelp', @LazHelp.Register); 
end; 

initialization
  RegisterPackage('lazhelppackage', @Register); 
end.
