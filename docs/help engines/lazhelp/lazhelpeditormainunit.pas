unit LazHelpEditorMainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, ExtCtrls, LazHelp, LResources, LazHelpHTML, ComCtrls, process;

type

  { TMain }

  TMain = class(TForm)
    cbPages: TComboBox;
    EditingLazHelp: TLazHelp;
    DummyImage: TImage;
    LazHelp1: TLazHelp;
    LazHelpViewer1: TLazHelpViewer;
    EditingWindowedViewer: TLazHelpWindowedViewer;
    LazHelpWindowedViewer1: TLazHelpWindowedViewer;
    MainMenu1: TMainMenu;
    Code: TMemo;
    mFileViewHelpSite: TMenuItem;
    mFileBar2: TMenuItem;
    mFileOpen: TMenuItem;
    mFileSave: TMenuItem;
    mFileSaveAs: TMenuItem;
    mFileBar1: TMenuItem;
    mFileNew: TMenuItem;
    mHelpAbout: TMenuItem;
    mHelpContents: TMenuItem;
    mHelp: TMenuItem;
    mFileExit: TMenuItem;
    mFile: TMenuItem;
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    SaveDialog1: TSaveDialog;
    Splitter2: TSplitter;
    StringListLazHelpProvider1: TStringListLazHelpProvider;
    Timer1: TTimer;
    procedure cbPagesChange(Sender: TObject);
    procedure CodeChange(Sender: TObject);
    procedure EditingLazHelpGetGraphic(Sender: TObject; AName: string;
      var AGraphic: TGraphic);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LazHelp1GetGraphic(Sender: TObject; AName: string;
      var AGraphic: TGraphic);
    procedure LazHelpViewer1PageTitle(Sender: TObject; ATitle: string);
    procedure mFileExitClick(Sender: TObject);
    procedure mFileNewClick(Sender: TObject);
    procedure mFileOpenClick(Sender: TObject);
    procedure mFileSaveAsClick(Sender: TObject);
    procedure mFileSaveClick(Sender: TObject);
    procedure mFileViewHelpSiteClick(Sender: TObject);
    procedure mHelpAboutClick(Sender: TObject);
    procedure mHelpContentsClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    CurrentPage: string;
    UpdateCounter: Integer;
    lazhelpeditorwindow: TGraphic;
    FileName: string;
    function OkToLoseEverything: Boolean;
    procedure UpdateHelp;
    procedure UpdateCaption;
  end; 

var
  Main: TMain;

implementation

{$R *.lfm}

{ TMain }

procedure TMain.Timer1Timer(Sender: TObject);
begin
  if UpdateCounter=1 then UpdateHelp;
  if UpdateCounter > 0 then Dec(UpdateCounter);
end;

function TMain.OkToLoseEverything: Boolean;
begin
  if Code.Modified then begin
    if MessageDlg('The file is modified', 'If you continue now, you will lose your modifications. Do you want to continue and lose the modifications?', mtConfirmation, mbYesNo, 0) <> mrYes then Exit(False);
  end;
  Result:=True;
end;

procedure TMain.CodeChange(Sender: TObject);
begin
  UpdateCounter:=4;
end;

procedure TMain.EditingLazHelpGetGraphic(Sender: TObject; AName: string;
  var AGraphic: TGraphic);
begin
  AGraphic:=DummyImage.Picture.Graphic;
end;

procedure TMain.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  CanClose:=OkToLoseEverything;
end;

procedure TMain.FormCreate(Sender: TObject);
begin
  lazhelpeditorwindow:=CreateBitmapFromLazarusResource('lazhelpeditorwindow');
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  lazhelpeditorwindow.Free;
end;

procedure TMain.FormShow(Sender: TObject);
begin
  UpdateHelp;
end;

procedure TMain.LazHelp1GetGraphic(Sender: TObject; AName: string;
  var AGraphic: TGraphic);
begin
  if AName='lazhelpeditorwindow' then AGraphic:=lazhelpeditorwindow;
end;

procedure TMain.LazHelpViewer1PageTitle(Sender: TObject; ATitle: string);
begin
  UpdateCaption;
  Caption:=Caption + ' [' + ATitle + ']';
end;

procedure TMain.mFileExitClick(Sender: TObject);
begin
  Close;
end;

procedure TMain.mFileNewClick(Sender: TObject);
begin
  if OkToLoseEverything then begin
    Code.Clear;
    Code.Modified:=False;
    EditingLazHelp.Code:='';
    LazHelpViewer1.Repaint;
    CurrentPage:='';
    FileName:='';
    UpdateCaption;
    UpdateHelp;
  end;
end;

procedure TMain.mFileOpenClick(Sender: TObject);
begin
  if OkToLoseEverything then begin
    OpenDialog1.FileName:=FileName;
    if OpenDialog1.Execute then try
      Code.Lines.LoadFromFile(OpenDialog1.FileName);
      FileName:=OpenDialog1.FileName;
      UpdateCaption;
      UpdateHelp;
    except
      on E : Exception do begin
        MessageDlg('Error', E.Message, mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMain.mFileSaveAsClick(Sender: TObject);
begin
  SaveDialog1.FileName:=FileName;
  if SaveDialog1.Execute then try
    Code.Lines.SaveToFile(SaveDialog1.FileName);
    FileName:=SaveDialog1.FileName;
    UpdateCaption;
  except
    on E : Exception do begin
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMain.mFileSaveClick(Sender: TObject);
begin
  if FileName='' then begin
    mFileSaveAsClick(Sender);
  end else try
    Code.Lines.SaveToFile(FileName);
    Code.Modified:=False;
  except
    on E : Exception do begin
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMain.mFileViewHelpSiteClick(Sender: TObject);
begin
  EditingWindowedViewer.ShowHelp;
end;

procedure TMain.mHelpAboutClick(Sender: TObject);
begin
  ShowMessage('LazHelp Editor version 0.1 by Kostas Michalopoulos');
end;

procedure TMain.mHelpContentsClick(Sender: TObject);
begin
  LazHelpWindowedViewer1.ShowHelp;
end;

procedure TMain.cbPagesChange(Sender: TObject);
begin
  CurrentPage:=cbPages.Items[cbPages.ItemIndex];
  LazHelpViewer1.Page:=CurrentPage;
end;

procedure TMain.UpdateHelp;
var
  i, Index: Integer;
begin
  EditingLazHelp.Code:=Code.Text;
  cbPages.Clear;
  Index:=-1;
  for i:=0 to EditingLazHelp.PageCount - 1 do begin
    cbPages.Items.Add(EditingLazHelp.Pages[i]);
    if EditingLazHelp.Pages[i]=CurrentPage then Index:=i;
  end;
  cbPages.ItemIndex:=Index;
  if (Index=-1) and (EditingLazHelp.PageCount > 0) then begin
    Index:=0;
    cbPages.ItemIndex:=0;
    CurrentPage:=EditingLazHelp.Pages[0];
  end;
  LazHelpViewer1.Page:=CurrentPage;
  LazHelpViewer1.Repaint;
end;

procedure TMain.UpdateCaption;
begin
  if FileName='' then
    Caption:='LazHelp Editor'
  else
    Caption:='LazHelp Editor - ' + ExtractFileName(FileName);
end;

initialization
  {$I lazhelpeditorhelpimages.lrs}
end.

