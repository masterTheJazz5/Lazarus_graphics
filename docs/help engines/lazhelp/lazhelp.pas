unit LazHelp;

{$mode objfpc}{$H+}

interface

uses
  Types, Classes, SysUtils, Controls, Graphics, StdCtrls, Forms, LResources;

type
  ELazHelpPageIndexOutOfRange = class(Exception);

  TLazHelpLinkHandler = (ltThisViewer, ltWindowedViewer, ltHelpManager);

  TOnLazHelpPageTitle = procedure(Sender: TObject; ATitle: string) of object;
  TOnLazHelpLinkClick = procedure(Sender: TObject; var ATarget: string; var Processed: Boolean) of object;
  TOnLazHelpGetGraphic = procedure(Sender: TObject; AName: string; var AGraphic: TGraphic) of object;
  TOnLazHelpLinkHandlerQuery = procedure(Sender: TObject; ATarget: string; var AHandler: TLazHelpLinkHandler) of object;

  TLazHelpWindowedViewer = class;

  { TLazHelpElement }

  TLazHelpElement = class(TPersistent)
  private
    FName: string;
    FParent: TLazHelpElement;
    FValue: string;
    FChildren: TList;
    FLinkIndex: PtrInt;
    function GetChild(AIndex: Integer): TLazHelpElement;
    function GetCount: Integer;
    function GetText: string;
  public
    constructor Create(AParent: TLazHelpElement; AName, AValue: string);
    destructor Destroy; override;
    function FindElementByName(AName: string): TLazHelpElement;
    property Parent: TLazHelpElement read FParent;
    property Name: string read FName;
    property Value: string read FValue;
    property Children[AIndex: Integer]: TLazHelpElement read GetChild; default;
    property Count: Integer read GetCount;
    property Text: string read GetText;
  end;

  { TLazHelpLinkInfo }

  TLazHelpLinkInfo = object
    Element: TLazHelpElement;
    Areas: array of TRect;
    AreaCount: Integer;

    procedure Reset;
    procedure AddArea(R: TRect);
    function Check(X, Y: Integer): Boolean;
  end;

  { TLazHelpGraphicInfo }
  TLazHelpGraphicInfo = object
    Name: string;
    Graphic: TGraphic;
  end;

  { TLazHelpPaintInfo }

  TLazHelpPaintInfo = object
    Color, BackgroundColor, LinkColor, HeadColor: TColor;
    Canvas: TCanvas;
    Pad, Rect: TRect;
    Scroll: Integer;
    { Filled by PaintAt }
    PageHeight: Integer;
    { Used by PaintAt }
    X, Y, ListDepth: Integer;
    Link: Integer;
    ImportCount: Integer;
    LineHeight: Integer;

    procedure Prepare(ACanvas: TCanvas; ARect: TRect; AScroll: Integer);
  end;

  { TLazHelp }

  TLazHelp = class(TComponent)
  private
    FCode: string;
    FFileName: string;
    FOnGetGraphic: TOnLazHelpGetGraphic;
    FPage: TLazHelpElement;
    FDefaultPage: string;
    FTitle: string;
    FPages: array of TLazHelpElement;
    Links: array of TLazHelpLinkInfo;
    Graphics: array of TLazHelpGraphicInfo;
    Root: TLazHelpElement;
    Head: Integer;
    FOnLinkClick: TOnLazHelpLinkClick;
    function GetPage: string;
    function GetPageCount: Integer;
    function GetPageElement(AIndex: Integer): TLazHelpElement;
    function GetPages(AIndex: Integer): string;
    procedure SetFileName(const AValue: string);
    procedure SetPage(const AValue: string);
    procedure ParseCodeFor(Node: TLazHelpElement);
    procedure SetCode(const AValue: string);
    procedure PaintElement(var LazHelpPaintInfo: TLazHelpPaintInfo; E: TLazHelpElement);
    function GetLinkIndexAt(X, Y: Integer): Integer;
    function GetGraphicIndex(AName: string): Integer;
    function GetGraphic(AName: string): TGraphic;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure UseNiceDefaultFont(var LazHelpPaintInfo: TLazHelpPaintInfo);
    procedure PaintAt(var LazHelpPaintInfo: TLazHelpPaintInfo);
    function GetCursorAt(X, Y: Integer): TCursor;
    function ProcessClickAt(X, Y: Integer): Boolean;
    function GetLinkElementAt(X, Y: Integer): TLazHelpElement;
    function FindPageByName(AName: string): TLazHelpElement;
    procedure LoadFromStream(AStream: TStream);
    procedure LoadFromFile(AFileName: string);
    procedure ReloadFile;
    procedure RegisterGraphic(AName: string; AGraphic: TGraphic);
    procedure UnregisterGraphic(AName: string);
    procedure RegisterImageList(APrefix: string; AImageList: TImageList);
    property Title: string read FTitle;
    property DefaultPage: string read FDefaultPage;
    property RootElement: TLazHelpElement read Root;
    property Pages[AIndex: Integer]: string read GetPages;
    property PageElement[AIndex: Integer]: TLazHelpElement read GetPageElement;
    property PageCount: Integer read GetPageCount;
  published
    property Page: string read GetPage write SetPage;
    property Code: string read FCode write SetCode;
    property FileName: string read FFileName write SetFileName;
    property OnLinkClick: TOnLazHelpLinkClick read FOnLinkClick write FOnLinkClick;
    property OnGetGraphic: TOnLazHelpGetGraphic read FOnGetGraphic write FOnGetGraphic;
  end;

  { TStringListLazHelpProvider }

  TStringListLazHelpProvider = class(TComponent)
  private
    FCode: TStrings;
    FLazHelp: TLazHelp;
    procedure SetCode(const AValue: TStrings);
    procedure SetLazHelp(const AValue: TLazHelp);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LazHelp: TLazHelp read FLazHelp write SetLazHelp;
    property Code: TStrings read FCode write SetCode;
  end;

  { TCustomLazHelpControl }

  TCustomLazHelpControl = class(TCustomControl)
  private
    FAutoHeight: Boolean;
    FBackgroundColor: TColor;
    FHeadingColor: TColor;
    FLazHelp: TLazHelp;
    FLinkColor: TColor;
    FOnLinkHandlerQuery: TOnLazHelpLinkHandlerQuery;
    FPage: string;
    FDefaultCursor: TCursor;
    FOnPageTitle: TOnLazHelpPageTitle;
    FOnPageChanged: TNotifyEvent;
    FScrollBar: TScrollBar;
    FFollowLinks: Boolean;
    FTextColor: TColor;
    FLazHelpWindowedViewer: TLazHelpWindowedViewer;
    MX, MY: Integer;
    HistPage: array of string;
    HistScroll, HistMax: array of Integer;
    HistPos: Integer;
    procedure SaveHistory;
    procedure SetAutoHeight(const AValue: Boolean);
    procedure SetBackgroundColor(const AValue: TColor);
    procedure SetFollowLinks(const AValue: Boolean);
    procedure SetHeadingColor(const AValue: TColor);
    procedure SetLazHelp(const AValue: TLazHelp);
    procedure SetLinkColor(const AValue: TColor);
    procedure SetPage(const AValue: string);
    procedure ScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure SetTextColor(const AValue: TColor);
  protected
    procedure Paint; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
      MousePos: TPoint): Boolean; override;
    procedure Loaded; override;
    procedure Click; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function CanGoBack: Boolean;
    function CanGoForward: Boolean;
    procedure GoBack;
    procedure GoForward;
  published
    property Page: string read FPage write SetPage;
    property LazHelp: TLazHelp read FLazHelp write SetLazHelp;
    property FollowLinks: Boolean read FFollowLinks write SetFollowLinks default True;
    property TextColor: TColor read FTextColor write SetTextColor default clBlack;
    property BackgroundColor: TColor read FBackgroundColor write SetBackgroundColor default clWhite;
    property LinkColor: TColor read FLinkColor write SetLinkColor default $556E00;
    property HeadingColor: TColor read FHeadingColor write SetHeadingColor default $1900A1;
    property AutoHeight: Boolean read FAutoHeight write SetAutoHeight default False;
    property LazHelpWindowedViewer: TLazHelpWindowedViewer read FLazHelpWindowedViewer write FLazHelpWindowedViewer;
    property BorderStyle default bsSingle;
    property OnPageTitle: TOnLazHelpPageTitle read FOnPageTitle write FOnPageTitle;
    property OnPageChanged: TNotifyEvent read FOnPageChanged write FOnPageChanged;
    property OnLinkHandlerQuery: TOnLazHelpLinkHandlerQuery read FOnLinkHandlerQuery write FOnLinkHandlerQuery;
  end;

  { TLazHelpViewer }

  TLazHelpViewer = class(TCustomLazHelpControl)
  published
    property Align;
    property Anchors;
    property BorderSpacing;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property Hint;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Visible;
    property Page;
    property LazHelp;
    property FollowLinks;
    property TextColor;
    property BackgroundColor;
    property LinkColor;
    property HeadingColor;
    property AutoHeight;
    property LazHelpWindowedViewer;
    property BorderStyle default bsSingle;
    property Width default 300;
    property Height default 200;
    property OnChangeBounds;
    property OnClick;
    property OnContextPopup;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnResize;
    property OnStartDrag;
    property OnPageTitle;
    property OnPageChanged;
  end;

  { TLazHelpWindowedViewer }

  TLazHelpWindowedViewer = class(TComponent)
  private
    FBackgroundColor: TColor;
    FBorderStyle: TBorderStyle;
    FFollowLinks: Boolean;
    FFont: TFont;
    FHeadingColor: TColor;
    FHelpForm: TForm;
    FLazHelp: TLazHelp;
    FLinkColor: TColor;
    FOnPageChanged: TNotifyEvent;
    FOnPageTitle: TOnLazHelpPageTitle;
    FPage: string;
    FRegisterHelpManager: Boolean;
    FShowButtons: Boolean;
    FTextColor: TColor;
    FWindowHeight: Integer;
    FWindowWidth: Integer;
    FPrevHelpManager, FHelpManager: Pointer;
    procedure SetBackgroundColor(const AValue: TColor);
    procedure SetBorderStyle(const AValue: TBorderStyle);
    procedure SetFollowLinks(const AValue: Boolean);
    procedure SetFont(const AValue: TFont);
    procedure SetHeadingColor(const AValue: TColor);
    procedure SetLazHelp(const AValue: TLazHelp);
    procedure SetLinkColor(const AValue: TColor);
    procedure SetOnPageChanged(const AValue: TNotifyEvent);
    procedure SetOnPageTitle(const AValue: TOnLazHelpPageTitle);
    procedure SetPage(const AValue: string);
    procedure SetRegisterHelpManager(const AValue: Boolean);
    procedure SetShowButtons(const AValue: Boolean);
    procedure SetTextColor(const AValue: TColor);
    procedure SetWindowHeight(const AValue: Integer);
    procedure SetWindowWidth(const AValue: Integer);
    procedure RegisterLazHelpManager;
    procedure UnregisterLazHelpManager;
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ShowHelp;
    procedure ShowHelp(APage: string);
    procedure Refresh;
    procedure Hide;
    function IsVisible: Boolean;
  published
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
    property Page: string read FPage write SetPage;
    property LazHelp: TLazHelp read FLazHelp write SetLazHelp;
    property FollowLinks: Boolean read FFollowLinks write SetFollowLinks default True;
    property TextColor: TColor read FTextColor write SetTextColor default clBlack;
    property BackgroundColor: TColor read FBackgroundColor write SetBackgroundColor default clWhite;
    property LinkColor: TColor read FLinkColor write SetLinkColor default $556E00;
    property HeadingColor: TColor read FHeadingColor write SetHeadingColor default $1900A1;
    property WindowWidth: Integer read FWindowWidth write SetWindowWidth default 450;
    property WindowHeight: Integer read FWindowHeight write SetWindowHeight default 500;
    property RegisterHelpManager: Boolean read FRegisterHelpManager write SetRegisterHelpManager;
    property ShowButtons: Boolean read FShowButtons write SetShowButtons default True;
    property Font: TFont read FFont write SetFont;
    property OnPageTitle: TOnLazHelpPageTitle read FOnPageTitle write SetOnPageTitle;
    property OnPageChanged: TNotifyEvent read FOnPageChanged write SetOnPageChanged;
  end;

  TLazHelpPopupViewerLinkAction = (laClosePopupViewer, laShowInWindowedViewer, laCallHelpManager);
  TLazHelpPopupViewerHideEvent = (hevClickOnPopup, hevLeavePopup);
  TLazHelpPopupViewerHideEvents = set of TLazHelpPopupViewerHideEvent;

  { TLazHelpPopupViewer }

  TLazHelpPopupViewer = class(TComponent)
  private
    FBackgroundColor: TColor;
    FFollowLinks: Boolean;
    FFont: TFont;
    FHeadingColor: TColor;
    FLazHelp: TLazHelp;
    FLazHelpWindowedViewer: TLazHelpWindowedViewer;
    FLinkAction: TLazHelpPopupViewerLinkAction;
    FLinkColor: TColor;
    FOnLinkHandlerQuery: TOnLazHelpLinkHandlerQuery;
    FPage: string;
    FPopupWidth: Integer;
    FTextColor: TColor;
    FPopupForm: TForm;
    FHideEvents: TLazHelpPopupViewerHideEvents;
    procedure SetBackgroundColor(const AValue: TColor);
    procedure SetFont(const AValue: TFont);
    procedure SetHeadingColor(const AValue: TColor);
    procedure SetLazHelp(const AValue: TLazHelp);
    procedure SetLazHelpWindowedViewer(const AValue: TLazHelpWindowedViewer);
    procedure SetLinkAction(const AValue: TLazHelpPopupViewerLinkAction);
    procedure SetLinkColor(const AValue: TColor);
    procedure SetPage(const AValue: string);
    procedure SetPopupWidth(const AValue: Integer);
    procedure SetTextColor(const AValue: TColor);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ShowPageAt(APage: string; X, Y: Integer);
    procedure Hide;
    function IsVisible: Boolean;
  published
    property Page: string read FPage write SetPage;
    property LazHelp: TLazHelp read FLazHelp write SetLazHelp;
    property LazHelpWindowedViewer: TLazHelpWindowedViewer read FLazHelpWindowedViewer write SetLazHelpWindowedViewer;
    property FollowLinks: Boolean read FFollowLinks write FFollowLinks default True;
    property LinkAction: TLazHelpPopupViewerLinkAction read FLinkAction write SetLinkAction default laClosePopupViewer;
    property TextColor: TColor read FTextColor write SetTextColor default clInfoText;
    property BackgroundColor: TColor read FBackgroundColor write SetBackgroundColor default clInfoBk;
    property LinkColor: TColor read FLinkColor write SetLinkColor default $556E00;
    property HeadingColor: TColor read FHeadingColor write SetHeadingColor default $1900A1;
    property PopupWidth: Integer read FPopupWidth write SetPopupWidth default 300;
    property HideEvents: TLazHelpPopupViewerHideEvents read FHideEvents write FHideEvents default [hevClickOnPopup, hevLeavePopup];
    property Font: TFont read FFont write SetFont;
    property OnLinkHandlerQuery: TOnLazHelpLinkHandlerQuery read FOnLinkHandlerQuery write FOnLinkHandlerQuery;
  end;

procedure Register;

implementation

uses
  Dialogs, ExtCtrls, ComCtrls, HelpIntfs, Menus, LCLType, GraphType, ImgList;

type
  { THelpForm }

  THelpForm = class(TForm)
  private
    FLazHelpViewer: TLazHelpViewer;
    FButtonPanel: TPanel;
    FOuterPanel: TPanel;
    FPopupMenu: TPopupMenu;
    FOutline: TTreeView;
    FSplitter: TSplitter;
    Viewer: TLazHelpWindowedViewer;
    FOnPageTitle: TOnLazHelpPageTitle;
    FOnPageChanged: TNotifyEvent;
    procedure OnTitle(Sender: TObject; ATitle: string);
    procedure OnPageChange(Sender: TObject);
    procedure OutlineSelectionChanged(Sender: TObject);
    procedure OutlineGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure ButtonClick(Sender: TObject);
    procedure ScanForButtons;
    procedure ScanForOutline;
    procedure UpdateOutlinePage;
    function AddButton(ACaption: string; ATag: Integer): TButton;
  protected
    procedure DoShow; override;
  public
    constructor CreateNew(AOwner: TComponent; Num: Integer=0); override;
  end;

  { TPopupForm }

  TPopupForm = class(TForm)
  private
    Viewer: TLazHelpPopupViewer;
    MX, MY: Integer;
  protected
    procedure Paint; override;
    procedure Deactivate; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Click; override;
  public
    constructor CreateNew(AOwner: TComponent; Num: Integer=0); override;
  end;

  { TLazHelpManager }
  TLazHelpManager = class(THelpManager)
  private
    FLazHelpWindowedViewer: TLazHelpWindowedViewer;
  public
    constructor Create(ALazHelpWindowedViewer: TLazHelpWindowedViewer);

    procedure ShowError(ShowResult: TShowHelpResult; const ErrMsg: string); override;
    function ShowHelpForQuery(Query: THelpQuery; AutoFreeQuery: boolean;
       var ErrMsg: string): TShowHelpResult; override;
    function ShowHelpForContext(Query: THelpQueryContext; var ErrMsg: string): TShowHelpResult; override;
    function ShowHelpForKeyword(Query: THelpQueryKeyword; var ErrMsg: string): TShowHelpResult; override;
    function ShowTableOfContents(var ErrMsg: string): TShowHelpResult; override;
  end;

var
  Icons: TImageList;

procedure Register;
begin
  RegisterComponents('LazHelp', [TLazHelp, TStringListLazHelpProvider,
    TLazHelpViewer, TLazHelpWindowedViewer, TLazHelpPopupViewer]);
end;

{ THelpPopupForm }

procedure TPopupForm.Paint;
var
  BgColor: TColor;
  PaintInfo: TLazHelpPaintInfo;
begin
  inherited Paint;
  BgColor:=ColorToRGB(Viewer.BackgroundColor);
  PaintInfo.Prepare(Canvas, Rect(0, 0, Width, Height), 0);
  Canvas.Font.Assign(Viewer.Font);
  with PaintInfo do begin
    Color:=Viewer.TextColor;
    BackgroundColor:=Viewer.BackgroundColor;
    if Viewer.FollowLinks then
      LinkColor:=Viewer.LinkColor
    else
      LinkColor:=Viewer.TextColor;
    HeadColor:=Viewer.HeadingColor;
  end;
  if Assigned(Viewer.LazHelp) then begin
    Viewer.LazHelp.Page:=Viewer.Page;
    Viewer.LazHelp.PaintAt(PaintInfo);
    ClientHeight:=PaintInfo.PageHeight;
  end;
  {$IFNDEF DARWIN}
  // The carbon widgetset crashes here. Also the outline doesn't look good with
  // the shadows and such.
  with Canvas do begin
    Pen.Style:=psSolid;
    Pen.Color:=RGBToColor(Red(BgColor) div 2, Green(BgColor) div 2, Blue(BgColor) div 2);
    Brush.Style:=bsClear;
    Clipping:=False;
    Rectangle(0, 0, Width, Height);
  end;
  {$ENDIF}
end;

procedure TPopupForm.Deactivate;
begin
  inherited Deactivate;
  if hevLeavePopup in Viewer.HideEvents then Hide;
end;

procedure TPopupForm.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);

  if not Viewer.FollowLinks then Exit;

  MX:=X;
  MY:=Y;
  Cursor:=Viewer.LazHelp.GetCursorAt(X, Y);
end;

procedure TPopupForm.Click;
var
  Link: TLazHelpElement;
  Handler: TLazHelpLinkHandler;
  LinkAction: TLazHelpPopupViewerLinkAction;
begin
  inherited Click;
  Link:=nil;
  if Viewer.FollowLinks then Link:=Viewer.LazHelp.GetLinkElementAt(MX, MY);
  if Link=nil then begin
    if hevClickOnPopup in Viewer.HideEvents then Hide;
    Viewer.LazHelp.ProcessClickAt(MX, MY);
    Exit;
  end;

  LinkAction:=Viewer.LinkAction;

  if Assigned(Viewer.FOnLinkHandlerQuery) then begin
    case LinkAction of
      laShowInWindowedViewer: Handler:=ltWindowedViewer;
      laCallHelpManager: Handler:=ltHelpManager;
    end;
    Viewer.FOnLinkHandlerQuery(Viewer, Link.Value, Handler);
    case Handler of
      ltThisViewer: LinkAction:=laClosePopupViewer;
      ltWindowedViewer: LinkAction:=laShowInWindowedViewer;
      ltHelpManager: LinkAction:=laCallHelpManager;
    end;
  end;

  case LinkAction of
    laClosePopupViewer: Hide;
    laShowInWindowedViewer: begin
      Hide;
      if Assigned(Viewer.FLazHelpWindowedViewer) then Viewer.LazHelpWindowedViewer.ShowHelp(Link.Value);
    end;
    laCallHelpManager: begin
      Hide;
      Application.HelpKeyword(Link.Value);
    end;
  end;
end;

constructor TPopupForm.CreateNew(AOwner: TComponent; Num: Integer);
begin
  inherited CreateNew(AOwner);
  Viewer:=AOwner as TLazHelpPopupViewer;
  Visible:=False;
  BorderStyle:=bsNone;
  Position:=poDesigned;
  Width:=450;
  Height:=500;
end;

{ TLazHelpPopupViewer }

procedure TLazHelpPopupViewer.SetBackgroundColor(const AValue: TColor);
begin
  if FBackgroundColor=AValue then exit;
  FBackgroundColor:=AValue;
end;

procedure TLazHelpPopupViewer.SetFont(const AValue: TFont);
begin
  FFont.Assign(AValue);
end;

procedure TLazHelpPopupViewer.SetHeadingColor(const AValue: TColor);
begin
  if FHeadingColor=AValue then exit;
  FHeadingColor:=AValue;
end;

procedure TLazHelpPopupViewer.SetLazHelp(const AValue: TLazHelp);
begin
  if FLazHelp=AValue then exit;
  FLazHelp:=AValue;
  if FPage='' then FPage:=FLazHelp.DefaultPage;
end;

procedure TLazHelpPopupViewer.SetLazHelpWindowedViewer(
  const AValue: TLazHelpWindowedViewer);
begin
  if FLazHelpWindowedViewer=AValue then exit;
  FLazHelpWindowedViewer:=AValue;
  if not Assigned(FLazHelp) then FLazHelp:=AValue.LazHelp;
end;

procedure TLazHelpPopupViewer.SetLinkAction(
  const AValue: TLazHelpPopupViewerLinkAction);
begin
  if FLinkAction=AValue then exit;
  FLinkAction:=AValue;
end;

procedure TLazHelpPopupViewer.SetLinkColor(const AValue: TColor);
begin
  if FLinkColor=AValue then exit;
  FLinkColor:=AValue;
end;

procedure TLazHelpPopupViewer.SetPage(const AValue: string);
begin
  if FPage=AValue then exit;
  FPage:=AValue;
end;

procedure TLazHelpPopupViewer.SetPopupWidth(const AValue: Integer);
begin
  if FPopupWidth=AValue then exit;
  FPopupWidth:=AValue;
end;

procedure TLazHelpPopupViewer.SetTextColor(const AValue: TColor);
begin
  if FTextColor=AValue then exit;
  FTextColor:=AValue;
end;

constructor TLazHelpPopupViewer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPage:='';
  FFollowLinks:=True;
  FLinkAction:=laClosePopupViewer;
  FTextColor:=clInfoText;
  FBackgroundColor:=clInfoBk;
  FLinkColor:=$556E00;
  FHeadingColor:=$1900A1;
  FPopupWidth:=300;
  FFont:=TFont.Create;
  FHideEvents:=[hevClickOnPopup, hevLeavePopup];
  if not (csDesigning in ComponentState) then FPopupForm:=TPopupForm.CreateNew(Self);
end;

destructor TLazHelpPopupViewer.Destroy;
begin
  FreeAndNil(FFont);
  FreeAndNil(FPopupForm);
  inherited Destroy;
end;

procedure TLazHelpPopupViewer.ShowPageAt(APage: string; X, Y: Integer);
begin
  FPopupForm.FormStyle:=fsStayOnTop;
  FPopupForm.Left:=X;
  FPopupForm.Top:=Y;
  FPopupForm.Width:=FPopupWidth;
  FPopupForm.Height:=200;
  FPopupForm.Color:=FBackgroundColor;
  FPopupForm.Font.Assign(FFont);
  try // sad hack to avoid crashes with the Carbon widgetset
    FPopupForm.Show;
  except
  end;
end;

procedure TLazHelpPopupViewer.Hide;
begin
  FPopupForm.Hide;
end;

function TLazHelpPopupViewer.IsVisible: Boolean;
begin
  Result:=FPopupForm.Visible;
end;

{ TLazHelpManager }

constructor TLazHelpManager.Create(
  ALazHelpWindowedViewer: TLazHelpWindowedViewer);
begin
  FLazHelpWindowedViewer:=ALazHelpWindowedViewer;
end;

procedure TLazHelpManager.ShowError(ShowResult: TShowHelpResult;
  const ErrMsg: string);
begin
  if ShowResult <> shrSuccess then MessageDlg('Help Error', ErrMsg, mtError, [mbOK], 0);
end;

function TLazHelpManager.ShowHelpForQuery(Query: THelpQuery;
  AutoFreeQuery: boolean; var ErrMsg: string): TShowHelpResult;
begin
  if Query is THelpQueryContext then begin
    Result:=ShowHelpForContext(THelpQueryContext(Query), ErrMsg);
    if AutoFreeQuery then Query.Free;
  end else if Query is THelpQueryKeyword then begin
    Result:=ShowHelpForKeyword(THelpQueryKeyword(Query), ErrMsg);
    if AutoFreeQuery then Query.Free;
  end else
    Result:=inherited ShowHelpForQuery(Query, AutoFreeQuery, ErrMsg);
end;

function TLazHelpManager.ShowHelpForContext(Query: THelpQueryContext;
  var ErrMsg: string): TShowHelpResult;
begin
  FLazHelpWindowedViewer.ShowHelp('hc' + IntToStr(Query.Context));
  Result:=shrSuccess;
end;

function TLazHelpManager.ShowHelpForKeyword(Query: THelpQueryKeyword;
  var ErrMsg: string): TShowHelpResult;
begin
  FLazHelpWindowedViewer.ShowHelp(Query.Keyword);
  Result:=shrSuccess;
end;

function TLazHelpManager.ShowTableOfContents(var ErrMsg: string
  ): TShowHelpResult;
begin
  FLazHelpWindowedViewer.ShowHelp;
  Result:=shrSuccess;
end;

{ THelpForm }

procedure THelpForm.OnTitle(Sender: TObject; ATitle: string);
begin
  if Assigned(FOnPageTitle) then FOnPageTitle(Owner, ATitle);
  Caption:='Help - ' + ATitle;
end;

procedure THelpForm.OnPageChange(Sender: TObject);
begin
  TLazHelpWindowedViewer(Owner).FPage:=FLazHelpViewer.Page;
  if Assigned(FOnPageChanged) then FOnPageChanged(Owner);
  UpdateOutlinePage;
end;

procedure THelpForm.OutlineSelectionChanged(Sender: TObject);
var
  Target: TLazHelpElement;
begin
  if not Assigned(FOutline.Selected) then Exit;
  Target:=TLazHelpElement(FOutline.Selected.Data);
  if Assigned(Target) then
    Viewer.SetPage(Target.Value)
  else
    MessageDlg('LazHelp Error', 'Page not found', mtError, [mbOK], 0);
end;

procedure THelpForm.OutlineGetImageIndex(Sender: TObject; Node: TTreeNode);
begin
  if Node.Count > 0 then begin
    if Node.Expanded then
      Node.ImageIndex:=0
    else
      Node.ImageIndex:=1;
  end else
    Node.ImageIndex:=2;
  Node.SelectedIndex:=Node.ImageIndex;
end;

procedure THelpForm.ButtonClick(Sender: TObject);
var
  Button: TComponent;
begin
  Button:=Sender as TComponent;
  if Button.Tag=1 then begin // Back
    FLazHelpViewer.GoBack;
  end else if Button.Tag=2 then begin // Forward
    FLazHelpViewer.GoForward;
  end else if Button.Tag=3 then begin // Home
    FLazHelpViewer.Page:=Viewer.LazHelp.DefaultPage;
  end else if Button.Tag >= 100 then begin
    FLazHelpViewer.Page:=Viewer.LazHelp.RootElement.Children[Button.Tag - 100].Value;
  end;
  FLazHelpViewer.SetFocus;
end;

procedure THelpForm.ScanForButtons;
var
  i: Integer;
  Button: TButton;
  MenuItem: TMenuItem;
begin
  while FButtonPanel.ControlCount > 3 do begin
    Button:=FButtonPanel.Controls[3] as TButton;
    FButtonPanel.RemoveControl(Button);
    FreeAndNil(Button);
  end;
  while FPopupMenu.Items.Count > 3 do begin
    MenuItem:=FPopupMenu.Items[3];
    FPopupMenu.Items.Remove(MenuItem);
    FreeAndNil(MenuItem);
  end;
  if Assigned(Viewer.LazHelp) and Assigned(Viewer.LazHelp.RootElement) then begin
    for i:=0 to Viewer.LazHelp.RootElement.Count - 1 do begin
      if Viewer.LazHelp.RootElement.Children[i].Name='button' then begin
        Button:=AddButton(Viewer.LazHelp.RootElement.Children[i].Text, 100 + i);
      end;
    end;
  end;
end;

procedure THelpForm.ScanForOutline;
var
  OFirst: TLazHelpElement;

  procedure ScanElement(E: TLazHelpElement; TN: TTreeNode);
  var
    SubTN: TTreeNode;
    I: Integer;
    Target, Title: TLazHelpElement;
  begin
    for I:=0 to E.Count - 1 do begin
      if E[I].Name <> 'o' then Continue;
      SubTN:=FOutline.Items.AddChild(TN, '');
      Target:=Viewer.LazHelp.FindPageByName(E[I].Value);
      SubTN.Data:=Target;
      if Assigned(Target) then begin
        Title:=Target.FindElementByName('title');
        if Assigned(Title) then
          SubTN.Text:=Title.Value
        else
          SubTN.Text:=Target.Value;
      end;
      ScanElement(E[I], SubTN);
    end;
  end;

begin
  OFirst:=nil;
  if Assigned(Viewer.LazHelp) and Assigned(Viewer.LazHelp.RootElement) then
    OFirst:=Viewer.LazHelp.RootElement.FindElementByName('o');
  if not Assigned(OFirst) then begin
    FreeAndNil(FSplitter);
    FreeAndNil(FOutline);
    Exit;
  end;
  if not Assigned(FOutline) then begin
    if not Assigned(Icons) then begin
      Icons:=TImageList.Create(Nil);
      Icons.Width:=16;
      Icons.Height:=16;
      Icons.AddLazarusResource('OpenBookLazHelpIcon', RGBToColor(0, 255, 255));
      Icons.AddLazarusResource('ClosedBookLazHelpIcon', RGBToColor(0, 255, 255));
      Icons.AddLazarusResource('PageLazHelpIcon', RGBToColor(0, 255, 255));
    end;
    FOutline:=TTreeView.Create(Self);
    FOutline.Align:=alLeft;
    FOutline.Width:=220;
    FOutline.ReadOnly:=True;
    FOutline.Images:=Icons;
    FOutline.StateImages:=Icons;
    FOutline.ShowButtons:=False;
    FOutline.ShowRoot:=False;
    FOutline.OnSelectionChanged:=@OutlineSelectionChanged;
    FOutline.OnGetImageIndex:=@OutlineGetImageIndex;
    FOutline.OnGetSelectedIndex:=@OutlineGetImageIndex;
    InsertControl(FOutline);
    FSplitter:=TSplitter.Create(Self);
    FSplitter.Align:=alLeft;
    InsertControl(FSplitter);
    FSplitter.Left:=FOutline.Width + 16;
  end;
  FOutline.Items.Clear;
  ScanElement(OFirst.Parent, nil);
end;

procedure THelpForm.UpdateOutlinePage;
var
  PageEl: TLazHelpElement;
  TN: TTreeNode;
begin
  if Assigned(FOutline) then begin
    PageEl:=FLazHelpViewer.LazHelp.FindPageByName(FLazHelpViewer.Page);
    if Assigned(PageEl) then begin
      TN:=FOutline.Items.FindNodeWithData(PageEl);
      if Assigned(TN) then begin
        TN.ExpandParents;
        TN.Expanded:=True;
        FOutline.OnSelectionChanged:=nil;
        FOutline.Selected:=TN;
        FOutline.OnSelectionChanged:=@OutlineSelectionChanged;
      end;
    end;
  end;
end;

function THelpForm.AddButton(ACaption: string; ATag: Integer): TButton;
var
  MenuItem: TMenuItem;
begin
  MenuItem:=TMenuItem.Create(FPopupMenu);
  MenuItem.Caption:=ACaption;
  MenuItem.OnClick:=@ButtonClick;
  MenuItem.Tag:=ATag;
  if ATag=1 then MenuItem.ShortCut:=ShortCut(VK_LEFT, [ssCtrl]);
  if ATag=2 then MenuItem.ShortCut:=ShortCut(VK_RIGHT, [ssCtrl]);
  FPopupMenu.Items.Add(MenuItem);
  Result:=TButton.Create(FButtonPanel);
  Result.AutoSize:=True;
  Result.Caption:=ACaption;
  Result.OnClick:=@ButtonClick;
  Result.TabStop:=False;
  Result.Tag:=ATag;
  FButtonPanel.InsertControl(Result);
  {$IFDEF DARWIN}
  // the height with the Carbon widgetset is not set correctly and is off by
  // one or two pixels
  FButtonPanel.AutoSize:=False;
  FButtonPanel.Height:=Result.Height;
  {$ENDIF}
end;

procedure THelpForm.DoShow;
begin
  inherited DoShow;
  Position:=poDefaultPosOnly;
end;

constructor THelpForm.CreateNew(AOwner: TComponent; Num: Integer);
begin
  inherited CreateNew(AOwner, Num);
  Visible:=False;
  Viewer:=AOwner as TLazHelpWindowedViewer;
  FOuterPanel:=TPanel.Create(Self);
  FOuterPanel.Align:=alClient;
  FOuterPanel.BorderStyle:=bsSingle;
  FOuterPanel.BevelOuter:=bvNone;
  FOuterPanel.BevelOuter:=bvNone;
  InsertControl(FOuterPanel);
  Caption:='Help';
  Position:=poMainFormCenter;
  Width:=450;
  Height:=500;
  FButtonPanel:=TPanel.Create(FOuterPanel);
  FButtonPanel.Align:=alTop;
  FButtonPanel.Caption:='';
  FButtonPanel.BorderStyle:=bsNone;
  FButtonPanel.BevelInner:=bvNone;
  FButtonPanel.BevelOuter:=bvNone;
  FOuterPanel.InsertControl(FButtonPanel);
  FLazHelpViewer:=TLazHelpViewer.Create(FOuterPanel);
  FLazHelpViewer.BorderStyle:=bsNone;
  FLazHelpViewer.Align:=alClient;
  FLazHelpViewer.OnPageTitle:=@OnTitle;
  FLazHelpViewer.OnPageChanged:=@OnPageChange;
  FPopupMenu:=TPopupMenu.Create(Self);
  FLazHelpViewer.PopupMenu:=FPopupMenu;
  FOuterPanel.InsertControl(FLazHelpViewer);
  FButtonPanel.ChildSizing.Layout:=cclTopToBottomThenLeftToRight;
  FButtonPanel.AutoSize:=True;
  FButtonPanel.Visible:=Viewer.ShowButtons;
  AddButton('&Back', 1);
  AddButton('&Forward', 2);
  AddButton('&Home', 3);
end;

{ TLazHelpWindowedViewer }

procedure TLazHelpWindowedViewer.SetBackgroundColor(const AValue: TColor);
begin
  if FBackgroundColor=AValue then exit;
  FBackgroundColor:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.BackgroundColor:=AValue;
end;

procedure TLazHelpWindowedViewer.SetBorderStyle(const AValue: TBorderStyle);
begin
  if FBorderStyle=AValue then exit;
  FBorderStyle:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.BorderStyle:=AValue;
end;

procedure TLazHelpWindowedViewer.SetFollowLinks(const AValue: Boolean);
begin
  if FFollowLinks=AValue then exit;
  FFollowLinks:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.FollowLinks:=AValue;
end;

procedure TLazHelpWindowedViewer.SetFont(const AValue: TFont);
begin
  FFont.Assign(AValue);
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.Font.Assign(FFont);
end;

procedure TLazHelpWindowedViewer.SetHeadingColor(const AValue: TColor);
begin
  if FHeadingColor=AValue then exit;
  FHeadingColor:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.HeadingColor:=AValue;
end;

procedure TLazHelpWindowedViewer.SetLazHelp(const AValue: TLazHelp);
begin
  if FLazHelp=AValue then exit;
  FLazHelp:=AValue;
  if Assigned(FHelpForm) then begin
    if FPage='' then FPage:=FLazHelp.DefaultPage;
    THelpForm(FHelpForm).FLazHelpViewer.LazHelp:=AValue;
    THelpForm(FHelpForm).FLazHelpViewer.Page:=FPage;
    THelpForm(FHelpForm).ScanForButtons;
    THelpForm(FHelpForm).ScanForOutline;
  end;
end;

procedure TLazHelpWindowedViewer.SetLinkColor(const AValue: TColor);
begin
  if FLinkColor=AValue then exit;
  FLinkColor:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.LinkColor:=AValue;
end;

procedure TLazHelpWindowedViewer.SetOnPageChanged(const AValue: TNotifyEvent);
begin
  if FOnPageChanged=AValue then Exit;
  FOnPageChanged:=AValue;
  if Assigned(FHelpForm) then begin
    THelpForm(FHelpForm).FOnPageChanged:=AValue;
  end;
end;

procedure TLazHelpWindowedViewer.SetOnPageTitle(
  const AValue: TOnLazHelpPageTitle);
begin
  if FOnPageTitle=AValue then Exit;
  FOnPageTitle:=AValue;
  if Assigned(FHelpForm) then begin
    THelpForm(FHelpForm).FOnPageTitle:=AValue;
  end;
end;

procedure TLazHelpWindowedViewer.SetPage(const AValue: string);
begin
  if FPage=AValue then exit;
  FPage:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.Page:=AValue;
end;

procedure TLazHelpWindowedViewer.SetRegisterHelpManager(const AValue: Boolean);
begin
  if FRegisterHelpManager=AValue then exit;
  FRegisterHelpManager:=AValue;
  if AValue then
    RegisterLazHelpManager
  else
    UnregisterLazHelpManager;
end;

procedure TLazHelpWindowedViewer.SetShowButtons(const AValue: Boolean);
begin
  if FShowButtons=AValue then exit;
  FShowButtons:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FButtonPanel.Visible:=AValue;
end;

procedure TLazHelpWindowedViewer.SetTextColor(const AValue: TColor);
begin
  if FTextColor=AValue then exit;
  FTextColor:=AValue;
  if Assigned(FHelpForm) then THelpForm(FHelpForm).FLazHelpViewer.TextColor:=AValue;
end;

procedure TLazHelpWindowedViewer.SetWindowHeight(const AValue: Integer);
begin
  if FWindowHeight=AValue then exit;
  FWindowHeight:=AValue;
  if Assigned(FHelpForm) then FHelpForm.ClientHeight:=AValue;
end;

procedure TLazHelpWindowedViewer.SetWindowWidth(const AValue: Integer);
begin
  if FWindowWidth=AValue then exit;
  FWindowWidth:=AValue;
  if Assigned(FHelpForm) then begin
    if Assigned(THelpForm(FHelpForm).FOutline) then
      FHelpForm.ClientWidth:=AValue + 220 + THelpForm(FHelpForm).FSplitter.Width
    else
      FHelpForm.ClientWidth:=AValue;
  end;
end;

procedure TLazHelpWindowedViewer.RegisterLazHelpManager;
begin
  if csDesigning in ComponentState then Exit;
  FPrevHelpManager:=HelpManager;
  HelpManager:=TLazHelpManager.Create(Self);
  FHelpManager:=HelpManager;
end;

procedure TLazHelpWindowedViewer.UnregisterLazHelpManager;
begin
  if csDesigning in ComponentState then Exit;
  if (FPrevHelpManager <> nil) and (HelpManager=THelpManager(FHelpManager)) then HelpManager:=THelpManager(FPrevHelpManager);
  if FHelpManager <> nil then TLazHelpManager(FHelpManager).Free;
  FHelpManager:=nil;
  FPrevHelpManager:=nil;
end;

procedure TLazHelpWindowedViewer.Loaded;
begin
  inherited Loaded;
  if Assigned(FHelpForm) then begin
    if Assigned(THelpForm(FHelpForm).FOutline) then
      FHelpForm.ClientWidth:=WindowWidth + 220 + THelpForm(FHelpForm).FSplitter.Width
    else
      FHelpForm.ClientWidth:=WindowWidth;
    THelpForm(FHelpForm).ClientHeight:=WindowHeight;
  end;
end;

constructor TLazHelpWindowedViewer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  if not (csDesigning in ComponentState) then FHelpForm:=THelpForm.CreateNew(Self, 0);
  FBorderStyle:=bsSingle;
  FFollowLinks:=True;
  FPage:='';
  FTextColor:=clBlack;
  FBackgroundColor:=clWhite;
  FLinkColor:=$556E00;
  FHeadingColor:=$1900A1;
  FWindowWidth:=450;
  FWindowHeight:=500;
  FShowButtons:=True;
  FRegisterHelpManager:=False;
  FFont:=TFont.Create;
end;

destructor TLazHelpWindowedViewer.Destroy;
begin
  if FHelpManager <> nil then UnregisterLazHelpManager;
  FreeAndNil(FFont);
  inherited Destroy;
end;

procedure TLazHelpWindowedViewer.ShowHelp;
begin
  if not Assigned(FLazHelp) then Exit;
  ShowHelp(FLazHelp.DefaultPage);
end;

procedure TLazHelpWindowedViewer.ShowHelp(APage: string);
var
  HadOutline: Boolean;
begin
  if not Assigned(FLazHelp) then Exit;
  THelpForm(FHelpForm).FLazHelpViewer.Font.Assign(FFont);
  THelpForm(FHelpForm).FLazHelpViewer.Page:=APage;
  THelpForm(FHelpForm).ScanForButtons;
  HadOutline:=Assigned(THelpForm(FHelpForm).FOutline);
  THelpForm(FHelpForm).ScanForOutline;
  if HadOutline <> Assigned(THelpForm(FHelpForm).FOutline) then begin
    if Assigned(THelpForm(FHelpForm).FOutline) then
      FHelpForm.ClientWidth:=WindowWidth+ 220 + THelpForm(FHelpForm).FSplitter.Width
    else
      FHelpForm.ClientWidth:=WindowWidth;
    FHelpForm.ClientHeight:=WindowHeight;
  end;
  THelpForm(FHelpForm).UpdateOutlinePage;
  THelpForm(FHelpForm).FButtonPanel.Visible:=ShowButtons;
  FHelpForm.Show;
  FHelpForm.BringToFront;
end;

procedure TLazHelpWindowedViewer.Refresh;
begin
  THelpForm(FHelpForm).Invalidate;
  THelpForm(FHelpForm).FLazHelpViewer.Invalidate;
end;

procedure TLazHelpWindowedViewer.Hide;
begin
  FHelpForm.Hide;
end;

function TLazHelpWindowedViewer.IsVisible: Boolean;
begin
  Result:=FHelpForm.Visible;
end;

{ TStringListLazHelpProvider }

procedure TStringListLazHelpProvider.SetCode(const AValue: TStrings);
begin
  if (AValue=nil) or (FCode.Text=AValue.Text) then Exit;
  FCode.Assign(AValue);
  if Assigned(FLazHelp) then FLazHelp.Code:=FCode.Text;
end;

procedure TStringListLazHelpProvider.SetLazHelp(const AValue: TLazHelp);
begin
  if FLazHelp=AValue then exit;
  FLazHelp:=AValue;
  FLazHelp.Code:=FCode.Text;
end;

constructor TStringListLazHelpProvider.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FCode:=TStringList.Create;
end;

destructor TStringListLazHelpProvider.Destroy;
begin
  FreeAndNil(FCode);
  inherited Destroy;
end;

{ TCustomLazHelpControl }

procedure TCustomLazHelpControl.SaveHistory;
begin
  if FPage='' then Exit;
  SetLength(HistPage, HistPos + 1);
  SetLength(HistScroll, HistPos + 1);
  SetLength(HistMax, HistPos + 1);
  HistPage[HistPos]:=FPage;
  HistScroll[HistPos]:=FScrollBar.Position;
  HistMax[HistPos]:=FScrollBar.Max;
  Inc(HistPos);
end;

procedure TCustomLazHelpControl.SetAutoHeight(const AValue: Boolean);
begin
  if FAutoHeight=AValue then exit;
  FAutoHeight:=AValue;
  Invalidate;
end;

procedure TCustomLazHelpControl.SetBackgroundColor(const AValue: TColor);
begin
  if FBackgroundColor=AValue then exit;
  FBackgroundColor:=AValue;
  Invalidate;
end;

procedure TCustomLazHelpControl.SetFollowLinks(const AValue: Boolean);
begin
  if FFollowLinks=AValue then exit;
  FFollowLinks:=AValue;
  Invalidate;
end;

procedure TCustomLazHelpControl.SetHeadingColor(const AValue: TColor);
begin
  if FHeadingColor=AValue then exit;
  FHeadingColor:=AValue;
  Invalidate;
end;

procedure TCustomLazHelpControl.SetLazHelp(const AValue: TLazHelp);
begin
  if FLazHelp=AValue then exit;
  FLazHelp:=AValue;
  LazHelp.Page:=FPage;
  Invalidate;
end;

procedure TCustomLazHelpControl.SetLinkColor(const AValue: TColor);
begin
  if FLinkColor=AValue then exit;
  FLinkColor:=AValue;
  Invalidate;
end;

procedure TCustomLazHelpControl.SetPage(const AValue: string);
begin
  if FPage=AValue then exit;
  SaveHistory;
  FPage:=AValue;
  FScrollBar.Position:=0;
  Invalidate;
  if Assigned(FOnPageChanged) then FOnPageChanged(Self);
end;

procedure TCustomLazHelpControl.ScrollBarScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  Invalidate;
end;

procedure TCustomLazHelpControl.SetTextColor(const AValue: TColor);
begin
  if FTextColor=AValue then exit;
  FTextColor:=AValue;
  Invalidate;
end;

procedure TCustomLazHelpControl.Paint;
var
  PaintInfo: TLazHelpPaintInfo;
  PrevSBVis: Boolean;
begin
  inherited Paint;
  if Assigned(FLazHelp) then begin
    FLazHelp.Page:=FPage;
    if Assigned(FOnPageTitle) then FOnPageTitle(Self, FLazHelp.Title);
    PaintInfo.Prepare(Canvas, Rect(0, 0, ClientWidth, ClientHeight), 0);
    PaintInfo.Color:=FTextColor;
    PaintInfo.BackgroundColor:=FBackgroundColor;
    if FFollowLinks then
      PaintInfo.LinkColor:=FLinkColor
    else
      PaintInfo.LinkColor:=FTextColor;
    PaintInfo.HeadColor:=FHeadingColor;
    PrevSBVis:=FScrollBar.Visible;
    if FAutoHeight then begin
      FScrollBar.Visible:=False;
      PaintInfo.Scroll:=0;
    end else begin
      if FScrollBar.Visible then PaintInfo.Rect.Right:=PaintInfo.Rect.Right - FScrollBar.Width;
      PaintInfo.Scroll:=FScrollBar.Position;
    end;
    FLazHelp.UseNiceDefaultFont(PaintInfo);
    FLazHelp.PaintAt(PaintInfo);
    if AutoHeight then begin
      if PaintInfo.PageHeight=0 then
        ClientHeight:=1
      else
        ClientHeight:=PaintInfo.PageHeight;
    end else begin
      if PaintInfo.PageHeight > ClientHeight then begin
        FScrollBar.Visible:=True;
        // Currently TScrollBar's behaviour differs in Win32 from other widgetsets
        // See Lazarus bug #16107
        {$IFDEF WINDOWS}
        FScrollBar.Max:=PaintInfo.PageHeight;
        FScrollBar.PageSize:=ClientHeight;
        if FScrollBar.Position + ClientHeight > FScrollBar.Max then FScrollBar.Position:=FScrollBar.Max - ClientHeight;
        {$ELSE}
        FScrollBar.Max:=PaintInfo.PageHeight - ClientHeight;
        FScrollBar.PageSize:=ClientHeight;
        {$ENDIF}
      end else begin
        FScrollBar.Visible:=False;
      end;
      if FScrollBar.Visible <> PrevSBVis then Paint;
    end;
  end;
  FScrollBar.LargeChange:=Height div 2;
end;

procedure TCustomLazHelpControl.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewCursor: TCursor;
begin
  inherited MouseMove(Shift, X, Y);

  if not FFollowLinks then Exit;

  MX:=X;
  MY:=Y;
  NewCursor:=crDefault;
  if Assigned(FLazHelp) then
    NewCursor:=FLazHelp.GetCursorAt(X, Y);
  if NewCursor=crDefault then
    Cursor:=FDefaultCursor
  else
    Cursor:=NewCursor;
end;

function TCustomLazHelpControl.DoMouseWheel(Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint): Boolean;
begin
  Result:=inherited DoMouseWheel(Shift, WheelDelta, MousePos);
  if FScrollBar.Visible then try
    FScrollBar.Position:=FScrollBar.Position - WheelDelta div 4;
    // Currently TScrollBar's behaviour differs in Win32 from other widgetsets
    // See Lazarus bug #16107
    {$IFDEF WINDOWS}
    if FScrollBar.Position + ClientHeight > FScrollBar.Max then FScrollBar.Position:=FScrollBar.Max - ClientHeight;
    {$ENDIF}
    Invalidate;
  except
  end;
end;

procedure TCustomLazHelpControl.Loaded;
begin
  inherited Loaded;
  FDefaultCursor:=Cursor;
  if not Assigned(FLazHelp) then
    LazHelp:=TLazHelp.Create(Self);
end;

procedure TCustomLazHelpControl.Click;
var
  Link: TLazHelpElement;
  Handler: TLazHelpLinkHandler;
begin
  inherited Click;
  if not FFollowLinks then Exit;
  if not Assigned(FLazHelp) then Exit;

  Link:=FLazHelp.GetLinkElementAt(MX, MY);

  Handler:=ltThisViewer;
  if Assigned(FLazHelpWindowedViewer) and Assigned(Link) then Handler:=ltWindowedViewer;
  if Assigned(FOnLinkHandlerQuery) then FOnLinkHandlerQuery(Self, Link.Value, Handler);
  if (Handler=ltWindowedViewer) and (not Assigned(FLazHelpWindowedViewer)) then Handler:=ltThisViewer;

  case Handler of
    ltThisViewer: if FLazHelp.ProcessClickAt(MX, MY) then begin
      SaveHistory;
      FPage:=FLazHelp.Page;
      FScrollBar.Position:=0;
      Invalidate;
      if Assigned(FOnPageChanged) then FOnPageChanged(Self);
      MouseMove([], MX, MY);
    end;
    ltWindowedViewer: FLazHelpWindowedViewer.ShowHelp(Link.Value);
    ltHelpManager: Application.HelpKeyword(Link.Value);
  end;
end;

constructor TCustomLazHelpControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  BorderStyle:=bsSingle;
  FScrollBar:=TScrollBar.Create(Self);
  FScrollBar.Kind:=sbVertical;
  FScrollBar.Align:=alRight;
  FScrollBar.Visible:=False;
  FScrollBar.TabStop:=False;
  FScrollBar.SmallChange:=Font.GetTextHeight('W');
  FScrollBar.LargeChange:=Font.GetTextHeight('W')*16;
  FScrollBar.OnScroll:=@ScrollBarScroll;
  InsertControl(FScrollBar);
  FFollowLinks:=True;
  FPage:='main';
  FTextColor:=clBlack;
  FBackgroundColor:=clWhite;
  FLinkColor:=$556E00;
  FAutoHeight:=False;
  FHeadingColor:=$1900A1;
  DoubleBuffered:=True;
  Width:=300;
  Height:=200;
end;

destructor TCustomLazHelpControl.Destroy;
begin
  inherited Destroy;
end;

function TCustomLazHelpControl.CanGoBack: Boolean;
begin
  Result:=HistPos > 0;
end;

function TCustomLazHelpControl.CanGoForward: Boolean;
begin
  Result:=HistPos < Length(HistPage) - 1;
end;

procedure TCustomLazHelpControl.GoBack;
begin
  if CanGoBack then begin
    if HistPos=Length(HistPage) then begin
      SaveHistory;
      Dec(HistPos);
    end;
    HistScroll[HistPos]:=FScrollBar.Position;
    Dec(HistPos);
    FPage:=HistPage[HistPos];
    try
      FScrollBar.Max:=HistMax[HistPos];
      FScrollBar.Position:=HistScroll[HistPos];
    except
    end;
    Invalidate;
    if Assigned(FOnPageChanged) then FOnPageChanged(Self);
  end;
end;

procedure TCustomLazHelpControl.GoForward;
begin
  if CanGoForward then begin
    HistScroll[HistPos]:=FScrollBar.Position;
    Inc(HistPos);
    FPage:=HistPage[HistPos];
    try
      FScrollBar.Max:=HistMax[HistPos];
      FScrollBar.Position:=HistScroll[HistPos];
    except
    end;
    Invalidate;
    if Assigned(FOnPageChanged) then FOnPageChanged(Self);
  end;
end;

{ TLazHelpLinkInfo }

procedure TLazHelpLinkInfo.Reset;
begin
  AreaCount:=0;
end;

procedure TLazHelpLinkInfo.AddArea(R: TRect);
begin
  if Length(Areas) <= AreaCount then SetLength(Areas, Length(Areas) + 1);
  Areas[AreaCount]:=R;
  Inc(AreaCount);
end;

function TLazHelpLinkInfo.Check(X, Y: Integer): Boolean;
var
  i: Integer;
begin
  for i:=0 to AreaCount - 1 do with Areas[i] do begin
    if (X >= Left) and (Y >= Top) and (X <= Right) and (Y <= Bottom) then Exit(True);
  end;
  Result:=False;
end;

{ TLazHelpPaintInfo }

procedure TLazHelpPaintInfo.Prepare(ACanvas: TCanvas; ARect: TRect;
  AScroll: Integer);
begin
  Color:=clBlack;
  BackgroundColor:=$FFFFFF;
  LinkColor:=$556E00;
  HeadColor:=$1900A1;
  Canvas:=ACanvas;
  Rect:=ARect;
  Pad:=Types.Rect(8, 8, 8, 8);
  Scroll:=AScroll;
  PageHeight:=0;
  ListDepth:=0;
  X:=ARect.Left + Pad.Left;
  Y:=ARect.Top + Pad.Left
end;

{ TLazHelpElement }

function TLazHelpElement.GetChild(AIndex: Integer): TLazHelpElement;
begin
  Result:=TLazHelpElement(FChildren[AIndex]);
end;

function TLazHelpElement.GetCount: Integer;
begin
  Result:=FChildren.Count;
end;

function TLazHelpElement.GetText: string;
var
  i: Integer;
begin
  if Name='' then Exit(Value);
  Result:='';
  for i:=0 to Count - 1 do Result:=Result + Children[i].Text;
end;

constructor TLazHelpElement.Create(AParent: TLazHelpElement; AName,
  AValue: string);
begin
  FChildren:=TList.Create;
  FParent:=AParent;
  FName:=AName;
  FValue:=AValue;
  if Assigned(AParent) then AParent.FChildren.Add(Self);
end;

destructor TLazHelpElement.Destroy;
var
  i: Integer;
begin
  for i:=0 to Count - 1 do Children[i].Free;
  FreeAndNil(FChildren);
  inherited Destroy;
end;

function TLazHelpElement.FindElementByName(AName: string): TLazHelpElement;
var
  I: Integer;
begin
  for I:=0 to Count - 1 do
    if Children[I].Name=AName then Exit(Children[I]);
  Result:=nil;
end;

{ TLazHelp }

procedure TLazHelp.ParseCodeFor(Node: TLazHelpElement);
var
  TextChild: TLazHelpElement;

  procedure ProcessElement(E: TLazHelpElement);
  begin
    if E.Name='a' then begin
      SetLength(Links, Length(Links) + 1);
      with Links[Length(Links) - 1] do begin
        Element:=E;
        E.FLinkIndex:=Length(Links) - 1;
        SetLength(Areas, 0);
      end;
    end else if E.Name='page' then begin
      SetLength(FPages, Length(FPages) + 1);
      FPages[Length(FPages) - 1]:=E;
    end else if E.Name='default' then begin
      FDefaultPage:=E.Value;
    end;
  end;

  procedure SkipSpaces;
  begin
    while (Head <= Length(FCode)) and (FCode[Head] in [' ', #9, #10, #13]) do Inc(Head);
  end;

  function NextWord: string;
  begin
    SkipSpaces;
    Result:='';
    while (Head <= Length(FCode)) and (FCode[Head] in ['a'..'z', 'A'..'Z', '0'..'9', '_']) do begin
      Result:=Result + FCode[Head];
      Inc(Head);
    end;
  end;

  procedure ParseChildElement;
  var
    Child: TLazHelpElement;
    Name, Value: string;
  begin
    Name:=NextWord;
    if (Head <= Length(FCode)) and (FCode[Head]='[') then begin
      Inc(Head);
      Value:='';
      while (Head <= Length(FCode)) and (FCode[Head] <> ']') do begin
        if FCode[Head]='/' then begin
          Inc(Head);
          if Head <= Length(FCode) then begin
            Value:=Value + FCode[Head];
            Inc(Head);
          end;
          Continue;
        end;
        Value:=Value + FCode[Head];
        Inc(Head);
      end;
      if (Head <= Length(FCode)) and (FCode[Head]=']') then Inc(Head);
    end else
      Value:='';
    if (Head <= Length(FCode)) and (FCode[Head]='{') then begin
      Inc(Head);
      Child:=TLazHelpElement.Create(Node, Name, Value);
      ParseCodeFor(Child);
    end else
      Child:=TLazHelpElement.Create(Node, Name, Value);
    ProcessElement(Child);
  end;

begin
  TextChild:=nil;
  while Head <= Length(FCode) do begin
    if FCode[Head]='}' then begin
      Inc(Head);
      Break;
    end else if FCode[Head]='\' then begin
      Inc(Head);
      TextChild:=nil;
      ParseChildElement;
    end else if FCode[Head]='/' then begin
      Inc(Head);
      if Head <= Length(FCode) then begin
        if not Assigned(TextChild) then TextChild:=TLazHelpElement.Create(Node, '', '');
        TextChild.FValue:=TextChild.FValue + FCode[Head];
        Inc(Head);
      end;
    end else begin
      if not Assigned(TextChild) then TextChild:=TLazHelpElement.Create(Node, '', '');
      TextChild.FValue:=TextChild.FValue + FCode[Head];
      Inc(Head);
    end;
  end;
end;

function TLazHelp.GetPage: string;
begin
  if FPage=nil then Result:='' else Result:=FPage.Value;
end;

function TLazHelp.GetPageCount: Integer;
begin
  Result:=Length(FPages);
end;

function TLazHelp.GetPageElement(AIndex: Integer): TLazHelpElement;
begin
  if (AIndex < 0) or (AIndex >= Length(FPages)) then raise ELazHelpPageIndexOutOfRange.Create('Page index ' + IntToStr(AIndex) + ' out of range');
  Result:=FPages[AIndex];
end;

function TLazHelp.GetPages(AIndex: Integer): string;
begin
  if (AIndex < 0) or (AIndex >= Length(FPages)) then raise ELazHelpPageIndexOutOfRange.Create('Page index ' + IntToStr(AIndex) + ' out of range');
  Result:=FPages[AIndex].Value;
end;

procedure TLazHelp.SetFileName(const AValue: string);
begin
  if FFileName=AValue then exit;
  FFileName:=AValue;
  if not (csDesigning in ComponentState) then LoadFromFile(FFileName);
end;

procedure TLazHelp.SetCode(const AValue: string);
begin
  if FCode=AValue then exit;
  FreeAndNil(Root);
  FCode:=AValue;
  Head:=1;
  Root:=TLazHelpElement.Create(nil, '', '');
  SetLength(FPages, 0);
  FPage:=nil;
  FFileName:='';
  SetLength(Links, 0);
  SetLength(Graphics, 0);
  ParseCodeFor(Root);
  SetPage(FDefaultPage);
end;

procedure TLazHelp.PaintElement(var LazHelpPaintInfo: TLazHelpPaintInfo;
  E: TLazHelpElement);

  procedure PaintText(Text: string);
  var
    W: string;
    i: Integer;
    AddSpace, AddSpaceOnChar: Boolean;
    SpaceSize: Integer;

    procedure PaintWord;
    var
      Size: TSize;
    begin
      with LazHelpPaintInfo do begin
        Size:=Canvas.TextExtent(W);
        if (X > Rect.Left + Pad.Left) and (X + Size.cx > Rect.Right - Pad.Right) then begin
          X:=Rect.Left + Pad.Left;
          Y:=Y + LineHeight;
          PageHeight:=PageHeight + LineHeight;
          LineHeight:=Size.cy;
        end;
        if (Y > -Size.cy) and (Y < Canvas.Height) then Canvas.TextOut(X, Y, W);
        //if (Y > -Size.cy) and (Y < Canvas.Height) then
        //  Canvas.TextRect(types.Rect(X, Y, Rect.Right, Rect.Bottom), X, Y, W, Canvas.TextStyle);
        if Link <> -1 then Links[Link].AddArea(Types.Rect(X, Y, X + Size.cx, Y + Size.cy));
        X:=X + Size.cx;
        if LineHeight < Size.cy then LineHeight:=Size.cy;
        W:='';
      end;
    end;

  begin
    with LazHelpPaintInfo do begin
      W:='';
      SpaceSize:=Canvas.TextWidth(' ');
      AddSpace:=False;
      AddSpaceOnChar:=False;
      for i:=1 to Length(Text) do begin
        if Text[i] in [' ', #9, #10, #13] then begin
          if W <> '' then PaintWord
          else if i=1 then AddSpaceOnChar:=True;
          if AddSpace and (X > Rect.Left + Pad.Left) then begin
            if Link <> -1 then Links[Link].AddArea(Types.Rect(X, Y, X + SpaceSize, Y + Canvas.TextHeight(' ')));
            X:=X + SpaceSize;
          end;
          AddSpace:=False;
        end else begin
          W:=W + Text[i];
          if AddSpaceOnChar and (X > Rect.Left + Pad.Left) then begin
            if Link <> -1 then Links[Link].AddArea(Types.Rect(X, Y, X + SpaceSize, Y + Canvas.TextHeight(' ')));
            X:=X + SpaceSize;
          end;
          AddSpaceOnChar:=False;
          AddSpace:=True;
        end;
      end;
      if W <> '' then PaintWord;
    end;
  end;

  procedure BlockBreak;
  begin
    with LazHelpPaintInfo do begin
      X:=Rect.Left + Pad.Left;
      Y:=Y + LineHeight;
      PageHeight:=PageHeight + LineHeight;
      LineHeight:=Canvas.TextHeight('W');
    end;
  end;

  procedure PaintGeneric;
  var
    i: Integer;
  begin
    if E.Name='' then PaintText(E.Value);
    for i:=0 to E.Count - 1 do PaintElement(LazHelpPaintInfo, E[i]);
  end;

  procedure PaintBold;
  var
    OldBold: Boolean;
  begin
    OldBold:=LazHelpPaintInfo.Canvas.Font.Bold;
    LazHelpPaintInfo.Canvas.Font.Bold:=True;
    PaintGeneric;
    LazHelpPaintInfo.Canvas.Font.Bold:=OldBold;
  end;

  procedure PaintItalic;
  var
    OldItalic: Boolean;
  begin
    OldItalic:=LazHelpPaintInfo.Canvas.Font.Italic;
    LazHelpPaintInfo.Canvas.Font.Italic:=True;
    PaintGeneric;
    LazHelpPaintInfo.Canvas.Font.Italic:=OldItalic;
  end;

  procedure PaintUnderline;
  var
    OldUnderline: Boolean;
  begin
    OldUnderline:=LazHelpPaintInfo.Canvas.Font.Underline;
    LazHelpPaintInfo.Canvas.Font.Underline:=True;
    PaintGeneric;
    LazHelpPaintInfo.Canvas.Font.Underline:=OldUnderline;
  end;

  procedure PaintTeleType;
  var
    OldFont: TFont;
  begin
    OldFont:=TFont.Create;
    try
      OldFont.Assign(LazHelpPaintInfo.Canvas.Font);
      {$IFDEF WINDOWS}
      LazHelpPaintInfo.Canvas.Font.Name:='Courier New';
      {$ELSE}
      LazHelpPaintInfo.Canvas.Font.Name:='Courier';
      {$ENDIF}
      PaintGeneric;
      LazHelpPaintInfo.Canvas.Font.Assign(OldFont);
    finally
      OldFont.Free;
    end;
  end;

  procedure PaintAnchor;
  var
    OldColor: TColor;
    PrevLink: Integer;
  begin
    OldColor:=LazHelpPaintInfo.Canvas.Font.Color;
    LazHelpPaintInfo.Canvas.Font.Color:=LazHelpPaintInfo.LinkColor;
    PrevLink:=LazHelpPaintInfo.Link;
    LazHelpPaintInfo.Link:=E.FLinkIndex;
    Links[LazHelpPaintInfo.Link].Reset;
    PaintGeneric;
    LazHelpPaintInfo.Link:=PrevLink;
    LazHelpPaintInfo.Canvas.Font.Color:=OldColor;
  end;

  procedure PaintHeading;
  var
    Size: Integer;
    OldColor: TColor;
  begin
    try
      Size:=StrToInt(E.Value);
      if Size > 7 then Size:=7 else if Size < 1 then Size:=1;
    except
      Size:=1;
    end;
    Size:=8-Size;
    OldColor:=LazHelpPaintInfo.Canvas.Font.Color;
    LazHelpPaintInfo.Canvas.Font.Color:=LazHelpPaintInfo.HeadColor;
    LazHelpPaintInfo.Canvas.Font.Size:=LazHelpPaintInfo.Canvas.Font.Size + 2*Size;
    if LazHelpPaintInfo.X > LazHelpPaintInfo.Rect.Left + LazHelpPaintInfo.Pad.Left then BlockBreak;
    PaintGeneric;
    LazHelpPaintInfo.Canvas.Font.Size:=LazHelpPaintInfo.Canvas.Font.Size - 2*Size;
    LazHelpPaintInfo.Canvas.Font.Color:=OldColor;
    BlockBreak;
  end;

  procedure PaintParagraph;
  begin
    if LazHelpPaintInfo.Y + LazHelpPaintInfo.Scroll > LazHelpPaintInfo.Rect.Top + LazHelpPaintInfo.Pad.Top then BlockBreak;
    PaintGeneric;
    BlockBreak;
  end;

  procedure PaintUnorderedList;
  var
    i: Integer;
  begin
    LazHelpPaintInfo.Rect.Left:=LazHelpPaintInfo.Rect.Left + 16;
    Inc(LazHelpPaintInfo.ListDepth);
    for i:=0 to E.Count - 1 do begin
      if E[i].Name <> 'li' then Continue;
      BlockBreak;
      LazHelpPaintInfo.Canvas.Pen.Style:=psClear;
      LazHelpPaintInfo.Canvas.Brush.Style:=bsSolid;
      LazHelpPaintInfo.Canvas.Brush.Color:=LazHelpPaintInfo.Color;
      LazHelpPaintInfo.Canvas.EllipseC(LazHelpPaintInfo.X, LazHelpPaintInfo.Y + 1 + LazHelpPaintInfo.Canvas.TextHeight('W') div 2, 4, 4);
      LazHelpPaintInfo.X:=LazHelpPaintInfo.X + 12;
      LazHelpPaintInfo.Canvas.Brush.Color:=LazHelpPaintInfo.BackgroundColor;
      PaintElement(LazHelpPaintInfo, E[i]);
    end;
    Dec(LazHelpPaintInfo.ListDepth);
    if LazHelpPaintInfo.ListDepth=0 then BlockBreak;
    LazHelpPaintInfo.Rect.Left:=LazHelpPaintInfo.Rect.Left - 16;
  end;

  procedure PaintImportedPage;
  var
    i: Integer;
  begin
    if LazHelpPaintInfo.ImportCount < 16 then begin
      Inc(LazHelpPaintInfo.ImportCount);
      for i:=0 to Length(FPages)-1 do if FPages[i].Value=E.Value then begin
        PaintElement(LazHelpPaintInfo, FPages[i]);
        Break;
      end;
      Dec(LazHelpPaintInfo.ImportCount);
    end;
  end;

  procedure PaintInlineImage;
  var
    Graphic: TGraphic;
  begin
    Graphic:=TGraphic(E.FLinkIndex);
    if Graphic=nil then begin
      Graphic:=GetGraphic(E.Value);
      E.FLinkIndex:=PtrInt(Graphic);
    end;
    if Graphic=nil then Exit;
    with LazHelpPaintInfo do begin
      if (X > Rect.Left + Pad.Left) and (X + Graphic.Width > Rect.Right - Pad.Right) then begin
        X:=Rect.Left + Pad.Left;
        Y:=Y + LineHeight;
        PageHeight:=PageHeight + LineHeight;
        LineHeight:=Graphic.Height;
      end;
      Canvas.Draw(X, Y, Graphic);
      if Link <> -1 then Links[Link].AddArea(Types.Rect(X, Y, X + Graphic.Width, Y + Graphic.Height));
      X:=X + Graphic.Width;
      if LineHeight < Graphic.Height then LineHeight:=Graphic.Height;
    end;
  end;

  procedure PaintHorizontalRuler;
  begin
    with LazHelpPaintInfo do begin
      if X > Rect.Left + Pad.Left then BlockBreak;
      LineHeight:=5;
      if E.Value <> '' then LineHeight:=StrToIntDef(E.Value, 5);
      if LineHeight < 1 then LineHeight:=1;
      Canvas.Pen.Color:=clBlack;
      Canvas.Line(Rect.Left + Pad.Left - 2, Y + LineHeight div 2, Rect.Right - Pad.Right + 2, Y + LineHeight div 2);
      X:=Rect.Left + Pad.Left;
      Y:=Y + LineHeight;
      PageHeight:=PageHeight + LineHeight;
      LineHeight:=1;
    end;
  end;

begin
  if E.Name='h' then PaintHeading
  else if E.Name='a' then PaintAnchor
  else if E.Name='b' then PaintBold
  else if E.Name='i' then PaintItalic
  else if E.Name='u' then PaintUnderline
  else if E.Name='tt' then PaintTeleType
  else if E.Name='p' then PaintParagraph
  else if E.Name='ul' then PaintUnorderedList
  else if E.Name='imp' then PaintImportedPage
  else if E.Name='img' then PaintInlineImage
  else if E.Name='hr' then PaintHorizontalRuler
  else if E.Name='br' then BlockBreak
  else if E.Name='sp' then begin
    if E.Value='' then
      LazHelpPaintInfo.X += LazHelpPaintInfo.Canvas.TextWidth(' ')
    else
      LazHelpPaintInfo.X += StrToIntDef(E.Value, 0);
  end
  else if E.Name='title' then Exit
  else begin
    PaintGeneric;
  end;
end;

function TLazHelp.GetLinkIndexAt(X, Y: Integer): Integer;
var
  i: Integer;
begin
  Result:=-1;
  for i:=0 to Length(Links) - 1 do
    if Links[i].Check(X, Y) then Exit(i);
end;

function TLazHelp.GetGraphicIndex(AName: string): Integer;
var
  i: Integer;
begin
  for i:=0 to Length(Graphics) - 1 do
    if Graphics[i].Name=AName then Exit(i);
  Result:=-1;
end;

function TLazHelp.GetGraphic(AName: string): TGraphic;
var
  i: Integer;
begin
  Result:=nil;
  for i:=0 to Length(Graphics) - 1 do
    if Graphics[i].Name=AName then begin
      Result:=Graphics[i].Graphic;
      Break;
    end;
  if Assigned(FOnGetGraphic) then FOnGetGraphic(Self, AName, Result);
end;

procedure TLazHelp.SetPage(const AValue: string);
var
  i: Integer;

  procedure ProcessElement(E: TLazHelpElement);
  var
    i: Integer;
  begin
    if E.Name='title' then FTitle:=E.Value;
    for i:=0 to E.Count - 1 do ProcessElement(E[i]);
  end;

begin
  if (FPage <> nil) and (FPage.Value=AValue) then Exit;
  for i:=0 to Length(Links) - 1 do Links[i].Reset;
  for i:=0 to Length(FPages)-1 do begin
    if AValue=FPages[i].Value then begin
      FPage:=FPages[i];
      ProcessElement(FPage);
      Exit;
    end;
  end;
end;

constructor TLazHelp.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDefaultPage:='main';
  FFileName:='';
end;

destructor TLazHelp.Destroy;
begin
  FreeAndNil(Root);
  inherited Destroy;
end;

procedure TLazHelp.UseNiceDefaultFont(var LazHelpPaintInfo: TLazHelpPaintInfo);
begin
  with LazHelpPaintInfo do begin
    try // choose a nice font
      {$IFDEF WINDOWS}
      if Canvas.Font.Name='default' then Canvas.Font.Name:='Tahoma';
      if Canvas.Font.Size=0 then Canvas.Font.Size:=10;
      {$ENDIF}
      {$IFDEF DARWIN}
      if Canvas.Font.Name='default' then Canvas.Font.Name:='Gill Sans';
      if Canvas.Font.Size=0 then Canvas.Font.Size:=15;
      {$ENDIF}
      {$IFDEF LCLGTK2}
      if Canvas.Font.Size=0 then Canvas.Font.SetDefault;
      {$ENDIF}
    except
    end;
  end;
end;

procedure TLazHelp.PaintAt(var LazHelpPaintInfo: TLazHelpPaintInfo);
var
  OldClipRect: TRect;
  OldClipping: Boolean;
  OldTextStyle, NewTextStyle: TTextStyle;
begin
  with LazHelpPaintInfo do with Canvas do begin
    OldClipping:=Clipping;
    Clipping:=True;
    OldClipRect:=ClipRect;
    ClipRect:=Rect;
    Pen.Style:=psSolid;
    Pen.Color:=BackgroundColor;
    Brush.Style:=bsSolid;
    Brush.Color:=BackgroundColor;
    Font.Color:=Color;
    Font.Style:=[];
    AntialiasingMode:=amOff;
    Rectangle(Rect.Left - 1, Rect.Top - 1, Rect.Right + 1, Rect.Bottom + 1);
    AntialiasingMode:=amDontCare;
    OldTextStyle:=TextStyle;
    NewTextStyle:=TextStyle;
    NewTextStyle.Opaque:=False;
    TextStyle:=NewTextStyle;
    X:=Rect.Left + Pad.Left;
    Y:=Rect.Top + Pad.Top - Scroll;
    PageHeight:=Pad.Top + Pad.Bottom;
    ListDepth:=0;
    ImportCount:=0;
    Link:=-1;
    LineHeight:=0;
    Brush.Style:=bsClear;
    if Assigned(FPage) then PaintElement(LazHelpPaintInfo, FPage);
    PageHeight:=PageHeight + TextHeight('W');
    TextStyle:=OldTextStyle;
    ClipRect:=OldClipRect;
    Clipping:=OldClipping;
  end;
end;

function TLazHelp.GetCursorAt(X, Y: Integer): TCursor;
begin
  Result:=crDefault;
  if GetLinkIndexAt(X, Y) <> -1 then Result:=crHandPoint;
end;

function TLazHelp.ProcessClickAt(X, Y: Integer): Boolean;
var
  Index: Integer;
  OldPage: TLazHelpElement;
  Processed: Boolean;
  Target: string;
begin
  Result:=False;
  Index:=GetLinkIndexAt(X, Y);
  if Index <> -1 then begin
    Processed:=False;
    Target:=Links[Index].Element.Value;
    if Assigned(FOnLinkClick) then
      FOnLinkClick(Self, Target, Processed);
    if Processed then Exit;
    OldPage:=FPage;
    SetPage(Target);
    Result:=OldPage <> FPage;
  end;
end;

function TLazHelp.GetLinkElementAt(X, Y: Integer): TLazHelpElement;
var
  Index: Integer;
begin
  Index:=GetLinkIndexAt(X, Y);
  if Index <> -1 then
    Result:=Links[Index].Element
  else
    Result:=nil;
end;

function TLazHelp.FindPageByName(AName: string): TLazHelpElement;
var
  I: Integer;
begin
  for I:=0 to High(FPages) do
    if FPages[I].Value=AName then Exit(FPages[I]);
  Result:=nil;
end;

procedure TLazHelp.LoadFromStream(AStream: TStream);
var
  StringList: TStringList;
  TheCode: string;
begin
  StringList:=TStringList.Create;
  try
    StringList.LoadFromStream(AStream);
    TheCode:=StringList.Text;
  except
    on E : Exception do begin
      StringList.Free;
      raise E;
    end;
  end;
  StringList.Free;
  Code:=TheCode;
end;

procedure TLazHelp.LoadFromFile(AFileName: string);
var
  StringList: TStringList;
  TheCode: string;
begin
  StringList:=TStringList.Create;
  try
    StringList.LoadFromFile(AFileName);
    TheCode:=StringList.Text;
  except
    StringList.Free;
    Exit;
  end;
  StringList.Free;
  Code:=TheCode;
  FFileName:=AFileName;
end;

procedure TLazHelp.ReloadFile;
begin
  if FFileName <> '' then LoadFromFile(FFileName);
end;

procedure TLazHelp.RegisterGraphic(AName: string; AGraphic: TGraphic);
begin
  SetLength(Graphics, Length(Graphics) + 1);
  with Graphics[Length(Graphics) - 1] do begin
    Name:=AName;
    Graphic:=AGraphic;
  end;
end;

procedure TLazHelp.UnregisterGraphic(AName: string);
var
  i, Index: Integer;
begin
  Index:=GetGraphicIndex(AName);
  if Index=-1 then Exit;
  for i:=Index to Length(Graphics) - 2 do
    Graphics[i]:=Graphics[i + 1];
  SetLength(Graphics, Length(Graphics) - 1);
end;

procedure TLazHelp.RegisterImageList(APrefix: string; AImageList: TImageList);
var
  i: Integer;
  Bmp: TImage;
  CBmp: TBitmap;
begin
  // NOTE: using TImage here so that LCL will automatically free the bitmaps
  // when TLazHelp is destroyed
  for i:=0 to AImageList.Count - 1 do begin
    Bmp:=TImage.Create(Self);
    Bmp.Width:=AImageList.Width;
    Bmp.Height:=AImageList.Height;
    Bmp.Transparent:=True;
    CBmp:=TBitmap.Create;
    AImageList.GetBitmap(i, CBmp);
    Bmp.Picture.Assign(CBmp);
    CBmp.Free;
    RegisterGraphic(APrefix + IntToStr(i), Bmp.Picture.Graphic);
  end;
end;

initialization
  {$I lazhelppackage.lrs}
finalization
  FreeAndNil(Icons);
end.

