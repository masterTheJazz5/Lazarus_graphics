unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Types, Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Menus, SynCompletion, LazHelp;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ImageList1: TImageList;
    LazHelp1: TLazHelp;
    LazHelpPopupViewer1: TLazHelpPopupViewer;
    LazHelpViewer1: TLazHelpViewer;
    LazHelpWindowedViewer1: TLazHelpWindowedViewer;
    ListBox1: TListBox;
    StringListLazHelpProvider1: TStringListLazHelpProvider;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LazHelpWindowedViewer1PageChanged(Sender: TObject);
    procedure LazHelpWindowedViewer1PageTitle(Sender: TObject; ATitle: string);
  private
    { private declarations }
  public
    { public declarations }
    LazHelpControl: TCustomLazHelpControl;
    procedure ShowHelp;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  ShowHelp;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  P: TPoint;
begin
  P:=Button2.ClientToScreen(Point(0, Button2.Height + 10));
  LazHelpPopupViewer1.ShowPageAt('main', P.x, P.y);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  for i:=0 to LazHelp1.PageCount - 1 do ListBox1.Items.Add(LazHelp1.Pages[i]);
  LazHelp1.RegisterImageList('il', ImageList1);
end;

procedure TForm1.LazHelpWindowedViewer1PageChanged(Sender: TObject);
begin
  ShowMessage('Page changed to ' + LazHelpWindowedViewer1.Page);
end;

procedure TForm1.LazHelpWindowedViewer1PageTitle(Sender: TObject; ATitle: string
  );
begin
  Caption:=ATitle;
end;

procedure TForm1.ShowHelp;
begin
  LazHelpWindowedViewer1.ShowHelp;
end;

end.

