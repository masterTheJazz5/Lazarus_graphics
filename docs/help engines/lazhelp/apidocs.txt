I'll write the detailed docs at some point later.  In the meanwhile, check
the editor and "lazhelptest" projects for usage examples.  The most
important classes are

TLazHelp - the main component, contains the help site, asks for graphics,
  does the rendering, etc.  In theory this is all you need, you just have
  to do a lot of the dirty work yourself.

TLazHelpViewer - In practice you don't want to do the dirty work yourself,
  so this control provides a help page viewer panel.  Connect it to a 
  TLazHelp to feed it pages and you have a working help viewer.  You should
  put one or two buttons that call GoBack and GoForward for navigating the
  history.  They can be linked with a TLazHelpWindowedViewer so that
  links are opened in a windowed viewer instead of this one.  They can also
  be set to automatically resize (vertically) to show the whole page.

TLazHelpWindowedViewer - Provides a help viewer in a form.  Similar to the
  viewer above, but it has a premade UI with buttons, etc (the help site
  can also use the \button element to add more buttons).  Can also be
  registered as a HelpManager to open help pages from keywords (as page
  names) and contexts (as pages named hc<context>).

TLazHelpPopupViewer - Provides a "popup" help viewer.  This is for small
  help panels, like for a "what does this control do" functionality.  They
  can be linked with TLazHelpWindowedViewers so that links open in a
  windowed viewer.

TStringListLazHelpProvider - Link it to a TLazHelp to provide code for the
  site from a TStrings object.  Helpful when you want to embed the help
  site with your application's executable.  Hopefully a better method will
  be provided later to do that.

TLazHelpElement - node class for the LazHelp site tree.  Obtain the root
  element using the RootElement property of TLazHelp.


To use images hook an event handler to TLazHelp's OnGetGraphic and
provide/load the requested images.  Note that LazHelp will only ask you
for the graphic the first time is requested from the site.

Kostas "Bad Sector" Michalopoulos
badsectoracula@gmail.com

Note: currently the windowed and popup viewers do not seem to work
      properly under Mac OS X.  The windowed viewer is misplaced and
      the popup viewer causes a crash.


