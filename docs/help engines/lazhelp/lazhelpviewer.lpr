program lazhelpviewer;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, LazHelpViewerMainUnit, lazhelppackage
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='LazHelp Viewer';
  Application.Initialize;
  Application.CreateForm(TMain, Main);
  Application.Run;
end.

