unit OBJMeshLoader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Maths, Meshes, Textures;

type

  TOBJMeshLoaderFace = record
    Geo, Tex, Nor: array [0..2] of Integer;
  end;

  { TOBJMeshLoader }

  TOBJMeshLoader = class(TMeshLoader)
  private
    FAutoLoadTexture: Boolean;
    GeoVtx, TexVtx, NorVtx: array of TVector;
    Faces: array of TOBJMeshLoaderFace;
    procedure CreateAndFillMesh(AFileName: string);
  public
    constructor Create; override;
    procedure LoadFromStream(AStream: TStream; AFileName: string); override;
    property AutoLoadTexture: Boolean read FAutoLoadTexture write FAutoLoadTexture;
  end;

implementation

uses
  Logger;

function ParseFloat(s: string): Double;
var
  FormatSettings: TFormatSettings;
begin
  FormatSettings:=DefaultFormatSettings;
  FormatSettings.DecimalSeparator:='.';
  FormatSettings.ThousandSeparator:=',';
  try
    Result:=StrToFloat(s, FormatSettings);
  except
    Result:=0;
  end;
end;

{ TOBJMeshLoader }

procedure TOBJMeshLoader.CreateAndFillMesh(AFileName: string);
var
  Vertices: array of TMeshVertex;
  Indices: array of Integer;
  I, VCnt: Integer;
  Texture: TTexture;
  ImgFileName: String;
begin
  SetLength(Indices, Length(Faces)*3);
  SetLength(Vertices, Length(Faces)*3);
  VCnt:=0;
  for I:=0 to High(Indices) do Indices[I]:=I;
  for I:=0 to High(Faces) do
    with Faces[I] do begin
      Vertices[VCnt].x:=GeoVtx[Geo[0] - 1].x;
      Vertices[VCnt].y:=GeoVtx[Geo[0] - 1].y;
      Vertices[VCnt].z:=GeoVtx[Geo[0] - 1].z;
      Vertices[VCnt].s:=TexVtx[Tex[0] - 1].x;
      Vertices[VCnt].t:=1-TexVtx[Tex[0] - 1].y;
      Vertices[VCnt].n.x:=NorVtx[Nor[0] - 1].x;
      Vertices[VCnt].n.y:=NorVtx[Nor[0] - 1].y;
      Vertices[VCnt].n.z:=NorVtx[Nor[0] - 1].z;
      Inc(VCnt);
      Vertices[VCnt].x:=GeoVtx[Geo[1] - 1].x;
      Vertices[VCnt].y:=GeoVtx[Geo[1] - 1].y;
      Vertices[VCnt].z:=GeoVtx[Geo[1] - 1].z;
      Vertices[VCnt].s:=TexVtx[Tex[1] - 1].x;
      Vertices[VCnt].t:=1-TexVtx[Tex[1] - 1].y;
      Vertices[VCnt].n.x:=NorVtx[Nor[1] - 1].x;
      Vertices[VCnt].n.y:=NorVtx[Nor[1] - 1].y;
      Vertices[VCnt].n.z:=NorVtx[Nor[1] - 1].z;
      Inc(VCnt);
      Vertices[VCnt].x:=GeoVtx[Geo[2] - 1].x;
      Vertices[VCnt].y:=GeoVtx[Geo[2] - 1].y;
      Vertices[VCnt].z:=GeoVtx[Geo[2] - 1].z;
      Vertices[VCnt].s:=TexVtx[Tex[2] - 1].x;
      Vertices[VCnt].t:=1-TexVtx[Tex[2] - 1].y;
      Vertices[VCnt].n.x:=NorVtx[Nor[2] - 1].x;
      Vertices[VCnt].n.y:=NorVtx[Nor[2] - 1].y;
      Vertices[VCnt].n.z:=NorVtx[Nor[2] - 1].z;
      Inc(VCnt);
    end;
  if AutoLoadTexture then begin
    ImgFileName:=ExtractFileNameWithoutExt(AFileName);
    if FileExists(ImgFileName + '.png') then
      ImgFileName:=ImgFileName + '.png'
    else if FileExists(ImgFileName + '.jpg') then
      ImgFileName:=ImgFileName + '.jpg'
    else if FileExists(ImgFileName + '.bmp') then
      ImgFileName:=ImgFileName + '.bmp'
    else if FileExists(ImgFileName + '.gif') then
      ImgFileName:=ImgFileName + '.gif'
    else begin
      Log('Warning: Cannot find PNG, JPEG, BMP or GIF texture for ' + AFileName);
      ImgFileName:='';;
      Texture:=nil;
    end;
    if ImgFileName <> '' then begin
      Texture:=TTexture.Create(ImgFileName);
      Texture.Reload;
    end;
  end else Texture:=nil;
  SetData(Vertices, Indices, Texture, AFileName);
end;

constructor TOBJMeshLoader.Create;
begin
  inherited Create;
  FAutoLoadTexture:=True;
end;

procedure TOBJMeshLoader.LoadFromStream(AStream: TStream; AFileName: string);
type
  TStringArray = array of string;
var
  Line: string;
  Cmd: TStringArray;

  procedure NextLine;
  var
    Ch: Char;
  begin
    Line:='';
    while AStream.Position < AStream.Size do begin
      Ch:=Char(AStream.ReadByte);
      if Ch in [#10, #13] then Break;
      Line:=Line + Ch;
    end;
  end;

  function Split(s: string; c: Char; Combine: Boolean): TStringArray;
  var
    p: string;
    I: Integer;
  begin
    SetLength(Result, 0);
    p:='';
    for I:=1 to Length(s) do
      if s[I]=c then begin
        if Combine and (p='') then Continue;
        SetLength(Result, Length(Result) + 1);
        Result[High(Result)]:=p;
        p:='';
      end else
        p:=p + s[I];
    if p <> '' then begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)]:=p;
      p:='';
    end;
  end;

  procedure ParseFace(var Face: TObjMeshLoaderFace; FaceDef: string; Idx: Integer);
  var
    Parts: TStringArray;
  begin
    Parts:=Split(FaceDef, '/', False);
    if Length(Parts) < 1 then Exit;
    try
      Face.Geo[Idx]:=StrToInt(Parts[0]);
      if (Length(Parts) > 1) and (Parts[1] <> '') then
        Face.Tex[Idx]:=StrToInt(Parts[1])
      else
        Face.Tex[Idx]:=1;
      if (Length(Parts) > 2) and (Parts[2] <> '') then
        Face.Nor[Idx]:=StrToInt(Parts[2])
      else
        Face.Nor[Idx]:=1;
    except
    end;
  end;

begin
  Log('Parsing OBJ file from ' + AFileName);
  while AStream.Position < AStream.Size do begin
    NextLine;
    if (Line='') or (Line[1]='#') then Continue;
    Cmd:=Split(Line, ' ', True);
    if Length(Cmd) < 1 then Continue;
    if Cmd[0]='v' then begin
      SetLength(GeoVtx, System.Length(GeoVtx) + 1);
      with GeoVtx[High(GeoVtx)] do begin
        if System.Length(Cmd) > 1 then x:=ParseFloat(Cmd[1]);
        if System.Length(Cmd) > 2 then y:=ParseFloat(Cmd[2]);
        if System.Length(Cmd) > 3 then z:=ParseFloat(Cmd[3]);
      end;
    end else if Cmd[0]='vt' then begin
      SetLength(TexVtx, System.Length(TexVtx) + 1);
      with TexVtx[High(TexVtx)] do begin
        if System.Length(Cmd) > 1 then x:=ParseFloat(Cmd[1]);
        if System.Length(Cmd) > 2 then y:=ParseFloat(Cmd[2]);
        if System.Length(Cmd) > 3 then z:=ParseFloat(Cmd[3]);
      end;
    end else if Cmd[0]='vn' then begin
      SetLength(NorVtx, System.Length(NorVtx) + 1);
      with NorVtx[High(NorVtx)] do begin
        if System.Length(Cmd) > 1 then x:=ParseFloat(Cmd[1]);
        if System.Length(Cmd) > 2 then y:=ParseFloat(Cmd[2]);
        if System.Length(Cmd) > 3 then z:=ParseFloat(Cmd[3]);
      end;
    end else if Cmd[0]='f' then begin
      SetLength(Faces, System.Length(Faces) + 1);
      if System.Length(Cmd) > 1 then ParseFace(Faces[High(Faces)], Cmd[1], 0);
      if System.Length(Cmd) > 2 then ParseFace(Faces[High(Faces)], Cmd[2], 1);
      if System.Length(Cmd) > 3 then ParseFace(Faces[High(Faces)], Cmd[3], 2);
    end;
  end;
  if Length(TexVtx) < 1 then SetLength(TexVtx, 1); { for when texture coords are missing }
  if Length(NorVtx) < 1 then SetLength(NorVtx, 1); { for when normals are missing }
  Log('Creating Mesh from OBJ file ' + AFileName);
  CreateAndFillMesh(AFileName);
  if Assigned(Mesh) then
    Log('Done loading OBJ file ' + AFileName + ' with ' + IntToStr(Mesh.VertexCount) + ' vertices')
  else
    Log('Failed to load OBJ file ' + AFileName);
end;

end.

