unit Maths;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Math, GL, GLu;

const
  PI = 3.1415926535897932384626433832795;
  EPSILON = 0.0001;

type
  TAxis = (axX, axY, axZ);
  TAxes = set of TAxis;
  TPlaneSide = (psFront, psBack, psBoth);
  TCoplaneStyle = (csNotCoplanar, csSame, csInverted);

const
  AllAxes = [axX, axY, axZ];

type

  { TMeshBase }

  TMeshBase = class
  public
  end;

  { TVector }

  PVector = ^TVector;
  TVector = packed object
    x, y, z: Double;

    procedure Zero; inline;
    procedure Copy(const Src: TVector); inline;
    procedure Normalize; inline;
    function Normalized: TVector; inline;
    procedure Invert; inline;
    function Inverted: TVector; inline;
    function Equal(const V: TVector): Boolean; inline;
    function IsZero: Boolean; inline;
    function LengthSq: Double; inline;
    function Length: Double; inline;
    function GetMajorAxis: TAxis; inline;
    function Dot(const v: TVector): Double; inline;
    procedure Add(const v: TVector); inline;
    procedure Sub(const v: TVector); inline;
    function Added(const v: TVector): TVector; inline;
    function Subbed(const v: TVector): TVector; inline;
    procedure Cross(const v: TVector; out r: TVector); inline;
    function Crossed(const v: TVector): TVector; inline;
    procedure Scale(s: Double); inline;
    function Scaled(s: Double): TVector; inline;
    procedure Multiply(const v: TVector); inline;
    function Multiplied(const v: TVector): TVector; inline;
    procedure AddAndScale(const v: TVector; s: Double); inline;
    procedure Snap(GridSize: Integer); inline;
    function Snapped(GridSize: Integer): TVector; inline;
    procedure SnapToInteger; inline;
    procedure Dump; inline;
    function ToString: string; inline;
  end;

  TVectorArray = array of TVector;

  PIntVector = ^TIntVector;

  { TIntVector }

  TIntVector = packed object
    x, y, z: Integer;

    procedure Zero; inline;
    procedure Invert; inline;
    function Equal(const V: TIntVector): Boolean; inline;
    function LengthSq: Integer; inline;
    function Length: Double; inline;
    function GetMajorAxis: TAxis; inline;
    procedure Add(const v: TIntVector); inline;
    procedure Sub(const v: TIntVector); inline;
    function Added(const v: TIntVector): TIntVector; inline;
    function Subbed(const v: TIntVector): TIntVector; inline;
    procedure Multiply(const v: TIntVector); inline;
    procedure Snap(GridSize: Integer); inline;
    function Snapped(GridSize: Integer): TIntVector; inline;
    procedure Dump; inline;
  end;

  { TPlane }

  PPlane = ^TPlane;
  TPlane = object
    n: TVector;
    d: Double;

    procedure FromThreePoints(const a, b, c: TVector); inline;
    procedure FromPointAndNormal(const p, nor: TVector); inline;
    procedure Invert; inline;
    function SignDist(const p: TVector): Double; inline;
    function Front(const p: TVector): Boolean; inline;
    procedure Project(var p: TVector); inline;
    function Projected(const p: TVector): TVector; inline;
    function ProjectedNormal(const p: TVector): TVector; inline;
    procedure Flip(var p: TVector); inline;
    function Flipped(const p: TVector): TVector; inline;
    function Intersect(const a, b: TVector; out p: TVector): Boolean;
    function IntersectNEP(const a, b: TVector; out p: TVector): Boolean; // No-EndPoint version (0 and 1 are considered "out")
    procedure CalcTextureAxes(out tx, ty: TVector); inline;
    function CoplaneStyle(const APlane: TPlane): TCoplaneStyle;
  end;

  { TMatrix }

  TMatrix = packed object
    m11, m21, m31, m41,
    m12, m22, m32, m42,
    m13, m23, m33, m43,
    m14, m24, m34, m44: Double;

    procedure FromArray(const M: T16dArray);
    function ToArray: T16dArray;

    procedure Identity;
    procedure XRotation(Angle: Double);
    procedure YRotation(Angle: Double);
    procedure ZRotation(Angle: Double);
    procedure Rotation(x, y, z, Angle: Double);
    procedure Rotation(const n: TVector; Angle: Double);
    procedure XYZRotation(xa, ya, za: Double);
    procedure Translation(x, y, z: Double);
    procedure Scaling(s: Double);
    procedure Scaling(x, y, z: Double);
    procedure LookAt(const Position, Target, Up: TVector);
    procedure Direction(const Dir: TVector);
    procedure Perspective(Fov, Aspect, ZNear, ZFar: Double);
    procedure Orthographic(Left, Right, Top, Bottom, ZNear, ZFar: Double);

    procedure Transform(var v: TVector);
    procedure Transformed(const v: TVector; out r: TVector);
    procedure TransformNormal(var v: TVector);
    procedure TransformedNormal(const v: TVector; out r: TVector);
    function Transformed(const v: TVector): TVector;
    function Transformed(const p: TPlane): TPlane;
    function TransformedNormal(const v: TVector): TVector;
    procedure TransformProj(var v: TVector);
    procedure TransformedProj(const v: TVector; out r: TVector);
    function TransformedProj(const v: TVector): TVector;
    procedure Transform(var p: TPlane);

    procedure Multiply(const Src: TMatrix);
    function Multiplied(const Src: TMatrix): TMatrix;
    procedure SwapMultiply(const Src: TMatrix);
    function SwapMultiplied(const Src: TMatrix): TMatrix;

    function Determinant: Double;
    procedure Invert;
    function Inverted: TMatrix;

    procedure NormalizeAxes;

    function XAxis: TVector; inline;
    function YAxis: TVector; inline;
    function ZAxis: TVector; inline;
    function Translation: TVector; inline;
    function Position: TVector; inline;

    procedure SetXAxis(const v: TVector; w: Double=0); inline;
    procedure SetXAxis(x, y, z: Double; w: Double=0); inline;
    procedure SetYAxis(const v: TVector; w: Double=0); inline;
    procedure SetYAxis(x, y, z: Double; w: Double=0); inline;
    procedure SetZAxis(const v: TVector; w: Double=0); inline;
    procedure SetZAxis(x, y, z: Double; w: Double=0); inline;
    procedure SetTranslation(const v: TVector; w: Double=1); inline;
    procedure SetTranslation(x, y, z: Double; w: Double=1); inline;
    procedure SetAxes(x, y, z, w: TVector; xw: Double=0; yw: Double=0; zw: Double=0; ww: Double=1); inline;
    procedure SetAxes(x, y, z: TVector; xw: Double=0; yw: Double=0; zw: Double=0); inline;
    procedure RemoveTranslation; inline;
  end;

  { TFace }

  PFace = ^TFace;
  TPFaceArray = array of PFace;
  TFace = object
    v: TVectorArray;
    c: TVectorArray; { lightmap colors }
    n: TVectorArray; { normals }
    tx, ty, tshift: TVector; { texture axes }
    BrushFace: TObject;
    ls, lt, fs, ft: array of Double; { lightmap and face texture coordinates }
    Lightmap: TObject; { lightmap }

    procedure Assign(const AFace: TFace); inline;
    procedure Clear;
    procedure AddVertex(const P: TVector);
    procedure AddVertex(const P, Nor: TVector);
    procedure Invert; inline;
    function CheckValidity: Boolean;
    procedure MergeZeroLengthEdges;
    procedure SnapToInteger;
    procedure Clip(p: TPlane; InFront: Boolean=True);
    procedure Project(const p: TPlane);
    function SideOfPlane(const p: TPlane): TPlaneSide;
    {$IFDEF RTWORLD}
    function PointInFace(const p: TVector): Boolean;
    {$ENDIF}
    function PointInFace(const p: TVector; const Plane: TPlane): Boolean;
    function ClosestEdgePoint(const p: TVector; const Plane: TPlane): TVector;
    function ToConvexFaces: TPFaceArray;
    function IsConvex: Boolean;
    function NormalAt(const p: TVector): TVector;
    function CalcCenter: TVector;
    function CalcNormal: TVector;
    procedure InitializeNormalsTo(const nv: TVector);
  end;

  { TAABox }

  TAABox = object
  private
    function GetExtent: TVector; inline;
    function GetHeight: Double; inline;
    function GetLength: Double; inline;
    function GetWidth: Double; inline;
    procedure SetExtent(AValue: TVector); inline;
    procedure SetHeight(AValue: Double); inline;
    procedure SetLength(AValue: Double); inline;
    procedure SetWidth(AValue: Double); inline;
  public
    a, b: TVector;

    procedure Zero;
    procedure MakeMaxInverted; inline;
    procedure Include(const Box: TAABox); inline;
    procedure Include(const V: TVector); inline;
    procedure Include(const Face: TFace); inline;
    procedure MakeProper; inline;
    function Includes(const V: TVector): Boolean; inline;
    function Includes(const Box: TAABox): Boolean; inline;
    procedure Snap(GridSize: Integer); inline;
    function Overlaps(const Box: TAABox): Boolean; inline;
    procedure Grow(V: Double); inline;
    procedure MoveTo(const V: TVector); inline;
    procedure GetBoundingSphere(out Center: TVector; out Radius: Double); inline;
    //  minA------minB                   ^
    //    |\      |\                     | y-axis
    //    | \     | \      minA=a    |\  |
    //    |minC----minD    maxD=b  z-axis|
    //  maxA |---maxB|                  \|
    //     \ |     \ |                   *---------> x-axis
    //      \|      \|       (z towards the back)
    //     maxC----maxD
    procedure GetCorners(out MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD: TVector); inline;
    property Width: Double read GetWidth write SetWidth; // X size
    property Height: Double read GetHeight write SetHeight; // Y size
    property Length: Double read GetLength write SetLength; // Z size
    property Extent: TVector read GetExtent write SetExtent;
    function Center: TVector; inline;
  end;

  { TRay }

  TRay = object
    o, d: TVector;
    procedure FromSegment(const A, B: TVector); inline;
    function SphereHit(const sc: TVector; sr: Double; out ip: TVector): Boolean; inline;
    function PlaneHit(const Plane: TPlane; out ip: TVector): Boolean; inline;
    function TriangleHit(const A, B, C: TVector; out ip: TVector): Boolean; inline;
    function TriangleHit(const A, AB, AC: TVector; const P: TPlane; UU, UV, VV, DD: Double; out ip: TVector): Boolean; inline;
    function FaceHit(const Face: TFace; out ip: TVector): Boolean; inline;
    function AABoxHit(const AABox: TAABox): Boolean; inline;
    function AABoxHit(const AABox: TAABox; out t: Double): Boolean; inline;
    function AABoxHit(const AABox: TAABox; out ip: TVector): Boolean; inline;
    function CapsuleHit(const Head, Tail: TVector; Width: Double; out ip: TVector): Boolean; inline;
  end;

  { TConvexHullEdge }

  TConvexHullEdge = record
    F, A, B: Integer; // Faces[F].v[A] to Faces[F].v[B]
  end;

  { TConvexHull }

  TConvexHull = class
  public
    Planes: array of TPlane;
    Faces: array of TFace;
    Edges: array of TConvexHullEdge;

    procedure Assign(AHull: TConvexHull);
    procedure FromBox(const Box: TAABox);
    procedure FromMesh(const Mesh: TMeshBase);
    procedure AddPlane(const APlane: TPlane);
    function Inside(const P: TVector): Boolean;
    procedure IncludePoint(const P: TVector);
    function Intersect(const A, B: TVector; out IP: TVector): Boolean;
    function Intersect(const A, B: TVector; out FIdx: Integer; out IP: TVector): Boolean;
    function Intersect(AHull: TConvexHull; out IP, IPN: TVector): Boolean; // CalcEdges on both
    procedure CalcFaces;
    procedure CalcEdges; // Needs CalcFaces
    function CalcCenter: TVector; // Needs CalcFaces
    function ClosestPointOnFaces(const P: TVector): TVector; // Needs CalcFaces
    procedure ClosestPointAndPlaneIndexOnFaces(const P: TVector; out CP: TVector; out Idx: Integer); // Needs CalcFaces
    function Clone: TConvexHull;
    function Valid: Boolean;
  end;

  { TVector2D }

  TVector2D = packed object
    X, Y: Double;

    procedure Zero; inline;
    procedure Invert; inline;
    function Equal(const V: TVector2D): Boolean; inline;
    function LengthSq: Double; inline;
    function Length: Double; inline;
    function GetMajorAxis: TAxis; inline;
    procedure Add(const v: TVector2D); inline;
    procedure Sub(const v: TVector2D); inline;
    function Added(const v: TVector2D): TVector2D; inline;
    function Subbed(const v: TVector2D): TVector2D; inline;
    procedure Scale(s: Double); inline;
    function Scaled(s: Double): TVector2D; inline;
    procedure Multiply(const v: TVector2D); inline;
    function Multiplied(const v: TVector2D): TVector; inline;
    procedure Snap(GridSize: Integer); inline;
    function Snapped(GridSize: Integer): TVector2D; inline;
    procedure Dump; inline;
  end;

  TVector2DArray = array of TVector2D;

  { T2DShape }

  T2DShape = object
  public
    Vertices: TVector2DArray;

    procedure Add(const V: TVector2D); inline;
    procedure Remove(AIndex: Integer);
    procedure Clear; inline;
    procedure CutHole(const Hole: T2DShape);
    procedure ToFace(out Poly: TFace; Z: Double=0);
    procedure ToTriangles(out Tris: TVector2DArray);
  end;

  { TTransform }

  TTransform = object
  public
    Translation: TVector;
    Rotation: TVector;
    Scale: TVector;
    procedure Reset;
    procedure FromMatrix(AMatrix: TMatrix);
    function ToMatrix: TMatrix;
    function ReplaceTranslation(const ATranslation: TVector): TTransform; inline;
    function ReplaceRotation(const ARotation: TVector): TTransform; inline;
    function ReplaceScale(const AScale: TVector): TTransform; inline;
  end;

function Vector(x, y, z: Double): TVector; inline;
function Vector(x, y: Double): TVector; inline;
function IntVector(x, y, z: Integer): TIntVector; inline;
function Vector2D(X, Y: Double): TVector2D; inline;
function Plane(const N: TVector; D: Double): TPlane; inline;
function Plane(NX, NY, NZ, D: Double): TPlane; inline;
function Plane(const P, N: TVector): TPlane; inline;
function Plane(const a, b, c: TVector): TPlane; inline;
function AABox(const A, B: TVector): TAABox; inline;
function AABox(MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Double): TAABox; inline;
function AABoxOfTransformedAABox(const Box: TAABox; const M: TMatrix): TAABox; inline;
function Transform(const ATranslation, ARotation, AScale: TVector): TTransform;
function VectorToIntVector(const v: TVector): TIntVector; inline;
function IntVectorToVector(const v: TIntVector): TVector; inline;
function Vector2DToVector(const v: TVector2D; Z: Double=0): TVector; inline;
function VectorToVector2D(const v: TVector): TVector2D; inline;
function VectorToColor(const v: TVector): TColor; inline;
function ColorToVector(c: TColor): TVector; inline;
function ArrayToMatrix(const M: T16dArray): TMatrix; inline;
function Direction(const Source, Destination: TVector): TVector; inline;
function Distance(const a, b: TVector): Double; inline;
function DistanceSq(const a, b: TVector): Double; inline;
function Distance(const a, b: TVector2D): Double; inline;
function DistanceSq(const a, b: TVector2D): Double; inline;
function MidpointBetween(const a, b: TVector): TVector; inline;
function MidpointBetween(const a, b: TVector2D): TVector2D; inline;
function NormalBetween(const a, b: TVector): TVector; inline;
function Reflection(const v, n: TVector): TVector; inline;
function InnerAngleBetweenNormals(const a, b: TVector): Double; inline;
function AngleBetweenVectorsOnPlane(const a, b, pn: TVector): Double; inline;
// Used to be VectorProjection but didn't work, renamed to find uses in older projects
function ProjectVectorOnVector(const a, b: TVector): TVector; inline;
function DistanceBetweenSegments(const s1a, s1b, s2a, s2b: TVector; s1p: PVector=nil; s2p: PVector=nil): Double;
function SegmentIntersection2D(const s1a, s1b, s2a, s2b: TVector2D; out IP: TVector2D): Boolean;
function TriangleNormal(const a, b, c: TVector): TVector; inline;
procedure TriangleBarycentric(const p, a, b, c: TVector; out bu, bv, bw: Double); inline;
function Interpolation(const a, b, p: TVector): Double; inline;
function Interpolate(const a, b: TVector; Int: Double): TVector; inline;
function Interpolate(const a, b, Int: Double): Double; inline;
function Zero(v: Double): Boolean; inline;
function Equal(a, b: Double): Boolean; inline;
function ExtMod(a, b: Double): Double; inline;
function Max(a, b: Double): Double; inline;
function Max(a, b, c: Double): Double; inline;
function Min(a, b: Double): Double; inline;
function Min(a, b, c: Double): Double; inline;
function Max(a, b: Integer): Integer; inline;
function Max(a, b, c: Integer): Integer; inline;
function Min(a, b: Integer): Integer; inline;
function Min(a, b, c: Integer): Integer; inline;
function Clamp(v: Double; Min: Double=0; Max: Double=1): Double; inline;
function Clamp(v: Integer; Min: Integer=0; Max: Integer=MaxInt): Integer; inline;
function Sign(v: Double): Double; inline;
procedure GrabMatricesAndViewport(var ModelMatrix, ProjMatrix: T16dArray; var Viewport: TViewPortArray);
function UnprojectRay(X, Y: Integer; const ModelMatrix, ProjMatrix: T16dArray; const Viewport: TViewPortArray): TRay;
function IdentityMatrix: TMatrix; inline;

operator=(const A, B: TVector): Boolean; inline;
operator=(const A, B: TVector2D): Boolean; inline;
operator=(const A, B: TPlane): Boolean; inline;
operator=(const A, B: TTransform): Boolean; inline;

implementation

uses
  Meshes
{$IFDEF RTWORLD}
  , Viewports, Brush
{$ENDIF}
  ;

var
  TheIdentityMatrix: TMatrix;

function Max(a, b: Double): Double;
begin
  if a > b then Result:=a else Result:=b;
end;

function Max(a, b, c: Double): Double;
begin
  Result:=Max(Max(a, b), Max(b, c));
end;

function Min(a, b: Double): Double;
begin
  if a < b then Result:=a else Result:=b;
end;

function Min(a, b, c: Double): Double;
begin
  Result:=Min(Min(a, b), Min(b, c));
end;

function Max(a, b: Integer): Integer;
begin
  if a > b then Result:=a else Result:=b;
end;

function Max(a, b, c: Integer): Integer;
begin
  Result:=Max(Max(a, b), Max(b, c));
end;

function Min(a, b: Integer): Integer;
begin
  if a < b then Result:=a else Result:=b;
end;

function Min(a, b, c: Integer): Integer;
begin
  Result:=Min(Min(a, b), Min(b, c));
end;

function Sign(v: Double): Double;
begin
  if v > 0 then Sign:=1 else
  if v < 0 then Sign:=-1 else
  Sign:=0;
end;

procedure GrabMatricesAndViewport(var ModelMatrix, ProjMatrix: T16dArray; var Viewport: TViewPortArray);
begin
  glGetDoublev(GL_MODELVIEW_MATRIX, PGLDouble(ModelMatrix));
  glGetDoublev(GL_PROJECTION_MATRIX, PGLDouble(ProjMatrix));
  glGetIntegerv(GL_VIEWPORT, PGLInt(Viewport));
end;

function UnprojectRay(X, Y: Integer; const ModelMatrix, ProjMatrix: T16dArray; const Viewport: TViewPortArray): TRay;
var
  ox, oy, oz: GLDouble;
begin
  gluUnProject(X, Y, 0, ModelMatrix, ProjMatrix, Viewport, @ox, @oy, @oz);
  Result.o.x:=ox;
  Result.o.y:=oy;
  Result.o.z:=oz;
  gluUnProject(X, Y, 1, ModelMatrix, ProjMatrix, Viewport, @ox, @oy, @oz);
  Result.d.x:=ox - Result.o.x;
  Result.d.y:=oy - Result.o.y;
  Result.d.z:=oz - Result.o.z;
  Result.d.Normalize;
end;

function IdentityMatrix: TMatrix;
begin
  Result:=TheIdentityMatrix;
end;

operator=(const A, B: TVector): Boolean;
begin
  Result:=(A.X=B.X) and (A.Y=B.Y) and (A.Z=B.Z);
end;

operator=(const A, B: TVector2D): Boolean;
begin
  Result:=(A.X=B.X) and (A.Y=B.Y);
end;

operator=(const A, B: TPlane): Boolean;
begin
  Result:=(A.n=B.n) and (A.d=B.d);
end;

operator=(const A, B: TTransform): Boolean;
begin
  Result:=(A.Translation=B.Translation) and
          (A.Rotation=B.Rotation) and
          (A.Scale=B.Scale);
end;

function Clamp(v, Min, Max: Integer): Integer;
begin
  if v < Min then v:=Min;
  if v > Max then v:=Max;
  Result:=v;
end;

function Clamp(v, Min, Max: Double): Double;
begin
  if v < Min then v:=Min;
  if v > Max then v:=Max;
  Result:=v;
end;

{ TTransform }

procedure TTransform.Reset;
begin
  Translation:=Vector(0, 0, 0);
  Rotation:=Vector(0, 0, 0);
  Scale:=Vector(1, 1, 1);
end;

procedure TTransform.FromMatrix(AMatrix: TMatrix);
var
  Mtx: TMatrix;
begin
  Scale.x:=AMatrix.XAxis.Length;
  Scale.y:=AMatrix.YAxis.Length;
  Scale.z:=AMatrix.ZAxis.Length;
  AMatrix.NormalizeAxes;
  Translation:=AMatrix.Translation;
  Rotation.z:=RadToDeg(AngleBetweenVectorsOnPlane(
    Vector(Sign(AMatrix.ZAxis.z), 0, 0),
    AMatrix.XAxis,
    AMatrix.ZAxis.Normalized));
  Mtx.ZRotation(-DegToRad(Rotation.z));
  AMatrix.Multiply(Mtx);
  Rotation.y:=RadToDeg(AngleBetweenVectorsOnPlane(
    Vector(Sign(AMatrix.YAxis.y), 0, 0),
    AMatrix.XAxis,
    AMatrix.YAxis.Normalized));
  Mtx.YRotation(-DegToRad(Rotation.y));
  AMatrix.Multiply(Mtx);
  Rotation.x:=RadToDeg(AngleBetweenVectorsOnPlane(
    Vector(0, 1, 0),
    AMatrix.YAxis,
    AMatrix.XAxis.Normalized));
end;

function TTransform.ToMatrix: TMatrix;
var
  Tmp: TMatrix;
begin
  Result:=IdentityMatrix;
  Tmp.Translation(Translation.x, Translation.y, Translation.z);
  Result.Multiply(Tmp);
  Tmp.XYZRotation(Rotation.x*PI/180, Rotation.y*PI/180, Rotation.z*PI/180);
  Result.Multiply(Tmp);
  Tmp.Scaling(Scale.x, Scale.y, Scale.z);
  Result.Multiply(Tmp);
end;

function TTransform.ReplaceTranslation(const ATranslation: TVector): TTransform;
begin
  Result.Translation:=ATranslation;
  Result.Rotation:=Rotation;
  Result.Scale:=Scale;
end;

function TTransform.ReplaceRotation(const ARotation: TVector): TTransform;
begin
  Result.Translation:=Translation;
  Result.Rotation:=ARotation;
  Result.Scale:=Scale;
end;

function TTransform.ReplaceScale(const AScale: TVector): TTransform;
begin
  Result.Translation:=Translation;
  Result.Rotation:=Rotation;
  Result.Scale:=AScale;
end;

{ TVector2D }

procedure TVector2D.Zero;
begin
  X:=0;
  Y:=0;
end;

procedure TVector2D.Invert;
begin
  X:=-X;
  Y:=-Y;
end;

function TVector2D.Equal(const V: TVector2D): Boolean;
begin
  Result:=(X=V.X) and (Y=V.Y);
end;

function TVector2D.LengthSq: Double;
begin
  Result:=X*X + Y*Y;
end;

function TVector2D.Length: Double;
begin
  Result:=Sqrt(X*X + Y*Y);
end;

function TVector2D.GetMajorAxis: TAxis;
begin
  if Abs(X) > Abs(Y) then Result:=axX else Result:=axY;
end;

procedure TVector2D.Add(const v: TVector2D);
begin
  X:=X + v.X;
  Y:=Y + v.Y;
end;

procedure TVector2D.Sub(const v: TVector2D);
begin
  X:=X - v.X;
  Y:=Y - v.Y;
end;

function TVector2D.Added(const v: TVector2D): TVector2D;
begin
  Result.x:=x + v.x;
  Result.y:=y + v.y;
end;

function TVector2D.Subbed(const v: TVector2D): TVector2D;
begin
  Result.x:=x - v.x;
  Result.y:=y - v.y;
end;

procedure TVector2D.Scale(s: Double);
begin
  x:=x*s;
  y:=y*s;
end;

function TVector2D.Scaled(s: Double): TVector2D;
begin
  Result.x:=x*s;
  Result.y:=y*s;
end;

procedure TVector2D.Multiply(const v: TVector2D);
begin
  X:=X * v.X;
  Y:=Y * v.Y;
end;

function TVector2D.Multiplied(const v: TVector2D): TVector;
begin
  Result.X:=X * v.X;
  Result.Y:=Y * v.Y;
end;

procedure TVector2D.Snap(GridSize: Integer);
begin
  X:=Round(X) div GridSize * GridSize;
  Y:=Round(Y) div GridSize * GridSize;
end;

function TVector2D.Snapped(GridSize: Integer): TVector2D;
begin
  Result:=Self;
  Result.Snap(GridSize);
end;

procedure TVector2D.Dump;
begin
  if IsConsole then Writeln('(', x:4:4, ',', y:4:4, ')');
end;

{ T2DShape }

procedure T2DShape.Add(const V: TVector2D);
begin
  SetLength(Vertices, Length(Vertices) + 1);
  Vertices[High(Vertices)]:=V;
end;

procedure T2DShape.Remove(AIndex: Integer);
var
  I: Integer;
begin
  Assert((AIndex >= 0) and (AIndex < Length(Vertices)), 'Invalid index');
  for I:=AIndex to High(Vertices) - 1 do Vertices[I]:=Vertices[I + 1];
  SetLength(Vertices, Length(Vertices) - 1);
end;

procedure T2DShape.Clear;
begin
  SetLength(Vertices, 0);
end;

procedure T2DShape.CutHole(const Hole: T2DShape);
var
  I, Best: Integer;
  DSq: Double = 0;
  V0: TVector;
  NV: TVector2DArray;
  Sq: Double;

  procedure VAdd(const V: TVector2D);
  begin
    SetLength(NV, Length(NV) + 1);
    NV[High(NV)]:=V;
  end;

begin
  Assert(Length(Hole.Vertices) > 0, 'Empty hole');

  // Find closest index
  V0:=Vector2DToVector(Hole.Vertices[0]);
  Best:=-1;
  for I:=0 to High(Vertices) do begin
    Sq:=DistanceSq(Vector2DToVector(Vertices[I]), V0);
    if (Best=-1) or (Sq < DSq) then begin
      DSq:=Sq;
      Best:=I;
    end;
  end;

  // Create new vertices array
  SetLength(NV, 0);
  for I:=0 to Best do VAdd(Vertices[I]);
  for I:=High(Hole.Vertices) downto 0 do VAdd(Hole.Vertices[I]);
  VAdd(Hole.Vertices[High(Hole.Vertices)]);
  for I:=Best to High(Vertices) do VAdd(Vertices[I]);
  Vertices:=NV;
end;

procedure T2DShape.ToFace(out Poly: TFace; Z: Double);
var
  I: Integer;
begin
  SetLength(Poly.v, Length(Vertices));
  for I:=0 to High(Vertices) do Poly.v[I]:=Vector2DToVector(Vertices[I], Z);
end;

procedure T2DShape.ToTriangles(out Tris: TVector2DArray);
type
  TEdge = record A, B: TVector2D; end;
var
  Edges: array [0..16383] of TEdge;
  EdgeC: Integer;
  BeforeBest, Best, I: Integer;
  P: TPlane;

  function Edge(A, B: TVector2D): TEdge;
  begin
    Result.A:=A;
    Result.B:=B;
  end;

  procedure EAdd(E: TEdge);
  begin
    Edges[EdgeC]:=E;
    Inc(EdgeC);
  end;

  procedure ERemove(Index: Integer);
  var
    I: Integer;
  begin
    for I:=Index to EdgeC - 2 do Edges[I]:=Edges[I + 1];
    Dec(EdgeC);
  end;

  procedure TAdd(const A, B, C: TVector2D);
  begin
    SetLength(Tris, Length(Tris) + 3);
    Tris[Length(Tris) - 3]:=A;
    Tris[Length(Tris) - 2]:=B;
    Tris[Length(Tris) - 1]:=C;
  end;

  function IsEar(const A, B, C: TVector2D): Boolean;
  var
    IP: TVector2D;
    I: Integer;
    P1, P2, P3: TPlane;
  begin
    // Check if the edges cross the A-C ear edge
    for I:=0 to EdgeC - 1 do begin
      if SegmentIntersection2D(A, C, Edges[I].A, Edges[I].B, IP) then begin
        if (DistanceSq(IP, Edges[I].A) > EPSILON) and (DistanceSq(IP, Edges[I].B) > EPSILON) and
           (DistanceSq(IP, A) > EPSILON) and (DistanceSq(IP, C) > EPSILON) then Exit(False);
      end;
    end;

    // Make triangle space
    P1.FromThreePoints(Vector2DToVector(A), Vector2DToVector(B), Vector2DToVector(B, 1));
    P2.FromThreePoints(Vector2DToVector(B), Vector2DToVector(C), Vector2DToVector(C, 1));
    P3.FromThreePoints(Vector2DToVector(C), Vector2DToVector(A), Vector2DToVector(A, 1));

    // Check if any of the edge vertices is inside the triangle space
    for I:=0 to EdgeC - 1 do begin
      if (P1.SignDist(Vector2DToVector(Edges[I].A)) < -EPSILON) and
         (P2.SignDist(Vector2DToVector(Edges[I].A)) < -EPSILON) and
         (P3.SignDist(Vector2DToVector(Edges[I].A)) < -EPSILON) then begin
        Exit(False);
      end;
    end;

    Result:=True;
  end;

begin
  SetLength(Tris, 0);
  if Length(Vertices) < 3 then Exit;

  // Create edges list
  EdgeC:=0;
  for I:=0 to High(Vertices) - 1 do
    EAdd(Edge(Vertices[I], Vertices[I + 1]));
  EAdd(Edge(Vertices[High(Vertices)], Vertices[0]));

  // Clip ear triangles until there are only three edges left
  while EdgeC > 3 do begin
    // Find ear
    Best:=-1;
    for I:=0 to EdgeC - 1 do begin
      //N1:=TriangleNormal(Vector2DToVector(Edges[I].A), Vector2DToVector(Edges[I].B), Vector2DToVector(Edges[I].B, 1));
      //N2:=TriangleNormal(Vector2DToVector(Edges[(I + 1) mod EdgeC].A), Vector2DToVector(Edges[(I + 1) mod EdgeC].B), Vector2DToVector(Edges[(I + 1) mod EdgeC].B, 1));
      //if (N1.Dot(N2) < 0) then begin
      P.FromThreePoints(Vector2DToVector(Edges[I].A), Vector2DToVector(Edges[(I + 1) mod EdgeC].B), Vector2DToVector(Edges[(I + 1) mod EdgeC].B, 1));
      if P.Front(Vector2DToVector(Edges[I].B)) then begin
        if IsEar(Edges[I].A, Edges[I].B, Edges[(I + 1) mod EdgeC].B) then begin
          Best:=(I + 1) mod EdgeC;
          Break;
        end;
      end;
    end;
    if Best=-1 then break;

    if Best > 0 then BeforeBest:=Best - 1 else BeforeBest:=EdgeC - 1;

    TAdd(Edges[BeforeBest].A, Edges[Best].A, Edges[Best].B);
    Edges[BeforeBest].B:=Edges[Best].B;
    ERemove(Best);
  end;

  // Add the last one
  TAdd(Edges[0].A, Edges[1].A, Edges[2].A);
end;

{ TIntVector }

procedure TIntVector.Zero;
begin
  x:=0;
  y:=0;
  z:=0;
end;

procedure TIntVector.Invert;
begin
  x:=-x;
  y:=-y;
  z:=-z;
end;

function TIntVector.Equal(const V: TIntVector): Boolean;
begin
  Result:=(x=V.x) and (y=V.y) and (z=V.z);
end;

function TIntVector.LengthSq: Integer;
begin
  Result:=x*x + y*y +z*z;
end;

function TIntVector.Length: Double;
begin
  Result:=Sqrt(x*x + y*y +z*z);
end;

function TIntVector.GetMajorAxis: TAxis;
var
  ax, ay, az: Integer;
begin
  ax:=Abs(x);
  ay:=Abs(y);
  az:=Abs(z);
  if ax > ay then begin
    if ax > az then Exit(axX) else Exit(axZ);
  end else begin
    if ay > az then Exit(axY) else Exit(axZ);
  end;
end;

procedure TIntVector.Add(const v: TIntVector);
begin
  Inc(x, v.x);
  Inc(y, v.y);
  Inc(z, v.z);
end;

procedure TIntVector.Sub(const v: TIntVector);
begin
  Dec(x, v.x);
  Dec(y, v.y);
  Dec(z, v.z);
end;

function TIntVector.Added(const v: TIntVector): TIntVector;
begin
  Result.x:=x + v.x;
  Result.y:=y + v.y;
  Result.z:=z + v.z;
end;

function TIntVector.Subbed(const v: TIntVector): TIntVector;
begin
  Result.x:=x - v.x;
  Result.y:=y - v.y;
  Result.z:=z - v.z;
end;

procedure TIntVector.Multiply(const v: TIntVector);
begin
  x:=x*v.x;
  y:=y*v.y;
  z:=z*v.z;
end;

procedure TIntVector.Snap(GridSize: Integer);
begin
  X:=X div GridSize * GridSize;
  Y:=Y div GridSize * GridSize;
  Z:=Z div GridSize * GridSize;
end;

function TIntVector.Snapped(GridSize: Integer): TIntVector;
begin
  Result:=Self;
  Result.Snap(GridSize);
end;

procedure TIntVector.Dump;
begin
  if IsConsole then Writeln('(', x:6, ',', y:6, ',', z:6, ')');
end;

{ TConvexHull }

procedure TConvexHull.Assign(AHull: TConvexHull);
var
  I: Integer;
begin
  SetLength(Planes, Length(AHull.Planes));
  for I:=0 to High(Planes) do Planes[I]:=AHull.Planes[I];
end;

procedure TConvexHull.FromBox(const Box: TAABox);
var
  Plane: TPlane;
begin
  SetLength(Planes, 0);
  Plane.FromPointAndNormal(Box.a, Vector(-1, 0, 0)); AddPlane(Plane);
  Plane.FromPointAndNormal(Box.a, Vector(0, -1, 0)); AddPlane(Plane);
  Plane.FromPointAndNormal(Box.a, Vector(0, 0, -1)); AddPlane(Plane);
  Plane.FromPointAndNormal(Box.b, Vector(1, 0, 0)); AddPlane(Plane);
  Plane.FromPointAndNormal(Box.b, Vector(0, 1, 0)); AddPlane(Plane);
  Plane.FromPointAndNormal(Box.b, Vector(0, 0, 1)); AddPlane(Plane);
end;

procedure TConvexHull.FromMesh(const Mesh: TMeshBase);
var
  M: TMesh absolute Mesh;
  I, J: Integer;
  Plane: TPlane;
  Found: Boolean;
begin
  SetLength(Planes, 0);
  I:=0;
  while I < M.IndexCount do begin
    Plane.FromThreePoints(M.Vertices[M.Indices[I]], M.Vertices[M.Indices[I + 1]], M.Vertices[M.Indices[I + 2]]);
    Inc(I, 3);
    Found:=False;
    for J:=High(Planes) downto 0 do
      if Plane=Planes[J] then begin
        Found:=True;
        Break;
      end;
    if not Found then AddPlane(Plane);
  end;
end;

procedure TConvexHull.AddPlane(const APlane: TPlane);
begin
  SetLength(Planes, Length(Planes) + 1);
  Planes[High(Planes)]:=APlane;
end;

function TConvexHull.Inside(const P: TVector): Boolean;
var
  I: Integer;
begin
  Inside:=True;
  for I:=0 to High(Planes) do
    if Planes[I].Front(P) then Exit(False);
end;

procedure TConvexHull.IncludePoint(const P: TVector);
// TODO: find some faster method
type
  TEdge = record
    A, B: TVector;
    PA, PB: TPlane;
  end;
var
  I, J: Integer;
  TheEdges: array of TEdge;

  procedure AddEdge(A, B: TVector; const Pl: TPlane);
  var
    I: Integer;
  begin
    for I:=0 to High(TheEdges) do
      if (A.Equal(TheEdges[I].A) and B.Equal(TheEdges[I].B)) or
         (A.Equal(TheEdges[I].B) and B.Equal(TheEdges[I].A)) then begin
           TheEdges[I].PB:=Pl;
           Exit;
         end;
    SetLength(TheEdges, Length(TheEdges) + 1);
    if Pl.Front(P) then begin
      TheEdges[High(TheEdges)].A:=A;
      TheEdges[High(TheEdges)].B:=B;
    end else begin
      TheEdges[High(TheEdges)].A:=B;
      TheEdges[High(TheEdges)].B:=A;
    end;
    TheEdges[High(TheEdges)].PA:=Pl;
  end;

begin
  if Inside(P) then Exit;

  SetLength(TheEdges, 0);
  for I:=0 to High(Faces) do begin
    for J:=1 to High(Faces[I].v) do
      AddEdge(Faces[I].v[J - 1], Faces[I].v[J], Planes[I]);
    AddEdge(Faces[I].v[High(Faces[I].V)], Faces[I].v[0], Planes[I]);
  end;
  I:=0;
  while I < Length(Planes) do begin
    if Planes[I].Front(P) then begin
      for J:=I to High(Planes) - 1 do Planes[J]:=Planes[J + 1];
      SetLength(Planes, Length(Planes) - 1);
    end else
      Inc(I);
  end;
  for I:=0 to High(TheEdges) do
    if TheEdges[I].PA.Front(P) <> TheEdges[I].PB.Front(P) then begin
      AddPlane(Plane(TheEdges[I].A, TheEdges[I].B, P));
    end;
  CalcFaces;
end;

function TConvexHull.Intersect(const A, B: TVector; out IP: TVector): Boolean;
var
  FIdx: Integer;
begin
  Result:=Intersect(A, B, FIdx, IP);
end;

function TConvexHull.Intersect(const A, B: TVector; out FIdx: Integer; out
  IP: TVector): Boolean;
var
  I, J: Integer;
  ClosestIP: TVector;
  ClosestD, D: Double;
  IsFront: Boolean;
begin
  ClosestD:=MaxInt;
  FIdx:=-1;
  Result:=False;
  for I:=0 to High(Planes) do
    if Planes[I].Intersect(A, B, IP) then begin
      IsFront:=False;
      for J:=0 to High(Planes) do
        if (J <> I) and Planes[J].Front(IP) then begin
          IsFront:=True;
          Break;
        end;
      if not IsFront then begin
        D:=DistanceSq(A, IP);
        Result:=True;
        if D < ClosestD then begin
          ClosestD:=D;
          ClosestIP:=IP;
          FIdx:=I;
        end;
      end;
    end;
  IP:=ClosestIP;
end;

function TConvexHull.Intersect(AHull: TConvexHull; out IP, IPN: TVector): Boolean;
type
  TPoint = record
    P: TVector;
    PNI: Integer;
  end;

var
  Points: array of TPoint;
  I, FIdx, ClosestPlaneIndex: Integer;
  ClosestPlaneDistance, PlaneDist: Double;
  C, HullCenter: TVector;
begin
  Points:=nil;
  // Collect intersecting edges, if any
  for I:=0 to High(AHull.Edges) do with AHull.Edges[I] do begin
    if Intersect(AHull.Faces[F].v[A], AHull.Faces[F].v[B], FIdx, IP) then begin
      SetLength(Points, Length(Points) + 1);
      Points[High(Points)].P:=IP;
      Points[High(Points)].PNI:=FIdx;
    end;
  end;
  for I:=0 to High(Edges) do with Edges[I] do begin
    if AHull.Intersect(Faces[F].v[A], Faces[F].v[B], IP) then begin
      SetLength(Points, Length(Points) + 1);
      Points[High(Points)].P:=IP;
      Points[High(Points)].PNI:=F;
    end;
  end;
  // Check if the face centers are inside
  for I:=0 to High(AHull.Faces) do begin
    C:=AHull.Faces[I].CalcCenter;
    if Inside(C) then begin
      SetLength(Points, Length(Points) + 1);
      ClosestPointAndPlaneIndexOnFaces(C, Points[High(Points)].P, Points[High(Points)].PNI);
    end;
  end;
  // If there are no intersecting edges, the given hull might be inside
  if Inside(AHull.Faces[0].v[0]) then begin
    SetLength(Points, 1);
    Points[0].P:=AHull.Faces[0].v[0];
    ClosestPointAndPlaneIndexOnFaces(Points[0].P, C, Points[0].PNI); // C is dummy
  end;
  // Calculate average collision point, if any
  if Length(Points) > 0 then begin
    IP.Zero;
    IPN.Zero;
    ClosestPlaneIndex:=-1;
    ClosestPlaneDistance:=MaxInt;
    HullCenter:=AHull.CalcCenter;
    for I:=0 to High(Points) do begin
      IP.Add(Points[I].P);
      PlaneDist:=Abs(Planes[Points[I].PNI].SignDist(Points[I].P));
      if PlaneDist < ClosestPlaneDistance then begin
        ClosestPlaneDistance:=PlaneDist;
        ClosestPlaneIndex:=Points[I].PNI;
      end;
    end;
    if Length(Points) > 1 then begin
      IP.Scale(1/Length(Points));
      IPN:=Planes[ClosestPlaneIndex].n;
    end;
    Result:=True;
  end else
    Result:=False;
end;

procedure TConvexHull.CalcFaces;
var
  I, J, K: Integer;

  procedure CalcFace(Index: Integer);
  var
    F: TFace;
    Plane: TPlane;
    V, C: TVector;
    I, J: Integer;
    sv: Single;
    OrigVectors: array [0..3] of TVector;
  begin
    Plane:=Planes[Index];
    Plane.n.Normalize;
    SetLength(F.v, 4);
    case Plane.n.GetMajorAxis of
      axX: begin
        if Plane.n.x < 0 then begin
          F.v[0].x:=0; F.v[0].y:=1; F.v[0].z:=1;
          F.v[1].x:=0; F.v[1].y:=1; F.v[1].z:=-1;
          F.v[2].x:=0; F.v[2].y:=-1; F.v[2].z:=-1;
          F.v[3].x:=0; F.v[3].y:=-1; F.v[3].z:=1;
        end else begin
          F.v[0].x:=0; F.v[0].y:=1; F.v[0].z:=1;
          F.v[1].x:=0; F.v[1].y:=-1; F.v[1].z:=1;
          F.v[2].x:=0; F.v[2].y:=-1; F.v[2].z:=-1;
          F.v[3].x:=0; F.v[3].y:=1; F.v[3].z:=-1;
        end;
      end;
      axY: begin
        if Plane.n.y < 0 then begin
          F.v[0].x:=-1; F.v[0].y:=0; F.v[0].z:=-1;
          F.v[1].x:=1; F.v[1].y:=0; F.v[1].z:=-1;
          F.v[2].x:=1; F.v[2].y:=0; F.v[2].z:=1;
          F.v[3].x:=-1; F.v[3].y:=0; F.v[3].z:=1;
        end else begin
          F.v[0].x:=-1; F.v[0].y:=0; F.v[0].z:=-1;
          F.v[1].x:=-1; F.v[1].y:=0; F.v[1].z:=1;
          F.v[2].x:=1; F.v[2].y:=0; F.v[2].z:=1;
          F.v[3].x:=1; F.v[3].y:=0; F.v[3].z:=-1;
        end;
      end;
      axZ: begin
        if Plane.n.z < 0 then begin
          F.v[0].x:=-1; F.v[0].y:=1; F.v[0].z:=0;
          F.v[1].x:=1; F.v[1].y:=1; F.v[1].z:=0;
          F.v[2].x:=1; F.v[2].y:=-1; F.v[2].z:=0;
          F.v[3].x:=-1; F.v[3].y:=-1; F.v[3].z:=0;
        end else begin
          F.v[0].x:=-1; F.v[0].y:=1; F.v[0].z:=0;
          F.v[1].x:=-1; F.v[1].y:=-1; F.v[1].z:=0;
          F.v[2].x:=1; F.v[2].y:=-1; F.v[2].z:=0;
          F.v[3].x:=1; F.v[3].y:=1; F.v[3].z:=0;
        end;
      end;
    end;
    for I:=0 to 3 do F.v[I]:=Plane.Projected(F.v[I]);
    C.x:=(F.v[0].x + F.v[1].x + F.v[2].x + F.v[3].x)/4;
    C.y:=(F.v[0].y + F.v[1].y + F.v[2].y + F.v[3].y)/4;
    C.z:=(F.v[0].z + F.v[1].z + F.v[2].z + F.v[3].z)/4;
    for I:=0 to 3 do begin
      V.x:=F.v[i].x - C.x;
      V.y:=F.v[i].y - C.y;
      V.z:=F.v[i].z - C.z;
      V.Normalize;
      F.v[i].x:=C.x + V.x*100000.0;
      F.v[i].y:=C.y + V.y*100000.0;
      F.v[i].z:=C.z + V.z*100000.0;
      OrigVectors[i]:=F.v[i];
    end;
    for I:=0 to High(Planes) do begin
      if I=Index then Continue;
      F.Clip(Planes[I], False);
    end;
    for I:=0 to High(F.v) do begin
      sv:=F.v[I].x; F.v[I].x:=sv;
      sv:=F.v[I].y; F.v[I].y:=sv;
      sv:=F.v[I].z; F.v[I].z:=sv;
    end;

    F.MergeZeroLengthEdges;

    if F.CheckValidity then begin
      for I:=0 to 3 do
        for J:=0 to High(F.v) do
          if DistanceSq(F.v[J], OrigVectors[I]) < 0.1 then
            Exit;
    end else Exit;

    SetLength(Faces, Length(Faces) + 1);
    Faces[High(Faces)].Assign(F);
  end;

begin
  SetLength(Faces, 0);
  I:=0;
  while I < Length(Planes) do begin
    for J:=0 to High(Planes) do
      if (I <> J) and Equal(Planes[I].n.Dot(Planes[J].n), 1) then begin
        for K:=I to High(Planes) - 1 do Planes[K]:=Planes[K + 1];
        SetLength(Planes, Length(Planes) - 1);
        Dec(I);
        Break;
      end;
    Inc(I);
  end;
  for I:=0 to High(Planes) do
    CalcFace(I);
  SetLength(Planes, Length(Faces));
  for I:=0 to High(Faces) do
    Planes[I].FromThreePoints(Faces[I].v[0], Faces[I].v[1], Faces[I].v[2]);
end;

procedure TConvexHull.CalcEdges;
var
  I, J, Nx: Integer;

  function HasEdge(const vA, vB: TVector): Boolean;
  var
    I: Integer;
  begin
    for I:=High(Edges) downto 0 do with Edges[I] do begin
      if ((vA=Faces[F].v[A]) and (vB=Faces[F].v[B])) or
         ((vB=Faces[F].v[A]) and (vA=Faces[F].v[B])) then Exit(True);
    end;
    Result:=False;
  end;

begin
  SetLength(Edges, 0);
  for I:=0 to High(Faces) do with Faces[I] do begin
    for J:=0 to High(v) do begin
      Nx:=(J + 1) mod Length(v);
      if not HasEdge(v[J], v[Nx]) then begin
        SetLength(Edges, Length(Edges) + 1);
        with Edges[High(Edges)] do begin
          F:=I;
          A:=J;
          B:=Nx;
        end;
      end;
    end;
  end;
end;

function TConvexHull.CalcCenter: TVector;
var
  I: Integer;
begin
  Result.Zero;
  if Length(Faces) > 0 then begin
    for I:=0 to High(Faces) do
      Result.Add(Faces[I].CalcCenter);
    Result.Scale(1/Length(Faces));
  end;
end;

function TConvexHull.ClosestPointOnFaces(const P: TVector): TVector;
var
  Idx: Integer;
begin
  ClosestPointAndPlaneIndexOnFaces(P, Result, Idx);
end;

procedure TConvexHull.ClosestPointAndPlaneIndexOnFaces(const P: TVector; out
  CP: TVector; out Idx: Integer);
var
  I, J: Integer;
  ClosestDistance: Double = MaxInt;
  IsOut: Boolean;
  Dist: Double;
begin
  Idx:=-1;
  for I:=0 to High(Planes) do begin
    CP:=Planes[I].Projected(P);
    IsOut:=False;
    for J:=0 to High(Planes) do
      if (I <> J) and (Planes[J].SignDist(CP) > 0.0) then begin
        IsOut:=True;
        Break;
      end;
    if not IsOut then begin
      Dist:=Abs(Planes[I].SignDist(P));
      if Dist < ClosestDistance then begin
        Idx:=I;
        ClosestDistance:=Dist;
      end;
    end;
  end;
  if Idx <> -1 then
    CP:=Planes[Idx].Projected(P)
  else
    CP.Zero;
end;

function TConvexHull.Clone: TConvexHull;
begin
  Result:=TConvexHull.Create;
  SetLength(Result.Planes, Length(Planes));
  Move(Planes[0], Result.Planes[0], SizeOf(TPlane)*Length(Planes));
  Result.CalcFaces;
end;

function TConvexHull.Valid: Boolean;
begin
  Result:=Length(Faces) >= 4;
end;

{ TRay }

procedure TRay.FromSegment(const A, B: TVector);
begin
  o:=A;
  d:=B;
  d.Sub(o);
  d.Normalize;
end;

function TRay.SphereHit(const sc: TVector; sr: Double; out ip: TVector): Boolean;
var
  b, c, dd, e, t0, t1, t, srx, sry, srz: Double;
begin
  srx:=o.x - sc.x;
  sry:=o.y - sc.y;
  srz:=o.z - sc.z;
  b:=srx*d.x + sry*d.y + srz*d.z;
  c:=(srx*srx + sry*sry + srz*srz) - sr*sr;
  dd:=b*b - c;
  if (dd > EPSILON) then begin
    e:=Sqrt(dd);
    t0:=-b-e;
    t1:=-b+e;
    if t1 < t0 then t:=t1 else t:=t0;
    if t < 0.0 then Exit(False);
    ip.x:=o.x + d.x*t;
    ip.y:=o.y + d.y*t;
    ip.z:=o.z + d.z*t;
    Result:=True;
  end else
    Result:=False;
end;

function TRay.PlaneHit(const Plane: TPlane; out ip: TVector): Boolean;
var
  W: TVector;
  T, VD, VN: Double;
begin
  VD:=Plane.n.Dot(d);
  if VD=0 then Exit(False);
  W.x:=o.x - Plane.n.x*Plane.d;
  W.y:=o.y - Plane.n.y*Plane.d;
  W.z:=o.z - Plane.n.z*Plane.d;
  VN:=-Plane.n.Dot(W);
  T:=VN/VD;
  if T < 0.0 then Exit(False);
  IP.x:=o.x + d.x*t;
  IP.y:=o.y + d.y*t;
  IP.z:=o.z + d.z*t;
  Result:=True;
end;

function TRay.TriangleHit(const A, B, C: TVector; out ip: TVector): Boolean;
var
  P: TPlane;
  AB, AC: TVector;
  uu, uv, vv, wu, wv, wx, wy, wz, dd, sv, tv: Double;
begin
  AB:=B;
  AB.Sub(A);
  AC:=C;
  AC.Sub(A);
  AB.Cross(AC, P.n);
  P.n.Normalize;
  P.d:=A.Dot(P.n);
  if not PlaneHit(P, ip) then Exit(False);
  uu:=AB.x*AB.x + AB.y*AB.y + AB.z*AB.z;
  uv:=AB.x*AC.x + AB.y*AC.y + AB.z*AC.z;
  vv:=AC.x*AC.x + AC.y*AC.y + AC.z*AC.z;
  wx:=ip.x - A.x;
  wy:=ip.y - A.y;
  wz:=ip.z - A.z;
  wu:=wx*AB.x + wy*AB.y + wz*AB.z;
  wv:=wx*AC.x + wy*AC.y + wz*AC.z;
  dd:=uv*uv - uu*vv;
  if dd=0 then Exit(False);
  sv:=(uv*wv - vv*wu)/dd;
  if (sv < 0.0) or (sv > 1.0) then Exit(False);
  tv:=(uv*wu - uu*wv)/dd;
  if (tv < 0.0) or ((sv + tv) > 1.0) then Exit(False);
  Result:=True;
end;

function TRay.TriangleHit(const A, AB, AC: TVector; const P: TPlane; UU, UV, VV, DD: Double; out ip: TVector): Boolean;
var
  wu, wv, wx, wy, wz, sv, tv: Double;
begin
  if not PlaneHit(P, ip) then Exit(False);
  wx:=ip.x - A.x;
  wy:=ip.y - A.y;
  wz:=ip.z - A.z;
  wu:=wx*AB.x + wy*AB.y + wz*AB.z;
  wv:=wx*AC.x + wy*AC.y + wz*AC.z;
  sv:=(uv*wv - vv*wu)/dd;
  if (sv < 0.0) or (sv > 1.0) then Exit(False);
  tv:=(uv*wu - uu*wv)/dd;
  if (tv < 0.0) or ((sv + tv) > 1.0) then Exit(False);
  Result:=True;
end;

function TRay.FaceHit(const Face: TFace; out ip: TVector): Boolean;
var
  i: Integer;
begin
  if Length(Face.v) < 3 then Exit(False);
  for i:=2 to Length(Face.v) - 1 do begin
    if TriangleHit(Face.v[0], Face.v[i - 1], Face.v[i], ip) then Exit(True);
  end;
  Result:=False;
end;

function TRay.AABoxHit(const AABox: TAABox): Boolean;
var
  df: TVector;
  t1, t2, t3, t4, t5, t6, tmin, tmax: Double;
begin
  if d.x=0 then df.x:=0 else df.x:=1/d.x;
  if d.y=0 then df.y:=0 else df.y:=1/d.y;
  if d.z=0 then df.z:=0 else df.z:=1/d.z;
  t1:=(AABox.a.x - o.x)*df.x;
  t2:=(AABox.b.x - o.x)*df.x;
  t3:=(AABox.a.y - o.y)*df.y;
  t4:=(AABox.b.y - o.y)*df.y;
  t5:=(AABox.a.z - o.z)*df.z;
  t6:=(AABox.b.z - o.z)*df.z;
  tmin:=Max(Max(Min(t1, t2), Min(t3, t4)), Min(t5, t6));
  tmax:=Min(Min(Max(t1, t2), Max(t3, t4)), Max(t5, t6));
  if TMax < 0 then Exit(False);
  if TMin > TMax then Exit(False);
  Exit(True);
end;

function TRay.AABoxHit(const AABox: TAABox; out t: Double): Boolean;
var
  df: TVector;
  t1, t2, t3, t4, t5, t6, tmin, tmax: Double;
begin
  df.x:=1/d.x;
  df.y:=1/d.y;
  df.z:=1/d.z;
  t1:=(AABox.a.x - o.x)*df.x;
  t2:=(AABox.b.x - o.x)*df.x;
  t3:=(AABox.a.y - o.y)*df.y;
  t4:=(AABox.b.y - o.y)*df.y;
  t5:=(AABox.a.z - o.z)*df.z;
  t6:=(AABox.b.z - o.z)*df.z;
  tmin:=Max(Max(Min(t1, t2), Min(t3, t4)), Min(t5, t6));
  tmax:=Min(Min(Max(t1, t2), Max(t3, t4)), Max(t5, t6));
  if TMax < 0 then begin
    t:=tmax;
    Exit(False);
  end;
  if TMin > TMax then begin
    t:=tmax;
    Exit(False);
  end;
  t:=tmin;
  Exit(True);
end;

function TRay.AABoxHit(const AABox: TAABox; out ip: TVector): Boolean;
var
  t: Double;
begin
  Result:=AABoxHit(AABox, t);
  ip:=d;
  ip.Scale(t);
  ip.Add(o);
end;

function TRay.CapsuleHit(const Head, Tail: TVector; Width: Double; out ip: TVector): Boolean;
var
  AB, AO, Q, R, TIP, HIP: TVector;
  ABD, ABAO, ABAB, m, n, A, B, C, DD: Double;
  HIN, TIN: Boolean;
  tmin, tmax, tmp, K: Double;
begin
  AB:=Tail.Subbed(Head);
  AO:=o.Subbed(Head);
  ABD:=AB.Dot(d);
  ABAO:=AB.Dot(AO);
  ABAB:=AB.Dot(AB);
  m:=ABD/ABAB;
  n:=ABAO/ABAB;
  Q:=d.Subbed(AB.Scaled(m));
  R:=AO.Subbed(AB.Scaled(n));

  A:=Q.Dot(Q);
  B:=2.0*Q.Dot(R);
  C:=R.Dot(R) - Width*Width;

  if A=0.0 then begin
    HIN:=SphereHit(Head, Width, HIP);
    TIN:=SphereHit(Tail, Width, TIP);
    if not (HIN or TIN) then Exit(False);
  end else begin
    DD:=B*B - 4*A*C;
    if DD < 0 then Exit(False);

    tmin:=(-B - Sqrt(DD))/(2*A);
    tmax:=(-B + Sqrt(DD))/(2*A);

    if tmin > tmax then begin
      tmp:=tmin;
      tmin:=tmax;
      tmax:=tmp;
    end;

    K:=tmin*m + n;
    if K < 0.0 then begin
      if not SphereHit(Head, Width, HIP) then Exit;
    end else if K > 1.0 then begin
      if not SphereHit(Head, Width, HIP) then Exit;
    end else
      HIP:=o.Added(d.Scaled(tmin));

    K:=tmin*m + n;
    if K < 0.0 then begin
      if not SphereHit(Tail, Width, TIP) then Exit;
    end else if K > 1.0 then begin
      if not SphereHit(Tail, Width, TIP) then Exit;
    end else
      TIP:=o.Added(d.Scaled(tmax));
  end;

  if DistanceSq(o, HIP) < DistanceSq(o, TIP) then
    IP:=HIP
  else
    IP:=TIP;
  Result:=True;
end;

{ TVector }

procedure TVector.Zero;
begin
  x:=0;
  y:=0;
  z:=0;
end;

procedure TVector.Copy(const Src: TVector);
begin
  X:=Src.X;
  Y:=Src.Y;
  Z:=Src.Z;
end;

procedure TVector.Normalize;
var
  l: Double;
begin
  l:=Length;
  if l > 0.0 then begin
    x:=x/l;
    y:=y/l;
    z:=z/l;
  end;
end;

function TVector.Normalized: TVector;
begin
  Result:=Self;
  Result.Normalize;
end;

procedure TVector.Invert;
begin
  x:=-x;
  y:=-y;
  z:=-z;
end;

function TVector.Inverted: TVector;
begin
  Result:=Vector(-X, -Y, -Z);
end;

function TVector.Equal(const V: TVector): Boolean;
begin
  Result:=Maths.Equal(V.x, x) and Maths.Equal(V.y, y) and Maths.Equal(V.z, z);
end;

function TVector.IsZero: Boolean;
begin
  Result:=Maths.Zero(X) and Maths.Zero(Y) and Maths.Zero(Z);
end;

function TVector.LengthSq: Double;
begin
  Result:=x*x + y*y +z*z;
end;

function TVector.Length: Double;
begin
  Result:=Sqrt(x*x + y*y +z*z);
end;

function TVector.GetMajorAxis: TAxis;
var
  ax, ay, az: Double;
begin
  ax:=Abs(x);
  ay:=Abs(y);
  az:=Abs(z);
  if ax > ay then begin
    if ax > az then Exit(axX) else Exit(axZ);
  end else begin
    if ay > az then Exit(axY) else Exit(axZ);
  end;
end;

function TVector.Dot(const v: TVector): Double;
begin
  Result:=x*v.x + y*v.y + z*v.z;
end;

procedure TVector.Add(const v: TVector);
begin
  x += v.x;
  y += v.y;
  z += v.z;
end;

procedure TVector.Sub(const v: TVector);
begin
  x -= v.x;
  y -= v.y;
  z -= v.z;
end;

function TVector.Added(const v: TVector): TVector;
begin
  Result.x:=x + v.x;
  Result.y:=y + v.y;
  Result.z:=z + v.z;
end;

function TVector.Subbed(const v: TVector): TVector;
begin
  Result.x:=x - v.x;
  Result.y:=y - v.y;
  Result.z:=z - v.z;
end;

procedure TVector.Cross(const v: TVector; out r: TVector);
begin
  r.x:=y*v.z - z*v.y;
  r.y:=z*v.x - x*v.z;
  r.z:=x*v.y - y*v.x;
end;

function TVector.Crossed(const v: TVector): TVector;
begin
  Result.x:=y*v.z - z*v.y;
  Result.y:=z*v.x - x*v.z;
  Result.z:=x*v.y - y*v.x;
end;

procedure TVector.Scale(s: Double);
begin
  x:=x*s;
  y:=y*s;
  z:=z*s;
end;

function TVector.Scaled(s: Double): TVector;
begin
  Result.x:=x*s;
  Result.y:=y*s;
  Result.z:=z*s;
end;

procedure TVector.Multiply(const v: TVector);
begin
  X:=X*v.X;
  Y:=Y*v.Y;
  Z:=Z*v.Z;
end;

function TVector.Multiplied(const v: TVector): TVector;
begin
  Result.x:=x*v.x;
  Result.y:=y*v.y;
  Result.z:=z*v.z;
end;

procedure TVector.AddAndScale(const v: TVector; s: Double);
begin
  X:=X + v.X*s;
  Y:=Y + v.Y*s;
  Z:=Z + v.Z*s;
end;

procedure TVector.Snap(GridSize: Integer);
begin
  X:=Round(X) div GridSize * GridSize;
  Y:=Round(Y) div GridSize * GridSize;
  Z:=Round(Z) div GridSize * GridSize;
end;

function TVector.Snapped(GridSize: Integer): TVector;
begin
  Result:=Self;
  Result.Snap(GridSize);
end;

procedure TVector.SnapToInteger;
begin
  if Abs(X - Round(X)) < 0.01 then X:=Round(X);
  if Abs(Y - Round(Y)) < 0.01 then Y:=Round(Y);
  if Abs(Z - Round(Z)) < 0.01 then Z:=Round(Z);
end;

procedure TVector.Dump;
begin
  if IsConsole then Writeln('(', x:4:4, ',', y:4:4, ',', z:4:4, ')');
end;

function TVector.ToString: string;
begin
  Result:=FloatToStr(x, DefaultFormatSettings) + ' ' + FloatToStr(y, DefaultFormatSettings) + ' ' + FloatToStr(z, DefaultFormatSettings);
end;

{ TPlane }
procedure TPlane.FromThreePoints(const a, b, c: TVector);
var
  ab, ac: TVector;
begin
  ab.x:=b.x - a.x;
  ab.y:=b.y - a.y;
  ab.z:=b.z - a.z;
  ac.x:=c.x - a.x;
  ac.y:=c.y - a.y;
  ac.z:=c.z - a.z;
  ab.Cross(ac, n);
  n.Normalize;
  d:=n.x*a.x + n.y*a.y + n.z*a.z;
end;

procedure TPlane.FromPointAndNormal(const p, nor: TVector);
var
  tmp: TVector;
begin
  n:=nor;
  tmp:=p;
  tmp.Normalize;
  d:=p.Length*n.Dot(tmp);
end;

procedure TPlane.Invert;
begin
  d:=-d;
  n.Invert;
end;

function TPlane.SignDist(const p: TVector): Double;
var
  a: TVector;
begin
  a.x:=p.x - n.x*d;
  a.y:=p.y - n.y*d;
  a.z:=p.z - n.z*d;
  Result:=a.x*n.x + a.y*n.y + a.z*n.z;
end;

function TPlane.Front(const p: TVector): Boolean;
begin
  Result:=SignDist(p) >= 0.0;
end;

procedure TPlane.Project(var p: TVector);
var
  sd: Double;
begin
  sd:=SignDist(p);
  p.x -= n.x*sd;
  p.y -= n.y*sd;
  p.z -= n.z*sd;
end;

function TPlane.Projected(const p: TVector): TVector;
var
  sd: Double;
begin
  sd:=SignDist(p);
  Result.x:=p.x - n.x*sd;
  Result.y:=p.y - n.y*sd;
  Result.z:=p.z - n.z*sd;
end;

function TPlane.ProjectedNormal(const p: TVector): TVector;
begin
  Result.x:=p.x - n.x*n.x*p.x;
  Result.y:=p.y - n.y*n.y*p.y;
  Result.z:=p.z - n.z*n.z*p.z;
end;

procedure TPlane.Flip(var p: TVector);
var
  sd: Double;
begin
  sd:=SignDist(p);
  p.x -= n.x*sd*2;
  p.y -= n.y*sd*2;
  p.z -= n.z*sd*2;
end;

function TPlane.Flipped(const p: TVector): TVector;
var
  sd: Double;
begin
  sd:=SignDist(p);
  Result.x:=p.x - n.x*sd*2;
  Result.y:=p.y - n.y*sd*2;
  Result.z:=p.z - n.z*sd*2;
end;

function TPlane.Intersect(const a, b: TVector; out p: TVector): Boolean;
var
  u, w, pp: TVector;
  si, vd, vn: Double;
begin
  u.x:=b.x - a.x;
  u.y:=b.y - a.y;
  u.z:=b.z - a.z;

  pp.x:=n.x*d;
  pp.y:=n.y*d;
  pp.z:=n.z*d;

  w.x:=a.x - pp.x;
  w.y:=a.y - pp.y;
  w.z:=a.z - pp.z;

  vd:=n.x*u.x + n.y*u.y + n.z*u.z;
  vn:=-(n.x*w.x + n.y*w.y + n.z*w.z);

  if vd=0 then Exit(False);

  si:=vn/vd;
  if (si < 0.0) or (si > 1) then Exit(False);

  p.x:=a.x + u.x*si;
  p.y:=a.y + u.y*si;
  p.z:=a.z + u.z*si;

  Exit(True);
end;

function TPlane.IntersectNEP(const a, b: TVector; out p: TVector): Boolean;
var
  u, w, pp: TVector;
  si, vd, vn: Double;
begin
  u.x:=b.x - a.x;
  u.y:=b.y - a.y;
  u.z:=b.z - a.z;

  pp.x:=n.x*d;
  pp.y:=n.y*d;
  pp.z:=n.z*d;

  w.x:=a.x - pp.x;
  w.y:=a.y - pp.y;
  w.z:=a.z - pp.z;

  vd:=n.x*u.x + n.y*u.y + n.z*u.z;
  vn:=-(n.x*w.x + n.y*w.y + n.z*w.z);

  if vd=0 then Exit(False);

  si:=vn/vd;
  if (si <= EPSILON) or (si >= 1-EPSILON) then Exit(False);

  p.x:=a.x + u.x*si;
  p.y:=a.y + u.y*si;
  p.z:=a.z + u.z*si;

  Exit(True);
end;

procedure TPlane.CalcTextureAxes(out tx, ty: TVector);
begin
  case n.GetMajorAxis of
    axX: begin
      if n.x < 0 then begin
        tx.x:=0; tx.y:=0; tx.z:=1;
        ty.x:=0; ty.y:=-1; ty.z:=0;
      end else begin
        tx.x:=0; tx.y:=0; tx.z:=-1;
        ty.x:=0; ty.y:=-1; ty.z:=0;
      end;
    end;
    axY: begin
      if n.y < 0 then begin
        tx.x:=1; tx.y:=0; tx.z:=0;
        ty.x:=0; ty.y:=0; ty.z:=-1;
      end else begin
        tx.x:=1; tx.y:=0; tx.z:=0;
        ty.x:=0; ty.y:=0; ty.z:=1;
      end;
    end;
    axZ: begin
      if n.z < 0 then begin
        tx.x:=-1; tx.y:=0; tx.z:=0;
        ty.x:=0; ty.y:=-1; ty.z:=0;
      end else begin
        tx.x:=1; tx.y:=0; tx.z:=0;
        ty.x:=0; ty.y:=-1; ty.z:=0;
      end;
    end;
  end;
end;

function TPlane.CoplaneStyle(const APlane: TPlane): TCoplaneStyle;
begin
  if Equal(d, APlane.d) and Equal(n.Dot(APlane.n), 1.0) then
    Result:=csSame
  else if Equal(d, -APlane.d) and Equal(n.Dot(APlane.n), -1.0) then
    Result:=csInverted
  else
    Result:=csNotCoplanar;
end;

procedure TMatrix.FromArray(const M: T16dArray);
begin
  m11:=M[0];
  m21:=M[1];
  m31:=M[2];
  m41:=M[3];
  m12:=M[4];
  m22:=M[5];
  m32:=M[6];
  m42:=M[7];
  m13:=M[8];
  m23:=M[9];
  m33:=M[10];
  m43:=M[11];
  m14:=M[12];
  m24:=M[13];
  m34:=M[14];
  m44:=M[15];
end;

function TMatrix.ToArray: T16dArray;
begin
  Result[0]:=m11;
  Result[1]:=m21;
  Result[2]:=m31;
  Result[3]:=m41;
  Result[4]:=m12;
  Result[5]:=m22;
  Result[6]:=m32;
  Result[7]:=m42;
  Result[8]:=m13;
  Result[9]:=m23;
  Result[10]:=m33;
  Result[11]:=m43;
  Result[12]:=m14;
  Result[13]:=m24;
  Result[14]:=m34;
  Result[15]:=m44;
end;

{ TMatrix }
procedure TMatrix.Identity;
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  m11:=1.0;
  m22:=1.0;
  m33:=1.0;
  m44:=1.0;
end;

procedure TMatrix.XRotation(Angle: Double);
begin
  Rotation(1, 0, 0, Angle);
end;

procedure TMatrix.YRotation(Angle: Double);
begin
  Rotation(0, 1, 0, Angle);
end;

procedure TMatrix.ZRotation(Angle: Double);
begin
  Rotation(0, 0, 1, Angle);
end;

procedure TMatrix.Rotation(x, y, z, Angle: Double);
var
  t, s, c: Double;
begin
  s:=sin(Angle);
  c:=cos(Angle);
  t:=1-c;
  m11:=t*x*x + c;
  m12:=t*x*y - z*s;
  m13:=t*x*z + y*s;
  m14:=0;
  m21:=t*x*y + z*s;
  m22:=t*y*y + c;
  m23:=t*y*z - x*s;
  m24:=0;
  m31:=t*x*z - y*s;
  m32:=t*y*z + x*s;
  m33:=t*z*z + c;
  m34:=0;
  m41:=0;
  m42:=0;
  m43:=0;
  m44:=1;
end;

procedure TMatrix.Rotation(const n: TVector; Angle: Double);
begin
  Rotation(n.x, n.y, n.z, Angle);
end;

procedure TMatrix.XYZRotation(xa, ya, za: Double);
var
  Tmp: TMatrix;
begin
  Identity;
  Tmp.XRotation(xa);
  Multiply(Tmp);
  Tmp.YRotation(ya);
  Multiply(Tmp);
  Tmp.ZRotation(za);
  Multiply(Tmp);
end;

procedure TMatrix.Translation(x, y, z: Double);
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  m11:=1.0;
  m22:=1.0;
  m33:=1.0;
  m44:=1.0;
  m14:=x;
  m24:=y;
  m34:=z;
end;

procedure TMatrix.Scaling(s: Double);
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  m11:=s;
  m22:=s;
  m33:=s;
  m44:=1.0;
end;

procedure TMatrix.Scaling(x, y, z: Double);
begin
  FillChar(Self, SizeOf(TMatrix), 0);
  m11:=x;
  m22:=y;
  m33:=z;
  m44:=1.0;
end;

procedure TMatrix.LookAt(const Position, Target, Up: TVector);
var
  F, S, U: TVector;
  Tmp: TMatrix;
begin
  F:=Target.Subbed(Position).Normalized;
  S:=F.Crossed(Up).Normalized;
  U:=S.Crossed(F);
  m11:=S.x;
  m12:=S.y;
  m13:=S.z;
  m14:=0;
  m21:=U.x;
  m22:=U.y;
  m23:=U.z;
  m24:=0;
  m31:=-F.x;
  m32:=-F.y;
  m33:=-F.z;
  m34:=0;
  m41:=0;
  m42:=0;
  m43:=0;
  m44:=1.0;
  Tmp.Translation(-Position.x, -Position.y, -Position.z);
  Multiply(Tmp);
end;

procedure TMatrix.Direction(const Dir: TVector);
var
  U, XNormal, YNormal: TVector;
begin
  if Abs(Dir.y) > 0.8 then begin
    U:=Vector(0, 0, -1);
    XNormal:=U.Crossed(Dir);
    YNormal:=Dir.Crossed(XNormal);
  end else begin
    U:=Vector(0, 1, 0);
    XNormal:=U.Crossed(Dir);
    YNormal:=Dir.Crossed(XNormal);
  end;
  m11:=XNormal.x; m21:=XNormal.y; m31:=XNormal.z; m41:=0;
  m12:=YNormal.x; m22:=YNormal.y; m32:=YNormal.z; m42:=0;
  m13:=Dir.x; m23:=Dir.y; m33:=Dir.z; m43:=0;
  m14:=0; m24:=0; m34:=0; m44:=1;
end;

procedure TMatrix.Perspective(Fov, Aspect, ZNear, ZFar: Double);
var
  F: Double;
begin
  F:=1.0/Tan(Fov/2.0);
  Identity;
  m44:=0;
  m11:=F/Aspect;
  m22:=F;
  m33:=(ZFar + ZNear)/(ZNear - ZFar);
  m34:=(2.0*ZFar*ZNear)/(ZNear - ZFar);
  m43:=-1.0;
end;

procedure TMatrix.Orthographic(Left, Right, Top, Bottom, ZNear, ZFar: Double);
begin
  Identity;
  m11:=2/(Right - Left);
  m22:=2/(Top - Bottom);
  m33:=-2/(ZFar - ZNear);
  m14:=-(Right + Left)/(Right - Left);
  m24:=-(Top + Bottom)/(Top - Bottom);
  m34:=-(ZFar + ZNear)/(ZFar - ZNear);
end;

procedure TMatrix.Transform(var v: TVector);
var
  nx, ny: Double;
begin
  nx:=v.x*m11 + v.y*m12 + v.z*m13 + m14;
  ny:=v.x*m21 + v.y*m22 + v.z*m23 + m24;
  v.z:=v.x*m31 + v.y*m32 + v.z*m33 + m34;
  v.x:=nx;
  v.y:=ny;
end;

procedure TMatrix.Transformed(const v: TVector; out r: TVector);
begin
  r.x:=v.x;
  r.y:=v.y;
  r.z:=v.z;
  Transform(r);
end;

procedure TMatrix.TransformNormal(var v: TVector);
var
  nx, ny: Double;
begin
  nx:=v.x*m11 + v.y*m12 + v.z*m13;
  ny:=v.x*m21 + v.y*m22 + v.z*m23;
  v.z:=v.x*m31 + v.y*m32 + v.z*m33;
  v.x:=nx;
  v.y:=ny;
end;

procedure TMatrix.TransformedNormal(const v: TVector; out r: TVector);
begin
  r.x:=v.x;
  r.y:=v.y;
  r.z:=v.z;
  Transform(r);
end;

function TMatrix.Transformed(const v: TVector): TVector;
begin
  Result.x:=v.x*m11 + v.y*m12 + v.z*m13 + m14;
  Result.y:=v.x*m21 + v.y*m22 + v.z*m23 + m24;
  Result.z:=v.x*m31 + v.y*m32 + v.z*m33 + m34;
end;

function TMatrix.Transformed(const p: TPlane): TPlane;
begin
  Result:=p;
  Transform(Result);
end;

function TMatrix.TransformedNormal(const v: TVector): TVector;
begin
  Result.x:=v.x*m11 + v.y*m12 + v.z*m13;
  Result.y:=v.x*m21 + v.y*m22 + v.z*m23;
  Result.z:=v.x*m31 + v.y*m32 + v.z*m33;
end;

procedure TMatrix.TransformProj(var v: TVector);
var
  nx, ny, nz, nw: Double;
begin
  nx:=v.x*m11 + v.y*m12 + v.z*m13 + m14;
  ny:=v.x*m21 + v.y*m22 + v.z*m23 + m24;
  nz:=v.x*m31 + v.y*m32 + v.z*m33 + m34;
  nw:=v.x*m41 + v.y*m42 + v.z*m43 + m44;
  if nw=0 then begin
    v.x:=0;
    v.y:=0;
    v.z:=0;
  end else begin
    v.x:=nx/nw;
    v.y:=ny/nw;
    v.z:=nz/nw;
  end;
end;

procedure TMatrix.TransformedProj(const v: TVector; out r: TVector);
begin
  r.x:=v.x;
  r.y:=v.y;
  r.z:=v.z;
  TransformProj(r);
end;

function TMatrix.TransformedProj(const v: TVector): TVector;
var
  nx, ny, nz, nw: Double;
begin
  nx:=v.x*m11 + v.y*m12 + v.z*m13 + m14;
  ny:=v.x*m21 + v.y*m22 + v.z*m23 + m24;
  nz:=v.x*m31 + v.y*m32 + v.z*m33 + m34;
  nw:=v.x*m41 + v.y*m42 + v.z*m43 + m44;
  if nw=0 then begin
    Result.x:=0;
    Result.y:=0;
    Result.z:=0;
  end else begin
    Result.x:=nx/nw;
    Result.y:=ny/nw;
    Result.z:=nz/nw;
  end;
end;

procedure TMatrix.Transform(var p: TPlane);
var
  pop: TVector;
begin
  pop.x:=p.n.x*p.d;
  pop.y:=p.n.y*p.d;
  pop.z:=p.n.z*p.d;

  Transform(pop);
  Transform(p.n);
  p.n.x -= m14;
  p.n.y -= m24;
  p.n.z -= m34;
  p.n.Normalize;
  p.d:=pop.Dot(p.n);
end;

procedure TMatrix.Multiply(const Src: TMatrix);
var
  Tmp: TMatrix;
begin
  Tmp.m11:=m11*Src.m11 + m12*Src.m21 + m13*Src.m31 + m14*Src.m41;
  Tmp.m12:=m11*Src.m12 + m12*Src.m22 + m13*Src.m32 + m14*Src.m42;
  Tmp.m13:=m11*Src.m13 + m12*Src.m23 + m13*Src.m33 + m14*Src.m43;
  Tmp.m14:=m11*Src.m14 + m12*Src.m24 + m13*Src.m34 + m14*Src.m44;
  Tmp.m21:=m21*Src.m11 + m22*Src.m21 + m23*Src.m31 + m24*Src.m41;
  Tmp.m22:=m21*Src.m12 + m22*Src.m22 + m23*Src.m32 + m24*Src.m42;
  Tmp.m23:=m21*Src.m13 + m22*Src.m23 + m23*Src.m33 + m24*Src.m43;
  Tmp.m24:=m21*Src.m14 + m22*Src.m24 + m23*Src.m34 + m24*Src.m44;
  Tmp.m31:=m31*Src.m11 + m32*Src.m21 + m33*Src.m31 + m34*Src.m41;
  Tmp.m32:=m31*Src.m12 + m32*Src.m22 + m33*Src.m32 + m34*Src.m42;
  Tmp.m33:=m31*Src.m13 + m32*Src.m23 + m33*Src.m33 + m34*Src.m43;
  Tmp.m34:=m31*Src.m14 + m32*Src.m24 + m33*Src.m34 + m34*Src.m44;
  Tmp.m41:=m41*Src.m11 + m42*Src.m21 + m43*Src.m31 + m44*Src.m41;
  Tmp.m42:=m41*Src.m12 + m42*Src.m22 + m43*Src.m32 + m44*Src.m42;
  Tmp.m43:=m41*Src.m13 + m42*Src.m23 + m43*Src.m33 + m44*Src.m43;
  Tmp.m44:=m41*Src.m14 + m42*Src.m24 + m43*Src.m34 + m44*Src.m44;
  Self:=Tmp;
end;

function TMatrix.Multiplied(const Src: TMatrix): TMatrix;
begin
  Result.m11:=m11*Src.m11 + m12*Src.m21 + m13*Src.m31 + m14*Src.m41;
  Result.m12:=m11*Src.m12 + m12*Src.m22 + m13*Src.m32 + m14*Src.m42;
  Result.m13:=m11*Src.m13 + m12*Src.m23 + m13*Src.m33 + m14*Src.m43;
  Result.m14:=m11*Src.m14 + m12*Src.m24 + m13*Src.m34 + m14*Src.m44;
  Result.m21:=m21*Src.m11 + m22*Src.m21 + m23*Src.m31 + m24*Src.m41;
  Result.m22:=m21*Src.m12 + m22*Src.m22 + m23*Src.m32 + m24*Src.m42;
  Result.m23:=m21*Src.m13 + m22*Src.m23 + m23*Src.m33 + m24*Src.m43;
  Result.m24:=m21*Src.m14 + m22*Src.m24 + m23*Src.m34 + m24*Src.m44;
  Result.m31:=m31*Src.m11 + m32*Src.m21 + m33*Src.m31 + m34*Src.m41;
  Result.m32:=m31*Src.m12 + m32*Src.m22 + m33*Src.m32 + m34*Src.m42;
  Result.m33:=m31*Src.m13 + m32*Src.m23 + m33*Src.m33 + m34*Src.m43;
  Result.m34:=m31*Src.m14 + m32*Src.m24 + m33*Src.m34 + m34*Src.m44;
  Result.m41:=m41*Src.m11 + m42*Src.m21 + m43*Src.m31 + m44*Src.m41;
  Result.m42:=m41*Src.m12 + m42*Src.m22 + m43*Src.m32 + m44*Src.m42;
  Result.m43:=m41*Src.m13 + m42*Src.m23 + m43*Src.m33 + m44*Src.m43;
  Result.m44:=m41*Src.m14 + m42*Src.m24 + m43*Src.m34 + m44*Src.m44;
end;

procedure TMatrix.SwapMultiply(const Src: TMatrix);
var
  Tmp: TMatrix;
begin
  Tmp.m11:=Src.m11*m11 + Src.m12*m21 + Src.m13*m31 + Src.m14*m41;
  Tmp.m12:=Src.m11*m12 + Src.m12*m22 + Src.m13*m32 + Src.m14*m42;
  Tmp.m13:=Src.m11*m13 + Src.m12*m23 + Src.m13*m33 + Src.m14*m43;
  Tmp.m14:=Src.m11*m14 + Src.m12*m24 + Src.m13*m34 + Src.m14*m44;
  Tmp.m21:=Src.m21*m11 + Src.m22*m21 + Src.m23*m31 + Src.m24*m41;
  Tmp.m22:=Src.m21*m12 + Src.m22*m22 + Src.m23*m32 + Src.m24*m42;
  Tmp.m23:=Src.m21*m13 + Src.m22*m23 + Src.m23*m33 + Src.m24*m43;
  Tmp.m24:=Src.m21*m14 + Src.m22*m24 + Src.m23*m34 + Src.m24*m44;
  Tmp.m31:=Src.m31*m11 + Src.m32*m21 + Src.m33*m31 + Src.m34*m41;
  Tmp.m32:=Src.m31*m12 + Src.m32*m22 + Src.m33*m32 + Src.m34*m42;
  Tmp.m33:=Src.m31*m13 + Src.m32*m23 + Src.m33*m33 + Src.m34*m43;
  Tmp.m34:=Src.m31*m14 + Src.m32*m24 + Src.m33*m34 + Src.m34*m44;
  Tmp.m41:=Src.m41*m11 + Src.m42*m21 + Src.m43*m31 + Src.m44*m41;
  Tmp.m42:=Src.m41*m12 + Src.m42*m22 + Src.m43*m32 + Src.m44*m42;
  Tmp.m43:=Src.m41*m13 + Src.m42*m23 + Src.m43*m33 + Src.m44*m43;
  Tmp.m44:=Src.m41*m14 + Src.m42*m24 + Src.m43*m34 + Src.m44*m44;
  Self:=Tmp;
end;

function TMatrix.SwapMultiplied(const Src: TMatrix): TMatrix;
begin
  Result:=Src;
  Result.Multiply(Self);
end;

function TMatrix.Determinant: Double;
begin
  Result:=m14*m23*m32*m41 - m13*m24*m32*m41 -
          m14*m22*m33*m41 + m12*m24*m33*m41 +
          m13*m22*m32*m41 - m12*m23*m34*m41 -
          m14*m23*m31*m42 + m13*m24*m31*m42 +
          m14*m21*m33*m42 - m11*m24*m33*m42 -
          m13*m21*m34*m42 + m11*m23*m34*m42 +
          m14*m22*m31*m43 - m12*m24*m31*m43 -
          m14*m21*m32*m43 + m11*m24*m32*m43 +
          m12*m21*m34*m43 - m11*m22*m34*m43 -
          m13*m22*m31*m44 + m12*m23*m31*m44 +
          m13*m21*m32*m44 - m11*m23*m32*m44 -
          m12*m21*m33*m44 + m11*m22*m33*m44;
end;

procedure TMatrix.Invert;
var
  R: TMatrix;
  Det: Double;
begin
  Det:=Determinant;
  if Det <> 0.0 then begin
    R.m11:=m23*m34*m42 - m24*m33*m42 + m24*m32*m43 - m22*m34*m43 - m23*m32*m44 + m22*m33*m44;
    R.m12:=m14*m33*m42 - m13*m34*m42 - m14*m32*m43 + m12*m34*m43 + m13*m32*m44 - m12*m33*m44;
    R.m13:=m13*m24*m42 - m14*m23*m42 + m14*m22*m43 - m12*m24*m43 - m13*m22*m44 + m12*m23*m44;
    R.m14:=m14*m23*m32 - m13*m24*m32 - m14*m22*m33 + m12*m24*m33 + m13*m22*m34 - m12*m23*m34;
    R.m21:=m24*m33*m41 - m23*m34*m41 - m24*m31*m43 + m21*m34*m43 + m23*m31*m44 - m21*m33*m44;
    R.m22:=m13*m34*m41 - m14*m33*m41 + m14*m31*m43 - m11*m34*m43 - m13*m31*m44 + m11*m33*m44;
    R.m23:=m14*m23*m41 - m13*m24*m41 - m14*m21*m43 + m11*m24*m43 + m13*m21*m44 - m11*m23*m44;
    R.m24:=m13*m24*m31 - m14*m23*m31 + m14*m21*m33 - m11*m24*m33 - m13*m21*m34 + m11*m23*m34;
    R.m31:=m22*m34*m41 - m24*m32*m41 + m24*m31*m42 - m21*m34*m42 - m22*m31*m44 + m21*m32*m44;
    R.m32:=m14*m32*m41 - m12*m34*m41 - m14*m31*m42 + m11*m34*m42 + m12*m31*m44 - m11*m32*m44;
    R.m33:=m12*m24*m41 - m14*m22*m41 + m14*m21*m42 - m11*m24*m42 - m12*m21*m44 + m11*m22*m44;
    R.m34:=m14*m22*m31 - m12*m24*m31 - m14*m21*m32 + m11*m24*m32 + m12*m21*m34 - m11*m22*m34;
    R.m41:=m23*m32*m41 - m22*m33*m41 - m23*m31*m42 + m21*m33*m42 + m22*m31*m43 - m21*m32*m43;
    R.m42:=m12*m33*m41 - m13*m32*m41 + m13*m31*m42 - m11*m33*m42 - m12*m31*m43 + m11*m32*m43;
    R.m43:=m13*m22*m41 - m12*m23*m41 - m13*m21*m42 + m11*m23*m42 + m12*m21*m43 - m11*m22*m43;
    R.m44:=m12*m23*m31 - m13*m22*m31 + m13*m21*m32 - m11*m23*m32 - m12*m21*m33 + m11*m22*m33;
    m11:=R.m11/Det; m12:=R.m12/Det; m13:=R.m13/Det; m14:=R.m14/Det;
    m21:=R.m21/Det; m22:=R.m22/Det; m23:=R.m23/Det; m24:=R.m24/Det;
    m31:=R.m31/Det; m32:=R.m32/Det; m33:=R.m33/Det; m34:=R.m34/Det;
    m41:=R.m41/Det; m42:=R.m42/Det; m43:=R.m43/Det; m44:=R.m44/Det;
  end;
end;

function TMatrix.Inverted: TMatrix;
var
  Det: Double;
begin
  Det:=Determinant;
  if Det <> 0.0 then begin
    Result.m11:=(m23*m34*m42 - m24*m33*m42 + m24*m32*m43 - m22*m34*m43 - m23*m32*m44 + m22*m33*m44)/Det;
    Result.m12:=(m14*m33*m42 - m13*m34*m42 - m14*m32*m43 + m12*m34*m43 + m13*m32*m44 - m12*m33*m44)/Det;
    Result.m13:=(m13*m24*m42 - m14*m23*m42 + m14*m22*m43 - m12*m24*m43 - m13*m22*m44 + m12*m23*m44)/Det;
    Result.m14:=(m14*m23*m32 - m13*m24*m32 - m14*m22*m33 + m12*m24*m33 + m13*m22*m34 - m12*m23*m34)/Det;
    Result.m21:=(m24*m33*m41 - m23*m34*m41 - m24*m31*m43 + m21*m34*m43 + m23*m31*m44 - m21*m33*m44)/Det;
    Result.m22:=(m13*m34*m41 - m14*m33*m41 + m14*m31*m43 - m11*m34*m43 - m13*m31*m44 + m11*m33*m44)/Det;
    Result.m23:=(m14*m23*m41 - m13*m24*m41 - m14*m21*m43 + m11*m24*m43 + m13*m21*m44 - m11*m23*m44)/Det;
    Result.m24:=(m13*m24*m31 - m14*m23*m31 + m14*m21*m33 - m11*m24*m33 - m13*m21*m34 + m11*m23*m34)/Det;
    Result.m31:=(m22*m34*m41 - m24*m32*m41 + m24*m31*m42 - m21*m34*m42 - m22*m31*m44 + m21*m32*m44)/Det;
    Result.m32:=(m14*m32*m41 - m12*m34*m41 - m14*m31*m42 + m11*m34*m42 + m12*m31*m44 - m11*m32*m44)/Det;
    Result.m33:=(m12*m24*m41 - m14*m22*m41 + m14*m21*m42 - m11*m24*m42 - m12*m21*m44 + m11*m22*m44)/Det;
    Result.m34:=(m14*m22*m31 - m12*m24*m31 - m14*m21*m32 + m11*m24*m32 + m12*m21*m34 - m11*m22*m34)/Det;
    Result.m41:=(m23*m32*m41 - m22*m33*m41 - m23*m31*m42 + m21*m33*m42 + m22*m31*m43 - m21*m32*m43)/Det;
    Result.m42:=(m12*m33*m41 - m13*m32*m41 + m13*m31*m42 - m11*m33*m42 - m12*m31*m43 + m11*m32*m43)/Det;
    Result.m43:=(m13*m22*m41 - m12*m23*m41 - m13*m21*m42 + m11*m23*m42 + m12*m21*m43 - m11*m22*m43)/Det;
    Result.m44:=(m12*m23*m31 - m13*m22*m31 + m13*m21*m32 - m11*m23*m32 - m12*m21*m33 + m11*m22*m33)/Det;
  end else Result:=Self;
end;

procedure TMatrix.NormalizeAxes;
var
  Tmp: TVector;
begin
  Tmp:=XAxis.Normalized;
  m11:=Tmp.x;
  m21:=Tmp.y;
  m31:=Tmp.z;
  Tmp:=YAxis.Normalized;
  m12:=Tmp.x;
  m22:=Tmp.y;
  m32:=Tmp.z;
  Tmp:=ZAxis.Normalized;
  m13:=Tmp.x;
  m23:=Tmp.y;
  m33:=Tmp.z;
end;

function TMatrix.XAxis: TVector;
begin
  Result.x:=m11;
  Result.y:=m21;
  Result.z:=m31;
end;

function TMatrix.YAxis: TVector;
begin
  Result.x:=m12;
  Result.y:=m22;
  Result.z:=m32;
end;

function TMatrix.ZAxis: TVector;
begin
  Result.x:=m13;
  Result.y:=m23;
  Result.z:=m33;
end;

function TMatrix.Translation: TVector;
begin
  Result.x:=m14;
  Result.y:=m24;
  Result.z:=m34;
end;

function TMatrix.Position: TVector;
begin
  Result.x:=-m14;
  Result.y:=-m24;
  Result.z:=-m34;
end;

procedure TMatrix.SetXAxis(const v: TVector; w: Double);
begin
  m11:=v.x;
  m21:=v.y;
  m31:=v.z;
  m41:=w;
end;

procedure TMatrix.SetXAxis(x, y, z: Double; w: Double);
begin
  m11:=x;
  m21:=y;
  m31:=z;
  m41:=w;
end;

procedure TMatrix.SetYAxis(const v: TVector; w: Double);
begin
  m12:=v.x;
  m22:=v.y;
  m32:=v.z;
  m42:=w;
end;

procedure TMatrix.SetYAxis(x, y, z: Double; w: Double);
begin
  m12:=x;
  m22:=y;
  m32:=z;
  m42:=w;
end;

procedure TMatrix.SetZAxis(const v: TVector; w: Double);
begin
  m13:=v.x;
  m23:=v.y;
  m33:=v.z;
  m43:=w;
end;

procedure TMatrix.SetZAxis(x, y, z: Double; w: Double);
begin
  m13:=x;
  m23:=y;
  m33:=z;
  m43:=w;
end;

procedure TMatrix.SetTranslation(const v: TVector; w: Double);
begin
  m14:=v.x;
  m24:=v.y;
  m34:=v.z;
  m44:=w;
end;

procedure TMatrix.SetTranslation(x, y, z: Double; w: Double);
begin
  m14:=x;
  m24:=y;
  m34:=z;
  m44:=w;
end;

procedure TMatrix.SetAxes(x, y, z, w: TVector; xw: Double; yw: Double;
  zw: Double; ww: Double);
begin
  SetXAxis(x, xw);
  SetYAxis(y, yw);
  SetZAxis(z, zw);
  SetTranslation(w, ww);
end;

procedure TMatrix.SetAxes(x, y, z: TVector; xw: Double; yw: Double; zw: Double);
begin
  SetXAxis(x, xw);
  SetYAxis(y, yw);
  SetZAxis(z, zw);
end;

procedure TMatrix.RemoveTranslation;
begin
  m14:=0.0;
  m24:=0.0;
  m34:=0.0;
  m44:=1.0;
end;

{ TFace }
procedure TFace.Assign(const AFace: TFace);
var
  i: Integer;
begin
  SetLength(v, Length(AFace.v));
  for i:=0 to Length(v) - 1 do v[i]:=AFace.v[i];
  SetLength(n, Length(AFace.n));
  for i:=0 to Length(n) - 1 do n[i]:=AFace.n[i];
  tx:=AFace.tx;
  ty:=AFace.ty;
  Lightmap:=AFace.Lightmap;
  SetLength(ls, Length(AFace.ls));
  SetLength(lt, Length(AFace.lt));
  for i:=0 to Length(ls) - 1 do begin
    ls[i]:=AFace.ls[i];
    lt[i]:=AFace.lt[i];
  end;
  BrushFace:=AFace.BrushFace;
end;

procedure TFace.Clear;
begin
  SetLength(v, 0);
  SetLength(c, 0);
  SetLength(n, 0);
  SetLength(ls, 0);
  SetLength(lt, 0);
  SetLength(fs, 0);
  SetLength(ft, 0);
end;

procedure TFace.AddVertex(const P: TVector);
begin
  SetLength(v, Length(v) + 1);
  v[High(v)]:=P;
end;

procedure TFace.AddVertex(const P, Nor: TVector);
begin
  SetLength(v, Length(v) + 1);
  v[High(v)]:=P;
  SetLength(n, Length(v));
  n[High(n)]:=Nor;
end;

procedure TFace.Invert;
var
  nv: array of TVector;
  i: Integer;
begin
  if Length(v)=0 then Exit;
  SetLength(nv, Length(v));
  for i:=0 to Length(v) - 1 do nv[i]:=v[Length(v) - i - 1];
  for i:=0 to Length(v) - 1 do v[i]:=nv[i];
  for i:=0 to Length(n) - 1 do n[i].Invert;
end;

function TFace.CheckValidity: Boolean;
var
  i: Integer;
begin
  Result:=False;
  if Length(v) < 3 then Exit;
  for i:=0 to Length(v) - 1 do with v[i] do begin
    if IsNan(x) or IsNan(y) or IsNan(z) or IsInfinite(x) or IsInfinite(y) or IsInfinite(z) then Exit;
  end;
  Result:=True;
end;

procedure TFace.MergeZeroLengthEdges;
var
  NewV: array of TVector;
  i, ni, nvc: Integer;
begin
  SetLength(NewV, 0);
  nvc:=0;
  for i:=0 to Length(v) - 1 do begin
    if IsNan(v[i].x) or IsNan(v[i].y) or IsNan(v[i].z) then Continue;
    ni:=(i + 1) mod Length(v);
    if not Zero(DistanceSq(v[i], v[ni])) then begin
      SetLength(NewV, nvc + 1);
      NewV[nvc]:=v[i];
      Inc(nvc);
    end;
  end;
  SetLength(v, nvc);
  for i:=0 to nvc - 1 do v[i]:=NewV[i];
end;

procedure TFace.SnapToInteger;
var
  i: Integer;
begin
  for i:=0 to Length(v) - 1 do v[i].SnapToInteger;
end;

procedure TFace.Clip(p: TPlane; InFront: Boolean);
var
  i, ff, Idx, Prev: Integer;
  nv: array of TVector;
  HasBack: Boolean;
  IP: TVector;

procedure Add(var p: TVector);
var
  i: Integer;
begin
  for i:=0 to Length(nv)-1 do
    if Equal(p.x, nv[i].x) and Equal(p.y, nv[i].y) and Equal(p.z, nv[i].z) then Exit;
  SetLength(nv, Length(nv) + 1);
  nv[Length(nv) - 1]:=p;
end;

begin
  { invert plane if InFront=False }
  if InFront=False then p.Invert;

  { find first vertex in front (=InFront) of the plane }
  ff:=-1;
  HasBack:=False;
  for i:=0 to Length(v)-1 do if p.Front(v[i]) then begin
    ff:=i;
    Break;
  end else HasBack:=True;
  if ff=-1 then begin // no vertices in front of the plane, discard the face
    SetLength(v, 0);
    Exit;
  end;
  if (ff=0) and (not HasBack) then begin // check for backfaces if ff was 0
    for i:=0 to Length(v)-1 do if not p.Front(v[i])then begin
      HasBack:=True;
      Break;
    end;
  end;
  if not HasBack then Exit; // no vertices behind the plane, ignore clipping

  Idx:=(ff + 1) mod Length(v);
  Prev:=ff;
  Add(v[ff]);
  while True do begin
    if p.Intersect(v[Prev], v[Idx], IP) then Add(IP);
    if Idx=ff then Break;
    if p.Front(v[Idx]) then Add(v[Idx]);

    Prev:=Idx;
    Idx:=(Idx + 1) mod Length(v);
  end;

  SetLength(v, Length(nv));
  for i:=0 to Length(v) - 1 do v[i]:=nv[i];
end;

procedure TFace.Project(const p: TPlane);
var
  I: Integer;
begin
  for I:=0 to High(v) do v[I]:=p.Projected(v[I]);
  MergeZeroLengthEdges;
end;

function TFace.SideOfPlane(const p: TPlane): TPlaneSide;
var
  i: Integer;
begin
  Result:=psFront;
  for i:=0 to Length(v) - 1 do begin
    if p.Front(v[i]) then begin
      if i=0 then Result:=psFront else
        if Result=psBack then begin
          Result:=psBoth;
          Break;
        end else Result:=psFront;
    end else begin
      if i=0 then Result:=psBack else
        if Result=psFront then begin
          Result:=psBoth;
          Break;
        end else Result:=psBack;
    end;
  end;
end;

{$IFDEF RTWORLD}
function TFace.PointInFace(const p: TVector): Boolean;
var
  Ray: TRay;
  IP: TVector;
begin
  if Zero(TBrushFace(BrushFace).Plane^.SignDist(p)) then begin
    Ray.o:=p;
    Ray.o.Sub(TBrushFace(BrushFace).Plane^.n);
    Ray.d:=TBrushFace(BrushFace).Plane^.n;
    Result:=Ray.FaceHit(Self, ip);
  end else Result:=False;
end;
{$ENDIF}

function TFace.PointInFace(const p: TVector; const Plane: TPlane): Boolean;
var
  Ray: TRay;
  IP: TVector;
begin
  if Zero(Plane.SignDist(p)) then begin
    Ray.o:=p;
    Ray.o.Sub(Plane.n);
    Ray.d:=Plane.n;
    Result:=Ray.FaceHit(Self, ip);
  end else Result:=False;
end;

function TFace.ClosestEdgePoint(const p: TVector; const Plane: TPlane): TVector;
var
  I, I2: Integer;
  EdgePlane: TPlane;
  ClosestEP, EP: TVector;
  ClosestDistance, ED: Double;
  Found: Boolean;
begin
  Result:=Plane.Projected(p);
  ClosestEP:=Result;
  ClosestDistance:=MaxInt;
  Found:=False;
  for I:=0 to High(v) do begin
    I2:=(I + 1) mod Length(v);
    EdgePlane.FromThreePoints(v[I], v[I2], v[I].Added(Plane.n));
    EP:=EdgePlane.Projected(Result);
    ED:=DistanceSq(EP, Result);
    if (ED < ClosestDistance) or not Found then begin
      ClosestDistance:=ED;
      ClosestEP:=EP;
      Found:=True;
    end;
  end;
  Result:=ClosestEP;
end;

function TFace.ToConvexFaces: TPFaceArray;
var
  Index, I, J: Integer;
  ThisFace, NewFace: TFace;
  Plane, EdgePlane: TPlane;
  A, B: TVector;
  UsedPlanes: array of TPlane;

  function CanUsePlane(const P: TPlane): Boolean;
  var
    UP: TPlane;
  begin
    for UP in UsedPlanes do if P.CoplaneStyle(UP)=csSame then Exit(False);
    Result:=True;
  end;

begin
  SetLength(Result, 1);
  SetLength(UsedPlanes, 0);
  Result[0]:=New(PFace);
  Result[0]^.Assign(Self);
  Index:=0;
  Plane.FromThreePoints(v[0], v[1], v[2]);
  while Index < Length(Result) do begin
    Result[Index]^.Project(Plane);
    ThisFace.Assign(Result[Index]^);
    for I:=0 to High(ThisFace.v) do begin
      A:=ThisFace.v[(I + 1) mod Length(ThisFace.v)];
      B:=ThisFace.v[I];
      B.Sub(Plane.n);
      EdgePlane.FromThreePoints(ThisFace.v[I], A, B);
      if not CanUsePlane(EdgePlane) then Continue;
      NewFace.Assign(Result[Index]^);
      Result[Index]^.Clip(EdgePlane, True);
      NewFace.Clip(EdgePlane, False);
      Result[Index]^.MergeZeroLengthEdges;
      NewFace.MergeZeroLengthEdges;
      if NewFace.CheckValidity then begin
        SetLength(Result, Length(Result) + 1);
        Result[High(Result)]:=New(PFace);
        Result[High(Result)]^.Assign(NewFace);
        SetLength(UsedPlanes, Length(UsedPlanes) + 1);
        UsedPlanes[High(UsedPlanes)]:=EdgePlane;
      end;
      if not Result[Index]^.CheckValidity then begin
        Dispose(Result[Index]);
        for J:=Index to High(Result) - 1 do Result[J]:=Result[J + 1];
        SetLength(Result, Length(Result) - 1);
        SetLength(UsedPlanes, Length(UsedPlanes) + 1);
        UsedPlanes[High(UsedPlanes)]:=EdgePlane;
        Dec(Index);
        Break;
      end;
    end;

    Inc(Index);
  end;
end;

function TFace.IsConvex: Boolean;
var
  EdgePlanes: array of TPlane;
  ProjectedVertices: array of TVector;
  A, B: TVector;
  I, J: Integer;
  P: TPlane;
begin
  if Length(v) < 3 then Exit(False);
  P:=Plane(v[0], CalcNormal);
  SetLength(EdgePlanes, Length(v));
  SetLength(ProjectedVertices, Length(v));
  for I:=0 to High(EdgePlanes) do begin
    A:=P.Projected(v[i]);
    B:=P.Projected(v[(I + 1) mod Length(EdgePlanes)]);
    EdgePlanes[I]:=Plane(A, NormalBetween(B.Subbed(A), P.n));
    ProjectedVertices[I]:=A;
  end;
  for I:=0 to High(ProjectedVertices) do begin
    for J:=0 to High(EdgePlanes) do
      if I <> J then
        if EdgePlanes[J].SignDist(ProjectedVertices[I]) > EPSILON then
          Exit(False);
  end;
  Result:=True;
end;

function TFace.NormalAt(const p: TVector): TVector;
var
  pp, Nor, PlnNor, Closest: TVector;
  ClosDist: Double;
  Pln: TPlane;
  I: Integer;

  function TriNormalAt(const sp, a, b, c, na, nb, nc: TVector): Boolean;
  var
    Ray: TRay;
    Tmp1, Tmp2, IP: TVector;
    Int, da, db, dc: Double;
  begin
    Tmp1:=sp; Tmp1.Sub(PlnNor);
    Tmp2:=sp; Tmp2.Add(PlnNor);
    Ray.FromSegment(Tmp1, Tmp2);

    if Ray.TriangleHit(a, b, c, IP) then begin
      da:=DistanceSq(a, sp);
      db:=DistanceSq(b, sp);
      dc:=DistanceSq(c, sp);
      if (da > db) and (da > dc) then begin
        Tmp2:=b;
        Tmp2.Add(PlnNor);
        Pln.FromThreePoints(b, Tmp2, c);
        Tmp1:=Pln.Projected(IP);
        Int:=Interpolation(b, c, Tmp1);
        Tmp2:=Interpolate(nb, nc, Int);
        Int:=Interpolation(Tmp1, a, IP);
        Nor:=Interpolate(Tmp2, na, Int);
      end else if (db > da) and (db > dc) then begin
        Tmp2:=a;
        Tmp2.Add(PlnNor);
        Pln.FromThreePoints(a, Tmp2, c);
        Tmp1:=Pln.Projected(IP);
        Int:=Interpolation(a, c, Tmp1);
        Tmp2:=Interpolate(na, nc, Int);
        Int:=Interpolation(Tmp1, b, IP);
        Nor:=Interpolate(Tmp2, nb, Int);
      end else if (dc > da) and (dc > db) then begin
        Tmp2:=a;
        Tmp2.Add(PlnNor);
        Pln.FromThreePoints(a, Tmp2, b);
        Tmp1:=Pln.Projected(IP);
        Int:=Interpolation(a, b, Tmp1);
        Tmp2:=Interpolate(na, nb, Int);
        Int:=Interpolation(Tmp1, c, IP);
        Nor:=Interpolate(Tmp2, nc, Int);
      end;
      Nor.Normalize;
      Exit(True);
    end;
    Result:=False;
  end;

begin
  Pln.FromThreePoints(v[0], v[1], v[2]);
  pp:=Pln.Projected(p);
  PlnNor:=Pln.n;
  Nor:=Pln.n;

  for I:=2 to High(v) do begin
    if TriNormalAt(p, v[0], v[I - 1], v[I], n[0], n[I - 1], n[I]) then Exit(Nor);
  end;

  ClosDist:=MaxFloat;
  Closest.Zero;
  for I:=0 to High(v) do begin
    Nor:=v[I];
    Nor.Add(PlnNor);
    Pln.FromThreePoints(v[I], Nor, v[(I + 1) mod Length(v)]);
    Nor:=Pln.Projected(pp);
    if DistanceSq(Nor, pp) < ClosDist then begin
      ClosDist:=DistanceSq(Nor, pp);
      Closest:=Interpolate(n[I], n[(I + 1) mod Length(v)],
        Interpolation(v[I], v[(I + 1) mod Length(v)], Nor));
    end;
  end;

  Result:=Closest;
end;

function TFace.CalcCenter: TVector;
var
  I: Integer;
begin
  Result.Zero;
  if Length(v) > 0 then begin
    for I:=0 to High(v) do Result.Add(v[I]);
    Result.Scale(1/Length(v));
  end;
end;

function TFace.CalcNormal: TVector;
var
  I: Integer;
begin
  Result.Zero;
  for I:=2 to High(v) do
    Result.Add(TriangleNormal(v[0], v[I - 1], v[I]));
  Result.Normalize;
end;

procedure TFace.InitializeNormalsTo(const nv: TVector);
var
  I: Integer;
begin
  SetLength(n, Length(v));
  for I:=0 to High(n) do n[I]:=nv;
end;

function TAABox.GetExtent: TVector;
begin
  Result.x:=b.x - a.x;
  Result.y:=b.y - a.y;
  Result.z:=b.z - a.z;
end;

function TAABox.GetHeight: Double;
begin
  Result:=Abs(b.y - a.y);
end;

function TAABox.GetLength: Double;
begin
  Result:=Abs(b.z - a.z);
end;

function TAABox.GetWidth: Double;
begin
  Result:=Abs(b.x - a.x);
end;

procedure TAABox.SetExtent(AValue: TVector);
begin
  b.x:=a.x + AValue.x;
  b.y:=a.y + AValue.y;
  b.z:=a.z + AValue.z;
end;

procedure TAABox.SetHeight(AValue: Double);
begin
  b.y:=a.y + AValue;
end;

procedure TAABox.SetLength(AValue: Double);
begin
  b.z:=a.z + AValue;
end;

procedure TAABox.SetWidth(AValue: Double);
begin
  b.x:=a.x + AValue;
end;

procedure TAABox.Zero;
begin
  a.Zero;
  b.Zero;
end;

procedure TAABox.MakeMaxInverted;
begin
  a:=Vector(MaxDouble, MaxDouble, MaxDouble);
  b:=Vector(-MaxDouble, -MaxDouble, -MaxDouble);
end;

procedure TAABox.Include(const Box: TAABox);
begin
  if Box.a.x < a.x then a.x:=Box.a.x;
  if Box.a.y < a.y then a.y:=Box.a.y;
  if Box.a.z < a.z then a.z:=Box.a.z;
  if Box.b.x > b.x then b.x:=Box.b.x;
  if Box.b.y > b.y then b.y:=Box.b.y;
  if Box.b.z > b.z then b.z:=Box.b.z;
end;

procedure TAABox.Include(const V: TVector);
begin
  if V.x < a.x then a.x:=V.x;
  if V.y < a.y then a.y:=V.y;
  if V.z < a.z then a.z:=V.z;
  if V.x > b.x then b.x:=V.x;
  if V.y > b.y then b.y:=V.y;
  if V.z > b.z then b.z:=V.z;
end;

procedure TAABox.Include(const Face: TFace);
var
  i: Integer;
begin
  for i:=0 to System.Length(Face.v) - 1 do
    Include(Face.v[i]);
end;

procedure TAABox.MakeProper;
var
  C: Double;
begin
  if A.x > B.x then begin
    C:=A.x;
    A.x:=B.x;
    B.x:=C;
  end;
  if A.y > B.y then begin
    C:=A.y;
    A.y:=B.y;
    B.y:=C;
  end;
  if A.z > B.z then begin
    C:=A.z;
    A.z:=B.z;
    B.z:=C;
  end;
end;

function TAABox.Includes(const V: TVector): Boolean;
begin
  Result:=(V.x >= a.x) and (V.y >= a.y) and (V.z >= a.z) and
          (V.x <= b.x) and (V.y <= b.y) and (V.z <= b.z);
end;

function TAABox.Includes(const Box: TAABox): Boolean;
begin
  Result:=(Box.a.x >= a.x) and (Box.a.y >= a.y) and (Box.a.z >= a.z) and
          (Box.b.x <= b.x) and (Box.b.y <= b.y) and (Box.b.z <= b.z);
end;

procedure TAABox.Snap(GridSize: Integer);
begin
  a.Snap(GridSize);
  b.Snap(GridSize);
end;

function TAABox.Overlaps(const Box: TAABox): Boolean;
begin
  Result:=not ((Box.a.x > b.x) or
              (Box.b.x < a.x) or
              (Box.a.y > b.y) or
              (Box.b.y < a.y) or
              (Box.a.z > b.z) or
              (Box.b.z < a.z));
end;

procedure TAABox.Grow(V: Double);
begin
  a.x:=a.x - V;
  a.y:=a.y - V;
  a.z:=a.z - V;
  b.x:=b.x + V;
  b.y:=b.y + V;
  b.z:=b.z + V;
end;

procedure TAABox.MoveTo(const V: TVector);
var
  E: TVector;
begin
  E:=Extent;
  a:=V;
  SetExtent(E);
end;

procedure TAABox.GetBoundingSphere(out Center: TVector; out Radius: Double);
var
  C, D, E, F, G, H, I, J: TVector;
begin
  Center:=b.Added(a).Scaled(0.5);
  GetCorners(C, D, E, F, G, H, I, J);
  Radius:=DistanceSq(Center, C);
  Radius:=Max(Radius, DistanceSq(Center, D));
  Radius:=Max(Radius, DistanceSq(Center, E));
  Radius:=Max(Radius, DistanceSq(Center, F));
  Radius:=Max(Radius, DistanceSq(Center, G));
  Radius:=Max(Radius, DistanceSq(Center, H));
  Radius:=Max(Radius, DistanceSq(Center, I));
  Radius:=Max(Radius, DistanceSq(Center, J));
  Radius:=Sqrt(Radius);
end;

procedure TAABox.GetCorners(out MinA, MinB, MinC, MinD, MaxA, MaxB, MaxC, MaxD: TVector);
begin
  MinA:=a;
  MinB:=Vector(b.x, a.y, a.z);
  MinC:=Vector(a.x, a.y, b.z);
  MinD:=Vector(b.x, a.y, b.z);
  MaxA:=Vector(a.x, b.y, a.z);
  MaxB:=Vector(b.x, b.y, a.z);
  MaxC:=Vector(a.x, b.y, b.z);
  MaxD:=b;
end;

{ TAABox }
function TAABox.Center: TVector;
begin
  Result.x:=(b.x + a.x)*0.5;
  Result.y:=(b.y + a.y)*0.5;
  Result.z:=(b.z + a.z)*0.5;
end;

{ Functions }

function Vector(x, y: Double): TVector;
begin
  Result.x:=x;
  Result.y:=y;
  Result.z:=0.0;
end;

function Vector(x, y, z: Double): TVector;
begin
  Result.x:=x;
  Result.y:=y;
  Result.z:=z;
end;

function IntVector(x, y, z: Integer): TIntVector;
begin
  Result.x:=x;
  Result.y:=y;
  Result.z:=z;
end;

function Vector2D(X, Y: Double): TVector2D;
begin
  Result.X:=X;
  Result.Y:=Y;
end;

function Plane(const N: TVector; D: Double): TPlane;
begin
  Result.n:=N;
  Result.d:=D;
end;

function Plane(NX, NY, NZ, D: Double): TPlane;
begin
  Result.n.x:=NX;
  Result.n.y:=NY;
  Result.n.z:=NZ;
  Result.d:=D;
end;

function Plane(const P, N: TVector): TPlane;
begin
  Result.FromPointAndNormal(P, N);
end;

function Plane(const a, b, c: TVector): TPlane;
begin
  Result.FromThreePoints(a, b, c);
end;

function AABox(const A, B: TVector): TAABox;
begin
  Result.a:=A;
  Result.b:=B;
end;

function AABox(MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Double): TAABox;
begin
  Result.a:=Vector(MinX, MinY, MinZ);
  Result.b:=Vector(MaxX, MaxY, MaxZ);
end;

function AABoxOfTransformedAABox(const Box: TAABox; const M: TMatrix): TAABox;
var
  A, B, C, D, E, F, G: TVector;
begin
  Box.GetCorners(A, B, C, D, E, F, G, Result.a);
  Result.a:=M.Transformed(Result.a);
  Result.b:=Result.a;
  Result.Include(M.Transformed(A));
  Result.Include(M.Transformed(B));
  Result.Include(M.Transformed(C));
  Result.Include(M.Transformed(D));
  Result.Include(M.Transformed(E));
  Result.Include(M.Transformed(F));
  Result.Include(M.Transformed(G));
end;

function Transform(const ATranslation, ARotation, AScale: TVector): TTransform;
begin
  with Result do begin
    Translation:=ATranslation;
    Rotation:=ARotation;
    Scale:=AScale;
  end;
end;

function VectorToIntVector(const v: TVector): TIntVector;
begin
  Result.x:=Round(v.x);
  Result.y:=Round(v.y);
  Result.z:=Round(v.z);
end;

function IntVectorToVector(const v: TIntVector): TVector;
begin
  Result.x:=v.x;
  Result.y:=v.y;
  Result.z:=v.z;
end;

function Vector2DToVector(const v: TVector2D; Z: Double): TVector;
begin
  Result.x:=v.X;
  Result.y:=v.Y;
  Result.z:=Z;
end;

function VectorToVector2D(const v: TVector): TVector2D;
begin
  Result.X:=v.x;
  Result.Y:=v.y;
end;

function VectorToColor(const v: TVector): TColor;
begin
  Result:=RGBToColor(Round(Clamp(v.X)*255), Round(Clamp(v.Y)*255), Round(Clamp(v.Z)*255));
end;

function ColorToVector(c: TColor): TVector;
var
  r, g, b: Byte;
begin
  RedGreenBlue(c, r, g, b);
  Result.X:=r/255;
  Result.Y:=g/255;
  Result.Z:=b/255;
end;

function ArrayToMatrix(const M: T16dArray): TMatrix;
begin
  {%H-}Result{%H-}.FromArray(M);{%H+}
end;

function Direction(const Source, Destination: TVector): TVector;
begin
  Result.x:=Destination.x - Source.x;
  Result.y:=Destination.y - Source.y;
  Result.z:=Destination.z - Source.z;
  Result.Normalize;
end;

function Distance(const a, b: TVector): Double;
begin
  Result:=sqrt(sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z));
end;

function DistanceSq(const a, b: TVector): Double;
begin
  Result:=sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z);
end;

function Distance(const a, b: TVector2D): Double;
begin
  Result:=sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
end;

function DistanceSq(const a, b: TVector2D): Double;
begin
  Result:=sqr(a.x - b.x) + sqr(a.y - b.y);
end;

function MidpointBetween(const a, b: TVector): TVector;
begin
  Result:=a.Added(b.Subbed(a).Scaled(0.5));
end;

function MidpointBetween(const a, b: TVector2D): TVector2D;
begin
  Result:=a.Added(b.Subbed(a).Scaled(0.5));
end;

function NormalBetween(const a, b: TVector): TVector;
begin
  a.Cross(b, Result);
  Result.Normalize;
end;

function Reflection(const v, n: TVector): TVector;
var
  d: Double;
begin
  d:=v.Dot(n);
  Result.x:=v.x - 2*d*n.x;
  Result.y:=v.y - 2*d*n.y;
  Result.z:=v.z - 2*d*n.z;
end;

function InnerAngleBetweenNormals(const a, b: TVector): Double;
begin
  Result:=arccos(a.Dot(b));
end;

function AngleBetweenVectorsOnPlane(const a, b, pn: TVector): Double;
begin
  Result:=arctan2(
    // Determinant
    a.x*b.y*pn.z + a.z*b.x*pn.y + a.y*b.z*pn.x - a.z*b.y*pn.x - a.x*b.z*pn.y - a.y*b.x*pn.z,
    // Dot product
    a.Dot(b));
end;

function ProjectVectorOnVector(const a, b: TVector): TVector;
var
  bu: TVector;
begin
  bu:=b.Normalized;
  Result:=bu.Scaled(a.Dot(bu));
end;

function DistanceBetweenSegments(const s1a, s1b, s2a, s2b: TVector; s1p: PVector; s2p: PVector): Double;
var
  u, v, w, dP, sP, tP: TVector;
  a, b, c, d, e, DD, sc, sN, sD, tc, tN, TD: Double;
begin
  u:=s1b;
  u.Sub(s1a);
  v:=s2b;
  v.Sub(s2a);
  w:=s1a;
  w.Sub(s2a);
  a:=u.Dot(u);
  b:=u.Dot(v);
  c:=v.Dot(v);
  d:=u.Dot(w);
  e:=v.Dot(w);
  DD:=a*c - b*b;
  sD:=DD;
  tD:=DD;
  if DD < EPSILON then begin
    sN:=0.0;
    sD:=1.0;
    tN:=e;
    tD:=e;
  end else begin
    sN:=b*e - c*d;
    tN:=a*e - b*d;
    if sN < 0.0 then begin
      sN:=0.0;
      tN:=e;
      tD:=c;
    end else if sN > sD then begin
      sN:=sD;
      tN:=e + b;
      tD:=c;
    end;
  end;

  if tN < 0.0 then begin
    tN:=0.0;
    if -d < 0.0 then sN:=0.0
    else if -d > a then sN:=sD
    else begin
      sN:=-d;
      sD:=a;
    end;
  end else if tN > tD then begin
    tN:=tD;
    if -d + b < 0.0 then sN:=0
    else if -d + b > a then sN:=sD
    else begin
      sN:=-d + b;
      sD:=a;
    end;
  end;

  if Abs(sN) < EPSILON then sc:=0.0 else sc:=sN/sD;
  if Abs(tN) < EPSILON then tc:=0.0 else tc:=tN/tD;

  sP:=u;
  sP.Scale(sc);
  if Assigned(s1p) then begin
    s1p^:=sP;
    s1p^.Add(s1a);
  end;
  tP:=v;
  tP.Scale(tc);
  if Assigned(s2p) then begin
    s2p^:=tP;
    s2p^.Add(s2a);
  end;
  dP:=sP;
  dP.Sub(tP);
  dp.Add(w);

  Result:=dp.Length;
end;

function SegmentIntersection2D(const s1a, s1b, s2a, s2b: TVector2D; out IP: TVector2D): Boolean;
var
  ua, ub, ubt: Double;
begin
  ubt:=((s2b.Y-s2a.Y)*(s1b.X-s1a.X))-((s2b.X-s2a.X)*(s1b.Y-s1a.Y));
  if ubt <> 0.0 then begin
    ua:=(((s2b.X-s2a.X)*(s1a.Y-s2a.Y))-((s2b.Y-s2a.Y)*(s1a.X-s2a.X)))/ubt;
    ub:=(((s1b.X-s1a.X)*(s1a.Y-s2a.Y))-((s1b.Y-s1a.Y)*(s1a.X-s2a.X)))/ubt;
    if (ua >= 0.0) and (ua <= 1.0) and (ub >= 0.0) and (ub <= 1.0) then begin
      IP:=Vector2D(s1a.X+(ua*(s1b.X-s1a.X)), s1a.Y+(ua*(s1b.Y-s1a.Y)));
      Exit(True);
    end;
  end;
  IP:=Vector2D(0, 0);
  Result:=False;
end;

function TriangleNormal(const a, b, c: TVector): TVector;
var
  ab, ac: TVector;
begin
  ab:=b;
  ab.Sub(a);
  ac:=c;
  ac.Sub(a);
  Result:=NormalBetween(ab, ac);
end;

procedure TriangleBarycentric(const p, a, b, c: TVector; out bu, bv, bw: Double);
var
  v1, v2, v3: TVector;
  d00, d01, d11, d20, d21, den: Double;
begin
  v1:=b; v1.Sub(a);
  v2:=c; v2.Sub(a);
  v3:=p; v3.Sub(a);
  d00:=v1.Dot(v1);
  d01:=v1.Dot(v2);
  d11:=v2.Dot(v2);
  d20:=v3.Dot(v1);
  d21:=v3.Dot(v2);
  den:=d00*d11 - d01*d01;
  bv:=(d11*d20 - d01*d21)/den;
  bw:=(d00*d21 - d01*d20)/den;
  bu:=1 - bv - bw;
end;

function Interpolation(const a, b, p: TVector): Double;
begin
  Result:=Distance(a, p)/Distance(a, b);
end;

function Interpolate(const a, b: TVector; Int: Double): TVector;
begin
  Result.x:=a.x + (b.x - a.x)*Int;
  Result.y:=a.y + (b.y - a.y)*Int;
  Result.z:=a.z + (b.z - a.z)*Int;
end;

function Interpolate(const a, b, Int: Double): Double;
begin
  Result:=a + (b - a)*Int;
end;

function Zero(v: Double): Boolean;
begin
  Result:=Abs(v) < EPSILON;
end;

function Equal(a, b: Double): Boolean;
begin
  Result:=Abs(a - b) < EPSILON;
end;

function ExtMod(a, b: Double): Double;
begin
  Result:=A - Round(A/B)*B;
end;

initialization
  TheIdentityMatrix.Identity;
end.

