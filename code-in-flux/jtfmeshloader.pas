unit JTFMeshLoader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Maths, Meshes, Textures;

type
  TJTFMeshLoaderException = class(Exception);

  { TJTFMeshLoader }

  TJTFMeshLoader = class(TMeshLoader)
  private
    FAutoLoadTexture: Boolean;
  public
    constructor Create; override;
    procedure LoadFromStream(AStream: TStream; AFileName: string); override;
    property AutoLoadTexture: Boolean read FAutoLoadTexture write FAutoLoadTexture;
  end;

implementation

uses
  Logger;

{ TJTFMeshLoader }

constructor TJTFMeshLoader.Create;
begin
  inherited Create;
  FAutoLoadTexture:=True;
end;

procedure TJTFMeshLoader.LoadFromStream(AStream: TStream; AFileName: string);
type
  TJTFVertex = packed record
    X, Y, Z, NX, NY, NZ, U, V: Single;
  end;
  TJTFFace = packed record
    A, B, C: TJTFVertex;
  end;
var
  FaceCount, I: Integer;
  Vertices: array of TMeshVertex;
  Indices: array of Integer;
  Face: TJTFFace;
  ImgFileName: String;
  Texture: TTexture;
begin
  // Read header
  if AStream.ReadByte <> 74 then raise TJTFMeshLoaderException.Create('Invalid JTF magic number');
  if AStream.ReadByte <> 84 then raise TJTFMeshLoaderException.Create('Invalid JTF magic number');
  if AStream.ReadByte <> 70 then raise TJTFMeshLoaderException.Create('Invalid JTF magic number');
  if AStream.ReadByte <> 33 then raise TJTFMeshLoaderException.Create('Invalid JTF magic number');
  // Read vertex format
  if AStream.ReadDWord <> 0 then raise TJTFMeshLoaderException.Create('Unknown JTF vertex format');
  FaceCount:=AStream.ReadDWord;
  // Prepare arrays
  SetLength(Vertices, FaceCount*3);
  SetLength(Indices, FaceCount*3);
  // Read vertices
  for I:=0 to FaceCount - 1 do begin
    AStream.Read(Face{%H-}, SizeOf(Face));
    Vertices[I*3].x:=Face.A.X;
    Vertices[I*3].y:=Face.A.Y;
    Vertices[I*3].z:=Face.A.Z;
    Vertices[I*3].n.x:=Face.A.NX;
    Vertices[I*3].n.y:=Face.A.NY;
    Vertices[I*3].n.z:=Face.A.NZ;
    Vertices[I*3].s:=Face.A.U;
    Vertices[I*3].t:=Face.A.V;
    Vertices[I*3 + 1].x:=Face.B.X;
    Vertices[I*3 + 1].y:=Face.B.Y;
    Vertices[I*3 + 1].z:=Face.B.Z;
    Vertices[I*3 + 1].n.x:=Face.B.NX;
    Vertices[I*3 + 1].n.y:=Face.B.NY;
    Vertices[I*3 + 1].n.z:=Face.B.NZ;
    Vertices[I*3 + 1].s:=Face.B.U;
    Vertices[I*3 + 1].t:=Face.B.V;
    Vertices[I*3 + 2].x:=Face.C.X;
    Vertices[I*3 + 2].y:=Face.C.Y;
    Vertices[I*3 + 2].z:=Face.C.Z;
    Vertices[I*3 + 2].n.x:=Face.C.NX;
    Vertices[I*3 + 2].n.y:=Face.C.NY;
    Vertices[I*3 + 2].n.z:=Face.C.NZ;
    Vertices[I*3 + 2].s:=Face.C.U;
    Vertices[I*3 + 2].t:=Face.C.V;
  end;
  for I:=0 to High(Indices) do Indices[I]:=I;
  if AutoLoadTexture then begin
    ImgFileName:=ExtractFileNameWithoutExt(AFileName);
    if FileExists(ImgFileName + '.png') then
      ImgFileName:=ImgFileName + '.png'
    else if FileExists(ImgFileName + '.jpg') then
      ImgFileName:=ImgFileName + '.jpg'
    else if FileExists(ImgFileName + '.bmp') then
      ImgFileName:=ImgFileName + '.bmp'
    else if FileExists(ImgFileName + '.gif') then
      ImgFileName:=ImgFileName + '.gif'
    else begin
      Log('Warning: Cannot find PNG, JPEG, BMP or GIF texture for ' + AFileName);
      ImgFileName:='';;
      Texture:=nil;
    end;
    if ImgFileName <> '' then begin
      Texture:=TTexture.Create(ImgFileName);
      Texture.Reload;
    end;
  end else Texture:=nil;
  Log('Creating Mesh from JTF file ' + AFileName);
  SetData(Vertices, Indices, Texture, AFileName);
  if Assigned(Mesh) then
    Log('Done loading JTF file ' + AFileName + ' with ' + IntToStr(FaceCount) + ' triangles')
  else
    Log('Failed to load JTF file ' + AFileName);
end;

end.

