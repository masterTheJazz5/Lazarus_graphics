unit Cameras;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Maths, MiscUtils;

type

  TCameraMode = (cmPerspective, cmOrthographic);

  { TCamera }

  TCamera = class(TComponent)
  private
    FAspect: Double;
    FFOV: Double;
    FMode: TCameraMode;
    FOrthoScale: Double;
    FProjectionMatrix: TMatrix;
    FModelViewMatrix: TMatrix;
    FPosition: TVector;
    FDirection: TVector;
    FRight: TVector;
    FUnprojectionMatrix: TMatrix;
    FUp: TVector;
    FZFar: Double;
    FZNear: Double;
    FChangeListeners: TNotifyEventList;
    function GetBookmark: string;
    function GetDirectionX: Double; inline;
    function GetDirectionY: Double; inline;
    function GetDirectionZ: Double; inline;
    function GetPositionX: Double; inline;
    function GetPositionY: Double; inline;
    function GetPositionZ: Double; inline;
    procedure SetAspect(AValue: Double);
    procedure SetBookmark(AValue: string);
    procedure SetDirectionX(AValue: Double);
    procedure SetDirectionY(AValue: Double);
    procedure SetDirectionZ(AValue: Double);
    procedure SetFOV(AValue: Double);
    procedure SetMode(AValue: TCameraMode);
    procedure SetOrthoScale(AValue: Double);
    procedure SetPosition(AValue: TVector);
    procedure SetDirection(AValue: TVector);
    procedure SetPositionX(AValue: Double); inline;
    procedure SetPositionY(AValue: Double); inline;
    procedure SetPositionZ(AValue: Double); inline;
    procedure SetZFar(AValue: Double);
    procedure SetZNear(AValue: Double);
    procedure NotifyChangeListeners;
    procedure UpdateProjectionMatrix;
    procedure UpdateModelViewMatrix;
    procedure UpdateUnprojectionMatrix;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure MoveTowards(const Vector: TVector);
    procedure MoveTowardsInView(const Vector: TVector);
    procedure MoveForward(Speed: Double);
    procedure RotateHorizontally(Angle: Double);
    procedure RotateVertically(Angle: Double);
    procedure RotateAroundPoint(const Point: TVector; HorizontalRotationAngle, VerticalRotationAngle: Double);
    property Position: TVector read FPosition write SetPosition;
    property Direction: TVector read FDirection write SetDirection;
    property Up: TVector read FUp;
    property Right: TVector read FRight;
    property ProjectionMatrix: TMatrix read FProjectionMatrix;
    property ModelViewMatrix: TMatrix read FModelViewMatrix;
    property UnprojectionMatrix: TMatrix read FUnprojectionMatrix;
    procedure AddChangeListener(AListener: TNotifyEvent);
    procedure RemoveChangeListener(AListener: TNotifyEvent);
  published
    property PositionX: Double read GetPositionX write SetPositionX;
    property PositionY: Double read GetPositionY write SetPositionY;
    property PositionZ: Double read GetPositionZ write SetPositionZ;
    property DirectionX: Double read GetDirectionX write SetDirectionX;
    property DirectionY: Double read GetDirectionY write SetDirectionY;
    property DirectionZ: Double read GetDirectionZ write SetDirectionZ;
    property Aspect: Double read FAspect write SetAspect;
    property FOV: Double read FFOV write SetFOV;
    property ZNear: Double read FZNear write SetZNear;
    property ZFar: Double read FZFar write SetZFar;
    property OrthoScale: Double read FOrthoScale write SetOrthoScale;
    property Mode: TCameraMode read FMode write SetMode default cmPerspective;
    property Bookmark: string read GetBookmark write SetBookmark stored False;
  end;

procedure Register;

implementation

uses
  LResources;

procedure Register;
begin
  {$I cameras_icon.lrs}
  RegisterComponents('RTTK', [TCamera]);
end;

{ TCamera }

procedure TCamera.SetPosition(AValue: TVector);
begin
  if FPosition=AValue then Exit;
  FPosition:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetAspect(AValue: Double);
begin
  if FAspect=AValue then Exit;;
  FAspect:=AValue;
  UpdateProjectionMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetBookmark(AValue: string);
var
  Parts: TStringArray;
begin
  if (Length(AValue) < 9) or (AValue[1] <> '[') or (AValue[Length(AValue)] <> ']') then Exit;
  AValue:=LowerCase(Copy(AValue, 2, MaxInt));
  AValue:=Copy(AValue, 1, Length(AValue) - 1);
  SplitString(AValue, ' ', Parts, False);
  if Length(Parts) <> 8 then Exit;
  if Parts[7]='perspective' then FMode:=cmPerspective
  else if Parts[7]='orthographic' then FMode:=cmOrthographic
  else Exit;
  FPosition.x:=StringToFloat(Parts[0]);
  FPosition.y:=StringToFloat(Parts[1]);
  FPosition.z:=StringToFloat(Parts[2]);
  FDirection.x:=StringToFloat(Parts[3]);
  FDirection.y:=StringToFloat(Parts[4]);
  FDirection.z:=StringToFloat(Parts[5]);
  FOrthoScale:=StringToFloat(Parts[6]);
  UpdateProjectionMatrix;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetDirectionX(AValue: Double);
begin
  if FDirection.x=AValue then Exit;
  FDirection.x:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetDirectionY(AValue: Double);
begin
  if FDirection.y=AValue then Exit;
  FDirection.y:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetDirectionZ(AValue: Double);
begin
  if FDirection.z=AValue then Exit;
  FDirection.z:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

function TCamera.GetDirectionX: Double;
begin
  Result:=FDirection.x;
end;

function TCamera.GetBookmark: string;
begin
  Result:='[' + FloatToString(PositionX) + ' '
              + FloatToString(PositionY) + ' '
              + FloatToString(PositionZ) + ' '
              + FloatToString(DirectionX) + ' '
              + FloatToString(DirectionY) + ' '
              + FloatToString(DirectionZ) + ' '
              + FloatToString(OrthoScale) + ' ';
  if Mode=cmPerspective then Result += 'perspective]'
                        else Result += 'orthographic]';
end;

function TCamera.GetDirectionY: Double;
begin
  Result:=FDirection.y;
end;

function TCamera.GetDirectionZ: Double;
begin
  Result:=FDirection.z;
end;

function TCamera.GetPositionX: Double;
begin
  Result:=FPosition.x;
end;

function TCamera.GetPositionY: Double;
begin
  Result:=FPosition.y;
end;

function TCamera.GetPositionZ: Double;
begin
  Result:=FPosition.z;
end;

procedure TCamera.SetFOV(AValue: Double);
begin
  if FFOV=AValue then Exit;
  FFOV:=AValue;
  UpdateProjectionMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetMode(AValue: TCameraMode);
begin
  if FMode=AValue then Exit;
  FMode:=AValue;
  UpdateProjectionMatrix;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetOrthoScale(AValue: Double);
begin
  if FOrthoScale=AValue then Exit;
  FOrthoScale:=AValue;
  UpdateProjectionMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetDirection(AValue: TVector);
begin
  if FDirection=AValue then Exit;
  FDirection:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetPositionX(AValue: Double);
begin
  if FPosition.x=AValue then Exit;
  FPosition.x:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetPositionY(AValue: Double);
begin
  if FPosition.y=AValue then Exit;
  FPosition.y:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetPositionZ(AValue: Double);
begin
  if FPosition.z=AValue then Exit;
  FPosition.z:=AValue;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetZFar(AValue: Double);
begin
  if FZFar=AValue then Exit;
  FZFar:=AValue;
  UpdateProjectionMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.SetZNear(AValue: Double);
begin
  if FZNear=AValue then Exit;
  FZNear:=AValue;
  UpdateProjectionMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.NotifyChangeListeners;
var
  I: Integer;
begin
  if Assigned(FChangeListeners) then begin
    for I:=0 to FChangeListeners.Count - 1 do
      FChangeListeners[I](Self);
  end;
end;

procedure TCamera.UpdateProjectionMatrix;
begin
  if Mode=cmPerspective then begin
    FProjectionMatrix.Perspective(FOV*PI/180, Aspect, ZNear, ZFar);
  end else begin
    FProjectionMatrix.Orthographic(-OrthoScale*0.5*Aspect, OrthoScale*0.5*Aspect, OrthoScale*0.5, -OrthoScale*0.5, -ZFar, ZFar);
  end;
end;

procedure TCamera.UpdateModelViewMatrix;
var
  Target, Tmp: TVector;
begin
  Target:=Position;
  FDirection.Normalize;
  Target.Add(FDirection);
  if Abs(FDirection.Y) > 0.999 then
    Tmp:=Vector(0, 0, 1)
  else
    Tmp:=Vector(0, 1, 0);
  FModelViewMatrix.LookAt(Position, Target, Tmp);
  FRight:=Direction.Crossed(Tmp).Normalized;
  FUp:=FRight.Crossed(Direction).Normalized;
end;

procedure TCamera.UpdateUnprojectionMatrix;
begin
  FUnprojectionMatrix:=ProjectionMatrix.Multiplied(ModelViewMatrix).Inverted;
end;

constructor TCamera.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFOV:=60;
  FAspect:=640/480;
  FZNear:=0.1;
  FZFar:=16384;
  FOrthoScale:=1;
  FPosition:=Vector(0, 0, 0);
  FDirection:=Vector(0, 0, 1);
  UpdateProjectionMatrix;
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
end;

destructor TCamera.Destroy;
begin
  FreeAndNil(FChangeListeners);
  inherited Destroy;
end;

procedure TCamera.MoveTowards(const Vector: TVector);
begin
  FPosition.Add(Vector);
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.MoveTowardsInView(const Vector: TVector);
begin
  MoveTowards(ModelViewMatrix.Inverted.TransformedNormal(Vector));
end;

procedure TCamera.MoveForward(Speed: Double);
var
  V: TVector;
begin
  V:=Direction;
  V.Scale(Speed);
  MoveTowards(V);
end;

procedure TCamera.RotateHorizontally(Angle: Double);
var
  Mtx: TMatrix;
begin
  Mtx.YRotation(Angle);
  Mtx.Transform(FDirection);
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.RotateVertically(Angle: Double);
var
  Mtx: TMatrix;
begin
  Mtx.Rotation(ModelViewMatrix.m11, ModelViewMatrix.m12, ModelViewMatrix.m13, Angle);
  Mtx.Transform(FDirection);
  UpdateModelViewMatrix;
  UpdateUnprojectionMatrix;
  NotifyChangeListeners;
end;

procedure TCamera.RotateAroundPoint(const Point: TVector;
  HorizontalRotationAngle, VerticalRotationAngle: Double);
var
  PointToCamera: TVector;
  Mtx: TMatrix;
begin
  PointToCamera:=Position.Subbed(Point);
  Mtx.Rotation(1, 0, 0, VerticalRotationAngle);
  Mtx.Transform(PointToCamera);
  Mtx.Rotation(0, 1, 0, HorizontalRotationAngle);
  Mtx.Transform(PointToCamera);
  Position:=PointToCamera.Added(Point);
end;

procedure TCamera.AddChangeListener(AListener: TNotifyEvent);
begin
  if not Assigned(FChangeListeners) then FChangeListeners:=TNotifyEventList.Create;
  FChangeListeners.Add(AListener);
end;

procedure TCamera.RemoveChangeListener(AListener: TNotifyEvent);
begin
  if Assigned(FChangeListeners) then FChangeListeners.Remove(AListener);
end;

end.

