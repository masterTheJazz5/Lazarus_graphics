unit MiscUtils;

{$mode objfpc}{$H+}
{$hints off}
{$warnings off}

interface

uses
  {$IFDEF WINDOWS}
  Windows,
  {$ENDIF}
  Classes, Controls, SysUtils, Graphics, FGL;

type
  TStringArray = array of string;
  TNotifyEventList = specialize TFPGList<TNotifyEvent>;
  TRectPart = (rpNone, rpTopLeft, rpTop, rpTopRight, rpLeft, rpInside, rpRight, rpBottomLeft, rpBottom, rpBottomRight);

  { TStreamIO }

  TStreamIO = object
    Stream: TStream;
    procedure WriteBuffer(const Buffer; Length: Integer); inline;
    procedure WriteByte(AByte: Byte); inline;
    procedure WriteWord(AWord: Word); inline;
    procedure WriteInteger(AInt: Integer); inline;
    procedure WriteInt64(AInt64: Int64); inline;
    procedure WriteUInt64(AUInt64: UInt64); inline;
    procedure WriteCardinal(ACardinal: Cardinal); inline;
    procedure WriteBoolean(ABool: Boolean); inline;
    procedure WriteSingle(ASingle: Single); inline;
    procedure WriteDouble(ADouble: Double); inline;
    procedure WriteString(AString: UTF8String); inline;
    procedure ReadBuffer(out Buffer; Length: Integer); inline;
    function ReadByte: Byte; inline;
    function ReadWord: Word; inline;
    function ReadInteger: Integer; inline;
    function ReadInt64: Int64; inline;
    function ReadUInt64: UInt64; inline;
    function ReadCardinal: Cardinal; inline;
    function ReadBoolean: Boolean; inline;
    function ReadSingle: Single; inline;
    function ReadDouble: Double; inline;
    function ReadString: UTF8String; inline;
  end;

var
  NormalFormatSettings: TFormatSettings;

function GetTextFileContents(FileName: string): string;
function SetTextFileContents(FileName, Data: string): Boolean;
function LoadImageFile(FileName: string): TGraphic;
function CanLoadImageFile(FileName: string): Boolean;
function GetFilesInDirectory(Path: string; Mask: string=AllFilesMask): TStringArray;
procedure MemZero(out V; Size: SizeUInt); inline;
procedure SplitString(S: string; Sep: Char; out Parts: TStringArray; SkipSpaces: Boolean=True);
function FloatToString(V: Double): string; inline;
function StringToFloat(s: string; DefValue: Double=0): Double; inline;
function TakeScreenshot: TBitmap;
function GetNumberOfHardwareThreads: Integer;
function GetRectPartAt(const ARect: TRect; X, Y: Integer; EdgeMiddlesOnly: Boolean=True; EdgeThreshold: Integer=5): TRectPart;
function GetResizeCursorForRectPart(ARectPart: TRectPart; InsideCursor: TCursor=crArrow): TCursor;
function OffsetRectPart(const ARect: TRect; ARectPart: TRectPart; DX, DY: Integer): TRect;
function NormalizeRectOrientation(const ARect: TRect): TRect;
function IndexOfWord(AWord: string; Words: array of string; NotFoundValue: Integer=-1): Integer;
function IndexOfCaseInsensitiveWord(AWord: string; Words: array of string; NotFoundValue: Integer=-1): Integer;

implementation

uses
  LCLIntf, LCLType;

function GetTextFileContents(FileName: string): string;
var
  Strings: TStringList = nil;
begin
  try
    Strings:=TStringList.Create;
    Strings.LoadFromFile(FileName);
    Result:=Strings.Text;
  except
    Result:='';
  end;
  if Assigned(Strings) then FreeAndNil(Strings);
end;

function SetTextFileContents(FileName, Data: string): Boolean;
var
  Strings: TStringList = nil;
begin
  Result:=False;
  try
    Strings:=TStringList.Create;
    Strings.Text:=Data;
    Strings.SaveToFile(FileName);
    Result:=True;
  except
  end;
  if Assigned(Strings) then FreeAndNil(Strings);
end;

function LoadImageFile(FileName: string): TGraphic;
var
  Pic: TPicture;
  Gfx: TGraphic;
begin
  Pic:=TPicture.Create;
  Result:=nil;
  try
    Pic.LoadFromFile(FileName);
    if Assigned(Pic.Graphic) then begin
      Gfx:=TGraphic(Pointer(Pic.Graphic.NewInstance));
      Gfx.Create;
      Gfx.Assign(Pic.Graphic);
      Result:=Gfx;
    end;
  except
    FreeAndNil(Result);
  end;
end;

function CanLoadImageFile(FileName: string): Boolean;
var
  Pic: TPicture;
  Ext: string;
begin
  Ext:=ExtractFileExt(FileName);
  if Ext='' then Exit(False);
  if Ext[1]='.' then Ext:=Copy(Ext, 2, Length(Ext));
  if Ext='' then Exit(False);
  Pic:=TPicture.Create;
  try
    Result:=Assigned(Pic.FindGraphicClassWithFileExt(Ext, False));
  except
    Result:=False;
  end;
  FreeAndNil(Pic);
end;

function GetFilesInDirectory(Path: string; Mask: string): TStringArray;
var
  Fi: TRawByteSearchRec;
  R: LongInt;
begin
  if (Path <> '') and not (Path[Length(Path)] in ['\', '/']) then Path += DirectorySeparator;
  R:=FindFirst(Path + Mask, faAnyFile, Fi);
  SetLength(Result, 0);
  if R=0 then begin
    while R=0 do begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)]:=ExpandFileName(Path + Fi.Name);
      R:=FindNext(Fi);
    end;
    FindClose(Fi);
  end;
end;

procedure MemZero(out V; Size: SizeUInt);
var
  Quads: SizeUInt;
begin
  // The weird (@V)^ is to avoid FPC's warning about unitialized parameter
  // since FillChar is defined to use "var" instead of "out"
  Quads:=Size div 8;
  FillQWord((@V)^, Quads, 0);
  FillChar(((@V) + Int64(Quads)*8)^, Int64(Size) - Int64(Quads)*8, 0);
end;

procedure SplitString(S: string; Sep: Char; out Parts: TStringArray; SkipSpaces: Boolean);
var
  I: Integer;
begin
  SetLength(Parts, 1);
  I:=1;
  while I <= Length(S) do begin
    if SkipSpaces and (I <= Length(S)) and (S[I] in [#9, ' ']) then begin
      while (I <= Length(S)) and (S[I] in [#9, ' ']) do Inc(I);
      if I > Length(S) then Break;
      if Sep in [#9, ' '] then begin
        SetLength(Parts, Length(Parts) + 1);
        Continue;
      end;
    end;
    if I > Length(S) then Break;
    if S[I]=Sep then begin
      Inc(I);
      SetLength(Parts, Length(Parts) + 1);
      Continue;
    end;
    Parts[High(Parts)]:=Parts[High(Parts)] + S[I];
    Inc(I);
  end;
end;

function FloatToString(V: Double): string;
begin
  Result:=FloatToStr(V, NormalFormatSettings);
end;

function StringToFloat(s: string; DefValue: Double): Double;
begin
  Result:=StrToFloatDef(s, DefValue, NormalFormatSettings);
end;

function TakeScreenshot: TBitmap;
var
  ScreenDC: HDC;
begin
  Result:=TBitmap.Create;
  ScreenDC:=GetDC(0);
  Result.LoadFromDevice(ScreenDC);
  ReleaseDC(0, ScreenDC);
end;

function GetNumberOfHardwareThreads: Integer;
{$IFDEF WINDOWS}
  function Win32Impl: Integer;
  var
    SI: SYSTEM_INFO;
  begin
    GetSystemInfo(SI);
    Result:=SI.dwNumberOfProcessors;
  end;
{$ENDIF}
begin
  {$IFDEF WINDOWS}
  Result:=Win32Impl;
  {$DEFINE GNOHT_FOUND}
  {$ENDIF}
  {$IFNDEF GNOHT_FOUND}
  Result:=1;
  {$ELSE}
  {$UNDEF GNOHT_FOUND}
  {$ENDIF}
end;

function GetRectPartAt(const ARect: TRect; X, Y: Integer;
  EdgeMiddlesOnly: Boolean; EdgeThreshold: Integer): TRectPart;
begin
  if (X < ARect.Left - EdgeThreshold) or (Y < ARect.Top - EdgeThreshold) or
     (X > ARect.Right + EdgeThreshold) or (Y > ARect.Bottom + EdgeThreshold) then Exit(rpNone);
  if (X > ARect.Left + EdgeThreshold) and (Y > ARect.Top + EdgeThreshold) and
     (X < ARect.Right - EdgeThreshold) and (Y < ARect.Bottom - EdgeThreshold) then Exit(rpInside);
  if Y < ARect.Top + EdgeThreshold then begin
    if X < ARect.Left + EdgeThreshold then Exit(rpTopLeft);
    if X > ARect.Right - EdgeThreshold then Exit(rpTopRight);
    if EdgeMiddlesOnly and ((X < ARect.Left + ARect.Width div 2 - EdgeThreshold) or
                            (X > ARect.Left + ARect.Width div 2 + EdgeThreshold)) then Exit(rpInside);
    Exit(rpTop);
  end;
  if Y > ARect.Bottom - EdgeThreshold then begin
    if X < ARect.Left + EdgeThreshold then Exit(rpBottomLeft);
    if X > ARect.Right - EdgeThreshold then Exit(rpBottomRight);
    if EdgeMiddlesOnly and ((X < ARect.Left + ARect.Width div 2 - EdgeThreshold) or
                            (X > ARect.Left + ARect.Width div 2 + EdgeThreshold)) then Exit(rpInside);
    Exit(rpBottom);
  end;
  if X < ARect.Left + EdgeThreshold then begin
    if EdgeMiddlesOnly and ((Y < ARect.Top + ARect.Height div 2 - EdgeThreshold) or
                            (Y > ARect.Top + ARect.Height div 2 + EdgeThreshold)) then Exit(rpInside);
    Exit(rpLeft);
  end else begin
    if EdgeMiddlesOnly and ((Y < ARect.Top + ARect.Height div 2 - EdgeThreshold) or
                            (Y > ARect.Top + ARect.Height div 2 + EdgeThreshold)) then Exit(rpInside);
    Exit(rpRight);
  end;
end;

function GetResizeCursorForRectPart(ARectPart: TRectPart; InsideCursor: TCursor): TCursor;
begin
  case ARectPart of
    rpTopLeft, rpBottomRight: Result:=crSizeNWSE;
    rpLeft, rpRight: Result:=crSizeWE;
    rpTop, rpBottom: Result:=crSizeNS;
    rpTopRight, rpBottomLeft: Result:=crSizeNESW;
    rpInside: Result:=InsideCursor;
    else Result:=crDefault;
  end;
end;

function OffsetRectPart(const ARect: TRect; ARectPart: TRectPart; DX,
  DY: Integer): TRect;
begin
  if ARectPart=rpNone then Exit(ARect);
  Result:=ARect;
  if ARectPart=rpInside then begin
    Result.Offset(DX, DY);
    Exit;
  end;
  if ARectPart in [rpLeft, rpTopLeft, rpBottomLeft] then Result.Left += DX;
  if ARectPart in [rpRight, rpTopRight, rpBottomRight] then Result.Right += DX;
  if ARectPart in [rpTop, rpTopLeft, rpTopRight] then Result.Top += DY;
  if ARectPart in [rpBottom, rpBottomLeft, rpBottomRight] then Result.Bottom += DY;
end;

function NormalizeRectOrientation(const ARect: TRect): TRect;
var
  Tmp: LongInt;
begin
  Result:=ARect;
  if Result.Left > Result.Right then begin
    Tmp:=Result.Left;
    Result.Left:=Result.Right;
    Result.Right:=Tmp;
  end;
  if Result.Top > Result.Bottom then begin
    Tmp:=Result.Top;
    Result.Top:=Result.Bottom;
    Result.Bottom:=Tmp;
  end;
end;

function IndexOfWord(AWord: string; Words: array of string;
  NotFoundValue: Integer): Integer;
var
  I: Integer;
begin
  for I:=0 to High(Words) do
    if AWord=Words[I] then Exit(I);
  Result:=NotFoundValue;
end;

function IndexOfCaseInsensitiveWord(AWord: string; Words: array of string;
  NotFoundValue: Integer): Integer;
var
  I: Integer;
begin
  AWord:=LowerCase(AWord);
  for I:=0 to High(Words) do
    if AWord=LowerCase(Words[I]) then Exit(I);
  Result:=NotFoundValue;
end;

{ TStreamIO }

procedure TStreamIO.WriteBuffer(const Buffer; Length: Integer);
begin
  Stream.Write(Buffer, Length);
end;

procedure TStreamIO.WriteByte(AByte: Byte);
begin
  Stream.WriteByte(AByte);
end;

procedure TStreamIO.WriteWord(AWord: Word);
begin
  Stream.WriteBuffer(AWord, SizeOf(AWord));
end;

procedure TStreamIO.WriteInteger(AInt: Integer);
begin
  Stream.WriteBuffer(AInt, SizeOf(AInt));
end;

procedure TStreamIO.WriteInt64(AInt64: Int64);
begin
  Stream.WriteBuffer(AInt64, SizeOf(AInt64));
end;

procedure TStreamIO.WriteUInt64(AUInt64: UInt64);
begin
  Stream.WriteBuffer(AUInt64, SizeOf(AUInt64));
end;

procedure TStreamIO.WriteCardinal(ACardinal: Cardinal);
begin
  Stream.WriteBuffer(ACardinal, SizeOf(ACardinal));
end;

procedure TStreamIO.WriteBoolean(ABool: Boolean);
begin
  if ABool then WriteByte(1) else WriteByte(0);
end;

procedure TStreamIO.WriteSingle(ASingle: Single);
begin
  Stream.WriteBuffer(ASingle, SizeOf(ASingle));
end;

procedure TStreamIO.WriteDouble(ADouble: Double);
begin
  Stream.WriteBuffer(ADouble, SizeOf(ADouble));
end;

procedure TStreamIO.WriteString(AString: UTF8String);
begin
  WriteInteger(Length(AString));
  WriteBuffer(AString[1], Length(AString));
end;

procedure TStreamIO.ReadBuffer(out Buffer; Length: Integer);
begin
  Stream.ReadBuffer(Buffer, Length);
end;

function TStreamIO.ReadByte: Byte;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadWord: Word;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadInteger: Integer;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadInt64: Int64;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadUInt64: UInt64;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadCardinal: Cardinal;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadBoolean: Boolean;
begin
  Result:=ReadByte <> 0;
end;

function TStreamIO.ReadSingle: Single;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadDouble: Double;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TStreamIO.ReadString: UTF8String;
begin
  SetLength(Result, ReadInteger);
  ReadBuffer(Result[1], Length(Result));
end;

initialization
  NormalFormatSettings:=DefaultFormatSettings;
  NormalFormatSettings.DecimalSeparator:='.';
end.

