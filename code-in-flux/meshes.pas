unit Meshes;

{$mode objfpc}{$H+}
{$modeswitch advancedrecords}

interface

uses
  Classes, SysUtils, Maths, GL, Textures;

type

  { TMeshVertex }

  TMeshVertex = object(TVector)
    s, t: Double;
    n: TVector;
  end;

  { TMesh }

  TMesh = class(TMeshBase)
  private
    FFileName: string;
    FTexture: TTexture;
    FDisplayList: GLuint;
    FVertices: array of TMeshVertex;
    FIndices: array of Integer;
    function GetDisplayList: GLuint; inline;
    function GetIndexCount: Integer; inline;
    function GetIndices(AIndex: Integer): Integer; inline;
    function GetVertexCount: Integer; inline;
    function GetVertices(AIndex: Integer): TMeshVertex; inline;
    procedure GenerateDisplayList;
    procedure InvalidateList;
    procedure SetData(AVertices: array of TMeshVertex; AIndices: array of Integer; ATexture: TTexture; AFileName: string);
  public
    destructor Destroy; override;
    procedure Clear;
    procedure Scale(Scalar: Double);
    procedure Scale(XYZ: TVector);
    procedure ScaleToHeight(AHeight: Double);
    procedure Translate(XYZ: TVector);
    function CalcBoundingRadius: Double;
    function CalcBoundingBox: TAABox;
    function AddVertex(const V: TMeshVertex): Integer;
    function AddVertex(const V, N: TVector; S, T: Double): Integer;
    function VertexAt(const V: TVector): Integer;
    function MatchingVertex(const V, N: TVector): Integer;
    function MatchingVertex(const V, N: TVector; S, T: Double): Integer;
    function AddFace(A, B, C: Integer): Integer;
    function RayHitAnyFace(ARay: TRay; const AMtx: TMatrix; out IP: TVector): Boolean;
    function RayHit(ARay: TRay; const AMtx: TMatrix; out IP: TVector): Boolean;
    property Vertices[AIndex: Integer]: TMeshVertex read GetVertices;
    property VertexCount: Integer read GetVertexCount;
    property Indices[AIndex: Integer]: Integer read GetIndices;
    property IndexCount: Integer read GetIndexCount;
    property DisplayList: GLuint read GetDisplayList;
    property Texture: TTexture read FTexture;
    property FileName: string read FFileName;
  end;

  { TMeshLoader }

  TMeshLoader = class
  private
    FMesh: TMesh;
  protected
    procedure SetData(AVertices: array of TMeshVertex; AIndices: array of Integer; ATexture: TTexture; AFileName: string);
  public
    constructor Create; virtual;
    procedure LoadFromStream(AStream: TStream; AFileName: string); virtual; abstract;
    procedure LoadFromFile(AFileName: string); virtual;
    property Mesh: TMesh read FMesh;
  end;

implementation

{ TMeshLoader }

procedure TMeshLoader.SetData(AVertices: array of TMeshVertex;
  AIndices: array of Integer; ATexture: TTexture; AFileName: string);
begin
  FMesh:=TMesh.Create;
  FMesh.SetData(AVertices, AIndices, ATexture, AFileName);
end;

constructor TMeshLoader.Create;
begin
end;

procedure TMeshLoader.LoadFromFile(AFileName: string);
var
  FileStream: TFileStream = nil;
begin
  try
    FileStream:=TFileStream.Create(AFileName, fmOpenRead);
    LoadFromStream(FileStream, AFileName);
  finally
    FreeAndNil(FileStream);
  end;
end;

{ TMesh }

function TMesh.GetDisplayList: GLuint;
begin
  if FDisplayList=0 then GenerateDisplayList;
  Result:=FDisplayList;
end;

function TMesh.GetIndexCount: Integer;
begin
  Result:=Length(FIndices);
end;

function TMesh.GetIndices(AIndex: Integer): Integer;
begin
  Result:=FIndices[AIndex];
end;

function TMesh.GetVertexCount: Integer;
begin
  Result:=Length(FVertices);
end;

function TMesh.GetVertices(AIndex: Integer): TMeshVertex;
begin
  Result:=FVertices[AIndex];
end;

procedure TMesh.GenerateDisplayList;
var
  I: Integer;
begin
  FDisplayList:=glGenLists(1);
  glNewList(FDisplayList, GL_COMPILE);
  glBegin(GL_TRIANGLES);
  for I:=0 to IndexCount - 1 do
    with Vertices[Indices[I]] do begin
      glNormal3d(n.x, n.y, n.z);
      glTexCoord2d(s, t);
      glVertex3d(x, y, z);
    end;
  glEnd;
  glEndList;
end;

procedure TMesh.InvalidateList;
begin
  if FDisplayList <> 0 then glDeleteLists(FDisplayList, 1);
  FDisplayList:=0;
end;

destructor TMesh.Destroy;
begin
  FreeAndNil(FTexture);
  InvalidateList;
  inherited Destroy;
end;

procedure TMesh.Clear;
begin
  InvalidateList;
  SetLength(FVertices, 0);
  SetLength(FIndices, 0);
end;

procedure TMesh.Scale(Scalar: Double);
var
  I: Integer;
begin
  for I:=0 to VertexCount - 1 do FVertices[I].Scale(Scalar);
end;

procedure TMesh.Scale(XYZ: TVector);
var
  I: Integer;
begin
  for I:=0 to VertexCount - 1 do FVertices[I].Multiply(XYZ);
end;

procedure TMesh.ScaleToHeight(AHeight: Double);
var
  Scal, MinY, MaxY: Double;
  I: Integer;
begin
  InvalidateList;
  if VertexCount < 3 then Exit;
  MinY:=0;
  MaxY:=0;
  for I:=0 to VertexCount - 1 do begin
    if (I=0) or (FVertices[I].y < MinY) then MinY:=FVertices[I].y;
    if (I=0) or (FVertices[I].y > MaxY) then MaxY:=FVertices[I].y;
  end;
  if Equal(MinY, MaxY) then Exit;
  Scal:=AHeight/(MaxY - MinY);
  for I:=0 to VertexCount - 1 do
    FVertices[I].Scale(Scal);
end;

procedure TMesh.Translate(XYZ: TVector);
var
  I: Integer;
begin
  for I:=0 to VertexCount - 1 do FVertices[I].Add(XYZ);
end;

function TMesh.CalcBoundingRadius: Double;
var
  L: Double;
  I: Integer;
begin
  Result:=0;
  for I:=0 to High(FVertices) do begin
    L:=FVertices[I].LengthSq;
    if L > Result then Result:=L;
  end;
  Result:=Sqrt(Result);
end;

function TMesh.CalcBoundingBox: TAABox;
var
  I: Integer;
begin
  if Length(FVertices)=0 then begin
    Result{%H-}.Zero;
    Exit;
  end;
  Result.a:=FVertices[0];
  Result.b:=Result.a;
  for I:=1 to High(FVertices) do Result.Include(FVertices[I]);
end;

function TMesh.AddVertex(const V: TMeshVertex): Integer;
begin
  InvalidateList;
  SetLength(FVertices, Length(FVertices) + 1);
  FVertices[High(FVertices)]:=V;
  Result:=High(FVertices);
end;

function TMesh.AddVertex(const V, N: TVector; S, T: Double): Integer;
var
  MV: TMeshVertex;
begin
  MV.Copy(V);
  MV.n:=N;
  MV.s:=S;
  MV.t:=T;
  Result:=AddVertex(MV);
end;

function TMesh.VertexAt(const V: TVector): Integer;
var
  I: Integer;
begin
  for I:=0 to High(FVertices) do
    if FVertices[I]=V then Exit(I);
  Result:=-1;
end;

function TMesh.MatchingVertex(const V, N: TVector): Integer;
var
  I: Integer;
begin
  for I:=0 to High(FVertices) do
    if (FVertices[I]=V) and (FVertices[I].n=V) then Exit(I);
  Result:=-1;
end;

function TMesh.MatchingVertex(const V, N: TVector; S, T: Double): Integer;
var
  I: Integer;
begin
  for I:=0 to High(FVertices) do
    if (FVertices[I]=V) and (FVertices[I].n=V) and (FVertices[I].s=S) and (FVertices[I].t=T) then Exit(I);
  Result:=-1;
end;

function TMesh.AddFace(A, B, C: Integer): Integer;
begin
  SetLength(FIndices, Length(FIndices) + 3);
  FIndices[Length(FIndices) - 3]:=A;
  FIndices[Length(FIndices) - 2]:=B;
  FIndices[Length(FIndices) - 1]:=C;
  Result:=Length(FIndices) - 3;
end;

function TMesh.RayHitAnyFace(ARay: TRay; const AMtx: TMatrix; out IP: TVector): Boolean;
var
  A, B, C: TVector;
  I: Integer;
begin
  I:=0;
  while I < IndexCount do begin
    A:=AMtx.Transformed(Vertices[Indices[I]]);
    B:=AMtx.Transformed(Vertices[Indices[I + 1]]);
    C:=AMtx.Transformed(Vertices[Indices[I + 2]]);
    if ARay.TriangleHit(A, B, C, IP) then Exit(True);
    Inc(I, 3);
  end;
  Result:=False;
end;

function TMesh.RayHit(ARay: TRay; const AMtx: TMatrix; out IP: TVector): Boolean;
var
  A, B, C: TVector;
  I: Integer;
  BestIP: TVector;
  Dist, BestDist: Double;
begin
  I:=0;
  BestDist:=MaxInt;
  BestIP:=Vector(0, 0, 0);
  Result:=False;
  while I < IndexCount do begin
    A:=AMtx.Transformed(Vertices[Indices[I]]);
    B:=AMtx.Transformed(Vertices[Indices[I + 1]]);
    C:=AMtx.Transformed(Vertices[Indices[I + 2]]);
    if ARay.TriangleHit(A, B, C, IP) then begin
      Dist:=DistanceSq(ARay.o, IP);
      if not Result or (Dist < BestDist) then begin
        BestDist:=Dist;
        BestIP:=IP;
        Result:=True;
      end;
    end;
    Inc(I, 3);
  end;
  IP:=BestIP;
end;

procedure TMesh.SetData(AVertices: array of TMeshVertex;
  AIndices: array of Integer; ATexture: TTexture; AFileName: string);
var
  I: Integer;
begin
  SetLength(FVertices, Length(AVertices));
  SetLength(FIndices, Length(AIndices));
  for I:=0 to High(AVertices) do FVertices[I]:=AVertices[I];
  for I:=0 to High(AIndices) do FIndices[I]:=AIndices[I];
  InvalidateList;
  FreeAndNil(FTexture);
  FTexture:=ATexture;
  FFileName:=AFileName;
end;

end.

