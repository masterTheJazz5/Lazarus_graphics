unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLContext, Forms, Controls, Graphics,
  Dialogs, Menus, ExtCtrls, ComCtrls, ExtDlgs, GL, GLu, Types;

type

  TViewMode = (vmSolidLit, vmSolidUnlit, vmWireframe, vmWireframeOverlay);

  { TVertex }

  TVertex = packed record
    X, Y, Z, NX, NY, NZ, U, V: Single;
  end;

  { TFace }

  TFace = packed record
    A, B, C: TVertex;
  end;

  { TMain }

  TMain = class(TForm)
    MainMenu1: TMainMenu;
    mFileRemoveTexture: TMenuItem;
    mFileBar2: TMenuItem;
    mFileOpenTexture: TMenuItem;
    mViewBar1: TMenuItem;
    mViewReset: TMenuItem;
    mViewSolidUnlit: TMenuItem;
    mViewWireframeOnly: TMenuItem;
    mViewWireframeOverlay: TMenuItem;
    mViewSolidLit: TMenuItem;
    mView: TMenuItem;
    mHelpAbout: TMenuItem;
    mHelp: TMenuItem;
    mFileBar1: TMenuItem;
    mFileExit: TMenuItem;
    mFileOpen: TMenuItem;
    mFile: TMenuItem;
    glc: TOpenGLControl;
    OpenDialog1: TOpenDialog;
    OpenPictureDialog1: TOpenPictureDialog;
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure glcMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure glcMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure glcMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure glcMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure glcPaint(Sender: TObject);
    procedure mFileExitClick(Sender: TObject);
    procedure mFileOpenClick(Sender: TObject);
    procedure mFileOpenTextureClick(Sender: TObject);
    procedure mFileRemoveTextureClick(Sender: TObject);
    procedure mHelpAboutClick(Sender: TObject);
    procedure mViewSolidLitClick(Sender: TObject);
    procedure mViewSolidUnlitClick(Sender: TObject);
    procedure mViewWireframeOnlyClick(Sender: TObject);
    procedure mViewWireframeOverlayClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    Faces: array of TFace;
    MinX, MinY, MinZ, MaxX, MaxY, MaxZ: Single;
    Distance, OffsetX, OffsetY, RotateX, RotateY: Single;
    MX, MY: Integer;
    Rotating, Panning: Boolean;
    ViewMode: TViewMode;
    Texture: TGLuint;
    procedure LoadJTF(Path: string);
    procedure Reset;
    procedure RemoveTexture;
    procedure Render;
  public
    { public declarations }
  end;

var
  Main: TMain;

implementation

{$R *.lfm}

{ TMain }

procedure TMain.mFileExitClick(Sender: TObject);
begin
  Close;
end;

procedure TMain.mFileOpenClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
    LoadJTF(OpenDialog1.FileName);
end;

procedure TMain.mFileOpenTextureClick(Sender: TObject);
var
  Pic: TPicture;
  PNG: TPortableNetworkGraphic;
  RGB: array of Byte;
  X, Y: Integer;
begin
  if OpenPictureDialog1.Execute then begin
    Pic:=nil;
    try
      Pic:=TPicture.Create;
      Pic.LoadFromFile(OpenPictureDialog1.FileName);
    except
      FreeAndNil(Pic);
      ShowMessage('Failed to open picture file ' + OpenPictureDialog1.FileName);
      Exit;
    end;
    PNG:=TPortableNetworkGraphic.Create;
    PNG.Width:=Pic.Width;
    PNG.Height:=Pic.Height;
    PNG.Canvas.Draw(0, 0, Pic.Graphic);
    SetLength(RGB, PNG.Width*PNG.Height*3);
    for Y:=0 to PNG.Height - 1 do
      for X:=0 to PNG.Width - 1 do begin
        RGB[(Y*PNG.Width + X)*3]:=PNG.Canvas.Colors[X, Y].Red shr 8;
        RGB[(Y*PNG.Width + X)*3 + 1]:=PNG.Canvas.Colors[X, Y].Green shr 8;
        RGB[(Y*PNG.Width + X)*3 + 2]:=PNG.Canvas.Colors[X, Y].Blue shr 8;
      end;
    RemoveTexture;
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, @Texture);
    glBindTexture(GL_TEXTURE_2D, Texture);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, PNG.Width, PNG.Height, GL_RGB, GL_UNSIGNED_BYTE, @RGB[0]);
    FreeAndNil(PNG);
    FreeAndNil(Pic);
  end;
end;

procedure TMain.mFileRemoveTextureClick(Sender: TObject);
begin
  RemoveTexture;
end;

procedure TMain.glcPaint(Sender: TObject);
begin
  if not glc.MakeCurrent then Exit;
  Render;
  glc.SwapBuffers;
end;

procedure TMain.glcMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if Rotating then begin
    RotateX += Y - MY;
    RotateY += X - MX;
  end;
  if Panning then begin
    OffsetY += (Y - MY)/glc.Height;
    OffsetX += (MX - X)/glc.Height;
  end;
  MX:=X;
  MY:=Y;
end;

procedure TMain.glcMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbLeft then
    Rotating:=False
  else
    Panning:=False;
end;

procedure TMain.glcMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  Distance -= WheelDelta*0.001;
  Handled:=True;
end;

procedure TMain.glcMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbLeft then
    Rotating:=True
  else
    Panning:=True;
  MX:=X;
  MY:=Y;
end;

procedure TMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  RemoveTexture;
end;

procedure TMain.mHelpAboutClick(Sender: TObject);
begin
  ShowMessage('JTF (Just Triangle Faces) File Viewer by Kostas "Bad Sector" Michalopoulos');
end;

procedure TMain.mViewSolidLitClick(Sender: TObject);
begin
  ViewMode:=vmSolidLit;
end;

procedure TMain.mViewSolidUnlitClick(Sender: TObject);
begin
  ViewMode:=vmSolidUnlit;
end;

procedure TMain.mViewWireframeOnlyClick(Sender: TObject);
begin
  ViewMode:=vmWireframe;
end;

procedure TMain.mViewWireframeOverlayClick(Sender: TObject);
begin
  ViewMode:=vmWireframeOverlay;
end;

procedure TMain.Timer1Timer(Sender: TObject);
begin
  {$IFDEF Darwin}
  glc.Invalidate;
  {$ELSE}
  glcPaint(nil);
  {$ENDIF}
end;

procedure TMain.LoadJTF(Path: string);
var
  F: TFileStream;
  I: Integer;
begin
  F:=nil;
  try
    F:=TFileStream.Create(Path, fmOpenRead);
  except
    StatusBar1.SimpleText:='Failed to load ' + Path;
    ShowMessage('Failed to open ' + Path);
    Exit;
  end;
  try
    if F.ReadByte <> 74 then raise Exception.Create('Invalid file format');
    if F.ReadByte <> 84 then raise Exception.Create('Invalid file format');
    if F.ReadByte <> 70 then raise Exception.Create('Invalid file format');
    if F.ReadByte <> 33 then raise Exception.Create('Invalid file format');
    if F.ReadDWord <> 0 then raise Exception.Create('Unknown vertex format');
    SetLength(Faces, F.ReadDWord);
    for I:=0 to High(Faces) do
      F.Read(Faces[I], SizeOf(TFace));
    StatusBar1.SimpleText:='Loaded ' + IntToStr(Length(Faces)) + ' triangles from ' + Path;
  except
    StatusBar1.SimpleText:='Failed to load ' + Path;
    ShowMessage('Error loading ' + Path);
  end;
  FreeAndNil(F);
  Reset;
end;

procedure TMain.Reset;

  function VLen(const V: TVertex): Double;
  begin
    Result:=Sqrt(Sqr(V.X) + Sqr(V.Y) + Sqr(V.Z));
  end;

  procedure Consider(const V: TVertex);
  var
    D: Double;
  begin
    D:=VLen(V);
    if D > Distance then Distance:=D;
    if MinX > V.X then MinX:=V.X;
    if MinY > V.Y then MinY:=V.Y;
    if MinZ > V.Z then MinZ:=V.Z;
    if MaxX < V.X then MaxX:=V.X;
    if MaxY < V.Y then MaxY:=V.Y;
    if MaxZ < V.Z then MaxZ:=V.Z;
  end;

var
  I: Integer;
begin
  Distance:=0;
  MinX:=MaxInt;
  MinY:=MaxInt;
  MinZ:=MaxInt;
  MaxX:=-MaxInt;
  MaxY:=-MaxInt;
  MaxZ:=-MaxInt;
  for I:=0 to High(Faces) do with Faces[I] do begin
    Consider(A);
    Consider(B);
    Consider(C);
  end;
  Distance *= 1.2;
  OffsetX:=(MinX + MaxX)/2;
  OffsetY:=(MinY + MaxY)/2;
  RotateX:=0;
  RotateY:=0;
end;

procedure TMain.RemoveTexture;
begin
  if Texture <> 0 then begin
    glDeleteTextures(1, @Texture);
    Texture:=0;
  end;
end;

procedure TMain.Render;

  procedure RenderMesh;
  var
    I: Integer;
  begin
    glBegin(GL_TRIANGLES);
    for I:=0 to High(Faces) do with Faces[I] do begin
      glNormal3f(A.NX, A.NY, A.NZ);
      glTexCoord2f(A.U, A.V);
      glVertex3f(A.X, A.Y, A.Z);
      glNormal3f(B.NX, B.NY, B.NZ);
      glTexCoord2f(B.U, B.V);
      glVertex3f(B.X, B.Y, B.Z);
      glNormal3f(C.NX, C.NY, C.NZ);
      glTexCoord2f(C.U, C.V);
      glVertex3f(C.X, C.Y, C.Z);
    end;
    glEnd();
  end;

begin
  glClearColor(0.1, 0.2, 0.3, 1.0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(50.0, glc.Width/glc.Height, 0.01, 1000.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(-OffsetX, -OffsetY, -Distance);
  glRotatef(RotateX, 1, 0, 0);
  glRotatef(RotateY, 0, 1, 0);
  if ViewMode in [vmSolidLit, vmWireframeOverlay] then begin
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
  end else begin
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
  end;
  if (Texture <> 0) and (ViewMode in [vmSolidLit, vmSolidUnlit, vmWireframeOverlay]) then begin
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, Texture);
  end else begin
    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
  end;
  glEnable(GL_DEPTH_TEST);
  if ViewMode in [vmSolidLit, vmSolidUnlit, vmWireframeOverlay] then begin
    glColor3f(1.0, 1.0, 1.0);
    RenderMesh;
  end;
  if ViewMode in [vmWireframe, vmWireframeOverlay] then begin
    glEnable(GL_POLYGON_OFFSET_LINE);
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glPolygonOffset(-0.75, 0.0);
    if ViewMode=vmWireframeOverlay then glColor3f(0.1, 0.1, 0.1);
    RenderMesh;
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_POLYGON_OFFSET_LINE);
  end;
end;

end.

