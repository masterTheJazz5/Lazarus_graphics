unit Textures;

{$mode objfpc}{$H+}

interface

uses
  {GlobalDefines, }Classes, SysUtils, GL, GLu, FGL, Graphics, FPimage, Maths, MiscUtils;

type

  { TExtColor }

  PExtColor = ^TExtColor;
  TExtColor = packed object
  private
    function GetBrightness: Double;
    function GetHue: Double;
    function GetSaturation: Double;
    procedure SetBrightness(AValue: Double);
    procedure SetHue(AValue: Double);
    procedure SetSaturation(AValue: Double);
  public
    r, g, b, a: Double;
    function ToColor: TColor;
    procedure FromColor(AColor: TColor; Alpha: Double=1.0);
    procedure FromVector(AVector: TVector; Alpha: Double=1.0);
    procedure FromString(S: string);
    function ToVector: TVector; inline;
    function ToString: string; inline;
    procedure ToHSB(out Hue, Saturation, Brightness: Double);
    procedure FromHSB(Hue, Saturation, Brightness: Double);
    procedure Zero; inline;
    procedure Black; inline;
    procedure Add(const V: TExtColor); inline;
    procedure Subtract(const V: TExtColor); inline;
    procedure Multiply(const V: TExtColor); inline;
    procedure Scale(S: Double); inline;
    procedure Invert; inline;
    property Hue: Double read GetHue write SetHue;
    property Saturation: Double read GetSaturation write SetSaturation;
    property Brightness: Double read GetBrightness write SetBrightness;
  end;

  { TTexture }

  TTexture = class
  private
    FGLName: GLuint;
    FHasAlpha: Boolean;
    FImage: TRasterImage;
    FFPImage: TFPMemoryImage;
    FFileName: string;
    FName: string;
    FUsed: Boolean;
    FTextureFileAge: LongInt;
    function GetFPImage: TFPMemoryImage;
    function GetGLName: GLuint;
    function GetHeight: Integer;
    function GetImage: TRasterImage;
    function GetWidth: Integer;
    procedure UploadTexture;
  public
    constructor Create(AFileName: string);
    destructor Destroy; override;
    procedure Reload;
    procedure ReloadIfNewer;
    procedure Release;
    procedure SetFromPixels(AWidth, AHeight: Integer; Pixels: array of Byte; AHasAlpha: Boolean);

    function Sample(S, T: Double): TExtColor; inline;

    property Used: Boolean read FUsed write FUsed;
    property FileName: string read FFileName;
    property Name: string read FName write FName;
    property GLName: GLuint read GetGLName;
    property Image: TRasterImage read GetImage;
    property FPImage: TFPMemoryImage read GetFPImage;
    property Width: Integer read GetWidth;
    property Height: Integer read GetHeight;
    property HasAlpha: Boolean read FHasAlpha;
  end;

  TTextureList = specialize TFPGObjectList<TTexture>;

  { TTextureRegistry }

  TTextureRegistry = class(TComponent)
  private
    FList: TTextureList;
    FDirs: array of string;
    FNeedSort: Boolean;
    procedure CheckSorting;
    function GetDirectories(AIndex: Integer): string; inline;
    function GetDirectoryCount: Integer; inline;
    function GetTextures(AIndex: Integer): TTexture; inline;
    function GetTextureCount: Integer; inline;

    procedure Consider(TexturePath: string);
    procedure ScanDirectory(APath: string);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Clear;
    procedure ResetUsedFlag;

    procedure Add(ATexture: TTexture);
    procedure Remove(ATexture: TTexture);
    function IndexOf(ATexture: TTexture): Integer;
    function IndexOf(AName: string): Integer;
    function Find(AName: string): TTexture;
    function FindOrDefault(AName: string): TTexture;
    function GetDefaultTexture: TTexture;

    procedure AddDirectory(APath: string);
    procedure RemoveDirectory(APath: string);
    procedure RemoveMissingFiles;
    procedure RescanDirectories;

    property Textures[AIndex: Integer]: TTexture read GetTextures;
    property TextureCount: Integer read GetTextureCount;
    property Directories[AIndex: Integer]: string read GetDirectories;
    property DirectoryCount: Integer read GetDirectoryCount;
  end;

const
  TransparentColor: TExtColor = (r:0; g:0; b:0; a:0);
  BlackColor: TExtColor = (r:0; g:0; b:0; a:1);
  WhiteColor: TExtColor = (r:1; g:1; b:1; a:1);

function ExtColor(r, g, b: Double; a: Double=1.0): TExtColor; inline;
function ToExtColor(const Color: TColor): TExtColor; inline;
operator=(const A, B: TExtColor): Boolean; inline;

procedure Register;

implementation

uses
  LResources, Events, Logger, LCLIntf;

procedure Register;
begin
  {$I textures_icon.lrs}
  RegisterComponents('RTTK', [TTextureRegistry]);
end;

function CompareTexturesFunc(const A, B: TTexture): Integer;
begin
  Result:=CompareStr(A.Name, B.Name);
end;

function ExtColor(r, g, b: Double; a: Double): TExtColor;
begin
  Result.r:=r;
  Result.g:=g;
  Result.b:=b;
  Result.a:=a;
end;

function ToExtColor(const Color: TColor): TExtColor;
begin
  Result{%H-}.FromColor(Color);
end;

operator=(const A, B: TExtColor): Boolean;
begin
  Result:=(A.R=B.R) and (A.G=B.G) and (A.B=B.B) and (A.A=B.A);
end;

{ TExtColor }

function TExtColor.GetBrightness: Double;
var
  H, S: Double;
begin
  ToHSB(H, S, Result);
end;

function TExtColor.GetHue: Double;
var
  S, B_: Double;
begin
  ToHSB(Result, S, B_);
end;

function TExtColor.GetSaturation: Double;
var
  H, B_: Double;
begin
  ToHSB(H, Result, B_);
end;

procedure TExtColor.SetBrightness(AValue: Double);
var
  H, S, B_: Double;
begin
  ToHSB(H, S, B_);
  B_:=AValue;
  FromHSB(H, S, B_);
end;

procedure TExtColor.SetHue(AValue: Double);
var
  H, S, B_: Double;
begin
  ToHSB(H, S, B_);
  H:=AValue;
  FromHSB(H, S, B_);
end;

procedure TExtColor.SetSaturation(AValue: Double);
var
  H, S, B_: Double;
begin
  ToHSB(H, S, B_);
  S:=AValue;
  FromHSB(H, S, B_);
end;

function TExtColor.ToColor: TColor;
begin
  Result:=RGBToColor(Round(r*255), Round(g*255), Round(b*255));
end;

procedure TExtColor.FromColor(AColor: TColor; Alpha: Double);
begin
  AColor:=TColor(ColorToRGB(AColor));
  r:=Red(AColor)/255;
  g:=Green(AColor)/255;
  b:=Blue(AColor)/255;
  a:=Alpha;
end;

procedure TExtColor.FromVector(AVector: TVector; Alpha: Double);
begin
  r:=AVector.x;
  g:=AVector.y;
  b:=AVector.z;
  a:=Alpha;
end;

procedure TExtColor.FromString(S: string);
var
  Parts: TStringArray;
begin
  SplitString(S, ',', Parts);
  if Length(Parts) in [3, 4] then begin
    r:=StrToFloatDef(Parts[0], 0, NormalFormatSettings);
    g:=StrToFloatDef(Parts[1], 0, NormalFormatSettings);
    b:=StrToFloatDef(Parts[2], 0, NormalFormatSettings);
    if Length(Parts)=4 then
      a:=StrToFloatDef(Parts[3], 1, NormalFormatSettings)
    else
      a:=1;
  end;
end;

function TExtColor.ToVector: TVector;
begin
  Result:=Vector(r, g, b);
end;

function TExtColor.ToString: string;
begin
  Result:=FloatToStr(r, NormalFormatSettings) + ',' +
          FloatToStr(g, NormalFormatSettings) + ',' +
          FloatToStr(b, NormalFormatSettings);
  if a <> 1 then Result += ',' + FloatToStr(a, NormalFormatSettings);
end;

procedure TExtColor.ToHSB(out Hue, Saturation, Brightness: Double);
var
  CMin, CDelta: Double;
begin
  Brightness:=Max(r, g, b);
  CMin:=Min(r, g, b);
  CDelta:=Brightness - CMin;
  Hue:=0;
  if Brightness=0 then Saturation:=0 else Saturation:=CDelta/Brightness;
  if CDelta <> 0 then begin
    if Brightness=r then
      Hue:=(g - b)/CDelta
    else if Brightness=g then
      Hue:=2 + (b - r)/CDelta
    else
      Hue:=4 + (r - g)/CDelta;
    Hue *= 60;
    if Hue < 0 then Hue += 360;
  end;
end;

procedure TExtColor.FromHSB(Hue, Saturation, Brightness: Double);
var
  IHue: Integer;
  F, P, Q, T: Double;
begin
  Hue:=Clamp(Hue, 0, 360);
  Saturation:=Clamp(Saturation);
  if Saturation=0 then begin
    r:=Clamp(Brightness);
    g:=Clamp(Brightness);
    b:=Clamp(Brightness);
    Exit;
  end;
  if Hue=360 then Hue:=0;
  Hue /= 60.0;
  IHue:=Trunc(Hue);
  F:=Hue - IHue;
  P:=Brightness*(1.0 - Saturation);
  Q:=Brightness*(1.0 - Saturation*F);
  T:=Brightness*(1.0 - Saturation*(1.0 - F));
  case IHue of
    0: begin
      r:=Brightness;
      g:=T;
      b:=P;
    end;
    1: begin
      r:=Q;
      g:=Brightness;
      b:=P;
    end;
    2: begin
      r:=P;
      g:=Brightness;
      b:=T;
    end;
    3: begin
      r:=P;
      g:=Q;
      b:=Brightness;
    end;
    4: begin
      r:=T;
      g:=P;
      b:=Brightness;
    end;
    else begin
      r:=Brightness;
      g:=P;
      b:=Q;
    end;
  end;
  r:=Clamp(r);
  g:=Clamp(g);
  b:=Clamp(b);
end;

procedure TExtColor.Zero;
begin
  r:=0;
  g:=0;
  b:=0;
  a:=0;
end;

procedure TExtColor.Black;
begin
  r:=0;
  g:=0;
  b:=0;
  a:=1;
end;

procedure TExtColor.Add(const V: TExtColor);
begin
  r:=r + V.r;
  g:=g + V.g;
  b:=b + V.b;
  a:=a + V.a;
end;

procedure TExtColor.Subtract(const V: TExtColor);
begin
  r:=Max(0, r - V.r);
  g:=Max(0, g - V.g);
  b:=Max(0, b - V.b);
  a:=Max(0, a - V.a);
end;

procedure TExtColor.Multiply(const V: TExtColor);
begin
  r:=r * V.r;
  g:=g * V.g;
  b:=b * V.b;
  a:=a * V.a;
end;

procedure TExtColor.Scale(S: Double);
begin
  r:=r * S;
  g:=g * S;
  b:=b * S;
  a:=a * S;
end;

procedure TExtColor.Invert;
begin
  r:=Max(0, 1-r);
  g:=Max(0, 1-g);
  b:=Max(0, 1-b);
  a:=Max(0, 1-a);
end;

{ TTextureRegistry }

function TTextureRegistry.GetTextureCount: Integer;
begin
  Result:=FList.Count;
end;

procedure TTextureRegistry.Consider(TexturePath: string);
var
  Texture: TTexture;
  I: Integer;
begin
  for I:=0 to TextureCount - 1 do
    if Textures[I].FileName=TexturePath then begin
      Textures[I].ReloadIfNewer;
      Exit;
    end;

  try
    Texture:=TTexture.Create(TexturePath);
  except
    Exit;
  end;

  Add(Texture);
end;

procedure TTextureRegistry.ScanDirectory(APath: string);
var
  r: LongInt;
  SR: TSearchRec;
begin
  r:=FindFirst(APath + DirectorySeparator + AllFilesMask, faAnyFile - faVolumeId, SR);
  while r=0 do begin
    if (SR.Attr and faDirectory)=0 then Consider(APath + DirectorySeparator + SR.Name);
    r:=FindNext(SR);
    if r <> 0 then FindClose(SR);
  end;
end;

procedure TTextureRegistry.CheckSorting;
begin
  if not FNeedSort then Exit;
  FList.Sort(@CompareTexturesFunc);
  FNeedSort:=False;
end;

function TTextureRegistry.GetDirectories(AIndex: Integer): string;
begin
  Result:=FDirs[AIndex];
end;

function TTextureRegistry.GetDirectoryCount: Integer;
begin
  Result:=Length(FDirs);
end;

function TTextureRegistry.GetTextures(AIndex: Integer): TTexture;
begin
  CheckSorting;
  Result:=FList[AIndex];
end;

constructor TTextureRegistry.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FList:=TTextureList.Create;
end;

destructor TTextureRegistry.Destroy;
begin
  FreeAndNil(FList);
  inherited Destroy;
end;

procedure TTextureRegistry.Clear;
begin
  FList.Clear;
end;

procedure TTextureRegistry.ResetUsedFlag;
var
  i: Integer;
begin
  for i:=0 to FList.Count - 1 do FList[i].Used:=False;
end;

procedure TTextureRegistry.Add(ATexture: TTexture);
begin
  FList.Add(ATexture);
  FNeedSort:=True;
end;

procedure TTextureRegistry.Remove(ATexture: TTexture);
begin
  if ATexture.Used then CallEventHandlers(Self, ATexture, nil, 'textureregistry-release-texture');
  FList.Remove(ATexture);
end;

function TTextureRegistry.IndexOf(ATexture: TTexture): Integer;
begin
  Result:=FList.IndexOf(ATexture);
end;

function TTextureRegistry.IndexOf(AName: string): Integer;
var
  i: Integer;
begin
  Result:=-1;
  for i:=0 to FList.Count - 1 do
    if FList[i].Name=AName then Exit(i);
end;

function TTextureRegistry.Find(AName: string): TTexture;
var
  i: Integer;
begin
  Result:=nil;
  for i:=0 to FList.Count - 1 do
    if FList[i].Name=AName then begin
      FList[i].Used:=True;
      Exit(FList[i]);
    end;
end;

function TTextureRegistry.FindOrDefault(AName: string): TTexture;
var
  i: Integer;
begin
  for i:=0 to FList.Count - 1 do
    if FList[i].Name=AName then begin
      FList[i].Used:=True;
      Exit(FList[i]);
    end;
  Result:=GetDefaultTexture;
end;

function TTextureRegistry.GetDefaultTexture: TTexture;
begin
  if TextureCount=0 then Result:=nil else Result:=Textures[0];
end;

procedure TTextureRegistry.AddDirectory(APath: string);
begin
  SetLength(FDirs, Length(FDirs) + 1);
  FDirs[High(FDirs)]:=APath;
  ScanDirectory(APath);
  CallEventHandlers(Self, nil, nil, 'textures-updated');
end;

procedure TTextureRegistry.RemoveDirectory(APath: string);
var
  I, J: Integer;
  ToRemove: array of TTexture;
begin
  SetLength(ToRemove, 0);
  for I:=0 to High(FDirs) do if FDirs[I]=APath then begin
    for J:=0 to TextureCount - 1 do
      if LeftStr(Textures[J].FileName, Length(APath))=APath then begin
        SetLength(ToRemove, Length(ToRemove) + 1);
        ToRemove[High(ToRemove)]:=Textures[J];
      end;
    for J:=I to High(FDirs) - 1 do FDirs[J]:=FDirs[J - 1];
    SetLength(FDirs, High(FDirs));
    Break;
  end;

  for I:=0 to High(ToRemove) do Remove(ToRemove[I]);

  CallEventHandlers(Self, nil, nil, 'textures-updated');
end;

procedure TTextureRegistry.RemoveMissingFiles;
var
  I: Integer;
  ToRemove: array of TTexture;
begin
  SetLength(ToRemove, 0);
  for I:=0 to TextureCount - 1 do
    if not FileExists(Textures[I].FileName) then begin
      SetLength(ToRemove, Length(ToRemove) + 1);
      ToRemove[High(ToRemove)]:=Textures[I];
    end;
  for I:=0 to High(ToRemove) do Remove(ToRemove[I]);
  CallEventHandlers(Self, nil, nil, 'textures-updated');
end;

procedure TTextureRegistry.RescanDirectories;
var
  I: Integer;
begin
  for I:=0 to High(FDirs) do
    ScanDirectory(FDirs[I]);
  CallEventHandlers(Self, nil, nil, 'textures-updated');
end;

{ TTexture }

function TTexture.GetGLName: GLuint;
begin
  if not Assigned(FPImage) then Exit(0);
  if FGLName=0 then UploadTexture;
  Result:=FGLName;
end;

function TTexture.GetHeight: Integer;
begin
  Result:=FPImage.Height;
end;

function TTexture.GetFPImage: TFPMemoryImage;
begin
  if not Assigned(FFPImage) then Reload;
  Result:=FFPImage;
end;

function TTexture.GetImage: TRasterImage;
begin
  if not Assigned(FPImage) then Exit(nil);
  if not Assigned(FImage) then FImage:=CreateBitmapFromFPImage(FFPImage);
  Result:=FImage;
end;

function TTexture.GetWidth: Integer;
begin
  Result:=FPImage.Width;
end;

procedure TTexture.UploadTexture;
var
  Pixels: array of Byte;
  c, x, y: Integer;
  Color: TFPColor;
begin
  if FGLName=0 then glGenTextures(1, @FGLName);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glBindTexture(GL_TEXTURE_2D, FGLName);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  for y:=0 to FFPImage.Height - 1 do
    for x:=0 to FFPImage.Width - 1 do
      if FFPImage.Colors[x, y].alpha < 65535 then begin
        FHasAlpha:=True;
        Break;
      end;
  if HasAlpha then begin
    SetLength(Pixels, FFPImage.Width*FFPImage.Height*4);
    c:=0;
    for y:=0 to FFPImage.Height - 1 do
      for x:=0 to FFPImage.Width - 1 do begin
        Color:=FFPImage.Colors[x, y];
        Pixels[c]:=Color.Red div 256; Inc(c);
        Pixels[c]:=Color.Green div 256; Inc(c);
        Pixels[c]:=Color.Blue div 256; Inc(c);
        Pixels[c]:=Color.Alpha div 256; Inc(c);
      end;
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, FFPImage.Width, FFPImage.Height, GL_RGBA, GL_UNSIGNED_BYTE, @Pixels[0]);
  end else begin
    SetLength(Pixels, FFPImage.Width*FFPImage.Height*3);
    c:=0;
    for y:=0 to FFPImage.Height - 1 do
      for x:=0 to FFPImage.Width - 1 do begin
        Color:=FFPImage.Colors[x, y];
        Pixels[c]:=Color.Red div 256; Inc(c);
        Pixels[c]:=Color.Green div 256; Inc(c);
        Pixels[c]:=Color.Blue div 256; Inc(c);
      end;
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, FFPImage.Width, FFPImage.Height, GL_RGB, GL_UNSIGNED_BYTE, @Pixels[0]);
  end;
end;

constructor TTexture.Create(AFileName: string);
begin
  FFileName:=AFileName;
  FName:=ExtractFileName(AFileName);
  FName:=Copy(FName, 1, Length(FName) - Length(ExtractFileExt(FName)));
  if (AFileName <> '') and (not FileExists(AFileName)) then raise EInOutError.Create('Texture file ' + AFileName + ' does not exist');
end;

destructor TTexture.Destroy;
begin
  Release;
  inherited Destroy;
end;

procedure TTexture.Reload;
begin
  Release;
  FFPImage:=TFPMemoryImage.Create(0, 0);
  try
    Log('Loading ' + FFileName);
    FFPImage.LoadFromFile(FFileName);
    FTextureFileAge:=FileAge(FFileName);
    Log('Loaded ' + FFileName);
  except
    FFPImage.Free;
    FFPImage:=TFPMemoryImage.Create(16, 16);
    FTextureFileAge:=0;
  end;
end;

procedure TTexture.ReloadIfNewer;
begin
  if not Assigned(FFPImage) then Exit;
  if FileAge(FFileName) <> FTextureFileAge then begin
    Log('Reloading newer ' + FFileName);
    Reload;
  end;
end;

procedure TTexture.Release;
begin
  FreeAndNil(FImage);
  FreeAndNil(FFPImage);
  if FGLName <> 0 then glDeleteTextures(1, @FGLName);
  FGLName:=0;
end;

procedure TTexture.SetFromPixels(AWidth, AHeight: Integer;
  Pixels: array of Byte; AHasAlpha: Boolean);
var
  x, y, c: Integer;
begin
  Release;
  FHasAlpha:=AHasAlpha;
  c:=0;
  FFPImage:=TFPMemoryImage.Create(AWidth, AHeight);
  for y:=0 to AHeight - 1 do
    for x:=0 to AWidth - 1 do begin
      FFPImage.Colors[x, y]:=FPColor(Pixels[c] shl 8, Pixels[c + 1] shl 8, Pixels[c + 2] shl 8, Pixels[c + 3] shl 8);
      Inc(c, 4);
    end;
end;

function TTexture.Sample(S, T: Double): TExtColor;
var
  C: TFPColor;
  x, y: Integer;
begin
  x:=Round(S*Width) mod Width;
  y:=Round(T*Height) mod Height;
  if x < 0 then x:=Width + x;
  if y < 0 then y:=Height + y;
  C:=FPImage.Colors[x, y];
  Result:=ExtColor(C.red/65535, C.green/65535, C.blue/65535, C.alpha/65535);
end;

end.

