{===============================================================================
  ChunkIO
  Copyright (C) 2011 Kostas Michalopoulos

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

     1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

     2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

     3. This notice may not be removed or altered from any source
     distribution.

  Kostas Michalopoulos badsector@runtimelegend.com
===============================================================================}

unit ChunkIO;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  UnknownChunkSize = $FFFFFFFF;
  ChunkEndID = 0;

type
  TChunkIDString = string[4];

  TChunkInfo = record
  end;

  { TChunkIO }

  TChunkIO = class
  private
    ChunkStack: array of Int64;
    ChunkStackPos: Integer;
    FStream: TStream;
  public
    constructor Create;

    procedure BeginChunk(ID: Cardinal);
    procedure WriteBoolean(ID: Cardinal; Value: Boolean);
    procedure WriteByte(ID: Cardinal; Value: Byte);
    procedure WriteShortInt(ID: Cardinal; Value: ShortInt);
    procedure WriteWord(ID: Cardinal; Value: Word);
    procedure WriteSmallInt(ID: Cardinal; Value: SmallInt);
    procedure WriteCardinal(ID, Value: Cardinal);
    procedure WriteInteger(ID: Cardinal; Value: Integer);
    procedure WriteQWord(ID: Cardinal; Value: QWord);
    procedure WriteInt64(ID: Cardinal; Value: Int64);
    procedure WriteSingle(ID: Cardinal; Value: Single);
    procedure WriteDouble(ID: Cardinal; Value: Double);
    procedure WriteExtended(ID: Cardinal; Value: Extended);
    procedure WriteString(ID: Cardinal; Value: ansistring);
    procedure WriteBuffer(ID: Cardinal; const Data; Size: Cardinal);
    procedure EndChunk(FixSize: Boolean=True);

    function NextChunk(out ID, Size: Cardinal): Boolean;
    procedure SkipChunk(Size: Cardinal);
    function ReadBoolean: Boolean;
    function ReadByte: Byte;
    function ReadShortInt: ShortInt;
    function ReadWord: Word;
    function ReadSmallInt: SmallInt;
    function ReadCardinal: Cardinal;
    function ReadInteger: Integer;
    function ReadQWord: QWord;
    function ReadInt64: Int64;
    function ReadSingle: Single;
    function ReadDouble: Double;
    function ReadExtended: Extended;
    function ReadString: ansistring;
    function ReadBuffer(Size: Cardinal): Pointer;
    procedure ReadBuffer(Size: Cardinal; out Data);

    property Stream: TStream read FStream write FStream;
  end;

function ChunkID(const StringID: TChunkIDString): Cardinal; inline;
function ChunkIDStr(ID: Cardinal): TChunkIDString; inline;

implementation

function ChunkID(const StringID: TChunkIDString): Cardinal; inline;
begin
  Result:=(Byte(StringID[4]) shl 24) or
          (Byte(StringID[3]) shl 16) or
          (Byte(StringID[2]) shl 8) or
          Byte(StringID[1]);
end;

function ChunkIDStr(ID: Cardinal): TChunkIDString;
begin
  Result[0]:=#4;
  Result[4]:=Chr((ID shr 24) and $FF);
  Result[3]:=Chr((ID shr 16) and $FF);
  Result[2]:=Chr((ID shr 8) and $FF);
  Result[1]:=Chr(ID and $FF);
end;

{ TChunkIO }

constructor TChunkIO.Create;
begin
end;

procedure TChunkIO.BeginChunk(ID: Cardinal);
begin
  if ChunkStackPos=Length(ChunkStack) then SetLength(ChunkStack, ChunkStackPos + 16);
  FStream.WriteDWord(ID);
  ChunkStack[ChunkStackPos]:=FStream.Position;
  Inc(ChunkStackPos);
  FStream.WriteDWord(UnknownChunkSize);
end;

procedure TChunkIO.WriteBoolean(ID: Cardinal; Value: Boolean);
begin
  if Value then WriteByte(ID, 1) else WriteByte(ID, 0);
end;

procedure TChunkIO.WriteByte(ID: Cardinal; Value: Byte);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(1);
  FStream.WriteByte(Value);
end;

procedure TChunkIO.WriteShortInt(ID: Cardinal; Value: ShortInt);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(1);
  FStream.WriteByte(PByte(@Value)^);
end;

procedure TChunkIO.WriteWord(ID: Cardinal; Value: Word);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(2);
  FStream.WriteWord(Value);
end;

procedure TChunkIO.WriteSmallInt(ID: Cardinal; Value: SmallInt);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(2);
  FStream.WriteWord(PWord(@Value)^);
end;

procedure TChunkIO.WriteCardinal(ID, Value: Cardinal);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(4);
  FStream.WriteDWord(Value);
end;

procedure TChunkIO.WriteInteger(ID: Cardinal; Value: Integer);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(4);
  FStream.WriteDWord(PCardinal(@Value)^);
end;

procedure TChunkIO.WriteQWord(ID: Cardinal; Value: QWord);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(8);
  FStream.WriteQWord(Value);
end;

procedure TChunkIO.WriteInt64(ID: Cardinal; Value: Int64);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(8);
  FStream.WriteQWord(PQWord(@Value)^);
end;

procedure TChunkIO.WriteSingle(ID: Cardinal; Value: Single);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(4);
  FStream.Write(Value, 4);
end;

procedure TChunkIO.WriteDouble(ID: Cardinal; Value: Double);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(8);
  FStream.Write(Value, 8);
end;

procedure TChunkIO.WriteExtended(ID: Cardinal; Value: Extended);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(10);
  FStream.Write(Value, 10);
end;

procedure TChunkIO.WriteString(ID: Cardinal; Value: ansistring);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(4 + Length(Value));
  FStream.WriteDWord(Length(Value));
  FStream.Write(Pointer(Value)^, Length(Value));
end;

procedure TChunkIO.WriteBuffer(ID: Cardinal; const Data; Size: Cardinal);
begin
  FStream.WriteDWord(ID);
  FStream.WriteDWord(Size);
  FStream.Write(Data, LongInt(Size));
end;

procedure TChunkIO.EndChunk(FixSize: Boolean);
var
  SavePos: Int64;
begin
  FStream.WriteDWord(ChunkEndID);
  Dec(ChunkStackPos);
  if not FixSize then Exit;
  try
    SavePos:=FStream.Position;
    FStream.Position:=ChunkStack[ChunkStackPos];
    FStream.WriteDWord(SavePos - FStream.Position - 4);
    FStream.Position:=SavePos;
  except
  end;
end;

function TChunkIO.NextChunk(out ID, Size: Cardinal): Boolean;
begin
  if FStream.Position >= FStream.Size then Exit(False);
  ID:=FStream.ReadDWord;
  if ID=ChunkEndID then Exit(False);
  Size:=FStream.ReadDWord;
  Result:=True;
end;

procedure TChunkIO.SkipChunk(Size: Cardinal);
var
  Tmp1, Tmp2: Cardinal;
begin
  if Size=$FFFFFFFF then begin
    while NextChunk(Tmp1, Tmp2) do SkipChunk(Tmp2);
  end else begin
    try
      FStream.Seek(Size, soCurrent);
    except
      on EStreamError do for Tmp1:=1 to Size do FStream.ReadByte;
    end;
  end;
end;

function TChunkIO.ReadBoolean: Boolean;
begin
  Result:=FStream.ReadByte <> 0;
end;

function TChunkIO.ReadByte: Byte;
begin
  FStream.Read(Result, 1);
end;

function TChunkIO.ReadShortInt: ShortInt;
begin
  FStream.Read(Result, 1);
end;

function TChunkIO.ReadWord: Word;
begin
  FStream.Read(Result, 2);
end;

function TChunkIO.ReadSmallInt: SmallInt;
begin
  FStream.Read(Result, 2);
end;

function TChunkIO.ReadCardinal: Cardinal;
begin
  FStream.Read(Result, 4);
end;

function TChunkIO.ReadInteger: Integer;
begin
  FStream.Read(Result, 4);
end;

function TChunkIO.ReadQWord: QWord;
begin
  FStream.Read(Result, 8);
end;

function TChunkIO.ReadInt64: Int64;
begin
  FStream.Read(Result, 8);
end;

function TChunkIO.ReadSingle: Single;
begin
  FStream.Read(Result, 4);
end;

function TChunkIO.ReadDouble: Double;
begin
  FStream.Read(Result, 8);
end;

function TChunkIO.ReadExtended: Extended;
begin
  FStream.Read(Result, 10);
end;

function TChunkIO.ReadString: ansistring;
var
  i, Len: Integer;
begin
  FStream.Read(Len, 4);
  Result:='';
  for i:=1 to Len do Result:=Result + Chr(FStream.ReadByte);
//  FStream.Read(Pointer(Result), Len);
end;

function TChunkIO.ReadBuffer(Size: Cardinal): Pointer;
begin
  Result:=GetMem(Size);
  ReadBuffer(Size, Result^);
end;

procedure TChunkIO.ReadBuffer(Size: Cardinal; out Data);
begin
  FStream.Read(Data, Size);
end;

end.

