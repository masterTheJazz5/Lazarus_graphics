unit MeshLoader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Meshes;

type
  TSupportedMeshFileFormats = record
    Extensions: array of string;
    Descriptions: array of string;
  end;

procedure GetSupportedMeshFileFormats(out Formats: TSupportedMeshFileFormats);
function GetSupportedMeshFilesFilter(AllFilesMask: Boolean=False): string;
function IsMeshFileFormatSupported(Extension: string): Boolean;
function LoadMeshFromFile(FileName: string; AutoLoadTexture: Boolean=True): TMesh;
function LoadMeshFromStream(Stream: TStream; FileName: string; AutoLoadTexture: Boolean=True): TMesh;

implementation

uses
  Logger, OBJMeshLoader, JTFMeshLoader;

function DoLoadMeshFromStream(Stream: TStream; FileName: string; AutoLoadTexture: Boolean=True): TMesh;
var
  Loader: TMeshLoader;
  Ext: string;
begin
  Result:=nil;
  Ext:=LowerCase(ExtractFileExt(FileName));
  if Ext='.obj' then begin
    Loader:=TOBJMeshLoader.Create;
    TOBJMeshLoader(Loader).AutoLoadTexture:=AutoLoadTexture;
  end else if Ext='.jtf' then begin
    Loader:=TJTFMeshLoader.Create;
    TJTFMeshLoader(Loader).AutoLoadTexture:=AutoLoadTexture;
  end else begin
    Log('Failed to guess the mesh type from the extension');
    Exit;
  end;
  try
    Loader.LoadFromStream(Stream, FileName);
    Result:=Loader.Mesh;
  finally
    FreeAndNil(Loader);
  end;
end;

procedure GetSupportedMeshFileFormats(out Formats: TSupportedMeshFileFormats);
begin
  SetLength(Formats.Extensions, 2);
  SetLength(Formats.Descriptions, 2);
  Formats.Extensions[0]:='obj';
  Formats.Extensions[1]:='jtf';
  Formats.Descriptions[0]:='Wavefont OBJ';
  Formats.Descriptions[1]:='Just Triangle Faces';
end;

function GetSupportedMeshFilesFilter(AllFilesMask: Boolean): string;
var
  Formats: TSupportedMeshFileFormats;
  I: Integer;
begin
  Result:='';
  GetSupportedMeshFileFormats(Formats);
  for I:=0 to High(Formats.Descriptions) do begin
    if I > 0 then Result += '|';
    Result += Formats.Descriptions[I] + ' (*.' + Formats.Extensions[I] + ')|*.' + Formats.Extensions[I];
  end;
  if AllFilesMask then Result += '|All Files|' + System.AllFilesMask;
end;

function IsMeshFileFormatSupported(Extension: string): Boolean;
begin
  if Extension='' then Exit(False);
  if Extension[1]='.' then Extension:=Copy(Extension, 2, Length(Extension));
  if Extension='' then Exit(False);
  Extension:=LowerCase(Extension);
  Result:=(Extension='obj') or (Extension='jtf');
end;

function LoadMeshFromFile(FileName: string; AutoLoadTexture: Boolean): TMesh;
var
  FileStream: TFileStream = nil;
begin
  try
    FileStream:=TFileStream.Create(FileName, fmOpenRead);
    Result:=LoadMeshFromStream(FileStream, FileName, AutoLoadTexture);
  except
    Log('Failed to load mesh ' + FileName + ': ' + Exception(ExceptObject).Message);
    Result:=nil;
  end;
  FreeAndNil(FileStream);
end;

function LoadMeshFromStream(Stream: TStream; FileName: string;
  AutoLoadTexture: Boolean): TMesh;
var
  MemoryStream: TMemoryStream = nil;
begin
  Result:=nil;
  try
    MemoryStream:=TMemoryStream.Create;
    MemoryStream.CopyFrom(Stream, Stream.Size);
    MemoryStream.Position:=0;
    Result:=DoLoadMeshFromStream(MemoryStream, FileName, AutoLoadTexture);
  except
    Log('Failed to load mesh ' + FileName + ': ' + Exception(ExceptObject).Message);
    Result:=nil;
  end;
  FreeAndNil(MemoryStream);
end;

end.

