
Lazarus Graphics
-----------------
*Updated Code From JazzMaster At Github*


If you remember the "Borland Graphics interface(BGI) "...this is the more modern replacement unit.

More details in the 2d section. 2D uses SDL1, 3D uses OpenGL/SDL2. Yes, Apparently 
You Can Combine the two. You Need to flushGlBuffers in between switching methods . 


The old school way:

	VESA / lib(S)VGA / Dos Assembler (Int 10,3Fxx) calls
	
	
The new way:

	SDL (XConsole/Lazarus)

TCanvas is a "user interface chunk" / slimmed down version of the old graphics unit.
TCanvas is not a replacement --  and was not implemented expecting multimedia development. We need to bypass use of TCanvas. 

**IT IS VERY LIMITED IN SCOPE to GDI based color schemes**
**ONLY 20 colors are available as a result.**

SDL "was designed for such a multimedia use", as was OpenGL(vulkan).

SDL can tap Quartz/DirectX/(DrawSprockets and CodeWarrior on Mac OS9)/GL.

Some code changes, as a result -MUST BE FORCED. 

Some routines will no longer be functional.

Drawing routines- using available modes "should work out of the box". 
(Your code will work fine, So excuse the C) 

Ive taken care of the liberty of "fixing the core routines".

**You might want to bump up your resolution/ scale your output/ update your shaders...etc... if using old code** (libswscale from ffmpeg/libav..)
I have provided some form of expanded FAT PIXELS. You are encouraged to use these routines.

We have enough "borland compatible libraries". Its time to break away. 
"SIMILAR TO" seems to be the MODUS OPERANDI lately.

This is the (NEW?) Graphics Core API for FPC/Lazarus.<br>
We are using SDL (and OpenGL) here. 

Please try it- especially if you are coming from DOS-era libGraph(C) or Graph.pas(BP/TP) units.<br>
"This" is a modern-day version of "that".

I am learning here too-
and will teach what Ive gathered (its NOT common knowledge, game devs/platform devs//engine devs keep a lot of things secret)

SDL_DrawSomething functions work, however Ive not had the time lately to devote to ripping thru C, checking drawing routines, especially expanded ones.

Header Data needs to be "sucked in", and "implemented USE CASE", made. If confused, refer to the included LAZ_BGI code in progress.

 Do Not Blindly Port Code. 
Its Dangerous , incomplete , and Often C Macros (ReadPort, RewritePortb) are used. (Ive seen Lazarus Board Devs pull this. ) 

(HMMM: Get/PutPixel missing?? SDL Bug) 

BUG:

Noticed in an ANSI C source file:

ANSI C (C/CPP) requires exit procs, as with old-school assembler(exit status codes). <br>
Lousy C!

This is shitty programming. 

LOG EVERYTHING. Throw a non-zero exit code, if aborted, only when necessary (to fully bail/end the program/app/game/routine).
You dont have to "return" anything.

SDL takes on ANSI C in development. 
This is an optional requirement, however SDL_Error codes(to see what happened) arent given UNLESS you use functions vs proceedural calls.
THIS IS WRONG.

SDL should log the error , instead of making you PROBE/guess and DIG for it.


## This sounds familiar...

Most four year college students(I started w Pascal Os Dev) are trying to reinvent some sort of wheel. 

The problem is-
    theres a reason why so many others have failed.

Most of the similar projects-- have failed. <br>
Ive already left several in the dust. not one has come close this level of compatibility, complexity.

#### raylib- 
	simply hasnt been heard of. (This is one of those, "forget the work everyone else is doing so far...Im rewriting everything"...libraries.)
	OPPOSITE DIRECTION that one needs to take.

You dont need to personally re-create every wheel.
This is wasteful "code dominance". 

**If you cant rely on others--get out of the race.**
-It also **drastically** increases development time(and learning curve).


#### EVAS(enlightenment)-
 is likely rarely used (into extinction)--

    "enlightenment Desktop use"- faded like a 90s fad. Its difficult, yet simple- to use.(designed for win95 boxes)

One Pascal Dev claims he has done this in Cairo-- SHOW ME THE CODE. I SEE NONE.


#### SDL 
SDL/GL have so far "kept up".

**SDL is a standard, but a dodgy, buggy,incomplete C one. One that leans on libHermes too much**

If Im reading the CPP correctly, we need recordPtrs and ObjectPtrs, not ClassPtr/In(sub)stantiation here.
(The CPP/OOP is not needed at all, Lazarus takes care of the rest.)

This means the "C/OOP/OBJ-C overlay" that SDL imposes- is unnessary and wasteful.

Further, they reimplement (an IDT?) instead of hooking the "OS system Timer". This is both WRONG and BAD Practice.

(The HPET can be accessed VERY easily- Ive actually done this in my kernel code, myself.)


**Im finding that more than half of SDL can be rewritten, outside of C**.

(Pardon my UN-C development here, Im checking the viability of the GFX SDL routines, specifically)

Things we need:

		Timer code
		Rendering Code(lines,fills,etc)
		Sprite APIs(think amiga or cartridge games, top layer of moving things)
		Background Layering code(transperent pixel stuff/colorkeys)
		CORE APIs(open,close,setMode,SetPalette,etc)
		Color Ops(LibHermes and similar, my code is barebones- I know it)
		Text APIS
		Menu APIs(you want to code the game, not waste time on this. Both in and out of the game)
		Dialog APIs(both in and outside of the app/game)
		Input tracking code(done correctly!!)
		
		The rest is Glue code- YOU write that.
	
	
It took a DOCTORATES to screw this up(not kidding you):

		NO, you do NOT drop the event handler(EVER)- 
		
				you exit the subroutine
				clear the buffer
				then allow the input section to exit(wait for the buffer to fill,then fetch the data)
				
		Anyone ever hear of a CPU register??  (the ax and rax?? mhm...oh, yes...were going there. Pointers, anyone?)

-Theres a trick (in Gaunlet Legends?) - you turn into a mouse, go thru a hole, change form again- and voila-- access to the room behind the wall.
(much like how I passed the GC around- and how linux kernel boots from GRUB.)

### Dont start the presses yet...

Printing is one unit we lack.<br> 
You need to convert the data into "postscript format"(PS)- and kick the data(output,file) into some sort of printer buffer
or printer engine, or "print server" - to print anything.

"Line printing"(LPR) disappeared with the s360(shared college classroom mainframe) years ago, in the 90s.

### The BLEEPING COMPUTER
 
It should be beeping...(why its not is another matter)<br>
I picked up where fpc team left off. <br>

**YES, THERE is SOUND!!**

Debian isnt beeping, Ubuntu is.<br>
(lack of a PC speaker on my notebook?)<br>

PC SPEAKER, not SOUNDCARD OUT.

So yes, that is back(ON LINUX), Grand Piano-ish...if you want to write older sound routines..<br>
I have docs on this , and its "sort of implemented" in my sub-units.

(See Late 1980s and early 1990s BASIC/ ABC MIDI refernces on this- yeah, its that old of a routine.)

MIDI support is coming, but Ive not implemented it yet. (likely Timidity hook)

Wav (and other multimedia codecs) are provided thru external libs(C), however, SDL supports playback (in SDL v1), even from RedBook Audio CDs.
Other libs are porting this (even RedBook audio) thru SDLv2.


### Development Path (for the unaware)

Suggested Dev learning path:

This will require access to original source code, for rewrites.
16bit apps can be patched(crt bugs, 32bitVM, etc) but not always "recompiled".

16/32 bit:
BP/TP5(7) - 16bit by default, 32bit (DPMI) vm available- use with NewFrontier unit
	(Patches are provided one level up.)

FreePascal - compiles 32bit applications(unless using i8086 port- id suggest LARGE memory model)
	yes, its available for dos. 
	<486 port (i8086) came into play a little bit ago. 
	i686-pentium was the old standard

	The IDE will compile thru at least 486, im aware of this much.
	Requires unit libs be placed(and rtl built ahead of time) for target arch

i8086 Port has to be built "from scratch". FPC Units (as a whole) take up to much room on disk. 32mb was a hard Hdd limit back then. 


64bit:
	cross build for win32+x86_64(win64).
	You will need to add the windows unit "to your clauses".

Linux(moreso OS9/OSX/Amiga) are completely different beasts.
	It may take OS level programming courses, (or time to learn methods-years) to understand unix/linux programming.
	Dos /windows methods DO NOT WORK on unices. (they may break things)

### License

Lazarus Graphics units are **Mozilla/Apache** licensed.<br>

Various other misc units are licenced under GPL, LGPL and "other Open Source parameters".<br>
You remain free to FREE them further - however, GPLv2 and LGPLv2 forbid "restrictive licence changes". (version3 has no such restriction.)
They can still be included in a nonfree distribution. 


### Piracy Warning

Its a shame I must put this here, but NASA interns have stolen my code.

This is not PUBLIC DOMAIN CODE. 
PUBLIC DOMAIN is ABANDONWARE- "do whatever licences".<br>

You are committing both piracy (and plagarism) if sources/previous authors are not mentioned (cited) in your product.
**YOU MUST GIVE CREDIT - TO WORK YOU HAVE NOT DONE.**

**DO NOT ADD "NON-GPL license" compliant code. It will be instantly rejected.**

Borland,Embercardero,Microsoft, Apple, and GL Consortium (as well as MESA and X Consortium(s))- and others- maintain the copywrights and licensure on 
GUI APIs- but allow hooking them. 

That is all Ive done. Hook routines. I may no intent to rewrite them.
I do not claim to have wrote that code, and I make no claim to. 

### Notice of refusal to comply with DMCA notices

Lawsuits (or DMCA notices) -on bais of imfingement or plagarism will be ordered  "dismissed outright, with predjudice". 
Your claim will be countered in open court.
	
However, I do comply with "reasonable" company requests.( if Borland7 release is an issue, i will redact it to version 5)
(Borland BP/TP5 is in the public domain, a decision made by Borland years ago.)

I have not infringed on anyone's work. Others may have.

#### WARRANTY

This unit uses (and links to) code from various people. 
THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY- 

		Including the merchantibility for ANY particular purpose.(It may not suit your needs)

The code is "mostly sane", but do use caution.<br>
This is WIP, but the most complete attempt that I have (built) found so far.

If you have problems,issues USE THE LATEST RELEASE, not the WIP "Git snapshot"(git clone xyz... ).


Timer Callbacks(EpikTImer-ish) are not fully implemented yet, use caution. Code will not be optimal, yet. 

Video Timer Callback must be properly Implemented . 

---

Ports status:

Win32/64 and Linux32/64(at least) are FULLY OPERATIONAL, (as a SDL "console app" in your OS UI of choice. Framebuffer("DOS" mode) is not currently targeted. WIP).<br>
Wherefore- The the binary likely WILL NOT "HAVE A LAZARUS INTERFACE."

FPIDE is in the seperate download of fpc32 binaries. Use it if you like. 


SDL version is determined a few ways. 

	By machine ARCH/capabilities(32 or 64 bit cpu)
	By whats installed(/usr/lib/<something> - hey,Im on Linux...)
	If FPC build options were thrown during compile(note: sse and x86 dont mix)
	If Framebuffer Modes Requested

Code for BOTH versions is included. 
There is no need to separate the unit out.

BUG: 

	SDL 32bits can be left/installed on a 64 bit OS(windows) and compile, but never runs(linking issues).
	- also the reverse.
	
	**TRIPLE CHECK YOU HAVE THE RIGHT 'BIT' version of the DLLs referenced/installed before complaining**
	
BUG:	

	SDL+Lazarus dont mix
	Lazarus LCL creates its own context and input API- and refuses to link to SDLs implementation.
	
	What this does is complicate things until I can hook the LCL window/GC(graphical context). 
	As such, Lazarus (LCL+SDL ) development isnt happening at the moment. Im looking into it.
	(It appears possible.)

GL seems to work- either called thru the console, or as a Lazarus app. (I use DelphiGL code from Khronos Team.)

NOTE: 
		SDLv2 had so many drastic changes due to the fact that OpenGL was used behind the scenes.
		OpenGL use was very limited on OS9 macs, so the code was abandoned, work was moved to Quartz2D(OSX). 
		(Possibility we can take advantage of this?)

### Code Status:

2D Graphics(BGI):

InitGraph: Working<br>
CloseGraph:  Working <br>
GraphDetect: 

	SDLv1:  Working
	SDLv2:  In Progress 

(Draw)Line:  Working <br>
(Draw)Rect:  Working <br>

PlotPixelWithNeighbors(FAT PIXELS):  Working<br> 
PutPixel:  Working <br>
GetPixel:  Working , needs rewrite<br>

Clearscreen/ClearDevice:  Working<br> 

SetBGcolor(multiple declarations):  Working<br> 
SetFGColor(multiple declarations):  Working <br>

Tris: somewhat implemented<br>
Circle(Bresnans): Should work, untested<br>
Ellipses, Polys,Fills: Unimplemented (JMs code)<br>

Logging: FULLY OPERATIONAL(temporarily bugged in FIleIO)


3D OpenGL(very basic, not much work done yet):

InitGraph:  Working <br>
CloseGraph:  Working <br>
Core GL ops:  Working, Linking OK from C <br>
GL "DirectRendering" (Drawing on the fly ops):  Working <br>

Textures(and QUADS): UNTESTED, doesnt need "shaders" with "basic objects".<br>

Games create objects(possibly thru shaders), then add Textures to them, like "slapping wallpaper on everything".<br>

---

Castle Engine may provide these:

GL Shaders(GLSL): MOSTLY BROKEN(quasi-C), possible to use<br>

	requires wonky C-like shader code(vertex/fragment) and "GLSL programs" to be "compiled"

3D spirtes(OGRE/AssImp):  UNIMPLEMENTED<br>
SceneGraph(WorldGen,SpeedTree,etc): UNIMPLEMENTED<br>
Collision(Bullet) Physics: UNIMPLEMENTED<br>
Compressed and Advanced Texture Loading: UNIMPLEMENTED, PLANNED<br>

---

### Depends HELL!

These units have many many depends.<br>
I have tried to source as many as possible for you, without requiring you to rebuild each and every one.

SDL Will Have To Be Static Compiled Going Forwards. SDL3 Breaks too much. 

**There is no way to avoid this unless you want to reprogram everything  
for 50+ years, and have a team of programmers to do it.**

A mistake noobs make- is ignoring this aspect, trying to do everything yourself, when its already been done properly.

JPEG support alone- in Pascal is an ancient mess, most will not know decades later how to implement it.<br>
**You have to co-ordinate between projects and programmers on an international level.**

OpenGL would not be possible to use in Lazarus if not for FContext and DelphiGL teams. **(In fact the Laz demos are broken otherwise)**

**Anyone scared of this- wanting static, self-made SINGLE DEPENDS libraries ...run into the worgen woods now.<br>
I cannot- and WILL NOT- help you.**<br>


#### Just Draw

I operate on the "just draw" concept. 

You shouldnt have to worry about the sublayered APIs.<br>
You want to draw, so lets make it easier!

I am not replacing every SDL function here- but I am rewriting "the Borland equivalent".<br>

You can use any "drawing method you like", once the context is operational.<br>
Same with OpenGL.

(so missing "JM code"- except for the basics- is not actually a FLAW, but WIP)


**OPENGL is reserved for 3D (and 2D/3D combinations) ONLY.**

Theres one possible glitch:

		2d SDL ops may be using "forced software surfaces" 
			-if not using fullscreen rendering/drawing
			-if using SDL1 vs SDL2 on newer hardware

Of course, hardware rendering is faster(if you can use it).<br>

DO NOT PRESUME that Accelerated drawing ops REQUIRE openGL. They DONT. <br>
Im getting 700fps, (yes 700!) on a "vector-rendered GLScene equivalent"(2.5d) in Windows10. (Yes, the C demo.) 
-The code is ANCIENT!!!

I Don't Tolerate Purist Programmer (C Or Pascal) Comments. CoExist. Im Porting, Not Hooking- for a reason . 

#### Portables??

You need to support an API that allows for "accellerated drawing".<br>

iOS devices have licencing issues that forbid developing for them in FPC/Lazarus.<br>
Android (from FPC) needs some help- or we need to find another way to accomplish this task.

The RasPi is showing Promise, for VTerm access(libsvga or directfb) thru the "mailbox acceleration framework".

Ive not tested, SDL 1/2, nor Lazaurus APIs. The libraries exist on this platform, ITS JUST UNTESTED. (time limitations)

I will add (not included) libraries and binaries, when possible. I have Pi3 and Pi400 (Pi4+keyboard) available to me.  As well as some older Macs. 

### The teardown

This "UNIT" should compose of two major elements:

        2d- BGI
        3d- OpenGL

OpenGL is EXTREMELY ADVANCED 2D/3D engine- that leaves most of the work, math, (and how-to) UP TO YOU.<br>
IT DOES NOT ASSUME ANYTHING.

If you need to mix 2D and 3D ops (at the same time) -use OpenGL.<br>
I will tell you why:

	OpenGL doesnt care what view you use, if you draw in 3d onto the side of a cube, etc.
	You can mix co ordinate systems 50x time a second- and it will keep up.

The GL co-ordinate system is what confuses people.<br>

**INTERMEADIATE ALGEBRA IS A OpenGL Programming REQUIREMENT**

Now, you can add a 2D HUD on top of all of that- AND work in 3D (at the same time), using a different
co-ordinate calculation(without dropping a single voxel- or slowing down). 

(Think "HUD", when you play HALO. All of that has to render, not just the HUD- the entire scene)

(Change your "perspective" matrix, reset it, change it again...)<br>
(or as I like to call it- "the push it, pop it, tweak it-bop it", method)

"Drawing primitives" is left to the core APIs that can better accomplish the task.(2D)<br>
Trying to rewrite these in a 3D API is proving to be hell (and hair) raising and too much headache.<br>
So- Im just not going to do it.(Its overkill trying)


### 2D

2D will consist of a "mimicked", Pseudo-BGI API based on what we already know about it,<br>
Ported multi-platform (and multi-arch) thru SDL/GL.


#### Mac OS(9)

MAC OS9 is SDL 1.2.12 limited!

OS9 doesnt support above OpenGL 1.2.12- SDL2 requires OpenGL version 2 or above, so as of SDL 1.2.13, OS9 was dropped.<br>

OS9 has 16bit memory limts. 

This was Code from OS8 and 7 that was never removed.
(OS8 removed the compatibility layer with 080 Motorola processors)

**Compatibility is actually a HUGE DEAL with MacOS**.<br>


I find that OS9 was ignored, and abandoned (often abused) when the latter moments 
of developing on the platform actually made OS9 (and the Mac) a solid product. <br>

**It was too perfect.**<br>

For a UI Use this version: CodeWarrior Pro 4 , Sep 1998

MPW can be used, however, fpc itself instatiates as a tool (with console IO)- it would be difficult to write code for anything outside of a console app.
The Open Source MPW Kit On Github Shows Promise . 


fpc generates "tools"( as itself is one) and "sioux (MacShell) applications" when cross compiling, be aware of this. 
These are "console applications".

Standard Programming of OS9 requires a lot of internal routine access, Its similar to the Amiga- the routines are foreign to most.

-There is no "DOS MODE", nor "text mode".
Color routines were "an addon function", then implemented better with DrawSprockets instead of "natively".
This is a real pain to learn by hand, docs exist- but theyre a mess.

(Snow Leopard still supports PowerPC applications- half of which are still supplied, as of SL-"PPC only" by apple)

Lion breaks Rosetta, Snow was supposed to remove it.
This is what it means by a FAT binary(32/64- and likely in some cases QUADFAT PowerPC/Intel)


SERIOUS MacOS UI Prgrogramming takes some SERIOUS development time (and research). You will be hitting the books HARD.
- IFF you can find them....

(Think Different,eh?)

OSX is really the way to go-
        However, I will do my best to support OS9 or "classic" or "sheep-shaved" installs


### 3D

3d will be a mix of:

        FContext+DelphiGL(portable)+Lazarus units

where applicable and 

        OpenGL+CodeWarrior

where it isnt.


#### (Delphi) GL Caveat

DGL doesnt allow lower than 24bpp, nor palettes.<br>
These parts are WIP, but MY variant implements this as a "32bit color system"(DWord aligned).

Implementing palettes often requires heavy understanding of Fragment Shaders and Textures(think unreal tournament).

If you just want "modern-day graphics", then focus on 3D API- <br>
and look at Game engines like "Castle". (OpenMorrowWind uses assImp/OGRE.)

Make your life easier, USE a tool designed for the job. 
You dont have to learn COCOs, Unreal APIS- just use a BETTER,sharper TOOL.

#### Cheating

Valve and Unity and Unreal devs "cheat compatibility"- <br>
by using WineAPIs (instead of porting the D3D code to OpenGL).

The WineAPIs allow them to "not know" Linux-es, "to CHEAT again" by running a windows application.<br>
There is no actual "Native Code" running- and these devs DO NOT CARE.<br>

90% of apps(and AAA titles) WILL in fact run under Linux(protonTricks,winetricks, PlayOnLinux,etc)

In my book- to claim "Cross-platform development", THIS IS CHEATING.


## Build Targets (all are supported)

	OS9/OSX /MacShell(OS9) (Intel and PowerPC macintosh)
	Win32/64 "Command Prompt" /OpenGL
	Lin32/64 X11/VTerm /OpenGL
	RasPi : VTerm(mailroom based GPU accleration)- partially implemented
	Android (experiental and unimplemented)
	HTML5 Canvas/JS/WebGL (Demos provided)

