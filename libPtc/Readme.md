﻿this code may be depreciated for "FULL cairo support" instead.
You can code for a console APP, or a Lazarus one- its just vterm/true console "support" is no more.
Without underlying APIs, I have nothing to hook into -to generate a context for drawing.
No context, no drawing-- its just that simple.

The main meat of where we might depend on libSVGA, ptc, etc has been removed from the operating system(kernel/X11/windows) over time.
The rest of the support(X11/windows) is already implemented at API levels(x11 prims,d2d,quartz2D,etc). I infact, I hook these, where possible.

DrawSprockets will tkae some time, but can be implemented. Theres documentation somewhere on this-- thanks APPLE--for making a mess!

Most of the core routines are libHERMES, timers(HPET), or are already implemented(bresnans drawing methods) - or rewritten.
InputAPI is being looked into, but current implementations(C,pascal) are incorrect. Everyone assumes directly that SDL is being used w/o abstraction libs(wrong)
and as a result, data is not passed the way it should be.

There is a way to do this, and CPP devs have taken advantage.
Furthermore, actual game design using event driven APIs (CPP/sdlv2) is incorrect. I see a serious dev flaw here, (one that may have affected multiple
OSes) and a lot of instatiated and classed code- where it simply is unnessessary.

Vectors can be easily implemented as well, but give me a moment. This just leaves rendering timing and sprite implementation.

A few demos as to how things are done- or the explaination thereof....and you have games and multimedia.

Voila! Mission accomplished!
