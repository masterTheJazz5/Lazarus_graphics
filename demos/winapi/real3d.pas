// OpenGL demo (work in progress)
program real3d;
{$APPTYPE GUI}
uses wingraph,wincrt,{$IFDEF FPC}gl,glu;{$ELSE}opengl;{$ENDIF}


{ patch this for DelphiGL.
 

procedure Init;
var ErrorCode: integer;
    gd,gm    : smallint;
begin
  gd:=nopalette; gm:=mFullScr;
  InitGraph(gd,gm,'');
  ErrorCode:=GraphResult;
  if (ErrorCode <> grOK) then Halt(1);
  Randomize;
end;
}

procedure OpenGLPlay;
const ground = 1;
      front_wall = 10; back_wall = 11; left_wall = 12; right_wall = 13; floor = 14;
      roof = 15; lamp = 16; house = 17; tree = 18; forest = 19; mountain = 20;
      stars = 21;
      glob : array[0..3] of GLfloat = (0.1,0.1,0.1,1.0);
      amb  : array[0..3] of GLfloat = (0.8,0.8,0.8,1.0);
      spec : array[0..3] of GLfloat = (1.0,1.0,1.0,1.0);
      diff : array[0..3] of GLfloat = (0.8,0.8,0.8,1.0);
      lpos : array[0..3] of GLfloat = (8.0,1.0,1.0,0.0);
      fog  : array[0..3] of GLfloat = (0.5,0.5,0.5,1.0);
      emiss: array[0..3] of GLfloat = (0.9,0.9,1.0,1.0);
      null : array[0..3] of GLfloat = (0.0,0.0,0.0,1.0);

var ViewInfo: ViewPortType;
{$IFDEF FPC}
    quad    : PGLUquadric;
{$ELSE}
    quad    : GLUquadricObj;
{$ENDIF}
    i       : integer;
    r,t,z   : double;

  procedure InitScenery;
  begin
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(40,GetMaxX div GetMaxY,0.1,10000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,integer(GL_TRUE));
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,integer(GL_TRUE));
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,@glob);
    glLightfv(GL_LIGHT0,GL_AMBIENT,@amb);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,@diff);
    glLightfv(GL_LIGHT0,GL_SPECULAR,@spec);
    glLightf(GL_LIGHT0,GL_CONSTANT_ATTENUATION,1.0);
    glLightf(GL_LIGHT0,GL_LINEAR_ATTENUATION,0.5);
    glLightf(GL_LIGHT0,GL_QUADRATIC_ATTENUATION,0.0);
    glEnable(GL_COLOR_MATERIAL);
    glFogi(GL_FOG_MODE,GL_EXP2);
    glFogfv(GL_FOG_COLOR,@fog);
    glFogf(GL_FOG_DENSITY,0.001);
    glHint(GL_FOG_HINT,GL_DONT_CARE);
    glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
    quad:=gluNewQuadric(); gluQuadricDrawStyle(quad,GLU_FILL);
  end;

  procedure DrawScenery;
  var i,h  : integer;
      t1,t2: double;
  begin
    glNewList(ground,GL_COMPILE);
      glEnable(GL_CULL_FACE);
      glCullFace(GL_BACK);
      glBegin(GL_QUADS);
        glNormal3f(0.0,1.0,0.0);
        glColor3f(0.0,0.5,0.0);
        glVertex3f(-1000.0,0.0,-1000.0);
        glVertex3f(-1000.0,0.0, 1000.0);
        glVertex3f( 1000.0,0.0, 1000.0);
        glVertex3f( 1000.0,0.0,-1000.0);
        glColor3f(0.4,0.4,0.4);
        glVertex3f(-2.0,0.1,3.0);
        glVertex3f(-2.0,0.1,100.0);
        glVertex3f( 2.0,0.1,100.0);
        glVertex3f( 2.0,0.1,3.0);
      glEnd();
      glDisable(GL_CULL_FACE);
    glEndList();
    glNewList(stars,GL_COMPILE);
    glMaterialfv(GL_FRONT,GL_EMISSION,@emiss);
      glPointSize(2.0);
      glBegin(GL_POINTS);
        for i:=1 to 500 do begin
                             t1:=2*Pi*Random; t2:=Pi/2-Pi/6*Random-0.02;
                             glVertex3f(1100*Sin(t1)*Sin(t2),1100*Cos(t2),
                                        1100*Cos(t1)*Sin(t2));
                           end;
      glEnd();
    glMaterialfv(GL_FRONT,GL_EMISSION,@null);
    glEndList();
    glNewList(floor,GL_COMPILE);
      glBegin(GL_QUADS);
        glNormal3f(0.0,1.0,0.0);
        glVertex3f(-6.0,0.1,-3.0);
        glVertex3f(-6.0,0.1, 3.0);
        glVertex3f( 6.0,0.1, 3.0);
        glVertex3f( 6.0,0.1,-3.0);
      glEnd();
    glEndList();
    glNewList(front_wall,GL_COMPILE);
      glBegin(GL_QUADS);
        glNormal3f(0.0,0.0,1.0);
        glVertex3f(-6.0, 0.0, 3.0);
        glVertex3f(-1.5, 0.0, 3.0);
        glVertex3f(-1.5, 6.0, 3.0);
        glVertex3f(-6.0, 6.0, 3.0);
        glVertex3f( 1.5, 0.0, 3.0);
        glVertex3f( 6.0, 0.0, 3.0);
        glVertex3f( 6.0, 6.0, 3.0);
        glVertex3f( 1.5, 6.0, 3.0);
        glVertex3f(-1.5, 5.0, 3.0);
        glVertex3f( 1.5, 5.0, 3.0);
        glVertex3f( 1.5, 6.0, 3.0);
        glVertex3f(-1.5, 6.0, 3.0);
      glEnd();
    glEndList();
    glNewList(back_wall,GL_COMPILE);
      glBegin(GL_QUADS);
        glNormal3f(0.0,0.0,-1.0);
        glVertex3f(-6.0, 0.0,-3.0);
        glVertex3f(-6.0, 6.0,-3.0);
        glVertex3f( 6.0, 6.0,-3.0);
        glVertex3f( 6.0, 0.0,-3.0);
      glEnd();
    glEndList();
    glNewList(left_wall,GL_COMPILE);
      glBegin(GL_QUADS);
        glNormal3f(-1.0,0.0,0.0);
        glVertex3f(-6.0, 0.0,-3.0);
        glVertex3f(-6.0, 0.0,-1.0);
        glVertex3f(-6.0, 6.0,-1.0);
        glVertex3f(-6.0, 6.0,-3.0);
        glVertex3f(-6.0, 0.0, 3.0);
        glVertex3f(-6.0, 6.0, 3.0);
        glVertex3f(-6.0, 6.0, 1.0);
        glVertex3f(-6.0, 0.0, 1.0);
        glVertex3f(-6.0, 0.0,-1.0);
        glVertex3f(-6.0, 0.0, 1.0);
        glVertex3f(-6.0, 3.0, 1.0);
        glVertex3f(-6.0, 3.0,-1.0);
        glVertex3f(-6.0, 5.0,-1.0);
        glVertex3f(-6.0, 5.0, 1.0);
        glVertex3f(-6.0, 6.0, 1.0);
        glVertex3f(-6.0, 6.0,-1.0);
      glEnd();
    glEndList();
    glNewList(right_wall,GL_COMPILE);
      glBegin(GL_QUADS);
        glNormal3f(1.0,0.0,0.0);
        glVertex3f( 6.0, 0.0,-3.0);
        glVertex3f( 6.0, 6.0,-3.0);
        glVertex3f( 6.0, 6.0,-1.0);
        glVertex3f( 6.0, 0.0,-1.0);
        glVertex3f( 6.0, 0.0, 3.0);
        glVertex3f( 6.0, 0.0, 1.0);
        glVertex3f( 6.0, 6.0, 1.0);
        glVertex3f( 6.0, 6.0, 3.0);
        glVertex3f( 6.0, 0.0,-1.0);
        glVertex3f( 6.0, 3.0,-1.0);
        glVertex3f( 6.0, 3.0, 1.0);
        glVertex3f( 6.0, 0.0, 1.0);
        glVertex3f( 6.0, 5.0,-1.0);
        glVertex3f( 6.0, 6.0,-1.0);
        glVertex3f( 6.0, 6.0, 1.0);
        glVertex3f( 6.0, 5.0, 1.0);
      glEnd();
    glEndList();
    glNewList(roof,GL_COMPILE);
      glBegin(GL_TRIANGLES);
        glNormal3f(-1.0,1.0,0.0);
        glVertex3f(-6.0, 6.0,-3.0);
        glVertex3f(-6.0, 6.0, 3.0);
        glVertex3f(-4.0, 9.0, 0.0);
      glEnd();
      glBegin(GL_TRIANGLES);
        glNormal3f(1.0,1.0,0.0);
        glVertex3f( 6.0, 6.0,-3.0);
        glVertex3f( 4.0, 9.0, 0.0);
        glVertex3f( 6.0, 6.0, 3.0);
      glEnd();
      glBegin(GL_QUADS);
        glNormal3f(0.0,1.0,1.0);
        glVertex3f(-6.0, 6.0, 3.0);
        glVertex3f( 6.0, 6.0, 3.0);
        glVertex3f( 4.0, 9.0, 0.0);
        glVertex3f(-4.0, 9.0, 0.0);
      glEnd();
      glBegin(GL_QUADS);
        glNormal3f(0.0,1.0,-1.0);
        glVertex3f(-6.0, 6.0,-3.0);
        glVertex3f(-4.0, 9.0, 0.0);
        glVertex3f( 4.0, 9.0, 0.0);
        glVertex3f( 6.0, 6.0,-3.0);
      glEnd();
    glEndList();
    glNewList(lamp,GL_COMPILE);
      glMaterialfv(GL_FRONT,GL_EMISSION,@emiss);
      gluSphere(quad,0.3,10,10);
      glMaterialfv(GL_FRONT,GL_EMISSION,@null);
      glBegin(GL_LINES);
        glVertex3f(0.0, 0.0,0.0);
        glVertex3f(0.0, 4.0,0.0);
      glEnd();
    glEndList();
    glNewList(house,GL_COMPILE);
      glEnable(GL_CULL_FACE);
      glCullFace(GL_BACK);
      glColor3f(0.4,0.4,0.4);
      glCallList(floor);
      glColor3f(0.5,0.5,0.8);
      glCallList(front_wall); glCallList(back_wall);
      glCallList(left_wall); glCallList(right_wall);
      glCullFace(GL_FRONT); glColor3f(0.7,0.7,0.3);
      glCallList(front_wall); glCallList(back_wall);
      glCallList(left_wall); glCallList(right_wall);
      glDisable(GL_CULL_FACE);
      glColor3f(1.0,0.5,0.0);
      glCallList(roof);
      glTranslatef(0.0, 4.0,0.0);
      glCallList(lamp);
      glTranslatef(0.0,-4.0,0.0);
    glEndList();
    glNewList(tree,GL_COMPILE);
      glColor3f(0.0,0.8,0.0);
      glRotatef(90,-1.0,0.0,0.0);
      gluCylinder(quad,1.0,0.5,6.0,5,5);
      glRotatef(90, 1.0,0.0,0.0);
      glTranslatef(0.0, 8.0,0.0);
      gluSphere(quad,2.5,10,10);
      glTranslatef(0.0,-8.0,0.0);
    glEndList();
    glNewList(forest,GL_COMPILE);
      for i:=0 to 4 do
      begin
        glTranslatef(-5.0,0.0, 10+20*i);
        glCallList(tree);
        glTranslatef( 5.0,0.0,-10-20*i);
        glTranslatef( 5.0,0.0, 10+20*i);
        glCallList(tree);
        glTranslatef(-5.0,0.0,-10-20*i);
      end;
    glEndList();
    glNewList(mountain,GL_COMPILE);
      glEnable(GL_FOG);
      glColor3f(0.0,0.3,0.8);
      glBegin(GL_TRIANGLES);
        glNormal3f(0.0,0.0,1.0);
        for i:=0 to 19 do
        begin
          t1:=i*Pi/20; t2:=Random*Pi/12+0.1; h:=20+Random(200);
          glVertex3f(1000*Cos(t1-t2),0,1000*Sin(t1-t2));
          glVertex3f(1000*Cos(t1+t2),0,1000*Sin(t1+t2));
          glVertex3f(1000*Cos(t1)   ,h,1000*Sin(t1)   );
        end;
      glEnd();
      glDisable(GL_FOG);
    glEndList();
  end;

begin
  SetOpenGLMode(DirectOn);
  GetViewSettings(ViewInfo);
  with ViewInfo do glViewPort(x1,y1,x2-x1+1,y2-y1+1);
  InitScenery; DrawScenery;
  r:=20; t:=0; z:=5;
  i:=0;
  repeat
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(r*Sin(t*Pi/180),z,r*Cos(t*Pi/180),0,4,0,0,1,0);
    t:=t+1;
    Inc(i);
    if (i > 100) and (i < 200) then begin
                                      r:=r+2; z:=z+0.2;
                                    end;
    if (i > 300) and (i < 400) then begin
                                      r:=r-2; z:=z-0.2;
                                    end;
    if (i = 500) then i:=0;
    glLightfv(GL_LIGHT0,GL_POSITION,@lpos);
    glEnable(GL_LIGHT0);
    glCallList(ground); glCallList(house); glCallList(forest); glCallList(mountain);
    glDisable(GL_LIGHT0);
    glCallList(stars);
    UpdateGraph(UpdateNow);
  until KeyPressed;
  glDisable(GL_LIGHTING);
  glDeleteLists(stars,1);
  gluDeleteQuadric(quad);
  SetOpenGLMode(DirectOff);
end;

begin
  Init;
  OpenGLPlay;
end.

