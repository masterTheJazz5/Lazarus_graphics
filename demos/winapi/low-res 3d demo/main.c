/* Shitty Software Rendering Engine */
/* Feel free to use any of this */

/* Include windows headers */
#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* Define the PI constant */
#ifdef PI
#undef PI
#endif
#define PI 3.14159265359f

/* Define this for fullscreen */
/*#define FULLSCREEN*/
/* Rendering resolution */
#define XRES 640
#define YRES 480
/* Window size (ignored if FULLSCREEN is set) */
#define XSIZE 640
#define YSIZE 480
/* Near and far planes distance */
#define ZNEAR 0.25f
#define ZFAR 15.0f
/* Maximum scene graph nodes to draw */
#define MAXDRAWNODES 1024

/* Screen space vector, used for rasterization */
typedef struct
{
    /* X and Y are screen pixel coordinates */
    SHORT   X, Y;
    /* U and V are texel coordinates (0..63 range) */ 
    BYTE    U, V;
    /* F is the fog shade level (0 no fog, 255 full fog) */
    BYTE    F;
} VEC2, *LPVEC2;

/* 3D space vector */
typedef struct
{
    FLOAT   X, Y, Z;
} VEC3, *LPVEC3;

/* 4D space vector (not used much) */
typedef struct
{
    FLOAT   X, Y, Z, W;
} VEC4, *LPVEC4;

/* 4x4 matrix */
typedef struct
{
    union
    {
        FLOAT   M[4][4];
        VEC4    V[4];
        FLOAT   A[16];
    };
} MAT4, *LPMAT4;

/* Mesh vertex, contains position, normal and texture coordinates */
typedef struct
{
    /* Position */
    VEC3    P;
    /* Normal */
    VEC3    N;
    /* Normalized texture coordinates */
    FLOAT   U, V;
} VERTEX, *LPVERTEX;

/* A single model, a pair of vertices and texture */
typedef struct
{
    /* Vertex data, this has Triangles*3 vertices */
    LPVERTEX    Vertices;
    /* Number of triangles in the model */
    DWORD       Triangles;
    /* Texture data */
    LPBYTE      Texture;
} MODEL, *LPMODEL;

/* A node in the scene graph */
typedef struct NODE
{
    /* Node children */
    struct NODE**   Children;
    DWORD           ChildCount;
    /* Optional model associated with the node */
    LPMODEL         Model;
    /* Local transformation matrix */
    MAT4            Matrix;
} NODE, *LPNODE;

/* A scene (duh) */
typedef struct
{
    /* Camera position */
    VEC3    CamPos;
    /* Camera rotation */
    VEC3    CamRot;
    /* Root scene node */
    LPNODE  Root;
} SCENE, *LPSCENE;

/* LPENTITY is a pointer to an entity, defined here because it is
 * used in THINKPROC which is used in the ENTITY struct */
typedef struct ENTITY* LPENTITY;

/* Callback for "thinking" entities */
typedef void (*THINKPROC)(LPENTITY);

/* A single entity in the world */
typedef struct ENTITY
{
    /* The position of the entity, can be used to recreate the node matrix */
    VEC3        Position;
    /* Optional thinking callback, called 60 times per second */
    THINKPROC   Think;
    /* Optional scene graph node related to the entity */
    LPNODE      Node;
    /* Generic DWORD data associated with the entity */
    DWORD       dwTag;
    /* Generic FLOAT data associated with the entity */
    FLOAT       fTag;
    /* Generic LPVOID data associated with the entity */
    LPVOID      lpTag;
} ENTITY;

/* Render window handle and device context */
HWND hWnd;
HDC hDC;
/* As long as it is true the program is running */
BOOL Running = TRUE;
/* Rendering DIB section bitmap, device context and pixel data */
HBITMAP PixBmp;
HDC hPixDC;
LPBYTE Pixels;
/* Currently active texture used for rasterization */
LPBYTE Texels;
/* Fog table, first subscript is the level, second the base color index */
BYTE FogTable[256][256];
/* Matrices. ModelView is used for rendering and built from Model and View */
MAT4 Projection, ModelView, Model, View;
/* World entities */
LPENTITY* Entity;
DWORD Entities;
/* A bunch of models used for rendering the default scene */
LPMODEL Plane, Broken, Monkey, Cube, Column, Table, Ceil, Heart;

/* Multiply the second matrix with the first and sets the result to the first */
void MatMultiply(LPMAT4 dst, LPMAT4 src)
{
    MAT4 r;
    r.M[0][0] = dst->M[0][0]*src->M[0][0] + dst->M[0][1]*src->M[1][0] + dst->M[0][2]*src->M[2][0] + dst->M[0][3]*src->M[3][0];
    r.M[0][1] = dst->M[0][0]*src->M[0][1] + dst->M[0][1]*src->M[1][1] + dst->M[0][2]*src->M[2][1] + dst->M[0][3]*src->M[3][1];
    r.M[0][2] = dst->M[0][0]*src->M[0][2] + dst->M[0][1]*src->M[1][2] + dst->M[0][2]*src->M[2][2] + dst->M[0][3]*src->M[3][2];
    r.M[0][3] = dst->M[0][0]*src->M[0][3] + dst->M[0][1]*src->M[1][3] + dst->M[0][2]*src->M[2][3] + dst->M[0][3]*src->M[3][3];
    r.M[1][0] = dst->M[1][0]*src->M[0][0] + dst->M[1][1]*src->M[1][0] + dst->M[1][2]*src->M[2][0] + dst->M[1][3]*src->M[3][0];
    r.M[1][1] = dst->M[1][0]*src->M[0][1] + dst->M[1][1]*src->M[1][1] + dst->M[1][2]*src->M[2][1] + dst->M[1][3]*src->M[3][1];
    r.M[1][2] = dst->M[1][0]*src->M[0][2] + dst->M[1][1]*src->M[1][2] + dst->M[1][2]*src->M[2][2] + dst->M[1][3]*src->M[3][2];
    r.M[1][3] = dst->M[1][0]*src->M[0][3] + dst->M[1][1]*src->M[1][3] + dst->M[1][2]*src->M[2][3] + dst->M[1][3]*src->M[3][3];
    r.M[2][0] = dst->M[2][0]*src->M[0][0] + dst->M[2][1]*src->M[1][0] + dst->M[2][2]*src->M[2][0] + dst->M[2][3]*src->M[3][0];
    r.M[2][1] = dst->M[2][0]*src->M[0][1] + dst->M[2][1]*src->M[1][1] + dst->M[2][2]*src->M[2][1] + dst->M[2][3]*src->M[3][1];
    r.M[2][2] = dst->M[2][0]*src->M[0][2] + dst->M[2][1]*src->M[1][2] + dst->M[2][2]*src->M[2][2] + dst->M[2][3]*src->M[3][2];
    r.M[2][3] = dst->M[2][0]*src->M[0][3] + dst->M[2][1]*src->M[1][3] + dst->M[2][2]*src->M[2][3] + dst->M[2][3]*src->M[3][3];
    r.M[3][0] = dst->M[3][0]*src->M[0][0] + dst->M[3][1]*src->M[1][0] + dst->M[3][2]*src->M[2][0] + dst->M[3][3]*src->M[3][0];
    r.M[3][1] = dst->M[3][0]*src->M[0][1] + dst->M[3][1]*src->M[1][1] + dst->M[3][2]*src->M[2][1] + dst->M[3][3]*src->M[3][1];
    r.M[3][2] = dst->M[3][0]*src->M[0][2] + dst->M[3][1]*src->M[1][2] + dst->M[3][2]*src->M[2][2] + dst->M[3][3]*src->M[3][2];
    r.M[3][3] = dst->M[3][0]*src->M[0][3] + dst->M[3][1]*src->M[1][3] + dst->M[3][2]*src->M[2][3] + dst->M[3][3]*src->M[3][3];
    memcpy(dst, &r, sizeof(MAT4));
}

/* Like MatMultiply but the operands are reversed */
void MatReverseMultiply(LPMAT4 dst, LPMAT4 src)
{
    MAT4 r;
    r.M[0][0] = src->M[0][0]*dst->M[0][0] + src->M[0][1]*dst->M[1][0] + src->M[0][2]*dst->M[2][0] + src->M[0][3]*dst->M[3][0];
    r.M[0][1] = src->M[0][0]*dst->M[0][1] + src->M[0][1]*dst->M[1][1] + src->M[0][2]*dst->M[2][1] + src->M[0][3]*dst->M[3][1];
    r.M[0][2] = src->M[0][0]*dst->M[0][2] + src->M[0][1]*dst->M[1][2] + src->M[0][2]*dst->M[2][2] + src->M[0][3]*dst->M[3][2];
    r.M[0][3] = src->M[0][0]*dst->M[0][3] + src->M[0][1]*dst->M[1][3] + src->M[0][2]*dst->M[2][3] + src->M[0][3]*dst->M[3][3];
    r.M[1][0] = src->M[1][0]*dst->M[0][0] + src->M[1][1]*dst->M[1][0] + src->M[1][2]*dst->M[2][0] + src->M[1][3]*dst->M[3][0];
    r.M[1][1] = src->M[1][0]*dst->M[0][1] + src->M[1][1]*dst->M[1][1] + src->M[1][2]*dst->M[2][1] + src->M[1][3]*dst->M[3][1];
    r.M[1][2] = src->M[1][0]*dst->M[0][2] + src->M[1][1]*dst->M[1][2] + src->M[1][2]*dst->M[2][2] + src->M[1][3]*dst->M[3][2];
    r.M[1][3] = src->M[1][0]*dst->M[0][3] + src->M[1][1]*dst->M[1][3] + src->M[1][2]*dst->M[2][3] + src->M[1][3]*dst->M[3][3];
    r.M[2][0] = src->M[2][0]*dst->M[0][0] + src->M[2][1]*dst->M[1][0] + src->M[2][2]*dst->M[2][0] + src->M[2][3]*dst->M[3][0];
    r.M[2][1] = src->M[2][0]*dst->M[0][1] + src->M[2][1]*dst->M[1][1] + src->M[2][2]*dst->M[2][1] + src->M[2][3]*dst->M[3][1];
    r.M[2][2] = src->M[2][0]*dst->M[0][2] + src->M[2][1]*dst->M[1][2] + src->M[2][2]*dst->M[2][2] + src->M[2][3]*dst->M[3][2];
    r.M[2][3] = src->M[2][0]*dst->M[0][3] + src->M[2][1]*dst->M[1][3] + src->M[2][2]*dst->M[2][3] + src->M[2][3]*dst->M[3][3];
    r.M[3][0] = src->M[3][0]*dst->M[0][0] + src->M[3][1]*dst->M[1][0] + src->M[3][2]*dst->M[2][0] + src->M[3][3]*dst->M[3][0];
    r.M[3][1] = src->M[3][0]*dst->M[0][1] + src->M[3][1]*dst->M[1][1] + src->M[3][2]*dst->M[2][1] + src->M[3][3]*dst->M[3][1];
    r.M[3][2] = src->M[3][0]*dst->M[0][2] + src->M[3][1]*dst->M[1][2] + src->M[3][2]*dst->M[2][2] + src->M[3][3]*dst->M[3][2];
    r.M[3][3] = src->M[3][0]*dst->M[0][3] + src->M[3][1]*dst->M[1][3] + src->M[3][2]*dst->M[2][3] + src->M[3][3]*dst->M[3][3];
    memcpy(dst, &r, sizeof(MAT4));
}

/* Build an identity matrix */
void MatIdentity(LPMAT4 m)
{
    ZeroMemory(m, sizeof(MAT4));
    m->M[0][0] = m->M[1][1] = m->M[2][2] = m->M[3][3] = 1.0f;
}

/* Build a rotation matrix for a radians around the axis specified by x,y,z */
void MatRotation(LPMAT4 m, FLOAT x, FLOAT y, FLOAT z, FLOAT a)
{
    FLOAT s = (FLOAT)sin(a);
    FLOAT c = (FLOAT)cos(a);
    FLOAT t = 1 - c;
    m->M[0][0] = t*x*x + c;
    m->M[0][1] = t*x*y - z*s;
    m->M[0][2] = t*x*z + y*s;
    m->M[1][0] = t*x*y + z*s;
    m->M[1][1] = t*y*y + c;
    m->M[1][2] = t*y*z - x*s;
    m->M[2][0] = t*x*z - y*s;
    m->M[2][1] = t*y*z + x*s;
    m->M[2][2] = t*z*z + c;
    m->M[0][3] = m->M[1][3] = m->M[2][3] = m->M[3][0] = m->M[3][1] = m->M[3][2] = 0.0f;
    m->M[3][3] = 1.0f;
}

/* Build a translation matrix by x,y,z */
void MatTranslation(LPMAT4 m, FLOAT x, FLOAT y, FLOAT z)
{
    MatIdentity(m);
    m->M[0][3] = x;
    m->M[1][3] = y;
    m->M[2][3] = z;
}

/* Build a perspective matrix (this is basically the OpenGL GLU matrix) */
void MatPerspective(LPMAT4 m, FLOAT fovy, FLOAT aspect, FLOAT znear, FLOAT zfar)
{
    FLOAT f = 1.0f/(FLOAT)tan(fovy/2.0f);
    ZeroMemory(m, sizeof(MAT4));
    m->M[0][0] = f/aspect;
    m->M[1][1] = f;
    m->M[2][2] = (zfar + znear)/(znear - zfar);
    m->M[2][3] = (2.0f*zfar*znear)/(znear - zfar);
    m->M[3][2] = -1.0f;
}

/* Transform the src vector using m and store the result in dst */
void MatTransform(LPMAT4 m, LPVEC3 dst, LPVEC3 src)
{
    FLOAT x = src->X*m->M[0][0] + src->Y*m->M[0][1] + src->Z*m->M[0][2] + m->M[0][3];
    FLOAT y = src->X*m->M[1][0] + src->Y*m->M[1][1] + src->Z*m->M[1][2] + m->M[1][3];
    dst->Z = src->X*m->M[2][0] + src->Y*m->M[2][1] + src->Z*m->M[2][2] + m->M[2][3];
    dst->X = x;
    dst->Y = y;
}

/* Like MatTransform but using a 4D vector (mainly used for projection) */
void MatTransform4(LPMAT4 m, LPVEC4 dst, LPVEC4 src)
{
    FLOAT x = src->X*m->M[0][0] + src->Y*m->M[0][1] + src->Z*m->M[0][2] + src->W*m->M[0][3];
    FLOAT y = src->X*m->M[1][0] + src->Y*m->M[1][1] + src->Z*m->M[1][2] + src->W*m->M[1][3];
    FLOAT z = src->X*m->M[2][0] + src->Y*m->M[2][1] + src->Z*m->M[2][2] + src->W*m->M[2][3];
    dst->W = src->X*m->M[3][0] + src->Y*m->M[3][1] + src->Z*m->M[3][2] + src->W*m->M[3][3];
    dst->X = x;
    dst->Y = y;
    dst->Z = z;
}

/* Load texture data from the given RAW file */
LPBYTE LoadTexture(LPSTR filename)
{
    FILE* f = fopen(filename, "rb");
    LPBYTE texels = malloc(64*64);
    fread(texels, 1, 64*64, f);
    fclose(f);
    return texels;
}

/* Load a model in JTF format (see http://runtimeterror.com/tech/jtf/) */
LPMODEL LoadModel(LPSTR filename, LPSTR texfile)
{
    FILE* f = fopen(filename, "rb");
    CHAR magic[4];
    DWORD verflags;
    LPMODEL model;
    fread(magic, 4, 1, f);
    if (memcmp(magic, "JTF!", 4))
    {
        fclose(f);
        return NULL;
    }
    fread(&verflags, 4, 1, f);
    if (verflags != 0)
    {
        fclose(f);
        return NULL;
    }
    model = malloc(sizeof(MODEL));
    fread(&model->Triangles, 4, 1, f);
    model->Vertices = malloc(sizeof(VERTEX)*3*model->Triangles);
    fread(model->Vertices, sizeof(VERTEX)*3*model->Triangles, 1, f);
    fclose(f);
    model->Texture = LoadTexture(texfile);
    return model;
}

/* Create a new scene graph node, model and mat are optional */
LPNODE NewNode(LPNODE parent, LPMODEL model, LPMAT4 mat)
{
    LPNODE node = malloc(sizeof(NODE));
    node->Children = NULL;
    node->ChildCount = 0;
    node->Model = model;
    if (mat)
    {
        memcpy(node->Matrix.A, mat, sizeof(MAT4));
    }
    else
    {
        MatIdentity(&node->Matrix);
    }
    if (parent)
    {
        parent->Children = realloc(parent->Children, sizeof(LPVOID)*(parent->ChildCount + 1));
        parent->Children[parent->ChildCount++] = node;
    }
    return node;
}

/* Delete the given node and its children */
void DeleteNode(LPNODE node)
{
    DWORD i;
    for (i=0; i < node->ChildCount; i++) DeleteNode(node->Children[i]);
    free(node->Children);
    free(node);
}

/* Create a new scene and its root node */
LPSCENE NewScene(void)
{
    LPSCENE scene = calloc(1, sizeof(SCENE));
    scene->Root = NewNode(NULL, NULL, NULL);
    return scene;
}

/* Delete the given scene and its nodes */
void DeleteScene(LPSCENE scene)
{
    DeleteNode(scene->Root);
    free(scene);
}

/* Draw a scanline left to right with the given endpoint coordinates */
void DrawScanlineLTR(LONG y, LONG x1, LONG x2, LONG u1, LONG v1, LONG u2, LONG v2, LONG f1, LONG f2)
{
    LONG d, dU, dV, dF;
    d = x2 - x1;
    if (!d) return;
    /* Calculate deltas */
    dU = (u2 - u1)/d;
    dV = (v2 - v1)/d;
    dF = (f2 - f1)/d;
    /* Handle clipping */
    if (x1 < 0)
    {
        d = -x1;
        x1 = 0;
        u1 += d*dU;
        v1 += d*dV;
        f1 += d*dF;
    }
    if (x2 > XRES) x2 = XRES;
    /* Scanline rasterization loop */
    for (; x1<x2; x1++)
    {
        Pixels[y*XRES + x1] = FogTable[f1 >> 16][Texels[(((v1 >> 16)&63) << 6) + ((u1 >> 16)&63)]];
        u1 += dU;
        v1 += dV;
        f1 += dF;
    }
}

/* Draw a scanline with the given endpoint coordinates */
void DrawScanline(LONG y, LONG x1, LONG x2, LONG u1, LONG v1, LONG u2, LONG v2, LONG f1, LONG f2)
{
    /* Swap endpoints if needed to ensure they are always from left to right */
    if (x1 < x2)
        DrawScanlineLTR(y, x1, x2, u1, v1, u2, v2, f1, f2);
    else
        DrawScanlineLTR(y, x2, x1, u2, v2, u1, v1, f2, f1);
}

/* Draw a 2D screen projected triangle with the given vertices */
void DrawTriangle2D(LPVEC2 a, LPVEC2 b, LPVEC2 c)
{
    LPVEC2 tvp;
    LONG dS, dL, dX1, dX2, y, y2, x1, x2;
    LONG dSU, dSV, dLU, dLV, u1, v1, u2, v2;
    LONG dSF, dLF, f1, f2;
    /* Sort vertices from top to bottom */
    if (b->Y < a->Y) { tvp = a; a = b; b = tvp; }
    if (c->Y < b->Y) { tvp = b; b = c; c = tvp; }
    if (b->Y < a->Y) { tvp = a; a = b; b = tvp; }
    /* Ignore single scanline triangles */
    if (a->Y == c->Y) return;
    /* Calculate deltas for the long edge of the triangle */
    dL = c->Y - a->Y;
    dX2 = ((c->X - a->X) << 16)/dL;
    dLU = ((c->U - a->U) << 16)/dL;
    dLV = ((c->V - a->V) << 16)/dL;
    dLF = ((c->F - a->F) << 16)/dL;
    /* Initial long edge values */
    x2 = a->X << 16;
    u2 = a->U << 16;
    v2 = a->V << 16;
    f2 = a->F << 16;
    /* Rasterize upper part of the triangle */
    dS = b->Y - a->Y;
    if (dS)
    {
        /* Calculate deltas for the upper short edge */
        dX1 = ((b->X - a->X) << 16)/dS;
        dSU = ((b->U - a->U) << 16)/dS;
        dSV = ((b->V - a->V) << 16)/dS;
        dSF = ((b->F - a->F) << 16)/dS;
        /* Initial upper short edge values */
        x1 = x2;
        u1 = u2;
        v1 = v2;
        f1 = f2;
        /* Handle clipping */
        y = a->Y;
        if (y < 0)
        {
            x1 += dX1*-y;
            x2 += dX2*-y;
            u1 += dSU*-y;
            v1 += dSV*-y;
            u2 += dLU*-y;
            v2 += dLV*-y;
            f1 += dSF*-y;
            f2 += dLF*-y;
            y = 0;
        }
        y2 = b->Y;
        if (y2 > YRES) y2 = YRES;
        /* Upper part rasterization loop */
        for (; y < y2; y++)
        {
            DrawScanline(y, x1 >> 16, x2 >> 16, u1, v1, u2, v2, f1, f2);
            x1 += dX1;
            x2 += dX2;
            u1 += dSU;
            v1 += dSV;
            u2 += dLU;
            v2 += dLV;
            f1 += dSF;
            f2 += dLF;
        }
    }
    /* Recalculate long edge positions in case they were clipped */
    x2 = (a->X << 16) + dX2*dS;
    u2 = (a->U << 16) + dLU*dS;
    v2 = (a->V << 16) + dLV*dS;
    f2 = (a->F << 16) + dLF*dS;
    /* Rasterize lower part of the triangle */
    dS = c->Y - b->Y;
    if (dS)
    {
        /* Calculate deltas for the lower short edge */
        dX1 = ((c->X - b->X) << 16)/dS;
        dSU = ((c->U - b->U) << 16)/dS;
        dSV = ((c->V - b->V) << 16)/dS;
        dSF = ((c->F - b->F) << 16)/dS;
        /* Initial lower short edge values */
        x1 = b->X << 16;
        u1 = b->U << 16;
        v1 = b->V << 16;
        f1 = b->F << 16;
        /* Handle clipping */
        y = b->Y;
        if (y < 0)
        {
            x1 += dX1*-y;
            x2 += dX2*-y;
            u1 += dSU*-y;
            v1 += dSV*-y;
            u2 += dLU*-y;
            v2 += dLV*-y;
            f1 += dSF*-y;
            f2 += dLF*-y;
            y = 0;
        }
        y2 = c->Y;
        if (y2 > YRES) y2 = YRES;
        /* Lower part rasterization loop */
        for (; y < y2; y++)
        {
            DrawScanline(y, x1 >> 16, x2 >> 16, u1, v1, u2, v2, f1, f2);
            x1 += dX1;
            x2 += dX2;
            u1 += dSU;
            v1 += dSV;
            u2 += dLU;
            v2 += dLV;
            f1 += dSF;
            f2 += dLF;
        }
    }
}

/* Draw a triangle in 3D space at the given vertices */
void DrawTriangle(LPVERTEX a, LPVERTEX b, LPVERTEX c)
{
    VEC4 pa, pb, pc;
    VEC2 sa, sb, sc;
    INT fog;
    /* Transform the vertices from world space to camera space */
    MatTransform(&ModelView, (LPVEC3)&pa, &a->P);
    MatTransform(&ModelView, (LPVEC3)&pb, &b->P);
    MatTransform(&ModelView, (LPVEC3)&pc, &c->P);
    /* Ignore triangles behind the near plane */
    if (pa.Z < ZNEAR && pb.Z < ZNEAR && pc.Z < ZNEAR) return;
    /* Clamp vertices to near plane (normally the triangle would be
     * clipped here, potentially creating a new triangle, but eh, as long
     * as there is enough tesselation, this is a good enough approximation */
    if (pa.Z < ZNEAR) pa.Z = ZNEAR;
    if (pb.Z < ZNEAR) pb.Z = ZNEAR;
    if (pc.Z < ZNEAR) pc.Z = ZNEAR;
    /* Prepare projected vertices */
    ZeroMemory(&sa, sizeof(VEC2));
    ZeroMemory(&sb, sizeof(VEC2));
    ZeroMemory(&sc, sizeof(VEC2));
    /* Calculate fog levels (index to the fog table's first subscript) */
    fog = (INT)(255.0f*(1.0f - (pa.Z - ZNEAR)/(ZFAR - ZNEAR)));
    if (fog < 0) fog = 0; else if (fog > 255) fog = 255;
    sa.F = (BYTE)fog;
    fog = (INT)(255.0f*(1.0f - (pb.Z - ZNEAR)/(ZFAR - ZNEAR)));
    if (fog < 0) fog = 0; else if (fog > 255) fog = 255;
    sb.F = (BYTE)fog;
    fog = (INT)(255.0f*(1.0f - (pc.Z - ZNEAR)/(ZFAR - ZNEAR)));
    if (fog < 0) fog = 0; else if (fog > 255) fog = 255;
    sc.F = (BYTE)fog;
    /* Project the vertices from camera space to screen space */
    pa.W = pb.W = pc.W = 1.0f;
    MatTransform4(&Projection, &pa, &pa);
    MatTransform4(&Projection, &pb, &pb);
    MatTransform4(&Projection, &pc, &pc);
    pa.X /= pa.W; pa.Y /= pa.W; pa.Z /= pa.W;
    pb.X /= pb.W; pb.Y /= pb.W; pb.Z /= pb.W;
    pc.X /= pc.W; pc.Y /= pc.W; pc.Z /= pc.W;
    /* Ignore back facing triangles */
    if ((pc.X-pa.X)*(pc.Y-pb.Y) - (pc.X-pb.X)*(pc.Y-pa.Y) > 0.0f)
        return;
    /* Ignore triangles that are completely outside the screen */
    if ((pa.X < -1 && pb.X < -1 && pc.X < -1) ||
        (pa.Y < -1 && pb.Y < -1 && pc.Y < -1) ||
        (pa.X > 1 && pb.X > 1 && pc.X > 1) ||
        (pa.Y > 1 && pb.Y > 1 && pc.Y > 1)) return;
    /* Calculate the pixel coordinates for the vertices */
    /* NOTE: if you want to add subpixel precision change the data type to
     * LONG for the X and Y in VEC2, multiply these by 65536 and remove the
     * << 16 from x/y values in DrawTriangle2D (ensure you also take
     * dL and dS into account).  Similarly, if you want subtexel precision
     * change the BYTE to SHORT or LONG for U and V, multiply them here by
     * 65536 and remove the << 16 and >> 16 from anywhere in DrawTriangle
     * DrawScanlineLTR except those in the innermost loop. */
    sa.X = (XRES/2) + (SHORT)(pa.X*XRES*0.5f);
    sa.Y = (YRES/2) + (SHORT)(pa.Y*YRES*0.5f);
    sa.U = (BYTE)(a->U*63.0f); sa.V = (BYTE)(a->V*63.0f);
    sb.X = (XRES/2) + (SHORT)(pb.X*XRES*0.5f);
    sb.Y = (YRES/2) + (SHORT)(pb.Y*YRES*0.5f);
    sb.U = (BYTE)(b->U*63.0f); sb.V = (BYTE)(b->V*63.0f);
    sc.X = (XRES/2) + (SHORT)(pc.X*XRES*0.5f);
    sc.Y = (YRES/2) + (SHORT)(pc.Y*YRES*0.5f);
    sc.U = (BYTE)(c->U*63.0f); sc.V = (BYTE)(c->V*63.0f);
    /* Rasterize the triangle */
    DrawTriangle2D(&sa, &sb, &sc);
}

/* Draw the given 3D model (updates ModelView from Model and View) */
void DrawModel(LPMODEL model)
{
    DWORD i = 0, t;
    Texels = model->Texture;
    MatIdentity(&ModelView);
    MatMultiply(&ModelView, &View);
    MatMultiply(&ModelView, &Model);
    for (t=model->Triangles; t; t--)
    {
        DrawTriangle(model->Vertices + i, model->Vertices + i + 1, model->Vertices + i + 2);
        i += 3;
    }
}

/* Struct used to keep the node models to draw */
typedef struct DRAWNODE
{
    /* Global transformation matrix */
    MAT4                Mat;
    /* The model to draw */
    LPMODEL             Model;
    /* Reference distance used for sorting */
    FLOAT               Ref;
} DRAWNODE, *LPDRAWNODE;
/* The nodes to draw, this is filled and sorted later */
DRAWNODE    DrawNode[MAXDRAWNODES];
DWORD       DrawNodes;
DWORD       DrawnNodes; /* This is used to report the node count */

void DrawSceneNode(LPNODE node)
{
    /* Save the current model matrix */
    MAT4 saveModel = Model;
    DWORD i;
    /* Abort if we reached the maximum nodes to draw */
    if (DrawNodes == MAXDRAWNODES) return;
    /* Multiply the model matrix with the node's own local matrix */
    MatReverseMultiply(&Model, &node->Matrix);
    /* Add the model of this node if the node has one */
    if (node->Model)
    {
        /* Calculate a projected reference vector for use with sorting */
        VEC4 RefVec = {0, 0, 0, 0};
        MatTransform(&Model, (LPVEC3)&RefVec, (LPVEC3)&RefVec);
        MatTransform(&View, (LPVEC3)&RefVec, (LPVEC3)&RefVec);
        RefVec.W = 1.0f;
        MatTransform4(&Projection, &RefVec, &RefVec);
        if (fabs(RefVec.W) > 0.01f)
        {
            RefVec.X /= RefVec.W;
            RefVec.Y /= RefVec.W;
        }
        else
        {
            RefVec.X = RefVec.Y = RefVec.Z = 0.0f;
        }
        /* If the reference vector is inside the view code (with some
         * handwavey threshold) add it to the draw nodes array */
        if (RefVec.Z > -ZFAR-3 && RefVec.Z < -ZNEAR &&
            RefVec.X > -3 && RefVec.X < 3)
        {
            DrawNode[DrawNodes].Mat = Model;
            DrawNode[DrawNodes].Model = node->Model;
            DrawNode[DrawNodes].Ref = -RefVec.Z;
            DrawNodes++;
        }
    }
    /* Process the node's children */
    for (i=0; i < node->ChildCount; i++)
    {
        DrawSceneNode(node->Children[i]);
    }
    /* Restore the model matrix */
    Model = saveModel;
}

/* Comparison function used by qsort to sort the nodes */
#ifdef __WATCOMC__
int __watcall DrawSceneCmp(const void* e1, const void* e2)
#else
int __cdecl DrawSceneCmp(const void* e1, const void* e2)
#endif
{
    LPDRAWNODE dn1 = (LPDRAWNODE)e1;
    LPDRAWNODE dn2 = (LPDRAWNODE)e2;
    return dn1->Ref < dn2->Ref ? -1 : (dn1->Ref > dn2->Ref ? 1 : 0);
}

/* Draw the given scene (resets all matrices) */
void DrawScene(LPSCENE scene)
{
    MAT4 tmp;
    /* Calculate view matrix for the scene's camera */
    MatIdentity(&View);
    MatTranslation(&tmp, -scene->CamPos.X, -scene->CamPos.Y, -scene->CamPos.Z);
    MatReverseMultiply(&View, &tmp);
    MatRotation(&tmp, 0, 0, 1, scene->CamRot.Z);
    MatReverseMultiply(&View, &tmp);
    MatRotation(&tmp, 0, 1, 0, scene->CamRot.Y);
    MatReverseMultiply(&View, &tmp);
    MatRotation(&tmp, 1, 0, 0, scene->CamRot.X);
    MatReverseMultiply(&View, &tmp);
    /* Figure out the nodes to draw */
    MatIdentity(&Model);
    DrawNodes = 0;
    DrawSceneNode(scene->Root);
    /* Sort the nodes from back to front */
    qsort(DrawNode, DrawNodes, sizeof(DRAWNODE), DrawSceneCmp);
    /* Keep the node count for later reporting */
    DrawnNodes = DrawNodes;
    /* Draw the nodes */
    while (DrawNodes > 0)
    {
        DrawNodes--;
        Model = DrawNode[DrawNodes].Mat;
        DrawModel(DrawNode[DrawNodes].Model);
    }
}

/* Create a new entity with the given position, think proc and node (both
 * are optional, if node is given its matrix will be set for the position) */
LPENTITY NewEntity(FLOAT x, FLOAT y, FLOAT z, THINKPROC think, LPNODE node)
{
    LPENTITY ent = calloc(1, sizeof(ENTITY));
    ent->Position.X = x;
    ent->Position.Y = y;
    ent->Position.Z = z;
    ent->Think = think;
    ent->Node = node;
    if (node) MatTranslation(&ent->Node->Matrix, x, y, z);
    Entity = realloc(Entity, sizeof(LPENTITY)*(Entities + 1));
    Entity[Entities++] = ent;
    return ent;
}

/* Delete the given entity */
void DeleteEntity(LPENTITY ent)
{
    DWORD i;
    for (i=0; i < Entities; i++)
        if (Entity[i] == ent)
        {
            memmove(Entity + i, Entity + i + 1, sizeof(LPENTITY)*(Entities - i - 2));
            break;
        }
    free(ent);
}

/* Think procedure for the heart entity */
void HeartThink(LPENTITY ent)
{
    /* Rotate the associated node */
    MatRotation(&ent->Node->Matrix, 0, 1, 0, ent->fTag + ent->Position.X + ent->Position.Z);
    /* Adjust the node's position to move up and down */
    ent->Node->Matrix.M[0][3] = ent->Position.X;
    ent->Node->Matrix.M[1][3] = ent->Position.Y + (FLOAT)sin(ent->fTag*0.5f + ent->Position.X + ent->Position.Z)*0.2f;
    ent->Node->Matrix.M[2][3] = ent->Position.Z;
    /* Update the tag for the next rotation and motion */
    ent->fTag += 0.05f;
}

/* Window procedure, it just posts WM_QUIT when the window is destroyed */
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    }
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/* Return the number of milliseconds since application startup */
DOUBLE GetMillis(void)
{
    static DOUBLE start, freq;
    LARGE_INTEGER t;
    if (!freq)
    {
        QueryPerformanceFrequency(&t);
        freq = (((DOUBLE)t.HighPart)*(DOUBLE)0xFFFFFFFFUL + (DOUBLE)t.LowPart)/1000.0;
        QueryPerformanceCounter(&t);
        start = ((DOUBLE)t.HighPart)*(DOUBLE)0xFFFFFFFFUL + (DOUBLE)t.LowPart;
    }
    QueryPerformanceCounter(&t);
    return ((((DOUBLE)t.HighPart)*(DOUBLE)0xFFFFFFFFUL + (DOUBLE)t.LowPart) - start)/freq;
}

/* Windows entry point */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    RECT r;
    DWORD style;
    MSG msg;
    DOUBLE last;
#ifndef FULLSCREEN
    DOUBLE report = 0;
#endif
    LPSCENE scene;
    union
    {
        BITMAPINFO BMI;
        struct
        {
            BITMAPINFOHEADER bmiHeader;
            RGBQUAD bmiColor[256];
        } s;
    } BMI;
    
    //Jazz thanks you very much, mister! FInally, some Windows only drawing GC code! 
    WNDCLASS wc = {0};
    
    /* Register the window class */
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hInstance = hInstance;
    wc.lpfnWndProc = WndProc;
    wc.lpszClassName = "SSREWINCLASS";
    RegisterClass(&wc);
    /* Create the main window (adjust these for different resolutions */
#ifdef FULLSCREEN
    GetClientRect(GetDesktopWindow(), &r);
    style = WS_POPUP;
    ShowCursor(FALSE);
#else
    r.right = XSIZE;
    r.bottom = YSIZE;
    r.left = r.top = 0;
    style = WS_CAPTION|WS_SYSMENU|WS_BORDER|WS_MINIMIZEBOX;
    AdjustWindowRectEx(&r, style, FALSE, WS_EX_CLIENTEDGE);
#endif
    hWnd = CreateWindowEx(
#ifdef FULLSCREEN
        WS_EX_TOPMOST,
#else
        WS_EX_CLIENTEDGE,
#endif
        "SSREWINCLASS", "SSRE", style,
        CW_USEDEFAULT, CW_USEDEFAULT,
        r.right - r.left, r.bottom - r.top,
        NULL, NULL, hInstance, NULL);
    ShowWindow(hWnd, nShowCmd);
    /* Obtain the device context for the window */
    hDC = GetDC(hWnd);
    
    /* Prepare the DIB section bitmap  */
    hPixDC = CreateCompatibleDC(hDC);
    ZeroMemory(&BMI, sizeof(BMI));
    /* Bitmap header info */
    BMI.s.bmiHeader.biSize = sizeof(BMI.s.bmiHeader);
    BMI.s.bmiHeader.biWidth = XRES;
    BMI.s.bmiHeader.biHeight = -YRES;
    BMI.s.bmiHeader.biPlanes = 1;
    BMI.s.bmiHeader.biBitCount = 8;
    BMI.s.bmiHeader.biCompression = BI_RGB;
    /* This calculates the palette and fog table */
    {
        INT r, g, b, i, s;
        /* The first 216 colors are the so-called web-safe palette */
        i = 0;
        for (r=0; r < 6; r++)
            for (g=0; g < 6; g++)
                for (b=0; b < 6; b++, i++)
                {
                    BMI.s.bmiColor[b+g*6+r*36].rgbRed = (r*3) << 4;
                    BMI.s.bmiColor[b+g*6+r*36].rgbGreen = (g*3) << 4;
                    BMI.s.bmiColor[b+g*6+r*36].rgbBlue = (b*3) << 4;
                }
        /* The rest of the colors are a grayscale gradient */
        for (r=0; r < 41; r++)
        {
            BMI.s.bmiColor[r + 215].rgbRed =
            BMI.s.bmiColor[r + 215].rgbGreen =
            BMI.s.bmiColor[r + 215].rgbBlue = (BYTE)((FLOAT)r*6.375f);
        }
        /* Calculate the fog table (s=level, i=index) */
        for (s=0; s < 256; s++)
        {
            FLOAT d = (1.0f/255.0f)*(FLOAT)s;
            /* Fog color */
            VEC3 fog = {0.75f,0.85f,0.81f};/*{0.72f, 0.73f, 0.77f};*/
            /* Calculate the closest colors for this level */
            for (i=0; i < 256; i++)
            {
                VEC3 tcol;
                INT tr, tg, tb;
                INT j, closest = -1;
                FLOAT closestDist = 0;
                /* Calculate the target color we want to achieve */
                tcol.X = fog.X*(1.0f-d) + ((FLOAT)BMI.s.bmiColor[i].rgbRed)/255.0f*d;
                tcol.Y = fog.Y*(1.0f-d) + ((FLOAT)BMI.s.bmiColor[i].rgbGreen)/255.0f*d;
                tcol.Z = fog.Z*(1.0f-d) + ((FLOAT)BMI.s.bmiColor[i].rgbBlue)/255.0f*d;
                tr = (INT)(tcol.X*255.0f);
                tg = (INT)(tcol.Y*255.0f);
                tb = (INT)(tcol.Z*255.0f);
                /* Find the closest color in the base palette to the target */
                for (j=0; j < 256; j++)
                {
                    INT dr = tr - BMI.s.bmiColor[j].rgbRed;
                    INT dg = tg - BMI.s.bmiColor[j].rgbGreen;
                    INT db = tb - BMI.s.bmiColor[j].rgbBlue;
                    FLOAT dist = (FLOAT)(dr*dr + dg*dg + db*db);
                    if (closest == -1 || dist < closestDist)
                    {
                        closest = j;
                        closestDist = dist;
                    }
                }
                FogTable[s][i] = closest;
            }
        }
    }
    /* Create and select the DIB section bitmap */
    PixBmp = CreateDIBSection(hPixDC, &BMI.BMI, DIB_RGB_COLORS, (LPVOID)&Pixels, NULL, 0);
    SelectObject(hPixDC, PixBmp);
    /* Load some models */
    Plane = LoadModel("plane.jtf", "tex1.raw");
    Broken = LoadModel("broken.jtf", "tex4.raw");
    Column = LoadModel("column.jtf", "tex2.raw");
    Table = LoadModel("table.jtf", "tex3.raw");
    Cube = LoadModel("cube.jtf", "tex5.raw");
    Ceil = LoadModel("ceil.jtf", "tex6.raw");
    Heart = LoadModel("heart.jtf", "heart.raw");
    /* Create some scene */
    scene = NewScene();
    scene->CamRot.Y = 1;
    {
        LPSTR tiles = "==========================="
                      "= h x = x h h*  hT  T  T  ="
                      "= ****=****   x           ="
                      "=   x hx h         x    h ="
                      "===   *   *  * h *   ***  ="
                      "=h*   T   *x=====* h h *x ="
                      "=h*   hx  * = x =        h="
                      "=h* T T T * = h =h    .x  ="
                      "=h*  x   h* =  x     * *  ="
                      "=h* T T=  ===T    T   x   ="
                      "=h*****=   h====  x     T ="
                      "=  x h   Txh=        T    ="
                      "===========================";
        INT x, z;
        /* Create main nodes and entities */
        for (z=-6; z <= 6; z++)
            for (x=-13; x <= 13; x++)
            {
                MAT4 mat;
                LPMODEL mdl = Plane;
                switch (tiles[(z+6)*27 + x+13])
                {
                case '.': scene->CamPos.X = (FLOAT)x*2; scene->CamPos.Z = (FLOAT)z*2; break;
                case '*': mdl = Column; break;
                case 'T': mdl = Table; break;
                case '=': mdl = Cube; break;
                case 'x': mdl = Broken; break;
                }
                MatTranslation(&mat, (FLOAT)x*2, 0, (FLOAT)z*2);
                NewNode(scene->Root, mdl, &mat);
                switch (tiles[(z+6)*27 + x+13])
                {
                case 'h': NewEntity((FLOAT)x*2, 0, (FLOAT)z*2, HeartThink, NewNode(scene->Root, Heart, NULL)); break;
                }
            }
        /* Create nodes for the ceiling */ 
        for (z=-8; z <= 8; z++)
            for (x=-15; x <= 15; x++)
            {
                MAT4 mat;
                MatTranslation(&mat, (FLOAT)x*2, 3, (FLOAT)z*2);
                NewNode(scene->Root, Ceil, &mat);
            }
    }
    /* Setup the projection matrix (we use the window size for the aspect ratio regardless
     * of render resolution so that even in fullscreen -e.g.- 320x240 we'd get a 16:9
     * aspect ratio if the user has a 16:9 monitor) */
    MatPerspective(&Projection, 50.0f*PI/180.0f, (FLOAT)r.right/(FLOAT)r.bottom, ZNEAR, ZFAR);
    /* Main loop */
    last = GetMillis();
    while (Running)
    {
#ifndef FULLSCREEN
        DOUBLE pre, post;
#endif
        /* Handle windows messages */
        while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT) Running = FALSE;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        /* Update loop that runs ~60 times per second */
        while (GetMillis() - last > 16)
        {
            DWORD i;
            /* Handle movement and look keys */
            if (GetKeyState(VK_END)&0x8000) scene->CamRot.X = 0;
            if (GetKeyState(VK_PRIOR)&0x8000) scene->CamRot.X += 0.0125f;
            if (GetKeyState(VK_NEXT)&0x8000) scene->CamRot.X -= 0.0125f;
            if (GetKeyState(VK_LEFT)&0x8000) scene->CamRot.Y -= 0.025f;
            if (GetKeyState(VK_RIGHT)&0x8000) scene->CamRot.Y += 0.025f;
            if ((GetKeyState(VK_UP)&0x8000) || (GetKeyState(VK_DOWN)&0x8000))
            {
                MAT4 tmp;
                VEC3 fw = {0, 0, 0.025f};
                if (GetKeyState(VK_DOWN)&0x8000) fw.Z = -fw.Z;
                MatRotation(&tmp, 0, -1, 0, scene->CamRot.Y);
                MatTransform(&tmp, &fw, &fw);
                scene->CamPos.X += fw.X;
                scene->CamPos.Y += fw.Y;
                scene->CamPos.Z += fw.Z;
            }
            /* Update entities */
            for (i=0; i < Entities; i++)
                if (Entity[i]->Think)
                    Entity[i]->Think(Entity[i]);
            last += 16;
        }
#ifndef FULLSCREEN
        /* Store time before rendering */
        pre = GetMillis();
#endif
        /* Clear the render buffer to the foggiest color */
        memset(Pixels, FogTable[0][0], XRES*YRES);
        /* Render the scene */
        scene->CamPos.Y = 1 + (FLOAT)sin(Entity[0]->fTag)*0.01f;
        DrawScene(scene);
        /* Draw the render buffer in the window */
        GetClientRect(hWnd, &r);
        StretchBlt(hDC, 0, 0, r.right, r.bottom, hPixDC, 0, 0, XRES, YRES, SRCCOPY);
        /* Update the window title with stats every ~10ms */
#ifndef FULLSCREEN
        /* Store time after rendering */
        post = GetMillis();
        if (post - report > 10)
        {
            CHAR tmp[256];
            report = post;
            if (post == pre)
            {
                sprintf(tmp, "Above 1000 fps");
            }
            else
            {
                sprintf(tmp, "%0.2f FPS (%0.2f ms) models=%i", 1000.0/(post - pre), post - pre, (int)DrawnNodes);
            }
            SetWindowText(hWnd, tmp);
        }
#endif
    }
    return 0;
}
