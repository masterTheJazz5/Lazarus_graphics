program Quadratic_equation;
procedure linear(a: double; b: double);
var x: double;
begin
  if a=0 then
    if b=0 then
      writeln('identity equation')
    else
      writeln('contrary equation')
  else begin
    x:=-b/a;
    writeln('Linear equation, x = ',x);
  end;
end;
var a,b,c,delta,x,x1,x2: double;
begin
  write('a = ');
  readln(a);
  write('b = ');
  readln(b);
  write('c = ');
  readln(c);
  if a=0 then
    linear(b,c)
  else begin
    delta:=b*b-4*a*c;
    if delta<0 then
      writeln('lack of solutions')
    else
    if delta=0 then begin
      x:=-b/(2*a);
      writeln('one solution x = ',x);
    end else begin
      x1:=(-b+sqrt(delta))/(2*a);
      x2:=(-b-sqrt(delta))/(2*a);
      writeln('x1 = ',x1);
      writeln('x2 = ',x2);
    end;
  end;
end.
