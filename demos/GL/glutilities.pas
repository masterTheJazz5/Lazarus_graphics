unit GLUtilities;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Maths, Textures;

procedure DrawWireframeBox(const Box: TAABox);
procedure DrawSolidBox(const Box: TAABox);
procedure DrawSprite(const C, CamRight, CamUp: TVector; const Sprite: TTexture; Size: Double);
procedure DrawSprite(const C, CamRight, CamUp: TVector; SpriteGLTexture: Integer; Size: Double);
procedure DrawPlane(const P: TPlane; Size: Double; const Color: TExtColor);

implementation

uses
  GL, GLext, UtilMeshData;

procedure DrawWireframeBox(const Box: TAABox);
begin
  glBegin(GL_LINE_STRIP);
  glVertex3d(Box.a.x, Box.a.y, Box.a.z);
  glVertex3d(Box.b.x, Box.a.y, Box.a.z);
  glVertex3d(Box.b.x, Box.a.y, Box.b.z);
  glVertex3d(Box.a.x, Box.a.y, Box.b.z);
  glVertex3d(Box.a.x, Box.a.y, Box.a.z);
  glEnd;
  glBegin(GL_LINE_STRIP);
  glVertex3d(Box.a.x, Box.b.y, Box.a.z);
  glVertex3d(Box.b.x, Box.b.y, Box.a.z);
  glVertex3d(Box.b.x, Box.b.y, Box.b.z);
  glVertex3d(Box.a.x, Box.b.y, Box.b.z);
  glVertex3d(Box.a.x, Box.b.y, Box.a.z);
  glEnd;
  glBegin(GL_LINES);
  glVertex3d(Box.a.x, Box.a.y, Box.a.z);
  glVertex3d(Box.a.x, Box.b.y, Box.a.z);
  glVertex3d(Box.b.x, Box.a.y, Box.a.z);
  glVertex3d(Box.b.x, Box.b.y, Box.a.z);
  glVertex3d(Box.a.x, Box.a.y, Box.b.z);
  glVertex3d(Box.a.x, Box.b.y, Box.b.z);
  glVertex3d(Box.b.x, Box.a.y, Box.b.z);
  glVertex3d(Box.b.x, Box.b.y, Box.b.z);
  glEnd;
end;

procedure DrawSolidBox(const Box: TAABox);
var
  C, E: TVector;
  I: Integer;
begin
  C:=Box.Center;
  E:=Box.Extent;
  I:=0;
  glBegin(GL_TRIANGLES);
  while I < Length(CubeVertices) do begin
    glVertex3f(CubeVertices[I]*E.x + C.x, CubeVertices[I + 1]*E.y + C.y, CubeVertices[I + 2]*E.z + C.z);
    Inc(I, 3);
  end;
  glEnd();
end;

procedure DrawSprite(const C, CamRight, CamUp: TVector; const Sprite: TTexture; Size: Double);
begin
  DrawSprite(C, CamRight, CamUp, Sprite.GLName, Size);
end;

procedure DrawSprite(const C, CamRight, CamUp: TVector; SpriteGLTexture: Integer; Size: Double);
begin
  Size:=Size*0.5;
  glPushAttrib(GL_COLOR_BUFFER_BIT or GL_CURRENT_BIT or GL_ENABLE_BIT or GL_LIGHTING_BIT);
  glDisable(GL_LIGHTING);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, SpriteGLTexture);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBegin(GL_QUADS);
  glTexCoord2d(1, 0); glVertex3d(C.X + CamRight.X*Size + CamUp.X*Size, C.Y + CamRight.Y*Size + CamUp.Y*Size, C.Z + CamRight.Z*Size + CamUp.Z*Size);
  glTexCoord2d(0, 0); glVertex3d(C.X - CamRight.X*Size + CamUp.X*Size, C.Y - CamRight.Y*Size + CamUp.Y*Size, C.Z - CamRight.Z*Size + CamUp.Z*Size);
  glTexCoord2d(0, 1); glVertex3d(C.X - CamRight.X*Size - CamUp.X*Size, C.Y - CamRight.Y*Size - CamUp.Y*Size, C.Z - CamRight.Z*Size - CamUp.Z*Size);
  glTexCoord2d(1, 1); glVertex3d(C.X + CamRight.X*Size - CamUp.X*Size, C.Y + CamRight.Y*Size - CamUp.Y*Size, C.Z + CamRight.Z*Size - CamUp.Z*Size);
  glEnd;
  glPopAttrib;
end;

procedure DrawPlane(const P: TPlane; Size: Double; const Color: TExtColor);
var
  V: array [0..3] of TVector;
  C: TVector;
  I: Integer;
begin
  Size *= 0.5;
  case P.n.GetMajorAxis of
    axX: begin
      if P.n.x < 0 then begin
        V[0].x:=0; V[0].y:=1; V[0].z:=1;
        V[1].x:=0; V[1].y:=1; V[1].z:=-1;
        V[2].x:=0; V[2].y:=-1; V[2].z:=-1;
        V[3].x:=0; V[3].y:=-1; V[3].z:=1;
      end else begin
        V[0].x:=0; V[0].y:=1; V[0].z:=1;
        V[1].x:=0; V[1].y:=-1; V[1].z:=1;
        V[2].x:=0; V[2].y:=-1; V[2].z:=-1;
        V[3].x:=0; V[3].y:=1; V[3].z:=-1;
      end;
    end;
    axY: begin
      if P.n.y < 0 then begin
        V[0].x:=-1; V[0].y:=0; V[0].z:=-1;
        V[1].x:=1; V[1].y:=0; V[1].z:=-1;
        V[2].x:=1; V[2].y:=0; V[2].z:=1;
        V[3].x:=-1; V[3].y:=0; V[3].z:=1;
      end else begin
        V[0].x:=-1; V[0].y:=0; V[0].z:=-1;
        V[1].x:=-1; V[1].y:=0; V[1].z:=1;
        V[2].x:=1; V[2].y:=0; V[2].z:=1;
        V[3].x:=1; V[3].y:=0; V[3].z:=-1;
      end;
    end;
    axZ: begin
      if P.n.z < 0 then begin
        V[0].x:=-1; V[0].y:=1; V[0].z:=0;
        V[1].x:=1; V[1].y:=1; V[1].z:=0;
        V[2].x:=1; V[2].y:=-1; V[2].z:=0;
        V[3].x:=-1; V[3].y:=-1; V[3].z:=0;
      end else begin
        V[0].x:=-1; V[0].y:=1; V[0].z:=0;
        V[1].x:=-1; V[1].y:=-1; V[1].z:=0;
        V[2].x:=1; V[2].y:=-1; V[2].z:=0;
        V[3].x:=1; V[3].y:=1; V[3].z:=0;
      end;
    end;
  end;
  for I:=0 to 3 do V[I]:=P.Projected(V[I]);
  C.x:=(V[0].x + V[1].x + V[2].x + V[3].x)*0.25;
  C.y:=(V[0].y + V[1].y + V[2].y + V[3].y)*0.25;
  C.z:=(V[0].z + V[1].z + V[2].z + V[3].z)*0.25;
  for I:=0 to 3 do V[I]:=V[I].Subbed(C).Normalized.Scaled(Size).Added(C);
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glDisable(GL_LIGHTING);
  glDepthMask(GL_FALSE);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glColor4d(Color.r, Color.g, Color.b, 1.0);
  glBegin(GL_LINE_STRIP);
  for I:=0 to 4 do glVertex3dv(@V[I and 3].x);
  glEnd;
  glBegin(GL_LINES);
  glVertex3d(C.x, C.y, C.z);
  glVertex3d(C.x + P.n.x*Size, C.y + P.n.y*Size, C.z + P.n.z*Size);
  glEnd;
  glColor4d(Color.r, Color.g, Color.b, 0.25);
  glBegin(GL_QUADS);
  for I:=0 to 3 do glVertex3dv(@V[I].x);
  glEnd;
  glPopAttrib;
end;

end.

