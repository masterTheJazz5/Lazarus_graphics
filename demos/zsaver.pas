
Program Zsaver;
//Copyright (c) 1996 by Richard Jasmin
//sort of a computer 'joke'...sleeping computer hits some 'Z's...

//needs a SDL patch- Im working the drawing routines in memory, from memory...BLIND.
//originally written for Borland Pascal BGI.

uses
    crt,sdl2,lazgfx;

var
    //Color:Word; //only works for 16 bit CPUs
    //Color:byte; //only works for 8bit CPUs
    Color:PSDL_Color; //32bit CPUs
    startx,starty,y1,x1,y2,x2,y3,x3:word;

procedure DrawZ;
var
    sizeofZ:word;

begin

//you can render directly into a Texture- or do it in a surface and RenderCopy
//use of SDLv1 compared to SDLv2 will determine which is used.

//draw a Z

//THIS IS ONLY one 'Z'.

    sizeofZ:=Random(500); //size in pixels
    Color:=Random(Maxcolors);
    if color = 0 then Color:=Random(MaxColors); //try again
    
    startX:=Random(MaxX);
    startY:=Random(MaxY);

    if (startX > (MaxX - sizeofZ)) then begin //try again- (The 'Z' is cutoff.)
        startX:=Random(MaxX);
        startY:=Random(MaxY);
    end;

    //the math is a bit funky to apply.
    //we use "Relative line co-ords" -but also modified HLine and VLIne to draw with
    //one variable doesnt change- (X or Y)

    //from start to length of line, draw pixels

    x2:= (startX+sizeofZ);
    Line(startX,startY,x2,startY);

    //drop Y down, reset X -starting at last position(LineRel)
    y2:= (startY+sizeofZ);
    x3:= (startX);

    Line(x2,startY,x3,y2);

    //repeat the first line
    
    Line(x3,y2,x2,y2);

//done 

RenderCopy(Surface,Texture);
RenderPresent(Rederer,Texture);

end;

Procedure FEtchInput;

//with SDL_KeyBuffer do begin...

//if KBHit then exit;

//end
end;

var
	quit:boolean;

//mainSDL loop
begin
   quit:=fase;

	repeat
		DrawZ; //never block here
		FetchInput;
		SDL_Delay(10);
	until quit=true;
    SDL_delay(2000);
	SEtBGColor(Black);
end.


